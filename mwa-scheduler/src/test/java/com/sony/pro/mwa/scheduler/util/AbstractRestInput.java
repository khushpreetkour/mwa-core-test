package com.sony.pro.mwa.scheduler.util;

import java.util.Map;

public abstract class AbstractRestInput implements IRestInput {

	
	protected class MapReader {
		Map<String, Object> map;
		public MapReader(Map<String, Object> map) {
			this.map = map;
		}
		public String get(String key) {
			Object obj = this.map.get(key);
			return (obj == null) ? null : obj.toString();
		}
	}

	
	@Override
	public abstract String getHttpMethod();

	@Override
	public abstract  void setHttpMethod(String httpMethod); 

	@Override
	public abstract String getUrl();
	
	@Override
	public abstract void setUrl(String url);

	@Override
	public abstract String getJsonParam();

	@Override
	abstract public void setJsonParam(String jsonParam);

}
