package com.sony.pro.mwa.scheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.scheduler.util.IRestInput;
import com.sony.pro.mwa.scheduler.util.RestClient;
import com.sony.pro.mwa.scheduler.util.RestInput;
import com.sony.pro.mwa.scheduler.util.TestUtility;

public class UT_SelectAllSimpleTriggerInfo {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	RestClient mwaClient;

	final String TEST_PROP_FILE = "src\\test\\resources\\testScheduler.properties";
	final static String KEY_UT_PROP = "UT_schedler";

	private static PropertiesConfiguration config;
	private static String correntMethodName;
	private static List<String> schedIdsOfSimpleTrigger;
	private static List<String> schedIdsOfCronTrigger;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		// 初期化
		schedIdsOfSimpleTrigger = new ArrayList<>();
		schedIdsOfCronTrigger = new ArrayList<>();
		mwaClient = new RestClient("01");

		try {
			config = new PropertiesConfiguration(TEST_PROP_FILE);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

		// simpleTrigger,cronTriggerが一件も登録されていないことを確認
		{
			Map<String, Object> selectResultMapOfSimpleTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".simple.url"));
			Map<String, Object> selectResultMapOfCronTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".cron.url"));

			if (0 != (int) selectResultMapOfSimpleTrigger.get("count") || 0 != (int) selectResultMapOfCronTrigger.get("count")) {
				logger.error("Please the trigger registration number to the of 0.");
				throw new Exception();
			}
		}
	}

	@After
	public void tearDown() throws Exception
	{
		// trigger全て削除
		IRestInput input4delete = new RestInput();
		for (int count = 0; count < 2; count++) {
			if (0 == count) {
				input4delete.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			} else {
				input4delete.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
			}
			mwaClient.rest("DELETE", input4delete);
		}
	}

	// simpleTriggerを2件、cronTriggerを3件登録状態で、cronTrigger全件検索
	@SuppressWarnings("unchecked")
	@Test
	public void UT_SelectAllSimpleTriggerInfo_0001()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerを2件、cronTriggerを3件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			for (int count = 0; count < 5; count++) {
				if (0 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
				} else if (2 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".cron.jsonParameter").toArray(), ","));
				}
				mwaClient.rest("POST", input4addTrigger);

				// 作成したIDを配列に保管
				String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
				if (1 >= count) {
					schedIdsOfSimpleTrigger.add(schedId);
				} else if (2 <= count) {
					schedIdsOfCronTrigger.add(schedId);
				}
			}
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		logger.info("cronTriggerの登録件数 -> " + schedIdsOfCronTrigger.size());
		// 登録したsimpleTriggerのschedIdを出力
		for (int i = 0; i < schedIdsOfSimpleTrigger.size(); i++) {
			logger.info("登録したsimpleTriggerのschedId" + (i + 1) + ":" + schedIdsOfSimpleTrigger.get(i));
		}

		// simpleTrigger情報を検索
		Map<String, Object> selectResultMap = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".simple.url"));
		// 検索結果がschedIdsOfCronTrigger.size()と同値であること
		logger.info("simpleTrigger情報の検索結果件数 -> " + (int) selectResultMap.get("count"));
		Assert.assertTrue(schedIdsOfSimpleTrigger.size() == (int) selectResultMap.get("count"));

		// cronTriggerの情報のみ取得していること
		Assert.assertTrue(TestUtility.checkSchedId(schedIdsOfSimpleTrigger, (List<Map<String, Object>>) selectResultMap.get("models")));

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// cronTriggerのみ3件登録状態で、cronTrigger全件検索
	@Test
	public void UT_SelectAllSimpleTriggerInfo_0002()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// cronTriggerを3件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			for (int count = 0; count < 3; count++) {
				if (0 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".cron.jsonParameter").toArray(), ","));
				}
				mwaClient.rest("POST", input4addTrigger);

				// 作成したIDを配列に保管
				String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
				schedIdsOfCronTrigger.add(schedId);
			}
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		logger.info("cronTriggerの登録件数 -> " + schedIdsOfCronTrigger.size());

		// simpleTrigger情報を検索
		Map<String, Object> selectResultMap = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".simple.url"));
		// 検索結果が0であること
		logger.info("simpleTrigger情報の検索結果件数 -> " + (int) selectResultMap.get("count"));
		Assert.assertTrue(0 == (int) selectResultMap.get("count"));

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}
}
