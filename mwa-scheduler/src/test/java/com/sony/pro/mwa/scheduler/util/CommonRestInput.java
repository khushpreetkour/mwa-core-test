package com.sony.pro.mwa.scheduler.util;


/**
 * Rest通信を行うために必要な情報を格納するクラスです。
 */
public class CommonRestInput extends AbstractRestInput {
	

	private String httpMethod = null;
	private String url = null;
	private String jsonParam = null;

	@Override
	public String getHttpMethod() {
		return httpMethod;
	}

	@Override
	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getJsonParam() {
		return jsonParam;
	}

	@Override
	public void setJsonParam(String jsonParam) {
		this.jsonParam = jsonParam;
	}
}
