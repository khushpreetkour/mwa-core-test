package com.sony.pro.mwa.scheduler;

import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.scheduler.util.IRestInput;
import com.sony.pro.mwa.scheduler.util.RestClient;
import com.sony.pro.mwa.scheduler.util.RestInput;
import com.sony.pro.mwa.scheduler.util.TestStringUtils;
import com.sony.pro.mwa.scheduler.util.TestUtility;

public class UT_addCronTrigger {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	RestClient mwaClient;

	final String TEST_PROP_FILE = "src\\test\\resources\\testScheduler.properties";

	final static String REPLACE_SCHED_ID = "@@SCHED_ID@@";

	private static PropertiesConfiguration config;
	private final static String KEY_UT_PROP = "UT_schedler";
	private static String correntMethodName;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		mwaClient = new RestClient("01");
		try {
			config = new PropertiesConfiguration(TEST_PROP_FILE);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

		String deleteUrl = config.getString(KEY_UT_PROP + ".cron.url");
		IRestInput input4delete = new RestInput();
		input4delete.setUrl(deleteUrl);
		mwaClient.rest("DELETE", input4delete);
	}

	@After
	public void tearDown() throws Exception
	{
		String deleteUrl = config.getString(KEY_UT_PROP + ".cron.url");
		IRestInput input4delete = new RestInput();
		input4delete.setUrl(deleteUrl);
		mwaClient.rest("DELETE", input4delete);
	}

	// schedIdをキーとて検索し、結果を返します。
	private String getSelectResultJson(String schedId)
	{
		String selectUrl = config.getString(KEY_UT_PROP + ".cron.url2");
		selectUrl = selectUrl.replaceFirst(REPLACE_SCHED_ID, schedId);
		IRestInput input4select = new RestInput();
		input4select.setUrl(selectUrl);
		mwaClient.rest("GET", input4select);

		return mwaClient.getJsonResult();
	}

	// HTTP_METHOD_TYPEがPOST
	@Test
	public void UT_addCronTrigger_0001()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{ \"cronExpression\": \"" + CRON_EXPRESSION + "\",\"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\", " + " \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// HTTP_METHOD_TYPEがGET
	@Test
	public void UT_addCronTrigger_0002()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{ \"cronExpression\": \"" + CRON_EXPRESSION + "\",\"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\", " + " \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// HTTP_METHOD_TYPEがPUT
	@Test
	public void UT_addCronTrigger_0003()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{ \"cronExpression\": \"" + CRON_EXPRESSION + "\",\"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\", " + " \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// HTTP_METHOD_TYPEがDELETE
	@Test
	public void UT_addCronTrigger_0004()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{ \"cronExpression\": \"" + CRON_EXPRESSION + "\",\"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\", " + " \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// CRON_EXPRESSIONが一分刻み指定
	@Test
	public void UT_addCronTrigger_0005()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{ \"cronExpression\": \"" + CRON_EXPRESSION + "\",\"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\", " + " \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// PARAMETERがある
	@Test
	public void UT_addCronTrigger_0006()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String PARAMETER = StringUtils.join(config.getList(correntMethodName + ".parameter").toArray(), ",");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"parameter\": \"" + PARAMETER + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT
				+ "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// timeZoneがある①
	@Test
	public void UT_addCronTrigger_0007()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");
		final String TIME_ZONE = config.getString(correntMethodName + ".timeZone");

		input.setJsonParam("{  \"timeZone\": \"" + TIME_ZONE + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT
				+ "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// timeZoneがある②
	@Test
	public void UT_addCronTrigger_0008()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");
		final String TIME_ZONE = config.getString(correntMethodName + ".timeZone");

		input.setJsonParam("{  \"timeZone\": \"" + TIME_ZONE + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT
				+ "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// DESCRIPTIONがある
	@Test
	public void UT_addCronTrigger_0009()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String DESCRIPTION = config.getString(correntMethodName + ".description");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{ \"description\": \"" + DESCRIPTION + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \""
				+ ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// Jsonにセットするパラメータが全てある
	@Test
	public void UT_addCronTrigger_0010()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String DESCRIPTION = config.getString(correntMethodName + ".description");
		final String PARAMETER = StringUtils.join(config.getList(correntMethodName + ".parameter").toArray(), ",");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");
		final String TIME_ZONE = config.getString(correntMethodName + ".timeZone");

		input.setJsonParam("{  \"timeZone\": \"" + TIME_ZONE + "\", \"parameter\": \"" + PARAMETER + "\",\"cronExpression\": \"" + CRON_EXPRESSION + "\",\"description\": \"" + DESCRIPTION
				+ "\",  \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// DESCRIPTIONのバイト数が256以上(257byte)
	@Test
	public void UT_addCronTrigger_1001()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String DESCRIPTION = config.getString(correntMethodName + ".description");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{ \"description\": \"" + DESCRIPTION + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \""
				+ ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// 対応外のTimeZoneのID
	@Test
	public void UT_addCronTrigger_1002()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");
		final String TIME_ZONE = config.getString(correntMethodName + ".timeZone");

		input.setJsonParam("{  \"timeZone\": \"" + TIME_ZONE + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT
				+ "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// TimeZoneの値が空文字
	@Test
	public void UT_addCronTrigger_1003()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");
		final String TIME_ZONE = config.getString(correntMethodName + ".timeZone");

		input.setJsonParam("{  \"timeZone\": \"" + TIME_ZONE + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT
				+ "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// 対応外のパラメータをセット
	@Test
	public void UT_addCronTrigger_1004()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String PARAMETER = StringUtils.join(config.getList(correntMethodName + ".parameter").toArray(), ",");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"parameter\": \"" + PARAMETER + "\", \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT
				+ "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		// resultにsched_idが含まれていること
		if (-1 == mwaClient.getJsonResult().indexOf("sched_id")) {
			Assert.fail();
		}
		String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
		logger.info("schedId -> " + schedId);

		// schedIdをキーとしてsimpleTrigger情報を検索
		String selectResult = getSelectResultJson(schedId);

		logger.info("select結果 -> " + selectResult);
		// select結果がnullでないこと
		Assert.assertNotNull(selectResult);
		if (-1 == selectResult.indexOf(ENDPOINT)) {
			Assert.fail();
		}
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// 対応外のcronExpressionをセット
	@Test
	public void UT_addCronTrigger_1005()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// cronExpressionが空文字
	@Test
	public void UT_addCronTrigger_1006()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// cronExpressionがnull（Jsonにセットしない）
	@Test
	public void UT_addCronTrigger_1007()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");

		input.setJsonParam("{ \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// ENDPOINTが空文字
	@Test
	public void UT_addCronTrigger_1009()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND", "MWARC.00001102" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// ENDPOINTがnull
	@Test
	public void UT_addCronTrigger_1010()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"cronExpression\": \"" + CRON_EXPRESSION + "\",  \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND", "MWARC.00001102" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// HTTP_METHOD_TYPEが値がPOST,GET,PUT,DELETE以外
	@Test
	public void UT_addCronTrigger_1011()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// HTTP_METHOD_TYPEが空文字
	@Test
	public void UT_addCronTrigger_1012()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String HTTP_METHOD_TYPE = config.getString(correntMethodName + ".httpMethodType");
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"cronExpression\": \"" + CRON_EXPRESSION + "\", \"httpMethodType\": \"" + HTTP_METHOD_TYPE + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND", "MWARC.00001102" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// HTTP_METHOD_TYPEがnull
	@Test
	public void UT_addCronTrigger_1013()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		IRestInput input = new RestInput();
		input.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));

		// プロパティファイルからテストパラメータを取得
		final String ENDPOINT = config.getString(correntMethodName + ".endpoint");
		final String CRON_EXPRESSION = config.getString(correntMethodName + ".cronExpression");

		input.setJsonParam("{  \"cronExpression\": \"" + CRON_EXPRESSION + "\",  \"endpoint\": \"" + ENDPOINT + "\"}");

		mwaClient.rest("POST", input);

		Assert.assertNotNull(mwaClient.getJsonResult());
		logger.info(mwaClient.getJsonResult());
		Map<String, Object> result = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		Assert.assertArrayEquals(new String[] { "INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND", "MWARC.00001102" }, new String[] { result.get("message").toString(), result.get("code").toString() });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}
}
