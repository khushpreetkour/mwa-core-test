package com.sony.pro.mwa.scheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.scheduler.util.IRestInput;
import com.sony.pro.mwa.scheduler.util.RestClient;
import com.sony.pro.mwa.scheduler.util.RestInput;
import com.sony.pro.mwa.scheduler.util.TestStringUtils;
import com.sony.pro.mwa.scheduler.util.TestUtility;

public class UT_PauseSimpleTrigger {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	RestClient mwaClient;

	final String TEST_PROP_FILE = "src\\test\\resources\\testScheduler.properties";
	final static String KEY_UT_PROP = "UT_schedler";
	final static String REPLACE_KEY_SCHED_ID = "@@SCHED_ID@@";
	final static String REPLACE_KEY_OPERATION = "@@OPERATION@@";

	private static PropertiesConfiguration config;
	private static String correntMethodName;
	private static List<String> schedIdsOfSimpleTrigger;
	private static List<String> schedIdsOfCronTrigger;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		// 初期化
		schedIdsOfSimpleTrigger = new ArrayList<>();
		schedIdsOfCronTrigger = new ArrayList<>();
		mwaClient = new RestClient("01");

		try {
			config = new PropertiesConfiguration(TEST_PROP_FILE);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

		// simpleTrigger,cronTriggerが一件も登録されていないことを確認
		{
			Map<String, Object> selectResultMapOfSimpleTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".simple.url"));
			Map<String, Object> selectResultMapOfCronTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".cron.url"));

			if (0 != (int) selectResultMapOfSimpleTrigger.get("count") || 0 != (int) selectResultMapOfCronTrigger.get("count")) {
				logger.error("Please the trigger registration number to the of 0.");
				throw new Exception();
			}
		}
	}

	@After
	public void tearDown() throws Exception
	{
		// trigger全て削除
		IRestInput input4delete = new RestInput();
		for (int count = 0; count < 2; count++) {
			if (0 == count) {
				input4delete.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			} else {
				input4delete.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
			}
			mwaClient.rest("DELETE", input4delete);
		}
	}

	// 正常系
	@SuppressWarnings("unchecked")
	@Test
	public void UT_PauseSimpleTrigger_0001()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerを1件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
			mwaClient.rest("POST", input4addTrigger);

			// 作成したIDを配列に保管
			String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
			schedIdsOfSimpleTrigger.add(schedId);
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		// 各schedIdを出力
		logger.info("登録したsimpleTriggerのschedId:" + schedIdsOfSimpleTrigger.get(0));

		// schedIdsOfSimpleTrigger.get(0)のschedIdでPAUSEを要求
		logger.info("◆PAUSEを要求 schedId:" + schedIdsOfSimpleTrigger.get(0) + "◆");
		String url = config.getString(KEY_UT_PROP + ".simple.opeUrl");
		IRestInput input = new RestInput();
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, schedIdsOfSimpleTrigger.get(0)).replace(REPLACE_KEY_OPERATION, "PAUSE"));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		Map<String, Object> resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// ステータスが"SUCCESS"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "SUCCESS" }, new String[] { (String) resultMap.get("value") });

		// schedIdsOfSimpleTrigger.get(0)のsimpleTrigger情報を検索
		String selectUrl = config.getString(KEY_UT_PROP + ".simple.url2");
		selectUrl = selectUrl.replace(REPLACE_KEY_SCHED_ID, schedIdsOfSimpleTrigger.get(0));
		Map<String, Object> selectResultMap = TestUtility.getSelectResult(selectUrl);

		// 検索結果
		List<Map<String, Object>> models = (List<Map<String, Object>>) selectResultMap.get("models");
		Map<String, Object> model = models.get(0);
		final String triggerState = (String) model.get("triggerState");
		logger.info("検索したcronTriggerのschedId:" + model.get("schedId"));
		logger.info("検索したcronTriggerのtriggerState:" + triggerState);
		Assert.assertArrayEquals(new String[] { "PAUSED" }, new String[] { triggerState });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// 存在しないschedId
	@Test
	public void UT_PauseSimpleTrigger_1001()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerを1件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
			mwaClient.rest("POST", input4addTrigger);

			// 作成したIDを配列に保管
			String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
			schedIdsOfSimpleTrigger.add(schedId);
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		// 各schedIdを出力
		logger.info("登録したsimpleTriggerのschedId:" + schedIdsOfSimpleTrigger.get(0));

		// 登録されていないschedIdでPAUSEを要求
		logger.info("◆PAUSEを要求 schedId:00000000-0000-0000-0000-000000000000◆");
		String url = config.getString(KEY_UT_PROP + ".simple.opeUrl");
		IRestInput input = new RestInput();
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, "00000000-0000-0000-0000-000000000000").replace(REPLACE_KEY_OPERATION, "PAUSE"));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		Map<String, Object> resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// エラーの種類が"INVALID_INPUT"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { (String) resultMap.get("message"), (String) resultMap.get("code") });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// 登録してあるcronTriggerで検索
	@Test
	public void UT_PauseSimpleTrigger_1002()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerを2件、cronTriggerを3件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			for (int count = 0; count < 5; count++) {
				if (0 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
				} else if (2 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".cron.jsonParameter").toArray(), ","));
				}
				mwaClient.rest("POST", input4addTrigger);

				// 作成したIDを配列に保管
				String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
				if (1 >= count) {
					schedIdsOfSimpleTrigger.add(schedId);
				} else if (2 <= count) {
					schedIdsOfCronTrigger.add(schedId);
				}
			}
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		logger.info("cronTriggerの登録件数 -> " + schedIdsOfCronTrigger.size());
		// 登録したcronTriggerのschedIdを出力
		for (int i = 0; i < schedIdsOfCronTrigger.size(); i++) {
			logger.info("登録したcronTriggerのschedId" + (i + 1) + ":" + schedIdsOfCronTrigger.get(i));
		}

		// schedIdsOfSimpleTrigger.get(1)のschedIdでPAUSEを要求
		logger.info("◆PAUSEを要求 schedId:" + schedIdsOfCronTrigger.get(1) + "◆");
		String url = config.getString(KEY_UT_PROP + ".simple.opeUrl");
		IRestInput input = new RestInput();
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, schedIdsOfCronTrigger.get(1)).replace(REPLACE_KEY_OPERATION, "PAUSE"));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		Map<String, Object> resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// エラーの種類が"INVALID_INPUT"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { (String) resultMap.get("message"), (String) resultMap.get("code") });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// schedIdをなし(空文字)
	@Test
	public void UT_PauseSimpleTrigger_1003()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerを1件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
			mwaClient.rest("POST", input4addTrigger);

			// 作成したIDを配列に保管
			String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
			schedIdsOfSimpleTrigger.add(schedId);
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		// 各schedIdを出力
		logger.info("登録したsimpleTriggerのschedId:" + schedIdsOfSimpleTrigger.get(0));

		// schedIdをなし(空文字)でPAUSEを要求
		logger.info("◆PAUSEを要求 schedId:" + schedIdsOfSimpleTrigger.get(0) + "◆");
		String url = config.getString(KEY_UT_PROP + ".simple.opeUrl");
		IRestInput input = new RestInput();
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, StringUtils.EMPTY).replace(REPLACE_KEY_OPERATION, "PAUSE"));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		Map<String, Object> resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// ステータスが"SUCCESS"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "SYSTEM_ERROR", "MWARC.0000FF00" }, new String[] { (String) resultMap.get("message"), (String) resultMap.get("code") });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// operationがTEST
	@Test
	public void UT_PauseSimpleTrigger_1004()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerを1件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
			mwaClient.rest("POST", input4addTrigger);

			// 作成したIDを配列に保管
			String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
			schedIdsOfSimpleTrigger.add(schedId);
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		// 各schedIdを出力
		logger.info("登録したcronTriggerのschedId:" + schedIdsOfSimpleTrigger.get(0));

		// schedIdsOfSimpleTrigger.get(1)のschedIdで【TEST】を要求
		logger.info("◆PAUSEを要求 schedId:" + schedIdsOfSimpleTrigger.get(0) + "◆");
		String url = config.getString(KEY_UT_PROP + ".simple.opeUrl");
		IRestInput input = new RestInput();
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, schedIdsOfSimpleTrigger.get(0)).replace(REPLACE_KEY_OPERATION, "TEST"));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		Map<String, Object> resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// エラーの種類が"INVALID_INPUT"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { (String) resultMap.get("message"), (String) resultMap.get("code") });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// operatonなし(空文字)
	@Test
	public void UT_PauseSimpleTrigger_1005()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerを1件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
			mwaClient.rest("POST", input4addTrigger);

			// 作成したIDを配列に保管
			String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
			schedIdsOfSimpleTrigger.add(schedId);
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		// 各schedIdを出力
		logger.info("登録したsimpleTriggerのschedId:" + schedIdsOfSimpleTrigger.get(0));

		// schedIdsOfCronTrigger.get(1)のschedIdで【TEST】を要求
		logger.info("◆PAUSEを要求 schedId:" + schedIdsOfSimpleTrigger.get(0) + "◆");
		String url = config.getString(KEY_UT_PROP + ".simple.opeUrl");
		IRestInput input = new RestInput();
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, schedIdsOfSimpleTrigger.get(0)).replace(REPLACE_KEY_OPERATION, StringUtils.EMPTY));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		Map<String, Object> resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// エラーの種類が"INVALID_INPUT"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { (String) resultMap.get("message"), (String) resultMap.get("code") });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// PAUSE中にPAUSE
	@Test
	public void UT_PauseSimpleTrigger_1006()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// cronTriggerを1件登録する
		{
			IRestInput input4addTrigger = new RestInput();
			input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
			mwaClient.rest("POST", input4addTrigger);

			// 作成したIDを配列に保管
			String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
			schedIdsOfSimpleTrigger.add(schedId);
		}

		// 各登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		// 各schedIdを出力
		logger.info("登録したsimpleTriggerのschedId:" + schedIdsOfSimpleTrigger.get(0));

		// schedIdsOfSimpleTrigger.get(0)のschedIdでPAUSEを要求
		logger.info("◆PAUSEを要求 schedId:" + schedIdsOfSimpleTrigger.get(0) + "◆");
		String url = config.getString(KEY_UT_PROP + ".simple.opeUrl");
		IRestInput input = new RestInput();
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, schedIdsOfSimpleTrigger.get(0)).replace(REPLACE_KEY_OPERATION, "PAUSE"));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		Map<String, Object> resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// ステータスが"SUCCESS"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "SUCCESS" }, new String[] { (String) resultMap.get("value") });

		// schedIdsOfSimpleTrigger.get(0)のschedIdでPAUSEを要求[2回目]
		logger.info("◆PAUSEを要求(2回目) schedId:" + schedIdsOfSimpleTrigger.get(0) + "◆");
		input.setUrl(url.replace(REPLACE_KEY_SCHED_ID, schedIdsOfSimpleTrigger.get(0)).replace(REPLACE_KEY_OPERATION, "PAUSE"));
		input.setJsonParam(StringUtils.EMPTY);
		mwaClient.rest("POST", input);
		resultMap = TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());

		// ステータスが"SUCCESS"であること
		logger.info("実行結果 ->" + resultMap);
		Assert.assertArrayEquals(new String[] { "SUCCESS" }, new String[] { (String) resultMap.get("value") });

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}
}
