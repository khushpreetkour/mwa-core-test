package com.sony.pro.mwa.scheduler.util;

import java.nio.charset.StandardCharsets;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

public class RestClient {

	protected String instanceId = "";

	private String JsonResult;

	private int httpStatus;

	private String httpMethod;

	private String Uri;

	/**
	 * HTTPヘッダの設定
	 */
	private static Header[] headers = {
			new BasicHeader("Content-type", "application/json; charset=UTF-8"),
			new BasicHeader("Accept", "application/json"),
			new BasicHeader("Accept-Charset", "UTF-8") };

	/*-------------------------------------------------------------------------------------------
	 *  コンストラクタ
	 -------------------------------------------------------------------------------------------*/
	public RestClient(String instanceId) {
		this.instanceId = instanceId;
	}

	// UT用コンストラクタ
	public RestClient(String instanceId, Header[] headers) {
		this.instanceId = instanceId;
		RestClient.headers = headers;
	}

	public void rest(String reqesutType, IRestInput restInput) {

		switch (reqesutType) {
		case "POST":
			restPost(restInput);
			break;
		case "GET":
			restGet(restInput);
			break;
		case "PUT":
			restPut(restInput);
			break;
		case "DELETE":
			restDelete(restInput);
			break;
		default:
			break;
		}

	}

	public void restPost(IRestInput restInput) {

		final String url = restInput.getUrl();
		final String jsonParam = restInput.getJsonParam();

		executePost(url, jsonParam);
	}

	public void restGet(IRestInput restInput) {

		final String url = restInput.getUrl();
		executeGet(url);
	}

	public void restPut(IRestInput restInput) {

		final String url = restInput.getUrl();
		final String jsonParam = restInput.getJsonParam();
		executePut(url, jsonParam);
	}

	public void restDelete(IRestInput restInput) {

		final String url = restInput.getUrl();
		executeDelete(url);
	}

	private void executePost(String url, String jsonParam) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPost request = new HttpPost(url);
			request.setEntity(new StringEntity(jsonParam,
					StandardCharsets.UTF_8));
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executeGet(String url) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet request = new HttpGet(url);
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void executePut(String url, String jsonParam) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPut request = new HttpPut(url);
			request.setEntity(new StringEntity(jsonParam,
					StandardCharsets.UTF_8));
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executeDelete(String url) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpDelete request = new HttpDelete(url);
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void httpReauestExecute(CloseableHttpClient httpClient,
			final HttpUriRequest request) throws Exception {

		// HTTPヘッダの設定
		request.setHeaders(RestClient.headers);
		try (CloseableHttpResponse response = httpClient.execute(request)) {
			final HttpEntity entity = response.getEntity();
			final String resultJson = EntityUtils.toString(entity,
					StandardCharsets.UTF_8);
			setHttpStatus(response.getStatusLine().getStatusCode());
			setJsonResult(resultJson);
			Uri = request.getURI().toString();
			httpMethod = request.getMethod();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getLoggerPrefix() {
		return "[" + this.instanceId + "]";
	}

	public String getJsonResult() {
		return JsonResult;
	}

	private void setJsonResult(final String resJson) {

		this.JsonResult = resJson;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	private void setHttpStatus(final int httpSt) {

		this.httpStatus = httpSt;
	}

	public String getHttpMethod() {

		return httpMethod;
	}

	public String Uri() {

		return Uri;
	}
}
