package com.sony.pro.mwa.scheduler.util;


public class RestInput implements IRestInput {

	private String httpMethod;
	private String url;
	private String jsonParam;

	@Override
	public String getHttpMethod() {
		return httpMethod;
	}

	@Override
	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getJsonParam() {
		return jsonParam;
	}

	@Override
	public void setJsonParam(String jsonParam) {
		this.jsonParam = jsonParam;
	}

}
