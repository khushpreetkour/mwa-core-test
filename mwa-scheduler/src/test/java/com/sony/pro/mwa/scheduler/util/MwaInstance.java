package com.sony.pro.mwa.scheduler.util;

import java.util.HashMap;
import java.util.Map;

public class MwaInstance {
	private String instanceId = "";
	private String status = "";
	private String statusDetails = "";
	private String progress = "";
	private String deviceResponseCode = "";
	private String deviceResponseDetails = "";
	private Map<String, Object> outputDetails = new HashMap<String, Object>();

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDetails() {
		return statusDetails;
	}

	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public String getDeviceResponseCode() {
		return deviceResponseCode;
	}

	public void setDeviceResponseCode(String deviceResponseCode) {
		this.deviceResponseCode = deviceResponseCode;
	}

	public String getDeviceResponseDetails() {
		return deviceResponseDetails;
	}

	public void setDeviceResponseDetails(String deviceResponseDetails) {
		this.deviceResponseDetails = deviceResponseDetails;
	}

	public Map<String, Object> getOutputDetails() {
		return outputDetails;
	}
}
