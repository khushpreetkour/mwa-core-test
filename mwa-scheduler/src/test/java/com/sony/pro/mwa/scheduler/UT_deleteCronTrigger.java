package com.sony.pro.mwa.scheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.scheduler.util.IRestInput;
import com.sony.pro.mwa.scheduler.util.RestClient;
import com.sony.pro.mwa.scheduler.util.RestInput;
import com.sony.pro.mwa.scheduler.util.TestStringUtils;
import com.sony.pro.mwa.scheduler.util.TestUtility;

public class UT_deleteCronTrigger {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	RestClient mwaClient;

	final String TEST_PROP_FILE = "src\\test\\resources\\testScheduler.properties";
	private final static String KEY_UT_PROP = "UT_schedler";
	final static String REPLACE_KEY_SCHED_ID = "@@SCHED_ID@@";

	private static PropertiesConfiguration config;
	private static String correntMethodName;
	private static List<String> schedIdsOfSimpleTrigger;
	private static List<String> schedIdsOfCronTrigger;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		// 初期化
		schedIdsOfSimpleTrigger = new ArrayList<>();
		schedIdsOfCronTrigger = new ArrayList<>();
		mwaClient = new RestClient("01");

		try {
			config = new PropertiesConfiguration(TEST_PROP_FILE);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

		// simpleTrigger,cronTriggerが1件も登録されていないことを確認
		{
			Map<String, Object> selectResultMapOfSimpleTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".simple.url"));
			Map<String, Object> selectResultMapOfCronTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".cron.url"));

			if (0 != (int) selectResultMapOfSimpleTrigger.get("count") || 0 != (int) selectResultMapOfCronTrigger.get("count")) {
				logger.error("Please the trigger registration number to the of 0.");
				throw new Exception();
			}
		}
	}

	@After
	public void tearDown() throws Exception
	{
		// trigger全て削除
		IRestInput input4delete = new RestInput();
		for (int count = 0; count < 2; count++) {
			if (0 == count) {
				input4delete.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
			} else {
				input4delete.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
			}
			mwaClient.rest("DELETE", input4delete);
		}
	}

	// simpleTriggerとcronTriggerが登録してある状態で、simpleTrigger1件削除用メソッド実行
	@Test
	public void UT_deleteAllCronTrigger_0001()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerとcronTriggerを2件ずつ登録する
		{
			IRestInput input4addTrigger = new RestInput();
			for (int count = 0; count < 4; count++) {
				if (0 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
				} else if (2 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".cron.jsonParameter").toArray(), ","));
				}
				mwaClient.rest("POST", input4addTrigger);

				// 作成したIDを配列に保管
				String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
				if (1 >= count) {
					schedIdsOfSimpleTrigger.add(schedId);
				} else if (2 <= count) {
					schedIdsOfCronTrigger.add(schedId);
				}
			}
		}

		// 削除前の登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		logger.info("cronTriggerの登録件数 -> " + schedIdsOfCronTrigger.size());

		// simpleTrigger1件削除(対象：schedIdsOfSimpleTriggerの1件目)
		IRestInput input4delete = new RestInput();
		String tempUrl = config.getString(KEY_UT_PROP + ".cron.url2");
		input4delete.setUrl(tempUrl.replaceAll(REPLACE_KEY_SCHED_ID, schedIdsOfCronTrigger.get(0)));
		mwaClient.rest("DELETE", input4delete);
		String deleteResult = mwaClient.getJsonResult();
		logger.info("delete実行結果 -> " + deleteResult);

		Map<String, Object> deleteResultMap = TestStringUtils.convertJsonToMap(deleteResult);
		// ステータスが"SUCCESS"であること
		Assert.assertArrayEquals(new String[] { "SUCCESS" }, new String[] { (String) deleteResultMap.get("value") });

		// simpleTrigger情報を検索
		Map<String, Object> selectResultMapOfSimpleTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".simple.url"));
		// 検索結果がschedIdsOfSimpleTriggerの件数と同値であること
		logger.info("simpleTrigger情報の検索結果件数 -> " + (int) selectResultMapOfSimpleTrigger.get("count"));
		Assert.assertTrue(schedIdsOfSimpleTrigger.size() == (int) selectResultMapOfSimpleTrigger.get("count"));

		// cronTrigger情報を検索
		Map<String, Object> selectResultMapOfCronTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".cron.url"));
		// 検索結果が(schedIdsOfCronTrigger.size() - 1)と同値であること
		logger.info("cronTrigger情報の検索結果件数 -> " + (int) selectResultMapOfCronTrigger.get("count"));
		Assert.assertTrue((schedIdsOfCronTrigger.size() - 1) == (int) selectResultMapOfCronTrigger.get("count"));

		// schedIdsOfCronTriggerの2件目が削除されていないこと
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> cronTriggerModels = (List<Map<String, Object>>) selectResultMapOfCronTrigger.get("models");
		Assert.assertTrue(1 == cronTriggerModels.size());
		if (1 == cronTriggerModels.size()) {
			Map<String, Object> modelDetail = cronTriggerModels.get(0);
			Assert.assertTrue(modelDetail.containsValue(schedIdsOfCronTrigger.get(1)));
		} else {
			Assert.fail();
		}

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}

	// simpleTriggerとcronTriggerが登録してある状態で、simpleTrigger1件削除用メソッド実行（引数が存在しないSCHED_ID）
	@Test
	public void UT_deleteAllCronTrigger_1001()
	{
		correntMethodName = new Throwable().getStackTrace()[0].getMethodName();
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");
		logger.info("▼▼ START " + correntMethodName);
		logger.info("▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼");

		// simpleTriggerとcronTriggerを2件ずつ登録する
		{
			IRestInput input4addTrigger = new RestInput();
			for (int count = 0; count < 4; count++) {
				if (0 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".simple.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".simple.jsonParameter").toArray(), ","));
				} else if (2 == count) {
					input4addTrigger.setUrl(config.getString(KEY_UT_PROP + ".cron.url"));
					input4addTrigger.setJsonParam(StringUtils.join(config.getList(KEY_UT_PROP + ".cron.jsonParameter").toArray(), ","));
				}
				mwaClient.rest("POST", input4addTrigger);

				// 作成したIDを配列に保管
				String schedId = TestUtility.getSchedId(mwaClient.getJsonResult());
				if (1 >= count) {
					schedIdsOfSimpleTrigger.add(schedId);
				} else if (2 <= count) {
					schedIdsOfCronTrigger.add(schedId);
				}
			}
		}

		// 削除前の登録件数
		logger.info("simpleTriggerの登録件数 -> " + schedIdsOfSimpleTrigger.size());
		logger.info("cronTriggerの登録件数 -> " + schedIdsOfCronTrigger.size());

		// simpleTrigger1件削除(対象：存在しないSCHED_ID)
		IRestInput input4delete = new RestInput();
		String tempUrl = config.getString(KEY_UT_PROP + ".simple.url2");
		input4delete.setUrl(tempUrl.replaceAll(REPLACE_KEY_SCHED_ID, "00000000-0000-0000-0000-000000000000"));
		mwaClient.rest("DELETE", input4delete);
		String deleteResult = mwaClient.getJsonResult();
		logger.info("delete実行結果 -> " + deleteResult);

		Map<String, Object> deleteResultMap = TestStringUtils.convertJsonToMap(deleteResult);
		// エラーの種類が"INVALID_INPUT"であること
		Assert.assertArrayEquals(new String[] { "INVALID_INPUT", "MWARC.00001000" }, new String[] { (String) deleteResultMap.get("message"), (String) deleteResultMap.get("code") });

		// simpleTrigger情報を検索
		Map<String, Object> selectResultMapOfSimpleTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".simple.url"));
		// 検索結果がschedIdsOfSimpleTriggerの件数と同値であること
		logger.info("simpleTrigger情報の検索結果件数 -> " + (int) selectResultMapOfSimpleTrigger.get("count"));
		Assert.assertTrue(schedIdsOfSimpleTrigger.size() == (int) selectResultMapOfSimpleTrigger.get("count"));

		// cronTrigger情報を検索
		Map<String, Object> selectResultMapOfCronTrigger = TestUtility.getSelectResult(config.getString(KEY_UT_PROP + ".cron.url"));
		// 検索結果がschedIdsOfCronTriggerの件数と同値であること
		logger.info("cronTrigger情報の検索結果件数 -> " + (int) selectResultMapOfCronTrigger.get("count"));
		Assert.assertTrue(schedIdsOfCronTrigger.size() == (int) selectResultMapOfCronTrigger.get("count"));

		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
		logger.info("▲▲ END " + new Throwable().getStackTrace()[0].getMethodName());
		logger.info("▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲");
	}
}
