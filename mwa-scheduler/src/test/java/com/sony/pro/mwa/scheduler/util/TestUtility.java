package com.sony.pro.mwa.scheduler.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.message.BasicHeader;

import com.sony.pro.mwa.common.log.MwaLogger;

public class TestUtility {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	/**
	 * createMwaRestClient MWAへの通信用クライアントを作成します。（ResponseがJsonとなる）
	 * 
	 * @return
	 */
	public static RestClient createMwaRestClient()
	{
		final Header[] headers = { new BasicHeader("Content-type", "application/json; charset=UTF-8"), new BasicHeader("Accept", "application/json"), new BasicHeader("Accept-Charset", "UTF-8") };

		return new RestClient("", headers);
	}

	/**
	 * Mwaからの戻りのresultXml（実態はJson）を入れると、InstanceIdを取得します。
	 * 
	 * @param responseBodyJson
	 * @return
	 */
	public static String getMwaResponseInstance(String responseBodyJson)
	{

		List<Object> resultList = TestStringUtils.convertJsonToList(responseBodyJson);
		String instancdId = "";
		for (Object obj : resultList) {
			if (obj instanceof Map) {
				Map<String, Object> resultMap = (Map<String, Object>) obj;
				if (resultMap.containsValue("ActivityInstanceId")) {
					instancdId = (String) resultMap.get("value");
					break;
				}
			}
		}

		return instancdId;
	}

	/**
	 * Mwaからの戻りのresultから、SchedIdを取得します。
	 * 
	 * @param responseBodyJson
	 * @return
	 */
	public static String getSchedId(String responseBodyJson)
	{
		return responseBodyJson.substring(27, responseBodyJson.lastIndexOf("\""));
	}

	/**
	 * instanceIdからInstanceの状態をDBから取得します。
	 * 
	 * @param instanceId
	 * @return
	 */
	public static MwaInstance getMwaInstanceStatus(String instanceId)
	{

		MwaInstance instance = new MwaInstance();

		if (StringUtils.isNotEmpty(instanceId)) {
			IRestInput input = new CommonRestInput();
			input.setUrl("http://localhost:8080/mwa/api/v1/activity-instances/" + instanceId);
			RestClient mwaClient = createMwaRestClient();
			mwaClient.rest("GET", input);
			String instanceDetailJson = mwaClient.getJsonResult();
			Map<String, Object> instanceDetailMap = TestStringUtils.convertJsonToMap(instanceDetailJson);

			instance.setInstanceId(instanceId);

			if (instanceDetailMap.containsKey("progress") && (instanceDetailMap.get("progress") instanceof Integer)) {
				Object pogressObj = instanceDetailMap.get("progress");
				String progressStr = String.valueOf(pogressObj);
				instance.setProgress(progressStr);
			}

			if (instanceDetailMap.containsKey("status") && (instanceDetailMap.get("status") instanceof Map)) {
				Map<String, Object> statusMap = (Map<String, Object>) instanceDetailMap.get("status");
				String status = (String) statusMap.get("name");
				instance.setStatus(status);
			}

			if (instanceDetailMap.containsKey("statusDetails") && (instanceDetailMap.get("statusDetails") instanceof Map)) {
				Map<String, Object> statusMap = (Map<String, Object>) instanceDetailMap.get("statusDetails");
				String deviceResponse = (String) statusMap.get("deviceResponse");
				instance.setDeviceResponseCode(deviceResponse);
				String deviceResponseDetails = (String) statusMap.get("deviceResponseDetails");
				instance.setDeviceResponseDetails(deviceResponseDetails);

				if (statusMap.get("result") instanceof Map) {
					Map<String, Object> resultMap = (Map<String, Object>) statusMap.get("result");
					if (resultMap.containsKey("code")) {
						instance.setStatusDetails((String) resultMap.get("code"));
					}

				}
			}

			if (instanceDetailMap.containsKey("outputDetailList") && (instanceDetailMap.get("outputDetailList") instanceof List)) {
				List<Object> outputList = (List<Object>) instanceDetailMap.get("outputDetailList");
				for (Object objOutput : outputList) {
					if (objOutput instanceof Map) {
						Map<String, Object> output = (Map<String, Object>) objOutput;
						if (output.containsKey("key") && (output.get("key") instanceof String)) {
							instance.getOutputDetails().put((String) output.get("key"), output.get("value"));
						}
					}
				}
			}
		}

		return instance;
	}

	/**
	 * restメソッドを呼び出してinstanceIdを取得するクラスです。
	 * 
	 * @param reqesutType
	 * @param restInput
	 * @return
	 */
	public static MwaInstance rest(String reqesutType, IRestInput restInput)
	{

		// インスタンス化
		RestClient restClient = new RestClient("");
		MwaInstance instance = null;
		// restメソッドを呼び出す
		restClient.rest(reqesutType, restInput);
		// HttpStatusが200なら.....
		if (restClient.getHttpStatus() == HttpStatus.SC_OK) {
			// 3秒スリープ
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			final String resultJson = restClient.getJsonResult();
			// InstanceIdを取得
			String instanceId = TestUtility.getMwaResponseInstance(resultJson);
			instance = TestUtility.getMwaInstanceStatus(instanceId);
		} else {

			// 取得したJsonの値をマッピングする
			Map<String, Object> responseMap = TestStringUtils.convertJsonToMap(restClient.getJsonResult());

			// エラーメッセージ、エラーコードの取得
			String message = (String) responseMap.get("message");
			String deviceMessage = (String) responseMap.get("deviceMessage");
			String deviceCode = (String) responseMap.get("deviceCode");
			String code = (String) responseMap.get("code");

			// エラーメッセージ、エラーコードの表示
			instance = new MwaInstance();
			instance.setStatusDetails(message);
			instance.setDeviceResponseDetails(deviceMessage);
			instance.setDeviceResponseCode(deviceCode);
			instance.setStatus(code);
		}

		// Mwainstanceをかえす
		return instance;

	}

	// urlに対して、GETでREST通信を行い、結果をMap<String,Object>で返します。
	public static Map<String, Object> getSelectResult(String url)
	{
		RestClient mwaClient = new RestClient("01");
		IRestInput input4select = new RestInput();
		input4select.setUrl(url);
		mwaClient.rest("GET", input4select);

		return TestStringUtils.convertJsonToMap(mwaClient.getJsonResult());
	}

	// targetModelsの各schedIdがschedIdsに含まれていること
	public static boolean checkSchedId(List<String> schedIds, List<Map<String, Object>> targetModels)
	{
		int i = 1;
		nextTarget: for (Map<String, Object> target : targetModels) {
			for (String schedId : schedIds) {
				if (schedId.equals(target.get("schedId"))) {
					i++;
					continue nextTarget;
				}
			}
			return false;
		}
		return true;
	}
}
