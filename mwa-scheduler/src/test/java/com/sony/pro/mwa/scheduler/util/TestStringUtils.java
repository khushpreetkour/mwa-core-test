package com.sony.pro.mwa.scheduler.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * ODAライブラリとの通信関連で使用するStringUtilsクラスです。
 */
public class TestStringUtils {


	/**
	 * 
	 * @param targetMap
	 * @return
	 */
	public static String convetMapToJson(Map<String, Object> targetMap) {
		String json = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			json = objectMapper.writeValueAsString(targetMap);
		} catch (JsonGenerationException e) {

		} catch (JsonMappingException e) {
	
		} catch (IOException e) {
		
		}
		return json;
	}

	/**
	 * 
	 * @param json
	 * @return
	 */
	public static Map<String, Object> convertJsonToMap(String json) {
		// json(mwa) --> map(activity)
		Map<String, Object> kv = null;
		try {
			kv = new ObjectMapper().readValue(json, HashMap.class);
		} catch (JsonParseException e) {
	
		} catch (JsonMappingException e) {
		
		} catch (IOException e) {
		
		}
		return kv;
	}

	/**
	 * 
	 * @param targetMap
	 * @return
	 */
	public static String convetListToJson(List<Object> targetList) {
		String json = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			json = objectMapper.writeValueAsString(targetList);
		} catch (JsonGenerationException e) {
	
		} catch (JsonMappingException e) {
	
		} catch (IOException e) {
	
		}
		return json;
	}

	/**
	 * 
	 * @param json
	 * @return
	 */
	public static List<Object> convertJsonToList(String json) {
		// json(mwa) --> map(activity)
		List<Object> list = null;
		try {
			list = new ObjectMapper().readValue(json, List.class);
		} catch (JsonParseException e) {
	
		} catch (JsonMappingException e) {
		
		} catch (IOException e) {

		}
		return list;
	}

	/**
	 * urlをエンコードします。
	 * 
	 * エンコードの方式はURLEncode.encod(str, "UTF-8")に準拠し、
	 * エンコードされない文字"*"、"-"、" "をそれぞれ"%2a"、"%2d"、"%20" にreplaceします。
	 * 
	 * @param str
	 *            対象のurl
	 * @return エンコードされたurl
	 */
	public static String urlEncoder(String str) {
		try {
			str = URLEncoder.encode(str, "UTF-8");
			str = str.replace("*", "%2a");
			str = str.replace("-", "%2d");
			str = str.replace("+", "%20");
		} catch (UnsupportedEncodingException e) {
			
		}

		return str;
	}

}
