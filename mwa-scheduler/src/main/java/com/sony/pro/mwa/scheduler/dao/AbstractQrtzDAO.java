package com.sony.pro.mwa.scheduler.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class AbstractQrtzDAO {
	
	private BasicDataSource dataSource;

	public BasicDataSource getDataSource() {
		return dataSource;
	}

	@Autowired
	public void setDataSource(BasicDataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * クローズ処理
	 * 
	 * @param conn
	 * @param pstmt
	 * @param resultSet
	 */
	protected static void close(Connection conn, PreparedStatement pstmt, ResultSet resultSet)
	{
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * クローズ処理
	 * 
	 * @param conn
	 * @param pstmt
	 */
	protected static void close(Connection conn, PreparedStatement pstmt)
	{

		if (pstmt != null) {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
