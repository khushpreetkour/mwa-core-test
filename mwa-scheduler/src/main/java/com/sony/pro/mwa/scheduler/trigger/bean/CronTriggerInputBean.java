package com.sony.pro.mwa.scheduler.trigger.bean;

public class CronTriggerInputBean extends TriggerInputBean{

	private String cronExpression;
	
	public String getCronExpression()
	{
		return cronExpression;
	}

	public void setCronExpression(String cronExpression)
	{
		this.cronExpression = cronExpression;
	}

}
