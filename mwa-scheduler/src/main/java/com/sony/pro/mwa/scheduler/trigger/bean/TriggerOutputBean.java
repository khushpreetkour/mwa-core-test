package com.sony.pro.mwa.scheduler.trigger.bean;

public class TriggerOutputBean {

	private String schedId;
	
	private String responseBody;
	
	private String httpStatusCode;

	public String getSchedId()
	{
		return schedId;
	}

	public void setSchedId(String schedId)
	{
		this.schedId = schedId;
	}

	public String getResponseBody()
	{
		return responseBody;
	}

	public void setResponseBody(String responseBody)
	{
		this.responseBody = responseBody;
	}

	public String getHttpStatusCode()
	{
		return httpStatusCode;
	}

	public void setHttpStatusCode(String httpStatusCode)
	{
		this.httpStatusCode = httpStatusCode;
	}
	
	
	
	
}
