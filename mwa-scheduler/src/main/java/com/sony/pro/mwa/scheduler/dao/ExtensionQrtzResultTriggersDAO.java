package com.sony.pro.mwa.scheduler.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.quartz.utils.DBConnectionManager;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.common.SchedulerCommon;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzResultTriggers;
import com.sony.pro.mwa.scheduler.dao.utils.ResultSetMapper;

public class ExtensionQrtzResultTriggersDAO extends AbstractQrtzDAO implements ISchedulerConstants {

	private static final MwaLogger logger = MwaLogger.getLogger(ExtensionQrtzResultTriggersDAO.class);

	/**
	 * QQUARTZ.EXTENSION_QRTZ_RESULT_TRIGGERS　テーブル情報の取得
	 * 
	 * @return
	 */
	public ArrayList<HashMap<String, Object>> find(String pk) throws MwaInstanceError
	{

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> resultList = null;
		try {
			conn = this.getDataSource().getConnection();

			final String sql = "SELECT * FROM QUARTZ.EXTENSION_QRTZ_RESULT_TRIGGERS WHERE SCHED_ID = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, pk);

			resultSet = pstmt.executeQuery();

			// resultSetからArrayList<String,Object>に変換
			ResultSetMapper resultSetMapper = new ResultSetMapper();
			resultList = resultSetMapper.resultSet2ArrayList(resultSet);

			resultSet.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			logger.error("Unexpected DB error.", e);
			throw new MwaInstanceError(MWARC.DATABASE_ERROR);
		} finally {
			close(conn, pstmt, resultSet);
		}

		return resultList;

	}

	/**
	 * QUARTZ.EXTENSION_QRTZ_RESULT_TRIGGERS　テーブル情報の登録
	 * 
	 * @param seq
	 * @param updatedTime
	 * @return
	 */
	public int insert(ExtensionQrtzResultTriggers entity) throws MwaInstanceError
	{

		Connection conn = null;
		PreparedStatement pstmt = null;
		int num = 0;
		try {
			conn = this.getDataSource().getConnection();

			final String sql = "INSERT INTO QUARTZ.EXTENSION_QRTZ_RESULT_TRIGGERS ("
					+ "id"
					+ ",sched_id"
					+ ",trigger_job_status"
					+ ",result"
					+ ",created_time) values (?,?,?,?,?);";

			pstmt = conn.prepareStatement(sql);
			// Autoコミットしない。
			conn.setAutoCommit(false);
			pstmt.setString(1, UUID.randomUUID().toString());
			pstmt.setString(2, entity.getSched_id());
			pstmt.setString(3, entity.getTrigger_job_status());
			pstmt.setString(4, entity.getResult());

			// 登録タイムスタンプのセット
			final String update = SchedulerCommon.getTimeStampStr();
			pstmt.setString(5, update);

			// DB挿入
			num = pstmt.executeUpdate();

			if (num == 0) {
				logger.error("Insert failed.sql ->" + sql);
				throw new MwaInstanceError(MWARC.DATABASE_ERROR);
			}

			// コミット
			conn.commit();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Unexpected DB error.", e);
			throw new MwaInstanceError(MWARC.DATABASE_ERROR);

		} finally {
			close(conn, pstmt);
		}

		return num;

	}

}
