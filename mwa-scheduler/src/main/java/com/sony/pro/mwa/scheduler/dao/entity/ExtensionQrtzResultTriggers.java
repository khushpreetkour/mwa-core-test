package com.sony.pro.mwa.scheduler.dao.entity;

public class ExtensionQrtzResultTriggers {
	
	private String id;
	
	private String sched_id;
	
	private String trigger_job_status;
	
	private String result;
	
	private String created_time;

	public String getSched_id()
	{
		return sched_id;
	}

	
	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setSched_id(String sched_id)
	{
		this.sched_id = sched_id;
	}

	public String getTrigger_job_status()
	{
		return trigger_job_status;
	}

	public void setTrigger_job_status(String trigger_job_status)
	{
		this.trigger_job_status = trigger_job_status;
	}

	public String getResult()
	{
		return result;
	}

	public void setResult(String result)
	{
		this.result = result;
	}

	public String getCreated_time()
	{
		return created_time;
	}

	public void setCreated_time(String created_time)
	{
		this.created_time = created_time;
	}

	
	
}
