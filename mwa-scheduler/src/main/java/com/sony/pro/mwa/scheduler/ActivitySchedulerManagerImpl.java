package com.sony.pro.mwa.scheduler;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.quartz.CronExpression;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Matcher;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.matchers.KeyMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoModel;
import com.sony.pro.mwa.model.scheduler.CronTriggerInputModel;
import com.sony.pro.mwa.model.scheduler.SelectTriggerInfoInputModel;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInfoModel;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInputModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.common.SchedulerCommon;
import com.sony.pro.mwa.scheduler.dao.ExtensionQrtzResultTriggersFacade;
import com.sony.pro.mwa.scheduler.dao.ExtensionQrtzSequenceFacade;
import com.sony.pro.mwa.scheduler.dao.ExtensionQrtzTriggersFacade;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzSequence;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzTriggers;
import com.sony.pro.mwa.scheduler.dao.entity.dto.CronTriggersInfoDto;
import com.sony.pro.mwa.scheduler.dao.entity.dto.SImpleTriggersInfoDto;
import com.sony.pro.mwa.scheduler.enm.HttpMethodType;
import com.sony.pro.mwa.scheduler.enm.TriggerType;
import com.sony.pro.mwa.scheduler.job.listener.PollingJobListener;
import com.sony.pro.mwa.scheduler.job.listener.WatchFolderSchedulerJobListener;
import com.sony.pro.mwa.scheduler.trigger.RegistCronTrigger;
import com.sony.pro.mwa.scheduler.trigger.RegistSimpleTrigger;
import com.sony.pro.mwa.scheduler.trigger.bean.CronTriggerInputBean;
import com.sony.pro.mwa.scheduler.trigger.bean.SimpleTriggerInputBean;
import com.sony.pro.mwa.scheduler.trigger.bean.TriggerInputBean;
import com.sony.pro.mwa.service.scheduler.ISchedulerManager;

@Component
public class ActivitySchedulerManagerImpl implements ISchedulerManager, ISchedulerConstants {

	private final static Object lockObj = new Object();
	private final static MwaLogger logger = MwaLogger.getLogger(ActivitySchedulerManagerImpl.class);
	private final static String CHARACTER_CODE = "UTF-8";
	private final static int DB_BYTE_DESCRIPTION = 256;
	private JobListener paramJobListener;

	@Autowired
	private org.quartz.Scheduler scheduler;

	private ExtensionQrtzSequenceFacade extensionQrtzSequenceFacade;
	private ExtensionQrtzResultTriggersFacade extensionQrtzResultTriggersFacade;
	private ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade;

	@Autowired
	@Qualifier("scheduler")
	public void setScheduler(org.quartz.Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public void setExtensionQrtzSequenceFacade(ExtensionQrtzSequenceFacade extensionQrtzSequenceFacade) {
		this.extensionQrtzSequenceFacade = extensionQrtzSequenceFacade;
	}

	public void setExtensionQrtzResultTriggersFacade(ExtensionQrtzResultTriggersFacade extensionQrtzResultTriggersFacade) {
		this.extensionQrtzResultTriggersFacade = extensionQrtzResultTriggersFacade;
	}

	public void setExtensionQrtzTriggersFacade(ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade) {
		this.extensionQrtzTriggersFacade = extensionQrtzTriggersFacade;
	}

	public void initializeH2()
	{
		startScheduler();
	}

	public void initialize()
	{
		startScheduler();
		ActivitySchedulerFactory.setQuartzConfigFileName("quartz.properties");
	}

	public void destroy()
	{
		shutdownScheduler();
	}

	@Override
	public KeyValueModel startScheduler()
	{
		try {
			{
				List<Matcher<JobKey>> matchers = new ArrayList<Matcher<JobKey>>();
				// enumerate each job group
				for (String group : scheduler.getJobGroupNames()) {
					// enumerate each job in group
					for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group))) {
						matchers.add(KeyMatcher.keyEquals(jobKey));
					}
				}
				scheduler.getListenerManager().addJobListener(new PollingJobListener(this.extensionQrtzResultTriggersFacade), matchers);
			}

			logger.info("scheduler start!");
			scheduler.start();
			return new KeyValueModel(STATUS_KEY, START_SUCCESS);

		} catch (SchedulerException e) {
			logger.error("Scheduler-start failed.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

	}

	@Override
	public KeyValueModel shutdownScheduler()
	{

		try {
			// schedulerがshutdownの場合、処理終了
			if (scheduler.isShutdown()) {
				logger.info("scheduler is shutdown!");
				return new KeyValueModel(STATUS_KEY, SHUTDOWN_SUCCESS);
			}
			logger.info("scheduler shutdown!!");

			scheduler.shutdown(true);
			return new KeyValueModel(STATUS_KEY, SHUTDOWN_SUCCESS);

		} catch (SchedulerException e) {
			logger.error("Scheduler-shutdown failed.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

	}

	/**
	 * CronTriggerを登録する。
	 */
	@Override
	public KeyValueModel registCronScheduleJob(CronTriggerInputModel inputModel)
	{
		// 入力チェック
		checkCronInput(inputModel);

		// Input情報Beanの生成
		CronTriggerInputBean inputBean = createCronTriggerInputBean(inputModel);

		// CronTrigger登録
		addCronTrigger(scheduler, inputBean);

		// TriggerTypeを設定
		inputBean.setTriggerType(TRIGGER_TYPE_CRON);

		// EXTENSION_QRTZ_TRIGGERS　テーブル挿入用のエンティティオブジェクトを生成
		ExtensionQrtzTriggers insertEntity = createUpdateExtensionQrtzTriggersEntity(inputBean);

		// EXTENSION_QRTZ_TRIGGERSテーブルにTrigger情報を追加する。
		insertExtensionQrtzTriggers(insertEntity);

		try {
			scheduler.start();
		} catch (SchedulerException e) {
			logger.error("", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

		return new KeyValueModel(SCHED_ID_KEY, inputBean.getSchedId());

	}

	/**
	 * SimpleTriggerのスケジュールを登録します。
	 */
	@Override
	public KeyValueModel registSimpleScheduleJob(SimpleTriggerInputModel inputModel)
	{

		this.paramJobListener = new PollingJobListener(this.extensionQrtzResultTriggersFacade);
		return registSimpleScheduleJob(inputModel, false);
	}

	/**
	 * SimpleTriggerのスケジュールを登録します。starttimeが過去日の場合、即時発火します。
	 */
	@Override
	public KeyValueModel registSimpleScheduleJobStartTimeIsPastTimeSuccess(SimpleTriggerInputModel inputModel)
	{

		this.paramJobListener = new WatchFolderSchedulerJobListener(this.extensionQrtzResultTriggersFacade);
		return registSimpleScheduleJob(inputModel, true);
	}


	/**
	 * SimpleTriggerのスケジュールを登録します。
	 *
	 * @param inputModel
	 * @param pastTimeSuccessFlg
	 *            true 過去日の場合、即時発火します。false 過去日の場合、error
	 * @return
	 */
	protected KeyValueModel registSimpleScheduleJob(SimpleTriggerInputModel inputModel, boolean pastTimeSuccessFlg)
	{

		// 入力チェック
		checkSimpleInput(inputModel);

		// Input情報Beanの生成
		SimpleTriggerInputBean inputBean = createSimpleTriggerInputBean(inputModel);

		// 過去日の場合、ハンドリングを設定
		inputBean.setPastTimeSuccessFlg(pastTimeSuccessFlg);

		// SimpleTrigger登録
		addSimpleTrigger(scheduler, inputBean);

		// TriggerTypeを設定
		inputBean.setTriggerType(TRIGGER_TYPE_SIMPLE);

		// EXTENSION_QRTZ_TRIGGERS　テーブル挿入用のエンティティオブジェクトを生成
		ExtensionQrtzTriggers insertEntity = createUpdateExtensionQrtzTriggersEntity(inputBean);

		// EXTENSION_QRTZ_TRIGGERSテーブルにTrigger情報を追加する。
		insertExtensionQrtzTriggers(insertEntity);

		try {
			scheduler.start();
		} catch (SchedulerException e) {
			logger.error("", e);
		}

		return new KeyValueModel(SCHED_ID_KEY, inputBean.getSchedId());

	}

	@Override
	public KeyValueModel deleteSimpleTrigger(String schedId)
	{
		// deleteの場合はidがnullならエラー
		if (StringUtils.isEmpty(schedId)) {
			logger.error("SchedId is null.");
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		// Trigger情報の取得
		ExtensionQrtzTriggers entity = getExtensionQrtzTriggers(schedId);

		// entityがsimpleであることをチェック
		validTriggerType(TriggerType.SIMPLE, entity, schedId);

		return deleteTrigger(entity);
	}

	@Override
	public KeyValueModel deleteCronTrigger(String schedId)
	{
		// deleteの場合はidがnullならエラー
		if (StringUtils.isEmpty(schedId)) {
			logger.error("SchedId is null.");
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		// Trigger情報の取得
		ExtensionQrtzTriggers entity = getExtensionQrtzTriggers(schedId);

		// entityがcronであることをチェック
		validTriggerType(TriggerType.CRON, entity, schedId);

		return deleteTrigger(entity);
	}

	private KeyValueModel deleteTrigger(ExtensionQrtzTriggers entity)
	{

		// Trigger情報の削除
		TriggerKey key = getTriggerKey(entity);

		try {
			scheduler.unscheduleJob(key);
		} catch (SchedulerException e) {
			logger.error("Delete-SchedulerJob failed.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

		logger.info("TriggerJob was deleted. triggerKey [" + key + "]");
		return new KeyValueModel(STATUS_KEY, DELETE_SUCCESS);
	}

	@Override
	public KeyValueModel deleteSimpleTrigger()
	{

		//ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade = new ExtensionQrtzTriggersFacade();
		ExtensionQrtzTriggers entity = new ExtensionQrtzTriggers();
		// 全SimpleTriggerの情報取得
		List<SImpleTriggersInfoDto> simpleDtoList = extensionQrtzTriggersFacade.selectSimpleInfoListFind(entity);

		// 一斉削除に必要なTriggerKeyのList作成
		List<TriggerKey> triggerKeyList = new ArrayList<TriggerKey>();
		for (SImpleTriggersInfoDto stid : simpleDtoList) {
			triggerKeyList.add(new TriggerKey(stid.getTrigger_name(), stid.getTrigger_group()));
		}

		return deleteTriggers(triggerKeyList);
	}

	@Override
	public KeyValueModel deleteCronTrigger()
	{

		//ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade = new ExtensionQrtzTriggersFacade();
		ExtensionQrtzTriggers entity = new ExtensionQrtzTriggers();
		// 全CronTriggerの情報取得
		List<CronTriggersInfoDto> cronDtoList = extensionQrtzTriggersFacade.selectCronInfoListFind(entity);

		// 一斉削除に必要なTriggerKeyのList作成
		List<TriggerKey> triggerKeyList = new ArrayList<TriggerKey>();
		for (CronTriggersInfoDto ctid : cronDtoList) {
			triggerKeyList.add(new TriggerKey(ctid.getTrigger_name(), ctid.getTrigger_group()));
		}

		return deleteTriggers(triggerKeyList);
	}

	private KeyValueModel deleteTriggers(List<TriggerKey> triggerKeyList)
	{
		try {
			// 削除処理
			scheduler.unscheduleJobs(triggerKeyList);
		} catch (SchedulerException e) {
			logger.error("Delete all cronTrigger faild.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

		return new KeyValueModel(STATUS_KEY, DELETE_SUCCESS);
	}

	@Override
	public KeyValueModel pauseSimpleTrigger(String schedId)
	{

		ExtensionQrtzTriggers entity = getExtensionQrtzTriggers(schedId);

		// entityがsimpleであることをチェック
		validTriggerType(TriggerType.SIMPLE, entity, schedId);

		return pauseTrigger(entity);
	}

	@Override
	public KeyValueModel pauseCronTrigger(String schedId)
	{

		ExtensionQrtzTriggers entity = getExtensionQrtzTriggers(schedId);

		// entityがcronであることをチェック
		validTriggerType(TriggerType.CRON, entity, schedId);

		return pauseTrigger(entity);
	}

	private KeyValueModel pauseTrigger(ExtensionQrtzTriggers entity)
	{
		// triggerKey生成
		TriggerKey key = getTriggerKey(entity);

		try {
			scheduler.pauseTrigger(key);
		} catch (SchedulerException e) {
			logger.error("Pause-SchedulerJob failed.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

		logger.info("TriggerJob was paused. triggerKey[" + key + "]");
		return new KeyValueModel(STATUS_KEY, PAUSE_SUCCESS);
	}

	@Override
	public KeyValueModel resumeSimpleTrigger(String schedId)
	{

		ExtensionQrtzTriggers entity = getExtensionQrtzTriggers(schedId);

		// entityがcronであることをチェック
		validTriggerType(TriggerType.SIMPLE, entity, schedId);

		return resumeTrigger(entity);
	}

	@Override
	public KeyValueModel resumeCronTrigger(String schedId)
	{
		ExtensionQrtzTriggers entity = getExtensionQrtzTriggers(schedId);

		// entityがcronであることをチェック
		validTriggerType(TriggerType.CRON, entity, schedId);

		return resumeTrigger(entity);
	}


	/**
	 * スケジューラ終了を監視するリスナー
	 * @throws MwaInstanceError
	 */
	@Override
	public void schedulerJobCompletedListener() throws MwaInstanceError{


		if(this.paramJobListener instanceof WatchFolderSchedulerJobListener){
			WatchFolderSchedulerJobListener jobListener = (WatchFolderSchedulerJobListener) this.paramJobListener;

			while (true) {

				//Jobが処理中か判定
				if (jobListener.getContext() == null && jobListener.getJobException() == null) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						//発生しないはず
						e.printStackTrace();
					}
					continue;
				}

				//エラー発生でJob終了したか判定
				if (jobListener.getJobException() != null) {

					MwaInstanceError mwaInstanceError = (MwaInstanceError) jobListener.getJobException().getCause().getCause();
					throw mwaInstanceError;
				}
				break;
			}

		}else if(this.paramJobListener instanceof PollingJobListener){
			PollingJobListener jobListener = (PollingJobListener) this.paramJobListener;

			while (true) {

				//Jobが処理中か判定
				if (jobListener.getContext() == null && jobListener.getJobException() == null) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						//発生しないはず
						e.printStackTrace();
					}
					continue;
				}

				//エラー発生でJob終了したか判定
				if (jobListener.getJobException() != null) {

					MwaInstanceError mwaInstanceError = (MwaInstanceError) jobListener.getJobException().getCause().getCause();
					throw mwaInstanceError;
				}
				break;
			}
		}

	}

	private KeyValueModel resumeTrigger(ExtensionQrtzTriggers entity)
	{
		// triggerKey生成
		TriggerKey key = getTriggerKey(entity);

		try {
			scheduler.resumeTrigger(key);
		} catch (SchedulerException e) {
			logger.error("Resume-SchedulerJob failed.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

		logger.info("TriggerJob was resumed. triggerKey[" + key + "]");
		return new KeyValueModel(STATUS_KEY, RESUME_SUCCESS);
	}

	private ExtensionQrtzTriggers getExtensionQrtzTriggers(String schedId)
	{
		// Trigger情報の取得
		//ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade = new ExtensionQrtzTriggersFacade();
		return extensionQrtzTriggersFacade.find(schedId);
	}

	private TriggerKey getTriggerKey(ExtensionQrtzTriggers entity)
	{
		// TriggerKey生成
		return TriggerKey.triggerKey(entity.getTrigger_name(), entity.getTrigger_group());
	}

	/**
	 * CronTrigger情報の検索
	 */
	@Override
	public CronTriggerInfoCollection selectCronTriggerInfo(SelectTriggerInfoInputModel inputModel)
	{

		// 検索条件のセット
		ExtensionQrtzTriggers entity = new ExtensionQrtzTriggers();
		entity.setSched_id(inputModel.getSchedId());
		// entity.setTag(inputModel.getTag());

		// CronTrigger情報の取得
		//ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade = new ExtensionQrtzTriggersFacade();
		List<CronTriggersInfoDto> list = extensionQrtzTriggersFacade.selectCronInfoListFind(entity);

		List<CronTriggerInfoModel> resultList = new ArrayList<CronTriggerInfoModel>();
		// 検索結果のセット
		for (CronTriggersInfoDto dto : list) {
			CronTriggerInfoModel resultModel = new CronTriggerInfoModel();

			try {
				// dtoのendpoint,parameter,description をresultModelに移送
				BeanUtils.copyProperties(resultModel, dto);
			} catch (IllegalAccessException e) {
				// 発生しないはず
				logger.error("", e);
			} catch (InvocationTargetException e) {
				// 発生しないはず
				logger.error("", e);
			}

	        String pattern = "yyyy-MM-dd HH:mm:ss.SSSX";
	        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

			// 他、検索結果をセット
			try {
				resultModel.setSchedId(dto.getSched_id());
				resultModel.setSchedName(dto.getSched_name());
				resultModel.setTriggerName(dto.getTrigger_name());
				resultModel.setTriggerGroup(dto.getTrigger_group());
				resultModel.setTriggerType(dto.getTrigger_type());
				resultModel.setHttp_methodType(dto.getHttp_method_type());
				resultModel.setCreatedTime(sdf.parse(dto.getCreated_time()).getTime());
				resultModel.setUpdatedTime(sdf.parse(dto.getUpdated_time()).getTime());
				resultModel.setTimeZone(dto.getTime_zone());
				resultModel.setTriggerState(dto.getTrigger_state());
				resultModel.setStartTime(Long.parseLong(dto.getStart_time()));
				resultModel.setNextFireTime(Long.parseLong(dto.getNext_fire_time()));
				resultModel.setCronExpression(dto.getCron_expression());
				resultModel.setEndTime((0 == Long.parseLong(dto.getEnd_time())) ? null : Long.parseLong(dto.getEnd_time()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        resultList.add(resultModel);
		}

		// 検索結果の返却
		return new CronTriggerInfoCollection(resultList);
	}

	/**
	 * SimpleTrigger情報の検索
	 */
	@Override
	public SimpleTriggerInfoCollection selectSimpleTriggerInfo(SelectTriggerInfoInputModel inputModel)
	{

		// 検索条件のセット
		ExtensionQrtzTriggers entity = new ExtensionQrtzTriggers();
		entity.setSched_id(inputModel.getSchedId());
		// entity.setTag(inputModel.getTag());

		// SimpleTrigger情報の取得
		//ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade = new ExtensionQrtzTriggersFacade();
		List<SImpleTriggersInfoDto> list = extensionQrtzTriggersFacade.selectSimpleInfoListFind(entity);

		List<SimpleTriggerInfoModel> resultList = new ArrayList<SimpleTriggerInfoModel>();
		// 検索結果のセット
		for (SImpleTriggersInfoDto dto : list) {
			SimpleTriggerInfoModel resultModel = new SimpleTriggerInfoModel();

			try {
				// dtoのendpoint,parameter,description をresultModelに移送
				BeanUtils.copyProperties(resultModel, dto);
			} catch (IllegalAccessException e) {
				// 発生しないはず
				logger.error("", e);
			} catch (InvocationTargetException e) {
				// 発生しないはず
				logger.error("", e);
			}

	        String pattern = "yyyy-MM-dd HH:mm:ss.SSSX";
	        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

	        // 他、検索結果をセット
			try {
				resultModel.setSchedId(dto.getSched_id());
				resultModel.setSchedName(dto.getSched_name());
				resultModel.setTriggerName(dto.getTrigger_name());
				resultModel.setTriggerGroup(dto.getTrigger_group());
				resultModel.setTriggerType(dto.getTrigger_type());
				resultModel.setHttp_methodType(dto.getHttp_method_type());
				resultModel.setCreatedTime(sdf.parse(dto.getCreated_time()).getTime());
				resultModel.setUpdatedTime(sdf.parse(dto.getUpdated_time()).getTime());
				resultModel.setRepeatCount(dto.getRepeat_count());
				resultModel.setRepeatInterval(dto.getRepeat_interval());
				resultModel.setTriggerState(dto.getTrigger_state());
				resultModel.setStartTime(sdf.parse(dto.getStart_time()).getTime());
				resultModel.setNextFireTime(Long.parseLong(dto.getNext_fire_time()));
				resultModel.setEndTime((0 == Long.parseLong(dto.getEnd_time())) ? null : Long.parseLong(dto.getEnd_time()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			resultList.add(resultModel);
		}

		// 検索結果の返却
		return new SimpleTriggerInfoCollection(resultList);
	}

	/**
	 * SimpleTrigger登録用のInput情報Beanを生成する。
	 *
	 * @param inputModel
	 * @return SimpleTrigger登録用のInput情報Bean
	 */
	protected SimpleTriggerInputBean createSimpleTriggerInputBean(SimpleTriggerInputModel inputModel)
	{

		// Input情報Beanの生成
		SimpleTriggerInputBean inputBean = new SimpleTriggerInputBean();

		try {
			// inputModelからinputBeanへ移送
			BeanUtils.copyProperties(inputBean, inputModel);
		} catch (IllegalAccessException e) {
			// 発生しないはず。
			logger.error("", e);
		} catch (InvocationTargetException e) {
			// 発生しないはず。
			logger.error("", e);
		}

		// sequence生成 seqは必ず値が入る。nullの場合、下回りの処理で例外を発行している。
		String seqStr = createTriggerSequence();

		// UUIDの生成・設定
		inputBean.setSchedId(UUID.randomUUID().toString());

		// JobName,JobGroup,TriggerName,TriggerGroup生成
		inputBean.setJobName(String.format(JOB_NAME_FORMAT, seqStr));
		inputBean.setJobGroup(String.format(JOB_GROUP_FORMAT, seqStr));
		inputBean.setTriggerName(String.format(TRIGGER_NAME_FORMAT, seqStr));
		inputBean.setTriggerGroup(String.format(TRIGGER_GROUP_FORMAT, seqStr));

		// TimeZoneの設定
		if (inputBean.getTimeZone() == null) {
			inputBean.setTimeZone(TimeZone.getDefault().getID());
		}

		return inputBean;
	}

	/**
	 * SimpleTriggerを登録する。
	 *
	 * @param scheduler
	 * @param inputBean
	 */
	protected void addSimpleTrigger(Scheduler scheduler, SimpleTriggerInputBean inputBean)
	{

		RegistSimpleTrigger registSimpleTrigger = new RegistSimpleTrigger();

		try {
			registSimpleTrigger.scheduleJob(scheduler, inputBean,this.paramJobListener);
		} catch (SchedulerException e) {
			logger.error("Regist-SimpleTrigger failed.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}

	}

	/**
	 * CronTrigger登録用のInput情報Beanを生成する。
	 *
	 * @param inputModel
	 * @return CronTrigger登録用のInput情報Bean
	 */
	protected CronTriggerInputBean createCronTriggerInputBean(CronTriggerInputModel inputModel)
	{

		// Input情報Beanの生成
		CronTriggerInputBean inputBean = new CronTriggerInputBean();

		try {
			// inputModelからinputBeanへ移送
			BeanUtils.copyProperties(inputBean, inputModel);
		} catch (IllegalAccessException e) {
			// 発生しないはず。
			logger.error("", e);
		} catch (InvocationTargetException e) {
			// 発生しないはず。
			logger.error("", e);
		}

		// sequence生成 seqは必ず値が入る。nullの場合、下回りの処理で例外を発行している。
		String seqStr = createTriggerSequence();

		// UUIDの生成・設定
		inputBean.setSchedId(UUID.randomUUID().toString());

		// JobName,JobGroup,TriggerName,TriggerGroup生成
		inputBean.setJobName(String.format(JOB_NAME_FORMAT, seqStr));
		inputBean.setJobGroup(String.format(JOB_GROUP_FORMAT, seqStr));
		inputBean.setTriggerName(String.format(TRIGGER_NAME_FORMAT, seqStr));
		inputBean.setTriggerGroup(String.format(TRIGGER_GROUP_FORMAT, seqStr));

		// TimeZoneの設定
		if (inputBean.getTimeZone() == null) {
			inputBean.setTimeZone(TimeZone.getDefault().getID());
		}

		return inputBean;
	}

	/**
	 * CronTriggerを登録する。
	 *
	 * @param scheduler
	 * @param inputBean
	 */
	protected void addCronTrigger(Scheduler scheduler, CronTriggerInputBean inputBean)
	{

		RegistCronTrigger registCronTrigger = new RegistCronTrigger(this.extensionQrtzResultTriggersFacade);

		try {
			registCronTrigger.scheduleJob(scheduler, inputBean);
		} catch (MwaInstanceError e) {
			logger.error("Regist-CronTrigger failed.", e);
			throw e;
		} catch (SchedulerException e) {
			logger.error("Regist-CronTrigger failed.", e);
			throw new MwaInstanceError(MWARC.INVALID_INPUT_SCHEDULER);
		}

	}

	/**
	 * EXTENSION_QRTZ_TRIGGERS　テーブル挿入用エンティティの値をセット
	 *
	 * @param inputModel
	 * @param inputBean
	 * @return EXTENSION_QRTZ_TRIGGERS　テーブル挿入用エンティティ
	 */
	protected ExtensionQrtzTriggers createUpdateExtensionQrtzTriggersEntity(TriggerInputBean inputBean)
	{

		// EXTENSION_QRTZ_TRIGGERS　テーブル挿入用のエンティティオブジェクトを生成
		ExtensionQrtzTriggers entity = new ExtensionQrtzTriggers();

		try {
			// inputModelからextensionQrtzTriggersへ移送
			// httpMethodType,endpoint,parameter,descriptionが移送される。
			BeanUtils.copyProperties(entity, inputBean);
		} catch (IllegalAccessException e) {
			// 発生しないはず。
			logger.error("", e);
		} catch (InvocationTargetException e) {
			// 発生しないはず。
			logger.error("", e);
		}

		// HttpMethodType,SchedId,TriggerName,TriggerGroup,Trigger_Typeのセット
		entity.setHttp_method_type(inputBean.getHttpMethodType());
		entity.setSched_id(inputBean.getSchedId());
		entity.setTrigger_name(inputBean.getTriggerName());
		entity.setTrigger_group(inputBean.getTriggerGroup());
		entity.setTrigger_type(inputBean.getTriggerType());

		// SCHED_NAMEの取得
		final String schedName = SchedulerCommon.getQuartzProp(QUARTS_PROP_SCHED_NAME);

		// SCHED_NAMEのセット
		entity.setSched_name(schedName);

		return entity;
	}

	/**
	 * EXTENSION_QRTZ_TRIGGERSテーブルにTrigger情報を追加する。
	 */
	protected void insertExtensionQrtzTriggers(ExtensionQrtzTriggers entity)
	{

		// EXTENSION_QRTZ_TRIGGERSテーブル　登録処理
		//ExtensionQrtzTriggersFacade extensionQrtzTriggersFacade = new ExtensionQrtzTriggersFacade();
		extensionQrtzTriggersFacade.insert(entity);

	}

	/**
	 * JobName,Group,TriggerName,Groupに命名に必要なsequenceNoを生成取得する。
	 *
	 * @return
	 */
	protected String createTriggerSequence()
	{

		String newSeq = null;

		synchronized (lockObj){
			// 現在のシーケンスを取得
			//ExtensionQrtzSequenceFacade extensionQrtzSequenceFacade = new ExtensionQrtzSequenceFacade();
			ExtensionQrtzSequence entity = null;
			try {
				entity = this.extensionQrtzSequenceFacade.find(TRIGGER_SEQ);

			} catch (MwaInstanceError e) {
				throw e;
			}

			Long seq = Long.parseLong(entity.getSeq_no());
			// sequence
			newSeq = Long.toString(++seq);
			entity.setSeq_no(newSeq);

			try {
				// シーケンスの生成・更新
				extensionQrtzSequenceFacade.update(entity);
			} catch (MwaInstanceError e) {
				throw e;
			}

		}
		return newSeq;
	}

	/**
	 * entityが予期したTriggerTypeかを調べます。異なる場合は例外（INVALID_INPUT）をスローします。
	 *
	 * @param triggerType
	 * @param entity
	 * @param schedId
	 */
	private void validTriggerType(TriggerType triggerType, ExtensionQrtzTriggers entity, String schedId)
	{
		// entityが空でないことをチェックをチェック
		if (null == entity) {
			logger.error("There is no Trigger at schedId=" + schedId);
			throw new MwaError(MWARC.INVALID_INPUT);
		}

		// 予期するTriggerTypeと取得した情報のTriggerTypeが一致するか
		if (!triggerType.name().equals(entity.getTrigger_type())) {
			if (triggerType == TriggerType.CRON) {
				// Cronを予期していた時のエラーメッセージ
				logger.error("There is no CronTrigger at scheID=" + schedId);
			} else {
				// Simpleを予期していた時のエラーメッセージ
				logger.error("There is no SimpleTrigger at scheID=" + schedId);
			}
			throw new MwaError(MWARC.INVALID_INPUT);
		}
	}

	/**
	 * CronTriggerInputModelの内容をチェックします。
	 * チェック項目：httpMethodType,endpoint,description,timeZone,cronExpression
	 *
	 * @param cronModel
	 */
	private void checkCronInput(CronTriggerInputModel cronModel)
	{

		checkTriggerInput(cronModel.getHttpMethodType(), cronModel.getEndpoint(), cronModel.getDescription());

		String timeZone = cronModel.getTimeZone();
		String cronExpression = cronModel.getCronExpression();

		if (timeZone != null) {
			if (!"GMT".equals(timeZone)) {
				// タイムゾーン取得
				TimeZone tz = TimeZone.getTimeZone(timeZone);

				// getTimeZoneで取得したタイムゾーンがGMTだったら存在しないTimeZoneIdなのでエラー
				if (tz.getID().equals("GMT")) {
					logger.error("timeZone is invalid format. value = " + timeZone);
					throw new MwaInstanceError(MWARC.INVALID_INPUT);
				}
			}
		}

		if (cronExpression == null || !CronExpression.isValidExpression(cronExpression)) {
			String message = "CronExpression is null or invalid format. value = " + cronExpression;
			try {
				CronExpression.validateExpression(cronExpression);
			} catch (ParseException e) {
				message += ", reason=" + e.getMessage();
			}
			logger.error(message);
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, message);
		}
	}

	/**
	 * SimpleTriggerInputModelの内容をチェックします。
	 * チェック項目:httpMethodType,endpoint,description,repeatCount,repeatInterval
	 *
	 * @param simpleModel
	 */
	private void checkSimpleInput(SimpleTriggerInputModel simpleModel)
	{

		checkTriggerInput(simpleModel.getHttpMethodType(), simpleModel.getEndpoint(), simpleModel.getDescription());

		String repeatCount = simpleModel.getRepeatCount();
		String repeatInterval = simpleModel.getRepeatIntervalSec();

		// repeatCountの入力チェック
		if (null != repeatCount) {
			try {
				Integer.parseInt(repeatCount);
			} catch (NumberFormatException e) {
				logger.error("repeatCount is invalid format. value = \"" + repeatCount + "\"", e);
				throw new MwaInstanceError(MWARC.INVALID_INPUT);
			}
		}

		// repeatIntervalの入力チェック
		if (null != repeatInterval) {
			try {
				Integer.parseInt(repeatInterval);
			} catch (NumberFormatException e) {
				logger.error("repeatInterval is invalid format. value = \"" + repeatInterval + "\"", e);
				throw new MwaInstanceError(MWARC.INVALID_INPUT);
			}
		}

		// repeatCountが0以外のとき、repeatIntervalがnullまたは0の場合エラー
		if ((null != repeatCount && 0 != Integer.parseInt(repeatCount)) && (null == repeatInterval || 0 == Integer.parseInt(repeatInterval))) {
			logger.error("Please set other than zero in repeatInterval if repeatCount is zero.");
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}

	}

	/**
	 * TriggerInputに含まれるCron、Simple共通のvalueをチェックします。サポートしない値なら例外（INVALID_INPUT）
	 * をスローします。
	 *
	 * @param httpMethodType
	 * @param endpoint
	 * @param description
	 */
	private void checkTriggerInput(String httpMethodType, String endpoint, String description)
	{
		// nulll、空文字、またはHttpMethodTypeに存在しなければエラー
		if (StringUtils.isEmpty(httpMethodType)) {
			logger.error("httpMethodType is null or EMPTY. ");
			throw new MwaInstanceError(MWARC.INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND);
		} else if (!containsHttpMethodType(httpMethodType)) {
			logger.error("httpMethodType is null or unsupported type. value = " + httpMethodType);
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}

		// null又は空文字の場合はエラー
		if (StringUtils.isEmpty(endpoint)) {
			logger.error("endpoint is null or EMPTY");
			throw new MwaInstanceError(MWARC.INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND);
		}

		// DBのバイト数(256byte)を超える場合はエラー
		if (description != null && !isSmallerStringBytes(description, DB_BYTE_DESCRIPTION)) {
			logger.error("description is over in DB varying. value = " + description);
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}

	}

	/**
	 * targetがHttpMethodTypeに含まれているか調査します。
	 *
	 * @param target
	 * @return 含まれている：true 含まれていない：false
	 */
	private boolean containsHttpMethodType(String target)
	{
		for (HttpMethodType methodType : HttpMethodType.values()) {
			if (methodType.name().equals(target.toUpperCase())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * targetのバイト数がbytesより小さい場合にtrueを返します。
	 *
	 * @param target
	 * @param bytes
	 *            比較するバイト数
	 * @return
	 */
	private boolean isSmallerStringBytes(String target, long bytes)
	{
		try {
			if (target.getBytes(CHARACTER_CODE).length <= bytes) {
				return true;
			}
		} catch (UnsupportedEncodingException e) {
			// 発生しないはず
			logger.error("", e);
		}

		return false;
	}

}