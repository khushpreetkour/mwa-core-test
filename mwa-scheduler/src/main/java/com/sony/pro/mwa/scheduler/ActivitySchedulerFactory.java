package com.sony.pro.mwa.scheduler;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;

/**
 * Quartz　Schedulerオブジェクトをシングルトンで取得するファクトリークラス
 * @author 9004033809
 *
 */
public class ActivitySchedulerFactory implements ISchedulerConstants {

	/**
	 * initialize処理でQuartzプロパティファイルを指定
	 */
	private static String quartzConfigFileName;
	

	private ActivitySchedulerFactory() {
		
	}
	
	/**
	 * 
	 * @return　Quartz　Schedulerオブジェクト
	 * @throws SchedulerException
	 */
	public static Scheduler getInstance() throws SchedulerException{

		if(ActivitySchedulerFactory.quartzConfigFileName == null){
			ActivitySchedulerFactory.quartzConfigFileName = H2_QUARTS_PROPS;
		}

		return new StdSchedulerFactory(ActivitySchedulerFactory.quartzConfigFileName).getScheduler();
	}


	public static void setQuartzConfigFileName(final String quartzConfigFileName)
	{
		ActivitySchedulerFactory.quartzConfigFileName = quartzConfigFileName;
	}
	
	public static String getQuartzConfigFileName()
	{
		return quartzConfigFileName;
	}
	

}
