package com.sony.pro.mwa.scheduler.dao.entity.dto;

import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzTriggers;

public class CronTriggersInfoDto extends ExtensionQrtzTriggers{
	
	private String cron_expression;
	
	private String time_zone;
	
	private String trigger_state;
	
	private String start_time;
	
	private String next_fire_time;

	public String getCron_expression()
	{
		return cron_expression;
	}

	public void setCron_expression(String cron_expression)
	{
		this.cron_expression = cron_expression;
	}

	public String getTime_zone()
	{
		return time_zone;
	}

	public void setTime_zone(String time_zone)
	{
		this.time_zone = time_zone;
	}

	public String getTrigger_state()
	{
		return trigger_state;
	}

	public void setTrigger_state(String trigger_state)
	{
		this.trigger_state = trigger_state;
	}

	public String getStart_time()
	{
		return start_time;
	}

	public void setStart_time(String start_time)
	{
		this.start_time = start_time;
	}

	public String getNext_fire_time()
	{
		return next_fire_time;
	}

	public void setNext_fire_time(String next_fire_time)
	{
		this.next_fire_time = next_fire_time;
	}
	
	

}
