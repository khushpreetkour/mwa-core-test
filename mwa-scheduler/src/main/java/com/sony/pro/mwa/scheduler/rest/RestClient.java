package com.sony.pro.mwa.scheduler.rest;

import java.io.IOException;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import com.sony.pro.mwa.common.http.HttpClientWrapper;
import com.sony.pro.mwa.common.http.HttpRequestCSRFTokenProviderImpl;
import com.sony.pro.mwa.common.http.HttpRequestConfigProviderImpl;
import com.sony.pro.mwa.common.http.HttpResponseResult;
import com.sony.pro.mwa.common.http.IHttpClientWrapper;
import com.sony.pro.mwa.common.http.IHttpRequestConfigProvider;
import com.sony.pro.mwa.common.log.ILoggerSourceWithPrefix;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;

public class RestClient implements ILoggerSourceWithPrefix {

	protected MwaLogger logger = MwaLogger.getLogger(this);

	private String instanceId;

	private String responseBody;

	private int statusCode;

	/**
	 * HTTPヘッダの設定
	 */
	private static Header[] headers = { 
		new BasicHeader("Content-type", "application/json; charset=UTF-8"), 
		new BasicHeader("Accept", "application/json"),
		new BasicHeader("Accept-Charset", "UTF-8")
	};
	private static Header[] nvxapiHeaders = { 
			// TODO: スケジューラ自体に Request Header を登録できる必要あり。 汎用的に Authorization / X-XSRF-TOKEN などを設定できる方がベストだが、現状はスケジューラに抱え込む形で修正する。
			// 加えて、前は amsadmin ユーザ固定になっていたが、スケジューラ実行時にも mwa ユーザにしたいとの事なのでそちらに変更した。
			new BasicHeader("Authorization", "Basic bXdhOk13YUFkbTFu"),
			new BasicHeader("Content-type", "application/json; charset=UTF-8"),
			new BasicHeader("Accept", "application/json"), 
			new BasicHeader("Accept-Charset", "UTF-8") 
	};
	
	/*-------------------------------------------------------------------------------------------
	 *  コンストラクタ
	 -------------------------------------------------------------------------------------------*/
	public RestClient(String instanceId) {
		this.instanceId = instanceId;
	}

	public void rest(IRestInput restInput) throws MwaInstanceError {

		switch (restInput.getHttpMethod()) {
		case "POST":
			restPost(restInput);
			break;
		case "GET":
			restGet(restInput);
			break;
		case "PUT":
			restPut(restInput);
			break;
		case "DELETE":
			restDelete(restInput);
			break;
		default:
			break;
		}

	}

	public void restPost(IRestInput restInput) throws MwaInstanceError {

		final String url = restInput.getUrl();
		final String jsonParam = restInput.getJsonParam();

		executePost(url, jsonParam);
	}

	public void restGet(IRestInput restInput) throws MwaInstanceError {

		final String url = restInput.getUrl();
		executeGet(url);
	}

	public void restPut(IRestInput restInput) throws MwaInstanceError {

		final String url = restInput.getUrl();
		final String jsonParam = restInput.getJsonParam();
		executePut(url, jsonParam);
	}

	public void restDelete(IRestInput restInput) throws MwaInstanceError {

		final String url = restInput.getUrl();
		executeDelete(url);
	}

	private void executePost(String url, String jsonParam) throws MwaInstanceError {

		try {
			IHttpRequestConfigProvider provider = null;
			// TODO: スケジューラ自体に Request Header を登録できる必要あり。 汎用的に Authorization / X-XSRF-TOKEN などを設定できる方がベストだが、現状はスケジューラに抱え込む形で修正する。
			if (url.contains("/nvx/api/")) {
				provider = new HttpRequestCSRFTokenProviderImpl(nvxapiHeaders);
			} else {
				provider = new HttpRequestConfigProviderImpl(headers);
			}
			IHttpClientWrapper httpClient = new HttpClientWrapper();
			HttpResponseResult response = httpClient.post(url, jsonParam, provider);
			setStatusCode(response.getStatusCode());
			setResponseBody(response.getResponseBody());
		} catch (Exception e) {
			logger.error("executePost caused exception: " + e.toString(), e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, "", e.getMessage());
		}
	}

	private void executeGet(String url) throws MwaInstanceError {
		try {
			IHttpRequestConfigProvider provider = null;
			// TODO: スケジューラ自体に Request Header を登録できる必要あり。 汎用的に Authorization / X-XSRF-TOKEN などを設定できる方がベストだが、現状はスケジューラに抱え込む形で修正する。
			if (url.contains("/nvx/api/")) {
				provider = new HttpRequestCSRFTokenProviderImpl(nvxapiHeaders);
			} else {
				provider = new HttpRequestConfigProviderImpl(headers);
			}
			IHttpClientWrapper httpClient = new HttpClientWrapper();
			HttpResponseResult response = httpClient.get(url, provider);
			setStatusCode(response.getStatusCode());
			setResponseBody(response.getResponseBody());
		} catch (Exception e) {
			logger.error("executeGet caused exception: " + e.toString(), e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, "", e.getMessage());
		}
	}

	private void executePut(String url, String jsonParam) throws MwaInstanceError {
		try {
			IHttpRequestConfigProvider provider = null;
			// TODO: スケジューラ自体に Request Header を登録できる必要あり。 汎用的に Authorization / X-XSRF-TOKEN などを設定できる方がベストだが、現状はスケジューラに抱え込む形で修正する。
			if (url.contains("/nvx/api/")) {
				provider = new HttpRequestCSRFTokenProviderImpl(nvxapiHeaders);
			} else {
				provider = new HttpRequestConfigProviderImpl(headers);
			}
			IHttpClientWrapper httpClient = new HttpClientWrapper();
			HttpResponseResult response = httpClient.put(url, jsonParam, provider);
			setStatusCode(response.getStatusCode());
			setResponseBody(response.getResponseBody());
		} catch (Exception e) {
			logger.error("executePut caused exception: " + e.toString(), e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, "", e.getMessage());
		}
	}

	private void executeDelete(String url) {
		try {
			IHttpRequestConfigProvider provider = null;
			// TODO: スケジューラ自体に Request Header を登録できる必要あり。 汎用的に Authorization / X-XSRF-TOKEN などを設定できる方がベストだが、現状はスケジューラに抱え込む形で修正する。
			if (url.contains("/nvx/api/")) {
				provider = new HttpRequestCSRFTokenProviderImpl(nvxapiHeaders);
			} else {
				provider = new HttpRequestConfigProviderImpl(headers);
			}
			IHttpClientWrapper httpClient = new HttpClientWrapper();
			HttpResponseResult response = httpClient.delete(url, null, provider);
			setStatusCode(response.getStatusCode());
			setResponseBody(response.getResponseBody());
		} catch (Exception e) {
			logger.error("executeDelete caused exception: " + e.toString(), e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, "", e.getMessage());
		}
	}

	@Override
	public String getLoggerPrefix() {
		return "[" + this.instanceId + "]";
	}

	public String getResponseBody() {
		return responseBody;
	}

	private void setResponseBody(final String responseBody) {
		// logger.info(responseBody);
		this.responseBody = responseBody;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

}
