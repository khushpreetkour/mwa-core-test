package com.sony.pro.mwa.scheduler.common;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.scheduler.ActivitySchedulerFactory;
import com.sony.pro.mwa.scheduler.dao.ExtensionQrtzTriggersFacade;

public class SchedulerCommon implements ISchedulerConstants {

	private static PropertiesConfiguration config;
	private static ExtensionQrtzTriggersFacade eqtf = new ExtensionQrtzTriggersFacade();

	public static String getQuartzProp(final String key)
	{

		if (config == null) {
			// quartz.propertiesを読み取り
			try {
				config = new PropertiesConfiguration(ActivitySchedulerFactory.getQuartzConfigFileName());
			} catch (ConfigurationException e1) {
				e1.printStackTrace();
			}

		}

		// quartz.propertiesで設定したdata_source名を取得
		return config.getString(key);

	}

	/**
	 * DB登録用のTimeStampを登録する。
	 * 
	 * @return
	 */
	public static String getTimeStampStr()
	{

		final Date date = new Date();
		final SimpleDateFormat sdf = new SimpleDateFormat(TIME_STAMP_FORMAT);
		return sdf.format(date);
	}

	/**
	 * Mwaからの戻りのresultXml（実態はJson）を入れると、InstanceIdを取得します。
	 * 
	 * @param responseBodyJson
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getMwaResponseInstance(String responseBodyJson)
	{

		List<Object> resultList = null;
		try {
			resultList = new ObjectMapper().readValue(responseBodyJson, List.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
			// 発生しないはず。
		} catch (JsonMappingException e) {
			e.printStackTrace();
			// 発生しないはず。
		} catch (IOException e) {
			e.printStackTrace();
			// 発生しないはず。
		}

		String instancdId = "";
		for (Object obj : resultList) {
			if (obj instanceof Map) {
				Map<String, Object> resultMap = (Map<String, Object>) obj;
				if (resultMap.containsValue("ActivityInstanceId")) {
					instancdId = (String) resultMap.get("value");
					break;
				}
			}
		}

		return instancdId;
	}
	
	/**
	 * Mapのkeyを小文字にします。
	 *   --経緯：Postgres(小文字)とH2(大文字)のJDBCでカラム名の表記が小文字、大文字がことなるため、差分吸収する。
	 * @param inMap
	 * @param outMap
	 * @return
	 */
	public static Map<String, Object> keyLower(Map<String, Object> inMap, Map<String, Object> outMap){
		if(outMap == null){
			outMap = new HashMap<String, Object>();
		}
		for(String key : inMap.keySet()){
			outMap.put(key.toLowerCase(), inMap.get(key));
		}
		return outMap;
	}
}
