package com.sony.pro.mwa.scheduler.job;

import java.util.UUID;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.rest.IRestInput;
import com.sony.pro.mwa.scheduler.rest.RestClient;
import com.sony.pro.mwa.scheduler.rest.RestClientFactory;
import com.sony.pro.mwa.scheduler.rest.RestInput;
import com.sony.pro.mwa.scheduler.trigger.bean.TriggerOutputBean;

public class ActivityExecuteJob implements Job, ISchedulerConstants {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException
	{

		RestClient restClient = null;
		String restId = null;

		try {

			JobDataMap dataMap = context.getJobDetail().getJobDataMap();

			// httpMethodTypeの取得
			final String httpMethodType = dataMap.getString(HTTP_METHOD_TYPE).toUpperCase();
			// endpointの取得
			final String endpoint = dataMap.getString(END_POINT);
			// parameterの取得
			final String jsonParam = dataMap.getString(PARAMETER);
			// Sched_idの取得
			final String schedId = dataMap.getString(SCHED_ID);
			// ユニークのUUIDをKeyとする。
			restId = UUID.randomUUID().toString();
			// RESTClientの取得(ユニークのUUIDをKeyとする。
			restClient = RestClientFactory.getInstance(restId);

			// String httpMethod,String url,String jsonParam
			IRestInput restInput = new RestInput(httpMethodType, endpoint, jsonParam);

			// REST実行
			restClient.rest(restInput);

			int statusCd = restClient.getStatusCode();

			// RESTAPIの結果
			switch (statusCd) {

				case 200:
				case 201:
				case 202:
				case 203:
				case 204:
				case 205:
				case 206:
				case 207:
				case 226:
					break;
				default:
					throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, Integer.toString(restClient.getStatusCode()), restClient.getResponseBody());
			}

			// 実行結果をoutputBeanにセット
			TriggerOutputBean outputBean = new TriggerOutputBean();
			outputBean.setSchedId(schedId);
			outputBean.setHttpStatusCode(Integer.toString(restClient.getStatusCode()));
			outputBean.setResponseBody(restClient.getResponseBody());

			// outputBeanをresultへセット。
			context.setResult(outputBean);

		} catch (MwaInstanceError e) {
			throw e;

		} catch (Exception e) {
			throw e;
		} finally {

			if (restClient != null && restId != null) {
				// お掃除処理
				RestClientFactory.removeInstance(restId);
			}

		}

	}

}
