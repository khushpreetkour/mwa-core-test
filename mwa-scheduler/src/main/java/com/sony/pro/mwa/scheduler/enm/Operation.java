package com.sony.pro.mwa.scheduler.enm;

public enum Operation {
	PAUSE,
	RESUME,
	START,
	SHUTDOWN
}

