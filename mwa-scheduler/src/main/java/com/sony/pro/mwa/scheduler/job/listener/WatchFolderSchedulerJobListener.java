package com.sony.pro.mwa.scheduler.job.listener;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.dao.ExtensionQrtzResultTriggersFacade;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzResultTriggers;

public class WatchFolderSchedulerJobListener implements JobListener, ISchedulerConstants {
	private final static MwaLogger logger = MwaLogger.getLogger(WatchFolderSchedulerJobListener.class);

	public static final String LISTENER_NAME = "WatchFolderSchedulerJobListener";
	public final static String SCHED_ID = "schedId";

	private JobExecutionException jobException;
	private JobExecutionContext context;
	private ExtensionQrtzResultTriggersFacade extensionQrtzResultTriggersFacade;
	
	public WatchFolderSchedulerJobListener(ExtensionQrtzResultTriggersFacade extensionQrtzResultTriggersFacade) {
		this.extensionQrtzResultTriggersFacade = extensionQrtzResultTriggersFacade;
	}
	
	@Override
	public String getName()
	{
		return LISTENER_NAME;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context)
	{
		// Sched_idの取得
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String schedId = dataMap.getString(SCHED_ID);

		logger.info("Job is to be executed.");
		logger.info("SCHED_ID : " + schedId + " is going to start...");

	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context)
	{

		// Sched_idの取得
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String schedId = dataMap.getString(SCHED_ID);

		// Job実行しない
		logger.info("Job-Execution vetoed.");
		logger.info("SCHED_ID : " + schedId + "execution vetoed.");
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException)
	
	{
		this.jobException = jobException;
		this.context = context;

		// Sched_idの取得
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String schedId = dataMap.getString(SCHED_ID);

		// 結果DB登録の準備
		ExtensionQrtzResultTriggers entity = new ExtensionQrtzResultTriggers();
		//ExtensionQrtzResultTriggersFacade dao = new ExtensionQrtzResultTriggersFacade();

		// Job実行時の例外
		if (jobException != null && StringUtils.isNotEmpty(jobException.getMessage())) {
			// Job実行時に例外発生
			logger.error("Exception thrown by SCHED_ID:" + schedId, jobException);

			entity.setSched_id(schedId);
			entity.setTrigger_job_status(JOB_RUN_FAILURE);
			entity.setResult(jobException.getMessage());

			// DBに結果挿入
			extensionQrtzResultTriggersFacade.insert(entity);

			MwaInstanceError mwaInstanceError = (MwaInstanceError) jobException.getCause().getCause();
			logger.error(mwaInstanceError.getMessage() + " schedId=" + schedId);
			throw mwaInstanceError;
		}
	}

	public JobExecutionContext getContext()
	{
		return context;
	}

	public JobExecutionException getJobException()
	{
		return jobException;
	}

}
