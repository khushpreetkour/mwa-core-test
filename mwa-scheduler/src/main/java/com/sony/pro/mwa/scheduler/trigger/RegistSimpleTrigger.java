package com.sony.pro.mwa.scheduler.trigger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Matcher;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.matchers.KeyMatcher;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.job.ActivityExecuteJob;
import com.sony.pro.mwa.scheduler.trigger.bean.SimpleTriggerInputBean;

public class RegistSimpleTrigger implements ISchedulerConstants {

	private final static MwaLogger logger = MwaLogger.getLogger(RegistSimpleTrigger.class);

	@SuppressWarnings("unchecked")
	public void scheduleJob(Scheduler scheduler, SimpleTriggerInputBean inputBean,JobListener paramJobListener) throws SchedulerException
	{
		JobDetail jobDetail = JobBuilder
				.newJob(ActivityExecuteJob.class)
				.withIdentity(inputBean.getJobName(), inputBean.getJobGroup())
				.usingJobData(HTTP_METHOD_TYPE, inputBean.getHttpMethodType())
				.usingJobData(END_POINT, inputBean.getEndpoint())
				.usingJobData(PARAMETER, inputBean.getParameter())
				.usingJobData(SCHED_ID, inputBean.getSchedId())
				.storeDurably(false).build();
		@SuppressWarnings("rawtypes")
		final TriggerBuilder tb = TriggerBuilder.newTrigger().withIdentity(inputBean.getTriggerName(), inputBean.getTriggerGroup());
		// 起動時間指定
		if (inputBean.getStartTime() != null) {
			Date date = null;

			if (inputBean.getStartTime() != null) {
				date = new Date(inputBean.getStartTime());
			} else {
				logger.error("startTime is null");
				throw new MwaInstanceError(MWARC.INVALID_INPUT_FORMAT);
			}

			// startTimeが過去の場合、エラー
			if (date.before(new Date())) {
				if(inputBean.isPastTimeSuccessFlg()){
					// 即時実行
					tb.startNow();
				}else{
					logger.error("startTime is in the past. startTime -> " + inputBean.getStartTime());
					throw new MwaInstanceError(MWARC.INVALID_INPUT_FORMAT);
				}
			}else{
				tb.startAt(date);
			}
		} else {
			// 即時実行
			tb.startNow();
		}

		// リピートカウント　リピートインターバルの設定
		if (inputBean.getRepeatCount() != null) {
			final SimpleScheduleBuilder sb = SimpleScheduleBuilder.simpleSchedule();

			// リピートカウント設定
			sb.withRepeatCount(Integer.parseInt(inputBean.getRepeatCount()));

			// リピートインターバル設定あり 単位は秒
			if ((inputBean.getRepeatIntervalSec() != null)) {
				sb.withIntervalInSeconds(Integer.parseInt(inputBean.getRepeatIntervalSec()));
			}

			tb.withSchedule(sb);
		}

		tb.forJob(jobDetail);

		// JobListenr登録
		/**
		 * TOOD SyncTaskとListenerを使い分けできるようにする。 SyncかPollingかの情報を上からもらう必要がある。
		 */
		{
			List<Matcher<JobKey>> matchers = new ArrayList<Matcher<JobKey>>();
			// enumerate each job group
			for (String group : scheduler.getJobGroupNames()) {
				// enumerate each job in group
				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group))) {
					matchers.add(KeyMatcher.keyEquals(jobKey));
				}
			}

			JobKey jobKey = jobDetail.getKey();
			matchers.add(KeyMatcher.keyEquals(jobKey));
			scheduler.getListenerManager().addJobListener(paramJobListener, matchers);
		}

		SimpleTrigger trigger = (SimpleTrigger) tb.build();

		scheduler.scheduleJob(jobDetail, trigger);

		logger.info("Registration of Simple_TriggerJob is complete.");
	}
}
