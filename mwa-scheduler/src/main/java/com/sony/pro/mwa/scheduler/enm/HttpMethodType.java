package com.sony.pro.mwa.scheduler.enm;

public enum HttpMethodType {
	POST,
	GET,
	PUT,
	DELETE
}
