package com.sony.pro.mwa.scheduler.dao.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class ResultSetMapper {

	/**
	 * ResultSetからArrayList<HashMap<String,Object>>への変換
	 * 
	 * @param resultSet
	 * @return　resulList<HashMap<String,Object>>
	 */
	public ArrayList<HashMap<String, Object>> resultSet2ArrayList(ResultSet resultSet) throws SQLException
	{

		ArrayList<HashMap<String, Object>> resultList = new ArrayList<HashMap<String, Object>>();
		ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
		int colCnt = resultSetMetaData.getColumnCount();

		while (resultSet.next()) {

			HashMap<String, Object> map = new HashMap<String, Object>();
			for (int i = 1; i <= colCnt; i++) {
				String colName = resultSetMetaData.getColumnLabel(i);
				Object value = resultSet.getString(i);
				map.put(colName, value);
			}
			resultList.add(map);
		}

		return resultList;
	}

}
