package com.sony.pro.mwa.scheduler.enm;

public enum TriggerType
{
	SIMPLE,
	CRON;
}
