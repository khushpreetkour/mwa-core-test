package com.sony.pro.mwa.scheduler.dao;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.quartz.utils.DBConnectionManager;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.common.SchedulerCommon;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzSequence;
import com.sony.pro.mwa.scheduler.dao.utils.ResultSetMapper;

public class ExtensionQrtzSequenceDAO extends AbstractQrtzDAO implements ISchedulerConstants {

	private static final MwaLogger logger = MwaLogger.getLogger(ExtensionQrtzSequenceDAO.class);

	/**
	 * QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の取得
	 * 
	 * @return
	 */
	public ArrayList<HashMap<String, Object>> find(String pk) throws MwaInstanceError
	{

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> resultList = null;
		try {
			conn = this.getDataSource().getConnection();

			final String sql = "SELECT * FROM QUARTZ.EXTENSION_QRTZ_SEQUENCE WHERE SEQ_NAME = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, pk);

			resultSet = pstmt.executeQuery();

			// resultSetからArrayList<String,Object>に変換
			ResultSetMapper resultSetMapper = new ResultSetMapper();
			resultList = resultSetMapper.resultSet2ArrayList(resultSet);

			resultSet.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			logger.error("Unexpected DB error.", e);
			throw new MwaInstanceError(MWARC.DATABASE_ERROR);
		} finally {
			close(conn, pstmt, resultSet);
		}

		return resultList;

	}

	/**
	 * QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の更新
	 * 
	 * @param beforeEntity
	 *            　QUARTZ.EXTENSION_QRTZ_SEQUENCE更新情報
	 * @return　
	 */
	public int updata(ExtensionQrtzSequence beforeEntity) throws MwaInstanceError
	{

		Connection conn = null;
		PreparedStatement pstmt = null;
		int num = 0;
		try {
			conn = this.getDataSource().getConnection();

			final String sql = "UPDATE QUARTZ.EXTENSION_QRTZ_SEQUENCE SET SEQ_NO = ?, UPDATED_TIME = ?  WHERE SEQ_NAME = ?;";
			pstmt = conn.prepareStatement(sql);
			// Autoコミットしない。
			conn.setAutoCommit(false);

			pstmt.setLong(1, Long.parseLong(beforeEntity.getSeq_no()));
			pstmt.setString(2, SchedulerCommon.getTimeStampStr());
			pstmt.setString(3, beforeEntity.getSeq_name());

			num = pstmt.executeUpdate();

			if (num == 0) {
				logger.error("Upload failed. sql ->" + sql);
				throw new MwaInstanceError(MWARC.DATABASE_ERROR);
			}

			// 楽観ロックチェック
			ArrayList<HashMap<String, Object>> list = find(TRIGGER_SEQ);
			ExtensionQrtzSequence entity = new ExtensionQrtzSequence();
			if (list.size() == 1) {
				HashMap<String, Object> map = list.get(0);
				try {
					BeanUtils.populate(entity, SchedulerCommon.keyLower(map, new HashMap<String, Object>()));
				} catch (IllegalAccessException e) {
					// 発生しないはず
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// 発生しないはず
					e.printStackTrace();
				}
			}

			if (!beforeEntity.getUpdated_time().equals(entity.getUpdated_time())) {
				// 楽観ロックエラー
				logger.error("Optimistic lock error. sql ->" + sql);
				throw new MwaInstanceError(MWARC.DATABASE_ERROR);
			}

			// コミット
			conn.commit();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Unexpected DB error.", e);
			throw new MwaInstanceError(MWARC.DATABASE_ERROR);

		} finally {
			close(conn, pstmt);
		}

		return num;

	}

}
