package com.sony.pro.mwa.scheduler.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.scheduler.common.SchedulerCommon;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzSequence;

public class ExtensionQrtzSequenceFacade {
	
	@Autowired
	private ExtensionQrtzSequenceDAO extensionQrtzSequenceDAO;
	
	@Autowired
	public void setExtensionQrtzSequenceDAO(ExtensionQrtzSequenceDAO extensionQrtzSequenceDAO) {
		this.extensionQrtzSequenceDAO = extensionQrtzSequenceDAO;
	}

	public ExtensionQrtzSequenceDAO getExtensionQrtzSequenceDAO() {
		return extensionQrtzSequenceDAO;
	}

	/**
	 * QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の取得
	 * @return
	 */
	public ExtensionQrtzSequence find(String pk) throws MwaInstanceError{
		
		ArrayList<HashMap<String,Object>> list = extensionQrtzSequenceDAO.find(pk);
		ExtensionQrtzSequence entity = new ExtensionQrtzSequence();
		
		if(list.size() == 1){
			HashMap<String,Object> map = list.get(0);
			try {
				BeanUtils.populate(entity, SchedulerCommon.keyLower(map, new HashMap<String,Object> ()));
				return entity;
			} catch (IllegalAccessException e) {
				//発生しないはず。
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				//発生しないはず。
				e.printStackTrace();
			}
		}

		return null;
	}
	

	/**
	 * QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の更新
	 * @param entity
	 * @return
	 */
	public void update(ExtensionQrtzSequence entity) throws MwaInstanceError{

		//QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の更新
		extensionQrtzSequenceDAO.updata(entity);
		
	}
	
	

	
}
