package com.sony.pro.mwa.scheduler.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.scheduler.common.SchedulerCommon;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzTriggers;
import com.sony.pro.mwa.scheduler.dao.entity.dto.CronTriggersInfoDto;
import com.sony.pro.mwa.scheduler.dao.entity.dto.SImpleTriggersInfoDto;

public class ExtensionQrtzTriggersFacade {
	
	@Autowired
	private ExtensionQrtzTriggersDAO extensionQrtzTriggersDAO;
	
	public ExtensionQrtzTriggersDAO getExtensionQrtzTriggersDAO() {
		return extensionQrtzTriggersDAO;
	}

	@Autowired
	public void setExtensionQrtzTriggersDAO(ExtensionQrtzTriggersDAO extensionQrtzTriggersDAO) {
		this.extensionQrtzTriggersDAO = extensionQrtzTriggersDAO;
	}

	public ExtensionQrtzTriggers find(String pk) throws MwaInstanceError {
		
		ArrayList<HashMap<String,Object>> list = extensionQrtzTriggersDAO.find(pk);
		ExtensionQrtzTriggers entity = new ExtensionQrtzTriggers();
		
		if(list.size() == 1){
			HashMap<String,Object> map = list.get(0);
			try {
				BeanUtils.populate(entity,SchedulerCommon.keyLower(map, new HashMap<String,Object> ()));
				return entity;
			} catch (IllegalAccessException e) {
				//発生しないはず。
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				//発生しないはず。
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	
	/**
	 * CRON Trgger登録情報のリストを取得
	 * @return
	 */
	public List<CronTriggersInfoDto> selectCronInfoListFind(ExtensionQrtzTriggers entity) throws MwaInstanceError{
		
		ArrayList<HashMap<String,Object>> list = extensionQrtzTriggersDAO.selectCronInfoListFind(entity);
		
		ArrayList<CronTriggersInfoDto> resultList = new ArrayList<CronTriggersInfoDto>();
		for(HashMap<String,Object> map :list){
			CronTriggersInfoDto dto = new CronTriggersInfoDto();
			try {
				BeanUtils.populate(dto,SchedulerCommon.keyLower(map, new HashMap<String,Object> ()));
			} catch (IllegalAccessException e) {
				//発生しないはず。
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				//発生しないはず。
				e.printStackTrace();
			}
			resultList.add(dto);
		}
		
		return resultList;
	}
	


	/**
	 * SIMPLE Trgger登録情報のリストを取得
	 * 
	 * @return
	 */
	public List<SImpleTriggersInfoDto> selectSimpleInfoListFind(ExtensionQrtzTriggers entity) throws MwaInstanceError
	{

		ArrayList<HashMap<String, Object>> list = extensionQrtzTriggersDAO.selectSimpleInfoListFind(entity);

		ArrayList<SImpleTriggersInfoDto> resultList = new ArrayList<SImpleTriggersInfoDto>();
		for (HashMap<String, Object> map : list) {
			SImpleTriggersInfoDto dto = new SImpleTriggersInfoDto();
			try {
				BeanUtils.populate(dto,SchedulerCommon.keyLower(map, new HashMap<String,Object> ()));
			} catch (IllegalAccessException e) {
				// 発生しないはず。
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// 発生しないはず。
				e.printStackTrace();
			}
			resultList.add(dto);
		}

		return resultList;
	}
	

	/**
	 * QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の更新
	 * @param entity
	 * @return
	 */
	public void insert(ExtensionQrtzTriggers entity) throws MwaInstanceError{

		//QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の挿入
		 extensionQrtzTriggersDAO.insert(entity);
		
	}

}
