package com.sony.pro.mwa.scheduler.dao.entity.dto;

import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzTriggers;

public class SImpleTriggersInfoDto extends ExtensionQrtzTriggers{
	
	private String repeat_interval;
	
	private String repeat_count;
	
	private String trigger_state;
	
	private String start_time;
	
	private String next_fire_time;

	private String end_time;

	public String getRepeat_interval()
	{
		return repeat_interval;
	}

	public void setRepeat_interval(String repeat_interval)
	{
		this.repeat_interval = repeat_interval;
	}

	public String getRepeat_count()
	{
		return repeat_count;
	}

	public void setRepeat_count(String repeat_count)
	{
		this.repeat_count = repeat_count;
	}

	public String getTrigger_state()
	{
		return trigger_state;
	}

	public void setTrigger_state(String trigger_state)
	{
		this.trigger_state = trigger_state;
	}

	public String getStart_time()
	{
		return start_time;
	}

	public void setStart_time(String start_time)
	{
		this.start_time = start_time;
	}

	public String getNext_fire_time()
	{
		return next_fire_time;
	}

	public void setNext_fire_time(String next_fire_time)
	{
		this.next_fire_time = next_fire_time;
	}

	public String getEnd_time()
    {
        return end_time;
    }

	public void setEnd_time(String end_time)
    {
        this.end_time = end_time;
    }
	
	
}
