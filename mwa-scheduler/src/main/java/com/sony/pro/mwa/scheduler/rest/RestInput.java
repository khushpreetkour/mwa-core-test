package com.sony.pro.mwa.scheduler.rest;

import com.sony.pro.mwa.common.log.MwaLogger;

public class RestInput implements IRestInput {
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	  
	private String httpMethod = null;
	private String url = null;
	private String jsonParam = null;
	
	public RestInput(String httpMethod,String url,String jsonParam) {

			this.httpMethod = httpMethod; 	
			this.url = url; 
			this.jsonParam = jsonParam; 
	}
	
	@Override
	public String getHttpMethod() {
		return httpMethod;
	}

	@Override
	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getJsonParam() {
		return jsonParam;
	}

	@Override
	public void setJsonParam(String jsonParam) {
		this.jsonParam = jsonParam;
	}


}
