package com.sony.pro.mwa.scheduler.dao.entity;

public class ExtensionQrtzSequence {
	
	private String seq_name;
	private String seq_no;
	private String seq_description;
	private String updated_time;
	public String getSeq_name()
	{
		return seq_name;
	}
	public void setSeq_name(String seq_name)
	{
		this.seq_name = seq_name;
	}
	public String getSeq_no()
	{
		return seq_no;
	}
	public void setSeq_no(String seq_no)
	{
		this.seq_no = seq_no;
	}
	public String getSeq_description()
	{
		return seq_description;
	}
	public void setSeq_description(String seq_description)
	{
		this.seq_description = seq_description;
	}
	public String getUpdated_time()
	{
		return updated_time;
	}
	public void setUpdated_time(String updated_time)
	{
		this.updated_time = updated_time;
	}
	
	

}
