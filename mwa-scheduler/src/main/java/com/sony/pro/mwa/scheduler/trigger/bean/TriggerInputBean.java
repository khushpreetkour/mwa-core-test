package com.sony.pro.mwa.scheduler.trigger.bean;

public class TriggerInputBean {

	private String schedId;

	private String triggerName;

	private String triggerGroup;

	private String jobName;

	private String jobGroup;

	private String httpMethodType;

	private String endpoint;

	private String parameter;

	private String description;

	// private String tag;

	private String timeZone;

	private String triggerType;

	private Long startTime;

	private Long endTime;

	public String getSchedId()
	{
		return schedId;
	}

	public void setSchedId(String schedId)
	{
		this.schedId = schedId;
	}

	public String getTriggerName()
	{
		return triggerName;
	}

	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	public String getJobName()
	{
		return jobName;
	}

	public void setJobName(String jobName)
	{
		this.jobName = jobName;
	}

	public String getJobGroup()
	{
		return jobGroup;
	}

	public void setJobGroup(String jobGroup)
	{
		this.jobGroup = jobGroup;
	}

	public String getHttpMethodType()
	{
		return httpMethodType;
	}

	public void setHttpMethodType(String httpMethodType)
	{
		this.httpMethodType = httpMethodType;
	}

	public String getEndpoint()
	{
		return endpoint;
	}

	public void setEndpoint(String endpoint)
	{
		this.endpoint = endpoint;
	}

	public String getParameter()
	{
		return parameter;
	}

	public void setParameter(String parameter)
	{
		this.parameter = parameter;
	}

	public String getTimeZone()
	{
		return timeZone;
	}

	public void setTimeZone(String timeZone)
	{
		this.timeZone = timeZone;
	}

	public String getTriggerType()
	{
		return triggerType;
	}

	public void setTriggerType(String triggerType)
	{
		this.triggerType = triggerType;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

}
