package com.sony.pro.mwa.scheduler.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.scheduler.common.SchedulerCommon;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzResultTriggers;

public class ExtensionQrtzResultTriggersFacade {
	
	@Autowired
	private ExtensionQrtzResultTriggersDAO extensionQrtzResultTriggersDAO;
	
	public ExtensionQrtzResultTriggersDAO getExtensionQrtzResultTriggersDAO() {
		return extensionQrtzResultTriggersDAO;
	}

	@Autowired
	public void setExtensionQrtzResultTriggersDAO(ExtensionQrtzResultTriggersDAO extensionQrtzResultTriggersDAO) {
		this.extensionQrtzResultTriggersDAO = extensionQrtzResultTriggersDAO;
	}

	/**
	 * QUARTZ.EXTENSION_QRTZ_RESULT_TRIGGERS の主キー参照
	 * @param pk
	 * @return　ExtensionQrtzResultTriggers
	 * @throws MwaInstanceError
	 */
	public ExtensionQrtzResultTriggers find(String pk) throws MwaInstanceError {
		
		ArrayList<HashMap<String,Object>> list = extensionQrtzResultTriggersDAO.find(pk);
		ExtensionQrtzResultTriggers entity = new ExtensionQrtzResultTriggers();
		
		if(list.size() == 1){
			HashMap<String,Object> map = list.get(0);
			try {
				BeanUtils.populate(entity,SchedulerCommon.keyLower(map, new HashMap<String,Object> ()));
				return entity;
			} catch (IllegalAccessException e) {
				//発生しないはず。
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				//発生しないはず。
				e.printStackTrace();
			}
		}
		
		return null;
	}

	/**
	 * QUARTZ.EXTENSION_QRTZ_RESULT_TRIGGERS 　テーブル情報の更新
	 * @param entity
	 * @return　
	 */
	public void insert(ExtensionQrtzResultTriggers entity) throws MwaInstanceError{

		//QUARTZ.EXTENSION_QRTZ_SEQUENCE　テーブル情報の挿入
		extensionQrtzResultTriggersDAO.insert(entity);
		
	}
	
}
