package com.sony.pro.mwa.scheduler.rest;

public interface IRestInput {
	
	public String getHttpMethod(); 
	public void setHttpMethod(final String httpMethod); 
	public String getUrl() ;
	public void setUrl(final String url);
	public String getJsonParam() ;
	public void setJsonParam(final String jsonParam) ;

}
