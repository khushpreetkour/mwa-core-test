package com.sony.pro.mwa.scheduler.common;

public interface ISchedulerConstants {

	public final static String QUARTS_PROPS = "quartz.properties";
	public final static String H2_QUARTS_PROPS = "h2.quartz.properties";

	public final static String QUARTS_PROP_DATASOURCE = "org.quartz.jobStore.dataSource";
	public final static String QUARTS_PROP_SCHED_NAME = "org.quartz.scheduler.instanceName";

	public final static String TRIGGER_SEQ = "trigger_seq";

	public final static String JOB_NAME_FORMAT = "Jobname%s";
	public final static String JOB_GROUP_FORMAT = "Jobgroup%s";
	public final static String TRIGGER_NAME_FORMAT = "triggername%s";
	public final static String TRIGGER_GROUP_FORMAT = "triggerGroup%s";

	public final static String HTTP_METHOD_TYPE = "http_method_type";
	public final static String END_POINT = "endpoint";
	public final static String PARAMETER = "parameter";
	public final static String SCHED_ID = "schedId";
	public final static String TRIGGER_TYPE_CRON = "CRON";
	public final static String TRIGGER_TYPE_SIMPLE = "SIMPLE";
	public final static String TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSX";

	public final static String JOB_RUN_FAILURE = "FAILURE";
	public final static String JOB_RUN_SUCCESS = "SUCCESS";

	public final static String STATUS_KEY = "status";
	public final static String SCHED_ID_KEY = "sched_id";
	public final static String START_SUCCESS = "SUCCESS";
	public final static String SHUTDOWN_SUCCESS = "SUCCESS";
	public final static String DELETE_SUCCESS = "SUCCESS";
	public final static String PAUSE_SUCCESS = "SUCCESS";
	public final static String RESUME_SUCCESS = "SUCCESS";

}
