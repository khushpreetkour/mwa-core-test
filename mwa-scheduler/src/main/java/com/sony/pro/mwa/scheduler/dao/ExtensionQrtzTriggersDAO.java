package com.sony.pro.mwa.scheduler.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.quartz.utils.DBConnectionManager;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.common.SchedulerCommon;
import com.sony.pro.mwa.scheduler.dao.entity.ExtensionQrtzTriggers;
import com.sony.pro.mwa.scheduler.dao.utils.ResultSetMapper;

public class ExtensionQrtzTriggersDAO extends AbstractQrtzDAO implements ISchedulerConstants {

	private static final MwaLogger logger = MwaLogger.getLogger(ExtensionQrtzTriggersDAO.class);

	/**
	 * EXTENSION_QRTZ_TRIGGERS　テーブル情報の取得
	 * 
	 * @return
	 */
	public ArrayList<HashMap<String, Object>> find(String pk) throws MwaInstanceError
	{

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> resultList = null;
		try {
			conn = this.getDataSource().getConnection();

			final String sql = "SELECT * FROM QUARTZ.EXTENSION_QRTZ_TRIGGERS WHERE SCHED_ID = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, pk);

			resultSet = pstmt.executeQuery();

			// resultSetからArrayList<String,Object>に変換
			ResultSetMapper resultSetMapper = new ResultSetMapper();
			resultList = resultSetMapper.resultSet2ArrayList(resultSet);

			resultSet.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			logger.error("Unexpected DB error.", e);
			throw new MwaInstanceError(MWARC.DATABASE_ERROR);
		} finally {
			close(conn, pstmt, resultSet);
		}

		return resultList;

	}

	/**
	 * CRON Trgger登録情報のリストを取得
	 * 
	 * @return
	 */
	public ArrayList<HashMap<String, Object>> selectCronInfoListFind(ExtensionQrtzTriggers entity) throws MwaInstanceError
	{
		final String sqlBody = "SELECT" + "	E.SCHED_ID AS SCHED_ID	" + ",E.SCHED_NAME AS SCHED_NAME	" + ",E.TRIGGER_NAME AS TRIGGER_NAME	" + ",E.TRIGGER_GROUP AS TRIGGER_GROUP	"
				+ ",E.TRIGGER_TYPE AS TRIGGER_TYPE	" + ",E.HTTP_METHOD_TYPE AS HTTP_METHOD_TYPE	" + ",E.ENDPOINT AS ENDPOINT	"
				+ ",E.PARAMETER AS PARAMETER	"
				+ ",E.DESCRIPTION AS DESCRIPTION	"
				// + ",E.TAG AS TAG	"
				+ ",E.CREATED_TIME AS CREATED_TIME	" + ",E.UPDATED_TIME AS UPDATED_TIME	" + ",C.CRON_EXPRESSION AS CRON_EXPRESSION	" + ",C.TIME_ZONE_ID AS TIME_ZONE	"
				+ ",T.TRIGGER_STATE AS TRIGGER_STATE	" + ",T.START_TIME AS START_TIME	" + ",T.NEXT_FIRE_TIME AS NEXT_FIRE_TIME " + ",T.END_TIME AS END_TIME " +  "FROM " + "	QUARTZ.EXTENSION_QRTZ_TRIGGERS E	"
				+ ",QUARTZ.QRTZ_CRON_TRIGGERS C	" + ",QUARTZ.QRTZ_TRIGGERS T " + "WHERE " + "E.SCHED_NAME = C.SCHED_NAME	" + "AND E.SCHED_NAME = T.SCHED_NAME	"
				+ "AND E.TRIGGER_NAME = C.TRIGGER_NAME	" + "AND E.TRIGGER_NAME = T.TRIGGER_NAME	" + "AND E.TRIGGER_GROUP = C.TRIGGER_GROUP	" + "AND E.TRIGGER_GROUP = T.TRIGGER_GROUP	";

		return selectSearchInfoListFind(sqlBody, entity);
	}

	/**
	 * SIMPLE Trgger登録情報のリストを取得
	 * 
	 * @return
	 */
	public ArrayList<HashMap<String, Object>> selectSimpleInfoListFind(ExtensionQrtzTriggers entity) throws MwaInstanceError
	{
		final String sqlBody = "SELECT" + " E.SCHED_ID AS SCHED_ID	" + ",E.SCHED_NAME AS SCHED_NAME	" + ",E.TRIGGER_NAME AS TRIGGER_NAME	" + ",E.TRIGGER_GROUP AS TRIGGER_GROUP	"
				+ ",E.TRIGGER_TYPE AS TRIGGER_TYPE	" + ",E.HTTP_METHOD_TYPE AS HTTP_METHOD_TYPE	" + ",E.ENDPOINT AS ENDPOINT	"
				+ ",E.PARAMETER AS PARAMETER	"
				+ ",E.DESCRIPTION AS DESCRIPTION	"
				// + ",E.TAG AS TAG	"
				+ ",E.CREATED_TIME AS CREATED_TIME	" + ",E.UPDATED_TIME AS UPDATED_TIME	" + ",S.REPEAT_COUNT AS REPEAT_COUNT" + ",S.REPEAT_INTERVAL AS REPEAT_INTERVAL"
				+ ",T.TRIGGER_STATE AS TRIGGER_STATE	" + ",T.START_TIME AS START_TIME	" + ",T.NEXT_FIRE_TIME AS NEXT_FIRE_TIME " + ",T.END_TIME AS END_TIME " + "FROM " + "	QUARTZ.EXTENSION_QRTZ_TRIGGERS E	"
				+ ",QUARTZ.QRTZ_SIMPLE_TRIGGERS S	" + ",QUARTZ.QRTZ_TRIGGERS T " + "WHERE " + "E.SCHED_NAME = S.SCHED_NAME	" + "AND E.SCHED_NAME = T.SCHED_NAME	"
				+ "AND E.TRIGGER_NAME = S.TRIGGER_NAME	" + "AND E.TRIGGER_NAME = T.TRIGGER_NAME	" + "AND E.TRIGGER_GROUP = S.TRIGGER_GROUP	" + "AND E.TRIGGER_GROUP = T.TRIGGER_GROUP	";

		return selectSearchInfoListFind(sqlBody, entity);
	}

	private ArrayList<HashMap<String, Object>> selectSearchInfoListFind(String sql, ExtensionQrtzTriggers entity)
	{

		Connection conn = null;
		ResultSet resultSet = null;
		PreparedStatement pstmt = null;
		ArrayList<HashMap<String, Object>> resultList = null;
		StringBuilder sb = new StringBuilder();

		try {
			conn = this.getDataSource().getConnection();

			final String sqlBody = sql;

			final String sqlWhereBySchedId = "AND E.SCHED_ID = ? ";
			// final String sqlWhereByTab = "AND E.TAG = ? ";
			final String sqlOrgerBy = "ORDER BY	" + "E.TRIGGER_NAME ASC";

			// SQL生成
			sb = new StringBuilder();
			sb.append(sqlBody);

			List<String> bindingList = new ArrayList<String>();

			// WHERE条件
			if (entity.getSched_id() != null) {
				sb.append(sqlWhereBySchedId);
				bindingList.add(entity.getSched_id());
			}

			// if(entity.getTag() != null){
			// sb.append(sqlWhereByTab);
			// bindingList.add(entity.getTag());
			// }

			// ソート条件
			sb.append(sqlOrgerBy);
			pstmt = conn.prepareStatement(sb.toString());

			// バイディング
			for (int i = 0; i < bindingList.size(); i++) {
				int index = (i + 1);
				pstmt.setString(index, bindingList.get(i));
			}

			resultSet = pstmt.executeQuery();

			// resultSetからArrayList<String,Object>に変換
			ResultSetMapper resultSetMapper = new ResultSetMapper();
			resultList = resultSetMapper.resultSet2ArrayList(resultSet);

			resultSet.close();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			logger.error("Unexpected DB error.", e);
			throw new MwaInstanceError(MWARC.DATABASE_ERROR);
		} finally {
			close(conn, pstmt, resultSet);
		}

		return resultList;

	}

	/**
	 * EXTENSION_QRTZ_TRIGGERS　テーブル情報の更新
	 * 
	 * @param EXTENSION_QRTZ_TRIGGERS
	 *            　テーブル更新情報
	 * @return
	 */
	public int updata(ExtensionQrtzTriggers beforeEntity) throws MwaInstanceError
	{

		return 0;

	}

	/**
	 * EXTENSION_QRTZ_TRIGGERS　テーブル情報の更新
	 * 
	 * @param seq
	 * @param updatedTime
	 * @return
	 */
	public int insert(ExtensionQrtzTriggers entity) throws MwaInstanceError
	{

		Connection conn = null;
		PreparedStatement pstmt = null;
		int num = 0;
		try {
			conn = this.getDataSource().getConnection();

			final String sql = "INSERT INTO QUARTZ.EXTENSION_QRTZ_TRIGGERS (" + "sched_id" + ",sched_name" + ",trigger_name" + ",trigger_group" + ",trigger_type" + ",http_method_type" + ",endpoint"
					+ ",parameter" + ",description"
					// + ",tag"
					+ ",created_time" + ",updated_time) values (?,?,?,?,?,?,?,?,?,?,?);";

			pstmt = conn.prepareStatement(sql);
			// Autoコミットしない。
			conn.setAutoCommit(false);

			pstmt.setString(1, entity.getSched_id());
			pstmt.setString(2, entity.getSched_name());
			pstmt.setString(3, entity.getTrigger_name());
			pstmt.setString(4, entity.getTrigger_group());
			pstmt.setString(5, entity.getTrigger_type());
			pstmt.setString(6, entity.getHttp_method_type());
			pstmt.setString(7, entity.getEndpoint());
			pstmt.setString(8, entity.getParameter());
			pstmt.setString(9, entity.getDescription());
			// pstmt.setString(10, entity.getTag());

			// 登録・更新タイムスタンプのセット
			final String update = SchedulerCommon.getTimeStampStr();
			pstmt.setString(10, update);
			pstmt.setString(11, update);

			// DB挿入
			num = pstmt.executeUpdate();

			if (num == 0) {
				logger.error("Insert failed. sql ->" + sql);
				throw new MwaInstanceError(MWARC.DATABASE_ERROR);
			}

			// コミット
			conn.commit();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			logger.error("Unexpected DB error.", e);
			throw new MwaInstanceError(MWARC.DATABASE_ERROR);

		} finally {
			close(conn, pstmt);
		}

		return num;

	}

}
