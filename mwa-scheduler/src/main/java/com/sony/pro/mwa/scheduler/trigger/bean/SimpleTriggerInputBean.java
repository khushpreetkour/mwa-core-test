package com.sony.pro.mwa.scheduler.trigger.bean;

public class SimpleTriggerInputBean extends TriggerInputBean{
	
	private String repeatCount;

	private String repeatIntervalSec;

	private boolean pastTimeSuccessFlg;
	
	public String getRepeatCount()
	{
		return repeatCount;
	}

	public void setRepeatCount(String repeatCount)
	{
		this.repeatCount = repeatCount;
	}

	public String getRepeatIntervalSec()
	{
		return repeatIntervalSec;
	}

	public void setRepeatIntervalSec(String repeatIntervalSec)
	{
		this.repeatIntervalSec = repeatIntervalSec;
	}

	public boolean isPastTimeSuccessFlg()
	{
		return pastTimeSuccessFlg;
	}

	public void setPastTimeSuccessFlg(boolean pastTimeSuccessFlg)
	{
		this.pastTimeSuccessFlg = pastTimeSuccessFlg;
	}

	
}
