package com.sony.pro.mwa.scheduler.dao.entity;

public class ExtensionQrtzTriggers {

	private String sched_id;

	private String sched_name;

	private String trigger_name;

	private String trigger_group;

	private String trigger_type;

	private String http_method_type;

	private String endpoint;

	private String parameter;

	private String description;

	// private String tag;

	private String created_time;

	private String updated_time;

	private String end_time;

	public String getSched_id()
	{
		return sched_id;
	}

	public void setSched_id(String sched_id)
	{
		this.sched_id = sched_id;
	}

	public String getSched_name()
	{
		return sched_name;
	}

	public void setSched_name(String sched_name)
	{
		this.sched_name = sched_name;
	}

	public String getTrigger_name()
	{
		return trigger_name;
	}

	public void setTrigger_name(String trigger_name)
	{
		this.trigger_name = trigger_name;
	}

	public String getTrigger_group()
	{
		return trigger_group;
	}

	public void setTrigger_group(String trigger_group)
	{
		this.trigger_group = trigger_group;
	}

	public String getTrigger_type()
	{
		return trigger_type;
	}

	public void setTrigger_type(String trigger_type)
	{
		this.trigger_type = trigger_type;
	}

	public String getHttp_method_type()
	{
		return http_method_type;
	}

	public void setHttp_method_type(String http_method_type)
	{
		this.http_method_type = http_method_type;
	}

	public String getEndpoint()
	{
		return endpoint;
	}

	public void setEndpoint(String endpoint)
	{
		this.endpoint = endpoint;
	}

	public String getParameter()
	{
		return parameter;
	}

	public void setParameter(String parameter)
	{
		this.parameter = parameter;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getCreated_time()
	{
		return created_time;
	}

	public void setCreated_time(String created_time)
	{
		this.created_time = created_time;
	}

	public String getUpdated_time()
	{
		return updated_time;
	}

	public void setUpdated_time(String updated_time)
	{
		this.updated_time = updated_time;
	}

	public String getEnd_time()
    {
        return end_time;
    }

	public void setEnd_time(String end_time)
    {
        this.end_time = end_time;
    }
}
