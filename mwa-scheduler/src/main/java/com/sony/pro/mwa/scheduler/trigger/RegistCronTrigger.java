package com.sony.pro.mwa.scheduler.trigger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Matcher;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.matchers.KeyMatcher;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.common.ISchedulerConstants;
import com.sony.pro.mwa.scheduler.dao.ExtensionQrtzResultTriggersFacade;
import com.sony.pro.mwa.scheduler.job.ActivityExecuteJob;
import com.sony.pro.mwa.scheduler.job.listener.PollingJobListener;
import com.sony.pro.mwa.scheduler.trigger.bean.CronTriggerInputBean;

public class RegistCronTrigger implements ISchedulerConstants {

	private final static MwaLogger logger = MwaLogger.getLogger(RegistCronTrigger.class);

	private ExtensionQrtzResultTriggersFacade extensionQrtzResultTriggersFacade;

	public RegistCronTrigger(ExtensionQrtzResultTriggersFacade extensionQrtzResultTriggersFacade){
		this.extensionQrtzResultTriggersFacade = extensionQrtzResultTriggersFacade;
	}

	public void scheduleJob(Scheduler scheduler, CronTriggerInputBean inputBean) throws SchedulerException
	{

		JobDetail jobDetail = JobBuilder.newJob(ActivityExecuteJob.class)
				.withIdentity(inputBean.getJobName(), inputBean.getJobGroup())
				.usingJobData(HTTP_METHOD_TYPE, inputBean.getHttpMethodType())
				.usingJobData(END_POINT, inputBean.getEndpoint())
				.usingJobData(PARAMETER, inputBean.getParameter())
				.usingJobData(SCHED_ID, inputBean.getSchedId())
				.storeDurably(true).build();

		// CronExpression が　Cronのスケジュール設定
		TriggerBuilder<CronTrigger> triggerBuilder = TriggerBuilder.newTrigger()
				.withIdentity(inputBean.getTriggerName(), inputBean.getTriggerGroup())
				.withSchedule(CronScheduleBuilder.cronSchedule(inputBean.getCronExpression())
						.withMisfireHandlingInstructionDoNothing()
						.inTimeZone(TimeZone.getTimeZone(inputBean.getTimeZone())));
		if (inputBean.getStartTime() != null) 
			triggerBuilder.startAt(new Date(inputBean.getStartTime()));
		else 
			triggerBuilder.startNow();
		if (inputBean.getEndTime() != null) 
			triggerBuilder.endAt(new Date(inputBean.getEndTime()));
		CronTrigger trigger = triggerBuilder.build();

		Date currentDate = new Date(System.currentTimeMillis());
		Date nextFireDate = trigger.getFireTimeAfter(currentDate);
		if (nextFireDate == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_SCHEDULER);
		}

		Calendar currentTimeCalendar = Calendar.getInstance();
		currentTimeCalendar.setTime(currentDate);
		currentTimeCalendar.add(Calendar.MINUTE, 5);
		currentTimeCalendar.add(Calendar.SECOND, -1);
		Date afterFiveMinutesDate = currentTimeCalendar.getTime();
		Date nextFireAfterFiveMinutesDate = trigger.getFireTimeAfter(afterFiveMinutesDate);
		if (nextFireAfterFiveMinutesDate == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_SCHEDULER_AFTER_FIVE_MINUTES);
		}

		// JobListenr登録
		/**
		 * TOOD SyncTaskとListenerを使い分けできるようにする。 SyncかPollingかの情報を上からもらう必要がある。
		 */
		{
			List<Matcher<JobKey>> matchers = new ArrayList<Matcher<JobKey>>();
			// enumerate each job group
			for (String group : scheduler.getJobGroupNames()) {
				// enumerate each job in group
				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group))) {
					matchers.add(KeyMatcher.keyEquals(jobKey));
				}
			}

			JobKey jobKey = jobDetail.getKey();
			matchers.add(KeyMatcher.keyEquals(jobKey));
			scheduler.getListenerManager().addJobListener(new PollingJobListener(this.extensionQrtzResultTriggersFacade), matchers);
		}

		scheduler.scheduleJob(jobDetail, trigger);

		logger.info("Registration of Cron_TriggerJob is complete.");
	}

}
