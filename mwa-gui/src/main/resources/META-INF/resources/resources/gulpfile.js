'use strict'

var path = require('path');
var minimist = require('minimist');
var merge = require('merge-stream');

// Pass arguments from the command line
// (https://github.com/gulpjs/gulp/blob/master/docs/recipes/pass-arguments-from-cli.md)
var options = minimist(process.argv.slice(2));

var SCRIPT_DIRECTORY = './scripts/' + options.name;
var STYLE_DIRECTORY = './styles/' + options.name;

var gulp = require('gulp');
var filter = require('gulp-filter');
var umd = require('gulp-umd');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var logger = require('gulp-logger');

gulp.task('generate-sources', function() {
  var script = gulp.src(
      [
        SCRIPT_DIRECTORY + '/' + options.name + '.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.enumeration.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.util.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.js',
//        SCRIPT_DIRECTORY + '/' + options.name + '.model.sample.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.key-value.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.file-system-entity.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.file.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.directory.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.scheduler.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-parameter-type-value.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-parameter-type.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-parameter.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-condition.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-instance.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-profile-group.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-profile.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.activity-profile-group-detail.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.workflow-diagram.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.workflow.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.workflow-diagram-detail.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.model.workflow-detail.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.js',
//        SCRIPT_DIRECTORY + '/' + options.name + '.view.sample.js'
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-diagram-save-dialog.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.activity-profile-list-view.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-step.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-connection-port.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-connection.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-parameter-editor.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-canvas.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-monitor.js',
        SCRIPT_DIRECTORY + '/' + options.name + '.view.workflow-editor.js'
      ]
      )
      .pipe(concat(options.name + '-' + options.version + '.js'))
      .pipe(umd({
        dependencies: function(file) {
          return [
            {
              name: 'Log4js',
              amd: 'log4js',
              cjs: 'log4js',
              global: 'Log4js',
              param: 'Log4js'
            },
            {
              name: 'properties',
              amd: 'properties',
              cjs: 'properties',
              global: '$.i18n',
              param: 'properties'
            },
            {
              name: 'Pro-UI',
              amd: 'proui',
              cjs: 'proui',
              global: 'proui',
              param: 'proui'
            },
            {
              name: 'Pro-UI Extension',
              amd: 'proui-ext',
              cjs: 'proui-ext',
              global: 'prouiExt',
              param: 'prouiExt'
            }
          ];
        },
        namespace: function(file) {
          return options.namespace;
        },
        exports: function(file) {
          return options.namespace;
        }
      }))
      .pipe(gulp.dest(SCRIPT_DIRECTORY))
      .pipe(logger({beforeEach: '[prepare-script] wrote: '}));
  var style = gulp.src(
      [
        STYLE_DIRECTORY + '/' + options.name + '.less'
      ]
      )
      .pipe(rename(options.name + '-' + options.version + '.less'))
      .pipe(gulp.dest(STYLE_DIRECTORY))
      .pipe(logger({beforeEach: '[prepare-style ] wrote: '}));
  return merge(script, style);
});

gulp.task('generate-scripts', ['generate-sources'], function() {
  return gulp.src([SCRIPT_DIRECTORY + '/**/*.js'])
      .pipe(filter(['*', '!**/*.min.js']))
      .pipe(uglify())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest(SCRIPT_DIRECTORY))
      .pipe(logger({beforeEach: '[minify-script ] wrote: '}));
});

gulp.task('generate-styles', ['generate-sources'], function() {
  return gulp.src([STYLE_DIRECTORY + '/**/*.less'])
      .pipe(less())
      .pipe(gulp.dest(STYLE_DIRECTORY))
      .pipe(logger({beforeEach: '[compile-style ] wrote: '}))
      .pipe(minifyCss())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest(STYLE_DIRECTORY))
      .pipe(logger({beforeEach: '[minify-style  ] wrote: '}));
});

gulp.task('build', ['generate-sources', 'generate-scripts', 'generate-styles']);

gulp.task('default', ['build']);
