;

if (process.versions != null && process.versions['atom-shell'] != null) {
  // Atom-shell
  // Do Nothing
} else {
  // NW.js
  global.document = window.document;
  global.navigator = window.navigator;
  global.location = window.location;

  global.XMLHttpRequest = window.XMLHttpRequest;

  require('nw.gui').Window.get().showDevTools();
}

// CAUTION:
// The script files of Pro-UI library are dependent on its style files. Therefore, it is necessary
// to load the desired style files before loading script files.
require('proui-style-flat');
require('proui-ext-style-flat');

// CAUTION:
// It is necessary to load script files after rendering DOM including images because Pro-UI library
// will confirm whether own style files are loaded appropriately by referring to the background
// image of html element.
window.onload = function() {
  var fs = require('fs');
  var $ = require('jquery');

  var mwa = require('../../scripts/mwa/mwa');
  mwa.enumeration = require('../../scripts/mwa/mwa.enumeration');
  mwa.model = require('../../scripts/mwa/mwa.model');
  // mwa.model.sample = require('../../scripts/mwa/mwa.model.sample');
  mwa.model.keyValue = require('../../scripts/mwa/mwa.model.key-value');
  mwa.model.activity = require('../../scripts/mwa/mwa.model.activity');
  mwa.model.activityInstance = require('../../scripts/mwa/mwa.model.activity-instance');
  mwa.model.workflow = require('../../scripts/mwa/mwa.model.workflow');

  mwa.view = require('../../scripts/mwa/mwa.view');
  // mwa.view.sample = require('../../scripts/mwa/mwa.view.sample');
  mwa.view.workflowCanvas = require('../../scripts/mwa/mwa.view.workflow-canvas.js');
  mwa.view.workflowMonitor = require('../../scripts/mwa/mwa.view.workflow-monitor.js');

  mwa.Model.prototype.urlRoot = function(resource) {
    resource = (typeof resource === 'undefined' || resource == null) ? '' : resource;
    return 'http://localhost:8181/mwa/api/v2/' + resource;
  };
  // mwa.Model.prototype.urlRoot = function(resource) {
  //   resource = (typeof resource === 'undefined' || resource == null) ? '' : resource;
  //   return 'http://localhost:8080/api/' + resource;
  // };

  var activityCollection = new mwa.model.activity.ActivityCollection();
  activityCollection.fetch(
      {
        success: function(collection, response, options) {
          console.log('success');
          console.log(collection);
          console.log(response);

          var activityModel = activityCollection.findWhere({
            name: 'com.sony.pro.mwa.activity.generic.filemanagement.transfer.TransferTask'
          });
          activityModel.listenTo(
              activityModel,
              'change',
              function(model, options) {
                console.log('change');
              }
          );
          activityModel.fetch({
            success: function(model, response, options) {
              console.log('success');
              console.log(model);
              console.log(response);
            },
            error: function(model, response, options) {
              console.log('error');
            }
          });

          var parameterArray = [
            {
              key: 'mode',
              value: 'copy'
            },
            {
              key: 'src',
              value: '<list><string>C:/mwa_home/src.txt</string></list>'
            },
            {
              key: 'dest',
              value: '<list><string>C:/mwa_home/' + Date.now() + '.txt</string></list>'
            }
          ];

          // Normal Case
          activityModel.createInstance(
              new mwa.model.keyValue.KeyValueCollection(parameterArray),
              {
                success: function(model, response, options) {
                  console.log('success');
                  console.log(model);
                  console.log(response);

                  var activityInstanceModel = new mwa.model.activityInstance.ActivityInstanceModel({
                    id: response[0].value,
                    templateId: activityModel.get('id'),
                    templateName: activityModel.get('name'),
                    templateVersion: activityModel.get('version')
                  });

                  activityInstanceModel.operateInstance(
                      'REQUEST_CANCEL',
                      null,
                      null,
                      {
                        success: function(model, response, options) {
                          console.log('success');
                          console.log(model);
                          console.log(response);
                        },
                        error: function(model, response, options) {
                          console.log('error');
                        }
                      }
                  );
                },
                error: function(model, response, options) {
                  console.log('error');
                }
              }
          );
          // Error Case
          activityModel.createInstance(parameterArray);
        },
        error: function(collection, response, options) {
          console.log('error');
        }
      }
  );

  var activityInstanceCollection = new mwa.model.activityInstance.ActivityInstanceCollection();
  activityInstanceCollection.fetch(
      {
        success: function(collection, response, options) {
          console.log('success');
          console.log(collection);
          console.log(response);

          var activityInstanceModel = new mwa.model.activityInstance.ActivityInstanceModel({
            id: collection.first().get('id'),
            templateId: collection.first().get('templateId'),
            templateName: collection.first().get('templateName'),
            templateVersion: collection.first().get('templateVersion')
          });
          activityInstanceModel.listenTo(
              activityInstanceModel,
              'change',
              function(model, options) {
                console.log('change');
              }
          );
          activityInstanceModel.fetch({
            success: function(model, response, options) {
              console.log('success');
              console.log(model);
              console.log(response);
            },
            error: function(model, response, options) {
              console.log('error');
            }
          });
        },
        error: function(collection, response, options) {
          console.log('error');
        }
      }
  );

  var testWorkflowModel = new mwa.model.workflow.WorkflowModel(
      JSON.parse(fs.readFileSync('workflow.json', 'utf8')),
      { parse: true }
      );

  var workflowCollection = new mwa.model.workflow.WorkflowCollection();
  workflowCollection.fetch();

  var workflowMonitorView = new mwa.view.workflowMonitor.WorkflowMonitorView({
    el: $('#mwa-view-workflow-monitor'),
    viewModel: new mwa.view.workflowMonitor.WorkflowMonitorViewModel({
      select: function(selectedWorkflowInstanceModel, isSelected) {
        var lastSelectedWorkflowInstanceModel =
            workflowMonitorView.options.viewModel.get('lastSelectedWorkflowInstanceModel');
        workflowCanvasView.options.viewModel.set(
            'workflowInstanceModel', lastSelectedWorkflowInstanceModel
        );
        if (typeof lastSelectedWorkflowInstanceModel === 'undefined' ||
            lastSelectedWorkflowInstanceModel == null) {
          workflowCanvasView.options.viewModel.set('workflowModel', null);
        } else {
          workflowCanvasView.options.viewModel.set('workflowModel', testWorkflowModel);
        }
      }
    })
  });

  var workflowCanvasView = new mwa.view.workflowCanvas.WorkflowCanvasView({
    el: $('#mwa-view-workflow-canvas'),
    viewModel: new mwa.view.workflowCanvas.WorkflowCanvasViewModel({
      workflowModel: testWorkflowModel
    })
  });
};
