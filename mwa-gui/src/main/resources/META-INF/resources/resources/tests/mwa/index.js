;

if (process.versions != null && process.versions['atom-shell'] != null) {
  // Atom-shell
  // Do Nothing
} else {
  // NW.js
  global.document = window.document;
  global.navigator = window.navigator;
  // global.location = window.location;

  global.XMLHttpRequest = window.XMLHttpRequest;

  require('nw.gui').Window.get().showDevTools();
}

// CAUTION:
// The script files of Pro-UI library are dependent on its style files. Therefore, it is necessary
// to load the desired style files before loading script files.
require('proui-style-flat');
require('proui-ext-style-flat');

// CAUTION:
// It is necessary to load script files after rendering DOM including images because Pro-UI library
// will confirm whether own style files are loaded appropriately by referring to the background
// image of html element.
window.onload = function() {
  var fs = require('fs');
  var $ = require('jquery');

  var mwa = require('../../scripts/mwa/mwa');
  mwa.enumeration = require('../../scripts/mwa/mwa.enumeration');
  mwa.model = require('../../scripts/mwa/mwa.model');
  // mwa.model.sample = require('../../scripts/mwa/mwa.model.sample');
  mwa.model.activity = require('../../scripts/mwa/mwa.model.activity');
  mwa.model.workflowDiagramDetail = require('../../scripts/mwa/mwa.model.workflow-diagram-detail');

  mwa.view = require('../../scripts/mwa/mwa.view');
  // mwa.view.sample = require('../../scripts/mwa/mwa.view.sample');
  mwa.view.workflowEditor = require('../../scripts/mwa/mwa.view.workflow-editor');

  var HOST = 'localhost';
  var PORT = '8181';
  var API_URL_ROOT = 'http://' + HOST + ':' + PORT + '/mwa/api/v2/';
  // var HOST = '43.16.195.179';
  // var PORT = '443';
  // var API_URL_ROOT = 'https://' + HOST + ':' + PORT + '/nvx/api/v1/';

  var render = function(data, status) {
    mwa.Model.prototype.urlRoot = function(resource) {
      resource = (typeof resource === 'undefined' || resource == null) ? '' : resource;
      return API_URL_ROOT + resource;
    };
    mwa.Collection.prototype.url = mwa.Model.prototype.urlRoot;

    var activityCollection = new mwa.model.activity.ActivityCollection();
    var workflowDiagramDetailCollection = new mwa.model.workflowDiagramDetail.WorkflowDiagramDetailCollection();
    var workflowDetailCollection = new mwa.model.workflowDetail.WorkflowDetailCollection();

    mwa.proui.Backbone.$.when(
        activityCollection.fetch({
          data: {
            sort: [
              'name+'
            ],
            filter: [
              mwa.proui._.values(
                  mwa.enumeration.WorkflowStepType
              ).map(
                  function(workflowStepType, index, array) {
                    return 'name==' + workflowStepType.templateName;
                  }
              ).concat([
                'name==com.sony.pro.mwa.activity.generic.cloud.services.GenericUploadTask',
                'name==com.sony.pro.mwa.activity.generic.qc.SimpleGenericQCTask',
                'name==com.sony.pro.mwa.activity.generic.transfer.services.GenericTransferTask',
                'name==com.sony.pro.mwa.activity.generic.transcoder.SimpleTranscodeTask',
                'type==' + mwa.enumeration.ActivityType.START.name,
                'type==' + mwa.enumeration.ActivityType.END.name
              ]).join(',')
            ]
          }
        }),
        workflowDiagramDetailCollection.fetch({
          data: {
            sort: [
              workflowDiagramDetailCollection.comparator + '+'
            ]
          }
        }),
        workflowDetailCollection.fetch({
          data: {
            sort: [
              workflowDetailCollection.comparator + '+'
            ],
            filter: [
              'deleteFlag==false'
            ]
          }
        })
    ).done(
        function(activityCollectionResponseArray, workflowDiagramDetailCollectionResponseArray, workflowDetailCollectionResponseArray) {
          workflowDiagramDetailCollection.add(
              new mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel(
                  JSON.parse(fs.readFileSync('workflow-diagram.json', 'utf8')),
                  {parse: true}
              )
          );

          var workflowEditorView = new mwa.view.workflowEditor.WorkflowEditorView({
            el: $('#mwa-view-workflow-editor'),
            viewModel: new mwa.view.workflowEditor.WorkflowEditorViewModel({
              hasWorkflowDiagramNavigationTreeView: true,
              activityCollection: activityCollection,
              workflowDiagramDetailCollection: workflowDiagramDetailCollection,
              workflowDetailCollection: workflowDetailCollection
            })
          });
        }
    );
  };

  render();
  // mwa.proui.Backbone.$.post('https://' + HOST + ':' + PORT + '/nvx/login',
  //     {
  //       username: 'amsadmin',
  //       password: 'S0nyAmsAdm1n'
  //     },
  //     render
  // );
};
