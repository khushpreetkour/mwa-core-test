/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

// INFORMATION:
// The following steps are an overview for adding a new View.
// 1. Create a new JavaScript file by copying
//    src/main/resources/META-INF/resources/resources/scripts/mwa.view.sample.js.
//  1.1. Replace the name of Namespace, View and View Model with appropriate one.
//  1.2. Add dependency modules to UMD header.
// 2. Create a new LESS file by copying
//    src/main/resources/META-INF/resources/resources/styles/mwa.view.sample.less.
//  2.1. Replace the name of Namespace with appropriate one.
// 3. Add a size definition section for the new View to
//    src/main/resources/META-INF/resources/resources/styles/mwa.size.less
//    in consideration of the dependency.
// 4. Add a color definition section for the new View to
//    src/main/resources/META-INF/resources/resources/styles/mwa.color.less
//    in consideration of the dependency.
// 5. Import the new LESS file in
//    src/main/resources/META-INF/resources/resources/styles/mwa.less
//    in consideration of the dependency.
// 6. Add the path of new JavaScript file to
//    src/main/resources/META-INF/resources/resources/gulpfile.js for build.
//    in consideration of the dependency.

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {}
    );
  } else {
    root.mwa.view.sample = factory(
        root.mwa,
        root.mwa.view
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }

  /**
   * The mwa.view.sample namespace.
   * @namespace
   */
  mwa.view.sample = (function() {
    'use strict';

    /**
     * Defines mwa.view.sample alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa sample view model.
     * @constructor
     * @inner
     * @alias mwa.view.sample.SampleViewModel
     * @memberof mwa.view.sample
     * @extends {mwa.ViewModel}
     */
    global.SampleViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.sample.SampleViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
//                    id: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
//                  id: [
//                    {
//                      required: true,
//                      msg: 'The "id" is required.'
//                    }
//                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa sample view.
     * @constructor
     * @inner
     * @alias mwa.view.sample.SampleView
     * @memberof mwa.view.sample
     * @extends {mwa.View}
     */
    global.SampleView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.sample.SampleView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                // TODO: To define the DOM structure.
                '<div class="mwa-view-sample"></div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              mwa.View.prototype.render.apply(this, []);

              // If you need new sub views, add here.

              return this;
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.sample;
}));
