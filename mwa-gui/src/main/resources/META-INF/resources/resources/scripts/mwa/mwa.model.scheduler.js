/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.scheduler = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.scheduler namespace.
   * @namespace
   */
  mwa.model.scheduler = (function() {
    'use strict';

    /**
     * Defines mwa.model.scheduler alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa scheduler model.
     * @constructor
     * @inner
     * @alias mwa.model.scheduler.SchedulerModel
     * @memberof mwa.model.scheduler
     * @extends {mwa.Model}
     */
    global.SchedulerModel = (function() {
      return mwa.Model.extend(
          /**
           * @lends mwa.model.scheduler.SchedulerModel
           */
          {
            alias: 'mwa.model.scheduler.SchedulerModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'schedulers' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    name: null,
                    requestUrl: null,
                    requestType: null,
                    requestData: null,
                    cronExpression: null,
                    cronTimeZone: null,
                    createTime: null,
                    updateTime: null,
                    startTime: null,
                    endTime: null,
                    previousFireTime: null,
                    nextFireTime: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  requestUrl: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  requestType: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        var type = mwa.enumeration.SchedulerRequestType[value];
                        if (typeof type === 'undefined' || type == null) {
                          msg = 'The value of "' + attr + '" is not included in [' +
                              mwa.proui._.keys(mwa.enumeration.SchedulerRequestType).join(', ') + '].';
                        }
                        return msg;
                      }
                    }
                  ],
                  requestData: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  cronExpression: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  cronTimeZone: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  createTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  updateTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  startTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  endTime: [
                    {
                      required: false
                    },
                    {
                      type: 'number'
                    }
                  ],
                  previousFireTime: [
                    {
                      required: false
                    },
                    {
                      type: 'number'
                    }
                  ],
                  nextFireTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ]
                }
            ),
            // TODO: NVX-7691 (https://www.tool.sony.biz/jira/browse/NVX-7691)
            // To remove this after completing BL implementation.
            /**
             * @see {@link mwa.Model.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.Model.prototype.parse.apply(this, [response, options]);

              if (response.id == null) {
                response.id = response.quartzId;
              }

              return response;
            },
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.Model.createEditingMetadataFieldCollection}
             * @public
             * @override
             */
            createEditingMetadataFieldCollection: function() {
              var modelEditingMetadataFieldCollection =
                  mwa.Model.prototype.createEditingMetadataFieldCollection.apply(this, []);
              var length = modelEditingMetadataFieldCollection.length;

              var SCHEDULER_PATTERN_TYPE_ARRAY =
                  mwa.proui._.sortBy(
                      mwa.proui._.values(mwa.enumeration.SchedulerPatternType),
                      'index'
                  );
              var metadataFields =
                  SCHEDULER_PATTERN_TYPE_ARRAY.reduce(
                      function(memo, schedulerPatternType, index, array) {
                        // CAUTION: NVX-7666 (https://www.tool.sony.biz/jira/browse/NVX-7666)
                        // It is necessary to add double quotes for string value.
                        memo['"' + schedulerPatternType.property + '"'] =
                            new mwa.prouiExt.metadataEditor.MetadataFieldCollection(
                                schedulerPatternType.metadataFieldArray
                            );

                        return memo;
                      },
                      {}
                  );

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                  modelEditingMetadataFieldCollection.models.concat([
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'pattern.type',
                      index: length++,
                      title: mwa.enumeration.Message['SENTENCE_SCHEDULE_PATTERN'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                      valueCollection:
                          new mwa.proui.Backbone.Collection(SCHEDULER_PATTERN_TYPE_ARRAY),
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                      isArray: false,
                      isHidden: false,
                      isRequired: false,
                      isReadOnly: false,
                      metadataFields: metadataFields,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: false,
                      canFilter: false,
                      canResize: true
                    }
                  ])
              );
            }
          }
      );
    })();

    /**
     * The mwa scheduler collection.
     * @constructor
     * @inner
     * @alias mwa.model.scheduler.SchedulerCollection
     * @memberof mwa.model.scheduler
     * @extends {mwa.Collection}
     */
    global.SchedulerCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.scheduler.SchedulerCollection
           */
          {
            url: global.SchedulerModel.prototype.urlRoot,
            model: global.SchedulerModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.scheduler;
}));
