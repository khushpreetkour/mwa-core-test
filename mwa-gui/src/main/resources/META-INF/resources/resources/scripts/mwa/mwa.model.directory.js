/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.fileSystemEntity == null) ? './mwa.model.file-system-entity' : {},
      (mwa.model == null || mwa.model.file == null) ? './mwa.model.file' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.fileSystemEntity == null) ? require('./mwa.model.file-system-entity') : {},
        (mwa.model == null || mwa.model.file == null) ? require('./mwa.model.file') : {}
    );
  } else {
    root.mwa.model.directory = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.fileSystemEntity,
        root.mwa.model.file
    );
  }
}(this, function(base, subBase, fileSystemEntity, file) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.fileSystemEntity namespace.
  if (mwa.model.fileSystemEntity == null) {
    mwa.model.fileSystemEntity = fileSystemEntity;
  }
  // To initialize the mwa.model.file namespace.
  if (mwa.model.file == null) {
    mwa.model.file = file;
  }

  /**
   * The mwa.model.directory namespace.
   * @namespace
   */
  mwa.model.directory = (function() {
    'use strict';

    /**
     * Defines mwa.model.directory alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa directory model.
     * @constructor
     * @inner
     * @alias mwa.model.directory.DirectoryModel
     * @memberof mwa.model.directory
     * @extends {mwa.model.fileSystemEntity.FileSystemEntityModel}
     */
    global.DirectoryModel = (function() {
      var validateDirectories = function(value, attr, computedState) {
        var msg = null;
        // CAUTION:
        // It is necessary to consider about a case that the value is collection because there is a
        // case that this attribute will be updated by tree view to display the relationship.
        value = (value instanceof mwa.model.directory.DirectoryCollection) ?
            value.toJSON() :
            value;
        if (!(mwa.proui.util.Object.isArray(value))) {
          msg = 'The "' + attr + '" must be an array.';
        }
        return msg;
      };

      var validateFiles = function(value, attr, computedState) {
        var msg = null;
        // CAUTION:
        // It is necessary to consider about a case that the value is collection because there is a
        // case that this attribute will be updated by tree view to display the relationship.
        value = (value instanceof mwa.model.file.FileCollection) ?
            value.toJSON() :
            value;
        if (!(mwa.proui.util.Object.isArray(value))) {
          msg = 'The "' + attr + '" must be an array.';
        }
        return msg;
      };

      return mwa.model.fileSystemEntity.FileSystemEntityModel.extend(
          /**
           * @lends mwa.model.directory.DirectoryModel
           */
          {
            alias: 'mwa.model.directory.DirectoryModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'directories' : resource;
              return mwa.model.fileSystemEntity.FileSystemEntityModel.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.model.fileSystemEntity.FileSystemEntityModel.prototype, 'defaults'),
                  {
                    directories: null,
                    files: null,
                    itemGroups: null, // Client Data (same as directories)
                    items: null       // Client Data (same as files)
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.model.fileSystemEntity.FileSystemEntityModel.prototype.validation,
                {
                  directories: [
                    {
                      required: false
                    },
                    {
                      fn: validateDirectories
                    }
                  ],
                  itemGroups: [
                    {
                      required: false
                    },
                    {
                      fn: validateDirectories
                    }
                  ],
                  files: [
                    {
                      required: false
                    },
                    {
                      fn: validateFiles
                    }
                  ],
                  items: [
                    {
                      required: false
                    },
                    {
                      fn: validateFiles
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.model.fileSystemEntity.FileSystemEntityModel.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.model.fileSystemEntity.FileSystemEntityModel.prototype.parse.apply(this, [response, options]);

              // CAUTION: NVX-6149 (https://www.tool.sony.biz/jira/browse/NVX-6149)
              //          NVX-6150 (https://www.tool.sony.biz/jira/browse/NVX-6150)
              // The following attributes are necessary fo displaying the structure using metadata
              // editor.
              response.itemGroups =
                  new mwa.model.directory.DirectoryCollection(
                      response.directories || [],
                      {parse: true}
                  );
              response.items =
                  new mwa.model.file.FileCollection(
                      response.files || [],
                      {parse: true}
                  );

              return response;
            },
            /**
             * @see {@link mwa.model.fileSystemEntity.FileSystemEntityModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.model.fileSystemEntity.FileSystemEntityModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa directory collection.
     * @constructor
     * @inner
     * @alias mwa.model.directory.DirectoryCollection
     * @memberof mwa.model.directory
     * @extends {mwa.model.fileSystemEntity.FileSystemEntityCollection}
     */
    global.DirectoryCollection = (function() {
      return mwa.model.fileSystemEntity.FileSystemEntityCollection.extend(
          /**
           * @lends mwa.model.directory.DirectoryCollection
           */
          {
            url: global.DirectoryModel.prototype.urlRoot,
            model: global.DirectoryModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.directory;
}));
