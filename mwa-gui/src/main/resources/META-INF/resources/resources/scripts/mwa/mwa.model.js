/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.util == null) ? './mwa.util' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.util == null) ? require('./mwa.util') : {}
    );
  } else {
    root.mwa.model = factory(
        root.mwa,
        root.mwa.util
    );
  }
}(this, function(base, util) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }

  // To initialize the mwa.util namespace.
  if (mwa.util == null) {
    mwa.util = util;
  }

  /**
   * The mwa.model namespace.
   * @namespace
   */
  mwa.model = (function() {
    'use strict';

    /**
     * Defines mwa.model alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    return global;
  })();

  return mwa.model;
}));
