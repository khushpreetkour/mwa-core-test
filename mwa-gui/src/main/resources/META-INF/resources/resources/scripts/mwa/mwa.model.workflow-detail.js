/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.workflow == null) ? './mwa.model.workflow' : {},
      (mwa.model == null || mwa.model.activityProfileGroup == null) ? './mwa.model.activity-profile-group' : {},
      (mwa.model == null || mwa.model.activityProfile == null) ? './mwa.model.activity-profile' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.workflow == null) ? require('./mwa.model.workflow') : {},
        (mwa.model == null || mwa.model.activityProfileGroup == null) ? require('./mwa.model.activity-profile-group') : {},
        (mwa.model == null || mwa.model.activityProfile == null) ? require('./mwa.model.activity-profile') : {}
    );
  } else {
    root.mwa.model.workflowDetail = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.workflow,
        root.mwa.model.activityProfileGroup,
        root.mwa.model.activityProfile
    );
  }
}(this, function(base, subBase, workflow, activityProfileGroup, activityProfile) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.workflow namespace.
  if (mwa.model.workflow == null) {
    mwa.model.workflow = workflow;
  }
  // To initialize the mwa.model.activityProfileGroup namespace.
  if (mwa.model.activityProfileGroup == null) {
    mwa.model.activityProfileGroup = activityProfileGroup;
  }
  // To initialize the mwa.model.activityProfile namespace.
  if (mwa.model.activityProfile == null) {
    mwa.model.activityProfile = activityProfile;
  }

  /**
   * The mwa.model.workflowDetail namespace.
   * @namespace
   */
  mwa.model.workflowDetail = (function() {
    'use strict';

    /**
     * Defines mwa.model.workflowDetail alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow detail model.
     * @constructor
     * @inner
     * @alias mwa.model.workflowDetail.WorkflowDetailModel
     * @memberof mwa.model.workflowDetail
     * @extends {mwa.model.workflow.WorkflowModel}
     */
    global.WorkflowDetailModel = (function() {
      return mwa.model.workflow.WorkflowModel.extend(
          /**
           * @lends mwa.model.workflowDetail.WorkflowDetailModel
           */
          {
            alias: 'mwa.model.workflowDetail.WorkflowDetailModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'workflows' : resource;
              return mwa.model.workflow.WorkflowModel.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.model.workflow.WorkflowModel.prototype, 'defaults'),
                  {
                    profileGroups: null,
                    profiles: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.model.workflow.WorkflowModel.prototype.validation,
                {
                  profileGroups: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        // CAUTION:
                        // It is necessary to consider about a case that the value is collection
                        // because this attribute will be updated by workflow navigation tree view
                        // in workflow editor to display the relationship.
                        value =
                            (
                                value instanceof
                                mwa.model.activityProfileGroup.ActivityProfileGroupCollection
                            ) ?
                                value.toJSON() :
                                value;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ],
                  profiles: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        // CAUTION:
                        // It is necessary to consider about a case that the value is collection
                        // because this attribute will be updated by workflow navigation tree view
                        // in workflow editor to display the relationship.
                        value =
                            (
                                value instanceof
                                mwa.model.activityProfile.ActivityProfileCollection
                            ) ?
                                value.toJSON() :
                                value;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.model.workflow.WorkflowModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.model.workflow.WorkflowModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa workflow detail collection.
     * @constructor
     * @inner
     * @alias mwa.model.workflowDetail.WorkflowDetailCollection
     * @memberof mwa.model.workflowDetail
     * @extends {mwa.model.workflow.WorkflowCollection}
     */
    global.WorkflowDetailCollection = (function() {
      return mwa.model.workflow.WorkflowCollection.extend(
          /**
           * @lends mwa.model.workflowDetail.WorkflowDetailCollection
           */
          {
            url: global.WorkflowDetailModel.prototype.urlRoot,
            model: global.WorkflowDetailModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.workflowDetail;
}));
