/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

// INFORMATION:
// The following steps are an overview for adding a new Model.
// 1. Create a new JavaScript file by copying
//    src/main/resources/META-INF/resources/resources/scripts/mwa.model.sample.js.
//  1.1. Replace the name of Namespace, Model and Collection with appropriate one.
//  1.2. Add dependency modules to UMD header.
// 2. Add the path of new JavaScript file to
//    src/main/resources/META-INF/resources/resources/gulpfile.js for build.
//    in consideration of the dependency.

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.sample = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.sample namespace.
   * @namespace
   */
  mwa.model.sample = (function() {
    'use strict';

    /**
     * Defines mwa.model.sample alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa sample model.
     * @constructor
     * @inner
     * @alias mwa.model.sample.SampleModel
     * @memberof mwa.model.sample
     * @extends {mwa.Model}
     */
    global.SampleModel = (function() {
      return mwa.Model.extend(
          /**
           * @lends mwa.model.sample.SampleModel
           */
          {
            alias: 'mwa.model.sample.SampleModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'samples' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
//                    id: ''
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
//                  id: [
//                    {
//                      required: true
//                    }
//                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa sample collection.
     * @constructor
     * @inner
     * @alias mwa.model.sample.SampleCollection
     * @memberof mwa.model.sample
     * @extends {mwa.Collection}
     */
    global.SampleCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.sample.SampleCollection
           */
          {
            url: global.SampleModel.prototype.urlRoot,
            model: global.SampleModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.sample;
}));
