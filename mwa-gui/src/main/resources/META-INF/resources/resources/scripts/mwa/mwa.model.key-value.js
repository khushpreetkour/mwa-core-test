/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.keyValue = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.keyValue namespace.
   * @namespace
   */
  mwa.model.keyValue = (function() {
    'use strict';

    /**
     * Defines mwa.model.keyValue alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa key value model.
     * @constructor
     * @inner
     * @alias mwa.model.keyValue.KeyValueModel
     * @memberof mwa.model.keyValue
     * @extends {mwa.Model}
     */
    global.KeyValueModel = (function() {
      return mwa.Model.extend(
          /**
           * @lends mwa.model.keyValue.KeyValueModel
           */
          {
            alias: 'mwa.model.keyValue.KeyValueModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'key-values' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'key',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    key: '',
                    value: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  key: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  value: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa key value collection.
     * @constructor
     * @inner
     * @alias mwa.model.keyValue.KeyValueCollection
     * @memberof mwa.model.keyValue
     * @extends {mwa.Collection}
     */
    global.KeyValueCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.keyValue.KeyValueCollection
           */
          {
            url: global.KeyValueModel.prototype.urlRoot,
            model: global.KeyValueModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.keyValue;
}));
