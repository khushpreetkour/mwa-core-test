/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.view == null || mwa.view.workflowConnectionPort == null) ? './mwa.view.workflow-connection-port' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.view == null || mwa.view.workflowConnectionPort == null) ? require('./mwa.view.workflow-connection-port') : {}
    );
  } else {
    root.mwa.view.workflowConnection = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.view.workflowConnectionPort
    );
  }
}(this, function(base, subBase, workflowConnectionPort) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.view.workflowConnectionPort namespace.
  if (mwa.view.workflowConnectionPort == null) {
    mwa.view.workflowConnectionPort = workflowConnectionPort;
  }

  /**
   * The mwa.view.workflowConnection namespace.
   * @namespace
   */
  mwa.view.workflowConnection = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowConnection alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow connection view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowConnection.WorkflowConnectionViewModel
     * @memberof mwa.view.workflowConnection
     * @extends {mwa.ViewModel}
     */
    global.WorkflowConnectionViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowConnection.WorkflowConnectionViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    isHidden: false,
                    isSelected: false,
                    canSelect: false,
                    canDrag: false,
                    canDelete: false,
                    fromWorkflowConnectionPortViewModel: null,
                    toWorkflowConnectionPortViewModel: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  isHidden: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isSelected: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canSelect: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDrag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDelete: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  fromWorkflowConnectionPortViewModel: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        var fromWorkflowConnectionPortViewModelType = value.get('type');
                        var toWorkflowConnectionPortViewModelType =
                            (
                                this.get('toWorkflowConnectionPortViewModel') ||
                                new mwa.proui.Backbone.Model({
                                  type:
                                      (fromWorkflowConnectionPortViewModelType ===
                                      mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT) ?
                                          mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT :
                                          mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT
                                })
                            ).get('type');
                        if (fromWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT ||
                            fromWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT) {
                          msg = 'The type of "' + attr + '" must be ' +
                              'mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT or ' +
                              'mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT.';
                        } else if ((fromWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT &&
                            toWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT) ||
                            (fromWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT &&
                            toWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT)) {
                          msg = 'The connection must be established by workflow connection ports ' +
                              'that have appropriate types.';
                        }
                        return msg;
                      }
                    }
                  ],
                  toWorkflowConnectionPortViewModel: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        var toWorkflowConnectionPortViewModelType = value.get('type');
                        var fromWorkflowConnectionPortViewModelType =
                            (
                                this.get('fromWorkflowConnectionPortViewModel') ||
                                new mwa.proui.Backbone.Model({
                                  type:
                                      (toWorkflowConnectionPortViewModelType ===
                                      mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT) ?
                                          mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT :
                                          mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT
                                })
                            ).get('type');
                        if (toWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT ||
                            toWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                          msg = 'The type of "' + attr + '" must be ' +
                              'mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT or ' +
                              'mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT.';
                        } else if ((toWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT &&
                            fromWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT) ||
                            (toWorkflowConnectionPortViewModelType ===
                            mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT &&
                            fromWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT)) {
                          msg = 'The connection must be established by workflow connection ports ' +
                              'that have appropriate types.';
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {boolean} [attributes.isHidden = false]
             * The flag whether workflow connection is hidden.
             * @param {boolean} [attributes.isSelected = false]
             * The flag whether workflow connection is selected.
             * @param {boolean} [attributes.canSelect = false]
             * The flag whether workflow connection is selectable.
             * @param {boolean} [attributes.canDrag = false]
             * The flag whether workflow connection is draggable.
             * @param {boolean} [attributes.canDelete = false]
             * The flag whether workflow connection is deletable.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} [attributes.fromWorkflowConnectionPortViewModel = null]
             * The workflow connection port view model that workflow connection is connected as
             * start point.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} [attributes.toWorkflowConnectionPortViewModel = null]
             * The workflow connection port view model that workflow connection is connected as end
             * point.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, [attributes, options]);

              this.applyWorkflowConnectionPortViewModel(
                  this, this.get('fromWorkflowConnectionPortViewModel'), null
              );
              this.applyWorkflowConnectionPortViewModel(
                  this, this.get('toWorkflowConnectionPortViewModel'), null
              );
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              this.listenToWorkflowConnectionPortViewModel(
                  this,
                  this.get('fromWorkflowConnectionPortViewModel'),
                  null
              );
              this.listenTo(
                  this,
                  'change:fromWorkflowConnectionPortViewModel',
                  this.listenToWorkflowConnectionPortViewModel
              );
              this.listenTo(
                  this,
                  'change:fromWorkflowConnectionPortViewModel',
                  this.applyWorkflowConnectionPortViewModel
              );

              this.listenToWorkflowConnectionPortViewModel(
                  this,
                  this.get('toWorkflowConnectionPortViewModel'),
                  null
              );
              this.listenTo(
                  this,
                  'change:toWorkflowConnectionPortViewModel',
                  this.listenToWorkflowConnectionPortViewModel
              );
              this.listenTo(
                  this,
                  'change:toWorkflowConnectionPortViewModel',
                  this.applyWorkflowConnectionPortViewModel
              );
            },
            /**
             * The event handler for changing fromWorkflowConnectionPortViewModel or
             * toWorkflowConnectionPortViewModel attribute of view model. This function removes
             * listeners from old WorkflowConnectionPortViewModel and adds listeners to new
             * WorkflowConnectionPortViewModel.
             * @protected
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
             * The mwa workflow connection view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowConnectionPortViewModel: function(model, value, options) {
              var valueType = value.get('type');

              // CAUTION:
              // It is important not to stop the listeners for fromWorkflowConnectionPortViewModel
              // and toWorkflowConnectionPortViewModel together because there is a case that an
              // instance of pipelineWorkflowStepViewModelCollection attribute of workflow step view
              // model is same between current and previous corresponding workflow connection port
              // view models. In such case, the listeners for
              // pipelineWorkflowStepViewModelCollection will be stopped in the second call of this
              // function if fromWorkflowConnectionPortViewModel and
              // toWorkflowConnectionPortViewModel are handled together.
              var previousValue =
                  model.previous(
                      (
                          valueType === mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT ||
                          valueType === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT
                      ) ?
                          'fromWorkflowConnectionPortViewModel' :
                          'toWorkflowConnectionPortViewModel'
                  );
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
                this.stopListening(previousValue.get('workflowStepViewModel'));
                this.stopListening(previousValue.get('workflowStepViewModel').get('pipelineWorkflowStepViewModelCollection'));
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);
                this.stopListening(value.get('workflowStepViewModel'));
                this.stopListening(value.get('workflowStepViewModel').get('pipelineWorkflowStepViewModelCollection'));

                this.listenTo(
                    value,
                    'change:isConnected',
                    this.applyIsConnected
                );

                // CAUTION:
                // It isn't necessary to add listeners for workflow step view model in data workflow
                // connection case because they are for updating the pipeline and it should be
                // updated only in control workflow connection case.
                if (valueType === mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT ||
                    valueType === mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT) {
                  this.listenToWorkflowStepViewModel(
                      value,
                      value.get('workflowStepViewModel'),
                      options
                  );
                  this.listenTo(
                      value,
                      'change:workflowStepViewModel',
                      this.listenToWorkflowStepViewModel
                  );

                  // INFORMATION:
                  // It isn't necessary to call applyPipelineWorkflowStepViewModelCollection
                  // function in this processing because necessary processing will be executed by
                  // applyWorkflowConnectionPortViewModel function.
                }
              }
            },
            /**
             * The event handler for changing workflowStepViewModel attribute of workflow connection
             * port view model. This function removes listeners from old workflowStepViewModel and
             * adds listeners to new workflowStepViewModel.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowStepViewModel: function(model, value, options) {
              // CAUTION:
              // This function is been referred by WorkflowConnectionView.

              var previousValue = model.previous('workflowStepViewModel');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
                this.stopListening(previousValue.get('pipelineWorkflowStepViewModelCollection'));
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);
                this.stopListening(value.get('pipelineWorkflowStepViewModelCollection'));

                this.listenToPipelineWorkflowStepViewModelCollection(
                    value,
                    value.get('pipelineWorkflowStepViewModelCollection'),
                    options
                );
                this.listenTo(
                    value,
                    'change:pipelineWorkflowStepViewModelCollection',
                    this.listenToPipelineWorkflowStepViewModelCollection
                );
                this.listenTo(
                    value,
                    'change:pipelineWorkflowStepViewModelCollection',
                    this.applyPipelineWorkflowStepViewModelCollection
                );
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * workflow step view model. This function removes listeners from old
             * pipelineWorkflowStepViewModelCollection and adds listeners to new
             * pipelineWorkflowStepViewModelCollection.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToPipelineWorkflowStepViewModelCollection: function(model, value, options) {
              // INFORMATION:
              // This function is been reused by listenToPipelineWorkflowStepViewModelCollection
              // function of view.

              var previousValue = model.previous('pipelineWorkflowStepViewModelCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'update', this.applyPipelineWorkflowStepViewModelCollection);
              }
            },
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              var json = this.toJSON();
              return {
                fromConnectionPortId: json.fromWorkflowConnectionPortViewModel.get('id'),
                toConnectionPortId: json.toWorkflowConnectionPortViewModel.get('id')
              };
            },
            /**
             * The event handler for changing fromWorkflowConnectionPortViewModel or
             * toWorkflowConnectionPortViewModel attribute of view model. This function updates
             * connection state of the workflow connection port according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
             * The mwa workflow connection view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyWorkflowConnectionPortViewModel: function(model, value, options) {
              var fromWorkflowConnectionPortViewModel =
                  this.get('fromWorkflowConnectionPortViewModel');
              var toWorkflowConnectionPortViewModel =
                  this.get('toWorkflowConnectionPortViewModel');

              // CAUTION:
              // It is necessary to update the state of the previous workflow connection port as
              // disconnected because workflow connection is connected to new workflow connection
              // port instead of previous one.
              var previousValues =
                  mwa.proui._.pick(
                      this.previousAttributes(),
                      // CAUTION:
                      // It is necessary to check whether change event was occurred in order not to
                      // execute the following processing in initialization case because
                      // previousAttributes function returns current attributes if change event have
                      // never been occurred once.
                      (this.hasChanged()) ?
                          [
                            'fromWorkflowConnectionPortViewModel',
                            'toWorkflowConnectionPortViewModel'
                          ] :
                          []
                  );
              var previousValuesLength = mwa.proui._.keys(previousValues).length;
              if (typeof previousValues !== 'undefined' && previousValues != null) {
                for (var propertyName in previousValues) {
                  if (previousValues.hasOwnProperty(propertyName)) {
                    var previousValue = this.previous(propertyName);
                    // CAUTION:
                    // The applyIsConnected function won't be called for previous
                    // WorkflowConnectionPortViewModel because that listener is already stopped in
                    // listenToWorkflowConnectionPortViewModel function before this processing.
                    previousValue.set('isConnected', false);

                    // INFORMATION:
                    // The pipeline should be updated only in control workflow connection case.
                    var previousValueType = previousValue.get('type');
                    if (previousValueType ===
                        mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT ||
                        previousValueType ===
                        mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT) {
                      switch (propertyName) {
                        case 'fromWorkflowConnectionPortViewModel':
                          // CAUTION:
                          // It is necessary to consider about a case that
                          // fromWorkflowConnectionPortViewModel and
                          // toWorkflowConnectionPortViewModel were changed at the same time.
                          (
                              (previousValuesLength === 1) ?
                                  toWorkflowConnectionPortViewModel :
                                  previousValues['toWorkflowConnectionPortViewModel']
                          )
                              .get('workflowStepViewModel')
                              .get('pipelineWorkflowStepViewModelCollection')
                              .remove(
                                  previousValue
                                      .get('workflowStepViewModel')
                                      .get('pipelineWorkflowStepViewModelCollection').models
                                      .concat(
                                          previousValue.get('workflowStepViewModel')
                                      )
                              );
                          break;

                        case 'toWorkflowConnectionPortViewModel':
                          // CAUTION:
                          // It is important not to remove the same pipeline workflow step view
                          // models twice when initialization (previousValuesLength: 0) or
                          // fromWorkflowConnectionPortViewModel and
                          // toWorkflowConnectionPortViewModel were changed at the same time (
                          // previousValuesLength: 2) because the internal counter of pipeline
                          // workflow step view model collection shouldn't be updated unnecessarily.
                          if (previousValuesLength === 1) {
                            previousValue
                                .get('workflowStepViewModel')
                                .get('pipelineWorkflowStepViewModelCollection')
                                .remove(
                                    // INFORMATION:
                                    // It isn't necessary to consider about a case that
                                    // fromWorkflowConnectionPortViewModel and
                                    // toWorkflowConnectionPortViewModel were changed at the same
                                    // time but it is better to implement similar with the above
                                    // fromWorkflowConnectionPortViewModel case to make the code
                                    // maintainable and readable.
                                    (
                                        (previousValuesLength === 1) ?
                                            fromWorkflowConnectionPortViewModel :
                                            previousValues['fromWorkflowConnectionPortViewModel']
                                    )
                                        .get('workflowStepViewModel')
                                        .get('pipelineWorkflowStepViewModelCollection').models
                                        .concat(
                                            fromWorkflowConnectionPortViewModel.get('workflowStepViewModel')
                                        )
                                );
                          }
                          break;

                        default:
                          break;
                      }
                    }
                  }
                }
              }

              if (typeof value !== 'undefined' && value != null) {
                value.set('isConnected', true);

                // INFORMATION:
                // The pipeline should be updated only in control workflow connection case.
                var valueType = value.get('type');
                if (valueType ===
                    mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT ||
                    valueType ===
                    mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT) {
                  switch (value) {
                    case fromWorkflowConnectionPortViewModel:
                      toWorkflowConnectionPortViewModel
                          .get('workflowStepViewModel')
                          .get('pipelineWorkflowStepViewModelCollection')
                          .add(
                              value
                                  .get('workflowStepViewModel')
                                  .get('pipelineWorkflowStepViewModelCollection').models
                                  .concat(
                                      value.get('workflowStepViewModel')
                                  )
                          );
                      break;

                    case toWorkflowConnectionPortViewModel:
                      // CAUTION:
                      // It is important not to add the same pipeline workflow step view models
                      // twice when initialization (previousValuesLength: 0) or
                      // fromWorkflowConnectionPortViewModel and toWorkflowConnectionPortViewModel
                      // were changed at the same time (previousValuesLength: 2) because the
                      // internal counter of pipeline workflow step view model collection shouldn't
                      // be updated unnecessarily.
                      if (previousValuesLength === 1) {
                        value
                            .get('workflowStepViewModel')
                            .get('pipelineWorkflowStepViewModelCollection')
                            .add(
                                fromWorkflowConnectionPortViewModel
                                    .get('workflowStepViewModel')
                                    .get('pipelineWorkflowStepViewModelCollection').models
                                    .concat(
                                        fromWorkflowConnectionPortViewModel.get('workflowStepViewModel')
                                    )
                            );
                      }
                      break;

                    default:
                      break;
                  }
                }
              }
            },
            /**
             * The event handler for changing isConnected attribute of
             * fromWorkflowConnectionPortViewModel or toWorkflowConnectionPortViewModel. This
             * function updates connection state of the workflow connection port according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsConnected: function(model, value, options) {
              if (typeof model !== 'undefined' && model != null) {
                // CAUTION:
                // It is necessary to revert the state of the workflow connection port as connected
                // if any workflow connection is connected because this value is updated by any
                // workflow connection when it is deleted or updated by setting new
                // WorkflowConnectionPortViewModel.
                model.set('isConnected', true);
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * workflow step view model and changing, adding, removing, updating and resetting
             * models in pipelineWorkflowStepViewModelCollection. This function updates pipeline
             * according to the value.
             * @protected
             */
            applyPipelineWorkflowStepViewModelCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:pipelineWorkflowStepViewModelCollection" (model, value, options)
              // pipelineWorkflowStepViewModelCollection
              //  - "change"                                         (model, options)
              //  - "change:[attribute]"                             (model, value, options)
              //  - "add", "remove"                                  (model, collection, options)
              //  - "update", "reset"                                (collection, options)
              var that = this;
              var args = {};

              var fromWorkflowConnectionPortViewModel =
                  that.get('fromWorkflowConnectionPortViewModel');
              var fromWorkflowStepViewModel =
                  fromWorkflowConnectionPortViewModel.get('workflowStepViewModel');
              var fromPipelineWorkflowStepViewModelCollection =
                  fromWorkflowStepViewModel
                      .get('pipelineWorkflowStepViewModelCollection');

              var toWorkflowConnectionPortViewModel =
                  that.get('toWorkflowConnectionPortViewModel');
              var toWorkflowStepViewModel =
                  toWorkflowConnectionPortViewModel.get('workflowStepViewModel');
              var toPipelineWorkflowStepViewModelCollection =
                  toWorkflowStepViewModel
                      .get('pipelineWorkflowStepViewModelCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === fromWorkflowStepViewModel ||
                    arguments[0] === toWorkflowStepViewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                  if (arguments[1] !== fromPipelineWorkflowStepViewModelCollection &&
                      arguments[1] !== toPipelineWorkflowStepViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === fromPipelineWorkflowStepViewModelCollection ||
                    arguments[0] === toPipelineWorkflowStepViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case
                    switch (args.collection) {
                      case fromPipelineWorkflowStepViewModelCollection:
                        // INFORMATION:
                        // It is better to check workflow connection type even if this listener is
                        // only set for control workflow connection.
                        if (fromWorkflowConnectionPortViewModel.get('type') ===
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT &&
                            toWorkflowConnectionPortViewModel.get('type') ===
                            mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT) {
                          toPipelineWorkflowStepViewModelCollection.add(
                              args.options.changes.added
                          );
                          toPipelineWorkflowStepViewModelCollection.remove(
                              args.options.changes.removed
                          );
                        }
                        break;

                      case toPipelineWorkflowStepViewModelCollection:
                        // Do Nothing
                        break;

                      default:
                        break;
                    }
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }
                }
              }
            }
          }
      );
    })();

    /**
     * The mwa workflow connection view model collection.
     * @constructor
     * @inner
     * @alias mwa.view.workflowConnection.WorkflowConnectionViewModelCollection
     * @memberof mwa.view.workflowConnection
     * @extends {mwa.Collection}
     */
    global.WorkflowConnectionViewModelCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.view.workflowConnection.WorkflowConnectionViewModelCollection
           */
          {
            url: global.WorkflowConnectionViewModel.prototype.urlRoot,
            model: global.WorkflowConnectionViewModel,
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              return this.map(
                  function(workflowConnectionViewModel, index, array) {
                    return workflowConnectionViewModel.toObject();
                  }
              );
            }
          }
      );
    })();

    /**
     * The mwa workflow connection view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowConnection.WorkflowConnectionView
     * @memberof mwa.view.workflowConnection
     * @extends {mwa.View}
     */
    global.WorkflowConnectionView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowConnection.WorkflowConnectionView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                // CAUTION:
                // It is important to use div as root element because jQuery cannot add and remove
                // style classes for svg elements.
                //  - jQuery SVG, why can't I addClass?
                //    (http://stackoverflow.com/questions/8638621/jquery-svg-why-cant-i-addclass)
                '<div class="mwa-view-workflow-connection mwa-view-workflow-item">' +
                    '<svg>' +
                        // CAUTION:
                        // It is necessary to prepare two paths that have different stroke width to
                        // easy to select the workflow connection.
                        '<path class="mwa-view-workflow-connection-path-foreground" />' +
                        '<path class="mwa-view-workflow-connection-path-background" />' +
                    '</svg>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  'mouseenter path': 'mouseEnterPath',
                  'mousedown path': 'mouseDownPath',
                  'mouseleave path': 'mouseLeavePath'
                }
            ),
            /**
             * @see {@link mwa.View.initialize}
             * @protected
             * @override
             */
            initialize: function(options, canRender) {
              this._timers = {};

              mwa.View.prototype.initialize.apply(this, [options, false]);
            },
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.listenTo(
                  this.options.viewModel,
                  'change:isHidden',
                  this.applyIsHidden
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:isSelected',
                  this.applyIsSelected
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canSelect',
                  this.applyCanSelect
              );

              this.listenToWorkflowConnectionPortViewModel(
                  this.options.viewModel,
                  this.options.viewModel.get('fromWorkflowConnectionPortViewModel'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:fromWorkflowConnectionPortViewModel',
                  this.listenToWorkflowConnectionPortViewModel
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:fromWorkflowConnectionPortViewModel',
                  this.applyWorkflowConnectionPortViewModel
              );

              this.listenToWorkflowConnectionPortViewModel(
                  this.options.viewModel,
                  this.options.viewModel.get('toWorkflowConnectionPortViewModel'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:toWorkflowConnectionPortViewModel',
                  this.listenToWorkflowConnectionPortViewModel
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:toWorkflowConnectionPortViewModel',
                  this.applyWorkflowConnectionPortViewModel
              );

              this.bindingKeyDownCanvas = mwa.proui._.bind(this.keyDownCanvas, this);
              this.bindingMouseDownCanvas = mwa.proui._.bind(this.mouseDownCanvas, this);
              mwa.proui.Backbone.$('body')
                  // CAUTION:
                  // It is necessary to use on (off) function instead of bind (unbind) function
                  // because the target elements aren't rendered yet at this timing.
                  .off('keydown', '.mwa-view-workflow-canvas', this.bindingKeyDownCanvas)
                  .on('keydown', '.mwa-view-workflow-canvas', this.bindingKeyDownCanvas)
                  .off('mousedown', '.mwa-view-workflow-canvas', this.bindingMouseDownCanvas)
                  .on('mousedown', '.mwa-view-workflow-canvas', this.bindingMouseDownCanvas);
            },
            /**
             * The event handler for changing fromWorkflowConnectionPortViewModel or
             * toWorkflowConnectionPortViewModel attribute of view model. This function removes
             * old listeners and adds new listeners.
             * @protected
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
             * The mwa workflow connection view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowConnectionPortViewModel: function(model, value, options) {
              var valueType = value.get('type');

              // CAUTION:
              // It is important not to stop the listeners for fromWorkflowConnectionPortViewModel
              // and toWorkflowConnectionPortViewModel together because there is a case that an
              // instance of pipelineWorkflowStepViewModelCollection attribute of workflow step view
              // model is same between current and previous corresponding workflow connection port
              // view models. In such case, the listeners for
              // pipelineWorkflowStepViewModelCollection will be stopped in the second call of this
              // function if fromWorkflowConnectionPortViewModel and
              // toWorkflowConnectionPortViewModel are handled together.
              var previousValue =
                  model.previous(
                      (
                          valueType === mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT ||
                          valueType === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT
                      ) ?
                          'fromWorkflowConnectionPortViewModel' :
                          'toWorkflowConnectionPortViewModel'
                  );
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
                this.stopListening(previousValue.get('workflowStepViewModel'));
                this.stopListening(previousValue.get('workflowStepViewModel').get('pipelineWorkflowStepViewModelCollection'));
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);
                this.stopListening(value.get('workflowStepViewModel'));
                this.stopListening(value.get('workflowStepViewModel').get('pipelineWorkflowStepViewModelCollection'));

                this.listenTo(value, 'change:positions', this.applyPositions);
                this.listenTo(value, 'destroy', this.destroy);

                this.listenToWorkflowStepViewModel(
                    value,
                    value.get('workflowStepViewModel'),
                    options
                );
                this.listenTo(
                    value,
                    'change:workflowStepViewModel',
                    this.listenToWorkflowStepViewModel
                );

                // INFORMATION:
                // It isn't necessary to call applyPipelineWorkflowStepViewModelCollection function
                // in this processing because necessary processing will be executed by
                // applyWorkflowConnectionPortViewModel function.
              }
            },
            /**
             * The event handler for changing workflowStepViewModel attribute of workflow connection
             * port view model. This function removes listeners from old workflowStepViewModel and
             * adds listeners to new workflowStepViewModel.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowStepViewModel: function(model, value, options) {
              mwa.view.workflowConnection.WorkflowConnectionViewModel.prototype
                  .listenToWorkflowStepViewModel
                  .apply(this, [model, value, options]);

              if (typeof value !== 'undefined' && value != null) {
                this.listenTo(
                    value,
                    'change:activityInstanceModel',
                    this.applyActivityInstanceModel
                );

                this.applyActivityInstanceModel(
                    value,
                    value.get('activityInstanceModel'),
                    options
                );
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * workflow step view model. This function removes listeners from old
             * pipelineWorkflowStepViewModelCollection and adds listeners to new
             * pipelineWorkflowStepViewModelCollection.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToPipelineWorkflowStepViewModelCollection: function(model, value, options) {
              mwa.view.workflowConnection.WorkflowConnectionViewModel.prototype
                  .listenToPipelineWorkflowStepViewModelCollection
                  .apply(this, [model, value, options]);
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              that.remove({silent: true});
              // INFORMATION:
              // It is important to call remove function instead of empty function for creating this
              // view's element from template.
              that.$el.remove();
              that.setElement(
                  mwa.proui.Backbone.$(
                      // CAUTION: NVXA-1492 (https://acropolis.atlassian.net/browse/NVXA-1492)
                      // It is necessary to parse the template string to create a DOM element
                      // certainly. Actually, jQuery sometimes doesn't create a DOM element
                      // correctly using the template string of this view including svg elements.
                      //  - jQuery 1.9 の $.parseHTML とかその辺
                      //    (http://t-ashula.hateblo.jp/entry/2013/01/23/114105)
                      mwa.proui.Backbone.$.parseHTML(that.template(that.presenter()))
                  )
              );

              that.views = {};

              that.applyIsSelected(
                  that.options.viewModel,
                  that.options.viewModel.get('isSelected'),
                  null
              );

              that.applyCanSelect(
                  that.options.viewModel,
                  that.options.viewModel.get('canSelect'),
                  null
              );

              // CAUTION:
              // It is necessary to hide the element until completing the positioning to avoid
              // flicker. And it is necessary to use visibility style setting for the non-display
              // because internal timer will stop based on whether the width is fixed.
              that.$el.addClass('proui-state-hidden');
              // CAUTION:
              // It is important to clear the timer before updating it in consideration of a case
              // that this processing is executed multiple times during the interval.
              clearInterval(that._timers.render);
              // CAUTION:
              // It is important to call applyPositions function after rendering view's element
              // because the width and height are unknown before rendering.
              that._timers.render = setInterval(function() {
                if (that.$el.width() != null) {
                  clearInterval(that._timers.render);

                  var fromWorkflowConnectionPortViewModel =
                      that.options.viewModel.get('fromWorkflowConnectionPortViewModel');
                  that.applyPositions(
                      fromWorkflowConnectionPortViewModel,
                      fromWorkflowConnectionPortViewModel.get('positions'),
                      null
                  );

                  // CAUTION:
                  // It is necessary to show the element after completing the positioning to avoid
                  // flicker.
                  that.applyIsHidden(
                      that.options.viewModel,
                      that.options.viewModel.get('isHidden'),
                      null
                  );
                }
              }, 100);

              return that;
            },
            /**
             * @see {@link mwa.View.remove}
             * @protected
             * @override
             */
            remove: function(options) {
              if (typeof options === 'undefined' || options == null || !options.silent) {
                // CAUTION:
                // It is important to clear the all timers when remove in order not to execute the
                // unnecessary processing after removal.
                for (var propertyName in this._timers) {
                  if (this._timers.hasOwnProperty(propertyName)) {
                    clearInterval(this._timers[propertyName]);
                  }
                }

                // CAUTION:
                // It is important to stop listeners of view model before updating isConnected
                // attribute of connected WorkflowConnectionPortViewModel not to execute own
                // listener function (applyIsConnected) and update the connection state correctly.
                // And this is also necessary for updating pipeline by exactly the same reason.
                this.options.viewModel.stopListening();

                var fromWorkflowConnectionPortViewModel =
                    this.options.viewModel.get('fromWorkflowConnectionPortViewModel');
                var toWorkflowConnectionPortViewModel =
                    this.options.viewModel.get('toWorkflowConnectionPortViewModel');

                fromWorkflowConnectionPortViewModel.set('isConnected', false);
                toWorkflowConnectionPortViewModel.set('isConnected', false);

                // INFORMATION:
                // The pipeline should be updated only in control workflow connection case.
                if (fromWorkflowConnectionPortViewModel.get('type') ===
                    mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT &&
                    toWorkflowConnectionPortViewModel.get('type') ===
                    mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT) {
                  toWorkflowConnectionPortViewModel
                      .get('workflowStepViewModel')
                      .get('pipelineWorkflowStepViewModelCollection')
                      .remove(
                          fromWorkflowConnectionPortViewModel
                              .get('workflowStepViewModel')
                              .get('pipelineWorkflowStepViewModelCollection').models
                              .concat(
                                  fromWorkflowConnectionPortViewModel.get('workflowStepViewModel')
                              )
                      );
                }
              }

              return mwa.View.prototype.remove.apply(this, [options]);
            },
            /**
             * The event handler of keydown event on canvas.
             * @protected
             *
             * @param {object} event
             * The keydown event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            keyDownCanvas: function(event) {
              var canPropagate = true;

              if (this.$el.closest('.mwa-view-workflow-canvas').is(':focus')) {
                switch (event.keyCode) {
                  case 46: // delete
                    if (this.options.viewModel.get('isSelected') &&
                        this.options.viewModel.get('canDelete')) {
                      this.remove();
                      canPropagate = false;
                    }
                    break;

                  default:
                    break;
                }
              }

              return canPropagate;
            },
            /**
             * The event handler of mousedown event on canvas.
             * @protected
             *
             * @param {object} event
             * The mousedown event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            mouseDownCanvas: function(event) {
              var $target = mwa.proui.Backbone.$(event.target);
              var $currentTarget = mwa.proui.Backbone.$(event.currentTarget);
              var $workflowItem = $target.closest('.mwa-view-workflow-item');

              if ($target.closest(this.$el).length === 0 &&
                  $workflowItem.length !== 0 &&
                  $workflowItem.hasClass('proui-state-selected') &&
                  $currentTarget.find(this.$el).length !== 0) {
                this.options.viewModel.set('isSelected', false);
              }

              return true;
            },
            /**
             * The event handler of mouseenter event on path element.
             * @protected
             *
             * @param {object} event
             * The mouseenter event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            mouseEnterPath: function(event) {
              // CAUTION:
              // Don't use event.button value because the value isn't normalized by jQuery.
              //  - event.which
              //    (https://api.jquery.com/event.which/)
              // It is important to apply the following style class only if any button isn't pressed
              // in consideration of drag scroll on workflow canvas.
              if (event.which === 0) {
                // INFORMATION:
                // It isn't necessary to consider about canSelect attribute because this code just
                // has the same role as hover.
                this.$el.addClass('proui-state-selecting');
              }

              return true;
            },
            /**
             * The event handler of mousedown event on path element.
             * @protected
             *
             * @param {object} event
             * The mousedown event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            mouseDownPath: function(event) {
              // CAUTION:
              // This code means to set isSelected to true only if canSelect is true.
              this.options.viewModel.set('isSelected', this.options.viewModel.get('canSelect'));

              this.$el.closest('.mwa-view-workflow-canvas').focus();

              return true;
            },
            /**
             * The event handler of mouseleave event on path element.
             * @protected
             *
             * @param {object} event
             * The mouseleave event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            mouseLeavePath: function(event) {
              // INFORMATION:
              // It isn't necessary to consider about canSelect attribute because this code just has
              // the same role as hover.
              this.$el.removeClass('proui-state-selecting');

              return true;
            },
            /**
             * The event handler for changing isHidden attribute of view model. This function adds
             * or removes hidden style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
             * The mwa workflow connection view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsHidden: function(model, value, options) {
              var isHidden = this.options.viewModel.get('isHidden');

              this.$el.toggleClass('proui-state-hidden', isHidden);
            },
            /**
             * The event handler for changing isSelected attribute of view model. This function adds
             * or removes selected style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
             * The mwa workflow connection view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsSelected: function(model, value, options) {
              var isSelected = this.options.viewModel.get('isSelected');

              if (isSelected) {
                // CAUTION:
                // It is necessary to trigger mousedown event to deselect any other workflow
                // connection but the event shouldn't be triggered in deselection case because the
                // event listener (mouseDownPath function) basically always set true to isSelected
                // attribute of view model in order not to deselect the workflow connection by GUI
                // operation.
                this.$el.find('path').trigger('mousedown');
              }
              this.$el.toggleClass('proui-state-selected', isSelected);
            },
            /**
             * The event handler for changing canSelect attribute of view model. This function adds
             * or removes selectable style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
             * The mwa workflow connection view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanSelect: function(model, value, options) {
              this.$el.toggleClass(
                  'proui-state-selectable', this.options.viewModel.get('canSelect')
              );
            },
            /**
             * The event handler for changing fromWorkflowConnectionPortViewModel or
             * toWorkflowConnectionPortViewModel attribute of view model. This function updates
             * workflow connection according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
             * The mwa workflow connection view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyWorkflowConnectionPortViewModel: function(model, value, options) {
              if (typeof value !== 'undefined' && value != null) {
                this.applyPositions(value, value.get('positions'), options);
              }
            },
            /**
             * The event handler for changing positions attribute of
             * fromWorkflowConnectionPortViewModel or toWorkflowConnectionPortViewModel. This
             * function moves element positions according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {object} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyPositions: function(model, value, options) {
              var fromWorkflowConnectionPortViewModel =
                  this.options.viewModel.get('fromWorkflowConnectionPortViewModel');
              var toWorkflowConnectionPortViewModel =
                  this.options.viewModel.get('toWorkflowConnectionPortViewModel');

              var startPositions = fromWorkflowConnectionPortViewModel.get('positions');
              var endPositions = toWorkflowConnectionPortViewModel.get('positions');
              var startStepPositions = fromWorkflowConnectionPortViewModel
                  .get('workflowStepViewModel').get('positions');
              var isHorizontal = startStepPositions.x < startPositions.x;

              var $connection = this.$el.find('path');

              if (fromWorkflowConnectionPortViewModel.get('type') ===
                  mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT) {
                var SIGN = (endPositions.y <= startPositions.y) ? -1 : 1;
                // TODO: 絶対値なら WIDTH_LENGTH、 HEIGHT_LENGTH どちらも絶対値に統一する。
                var WIDTH_LENGTH = endPositions.x - startPositions.x;
                var HEIGHT_LENGTH = Math.abs(startPositions.y - endPositions.y);
                var BASE_HORIZONTAL_LINE_THRESHOLD = 20;
                // TODO: 38 と 50 についても意味がわかったら定数化する。
                var pointArray = [];
                var bezierRadius = 10;

                // To draw polygonal line.
                // 以下の処理に出てくる「bezierRadius * 2」と「bezierRadius * 4」について、
                // 「bezierRadius * 2」は、横線にベジェ曲線が１つある場合にベジェ曲線分の横幅
                //                        |
                //                        |
                //      __________________」
                //
                // 「bezierRadius * 4」は、横線にベジェ曲線が２つある場合にベジェ曲線分の横幅
                //     |                    |
                //     |                    |
                //     Ｌ___________________」
                //
                if (startPositions.x + BASE_HORIZONTAL_LINE_THRESHOLD + bezierRadius < endPositions.x) {
                  if (HEIGHT_LENGTH < bezierRadius * 2) {
                    if (4 >= HEIGHT_LENGTH && -4 <= HEIGHT_LENGTH) {
                      // endPoint に収まるため直線表示する。
                      // 4 は、joint 部分のサイズに収まるかの判定に使用している。
                      bezierRadius = 0;
                    } else {
                      // ベジェ曲線の設定値が、10 のままでは高さが足りない場合、
                      // ベジェ曲線の設定値を、描写領域の半分の高さに設定し、曲線を引けるようにする。
                      bezierRadius = Math.floor(HEIGHT_LENGTH / 2);
                    }
                  }
                  if (isHorizontal) {
                    // mwa.proui.util.Log.log('1');
                    var startHorizontalLine = WIDTH_LENGTH - bezierRadius * 4;
                    // ここで BASE_HORIZONTAL_LINE_THRESHOLD * 2 としているのは、
                    // Width の間隔が短い際に真ん中で曲線描写するが、真ん中で折るかどうかを
                    // 判断するための基準として、最初の線と曲がったあとの線を
                    // BASE_HORIZONTAL_LINE_THRESHOLD * 2 で算出して基準としました。
                    if (BASE_HORIZONTAL_LINE_THRESHOLD * 2 + bezierRadius * 2 > WIDTH_LENGTH) {
                      // 横に伸びる線の描写範囲が狭い場合に、真ん中で線を曲げる。
                      startHorizontalLine = Math.floor((WIDTH_LENGTH - bezierRadius * 2) / 2);
                    }
                    pointArray = [
                      'M', startPositions.x, startPositions.y,
                      'l', startHorizontalLine, 0,
                      'q', bezierRadius, 0, bezierRadius, SIGN * bezierRadius,
                      'l', 0, (endPositions.y - startPositions.y) - (SIGN * bezierRadius * 2),
                      'q', 0, SIGN * bezierRadius, bezierRadius, SIGN * bezierRadius,
                      'L', endPositions.x, endPositions.y
                    ];
                  } else {
                    if (startPositions.y > endPositions.y) {
                      // mwa.proui.util.Log.log('2');
                      pointArray = [
                        'M', startPositions.x, startPositions.y,
                        'l', 0, bezierRadius * 2,
                        'q', 0, bezierRadius, bezierRadius, bezierRadius,
                        'l', (endPositions.x - startPositions.x) - bezierRadius * 4, 0,
                        'q', bezierRadius, 0, bezierRadius, SIGN * bezierRadius,
                        'l', 0, SIGN * bezierRadius + (endPositions.y - startPositions.y),
                        'q', 0, SIGN * bezierRadius, bezierRadius, SIGN * bezierRadius,
                        'L', endPositions.x, endPositions.y
                      ];
                    } else {
                      // mwa.proui.util.Log.log('3');
                      // endPoints が、startPoints より下にある場合は、
                      // endPoints まで線をおろして一度曲げる。
                      pointArray = [
                        'M', startPositions.x, startPositions.y,
                        'l', 0, endPositions.y - startPositions.y - bezierRadius,
                        'q', 0, bezierRadius, bezierRadius, bezierRadius,
                        'L', endPositions.x, endPositions.y
                      ];
                    }
                  }
                } else {
                  // TODO: Workflow step の一辺の長さ(64) は、
                  // Vier Model(https://www.tool.sony.biz/confluence/x/TpxwSQ) にて、
                  // width や height といった値で管理するようになった。
                  // 後、ズームイン/アウトを見据えて、width や height を使用したロジックに
                  // 変更する必要がある。
                  if (64 < HEIGHT_LENGTH) {
                    if (isHorizontal) {
                      // mwa.proui.util.Log.log('4');
                      pointArray = [
                        'M', startPositions.x, startPositions.y,
                        'l', BASE_HORIZONTAL_LINE_THRESHOLD, 0,
                        'q', bezierRadius, 0, bezierRadius, SIGN * bezierRadius,
                        'l', 0, (endPositions.y - startPositions.y) / 2 - (SIGN * BASE_HORIZONTAL_LINE_THRESHOLD),
                        'q', 0, SIGN * bezierRadius, -bezierRadius, SIGN * bezierRadius,
                        'l', (endPositions.x - startPositions.x) - 50, 0,
                        'q', -bezierRadius, 0, -bezierRadius, SIGN * bezierRadius,
                        'l', 0, (endPositions.y - startPositions.y) / 2 - (SIGN * BASE_HORIZONTAL_LINE_THRESHOLD),
                        'q', 0, SIGN * bezierRadius, bezierRadius, SIGN * bezierRadius,
                        'L', endPositions.x, endPositions.y
                      ];
                    } else {
                      if (BASE_HORIZONTAL_LINE_THRESHOLD + bezierRadius <= WIDTH_LENGTH) {
                        // mwa.proui.util.Log.log('5');
                        // endPoints まで横線を引く余裕があるため、
                        // 下に線を引いて一度曲げる。
                        pointArray = [
                          'M', startPositions.x, startPositions.y,
                          'l', 0, endPositions.y - startPositions.y - bezierRadius,
                          'q', 0, bezierRadius, bezierRadius, bezierRadius,
                          'L', endPositions.x, endPositions.y
                        ];
                      } else {
                        // mwa.proui.util.Log.log('6');
                        var horizontalLine = WIDTH_LENGTH - (BASE_HORIZONTAL_LINE_THRESHOLD + bezierRadius);
                        if (0 <= WIDTH_LENGTH) {
                          // 左に伸ばす線を算出する。
                          // widthLength の分と endPositions に線を引くために足りない分を追加する。
                          horizontalLine = WIDTH_LENGTH * -1 - (BASE_HORIZONTAL_LINE_THRESHOLD + bezierRadius - WIDTH_LENGTH);
                        }
                        pointArray = [
                          'M', startPositions.x, startPositions.y,
                          'l', 0, (endPositions.y - startPositions.y) / 2 - (SIGN * bezierRadius * 2),
                          'q', 0, SIGN * bezierRadius, -bezierRadius, SIGN * bezierRadius,
                          'l', horizontalLine, 0,
                          'q', -bezierRadius, 0, -bezierRadius, SIGN * bezierRadius,
                          'l', 0, (endPositions.y - startPositions.y) / 2 - (SIGN * bezierRadius),
                          'q', 0, SIGN * bezierRadius, bezierRadius, SIGN * bezierRadius,
                          'L', endPositions.x, endPositions.y
                        ];
                      }
                    }
                  } else {
                    // isDragging について、
                    //   true の場合、startPositions と endPositions が、つながっている状態。
                    //   false の場合は、startPositions から線が引かれはじめ、endPositions に達していない状態。
                    if (!model.get('isDragging') && startPositions.x <= endPositions.x && isHorizontal) {
                      // mwa.proui.util.Log.log('7');
                      // 線を引き始め、
                      // startPositions と endPositions が、近すぎて曲線が引けない距離の場合は、
                      // 直角で曲げ線の終点が endPositions となる手の位置につながるようにする。
                      // startPositions より左側に endPositions の手がある場合、
                      // ここの処理は行わない。
                      pointArray = [
                        'M', startPositions.x, startPositions.y,
                        'l', WIDTH_LENGTH, 0,
                        'L', endPositions.x, endPositions.y
                      ];
                    } else {
                      // mwa.proui.util.Log.log('8');
                      var startBuffer = 0;
                      var endBuffer = 0;
                      if (endPositions.y < startPositions.y) {
                        startBuffer = startPositions.y - endPositions.y;
                      } else {
                        endBuffer = endPositions.y - startPositions.y;
                      }
                      pointArray = [
                        'M', startPositions.x, startPositions.y,
                        'l', BASE_HORIZONTAL_LINE_THRESHOLD, 0,
                        'q', bezierRadius, 0, bezierRadius, -bezierRadius,
                        'l', 0, -38 - startBuffer,
                        'q', 0, -bezierRadius, -bezierRadius, -bezierRadius,
                        'l', (endPositions.x - startPositions.x) - 50, 0,
                        'q', -bezierRadius, 0, -bezierRadius, bezierRadius,
                        'l', 0, 38 + endBuffer,
                        'q', 0, bezierRadius, bezierRadius, bezierRadius,
                        'L', endPositions.x, endPositions.y
                      ];
                    }
                  }
                }

                $connection.attr('d', pointArray.join(' '));
              } else {
                // To draw curved line.
              }
            },
            /**
             * The event handler for changing activityInstanceModel attribute of workflow step view
             * model. This function adds or removes status style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activityInstance.ActivityInstanceModel} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyActivityInstanceModel: function(model, value, options) {
              var fromWorkflowConnectionPortViewModel =
                  this.options.viewModel
                      .get('fromWorkflowConnectionPortViewModel');
              var toWorkflowConnectionPortViewModel =
                  this.options.viewModel
                      .get('toWorkflowConnectionPortViewModel');

              this.$el.toggleClass(
                  'proui-state-success',
                  fromWorkflowConnectionPortViewModel.get('type') ===
                  mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT &&
                  toWorkflowConnectionPortViewModel.get('type') ===
                  mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT &&
                  fromWorkflowConnectionPortViewModel
                      .get('workflowStepViewModel')
                      .has('activityInstanceModel') &&
                  toWorkflowConnectionPortViewModel
                      .get('workflowStepViewModel')
                      .has('activityInstanceModel')
              );
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * workflow step view model and changing, adding, removing, updating and resetting
             * models in pipelineWorkflowStepViewModelCollection. This function updates workflow
             * parameter according to the value.
             * @protected
             */
            applyPipelineWorkflowStepViewModelCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:pipelineWorkflowStepViewModelCollection" (model, value, options)
              // pipelineWorkflowStepViewModelCollection
              //  - "change"                                         (model, options)
              //  - "change:[attribute]"                             (model, value, options)
              //  - "add", "remove"                                  (model, collection, options)
              //  - "update", "reset"                                (collection, options)
              var that = this;
              var args = {};

              var fromWorkflowConnectionPortViewModel =
                  that.options.viewModel.get('fromWorkflowConnectionPortViewModel');
              var fromWorkflowStepViewModel =
                  fromWorkflowConnectionPortViewModel.get('workflowStepViewModel');
              var fromPipelineWorkflowStepViewModelCollection =
                  fromWorkflowStepViewModel
                      .get('pipelineWorkflowStepViewModelCollection');

              var toWorkflowConnectionPortViewModel =
                  that.options.viewModel.get('toWorkflowConnectionPortViewModel');
              var toWorkflowStepViewModel =
                  toWorkflowConnectionPortViewModel.get('workflowStepViewModel');
              var toPipelineWorkflowStepViewModelCollection =
                  toWorkflowStepViewModel
                      .get('pipelineWorkflowStepViewModelCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === fromWorkflowStepViewModel ||
                    arguments[0] === toWorkflowStepViewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                  if (arguments[1] !== fromPipelineWorkflowStepViewModelCollection &&
                      arguments[1] !== toPipelineWorkflowStepViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === fromPipelineWorkflowStepViewModelCollection ||
                    arguments[0] === toPipelineWorkflowStepViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case
                    switch (args.collection) {
                      case fromPipelineWorkflowStepViewModelCollection:
                        // Do Nothing
                        break;

                      case toPipelineWorkflowStepViewModelCollection:
                        // CAUTION:
                        // It is necessary to check workflow connection type because this listener
                        // is set for both control and data workflow connections.
                        if (fromWorkflowConnectionPortViewModel.get('type') ===
                            mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT &&
                            toWorkflowConnectionPortViewModel.get('type') ===
                            mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT) {
                          // INFORMATION:
                          // The data workflow connection should be removed when pipeline is
                          // interrupted.
                          if (!toPipelineWorkflowStepViewModelCollection.contains(fromWorkflowStepViewModel)) {
                            that.remove();
                          }
                        }
                        break;

                      default:
                        break;
                    }
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }
                }
              }
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowConnection;
}));
