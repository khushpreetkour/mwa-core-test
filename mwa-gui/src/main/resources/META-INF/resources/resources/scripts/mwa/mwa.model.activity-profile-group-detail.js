/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.activityProfileGroup == null) ? './mwa.model.activity-profile-group' : {},
      (mwa.model == null || mwa.model.activityProfile == null) ? './mwa.model.activity-profile' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.activityProfileGroup == null) ? require('./mwa.model.activity-profile-group') : {},
        (mwa.model == null || mwa.model.activityProfile == null) ? require('./mwa.model.activity-profile') : {}
    );
  } else {
    root.mwa.model.activityProfileGroupDetail = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.activityProfileGroup,
        root.mwa.model.activityProfile
    );
  }
}(this, function(base, subBase, activityProfileGroup, activityProfile) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.activityProfileGroup namespace.
  if (mwa.model.activityProfileGroup == null) {
    mwa.model.activityProfileGroup = activityProfileGroup;
  }
  // To initialize the mwa.model.activityProfile namespace.
  if (mwa.model.activityProfile == null) {
    mwa.model.activityProfile = activityProfile;
  }

  /**
   * The mwa.model.activityProfileGroupDetail namespace.
   * @namespace
   */
  mwa.model.activityProfileGroupDetail = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityProfileGroupDetail alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa activity profile group detail model.
     * @constructor
     * @inner
     * @alias mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailModel
     * @memberof mwa.model.activityProfileGroupDetail
     * @extends {mwa.model.activityProfileGroup.ActivityProfileGroupModel}
     */
    global.ActivityProfileGroupDetailModel = (function() {
      return mwa.model.activityProfileGroup.ActivityProfileGroupModel.extend(
          /**
           * @lends mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailModel
           */
          {
            alias: 'mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activity-profile-groups' : resource;
              return mwa.model.activityProfileGroup.ActivityProfileGroupModel.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.model.activityProfileGroup.ActivityProfileGroupModel.prototype, 'defaults'),
                  {
                    profiles: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.model.activityProfileGroup.ActivityProfileGroupModel.prototype.validation,
                {
                  profiles: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        // CAUTION:
                        // It is necessary to consider about a case that the value is collection
                        // because this attribute will be updated by workflow navigation tree view
                        // in workflow editor to display the relationship.
                        value =
                            (
                                value instanceof
                                mwa.model.activityProfile.ActivityProfileCollection
                            ) ?
                                value.toJSON() :
                                value;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.model.activityProfileGroup.ActivityProfileGroupModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.model.activityProfileGroup.ActivityProfileGroupModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa activity profile group detail collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailCollection
     * @memberof mwa.model.activityProfileGroupDetail
     * @extends {mwa.model.activityProfileGroup.ActivityProfileGroupCollection}
     */
    global.ActivityProfileGroupDetailCollection = (function() {
      return mwa.model.activityProfileGroup.ActivityProfileGroupCollection.extend(
          /**
           * @lends mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailCollection
           */
          {
            url: global.ActivityProfileGroupDetailModel.prototype.urlRoot,
            model: global.ActivityProfileGroupDetailModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityProfileGroupDetail;
}));
