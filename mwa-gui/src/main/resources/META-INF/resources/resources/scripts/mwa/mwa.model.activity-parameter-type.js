/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.activityParameterTypeValue == null) ? './mwa.model.activity-parameter-type-value' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.activityParameterTypeValue == null) ? require('./mwa.model.activity-parameter-type-value') : {}
    );
  } else {
    root.mwa.model.activityParameterType = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.activityParameterTypeValue
    );
  }
}(this, function(base, subBase, activityParameterTypeValue) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.activityParameterTypeValue namespace.
  if (mwa.model.activityParameterTypeValue == null) {
    mwa.model.activityParameterTypeValue = activityParameterTypeValue;
  }

  /**
   * The mwa.model.activityParameterType namespace.
   * @namespace
   */
  mwa.model.activityParameterType = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityParameterType alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa activity parameter type model.
     * @constructor
     * @inner
     * @alias mwa.model.activityParameterType.ActivityParameterTypeModel
     * @memberof mwa.model.activityParameterType
     * @extends {mwa.Model}
     */
    global.ActivityParameterTypeModel = (function() {
      var activityParameterTypeValueModel =
          new mwa.model.activityParameterTypeValue.ActivityParameterTypeValueModel(
              null, {validate: false}
          );

      var validateActivityParameterTypeValue = function(attributes) {
        var msg = null;

        var validationError =
            activityParameterTypeValueModel.set(
                activityParameterTypeValueModel.parse(
                    // CAUTION:
                    // It is necessary to clone the instance not to change the original values by
                    // parse.
                    mwa.proui.util.Object.extend(true, {}, attributes),
                    null
                )
            ).validate();
        if (typeof validationError === 'string') {
          msg = validationError;
        } else if (typeof validationError === 'object') {
          for (var propertyName in validationError) {
            if (validationError.hasOwnProperty(propertyName)) {
              msg = validationError[propertyName];
              break;
            }
          }
        }

        return msg;
      };

      return mwa.Model.extend(
          /**
           * @lends mwa.model.activityParameterType.ActivityParameterTypeModel
           */
          {
            alias: 'mwa.model.activityParameterType.ActivityParameterTypeModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activity-parameter-types' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    name: null,
                    version: null,
                    values: null,
                    createTime: -1,
                    updateTime: -1
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  version: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  values: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        } else {
                          for (var i = 0; i < value.length; i++) {
                            msg = validateActivityParameterTypeValue(value[i]);
                            if (typeof msg !== 'undefined' && msg != null) {
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  createTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  updateTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa activity parameter type collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityParameterType.ActivityParameterTypeCollection
     * @memberof mwa.model.activityParameterType
     * @extends {mwa.Collection}
     */
    global.ActivityParameterTypeCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activityParameterType.ActivityParameterTypeCollection
           */
          {
            url: global.ActivityParameterTypeModel.prototype.urlRoot,
            model: global.ActivityParameterTypeModel,
            comparator: 'name'
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityParameterType;
}));
