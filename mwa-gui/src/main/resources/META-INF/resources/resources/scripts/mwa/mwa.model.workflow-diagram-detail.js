/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.workflowDiagram == null) ? './mwa.model.workflow-diagram' : {},
      (mwa.model == null || mwa.model.workflow == null) ? './mwa.model.workflow' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.workflowDiagram == null) ? require('./mwa.model.workflow-diagram') : {},
        (mwa.model == null || mwa.model.workflow == null) ? require('./mwa.model.workflow') : {}
    );
  } else {
    root.mwa.model.workflowDiagramDetail = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.workflowDiagram,
        root.mwa.model.workflow
    );
  }
}(this, function(base, subBase, workflowDiagram, workflow) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.workflowDiagram namespace.
  if (mwa.model.workflowDiagram == null) {
    mwa.model.workflowDiagram = workflowDiagram;
  }
  // To initialize the mwa.model.workflow namespace.
  if (mwa.model.workflow == null) {
    mwa.model.workflow = workflow;
  }

  /**
   * The mwa.model.workflowDiagramDetail namespace.
   * @namespace
   */
  mwa.model.workflowDiagramDetail = (function() {
    'use strict';

    /**
     * Defines mwa.model.workflowDiagramDetail alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow diagram detail model.
     * @constructor
     * @inner
     * @alias mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel
     * @memberof mwa.model.workflowDiagramDetail
     * @extends {mwa.model.workflowDiagram.WorkflowDiagramModel}
     */
    global.WorkflowDiagramDetailModel = (function() {
      return mwa.model.workflowDiagram.WorkflowDiagramModel.extend(
          /**
           * @lends mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel
           */
          {
            alias: 'mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'workflow-diagrams' : resource;
              return mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.model.workflowDiagram.WorkflowDiagramModel.prototype, 'defaults'),
                  {
                    workflows: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.validation,
                {
                  workflows: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        // CAUTION:
                        // It is necessary to consider about a case that the value is collection
                        // because this attribute will be updated by workflow navigation tree view
                        // in workflow editor to display the relationship.
                        value =
                            (
                                value instanceof
                                mwa.model.workflow.WorkflowCollection
                            ) ?
                                value.toJSON() :
                                value;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.model.workflowDiagram.WorkflowDiagramModel.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.parse.apply(this, [response, options]);

              if (mwa.proui.util.Object.isArray(response.workflows)) {
                response.workflows =
                    response.workflows.map(
                        // INFORMATION:
                        // The first argument is object type, not array. But we use plural name
                        // based on our JavaScript coding rule.
                        function(workflows, index, array) {
                          return mwa.model.workflow.WorkflowModel.prototype.parse(
                              workflows, options
                          );
                        }
                    );
              }

              return response;
            },
            /**
             * @see {@link mwa.model.workflowDiagram.WorkflowDiagramModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa workflow diagram detail collection.
     * @constructor
     * @inner
     * @alias mwa.model.workflowDiagramDetail.WorkflowDiagramDetailCollection
     * @memberof mwa.model.workflowDiagramDetail
     * @extends {mwa.model.workflowDiagram.WorkflowDiagramCollection}
     */
    global.WorkflowDiagramDetailCollection = (function() {
      return mwa.model.workflowDiagram.WorkflowDiagramCollection.extend(
          /**
           * @lends mwa.model.workflowDiagramDetail.WorkflowDiagramDetailCollection
           */
          {
            url: global.WorkflowDiagramDetailModel.prototype.urlRoot,
            model: global.WorkflowDiagramDetailModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.workflowDiagramDetail;
}));
