/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.fileSystemEntity == null) ? './mwa.model.file-system-entity' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.fileSystemEntity == null) ? require('./mwa.model.file-system-entity') : {}
    );
  } else {
    root.mwa.model.file = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.fileSystemEntity
    );
  }
}(this, function(base, subBase, fileSystemEntity) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.fileSystemEntity namespace.
  if (mwa.model.fileSystemEntity == null) {
    mwa.model.fileSystemEntity = fileSystemEntity;
  }

  /**
   * The mwa.model.file namespace.
   * @namespace
   */
  mwa.model.file = (function() {
    'use strict';

    /**
     * Defines mwa.model.file alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa file model.
     * @constructor
     * @inner
     * @alias mwa.model.file.FileModel
     * @memberof mwa.model.file
     * @extends {mwa.model.fileSystemEntity.FileSystemEntityModel}
     */
    global.FileModel = (function() {
      return mwa.model.fileSystemEntity.FileSystemEntityModel.extend(
          /**
           * @lends mwa.model.file.FileModel
           */
          {
            alias: 'mwa.model.file.FileModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'files' : resource;
              return mwa.model.fileSystemEntity.FileSystemEntityModel.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.model.fileSystemEntity.FileSystemEntityModel.prototype, 'defaults'),
                  {
//                    id: ''
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.model.fileSystemEntity.FileSystemEntityModel.prototype.validation,
                {
//                  id: [
//                    {
//                      required: true
//                    }
//                  ]
                }
            ),
            /**
             * @see {@link mwa.model.fileSystemEntity.FileSystemEntityModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.model.fileSystemEntity.FileSystemEntityModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa file collection.
     * @constructor
     * @inner
     * @alias mwa.model.file.FileCollection
     * @memberof mwa.model.file
     * @extends {mwa.model.fileSystemEntity.FileSystemEntityCollection}
     */
    global.FileCollection = (function() {
      return mwa.model.fileSystemEntity.FileSystemEntityCollection.extend(
          /**
           * @lends mwa.model.file.FileCollection
           */
          {
            url: global.FileModel.prototype.urlRoot,
            model: global.FileModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.file;
}));
