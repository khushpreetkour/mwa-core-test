/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.model == null || mwa.model.activityProfile == null) ? './mwa.model.activity-profile' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.model == null || mwa.model.activityProfile == null) ? require('./mwa.model.activity-profile') : {}
    );
  } else {
    root.mwa.view.activityProfileListView = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.model.activityProfile
    );
  }
}(this, function(base, subBase, activityProfile) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.model.activity namespace.
  if (mwa.model.activityProfile == null) {
    mwa.model.activityProfile = activityProfile;
  }

  /**
   * The mwa.view.activityProfileListView namespace.
   * @namespace
   */
  mwa.view.activityProfileListView = (function() {
    'use strict';

    /**
     * Defines mwa.view.activityProfileListView alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa activity profile list view view model.
     * @constructor
     * @inner
     * @alias mwa.view.activityProfileListView.ActivityProfileListViewViewModel
     * @memberof mwa.view.activityProfileListView
     * @extends {prouiExt.metadataEditorListView.MetadataEditorListViewViewModel}
     */
    global.ActivityProfileListViewViewModel = (function() {
      return mwa.prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.extend(
          /**
           * @lends mwa.view.activityProfileListView.ActivityProfileListViewViewModel
           */
          {
            defaults: function() {
              var activityProfileModel =
                  new mwa.model.activityProfile.ActivityProfileModel(null, {validate: false});

              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(
                      mwa.prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.prototype,
                      'defaults'
                  ),
                  {
                    // prouiExt.metadataEditorListView.MetadataEditorListViewViewModel
                    canMultiSelect: false, // TODO: To change to true after preparing PATCH API.
                    itemCollection:
                        new mwa.model.activityProfile.ActivityProfileCollection(),
                    selectedItemCollection:
                        new mwa.model.activityProfile.ActivityProfileCollection(),
                    metadataFieldCollection:
                        activityProfileModel.createEditingMetadataFieldCollection(),
                    metadataGroupCollection:
                        activityProfileModel.createEditingMetadataGroupCollection(),
                    // mwa.view.activityProfileListView.ActivityProfileListViewViewModel
                    activityModel: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.prototype.validation,
                {
                  // prouiExt.metadataEditorListView.MetadataEditorListViewViewModel
                  itemCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityProfile.ActivityProfileCollection
                    }
                  ],
                  selectedItemCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityProfile.ActivityProfileCollection
                    }
                  ],
                  // mwa.view.activityProfileListView.ActivityProfileListViewViewModel
                  activityModel: [
                    {
                      required: false
                    },
                    {
                      instance: mwa.model.activity.ActivityModel
                    }
                  ]
                }
            ),
            /**
             * @see {@link prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {mwa.model.activity.ActivityModel} [attributes.activityModel = null]
             * The activity model to be used for creating filter and header information.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.prototype.initialize.apply(this, [attributes, options]);

              this.applyActivityModel(this, this.get('activityModel'), null);
            },
            /**
             * @see {@link prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.prototype.listen.apply(this, []);

              this.listenTo(this, 'change:activityModel', this.applyActivityModel);
            },
            /**
             * @see {@link prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.save}
             * @public
             * @override
             */
            save: function(itemAttributes, collection) {
              collection.invoke('set', {templateId: this.get('activityModel').get('id')});

              return mwa.prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.prototype.save.apply(this, [itemAttributes, collection]);
            },
            /**
             * @see {@link prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.createListViewHeaderArray}
             * @public
             * @override
             */
            createListViewHeaderArray: function(metadataFieldCollection) {
              var inputActivityParameterCollection =
                  new mwa.model.activityParameter.ActivityParameterCollection(
                      (
                          this.get('activityModel') ||
                          new mwa.model.activity.ActivityModel(null, {validate: false})
                      ).get('inputs') || [],
                      {parse: true}
                  );

              return mwa.prouiExt.metadataEditorListView.MetadataEditorListViewViewModel.prototype
                  .createListViewHeaderArray.apply(this, [metadataFieldCollection]).map(
                      // INFORMATION:
                      // The first argument is object type, not array. But we use plural name based
                      // on our JavaScript coding rule.
                      function(headers, index, array) {
                        var inputActivityParameterModel =
                            inputActivityParameterCollection.get(
                                // CAUTION: NVX-6939 (https://www.tool.sony.biz/jira/browse/NVX-6939)
                                // It is important to remove the head string only in consideration
                                // of a case that the title contains the same string at the middle.
                                headers.key.replace(/^parameter\./, '')
                            );

                        return mwa.proui._.extend(
                            headers,
                            {
                              label:
                                  // CAUTION: NVX-6939 (https://www.tool.sony.biz/jira/browse/NVX-6939)
                                  // It is important to check whether the activity parameter is
                                  // required to remove special required mark in consideration of a
                                  // case that the title contains the same string at the end.
                                  (
                                      // CAUTION: NVX-6939 (https://www.tool.sony.biz/jira/browse/NVX-6939)
                                      // It is important to cache and use input activity parameter
                                      // model instead of dummy model in consideration of default
                                      // value setting for required attribute of activity parameter
                                      // model.
                                      inputActivityParameterModel instanceof
                                      mwa.model.activityParameter.ActivityParameterModel &&
                                      // CAUTION: NVX-6939 (https://www.tool.sony.biz/jira/browse/NVX-6939)
                                      // It is necessary to refer to original activity parameter
                                      // setting instead of the same in the specified metadata filed
                                      // model because the required setting is always false for
                                      // activity profile.
                                      inputActivityParameterModel.get('required')
                                  ) ?
                                      // CAUTION: NVX-6939 (https://www.tool.sony.biz/jira/browse/NVX-6939)
                                      // It is important to remove the trailing special required
                                      // mark only in consideration of a case that the title
                                      // contains the same string at the middle.
                                      headers.label.replace(
                                          mwa.model.activityProfile.SPECIAL_REQUIRED_MARKS.regExp,
                                          ''
                                      ) :
                                      headers.label
                            }
                        );
                      }
                  );
            },
            /**
             * The event handler for changing activityModel attribute of view model. This function
             * updates hasAddButton, hasEditButton, hasDeleteButton, hasRefreshButton, filterArray
             * and metadataFieldCollection attributes of view model according to the value.
             * @protected
             *
             * @param {mwa.view.activityProfileListView.ActivityProfileListViewViewModel} model
             * The mwa activity profile list view view model.
             * @param {mwa.model.activity.ActivityModel} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyActivityModel: function(model, value, options) {
              var activityModel = this.get('activityModel');
              var isProcessingActivity = mwa.util.WorkflowStep.isProcessing(activityModel);

              this.set({
                hasAddButton: isProcessingActivity,
                hasEditButton: isProcessingActivity,
                hasDeleteButton: isProcessingActivity,
                hasRefreshButton: isProcessingActivity,
                filterArray: (isProcessingActivity) ?
                    ['templateId' + '==' + activityModel.get('id')] :
                    // CAUTION:
                    // This setting is for not fetching any activity profile.
                    ['id' + '=='],
                metadataFieldCollection:
                    new mwa.model.activityProfile.ActivityProfileModel(
                        {
                          parameter: (isProcessingActivity) ?
                              activityModel.getParameter() :
                              null,
                          // CAUTION:
                          // It is necessary to pick the attributes only regarding activity because
                          // stack overflow will be happened in the processing for flattening values
                          // of metadata editor if there are additional attributes (e.g. workflow).
                          activity: (isProcessingActivity) ?
                              mwa.proui._.pick(
                                  activityModel.toJSON(),
                                  mwa.proui._.keys(
                                      mwa.proui._.result(
                                          mwa.model.activity.ActivityModel.prototype,
                                          'defaults'
                                      )
                                  )
                              ) :
                              null
                        },
                        {validate: false}
                    ).createEditingMetadataFieldCollection()
              });
            }
          }
      );
    })();

    /**
     * The mwa activity profile list view view.
     * @constructor
     * @inner
     * @alias mwa.view.activityProfileListView.ActivityProfileListViewView
     * @memberof mwa.view.activityProfileListView
     * @extends {prouiExt.metadataEditorListView.MetadataEditorListViewView}
     */
    global.ActivityProfileListViewView = (function() {
      return mwa.prouiExt.metadataEditorListView.MetadataEditorListViewView.extend(
          /**
           * @lends mwa.view.activityProfileListView.ActivityProfileListViewView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.prouiExt.metadataEditorListView.MetadataEditorListViewView.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            // CAUTION:
            // It is better to prepare a new function instance not to affect the original instance.
            template: mwa.proui._.bind(
                mwa.prouiExt.metadataEditorListView.MetadataEditorListViewView.prototype.template,
                {} // dummy
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.prouiExt.metadataEditorListView.MetadataEditorListViewView.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link prouiExt.metadataEditorListView.MetadataEditorListViewView.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.prouiExt.metadataEditorListView.MetadataEditorListViewView.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link prouiExt.metadataEditorListView.MetadataEditorListViewView.render}
             * @public
             * @override
             */
            render: function() {
              mwa.prouiExt.metadataEditorListView.MetadataEditorListViewView.prototype.render.apply(this, []);

              // If you need new sub views, add here.

              return this;
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.activityProfileListView;
}));
