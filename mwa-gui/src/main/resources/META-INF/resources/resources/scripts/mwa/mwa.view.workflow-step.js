/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.model == null || mwa.model.activity == null) ? './mwa.model.activity' : {},
      (mwa.model == null || mwa.model.activityParameter == null) ? './mwa.model.activity-parameter' : {},
      (mwa.model == null || mwa.model.activityInstance == null) ? './mwa.model.activity-instance' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.model == null || mwa.model.activity == null) ? require('./mwa.model.activity') : {},
        (mwa.model == null || mwa.model.activityParameter == null) ? require('./mwa.model.activity-parameter') : {},
        (mwa.model == null || mwa.model.activityInstance == null) ? require('./mwa.model.activity-instance') : {}
    );
  } else {
    root.mwa.view.workflowStep = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.model.activity,
        root.mwa.model.activityParameter,
        root.mwa.model.activityInstance
    );
  }
}(this, function(base, subBase, activity, activityParameter, activityInstance) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.model.activity namespace.
  if (mwa.model.activity == null) {
    mwa.model.activity = activity;
  }
  // To initialize the mwa.model.activityParameter namespace.
  if (mwa.model.activityParameter == null) {
    mwa.model.activityParameter = activityParameter;
  }
  // To initialize the mwa.model.activityInstance namespace.
  if (mwa.model.activityInstance == null) {
    mwa.model.activityInstance = activityInstance;
  }

  /**
   * The mwa.view.workflowStep namespace.
   * @namespace
   */
  mwa.view.workflowStep = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowStep alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow step view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowStep.WorkflowStepViewModel
     * @memberof mwa.view.workflowStep
     * @extends {mwa.ViewModel}
     */
    global.WorkflowStepViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowStep.WorkflowStepViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    id: '',
                    isReadOnly: true,
                    isDragging: false,
                    isSelected: false,
                    isExpanded: false,
                    canSelect: false,
                    canDrag: false,
                    canDelete: false,
                    text: '',
                    positions: null,
                    width: 96,
                    height: 96,
                    offset: 16,
                    activityModel: null,
                    activityInstanceModel: null,
                    inputActivityParameterCollection:
                        new mwa.model.activityParameter.ActivityParameterCollection(),
                    outputActivityParameterCollection:
                        new mwa.model.activityParameter.ActivityParameterCollection(),
                    pipelineWorkflowStepViewModelCollection:
                        new mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection(),
                    select: null,
                    execute: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  // proui.ViewModel
                  id: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  // mwa.view.workflowStep.WorkflowStepViewModel
                  isReadOnly: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isDragging: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isSelected: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isExpanded: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canSelect: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDrag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDelete: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  text: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  positions: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isPlainObject(value))) {
                          msg = 'The "' + attr + '" must be object.';
                        } else if (value.x == null || value.y == null || value.z == null) {
                          msg = 'The "' + attr + '" must have properties (x, y, z).';
                        }
                        return msg;
                      }
                    }
                  ],
                  width: [
                    {
                      required: true
                    },
                    {
                      // TODO: To be changeable.
                      range: [96, 96]
                    },
                    {
                      type: 'number'
                    }
                  ],
                  height: [
                    {
                      required: true
                    },
                    {
                      // TODO: To be changeable.
                      range: [96, 96]
                    },
                    {
                      type: 'number'
                    }
                  ],
                  offset: [
                    {
                      required: true
                    },
                    {
                      // TODO: To be changeable.
                      range: [16, 16]
                    },
                    {
                      type: 'number'
                    }
                  ],
                  activityModel: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activity.ActivityModel
                    }
                  ],
                  activityInstanceModel: [
                    {
                      required: false
                    },
                    {
                      instance: mwa.model.activityInstance.ActivityInstanceModel
                    }
                  ],
                  inputActivityParameterCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityParameter.ActivityParameterCollection
                    }
                  ],
                  outputActivityParameterCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityParameter.ActivityParameterCollection
                    }
                  ],
                  pipelineWorkflowStepViewModelCollection: [
                    {
                      required: true
                    },
                    {
                      instance: global.PipelineWorkflowStepViewModelCollection
                    }
                  ],
                  select: [
                    {
                      required: false
                    },
                    {
                      type: 'function'
                    }
                  ],
                  execute: [
                    {
                      required: false
                    },
                    {
                      type: 'function'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {string} [attributes.id = '']
             * The ID of workflow step.
             * @param {boolean} [attributes.isReadOnly = true]
             * The flag whether workflow step is read only.
             * @param {boolean} [attributes.isDragging = false]
             * The flag whether workflow step is been dragging.
             * @param {boolean} [attributes.isSelected = false]
             * The flag whether workflow step is selected.
             * @param {boolean} [attributes.isExpanded = false]
             * The flag whether workflow step is expanded.
             * @param {boolean} [attributes.canSelect = false]
             * The flag whether workflow step is selectable.
             * @param {boolean} [attributes.canDrag = false]
             * The flag whether workflow step is draggable.
             * @param {boolean} [attributes.canDelete = false]
             * The flag whether workflow step is deletable.
             * @param {string} [attributes.text = '']
             * The string to be displayed as workflow step name.
             * @param {object} [attributes.positions = null]
             * The position of workflow step.
             * @param {number} [attributes.width = 96]
             * The width of entire workflow step element.
             * @param {number} [attributes.height = 96]
             * The height of entire workflow step element.
             * @param {number} [attributes.offset = 16]
             * The offset between entire workflow step element and actual workflow step element.
             * @param {mwa.model.activity.ActivityModel} [attributes.activityModel = null]
             * The activity model corresponding to workflow step.
             * @param {mwa.model.activityInstance.ActivityInstanceModel} [attributes.activityInstanceModel = null]
             * The activity instance model corresponding to workflow step.
             * @param {mwa.model.activityParameter.ActivityParameterCollection} [attributes.inputActivityParameterCollection = new mwa.model.activityParameter.ActivityParameterCollection()]
             * The activity parameter collection corresponding to input of specified activity.
             * @param {mwa.model.activityParameter.ActivityParameterCollection} [attributes.outputActivityParameterCollection = new mwa.model.activityParameter.ActivityParameterCollection()]
             * The activity parameter collection corresponding to output of specified activity.
             * @param {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} [attributes.pipelineWorkflowStepViewModelCollection = new mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection()]
             * The workflow step view model collection for achieving pipeline.
             * @param {function} [attributes.select = null]
             * The callback function. This function is called when workflow step is selected or
             * deselected.
             * @param {function} [attributes.execute = null]
             * The callback function. This function is called when workflow step is double clicked
             * or double tapped.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, [attributes, options]);

              for (var propertyName in attributes) {
                if (attributes.hasOwnProperty(propertyName)) {
                  if ((propertyName === 'select' || propertyName === 'execute') &&
                      mwa.proui._.isFunction(attributes[propertyName])) {
                    this[propertyName] = attributes[propertyName];
                  }
                }
              }

              this.applyActivityModel(this, this.get('activityModel'), null);
              this.applyActivityInstanceModel(this, this.get('activityInstanceModel'), null);
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              this.listenTo(
                  this,
                  'change:activityModel',
                  this.applyActivityModel
              );

              this.listenTo(
                  this,
                  'change:activityInstanceModel',
                  this.applyActivityInstanceModel
              );

              this.listenToActivityParameterCollection(
                  this,
                  this.get('inputActivityParameterCollection'),
                  null
              );
              this.listenTo(
                  this,
                  'change:inputActivityParameterCollection',
                  this.listenToActivityParameterCollection
              );
              this.listenTo(
                  this,
                  'change:inputActivityParameterCollection',
                  this.applyActivityParameterCollection
              );

              this.listenToActivityParameterCollection(
                  this,
                  this.get('outputActivityParameterCollection'),
                  null
              );
              this.listenTo(
                  this,
                  'change:outputActivityParameterCollection',
                  this.listenToActivityParameterCollection
              );
              this.listenTo(
                  this,
                  'change:outputActivityParameterCollection',
                  this.applyActivityParameterCollection
              );
            },
            /**
             * The event handler for changing inputActivityParameterCollection or
             * outputActivityParameterCollection attribute of view model. This function removes
             * listeners from old ActivityParameterCollection and adds listeners to new
             * ActivityParameterCollection.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activityParameter.ActivityParameterCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToActivityParameterCollection: function(model, value, options) {
              var previousValue =
                  model.previous(
                      (value === this.get('inputActivityParameterCollection')) ?
                          'inputActivityParameterCollection' :
                          'outputActivityParameterCollection'
                  );
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'update', this.applyActivityParameterCollection);
                this.listenTo(value, 'reset', this.applyActivityParameterCollection);
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * view model. This function removes listeners from old
             * pipelineWorkflowStepViewModelCollection and adds listeners to new
             * pipelineWorkflowStepViewModelCollection.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToPipelineWorkflowStepViewModelCollection: function(model, value, options) {
              var previousValue = model.previous('pipelineWorkflowStepViewModelCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'update', this.applyPipelineWorkflowStepViewModelCollection);
                this.listenTo(value, 'reset', this.applyPipelineWorkflowStepViewModelCollection);
              }
            },
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              var json = this.toJSON();
              return {
                id: json.id,
                name: json.text,
                // INFORMATION:
                // To be careful about attribute name.
                position: json.positions,
                activity: json.activityModel.toJSON()
              };
            },
            /**
             * The callback function. This function is called when workflow step is selected or
             * deselected.
             * @public
             *
             * @param {mwa.view.workflowStep.WorkflowStepView} workflowStepView
             * The workflow step view that is selected or deselected.
             * @param {!boolean} isSelected
             * The flag whether workflow step is selected.
             */
            select: function(workflowStepView, isSelected) {
            },
            /**
             * The callback function. This function is called when workflow step is double clicked
             * or double tapped.
             * @public
             *
             * @param {mwa.view.workflowStep.WorkflowStepView} workflowStepView
             * The workflow step view that is double clicked or double tapped.
             */
            execute: function(workflowStepView) {
            },
            /**
             * The function to create activity instance tooltip model.
             * @public
             *
             * @returns {Backbone.Model}
             * The created activity instance tooltip model instance.
             */
            createActivityInstanceTooltipModel: function() {
              var that = this;

              return new mwa.proui.Backbone.Model({
                tooltip: function(viewModel) {
                  var activityInstanceModel = that.get('activityInstanceModel');

                  return (
                      (
                          activityInstanceModel instanceof
                          mwa.model.activityInstance.ActivityInstanceModel
                      ) ?
                          mwa.proui._.pairs(
                              mwa.proui._.object(
                                  [
                                    mwa.enumeration.Message['SENTENCE_NAME'],
                                    mwa.enumeration.Message['SENTENCE_ID'],
                                    mwa.enumeration.Message['SENTENCE_START_DATE_TIME'],
                                    mwa.enumeration.Message['SENTENCE_END_DATE_TIME'],
                                    mwa.enumeration.Message['SENTENCE_PROGRESS'],
                                    mwa.enumeration.Message['SENTENCE_DETAIL_PLURAL']
                                  ],
                                  [
                                    that.get('text'),
                                    activityInstanceModel.get('id'),
                                    mwa.proui.util.Date.formatDate(
                                        new Date(activityInstanceModel.get('startTime'))
                                    ),
                                    (activityInstanceModel.get('status').terminated) ?
                                        mwa.proui.util.Date.formatDate(
                                            new Date(activityInstanceModel.get('endTime'))
                                        ) :
                                        null,
                                    activityInstanceModel.get('progress') + ' %',
                                    (
                                        activityInstanceModel.get('statusDetails') ||
                                        {}
                                    ).deviceResponseDetails
                                  ]
                              )
                          ).reduce(
                              function(memo, keyValueArray, index, array) {
                                return (
                                    memo +
                                    keyValueArray[0] + ' : ' + (keyValueArray[1] || '') +
                                    '\n'
                                );
                              },
                              ''
                          ) :
                          null
                  );
                }
              });
            },
            /**
             * The event handler for changing activityModel attribute of view model. This function
             * updates input and output activity parameter collection according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activity.ActivityModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyActivityModel: function(model, value, options) {
              var activityModel =
                  this.get('activityModel') ||
                  new mwa.model.activity.ActivityModel(null, {validate: false});
              var pipelineWorkflowStepViewModelCollection =
                  this.get('pipelineWorkflowStepViewModelCollection');

              if (pipelineWorkflowStepViewModelCollection instanceof
                  mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection) {
                this.stopListening(pipelineWorkflowStepViewModelCollection);
              }

              // INFORMATION:
              // It is better not to add unnecessary listeners in consideration of performance.
              if (activityModel.get('name') ===
                  mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH.templateName) {
                this.listenToPipelineWorkflowStepViewModelCollection(
                    this,
                    pipelineWorkflowStepViewModelCollection,
                    null
                );
                this.listenTo(
                    this,
                    'change:pipelineWorkflowStepViewModelCollection',
                    this.listenToPipelineWorkflowStepViewModelCollection
                );
                this.listenTo(
                    this,
                    'change:pipelineWorkflowStepViewModelCollection',
                    this.applyPipelineWorkflowStepViewModelCollection
                );
              }

              [
                {
                  activityParameterCollection: this.get('inputActivityParameterCollection'),
                  parameterAttributeName: 'inputs'
                },
                {
                  activityParameterCollection: this.get('outputActivityParameterCollection'),
                  parameterAttributeName: 'outputs'
                }
              ].forEach(
                  // INFORMATION:
                  // The first argument is object type, not array. But we use plural name based on
                  // our JavaScript coding rule.
                  function(properties, index, array) {
                    properties.activityParameterCollection.reset(
                        // CAUTION:
                        // It is necessary to clone the instances not to change the original values
                        // by parse.
                        (activityModel.get(properties.parameterAttributeName) || []).map(
                            // INFORMATION:
                            // The first argument is object type, not array. But we use plural name
                            // based on our JavaScript coding rule.
                            function(parameters, index, array) {
                              return mwa.proui.util.Object.extend(true, {}, parameters);
                            }
                        ),
                        {parse: true}
                    );
                  }
              );
            },
            /**
             * The event handler for changing activityInstanceModel attribute of view model. This
             * function updates input and output activity parameter collection according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activityInstance.ActivityInstanceModel} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyActivityInstanceModel: function(model, value, options) {
              var activityInstanceModel =
                  this.get('activityInstanceModel') ||
                  new mwa.model.activityInstance.ActivityInstanceModel(null, {validate: false});

              [
                {
                  activityParameterCollection: this.get('inputActivityParameterCollection'),
                  parameterAttributeName: 'inputDetailsList'
                },
                {
                  activityParameterCollection: this.get('outputActivityParameterCollection'),
                  parameterAttributeName: 'outputDetailsList'
                }
              ].forEach(
                  // INFORMATION:
                  // The first argument is object type, not array. But we use plural name based on
                  // our JavaScript coding rule.
                  function(properties, index, array) {
                    // CAUTION:
                    // It is important to use set function instead of reset function in
                    // consideration of initial rendering of workflow not to lose activity parameter
                    // ID because it was specified by corresponding data workflow connection port.
                    properties.activityParameterCollection.set(
                        // CAUTION:
                        // It is necessary to clone the instances not to change the original values
                        // by parse.
                        (
                            activityInstanceModel.get(properties.parameterAttributeName) ||
                            // CAUTION:
                            // It is necessary to consider about a case that this function is called
                            // in initialization without activity instance model.
                            properties.activityParameterCollection.map(
                                function(activityParameterModel, index, array) {
                                  return {
                                    key: activityParameterModel.get('key'),
                                    // CAUTION:
                                    // It is necessary to clear result when activity instance model
                                    // is unset because the result was updated by any value when the
                                    // activity instance had been set.
                                    value: null
                                  };
                                }
                            )
                        ).map(
                            // INFORMATION:
                            // The first argument is object type, not array. But we use plural name
                            // based on our JavaScript coding rule.
                            function(parameters, index, array) {
                              // INFORMATION:
                              // It isn't necessary to consider about a case that there is any
                              // unknown activity parameter exists because they will be never used
                              // if there is no corresponding data workflow connection port.
                              return {
                                key: parameters.key,
                                result: parameters.value
                              };
                            }
                        ),
                        // CAUTION: NVX-5281 (https://www.tool.sony.biz/jira/browse/NVX-5281)
                        // It is important to disable add and remove and enable only merge not to
                        // change the parameter set in consideration of that activity instance not
                        // always have the all parameters. And validation error will be happened
                        // during add processing in a case that activity parameter models weren't
                        // created correctly (e.g. in predefined workflow).
                        // CAUTION: NVX-5190 (https://www.tool.sony.biz/jira/browse/NVX-5190)
                        // It is important to disable parse not to override type attribute of
                        // activity parameter model by unexpected default value.
                        {add: false, remove: false, merge: true, parse: false}
                    );
                  }
              );
            },
            /**
             * The event handler for changing inputActivityParameterCollection or
             * outputActivityParameterCollection attribute of view model and changing, adding,
             * removing, updating and resetting models in ActivityParameterCollection. This function
             * updates activity parameters according to the value.
             * @protected
             */
            applyActivityParameterCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:inputActivityParameterCollection"  (model, value, options)
              //  - "change:outputActivityParameterCollection" (model, value, options)
              // inputActivityParameterCollection
              // outputActivityParameterCollection
              //  - "change"                                   (model, options)
              //  - "change:[attribute]"                       (model, value, options)
              //  - "add", "remove"                            (model, collection, options)
              //  - "update", "reset"                          (collection, options)
              var that = this;
              var args = {};

              var activityModel = that.get('activityModel');
              var inputActivityParameterCollection = that.get('inputActivityParameterCollection');
              var outputActivityParameterCollection = that.get('outputActivityParameterCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.model.activityParameter.ActivityParameterCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.model.activityParameter.ActivityParameterModel) {
                  if (arguments[1] !== inputActivityParameterCollection &&
                      arguments[1] !== outputActivityParameterCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.model.activityParameter.ActivityParameterModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.model.activityParameter.ActivityParameterModel} model
                     * The added or removed model.
                     * @property {mwa.model.activityParameter.ActivityParameterCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === inputActivityParameterCollection ||
                    arguments[0] === outputActivityParameterCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.model.activityParameter.ActivityParameterCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  if (activityModel instanceof mwa.model.activity.ActivityModel) {
                    that.get('activityModel').set(
                        (arguments[0] === inputActivityParameterCollection) ? 'inputs' : 'outputs',
                        // CAUTION:
                        // It is important not to include value for parameter definitions in
                        // activity.
                        arguments[0].toObject(false)
                    );
                  }
                }
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * view model and changing, adding, removing, updating and resetting models in
             * pipelineWorkflowStepViewModelCollection. This function updates condition activity
             * parameter according to the value.
             * @protected
             */
            applyPipelineWorkflowStepViewModelCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:pipelineWorkflowStepViewModelCollection" (model, value, options)
              // pipelineWorkflowStepViewModelCollection
              //  - "change"                                         (model, options)
              //  - "change:[attribute]"                             (model, value, options)
              //  - "add", "remove"                                  (model, collection, options)
              //  - "update", "reset"                                (collection, options)
              var that = this;
              var args = {};

              var pipelineWorkflowStepViewModelCollection =
                  that.get('pipelineWorkflowStepViewModelCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                  if (arguments[1] !== pipelineWorkflowStepViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === pipelineWorkflowStepViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  if (that.get('activityModel').get('name') ===
                      mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH.templateName) {
                    var conditionActivityParameterModel =
                        // INFORMATION:
                        // It is possible to use get function instead of findWhere function in
                        // consideration of that idAttribute of activity parameter model is "key",
                        // not "id", but it is better to use findWhere function to make the code
                        // maintainable and readable.
                        that.get('inputActivityParameterCollection').findWhere({
                          key: mwa.enumeration.ActivityParameter.CONDITION.key
                        });
                    var conditionActivityParameterType =
                        conditionActivityParameterModel.get('type');

                    conditionActivityParameterModel.set({
                      value: conditionActivityParameterType.encodeText(
                          JSON.stringify(
                              new mwa.model.activityCondition.ActivityConditionCollection(
                                  (
                                      JSON.parse(
                                          conditionActivityParameterType.decodeText(
                                              conditionActivityParameterModel.get('value')
                                          )
                                      ) ||
                                      []
                                  ).filter(
                                      // INFORMATION:
                                      // The first argument is object type, not array. But we use
                                      // plural name based on our JavaScript coding rule.
                                      function(activityConditions, index, array) {
                                        var activityConditionModel =
                                            new mwa.model.activityCondition.ActivityConditionModel(
                                                mwa.proui._.extend(
                                                    activityConditions,
                                                    {
                                                      pipelineWorkflowStepViewModelCollection:
                                                          pipelineWorkflowStepViewModelCollection
                                                    }
                                                ),
                                                // CAUTION:
                                                // It is necessary to disable validation in order to
                                                // ignore pipeline check.
                                                {parse: true, validate: false}
                                            );
                                        var validationError =
                                            activityConditionModel.validate() || {};

                                        return (
                                            !pipelineWorkflowStepViewModelCollection.isEmpty() &&
                                            validationError.leftValue == null &&
                                            validationError.rightValue == null
                                        );
                                      }
                                  )
                              ).toObject()
                              // CAUTION:
                              // It is important to use toObject function in order not to include
                              // unnecessary attributes (e.g.
                              // pipelineWorkflowStepViewModelCollection).
                          )
                      )
                    });
                  }
                }
              }
            }
          }
      );
    })();

    /**
     * The mwa workflow step view model collection.
     * @constructor
     * @inner
     * @alias mwa.view.workflowStep.WorkflowStepViewModelCollection
     * @memberof mwa.view.workflowStep
     * @extends {mwa.Collection}
     */
    global.WorkflowStepViewModelCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.view.workflowStep.WorkflowStepViewModelCollection
           */
          {
            url: global.WorkflowStepViewModel.prototype.urlRoot,
            model: global.WorkflowStepViewModel,
            comparator: 'text',
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              return this.map(
                  function(workflowStepViewModel, index, array) {
                    return workflowStepViewModel.toObject();
                  }
              );
            }
          }
      );
    })();

    /**
     * The mwa pipeline workflow step view model collection.
     * @constructor
     * @inner
     * @alias mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection
     * @memberof mwa.view.workflowStep
     * @extends {mwa.Collection}
     */
    global.PipelineWorkflowStepViewModelCollection = (function() {
      return global.WorkflowStepViewModelCollection.extend(
          /**
           * @lends mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection
           */
          {
            /**
             * @see {@link mwa.view.workflowStep.WorkflowStepViewModelCollection.initialize}
             * @protected
             * @override
             */
            initialize: function() {
              var that = this;

              that._counters = {};

              mwa.view.workflowStep.WorkflowStepViewModelCollection.prototype.initialize.apply(
                  that, []
              );
            },
            /**
             * @see {@link mwa.view.workflowStep.WorkflowStepViewModelCollection.add}
             * @public
             * @override
             */
            add: function(models, options) {
              var that = this;

              // CAUTION:
              // The counter shouldn't be updated in this function because set function will be
              // called finally in super class.

              return mwa.view.workflowStep.WorkflowStepViewModelCollection.prototype.add.apply(
                  that, [models, options]
              );
            },
            /**
             * @see {@link mwa.view.workflowStep.WorkflowStepViewModelCollection.remove}
             * @public
             * @override
             */
            remove: function(models, options) {
              var that = this;

              models =
                  // INFORMATION:
                  // It is better to consider about a case that the value of "models" argument is
                  // undefined, null or singular.
                  [].concat(models || []).map(
                      function(model, index, array) {
                        that._counters[model.cid] = (that._counters[model.cid] || 1) - 1;

                        return model;
                      }
                  // CAUTION:
                  // This filter means not to remove specified models if they were added multiple
                  // times. In such case, only the value of corresponding counter is decreased.
                  ).filter(
                      function(model, index, array) {
                        return that._counters[model.cid] === 0;
                      }
                  );

              return mwa.view.workflowStep.WorkflowStepViewModelCollection.prototype.remove.apply(
                  that, [models, options]
              );
            },
            /**
             * @see {@link mwa.view.workflowStep.WorkflowStepViewModelCollection.set}
             * @public
             * @override
             */
            set: function(models, options) {
              var that = this;

              options = mwa.proui._.extend({add: true, remove: true, merge: true}, options);

              // INFORMATION:
              // It is better to consider about a case that the value of "models" argument is
              // undefined, null or singular.
              var collection = new mwa.proui.Backbone.Collection([].concat(models || []));

              // Add/Merge Case
              if (options.add || options.merge) {
                collection.models.forEach(
                    function(model, index, array) {
                      that._counters[model.cid] = (that._counters[model.cid] || 0) + 1;
                    }
                );
              }
              // Remove Case
              // CAUTION:
              // It is important to check this condition in consideration of a case that this
              // function is called by add function and the counter must not be updated in such
              // case.
              if (options.remove) {
                that.models.forEach(
                    function(model, index, array) {
                      that._counters[model.cid] =
                          (collection.get(model.cid) instanceof mwa.proui.Backbone.Model) ?
                              that._counters[model.cid] :
                              ((that._counters[model.cid] || 1) - 1);

                      // CAUTION:
                      // This condition means not to remove specified models if they were added
                      // multiple times. In such case, only the value of corresponding counter is
                      // decreased.
                      if (that._counters[model.cid] !== 0) {
                        // INFORMATION:
                        // It isn't necessary to consider about a case that the specified model
                        // already exists in the collection because the model won't be added by
                        // Backbone.js implementation in such case.
                        collection.add(model);
                      }
                    }
                );
              }

              return mwa.view.workflowStep.WorkflowStepViewModelCollection.prototype.set.apply(
                  that, [collection.models, options]
              );
            },
            /**
             * @see {@link mwa.view.workflowStep.WorkflowStepViewModelCollection.reset}
             * @public
             * @override
             */
            reset: function(models, options) {
              var that = this;

              // INFORMATION:
              // These codes mean to initialize counter object by deleting all properties without
              // changing the instance. It is better to keep using the same instance in the
              // lifecycle.
              for (var propertyName in that._counters) {
                if (that._counters.hasOwnProperty(propertyName)) {
                  delete that._counters[propertyName];
                }
              }

              // INFORMATION:
              // Actually, it isn't necessary to initialize the value of "models" argument but it is
              // better to implement similar with remove function to make the code maintainable and
              // readable.
              models =
                  // INFORMATION:
                  // It is better to consider about a case that the value of "models" argument is
                  // undefined, null or singular.
                  [].concat(models || []).map(
                      function(model, index, array) {
                        that._counters[model.cid] = (that._counters[model.cid] || 0) + 1;

                        return model;
                      }
                  );

              return mwa.view.workflowStep.WorkflowStepViewModelCollection.prototype.reset.apply(
                  that, [models, options]
              );
            }
          }
      );
    })();

    /**
     * The mwa workflow step view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowStep.WorkflowStepView
     * @memberof mwa.view.workflowStep
     * @extends {mwa.View}
     */
    global.WorkflowStepView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowStep.WorkflowStepView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-step mwa-view-workflow-item proui-state-user-select-disabled"' +
                ' style="width: <@= width @>px; height: <@= height @>px;">' +
                    '<div class="mwa-view-workflow-step-text">' +
                        '<input' +
                        ' class="proui-text-ellipsis"' +
                        ' style="min-width: <@= width @>px; max-width: <@= 2 * width @>px;"' +
                        ' type="text"' +
                        ' maxlength="100"' +
                        ' />' +
                    '</div>' +
                    '<div class="mwa-view-workflow-step-image"' +
                    ' data-template-name="<@= activityModel.get(\'name\') @>"' +
                    ' data-template-type="<@= activityModel.get(\'type\').name @>"' +
                    ' style="top: <@= offset @>px; left: <@= offset @>px;' +
                    ' width: <@= width - 2 * offset @>px; height: <@= height - 2 * offset @>px;">' +
                        '<@ var linearGradientId = mwa.proui.util.Uuid.generateUuidV4(); @>' +
                        '<svg>' +
                            '<defs>' +
                                '<linearGradient id="<@= linearGradientId @>"' +
                                ' x1="0" x2="0" y1="0" y2="1">' + // Vertical Gradient
                                    '<stop offset="0%" />' +
                                    '<stop offset="100%" />' +
                                '</linearGradient>' +
                            '</defs>' +
                            '<rect x="0" y="0" rx="6" ry="6" width="100%" height="100%" fill="url(#<@= linearGradientId @>)" />' +
                        '</svg>' +
                        '<img src="<@= activityModel.get(\'icon\') || (mwa.EXTERNAL_IMAGE_BASE_PATH + \'proui.util.gear.svg\') @>" />' +
                    '</div>' +
                    '<div class="mwa-view-workflow-step-progress-bar">' +
                        '<div style="top: <@= height - offset - 8 @>px; left: <@= offset + 4 @>px;' +
                        ' width: <@= width - 2 * offset - 8 @>px;">' +
                        '</div>' +
                    '</div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  'blur input:not([readonly])': 'blurInput',
                  'mousedown rect': 'mouseDownRect',
                  'dblclick rect': 'doubleClickRect',
                  'dbltap rect': 'doubleTapRect'
                }
            ),
            /**
             * @see {@link mwa.View.initialize}
             * @protected
             * @override
             */
            initialize: function(options, canRender) {
              this._timers = {};

              mwa.View.prototype.initialize.apply(this, [options, false]);
            },
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.listenTo(
                  this.options.viewModel,
                  'change:isDragging',
                  this.applyIsDragging
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:isSelected',
                  this.applyIsSelected
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canSelect',
                  this.applyCanSelect
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:isReadOnly',
                  this.applyIsReadOnly
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canDrag',
                  this.applyCanDrag
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:text',
                  this.applyText
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:positions',
                  this.applyPositions
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:activityInstanceModel',
                  this.applyActivityInstanceModel
              );

              this.bindingKeyDownCanvas = mwa.proui._.bind(this.keyDownCanvas, this);
              this.bindingMouseDownCanvas = mwa.proui._.bind(this.mouseDownCanvas, this);
              mwa.proui.Backbone.$('body')
                  // CAUTION:
                  // It is necessary to use on (off) function instead of bind (unbind) function
                  // because the target elements aren't rendered yet at this timing.
                  .off('keydown', '.mwa-view-workflow-canvas', this.bindingKeyDownCanvas)
                  .on('keydown', '.mwa-view-workflow-canvas', this.bindingKeyDownCanvas)
                  .off('mousedown', '.mwa-view-workflow-canvas', this.bindingMouseDownCanvas)
                  .on('mousedown', '.mwa-view-workflow-canvas', this.bindingMouseDownCanvas);
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              that.remove({silent: true});
              // INFORMATION:
              // It is important to call remove function instead of empty function for creating this
              // view's element from template.
              that.$el.remove();
              that.setElement(
                  mwa.proui.Backbone.$(
                      // CAUTION: NVXA-1492 (https://acropolis.atlassian.net/browse/NVXA-1492)
                      // It is necessary to parse the template string to create a DOM element
                      // certainly. Actually, jQuery sometimes doesn't create a DOM element
                      // correctly using the template string of this view including svg elements.
                      //  - jQuery 1.9 の $.parseHTML とかその辺
                      //    (http://t-ashula.hateblo.jp/entry/2013/01/23/114105)
                      mwa.proui.Backbone.$.parseHTML(that.template(that.presenter()))
                  )
              );

              that.views = {};

              that.$textElement = that.$el.find('.mwa-view-workflow-step-text input');
              that.$image = that.$el.find('.mwa-view-workflow-step-image');
              that.$progressBarElement = that.$el.find('.mwa-view-workflow-step-progress-bar div');

              that.applyIsReadOnly(
                  that.options.viewModel,
                  that.options.viewModel.get('isReadOnly'),
                  null
              );

              that.applyIsDragging(
                  that.options.viewModel,
                  that.options.viewModel.get('isDragging'),
                  null
              );

              that.applyIsSelected(
                  that.options.viewModel,
                  that.options.viewModel.get('isSelected'),
                  null
              );

              // CAUTION:
              // It isn't necessary to call applyCanDrag function in this processing because it is
              // called in applyCanSelect function.
              that.applyCanSelect(
                  that.options.viewModel,
                  that.options.viewModel.get('canSelect'),
                  null
              );

              // CAUTION:
              // It is necessary to hide the element until completing the positioning to avoid
              // flicker. And it is necessary to use visibility style setting for the non-display
              // because internal timer will stop based on whether the width is fixed.
              that.$el.css('visibility', 'hidden');
              // CAUTION:
              // It is important to clear the timer before updating it in consideration of a case
              // that this processing is executed multiple times during the interval.
              clearInterval(that._timers.render);
              // CAUTION:
              // It is important to call applyText and applyPositions functions after rendering
              // view's element because the width and height are unknown before rendering.
              that._timers.render = setInterval(function() {
                if (that.$el.width() != null) {
                  clearInterval(that._timers.render);

                  that.applyText(
                      that.options.viewModel,
                      that.options.viewModel.get('text'),
                      null
                  );

                  that.applyPositions(
                      that.options.viewModel,
                      that.options.viewModel.get('positions'),
                      null
                  );

                  // CAUTION:
                  // It is necessary to show the element after completing the positioning to avoid
                  // flicker.
                  that.$el.css('visibility', 'visible');
                }
              }, 100);

              that.applyActivityInstanceModel(
                  that.options.viewModel,
                  that.options.viewModel.get('activityInstanceModel'),
                  null
              );

              that.renderTextTooltip();

              return that;
            },
            /**
             * The function to render progress bar.
             * @protected
             *
             * @returns {?proui.progressBar.ProgressBarView}
             * The rendered progress bar view instance.
             */
            renderProgressBar: function() {
              if (this.views.progressBarView != null) {
                this.views.progressBarView.remove();
                // INFORMATION: NVX-4108 (https://www.tool.sony.biz/jira/browse/NVX-4108)
                // It is better to empty children elements in the $el element of progress bar view
                // for reuse.
                this.$progressBarElement.empty();
                this.$progressBarElement =
                    // CAUTION: NVX-4108 (https://www.tool.sony.biz/jira/browse/NVX-4108)
                    // It is necessary to reuse the $el element of progress bar view because it has
                    // specific styles for adjusting the position in consideration of workflow step
                    // size (width, height and offset).
                    mwa.proui.Backbone.$(this.$progressBarElement).appendTo(
                        this.$el.find('.mwa-view-workflow-step-progress-bar')
                    );
              }
              this.views.progressBarView = null;

              var activityInstanceModel = this.options.viewModel.get('activityInstanceModel');
              if (mwa.util.WorkflowStep.isProcessing(this.options.viewModel.get('activityModel')) &&
                  typeof activityInstanceModel !== 'undefined' && activityInstanceModel != null &&
                  !activityInstanceModel.get('status').terminated) {
                this.views.progressBarView = new mwa.proui.progressBar.ProgressBarView({
                  el: this.$progressBarElement,
                  viewModel: new mwa.proui.progressBar.ProgressBarViewModel({
                    value: activityInstanceModel.get('progress'),
                    tooltip: function(viewModel) {
                      return viewModel.get('value') + ' %';
                    }
                  })
                });
              }

              return this.views.progressBarView;
            },
            /**
             * The function to render text tooltip.
             * @protected
             *
             * @returns {!proui.tooltip.TooltipView}
             * The rendered tooltip view instance.
             */
            renderTextTooltip: function() {
              if (this.views.textTooltipView != null) {
                this.views.textTooltipView.remove();
              }
              this.views.textTooltipView = null;

              this.views.textTooltipView =
                  new mwa.proui.tooltip.TooltipView({
                    el: this.$textElement,
                    property: 'text',
                    viewModel: this.options.viewModel
                  });

              return this.views.textTooltipView;
            },
            /**
             * The function to render activity instance tooltip.
             * @protected
             *
             * @returns {!proui.tooltip.TooltipView}
             * The rendered tooltip view instance.
             */
            renderActivityInstanceTooltip: function() {
              if (this.views.activityInstanceTooltipView != null) {
                this.views.activityInstanceTooltipView.remove();
              }
              this.views.activityInstanceTooltipView = null;

              var activityInstanceModel = this.options.viewModel.get('activityInstanceModel');
              if (activityInstanceModel instanceof
                  mwa.model.activityInstance.ActivityInstanceModel) {
                this.views.activityInstanceTooltipView =
                    new mwa.proui.tooltip.TooltipView({
                      el: this.$image,
                      viewModel: this.options.viewModel.createActivityInstanceTooltipModel()
                    });
              }

              return this.views.textTooltipView;
            },
            /**
             * @see {@link mwa.View.remove}
             * @public
             * @override
             */
            remove: function(options) {
              if (typeof options === 'undefined' || options == null || !options.silent) {
                // CAUTION:
                // It is important to clear the all timers when remove in order not to execute the
                // unnecessary processing after removal.
                for (var propertyName in this._timers) {
                  if (this._timers.hasOwnProperty(propertyName)) {
                    clearInterval(this._timers[propertyName]);
                  }
                }
              }

              return mwa.View.prototype.remove.apply(this, [options]);
            },
            /**
             * The event handler of keydown event on canvas.
             * @protected
             *
             * @param {object} event
             * The keydown event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            keyDownCanvas: function(event) {
              var canPropagate = true;

              if (this.$el.closest('.mwa-view-workflow-canvas').is(':focus')) {
                switch (event.keyCode) {
                  case 46: // delete
                    if (this.options.viewModel.get('isSelected') &&
                        this.options.viewModel.get('canDelete')) {
                      this.remove();
                      canPropagate = false;
                    }
                    break;

                  default:
                    break;
                }
              }

              return canPropagate;
            },
            /**
             * The event handler of mousedown event on canvas.
             * @protected
             *
             * @param {object} event
             * The mousedown event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            mouseDownCanvas: function(event) {
              var $target = mwa.proui.Backbone.$(event.target);
              var $currentTarget = mwa.proui.Backbone.$(event.currentTarget);
              var $workflowItem = $target.closest('.mwa-view-workflow-item');

              if ($target.closest(this.$el).length === 0 &&
                  $workflowItem.length !== 0 &&
                  $workflowItem.hasClass('proui-state-selected') &&
                  $currentTarget.find(this.$el).length !== 0) {
                this.options.viewModel.set('isSelected', false);
              }

              return true;
            },
            /**
             * The event handler of blur event on input element.
             * @protected
             *
             * @param {object} event
             * The blur event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            blurInput: function(event) {
              this.options.viewModel.set('text', this.$textElement.val());

              // INFORMATION:
              // This implementation is same as completeText function of proui.textBox.TextBoxView.
              return false;
            },
            /**
             * The event handler of mousedown event on rect element.
             * @protected
             *
             * @param {object} event
             * The mousedown event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            mouseDownRect: function(event) {
              // CAUTION:
              // This code means to set isSelected to true only if canSelect is true.
              this.options.viewModel.set('isSelected', this.options.viewModel.get('canSelect'));

              this.$el.closest('.mwa-view-workflow-canvas').focus();

              return true;
            },
            /**
             * The event handler of dblclick event on rect element.
             * @protected
             *
             * @param {object} event
             * The dblclick event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            doubleClickRect: function(event) {
              // CAUTION:
              // Don't use event.button value because the value isn't normalized by jQuery.
              //  - event.which (https://api.jquery.com/event.which/)
              if (event.which === 1 &&
                  this.options.viewModel.get('canSelect')) {
                this.options.viewModel.execute(this);
              }

              return true;
            },
            /**
             * The event handler of dbltap event on rect element.
             * @protected
             *
             * @param {object} event
             * The dbltap event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            doubleTapRect: function(event) {
              // CAUTION:
              // It is important to stop zoom by calling preventDefault function.
              event.preventDefault();

              if (this.options.model.get('canSelect')) {
                this.options.viewModel.execute(this);
              }

              return true;
            },
            /**
             * @see {@link mwa.View.applyChanges}
             * @public
             * @override
             */
            applyChanges: function(model, options) {
              mwa.View.prototype.applyChanges.apply(this, [model, options]);

              // CAUTION:
              // It is necessary to call applyCanDrag function because applyChanges function is
              // called when isDisabled attribute of view model is changed and draggable settings
              // should be updated according to the values of isDisabled, canSelect and canDrag.
              this.applyCanDrag(
                  this.options.viewModel,
                  this.options.viewModel.get('canDrag'),
                  null
              );
            },
            /**
             * The event handler for changing isReadOnly attribute of view model. This function adds
             * or removes readonly attribute to or from text element according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsReadOnly: function(model, value, options) {
              this.$textElement.attr(
                  'readonly',
                  (this.options.viewModel.get('isReadOnly')) ? 'readonly' : null
              );
            },
            /**
             * The event handler for changing isDragging attribute of view model. This function adds
             * or removes dragging style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsDragging: function(model, value, options) {
              var isDragging = this.options.viewModel.get('isDragging');

              this.$el.toggleClass('proui-state-dragging', isDragging);
            },
            /**
             * The event handler for changing isSelected attribute of view model. This function adds
             * or removes selected style settings and calls select callback according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsSelected: function(model, value, options) {
              var isSelected = this.options.viewModel.get('isSelected');

              if (isSelected) {
                // CAUTION:
                // It is necessary to trigger mousedown event to deselect any other workflow step
                // but the event shouldn't be triggered in deselection case because the event
                // listener (mouseDownRect function) basically always set true to isSelected
                // attribute of view model in order not to deselect the workflow step by GUI
                // operation.
                this.$el.find('rect').trigger('mousedown');
              }
              this.$el.toggleClass('proui-state-selected', isSelected);

              this.options.viewModel.select(this, isSelected);
            },
            /**
             * The event handler for changing canSelect attribute of view model. This function adds
             * or removes selectable style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanSelect: function(model, value, options) {
              this.$el.toggleClass(
                  'proui-state-selectable', this.options.viewModel.get('canSelect')
              );

              // CAUTION:
              // It is necessary to call applyCanDrag function because applyCanSelect function is
              // called when canSelect attribute of view model is changed and draggable settings
              // should be updated according to the values of isDisabled, canSelect and canDrag.
              this.applyCanDrag(
                  this.options.viewModel,
                  this.options.viewModel.get('canDrag'),
                  null
              );
            },
            /**
             * The event handler for changing canDrag attribute of view model. This function adds or
             * removes draggable settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDrag: function(model, value, options) {
              var that = this;

              var isDisabled = that.options.viewModel.get('isDisabled');
              var canSelect = that.options.viewModel.get('canSelect');
              var canDrag = !isDisabled && canSelect && that.options.viewModel.get('canDrag');

              if (that.$el.hasClass('proui-state-draggable')) {
                that.$el.draggable('destroy');
              }
              that.$el.toggleClass('proui-state-draggable', canDrag);

              if (canDrag) {
                that.$el.draggable({
                  containment: 'parent',
                  start: function(event, ui) {
                    that.options.viewModel.set('isDragging', true);
                  },
                  drag: function(event, ui) {
                    that.options.viewModel.set(
                        'positions',
                        {
                          x: ui.position.left + that.$el.width() / 2,
                          y: ui.position.top + that.$el.height() / 2,
                          z: that.options.viewModel.get('positions').z
                        }
                    );
                  },
                  stop: function(event, ui) {
                    that.options.viewModel.set('isDragging', false);
                  }
                });
              }
            },
            /**
             * The event handler for changing text attribute of view model. This function displays
             * text according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {string} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyText: function(model, value, options) {
              var text = this.options.viewModel.get('text');
              var $dummyTextElement =
                  mwa.proui.Backbone.$('<span></span>')
                      .hide()
                      // CAUTION: NVX-7018 (https://www.tool.sony.biz/jira/browse/NVX-7018)
                      // It is important to apply proui-text-ellipsis style class to calculate the
                      // text width correctly.
                      .addClass('proui-text-ellipsis')
                      .insertAfter(this.$textElement)
                      .text(text);
              this.$textElement.val(text).width($dummyTextElement.width());
              $dummyTextElement.remove();
            },
            /**
             * The event handler for changing positions attribute of view model. This function moves
             * element positions according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {object} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyPositions: function(model, value, options) {
              var positions = this.options.viewModel.get('positions');
              this.$el.css({
                left: (positions.x - this.$el.width() / 2) + 'px',
                top: (positions.y - this.$el.height() / 2) + 'px',
                zIndex: positions.z
              });
            },
            /**
             * The event handler for changing activityInstanceModel attribute of view model. This
             * function shows or hides progress bar according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activityInstance.ActivityInstanceModel} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyActivityInstanceModel: function(model, value, options) {
              this.renderActivityInstanceTooltip();
              this.renderProgressBar();

              // TODO: This process is for NAB 2016 demonstration tentatively.
              var activityInstanceModel = this.options.viewModel.get('activityInstanceModel');
              this.applyActivityInstanceStatus(
                  activityInstanceModel,
                  (typeof activityInstanceModel === 'undefined' || activityInstanceModel == null) ?
                      null : activityInstanceModel.get('status'),
                  options
              );
            },
            /**
             * The event handler for changing status attribute of activityInstanceModel. This
             * function shows or hides status image on this step according to the value.
             * @protected
             *
             * @param {mwa.model.activityInstance.ActivityInstanceModel} model
             * The mwa activity instance model.
             * @param {object} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyActivityInstanceStatus: function(model, value, options) {
              for (var propertyName in mwa.enumeration.ActivityInstanceStatus) {
                if (mwa.enumeration.ActivityInstanceStatus.hasOwnProperty(propertyName)) {
                  this.$el.removeClass(mwa.enumeration.ActivityInstanceStatus[propertyName].styleClass);
                }
              }
              // TODO: This process is for NAB 2016 demonstration tentatively.
              this.$el.removeClass('proui-state-success');

              var activityInstanceModel = this.options.viewModel.get('activityInstanceModel');
              if (typeof activityInstanceModel !== 'undefined' && activityInstanceModel != null) {
                var status = activityInstanceModel.get('status');
                if (mwa.enumeration.ActivityInstanceStatus.hasOwnProperty(status.name)) {
                  this.$el.addClass(mwa.enumeration.ActivityInstanceStatus[status.name].styleClass);
                } else {
                  // TODO: This process is for NAB 2016 demonstration tentatively.
                  if (status.name === 'COMPLETED') {
                    this.$el.addClass('proui-state-success');
                  }
                }
              }
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowStep;
}));
