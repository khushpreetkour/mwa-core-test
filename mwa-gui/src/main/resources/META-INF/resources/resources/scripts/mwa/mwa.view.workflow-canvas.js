/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.view == null || mwa.view.workflowParameterEditor == null) ? './mwa.view.workflow-parameter-editor' : {},
      (mwa.model == null || mwa.model.workflowDiagram == null) ? './mwa.model.workflow-diagram' : {},
      (mwa.model == null || mwa.model.workflow == null) ? './mwa.model.workflow' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.view == null || mwa.view.workflowParameterEditor == null) ? require('./mwa.view.workflow-parameter-editor') : {},
        (mwa.model == null || mwa.model.workflowDiagram == null) ? require('./mwa.model.workflow-diagram') : {},
        (mwa.model == null || mwa.model.workflow == null) ? require('./mwa.model.workflow') : {}
    );
  } else {
    root.mwa.view.workflowCanvas = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.view.workflowParameterEditor,
        root.mwa.model.workflowDiagram,
        root.mwa.model.workflow
    );
  }
}(this, function(base, subBase, workflowParameterEditor, workflowDiagram, workflow) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.view.workflowParameterEditor namespace.
  if (mwa.view.workflowParameterEditor == null) {
    mwa.view.workflowParameterEditor = workflowParameterEditor;
  }
  // To initialize the mwa.model.workflowDiagram namespace.
  if (mwa.model.workflowDiagram == null) {
    mwa.model.workflowDiagram = workflowDiagram;
  }
  // To initialize the mwa.model.workflow namespace.
  if (mwa.model.workflow == null) {
    mwa.model.workflow = workflow;
  }

  /**
   * The mwa.view.workflowCanvas namespace.
   * @namespace
   */
  mwa.view.workflowCanvas = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowCanvas alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    var ACTIVITY_PARAMETER_TYPE_ARRAY =
        mwa.proui._.values(mwa.enumeration.ActivityParameterType);
    var WORKFLOW_CONNECTION_PORT_COLOR_ARRAY =
        mwa.proui._.values(mwa.enumeration.WorkflowConnectionPortColor);

    /**
     * The enumeration for workflow item.
     * @readonly
     * @enum {object}
     */
    var WorkflowItem = {
      STEP: {
        name: 'STEP',
        attributeName: 'steps',
        viewArrayName: 'workflowStepViewArray',
        viewModelCollectionName: 'workflowStepViewModelCollection',
        viewModelCollectionClass: mwa.view.workflowStep.WorkflowStepViewModelCollection,
        renderFunctionName: 'renderWorkflowStep'
      },
      CONNECTION_PORT: {
        name: 'CONNECTION_PORT',
        attributeName: 'connectionPorts',
        viewArrayName: 'workflowConnectionPortViewArray',
        viewModelCollectionName: 'workflowConnectionPortViewModelCollection',
        viewModelCollectionClass: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection,
        renderFunctionName: 'renderWorkflowConnectionPort'
      },
      CONNECTION: {
        name: 'CONNECTION',
        attributeName: 'connections',
        viewArrayName: 'workflowConnectionViewArray',
        viewModelCollectionName: 'workflowConnectionViewModelCollection',
        viewModelCollectionClass: mwa.view.workflowConnection.WorkflowConnectionViewModelCollection,
        renderFunctionName: 'renderWorkflowConnection'
      }
    };

    /**
     * The mwa workflow canvas view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowCanvas.WorkflowCanvasViewModel
     * @memberof mwa.view.workflowCanvas
     * @extends {mwa.ViewModel}
     */
    global.WorkflowCanvasViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowCanvas.WorkflowCanvasViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    hasGrid: true,
                    hasInformationButton: false,
                    hasWorkflowMetadataEditor: true,
                    isReadOnly: true,
                    canSelect: false,
                    canDrag: false,
                    canDrop: false,
                    canDelete: false,
                    canConnect: false,
                    enlargementFactor: 100,
                    activityParameterTypeCollection:
                        new mwa.model.activityParameterType.ActivityParameterTypeCollection(),
                    workflowDiagramModel: null,
                    workflowInstanceModel: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  hasGrid: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  hasInformationButton: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  hasWorkflowMetadataEditor: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isReadOnly: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canSelect: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDrag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDrop: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDelete: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canConnect: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  enlargementFactor: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  activityParameterTypeCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityParameterType.ActivityParameterTypeCollection
                    }
                  ],
                  workflowDiagramModel: [
                    {
                      required: false
                    },
                    {
                      instance: mwa.model.workflowDiagram.WorkflowDiagramModel
                    }
                  ],
                  workflowInstanceModel: [
                    {
                      required: false
                    },
                    {
                      instance: mwa.model.activityInstance.ActivityInstanceModel
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa workflow canvas view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowCanvas.WorkflowCanvasView
     * @memberof mwa.view.workflowCanvas
     * @extends {mwa.View}
     */
    global.WorkflowCanvasView = (function() {
      var emptyActivityParameterModel =
          new mwa.model.activityParameter.ActivityParameterModel(
              null,
              {validate: false}
          );

      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowCanvas.WorkflowCanvasView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                // CAUTION:
                // It is important to set tabindex attribute to detect keyboard event.
                '<div class="mwa-view-workflow-canvas proui-state-grabbable" tabindex="-1">' +
                    '<div class="mwa-view-workflow-canvas-workflow-step"></div>' +
                    '<div class="mwa-view-workflow-canvas-workflow-information"></div>' +
                    // CAUTION:
                    // It is important to prepare workflow information drawer element after workflow
                    // parameter element to always display the element in consideration of z-index.
                    '<div class="mwa-view-workflow-canvas-workflow-information-drawer"></div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.models = {
                workflowStepViewModelCollection:
                    new mwa.view.workflowStep.WorkflowStepViewModelCollection(),
                workflowConnectionPortViewModelCollection:
                    new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection(),
                workflowConnectionViewModelCollection:
                    new mwa.view.workflowConnection.WorkflowConnectionViewModelCollection()
              };

              // Workflow Step

              this.listenTo(
                  this.models.workflowStepViewModelCollection,
                  'change:text',
                  this.applyWorkflowStepViewModelCollection
              );
              this.listenTo(
                  this.models.workflowStepViewModelCollection,
                  // INFORMATION:
                  // To be careful about attribute name.
                  'change:positions',
                  this.applyWorkflowStepViewModelCollection
              );
              this.listenTo(
                  this.models.workflowStepViewModelCollection,
                  // CAUTION:
                  // It is necessary to check the change of isDragging attribute in order to update
                  // the position only once after completing drag.
                  'change:isDragging',
                  this.applyWorkflowStepViewModelCollection
              );
              this.listenTo(
                  this.models.workflowStepViewModelCollection,
                  'update',
                  this.applyWorkflowStepViewModelCollection
              );
              this.listenTo(
                  this.models.workflowStepViewModelCollection,
                  'reset',
                  this.applyWorkflowStepViewModelCollection
              );
              this.listenTo(
                  this.models.workflowStepViewModelCollection,
                  'destroy',
                  this.applyWorkflowStepViewModelCollection
              );
              this.listenTo(
                  this.models.workflowStepViewModelCollection,
                  'sort',
                  this.sortWorkflowItems
              );

              // Workflow Connection Port

              this.listenTo(
                  this.models.workflowConnectionPortViewModelCollection,
                  // CAUTION:
                  // It is necessary to check the change of any attribute in consideration of
                  // workflow parameter editing. And also, it is necessary to check the change of
                  // isDragging attribute in order to update the position only once after completing
                  // drag.
                  'change',
                  this.applyWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionPortViewModelCollection,
                  'add',
                  this.applyWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionPortViewModelCollection,
                  'update',
                  this.applyWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionPortViewModelCollection,
                  'reset',
                  this.applyWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionPortViewModelCollection,
                  'destroy',
                  this.applyWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionPortViewModelCollection,
                  'sort',
                  this.sortWorkflowItems
              );

              // Workflow Connection

              this.listenTo(
                  this.models.workflowConnectionViewModelCollection,
                  'change',
                  this.applyWorkflowConnectionViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionViewModelCollection,
                  'update',
                  this.applyWorkflowConnectionViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionViewModelCollection,
                  'reset',
                  this.applyWorkflowConnectionViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionViewModelCollection,
                  'destroy',
                  this.applyWorkflowConnectionViewModelCollection
              );
              this.listenTo(
                  this.models.workflowConnectionViewModelCollection,
                  'sort',
                  this.sortWorkflowItems
              );

              // View Model

              this.listenTo(
                  this.options.viewModel,
                  'change:hasGrid',
                  this.applyHasGrid
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:hasInformationButton',
                  this.applyHasInformationButton
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:isReadOnly',
                  this.applyIsReadOnly
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canSelect',
                  this.applyCanSelect
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canDrag',
                  this.applyCanDrag
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canDrop',
                  this.applyCanDrop
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canDelete',
                  this.applyCanDelete
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canConnect',
                  this.applyCanConnect
              );

              this.listenToWorkflowDiagramModel(
                  this.options.viewModel,
                  this.options.viewModel.get('workflowDiagramModel'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:workflowDiagramModel',
                  this.listenToWorkflowDiagramModel
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:workflowDiagramModel',
                  this.applyWorkflowDiagramModel
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:workflowInstanceModel',
                  this.applyWorkflowInstanceModel
              );
            },
            /**
             * The event handler for changing workflowDiagramModel attribute of view model. This
             * function removes old listeners and adds new listeners.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {mwa.model.workflowDiagram.WorkflowDiagramModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowDiagramModel: function(model, value, options) {
              var previousValue = model.previous('workflowDiagramModel');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'change', this.applyWorkflowDiagramModel);
              }
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              mwa.View.prototype.render.apply(this, []);

              this.$workflowCanvas = this.$el.children();
              this.$workflowStep = this.$el.find('.mwa-view-workflow-canvas-workflow-step');

              this.renderWorkflowItems(WorkflowItem.STEP);
              this.renderWorkflowItems(WorkflowItem.CONNECTION_PORT);
              this.renderWorkflowItems(WorkflowItem.CONNECTION);
              this.renderWorkflowInformationDrawer();

              this.applyHasGrid(
                  this.options.viewModel,
                  this.options.viewModel.get('hasGrid'),
                  null
              );

              this.applyHasInformationButton(
                  this.options.viewModel,
                  this.options.viewModel.get('hasInformationButton'),
                  null
              );

              // CAUTION:
              // It is necessary to call applyCanDrop function even if this function was already
              // called from applyChanges function by super function because this.$workflowCanvas
              // element hadn't be ready at that timing.
              this.applyCanDrop(
                  this.options.viewModel,
                  this.options.viewModel.get('canDrop'),
                  null
              );

              this.applyWorkflowInstanceModel(
                  this.options.viewModel,
                  this.options.viewModel.get('workflowInstanceModel'),
                  null
              );

              return this;
            },
            /**
             * The function to render workflow step.
             * @protected
             *
             * @param {!object} steps
             * The object including workflow step information.
             * @param {!string} steps.id
             * The string of workflow step ID.
             * @param {!string} steps.name
             * The string to be displayed as workflow step name.
             * @param {!object} steps.position
             * The object to decide display position of workflow step.
             * @param {!number} steps.position.x
             * The number for x-coordinate of workflow step position.
             * @param {!number} steps.position.y
             * The number for y-coordinate of workflow step position.
             * @param {!number} steps.position.z
             * The number for z-coordinate of workflow step position.
             * @param {!object} steps.activity
             * The object including activity information.
             *
             * @param {?boolean} [canPush = true]
             * The flag whether the rendered workflow step view and view model instances can be
             * pushed to managed array (collection).
             *
             * @returns {!mwa.view.workflowStep.WorkflowStepView}
             * The rendered workflow step view instance.
             */
            renderWorkflowStep: function(steps, canPush) {
              var that = this;

              var workflowStepView =
                  new mwa.view.workflowStep.WorkflowStepView({
                    viewModel: new mwa.view.workflowStep.WorkflowStepViewModel({
                      id: steps.id,
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      canSelect: that.options.viewModel.get('canSelect'),
                      canDrag: that.options.viewModel.get('canDrag'),
                      canDelete: that.options.viewModel.get('canDelete'),
                      // INFORMATION:
                      // To be careful about attribute name.
                      text: steps.name,
                      // INFORMATION:
                      // To be careful about attribute name.
                      positions: steps.position,
                      activityModel:
                          new mwa.model.activity.ActivityModel(
                              // CAUTION: NVX-5670 (https://www.tool.sony.biz/jira/browse/NVX-5670)
                              // It is necessary to prepare a different instance by deep copy in
                              // consideration of nested properties because the original instance is
                              // referred by workflowDiagramModel attribute of view model and the
                              // value shouldn't be updated through the parse processing of activity
                              // model in order not to detect unnecessary modification in initial
                              // rendering.
                              mwa.proui.util.Object.extend(true, {}, steps.activity),
                              {parse: true}
                          ),
                      execute: function(workflowStepView) {
                        if (that.views != null) {
                          if (that.views.workflowInformationTabView != null) {
                            // CAUTION: NVX-6171 (https://www.tool.sony.biz/jira/browse/NVX-6171)
                            // It is better to update the settings by this order in order to prevent
                            // flickering, in consideration of a case that the current selected tab
                            // is workflow and not step.
                            that.views.workflowInformationTabView.options.viewModel.set(
                                'selectedIndex',
                                that.views.workflowInformationTabView.options.viewModel
                                    .get('contentList')
                                    // INFORMATION:
                                    // The findTabIndex function should be defined similar to
                                    // workflow editor implementation if there will be a case to
                                    // need to find tab index in another implementation.
                                    .findIndex(
                                        function(tabContentModel, index, array) {
                                          return (
                                              tabContentModel.get('viewOptions').viewModel ===
                                              that.views.workflowParameterEditorView.options.viewModel
                                          );
                                        }
                                    )
                            );
                          }

                          if (that.views.workflowInformationDrawerView != null) {
                            that.views.workflowInformationDrawerView.options.viewModel.set(
                                'isSelected',
                                true
                            );
                          }
                        }
                      }
                    })
                  });

              // CAUTION:
              // It is necessary to set immediate values manually.
              var connectionPortArray =
                  that.options.viewModel.get('workflowDiagramModel').get('connectionPorts') ||
                  [];
              [
                {
                  activityParameterCollection:
                      workflowStepView.options.viewModel.get('inputActivityParameterCollection'),
                  connectionPortArray: mwa.proui._.where(
                      connectionPortArray,
                      {
                        stepId: steps.id,
                        type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT
                      }
                  )
                },
                {
                  activityParameterCollection:
                      workflowStepView.options.viewModel.get('outputActivityParameterCollection'),
                  connectionPortArray: mwa.proui._.where(
                      connectionPortArray,
                      {
                        stepId: steps.id,
                        type: mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT
                      }
                  )
                }
              ].forEach(
                  // INFORMATION:
                  // The first argument is object type, not array. But we use plural name based on
                  // our JavaScript coding rule.
                  function(properties, index, array) {
                    properties.activityParameterCollection.forEach(
                        function(activityParameterModel, index, array) {
                          activityParameterModel.set(
                              'value',
                              (
                                  // CAUTION:
                                  // It isn't possible to use findWhere function because activity
                                  // parameter model may not have ID at this timing and key is
                                  // specified in parameter property hierarchically.
                                  properties.connectionPortArray.filter(
                                      // INFORMATION:
                                      // The first argument is object type, not array. But we use
                                      // plural name based on our JavaScript coding rule.
                                      function(connectionPorts, index, array) {
                                        return (
                                            // INFORMATION:
                                            // To be careful about attribute name.
                                            connectionPorts.parameter.key ===
                                            activityParameterModel.get('key')
                                        );
                                      }
                                      // INFORMATION:
                                      // To be careful about attribute name.
                                  )[0] ||
                                  // CAUTION:
                                  // It is necessary to prepare dummy value in consideration of a
                                  // case that corresponding workflow connection port cannot be
                                  // found at workflow step creation timing because such workflow
                                  // connection ports will be created after workflow step creation
                                  // in drop callback.
                                  {
                                    parameter: {
                                      value: null
                                    }
                                  }
                              ).parameter.value
                          );
                        }
                    );
                  }
              );

              that.$workflowStep.append(workflowStepView.render().$el);

              if (canPush !== false) {
                that.views.workflowStepViewArray.push(workflowStepView);
                that.models.workflowStepViewModelCollection.add(
                    workflowStepView.options.viewModel,
                    // CAUTION:
                    // It isn't necessary to disable sort in this case for other views (e.g.
                    // workflow parameter editor).
                    {sort: true}
                );
              }

              return workflowStepView;
            },
            /**
             * The function to render workflow connection port.
             * @protected
             *
             * @param {!object} connectionPorts
             * The object including workflow connection port information.
             * @param {!string} connectionPorts.id
             * The string of workflow connection port ID.
             * @param {!object} connectionPorts.position
             * The object to decide display position of workflow connection port.
             * @param {!number} connectionPorts.position.x
             * The number for x-coordinate of workflow connection port position.
             * @param {!number} connectionPorts.position.y
             * The number for y-coordinate of workflow connection port position.
             * @param {!number} connectionPorts.position.z
             * The number for z-coordinate of workflow connection port position.
             * @param {!(string|mwa.enumeration.WorkflowConnectionPortType)} connectionPorts.type
             * The type of workflow connection port.
             * @param {!string} connectionPorts.stepId
             * The workflow step ID to which the workflow connection port belongs.
             * @param {?object} [connectionPorts.parameter]
             * The object including workflow parameter information.
             * @param {!boolean} [connectionPorts.parameter.visibleFlag]
             * The flag whether workflow parameter is visible.
             * @param {!boolean} [connectionPorts.parameter.listFlag]
             * The flag whether workflow parameter is list.
             * @param {!boolean} [connectionPorts.parameter.required]
             * The flag whether workflow parameter is required.
             * @param {!string} [connectionPorts.parameter.typeName]
             * The string to specify workflow parameter type.
             * @param {?string} [connectionPorts.parameter.parentKey]
             * The string to specify parent workflow parameter.
             * @param {?*} [connectionPorts.parameter.parentValue]
             * Any value for parent workflow parameter.
             * @param {!string} [connectionPorts.parameter.key]
             * The string to specify workflow parameter.
             * @param {?*} [connectionPorts.parameter.value]
             * Any value for workflow parameter.
             *
             * @param {?boolean} [canPush = true]
             * The flag whether the rendered workflow connection port view and view model instances
             * can be pushed to managed array (collection).
             *
             * @returns {!mwa.view.workflowConnectionPort.WorkflowConnectionPortView}
             * The rendered workflow connection port view instance.
             */
            renderWorkflowConnectionPort: function(connectionPorts, canPush) {
              var that = this;

              var type = (typeof connectionPorts.type === 'string') ?
                  mwa.enumeration.WorkflowConnectionPortType[connectionPorts.type] :
                  connectionPorts.type;
              // INFORMATION:
              // To be careful about attribute name.
              var parameters =
                  connectionPorts.parameter ||
                  // INFORMATION:
                  // This is for control workflow connection port and it should be always required.
                  mwa.proui._.extend(
                      // INFORMATION:
                      // It is better to use default values of activity parameter model in
                      // consideration of maintainability because almost properties are unnecessary
                      // for control workflow connection port. And it is better not to create a new
                      // instance of dummy activity parameter model in every rendering in
                      // consideration of performance.
                      emptyActivityParameterModel.toObject(true),
                      {
                        listFlag: false,
                        required: true,
                        // CAUTION:
                        // It is important to specify any value instead of null to detect correct
                        // color for control workflow connection port certainly except for
                        // conditional branch case.
                        typeName: mwa.enumeration.ActivityParameterType.UNKNOWN.typeName,
                        key: type.name
                      }
                  );
              var workflowStepViewModel =
                  that.models.workflowStepViewModelCollection.get(connectionPorts.stepId);
              var workflowStepActivityName = workflowStepViewModel.get('activityModel').get('name');

              var workflowConnectionPortView =
                  new mwa.view.workflowConnectionPort.WorkflowConnectionPortView({
                    viewModel: new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel({
                      id: connectionPorts.id,
                      key: parameters.key,
                      // INFORMATION:
                      // The data workflow connection port always should be hidden in current
                      // specification.
                      isHidden:
                          type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                          type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT,
                      isRequired: parameters.required,
                      canConnect: that.options.viewModel.get('canConnect'),
                      // INFORMATION:
                      // To be careful about attribute name.
                      positions: connectionPorts.position,
                      type: type,
                      typeName: parameters.typeName,
                      color:
                          (
                              workflowStepActivityName ===
                              mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH.templateName &&
                              type === mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT
                          ) ?
                              (
                                  (
                                      // INFORMATION:
                                      // To be careful about attribute name.
                                      workflowStepViewModel.get('positions').y ===
                                      connectionPorts.position.y
                                  ) ?
                                      mwa.enumeration.WorkflowConnectionPortColor.GREEN :
                                      mwa.enumeration.WorkflowConnectionPortColor.RED
                              ) :
                              mwa.proui._.findWhere(
                                  WORKFLOW_CONNECTION_PORT_COLOR_ARRAY,
                                  {
                                    isArray: parameters.listFlag,
                                    activityParameterType:
                                        mwa.proui._.findWhere(
                                            ACTIVITY_PARAMETER_TYPE_ARRAY,
                                            {typeName: parameters.typeName}
                                        ) ||
                                        mwa.enumeration.ActivityParameterType.SETTING
                                  }
                              ),
                      workflowStepViewModel: workflowStepViewModel,
                      connect: mwa.proui._.bind(that._connectWorkflowConnection, that)
                    })
                  });

              that.$workflowStep.append(workflowConnectionPortView.render().$el);

              if (canPush !== false) {
                that.views.workflowConnectionPortViewArray.push(workflowConnectionPortView);
                that.models.workflowConnectionPortViewModelCollection.add(
                    workflowConnectionPortView.options.viewModel
                );
              }

              return workflowConnectionPortView;
            },
            /**
             * The function to render workflow connection.
             * @protected
             *
             * @param {!object} connections
             * The object including workflow connection information.
             * @param {!string} connections.fromConnectionPortId
             * The string of workflow connection port ID as start point.
             * @param {!string} connections.toConnectionPortId
             * The string of workflow connection port ID as end point.
             *
             * @param {?boolean} [canPush = true]
             * The flag whether the rendered workflow connection view and view model instances can
             * be pushed to managed array (collection).
             *
             * @returns {!mwa.view.workflowConnection.WorkflowConnectionView}
             * The rendered workflow connection view instance.
             */
            renderWorkflowConnection: function(connections, canPush) {
              var that = this;

              var fromWorkflowConnectionPortViewModel =
                  that.models.workflowConnectionPortViewModelCollection.findWhere({
                    id: connections.fromConnectionPortId
                  });
              var toWorkflowConnectionPortViewModel =
                  that.models.workflowConnectionPortViewModelCollection.findWhere({
                    id: connections.toConnectionPortId
                  });

              var workflowConnectionView =
                  new mwa.view.workflowConnection.WorkflowConnectionView({
                    viewModel: new mwa.view.workflowConnection.WorkflowConnectionViewModel({
                      // INFORMATION:
                      // The data workflow connection always should be hidden in current
                      // specification.
                      isHidden:
                          fromWorkflowConnectionPortViewModel.get('type') ===
                          mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT &&
                          toWorkflowConnectionPortViewModel.get('type') ===
                          mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT,
                      canSelect:
                          that.options.viewModel.get('canSelect') &&
                          !that.options.viewModel.get('isReadOnly'),
                      canDrag: that.options.viewModel.get('canDrag'),
                      canDelete: that.options.viewModel.get('canDelete'),
                      fromWorkflowConnectionPortViewModel: fromWorkflowConnectionPortViewModel,
                      toWorkflowConnectionPortViewModel: toWorkflowConnectionPortViewModel
                    })
                  });

              // CAUTION:
              // It is necessary to use prepend function instead of append function because workflow
              // connections has large size and should be rendered at back to be able to click any
              // element.
              that.$workflowStep.prepend(workflowConnectionView.render().$el);

              if (canPush !== false) {
                that.views.workflowConnectionViewArray.push(workflowConnectionView);
                that.models.workflowConnectionViewModelCollection.add(
                    workflowConnectionView.options.viewModel
                );
              }

              return workflowConnectionView;
            },
            /**
             * The function to render workflow items.
             * @protected
             *
             * @param {!WorkflowItem} workflowItem
             * The workflow item.
             * @param {!string} workflowItem.attributeName
             * The name of attribute for workflow diagram model.
             * @param {!string} workflowItem.viewArrayName
             * The name of managed view array.
             * @param {!string} workflowItem.viewModelCollectionName
             * The name of managed view model collection.
             * @param {!string} workflowItem.renderFunctionName
             * The name of render function.
             *
             * @returns {!mwa.View[]}
             * The rendered workflow item view instances.
             */
            renderWorkflowItems: function(workflowItem) {
              var that = this;

              // Initialization
              mwa.proui._.invoke(that.views[workflowItem.viewArrayName], 'remove');
              that.views[workflowItem.viewArrayName] = [];
              // CAUTION:
              // It is important not to trigger reset event in this case in order that
              // change:[attribute] event won't be occurred.
              that.models[workflowItem.viewModelCollectionName].reset(null, {silent: true});

              // Rendering
              var workflowDiagramModel = that.options.viewModel.get('workflowDiagramModel');
              if (workflowDiagramModel instanceof mwa.model.workflowDiagram.WorkflowDiagramModel) {
                // CAUTION:
                // It isn't necessary to get previous value of steps because it wasn't updated when
                // managed view model collection had been reset with silent option in above
                // initialization.
                var workflowItemArray = workflowDiagramModel.get(workflowItem.attributeName) || [];

                that.views[workflowItem.viewArrayName] =
                    that.views[workflowItem.viewArrayName].concat(
                        workflowItemArray.map(
                            mwa.proui._.bind(
                                mwa.proui._.partial(
                                    that[workflowItem.renderFunctionName],
                                    mwa.proui._, false // arguments
                                ),
                                that
                            )
                        )
                    );
                // CAUTION:
                // It is important to add rendered workflow item view model instances at one time in
                // order that update event won't be occurred multiple times and change:[attribute]
                // event won't be occurred in rendering case. In other words, this is for not
                // detecting unnecessary modification in other views.
                that.models[workflowItem.viewModelCollectionName].add(
                    that.views[workflowItem.viewArrayName].map(
                        function(workflowItemView, index, array) {
                          return workflowItemView.options.viewModel;
                        }
                    ),
                    // CAUTION:
                    // It is necessary to disable sort in this case in order that change:[attribute]
                    // event won't be occurred and unnecessary modification won't be detected in
                    // other views.
                    {sort: false}
                );
              }

              return that.views[workflowItem.viewArrayName];
            },
            /**
             * The function to render workflow information drawer.
             * @protected
             *
             * @returns {!proui.drawer.DrawerView}
             * The rendered drawer view instance.
             */
            renderWorkflowInformationDrawer: function() {
              var that = this;

              // Initialization
              if (that.views.workflowInformationDrawerView != null) {
                that.views.workflowInformationDrawerView.remove();
                that.views.workflowInformationDrawerView = null;
              }

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-canvas-workflow-information-drawer');
              var $contentEl = that.$el.find('.mwa-view-workflow-canvas-workflow-information')
                  // INFORMATION:
                  // It is better to remove proui-state-hidden style class to be able to calculate
                  // the offset correctly.
                  .removeClass('proui-state-hidden');
              var offset =
                  parseInt($contentEl.css('padding-left'), 10) +
                  parseInt($contentEl.css('padding-right'), 10);
              var workflowDiagramModel =
                  that.options.viewModel.get('workflowDiagramModel') ||
                  // CAUTION:
                  // It is necessary for preparing dummy instance without validation because there
                  // is a case that workflow canvas will be rendered without specifying workflow
                  // diagram and validation error will be occurred in such case.
                  new mwa.model.workflowDiagram.WorkflowDiagramModel(
                      {
                        // CAUTION:
                        // It is necessary to set the following dummy values because they will be
                        // required in the rendering.
                        name: mwa.enumeration.Message['TITLE_WORKFLOW']
                      },
                      {validate: false}
                  );
              that.views.workflowInformationDrawerView =
                  new mwa.proui.drawer.DrawerView({
                    el: $el,
                    viewModel: new mwa.proui.drawer.DrawerViewModel({
                      isSelected: false,
                      icon: $el.css('background-image'),
                      offset: offset,
                      position: mwa.proui.expander.Position.RIGHT,
                      create: function(contentView, viewModel) {
                        switch (true) {
                          case contentView instanceof mwa.proui.tab.TabView:
                            that.views.workflowInformationTabView = contentView;
                            break;

                          default:
                            break;
                        }
                      },
                      collapse: function(contentView, viewModel) {
                        $contentEl.addClass('proui-state-hidden');
                      },
                      expand: function(contentView, viewModel) {
                        $contentEl.removeClass('proui-state-hidden');
                      }
                    }),
                    contentModel: new mwa.proui.drawer.DrawerContentModel({
                      viewClass:
                          (that.options.viewModel.get('hasWorkflowMetadataEditor')) ?
                              mwa.proui.tab.TabView :
                              mwa.view.workflowParameterEditor.WorkflowParameterEditorView,
                      viewOptions: {
                        el: $contentEl,
                        viewModel:
                            (that.options.viewModel.get('hasWorkflowMetadataEditor')) ?
                                new mwa.proui.tab.TabViewModel({
                                  isStrong: true,
                                  type: mwa.proui.tab.TabType.PANE,
                                  size: mwa.proui.tab.TabSize.NORMAL,
                                  contentList: new mwa.proui.tab.TabContentCollection([
                                    new mwa.proui.tab.TabContentModel({
                                      title: mwa.enumeration.Message['TITLE_WORKFLOW'],
                                      viewClass: mwa.prouiExt.metadataEditor.MetadataEditorView,
                                      viewOptions: {
                                        viewModel: new mwa.prouiExt.metadataEditor.MetadataEditorViewModel({
                                          hasHeader: false,
                                          itemCollection:
                                              new mwa.model.workflowDiagram.WorkflowDiagramCollection([
                                                workflowDiagramModel
                                              ]),
                                          metadataSchemaCollection:
                                              new mwa.prouiExt.metadataEditor.MetadataSchemaCollection([
                                                {
                                                  title: workflowDiagramModel.get('name'),
                                                  metadataFieldCollection: new mwa.prouiExt.metadataEditor.MetadataFieldCollection([
                                                    {
                                                      key: 'id',
                                                      index: 0,
                                                      title: mwa.enumeration.Message['SENTENCE_ID'],
                                                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                                                      valueCollection: null,
                                                      referenceValueCollection: null,
                                                      metadataFieldValidatorCollection: new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                                                      isArray: false,
                                                      isHidden: false,
                                                      isRequired: true,
                                                      isReadOnly: true
                                                    },
                                                    {
                                                      key:
                                                          (
                                                              workflowDiagramModel instanceof
                                                              mwa.model.workflow.WorkflowModel
                                                          ) ?
                                                              'alias' :
                                                              'name',
                                                      index: 1,
                                                      title: mwa.enumeration.Message['SENTENCE_NAME'],
                                                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                                                      valueCollection: null,
                                                      referenceValueCollection: null,
                                                      metadataFieldValidatorCollection: new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                                                      isArray: false,
                                                      isHidden: false,
                                                      isRequired: true,
                                                      isReadOnly: true
                                                    },
                                                    {
                                                      key: 'version',
                                                      index: 2,
                                                      title: mwa.enumeration.Message['SENTENCE_VERSION'],
                                                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                                                      valueCollection: null,
                                                      referenceValueCollection: null,
                                                      metadataFieldValidatorCollection: new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                                                      isArray: false,
                                                      isHidden: false,
                                                      isRequired: true,
                                                      isReadOnly: true
                                                    },
                                                    {
                                                      key: 'createTime',
                                                      index: 3,
                                                      title: mwa.enumeration.Message['SENTENCE_CREATION_DATE_TIME'],
                                                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.DATE_TIME,
                                                      valueCollection: null,
                                                      referenceValueCollection: null,
                                                      metadataFieldValidatorCollection: new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                                                      isArray: false,
                                                      isHidden: false,
                                                      isRequired: true,
                                                      isReadOnly: true
                                                    },
                                                    {
                                                      key: 'updateTime',
                                                      index: 4,
                                                      title: mwa.enumeration.Message['SENTENCE_UPDATE_DATE_TIME'],
                                                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.DATE_TIME,
                                                      valueCollection: null,
                                                      referenceValueCollection: null,
                                                      metadataFieldValidatorCollection: new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                                                      isArray: false,
                                                      isHidden: false,
                                                      isRequired: true,
                                                      isReadOnly: true
                                                    }
                                                  ])
                                                }
                                              ]),
                                          metadataGroupCollection: new mwa.prouiExt.metadataEditor.MetadataGroupCollection([
                                            {
                                              title: null,
                                              regExp: new RegExp('.*'),
                                              isHidden: false,
                                              isExpanded: true
                                            }
                                          ])
                                        })
                                      }
                                    }),
                                    new mwa.proui.tab.TabContentModel({
                                      title: mwa.enumeration.Message['TITLE_STEP'],
                                      viewClass: mwa.view.workflowParameterEditor.WorkflowParameterEditorView,
                                      viewOptions: {
                                        viewModel: that.createWorkflowParameterEditorViewModel()
                                      }
                                    })
                                  ]),
                                  create: function(contentView, index, viewModel, contentViewOptions) {
                                    switch (true) {
                                      case contentView instanceof mwa.view.workflowParameterEditor.WorkflowParameterEditorView:
                                        that.views.workflowParameterEditorView = contentView;
                                        break;

                                      default:
                                        break;
                                    }
                                  }
                                }) :
                                that.createWorkflowParameterEditorViewModel()
                      },
                      canResize: true,
                      width: 500 - offset,
                      minWidth: 500 - offset,
                      maxWidth: 700,
                      height: '100%'
                    })
                  });
              $el.css('background-image', 'none');
              $contentEl.toggleClass(
                  'proui-state-hidden',
                  !that.views.workflowInformationDrawerView.options.viewModel.get('isSelected')
              );

              return that.views.workflowInformationDrawerView;
            },
            /**
             * The function to create workflow parameter editor view model.
             * @protected
             *
             * @param {?object} [attributes]
             * The object including various attribute values of workflow parameter editor view
             * model.
             *
             * @returns {mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel}
             * The created workflow parameter editor view model instance.
             */
            createWorkflowParameterEditorViewModel: function(attributes) {
              var that = this;

              return new mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel(
                  mwa.proui._.extend(
                      {
                        isReadOnly: that.options.viewModel.get('isReadOnly'),
                        activityParameterTypeCollection:
                            that.options.viewModel.get('activityParameterTypeCollection'),
                        workflowStepViewModelCollection:
                            that.models.workflowStepViewModelCollection,
                        workflowConnectionPortViewModelCollection:
                            that.models.workflowConnectionPortViewModelCollection,
                        workflowConnectionViewModelCollection:
                            that.models.workflowConnectionViewModelCollection
                      },
                      attributes
                  )
              );
            },
            /**
             * @see {@link mwa.View.applyChanges}
             * @public
             * @override
             */
            applyChanges: function(model, options) {
              mwa.View.prototype.applyChanges.apply(this, [model, options]);

              // CAUTION:
              // It is necessary to call applyCanDrop function because applyChanges function is
              // called when isDisabled attribute of view model is changed and droppable settings
              // should be updated according to the values of isDisabled and canDrop.
              this.applyCanDrop(
                  this.options.viewModel,
                  this.options.viewModel.get('canDrop'),
                  null
              );
            },
            /**
             * The event handler for changing, adding, removing, updating, resetting and destroying
             * models in workflowStepViewModelCollection. This function updates workflow diagram
             * according to the value.
             * @protected
             */
            applyWorkflowStepViewModelCollection: function() {
              // arguments:
              // workflowStepViewModelCollection
              //  - "change"                   (model, options)
              //  - "change:[attribute]"       (model, value, options)
              //  - "add", "remove", "destroy" (model, collection, options)
              //  - "update", "reset"          (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                  if (arguments[1] !== that.models.workflowStepViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // CAUTION:
                    // It is important not to update steps attribute of workflow diagram model
                    // during drag in consideration of performance.
                    if (!args.model.get('isDragging')) {
                      that.options.viewModel.get('workflowDiagramModel').set(
                          'steps',
                          that.models.workflowStepViewModelCollection.toObject(),
                          {change: args}
                      );
                    }
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add, remove or destroy event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The added, removed or destroyed model.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModelCollection} collection
                     * The collection that was added removed or destroyed the model.
                     * @property {object} options
                     * The options of add, remove or destroy event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      if (args.options.index != null) {
                        // Remove Case

                        // If you need any processing, add here.
                      } else {
                        // Destroy Case

                        // To do same processing as update event.
                        that.applyWorkflowStepViewModelCollection(
                            that.models.workflowStepViewModelCollection,
                            args.options
                        );
                      }
                    }
                  }
                } else if (arguments[0] === that.models.workflowStepViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  // CAUTION:
                  // In add event case, the view instances should have been already added to the
                  // managed array, for example using render function.
                  that.views.workflowStepViewArray =
                      that.views.workflowStepViewArray.filter(
                          function(workflowStepView, index, array) {
                            return that.models.workflowStepViewModelCollection.contains(
                                workflowStepView.options.viewModel
                            );
                          }
                      );
                  that.options.viewModel.get('workflowDiagramModel').set(
                      'steps',
                      that.models.workflowStepViewModelCollection.toObject(),
                      {update: args}
                  );
                }
              }
            },
            /**
             * The event handler for changing, adding, removing, updating, resetting and destroying
             * models in workflowConnectionPortViewModelCollection. This function updates workflow
             * diagram according to the value.
             * @protected
             */
            applyWorkflowConnectionPortViewModelCollection: function() {
              // arguments:
              // workflowConnectionPortViewModelCollection
              //  - "change"                   (model, options)
              //  - "change:[attribute]"       (model, value, options)
              //  - "add", "remove", "destroy" (model, collection, options)
              //  - "update", "reset"          (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] instanceof mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel) {
                  if (arguments[1] !== that.models.workflowConnectionPortViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // CAUTION:
                    // It is important not to update connectionPorts attribute of workflow diagram
                    // model during drag in consideration of performance.
                    if (!args.model.get('isDragging')) {
                      that.options.viewModel.get('workflowDiagramModel').set(
                          'connectionPorts',
                          that.models.workflowConnectionPortViewModelCollection.toObject(),
                          {change: args}
                      );
                    }
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add, remove or destroy event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The added, removed or destroyed model.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                     * The collection that was added removed or destroyed the model.
                     * @property {object} options
                     * The options of add, remove or destroy event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    var type = args.model.get('type');

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // CAUTION:
                      // It is important to create a view instance for data workflow connection port
                      // because it is added by workflow parameter editor for start and end workflow
                      // step without the view.
                      if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                          type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                        // CAUTION:
                        // It is necessary to confirm whether the added workflow connection port
                        // view model is already displayed not to render unnecessary view.
                        if (!that.views.workflowConnectionPortViewArray.some(
                            function(workflowConnectionPortView, index, array) {
                              return (
                                  workflowConnectionPortView.options.viewModel === args.model
                              );
                            }
                            )) {
                          var workflowConnectionPortView =
                              new mwa.view.workflowConnectionPort.WorkflowConnectionPortView({
                                viewModel: args.model.set({
                                  // INFORMATION:
                                  // The data workflow connection port always should be hidden in
                                  // current specification.
                                  isHidden: true,
                                  canConnect: that.options.viewModel.get('canConnect'),
                                  connect: mwa.proui._.bind(that._connectWorkflowConnection, that)
                                })
                              });

                          that.$workflowStep.append(workflowConnectionPortView.render().$el);

                          that.views.workflowConnectionPortViewArray.splice(
                              index, 0, workflowConnectionPortView
                          );
                        }
                      }
                    } else {
                      if (args.options.index != null) {
                        // Remove Case

                        // If you need any processing, add here.
                      } else {
                        // Destroy Case

                        // To do same processing as update event.
                        that.applyWorkflowConnectionPortViewModelCollection(
                            that.models.workflowConnectionPortViewModelCollection,
                            args.options
                        );
                      }
                    }
                  }
                } else if (arguments[0] === that.models.workflowConnectionPortViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  // CAUTION:
                  // In add event case, the view instances should have been already added to the
                  // managed array, for example using render function.
                  that.views.workflowConnectionPortViewArray =
                      that.views.workflowConnectionPortViewArray.filter(
                          function(workflowConnectionPortView, index, array) {
                            return that.models.workflowConnectionPortViewModelCollection.contains(
                                workflowConnectionPortView.options.viewModel
                            );
                          }
                      );
                  that.options.viewModel.get('workflowDiagramModel').set(
                      {
                        // CAUTION: NVX-5595 (https://www.tool.sony.biz/jira/browse/NVX-5595)
                        // It is necessary to update steps attribute of workflow diagram model
                        // together because inputs and outputs attribute of activity model for start
                        // and end step should be updated.
                        steps:
                            that.models.workflowStepViewModelCollection.toObject(),
                        connectionPorts:
                            that.models.workflowConnectionPortViewModelCollection.toObject()
                      },
                      {update: args}
                  );
                }
              }
            },
            /**
             * The event handler for changing, adding, removing, updating, resetting and destroying
             * models in workflowConnectionViewModelCollection. This function updates workflow
             * diagram according to the value.
             * @protected
             */
            applyWorkflowConnectionViewModelCollection: function() {
              // arguments:
              // workflowConnectionViewModelCollection
              //  - "change"                   (model, options)
              //  - "change:[attribute]"       (model, value, options)
              //  - "add", "remove", "destroy" (model, collection, options)
              //  - "update", "reset"          (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] instanceof mwa.view.workflowConnection.WorkflowConnectionViewModel) {
                  if (arguments[1] !== that.models.workflowConnectionViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    that.options.viewModel.get('workflowDiagramModel').set(
                        'connections',
                        that.models.workflowConnectionViewModelCollection.toObject(),
                        {change: args}
                    );
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add, remove or destroy event.
                     * @property {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
                     * The added, removed or destroyed model.
                     * @property {mwa.view.workflowConnection.WorkflowConnectionViewModelCollection} collection
                     * The collection that was added removed or destroyed the model.
                     * @property {object} options
                     * The options of add, remove or destroy event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      if (args.options.index != null) {
                        // Remove Case

                        // If you need any processing, add here.
                      } else {
                        // Destroy Case

                        // To do same processing as update event.
                        that.applyWorkflowConnectionViewModelCollection(
                            that.models.workflowConnectionViewModelCollection,
                            args.options
                        );
                      }
                    }
                  }
                } else if (arguments[0] === that.models.workflowConnectionViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowConnection.WorkflowConnectionViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  // CAUTION:
                  // In add event case, the view instances should have been already added to the
                  // managed array, for example using render function.
                  that.views.workflowConnectionViewArray =
                      that.views.workflowConnectionViewArray.filter(
                          // INFORMATION:
                          // It isn't necessary to consider about workflow connection view removal
                          // because it is automatically removed when destroying the view model.
                          function(workflowConnectionView, index, array) {
                            return that.models.workflowConnectionViewModelCollection.contains(
                                workflowConnectionView.options.viewModel
                            );
                          }
                      );

                  that.options.viewModel.get('workflowDiagramModel').set(
                      'connections',
                      that.models.workflowConnectionViewModelCollection.toObject(),
                      {update: args}
                  );
                }
              }
            },
            /**
             * The event handler for changing hasGrid attribute of view model. This function shows
             * or hides grid according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyHasGrid: function(model, value, options) {
              this.$el.children().toggleClass(
                  'mwa-view-workflow-canvas-grid', this.options.viewModel.get('hasGrid')
              );
            },
            /**
             * The event handler for changing hasInformationButton attribute of view model. This
             * function shows or hides workflow information drawer according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyHasInformationButton: function(model, value, options) {
              if (this.views != null && this.views.workflowInformationDrawerView != null) {
                this.views.workflowInformationDrawerView.$el.toggle(
                    this.options.viewModel.get('hasInformationButton')
                );
              }
            },
            /**
             * The event handler for changing isReadOnly attribute of view model. This function
             * updates canSelect attribute of workflow connection view model and isReadOnly
             * attribute of workflow parameter editor view model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsReadOnly: function(model, value, options) {
              var isReadOnly = this.options.viewModel.get('isReadOnly');

              this.models.workflowConnectionViewModelCollection.invoke(
                  'set',
                  {canSelect: this.options.viewModel.get('canSelect') && !isReadOnly}
              );

              this.views.workflowParameterEditorView.options.viewModel.set(
                  'isReadOnly',
                  isReadOnly
              );
            },
            /**
             * The event handler for changing canSelect attribute of view model. This function
             * updates canSelect attribute of workflow step view model and workflow connection view
             * model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanSelect: function(model, value, options) {
              var canSelect = this.options.viewModel.get('canSelect');

              this.models.workflowStepViewModelCollection.invoke(
                  'set',
                  {canSelect: canSelect}
              );
              this.models.workflowConnectionViewModelCollection.invoke(
                  'set',
                  {canSelect: canSelect && !this.options.viewModel.get('isReadOnly')}
              );
            },
            /**
             * The event handler for changing canDrag attribute of view model. This function updates
             * canDrag attribute of workflow step view model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDrag: function(model, value, options) {
              var canDrag = this.options.viewModel.get('canDrag');

              this.models.workflowStepViewModelCollection.invoke(
                  'set',
                  {canDrag: canDrag}
              );
            },
            /**
             * The event handler for changing canDrop attribute of view model. This function adds or
             * removes droppable settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDrop: function(model, value, options) {
              var that = this;

              if (that.$workflowCanvas != null) {
                var isDisabled = that.options.viewModel.get('isDisabled');
                var canDrop = !isDisabled && that.options.viewModel.get('canDrop');

                if (that.$workflowCanvas.hasClass('proui-state-droppable')) {
                  that.$workflowCanvas.droppable('destroy');
                }
                that.$workflowCanvas.toggleClass('proui-state-droppable', canDrop);

                if (canDrop) {
                  that.$workflowCanvas.droppable({
                    greedy: true,
                    tolerance: 'pointer',
                    hoverClass: 'proui-state-selecting',
                    accept: function($draggable) {
                      var options = ($draggable.data('view') || {options: {}}).options;

                      return (options.model || {}) instanceof mwa.model.activity.ActivityModel;
                    },
                    drop: function(event, ui) {
                      var $droppable = mwa.proui.Backbone.$(this);
                      var options = ui.draggable.data('view').options;

                      //// Workflow Step ////
                      var magnificationFactor =
                          0.01 * that.options.viewModel.get('enlargementFactor');
                      var workflowStepViewModel = that.renderWorkflowStep({
                        id: mwa.proui.util.Uuid.generateUuidV4(),
                        name: options.model.get('alias'),
                        // INFORMATION:
                        // To be careful about attribute name.
                        position: {
                          // INFORMATION:
                          // - How do I get the coordinate position after using jQuery drag and drop?
                          //   (http://stackoverflow.com/questions/849030/how-do-i-get-the-coordinate-position-after-using-jquery-drag-and-drop)
                          x: ui.offset.left - $droppable.offset().left +
                              mwa.proui._.result(
                                  mwa.view.workflowStep.WorkflowStepViewModel.prototype, 'defaults'
                              ).width * magnificationFactor / 2 +
                              // CAUTION:
                              // It is necessary to consider about grab scroll.
                              that.$workflowStep.scrollLeft(),
                          y: ui.offset.top - $droppable.offset().top +
                              mwa.proui._.result(
                                  mwa.view.workflowStep.WorkflowStepViewModel.prototype, 'defaults'
                              ).height * magnificationFactor / 2 +
                              // CAUTION:
                              // It is necessary to consider about grab scroll.
                              that.$workflowStep.scrollTop(),
                          z: 0
                        },
                        // CAUTION:
                        // It is important to use toObject function instead of toJSON function in
                        // consideration that activity model is being parsed.
                        activity: options.model.toJSON()
                      }).options.viewModel;

                      //// Workflow Connection Port ////
                      var workflowStepActivityName = options.model.get('name');
                      var workflowStepActivityType = options.model.get('type');
                      // INFORMATION:
                      // To be careful about attribute name.
                      var workflowStepPositions = workflowStepViewModel.get('positions');
                      //// Workflow Connection Port (Control) ////
                      if (workflowStepActivityType !== mwa.enumeration.ActivityType.START) {
                        var inputControlWorkflowConnectionPortView =
                            that.renderWorkflowConnectionPort({
                              id: mwa.proui.util.Uuid.generateUuidV4(),
                              // INFORMATION:
                              // To be careful about attribute name.
                              position: {
                                x: workflowStepPositions.x - workflowStepViewModel.get('width') / 2,
                                y: workflowStepPositions.y,
                                z: workflowStepPositions.z
                              },
                              type: mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT,
                              stepId: workflowStepViewModel.get('id')
                            });
                      }
                      if (workflowStepActivityType !== mwa.enumeration.ActivityType.END) {
                        var outputControlWorkflowConnectionPortView =
                            that.renderWorkflowConnectionPort({
                              id: mwa.proui.util.Uuid.generateUuidV4(),
                              // INFORMATION:
                              // To be careful about attribute name.
                              position: {
                                x: workflowStepPositions.x + workflowStepViewModel.get('width') / 2,
                                y: workflowStepPositions.y,
                                z: workflowStepPositions.z
                              },
                              type: mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT,
                              stepId: workflowStepViewModel.get('id')
                            });
                      }
                      if (workflowStepActivityName ===
                          mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH.templateName) {
                        that.renderWorkflowConnectionPort({
                          id: mwa.proui.util.Uuid.generateUuidV4(),
                          // INFORMATION:
                          // To be careful about attribute name.
                          position: {
                            x: workflowStepPositions.x,
                            y: workflowStepPositions.y + workflowStepViewModel.get('height') / 2,
                            z: workflowStepPositions.z
                          },
                          type: mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT,
                          stepId: workflowStepViewModel.get('id')
                        });

                        // INFORMATION:
                        // The following processing means to set a decided value to
                        // conditionConnectionPortId parameter.
                        var inputActivityParameterModel =
                            workflowStepViewModel
                                .get('inputActivityParameterCollection')
                                // INFORMATION:
                                // It is possible to use get function instead of findWhere function
                                // in consideration of that idAttribute of activity parameter model
                                // is "key", not "id", but it is better to use findWhere function to
                                // make the code maintainable and readable.
                                .findWhere({
                                  key: mwa.enumeration.ActivityParameter.CONDITION_CONNECTION_PORT_ID.key
                                });
                        inputActivityParameterModel.set(
                            'value',
                            inputActivityParameterModel.get('type').encodeText(
                                outputControlWorkflowConnectionPortView.options.viewModel.get('id')
                            )
                        );
                      }
                      //// Workflow Connection Port (Data) ////
                      workflowStepViewModel.get('inputActivityParameterCollection').forEach(
                          function(inputActivityParameterModel, index, array) {
                            that.renderWorkflowConnectionPort({
                              // CAUTION:
                              // The ID will be set to corresponding activity parameter in the
                              // initialization of data workflow connection port to make the
                              // relationship.
                              id: mwa.proui.util.Uuid.generateUuidV4(),
                              // INFORMATION:
                              // To be careful about attribute name.
                              position: {
                                x: 0,
                                y: 0,
                                z: 0
                              },
                              type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT,
                              stepId: workflowStepViewModel.get('id'),
                              // INFORMATION:
                              // To be careful about attribute name.
                              parameter: inputActivityParameterModel.toObject(false)
                            });
                          }
                      );
                      workflowStepViewModel.get('outputActivityParameterCollection').forEach(
                          function(outputActivityParameterModel, index, array) {
                            that.renderWorkflowConnectionPort({
                              // CAUTION:
                              // The ID will be set to corresponding activity parameter in the
                              // initialization of data workflow connection port to make the
                              // relationship.
                              id: mwa.proui.util.Uuid.generateUuidV4(),
                              // INFORMATION:
                              // To be careful about attribute name.
                              position: {
                                x: 0,
                                y: 0,
                                z: 0
                              },
                              type: mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT,
                              stepId: workflowStepViewModel.get('id'),
                              // INFORMATION:
                              // To be careful about attribute name.
                              parameter: outputActivityParameterModel.toObject(false)
                            });
                          }
                      );
                    }
                  });
                }
              }
            },
            /**
             * The event handler for changing canDelete attribute of view model. This function
             * updates canDelete attribute of workflow step view model and workflow connection view
             * model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDelete: function(model, value, options) {
              var canDelete = this.options.viewModel.get('canDelete');

              this.models.workflowStepViewModelCollection.invoke(
                  'set',
                  {canDelete: canDelete}
              );
              this.models.workflowConnectionViewModelCollection.invoke(
                  'set',
                  {canDelete: canDelete}
              );
            },
            /**
             * The event handler for changing canConnect attribute of view model. This function
             * updates canConnect attribute of workflow connection port view model according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanConnect: function(model, value, options) {
              var canConnect = this.options.viewModel.get('canConnect');

              this.models.workflowConnectionPortViewModelCollection.invoke(
                  'set',
                  {canConnect: canConnect}
              );
            },
            /**
             * The event handler for changing workflowDiagramModel attribute of view model and
             * changing, destroying, syncing and failed of workflowDiagramModel. This function
             * renders this view according to the value.
             * @protected
             */
            applyWorkflowDiagramModel: function() {
              // arguments:
              // WorkflowCanvasViewModel
              //  - "change:workflowDiagramModel" (model, value, options)
              // workflowDiagramModel
              //  - "change"                      (model, options)
              //  - "change:[attribute]"          (model, value, options)
              //  - "destroy"                     (model, collection, options)
              //  - "sync", "error"               (model, response, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that.options.viewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
                   * The mwa workflow canvas view model.
                   * @property {mwa.model.workflowDiagram.WorkflowDiagramModel} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // CAUTION: NVX-5123 (https://www.tool.sony.biz/jira/browse/NVX-5123)
                  // It is necessary to stop listeners tentatively before re-rendering because some
                  // listeners will update attributes of new workflow diagram model unintentionally
                  // when sub views (e.g. workflow step, workflow connection port and workflow
                  // connection) are removed. Actually, it isn't necessary to stop all listeners but
                  // it is better for maintenance reason. And also, it isn't necessary to remove sub
                  // views before re-rendering because internal managed view model instances (e.g.
                  // workflowStepViewModelCollection, workflowConnectionPortViewModelCollection and
                  // workflowConnectionViewModelCollection) will be changed in listen function and
                  // unintended update won't be happened but it is better in order to succeed the
                  // re-rendering certainly.
                  that.stopListening();
                  that.remove({silent: true});
                  that.listen();

                  that.render();
                } else {
                  if (!(arguments[1] instanceof mwa.proui.Backbone.Collection)) {
                    if (arguments.length === 2) { // TODO: To detect change:[attribute] event.
                      /**
                       * @type {object}
                       * @description The arguments of change event.
                       * @property {mwa.model.workflowDiagram.WorkflowDiagramModel} model
                       * The changed model.
                       * @property {*} value
                       * The new changed value.
                       * @property {object} options
                       * The options of change event.
                       */
                      args = {
                        model: arguments[0],
                        value: (arguments.length === 2) ? null : arguments[1],
                        options: (arguments.length === 2) ? arguments[1] : arguments[2]
                      };

                      // Change Case

                      // TODO: To implement undo and redo features.
                      mwa.proui.util.Log.log('Workflow Diagram :');
                      mwa.proui.util.Log.log(args.model.previousAttributes());
                      mwa.proui.util.Log.log(args.model.changedAttributes());
                      mwa.proui.util.Log.log(args.options);
                    } else {
                      /**
                       * @type {object}
                       * @description The arguments of sync or error event.
                       * @property {mwa.model.workflowDiagram.WorkflowDiagramModel} model
                       * The model that was synced or failed.
                       * @property {object} response
                       * The response of request.
                       * @property {object} options
                       * The options of sync or error event.
                       */
                      args = {
                        model: arguments[0],
                        response: arguments[1],
                        options: arguments[2]
                      };

                      if (mwa.proui.util.Http.isRequestSuccess(args.response)) {
                        // Sync Case

                        // If you need any processing, add here.
                      } else {
                        // Error Case

                        // If you need any processing, add here.
                      }
                    }
                  } else {
                    // Destroy Case

                    // If you need any processing, add here.
                  }
                }
              }
            },
            /**
             * The event handler for changing workflowInstanceModel attribute of view model. This
             * function fetches its child activity instances to display the status on workflow steps
             * according to the value.
             * @protected
             *
             * @param {mwa.view.workflowCanvas.WorkflowCanvasViewModel} model
             * The mwa workflow canvas view model.
             * @param {mwa.model.activityInstance.ActivityInstanceModel} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyWorkflowInstanceModel: function(model, value, options) {
              var that = this;

              that.models.workflowStepViewModelCollection.invoke(
                  'set', {activityInstanceModel: null}
              );

              var workflowInstanceModel = that.options.viewModel.get('workflowInstanceModel');
              if (typeof workflowInstanceModel !== 'undefined' && workflowInstanceModel != null) {
                that.renderLoading();

                var activityInstanceCollection =
                    new mwa.model.activityInstance.ActivityInstanceCollection();
                activityInstanceCollection.fetch({
                  data: {
                    filter: [
                      'parentId' + '==' + workflowInstanceModel.get('id')
                    ]
                  },
                  success: function(collection, response, options) {
                    activityInstanceCollection.forEach(function(model, index, array) {
                      var workflowStepViewModel =
                          // CAUTION:
                          // The activity instance name must be workflow step ID.
                          that.models.workflowStepViewModelCollection.get(model.get('name'));
                      if (typeof workflowStepViewModel !== 'undefined' &&
                          workflowStepViewModel != null) {
                        workflowStepViewModel.set('activityInstanceModel', model);
                      }
                    });
                  },
                  complete: function(collection, response, options) {
                    that.removeLoading();
                  }
                });
              }
            },
            /**
             * The event handler for sorting models in workflowStepViewModelCollection,
             * workflowConnectionPortViewModelCollection and workflowConnectionViewModelCollection.
             * This function updates workflow diagram according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModelCollection|mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection|mwa.view.workflowConnection.WorkflowConnectionViewModelCollection} collection
             * The collection that was sorted.
             * @param {object} options
             * The options of sort event.
             */
            sortWorkflowItems: function(collection, options) {
              var workflowItem =
                  mwa.proui._.values(WorkflowItem).find(
                      function(workflowItem, index, array) {
                        return collection instanceof workflowItem.viewModelCollectionClass;
                      }
                  );

              if (typeof workflowItem === 'object') {
                var workflowDiagramModel = this.options.viewModel.get('workflowDiagramModel');

                if (workflowDiagramModel instanceof mwa.model.workflowDiagram.WorkflowDiagramModel) {
                  var workflowItemArray = workflowDiagramModel.get(workflowItem.attributeName);

                  // CAUTION:
                  // It is important to check whether the workflow item array isn't null. In this
                  // case, workflow diagram model shouldn't be updated and keep the null value for
                  // the attribute.
                  if (mwa.proui.util.Object.isArray(workflowItemArray)) {
                    if (workflowItemArray.length === collection.length) {
                      // CAUTION:
                      // It is important to update workflow diagram model without change:[attribute]
                      // event because there is a case that sort event will be occurred in rendering
                      // of other views (e.g. workflow parameter editor). the
                      // change:[attribute] event shouldn't be occurred in order not to detect
                      // unnecessary modification in other views.
                      //  - Is there a way to use Array.splice in javascript with the third parameter as an array?
                      //    (https://stackoverflow.com/questions/14715459/is-there-a-way-to-use-array-splice-in-javascript-with-the-third-parameter-as-an)
                      Array.prototype.splice.apply(
                          workflowItemArray,
                          [0, workflowItemArray.length].concat(collection.toObject())
                      );
                    } else {
                      // CAUTION:
                      // It is important to update workflow diagram model with change:[attribute]
                      // event because there is a case that sort event will be occurred in dropping
                      // of other views (e.g. workflow step). In this case, the change:[attribute]
                      // event should be occurred in order to detect necessary modification in other
                      // views. And also, be careful about that sort event will be occurred before
                      // update event.
                      workflowDiagramModel.set(workflowItem.attributeName, collection.toObject());
                    }
                  }
                }
              }
            },
            /**
             * @see {@link mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel.connect}
             * @private
             */
            _connectWorkflowConnection: function(workflowConnectionView) {
              var that = this;

              var fromWorkflowConnectionPortViewModel =
                  workflowConnectionView.options.viewModel
                      .get('fromWorkflowConnectionPortViewModel');
              var toWorkflowConnectionPortViewModel =
                  workflowConnectionView.options.viewModel
                      .get('toWorkflowConnectionPortViewModel');

              // CAUTION:
              // It is important to replace workflow connection view model instances to use the
              // instances certainly that are managed in this view. Actually, change event won't be
              // occurred if the instances are already same as the managed instances (e.g. control
              // workflow connection case).
              workflowConnectionView.options.viewModel.set({
                // INFORMATION:
                // The data workflow connection always should be hidden in current specification.
                isHidden:
                    fromWorkflowConnectionPortViewModel.get('type') ===
                    mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT &&
                    toWorkflowConnectionPortViewModel.get('type') ===
                    mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT,
                fromWorkflowConnectionPortViewModel:
                    that.models.workflowConnectionPortViewModelCollection.get(
                        fromWorkflowConnectionPortViewModel.get('id')
                    ),
                toWorkflowConnectionPortViewModel:
                    that.models.workflowConnectionPortViewModelCollection.get(
                        toWorkflowConnectionPortViewModel.get('id')
                    )
              });

              that.views.workflowConnectionViewArray.push(workflowConnectionView);
              that.models.workflowConnectionViewModelCollection.add(
                  workflowConnectionView.options.viewModel
              );
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowCanvas;
}));
