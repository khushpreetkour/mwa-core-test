/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.view == null || mwa.view.workflowStep == null) ? './mwa.view.workflow-step' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.view == null || mwa.view.workflowStep == null) ? require('./mwa.view.workflow-step') : {}
    );
  } else {
    root.mwa.view.workflowConnectionPort = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.view.workflowStep
    );
  }
}(this, function(base, subBase, workflowStep) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.view.workflowStep namespace.
  if (mwa.view.workflowStep == null) {
    mwa.view.workflowStep = workflowStep;
  }

  /**
   * The mwa.view.workflowConnectionPort namespace.
   * @namespace
   */
  mwa.view.workflowConnectionPort = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowConnectionPort alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow connection port view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
     * @memberof mwa.view.workflowConnectionPort
     * @extends {mwa.ViewModel}
     */
    global.WorkflowConnectionPortViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    id: '',
                    key: '',
                    isHidden: false,
                    isRequired: true,
                    isDragging: false,
                    isConnected: false,
                    canConnect: false,
                    positions: null,
                    type: null,
                    typeName: null,
                    color: null,
                    workflowStepViewModel: null,
                    connect: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  // proui.ViewModel
                  id: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  // mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
                  key: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  isHidden: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isRequired: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isDragging: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isConnected: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canConnect: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  positions: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isPlainObject(value))) {
                          msg = 'The "' + attr + '" must be object.';
                        } else if (value.x == null || value.y == null || value.z == null) {
                          msg = 'The "' + attr + '" must have properties (x, y, z).';
                        }
                        return msg;
                      }
                    }
                  ],
                  type: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = 'The "' + attr + '" must be mwa.enumeration.WorkflowConnectionPortType.';
                        for (var propertyName in mwa.enumeration.WorkflowConnectionPortType) {
                          if (mwa.enumeration.WorkflowConnectionPortType.hasOwnProperty(propertyName)) {
                            // INFORMATION:
                            // Don't check whether the instance is matching with enumeration object
                            // because it is difficult for development phase to fit it.
                            if (mwa.proui._.isEqual(value, mwa.enumeration.WorkflowConnectionPortType[propertyName])) {
                              msg = null;
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  typeName: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  color: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = 'The "' + attr + '" must be mwa.enumeration.WorkflowConnectionPortColor.';
                        for (var propertyName in mwa.enumeration.WorkflowConnectionPortColor) {
                          if (mwa.enumeration.WorkflowConnectionPortColor.hasOwnProperty(propertyName)) {
                            // INFORMATION:
                            // Don't check whether the instance is matching with enumeration object
                            // because it is difficult for development phase to fit it.
                            if (mwa.proui._.isEqual(value, mwa.enumeration.WorkflowConnectionPortColor[propertyName])) {
                              msg = null;
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  workflowStepViewModel: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowStep.WorkflowStepViewModel
                    }
                  ],
                  connect: [
                    {
                      required: false
                    },
                    {
                      type: 'function'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {string} [attributes.id = '']
             * The ID of workflow connection port.
             * @param {string} [attributes.key = '']
             * The string that is only for data workflow connection port to specify corresponding
             * activity parameter in related workflow step.
             * @param {boolean} [attributes.isHidden = false]
             * The flag whether workflow connection port is hidden.
             * @param {boolean} [attributes.isRequired = true]
             * The flag whether workflow connection port must be connected or have activity
             * parameter value.
             * @param {boolean} [attributes.isDragging = false]
             * The flag whether workflow connection port is been dragging.
             * @param {boolean} [attributes.isConnected = false]
             * The flag whether workflow connection port is connected to workflow connection.
             * @param {boolean} [attributes.canConnect = false]
             * The flag whether workflow connection port is connectable to workflow connection.
             * @param {object} [attributes.positions = null]
             * The position of workflow connection port.
             * @param {mwa.enumeration.WorkflowConnectionPortType} [attributes.type = null]
             * The type of workflow connection port.
             * @param {string} [attributes.typeName = null]
             * The type name of workflow connection port.
             * @param {mwa.enumeration.WorkflowConnectionPortColor} [attributes.color = null]
             * The color of workflow connection port.
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} [attributes.workflowStepViewModel = null]
             * The workflow step view model that workflow connection port is vested in.
             * @param {function} [attributes.connect = null]
             * The callback function. This function is called when workflow connection port is
             * connected.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, [attributes, options]);

              for (var propertyName in attributes) {
                if (attributes.hasOwnProperty(propertyName)) {
                  if ((propertyName === 'connect') &&
                      mwa.proui._.isFunction(attributes[propertyName])) {
                    this[propertyName] = attributes[propertyName];
                  }
                }
              }

              this.applyWorkflowStepViewModel(this, this.get('workflowStepViewModel'), null);
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              this.listenTo(
                  this,
                  'change:typeName',
                  this.applyTypeName
              );

              this.listenTo(
                  this,
                  'change:color',
                  this.applyColor
              );

              this.listenToWorkflowStepViewModel(
                  this,
                  this.get('workflowStepViewModel'),
                  null
              );
              this.listenTo(
                  this,
                  'change:workflowStepViewModel',
                  this.listenToWorkflowStepViewModel
              );
              this.listenTo(
                  this,
                  'change:workflowStepViewModel',
                  this.applyWorkflowStepViewModel
              );

              this.listenTo(
                  this,
                  'change:connect',
                  this.applyConnect
              );
            },
            /**
             * The event handler for changing workflowStepViewModel attribute of view model. This
             * function removes listeners from old workflowStepViewModel and adds listeners to new
             * workflowStepViewModel.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowStepViewModel: function(model, value, options) {
              var type = this.get('type');

              var previousValue = model.previous('workflowStepViewModel');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
                if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                  this.stopListening(previousValue.get(type.workflowStepParameterName));
                }
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);
                if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                  this.stopListening(value.get(type.workflowStepParameterName));
                }

                this.listenTo(value, 'change:isDragging', this.applyWorkflowStepIsDragging);
                this.listenTo(value, 'change:positions', this.applyWorkflowStepPositions);

                if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                  // CAUTION:
                  // It is necessary to define the same listeners in view model and view in
                  // consideration of each responsibility because unexpected null pointer exception
                  // will be occurred after adding a new workflow parameter without creating the
                  // corresponding view if activity parameter ID will be updated in the listener of
                  // view.

                  this.listenToActivityParameterCollection(
                      value,
                      value.get(type.workflowStepParameterName),
                      null
                  );
                  this.listenTo(
                      value,
                      'change:' + type.workflowStepParameterName,
                      this.listenToActivityParameterCollection
                  );
                }
              }
            },
            /**
             * The event handler for changing inputActivityParameterCollection or
             * outputActivityParameterCollection attribute of workflow step view model. This
             * function removes listeners from old ActivityParameterCollection and adds listeners to
             * new ActivityParameterCollection.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activityParameter.ActivityParameterCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToActivityParameterCollection: function(model, value, options) {
              var type = this.get('type');

              var previousValue = model.previous(type.workflowStepParameterName);
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'change:value', this.applyActivityParameterCollection);
                this.listenTo(value, 'update', this.applyActivityParameterCollection);
                this.listenTo(value, 'reset', this.applyActivityParameterCollection);
              }
            },
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              var json = this.toJSON();
              return {
                id: json.id,
                stepId: json.workflowStepViewModel.get('id'),
                // INFORMATION:
                // To be careful about attribute name.
                position: json.positions,
                // CAUTION:
                // The type of value is mwa.enumeration.WorkflowConnectionPortType. This value
                // should be converted to string for as the part of request in API call.
                type: json.type,
                // INFORMATION:
                // To be careful about attribute name.
                parameter: (
                    (json.type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    json.type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) ?
                        // CAUTION:
                        // It is necessary to use findWhere function instead of get function because
                        // ID attribute of activity parameter model is "key", not "id".
                        json.workflowStepViewModel.get(json.type.workflowStepParameterName)
                            .findWhere({id: json.id}).toObject(true) :
                        null
                )
              };
            },
            /**
             * The function to convert to activity parameter model.
             * @public
             *
             * @returns {?mwa.model.activityParameter.ActivityParameterModel}
             * The converted activity parameter model.
             */
            toActivityParameterModel: function() {
              var key = this.get('key');
              var type = this.get('type');
              var color = this.get('color');

              return (
                  (
                      type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                      type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT
                  ) ?
                      // INFORMATION:
                      // It isn't possible to use clone function for this implementation because
                      // validation won't be ignored when creating cloned instance.
                      new mwa.model.activityParameter.ActivityParameterModel(
                          mwa.proui._.extend(
                              // INFORMATION:
                              // It is better to pass an empty object to make a new instance
                              // certainly.
                              {},
                              (
                                  this.get('workflowStepViewModel')
                                  // INFORMATION:
                                  // It is possible to use get function instead of findWhere
                                  // function in consideration of that idAttribute of activity
                                  // parameter model is "key", not "id",  but it is better to use
                                  // findWhere function to make the code maintainable and readable.
                                  // On the other hand, it isn't possible to use the value of "id"
                                  // for this finding because there is a case that "id" attribute
                                  // hasn't been set yet (e.g. during initialization of data
                                  // workflow connection port).
                                      .get(type.workflowStepParameterName).findWhere({key: key}) ||
                                  // CAUTION:
                                  // It is necessary to consider about a case that corresponding
                                  // activity parameter doesn't exist (e.g. data workflow connection
                                  // port is added dynamically for start or end workflow step).
                                  new mwa.model.activityParameter.ActivityParameterModel(
                                      null,
                                      {validate: false}
                                  )
                              ).toJSON(),
                              {
                                id: this.get('id'),
                                listFlag: color.isArray,
                                required: this.get('isRequired'),
                                typeName: this.get('typeName'),
                                type: color.activityParameterType,
                                key: key
                                // CAUTION:
                                // It is important not to set value attribute in order not to
                                // override existing value. And it is OK to use default value for
                                // value attribute in a case that corresponding activity parameter
                                // doesn't exist.
                              }
                          )
                      ) :
                      null
              );
            },
            /**
             * The function to get activity parameter model.
             * @public
             *
             * @returns {?mwa.model.activityParameter.ActivityParameterModel}
             * The activity parameter model.
             */
            getActivityParameterModel: function() {
              var that = this;

              var workflowStepViewModel = that.get('workflowStepViewModel');

              // CAUTION:
              // It is necessary to find activity parameter model corresponding to view model
              // without merging input and output activity parameter models to Backbone.Collection
              // because idAttribute of activity parameter model is "key", not "id", and there is a
              // case that the correct activity parameter model won't been found if input and output
              // activity parameter models have the same key.
              return (
                  mwa.proui._.find(
                      [].concat(
                          workflowStepViewModel.get('inputActivityParameterCollection').models,
                          workflowStepViewModel.get('outputActivityParameterCollection').models
                      ),
                      function(activityParameterModel, index, array) {
                        return activityParameterModel.get('id') === that.get('id');
                      }
                  ) ||
                  // CAUTION:
                  // It is important to prepare null as possible candidate to normalize the result
                  // because find function will return undefined if no value passes the test.
                  null
              );
            },
            /**
             * The callback function. This function is called when workflow connection port is
             * connected.
             * @public
             *
             * @param {mwa.view.workflowConnection.WorkflowConnectionView} workflowConnectionView
             * The connected workflow connection view instance.
             */
            connect: function(workflowConnectionView) {
            },
            /**
             * The event handler for changing typeName attribute of view model. This function
             * updates typeName attribute of corresponding activity parameter model according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {string} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyTypeName: function(model, value, options) {
              this.applyWorkflowStepViewModel(this, this.get('workflowStepViewModel'), options);
            },
            /**
             * The event handler for changing color attribute of view model. This function updates
             * type attribute of corresponding activity parameter model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.enumeration.WorkflowConnectionPortColor} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyColor: function(model, value, options) {
              this.applyWorkflowStepViewModel(this, this.get('workflowStepViewModel'), options);
            },
            /**
             * The event handler for changing workflowStepViewModel attribute of view model. This
             * function adds or merges (especially for ID) data workflow connection port as an
             * activity parameter and updates some attributes of view model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyWorkflowStepViewModel: function(model, value, options) {
              // INFORMATION:
              // This function is been reused by applyTypeName and applyColor functions of view
              // model.

              var type = this.get('type');
              var workflowStepViewModel = this.get('workflowStepViewModel');

              if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                  type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                workflowStepViewModel.get(type.workflowStepParameterName).add(
                    this.toActivityParameterModel(),
                    // CAUTION:
                    // It is important to enable merge in order to set any value (e.g. data workflow
                    // connection port ID, type) to corresponding activity parameter to make the
                    // relationship.
                    {merge: true}
                );
              }

              this.applyWorkflowStepIsDragging(
                  workflowStepViewModel, workflowStepViewModel.get('isDragging'), options
              );

              this.applyWorkflowStepPositions(
                  workflowStepViewModel, workflowStepViewModel.get('positions'), options
              );
            },
            /**
             * The event handler for changing isDragging attribute of workflowStepViewModel. This
             * function updates isDragging attribute of view model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyWorkflowStepIsDragging: function(model, value, options) {
              this.set('isDragging', this.get('workflowStepViewModel').get('isDragging'));
            },
            /**
             * The event handler for changing positions attribute of workflowStepViewModel. This
             * function updates positions attribute of view model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {object} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyWorkflowStepPositions: function(model, value, options) {
              // CAUTION:
              // It is necessary to create a new object to trigger change event.
              var positions = mwa.proui._.extend({}, this.get('positions'));

              var workflowStepViewModel = this.get('workflowStepViewModel');
              var workflowStepPositions = workflowStepViewModel.get('positions');
              // CAUTION:
              // It is necessary to consider about a case that previous value doesn't exist in
              // initialization.
              var previousWorkflowStepPositions =
                  workflowStepViewModel.previous('positions') || workflowStepPositions;
              var workflowStepOffset = workflowStepViewModel.get('offset');
              var offsetWidth = workflowStepViewModel.get('width') / 2 - workflowStepOffset;
              var offsetHeight = workflowStepViewModel.get('height') / 2 - workflowStepOffset;

              switch (this.get('type')) {
                case mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT:
                  positions = {
                    x: workflowStepPositions.x - offsetWidth,
                    y: workflowStepPositions.y,
                    z: positions.z
                  };
                  break;

                case mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT:
                  // CAUTION:
                  // It is necessary to consider about conditional branch case.
                  positions = (positions.y === previousWorkflowStepPositions.y) ?
                      {
                        x: workflowStepPositions.x + offsetWidth,
                        y: workflowStepPositions.y,
                        z: positions.z
                      } :
                      {
                        x: workflowStepPositions.x,
                        y: workflowStepPositions.y + offsetHeight,
                        z: positions.z
                      };
                  break;

                default:
                  break;
              }

              this.set('positions', positions);
            },
            /**
             * The event handler for changing inputActivityParameterCollection or
             * outputActivityParameterCollection attribute of workflow step view model and changing,
             * adding, removing, updating and resetting models in ActivityParameterCollection. This
             * function removes the view itself according to the value.
             * @protected
             */
            applyActivityParameterCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:inputActivityParameterCollection"  (model, value, options)
              //  - "change:outputActivityParameterCollection" (model, value, options)
              // inputActivityParameterCollection
              // outputActivityParameterCollection
              //  - "change"                                   (model, options)
              //  - "change:[attribute]"                       (model, value, options)
              //  - "add", "remove"                            (model, collection, options)
              //  - "update", "reset"                          (collection, options)
              var that = this;
              var args = {};

              var type = that.get('type');
              var workflowStepViewModel = that.get('workflowStepViewModel');
              var activityParameterCollection =
                  workflowStepViewModel.get(type.workflowStepParameterName);

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === workflowStepViewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.model.activityParameter.ActivityParameterCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.model.activityParameter.ActivityParameterModel) {
                  if (arguments[1] !== activityParameterCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.model.activityParameter.ActivityParameterModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // CAUTION:
                    // It is necessary to trigger change event in order to update workflow diagram
                    // model correctly (e.g. in workflow canvas).
                    if (that.get('id') === args.model.get('id')) {
                      that.trigger('change', that, args.options);
                    }
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.model.activityParameter.ActivityParameterModel} model
                     * The added or removed model.
                     * @property {mwa.model.activityParameter.ActivityParameterCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === activityParameterCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.model.activityParameter.ActivityParameterCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  // INFORMATION:
                  // It is better to check the type not to make unintended processing in future
                  // implementation.
                  if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                      type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                    // CAUTION:
                    // It is important to find activity parameter model by "key", not "id", in
                    // consideration of a case that the relational ID was lost during update.
                    var activityParameterModel =
                        args.collection.findWhere({key: that.get('key')});
                    if (activityParameterModel instanceof
                        mwa.model.activityParameter.ActivityParameterModel) {
                      // CAUTION:
                      // It is important to set data workflow connection port ID to corresponding
                      // activity parameter to make the relationship. And it is necessary to use set
                      // function instead of using add function for args.collection in order not to
                      // make infinite loop.
                      activityParameterModel.set(that.toActivityParameterModel().toJSON());
                    }
                  }
                }
              }
            },
            /**
             * The event handler for changing connect attribute of view model. This function updates
             * connect callback according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {function} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyConnect: function(model, value, options) {
              if (mwa.proui._.isFunction(value)) {
                this.connect = value;
              } else {
                this.connect = function(workflowConnectionView) {};
              }
            }
          }
      );
    })();

    /**
     * The mwa workflow connection port view model collection.
     * @constructor
     * @inner
     * @alias mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection
     * @memberof mwa.view.workflowConnectionPort
     * @extends {mwa.Collection}
     */
    global.WorkflowConnectionPortViewModelCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection
           */
          {
            url: global.WorkflowConnectionPortViewModel.prototype.urlRoot,
            model: global.WorkflowConnectionPortViewModel,
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              return this.map(
                  function(workflowConnectionPortViewModel, index, array) {
                    return workflowConnectionPortViewModel.toObject();
                  }
              );
            }
          }
      );
    })();

    /**
     * The mwa workflow connection port view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowConnectionPort.WorkflowConnectionPortView
     * @memberof mwa.view.workflowConnectionPort
     * @extends {mwa.View}
     */
    global.WorkflowConnectionPortView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowConnectionPort.WorkflowConnectionPortView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                // CAUTION:
                // It is important to use div as root element because jQuery cannot add and remove
                // style classes for svg elements.
                //  - jQuery SVG, why can't I addClass?
                //    (http://stackoverflow.com/questions/8638621/jquery-svg-why-cant-i-addclass)
                '<div class="mwa-view-workflow-connection-port <@= color.styleClass @>" data-type="<@= type.name @>">' +
                    '<svg>' +
                        '<circle cx="8" cy="8" r="4" />' +
                    '</svg>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.initialize}
             * @protected
             * @override
             */
            initialize: function(options, canRender) {
              this._timers = {};

              mwa.View.prototype.initialize.apply(this, [options, false]);
            },
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.listenTo(
                  this.options.viewModel,
                  'change:isHidden',
                  this.applyIsHidden
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:isDragging',
                  this.applyIsDragging
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:isConnected',
                  this.applyIsConnected
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canConnect',
                  this.applyCanConnect
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:positions',
                  this.applyPositions
              );

              this.listenToWorkflowStepViewModel(
                  this.options.viewModel,
                  this.options.viewModel.get('workflowStepViewModel'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:workflowStepViewModel',
                  this.listenToWorkflowStepViewModel
              );
            },
            /**
             * The event handler for changing workflowStepViewModel of view model. This function
             * removes old listeners and adds new listeners.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowStepViewModel: function(model, value, options) {
              var type = this.options.viewModel.get('type');

              var previousValue = model.previous('workflowStepViewModel');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
                if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                  this.stopListening(previousValue.get(type.workflowStepParameterName));
                }
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);
                if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                  this.stopListening(value.get(type.workflowStepParameterName));
                }

                this.listenTo(value, 'destroy', this.destroy);

                if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                  this.listenToActivityParameterCollection(
                      value,
                      value.get(type.workflowStepParameterName),
                      null
                  );
                  this.listenTo(
                      value,
                      'change:' + type.workflowStepParameterName,
                      this.listenToActivityParameterCollection
                  );
                }
              }
            },
            /**
             * The event handler for changing inputActivityParameterCollection or
             * outputActivityParameterCollection attribute of workflow step view model. This
             * function removes listeners from old ActivityParameterCollection and adds listeners to
             * new ActivityParameterCollection.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activityParameter.ActivityParameterCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToActivityParameterCollection: function(model, value, options) {
              var type = this.options.viewModel.get('type');

              var previousValue = model.previous(type.workflowStepParameterName);
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'update', this.applyActivityParameterCollection);
                this.listenTo(value, 'reset', this.applyActivityParameterCollection);
              }
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              that.remove({silent: true});
              // INFORMATION:
              // It is important to call remove function instead of empty function for creating this
              // view's element from template.
              that.$el.remove();
              that.setElement(
                  mwa.proui.Backbone.$(
                      // CAUTION: NVXA-1492 (https://acropolis.atlassian.net/browse/NVXA-1492)
                      // It is necessary to parse the template string to create a DOM element
                      // certainly. Actually, jQuery sometimes doesn't create a DOM element
                      // correctly using the template string of this view including svg elements.
                      //  - jQuery 1.9 の $.parseHTML とかその辺
                      //    (http://t-ashula.hateblo.jp/entry/2013/01/23/114105)
                      mwa.proui.Backbone.$.parseHTML(that.template(that.presenter()))
                  )
              );

              that.views = {};

              // CAUTION:
              // It isn't necessary to call applyIsConnected function in this processing because the
              // main purpose of that function is updating draggable and droppable settings using
              // applyCanConnect function.
              that.applyCanConnect(
                  that.options.viewModel,
                  that.options.viewModel.get('canConnect'),
                  null
              );

              // CAUTION:
              // It is necessary to hide the element until completing the positioning to avoid
              // flicker. And it is necessary to use visibility style setting for the non-display
              // because internal timer will stop based on whether the width is fixed.
              that.$el.addClass('proui-state-hidden');
              // CAUTION:
              // It is important to clear the timer before updating it in consideration of a case
              // that this processing is executed multiple times during the interval.
              clearInterval(that._timers.render);
              // CAUTION:
              // It is important to call applyPositions function after rendering view's element
              // because the width and height are unknown before rendering.
              that._timers.render = setInterval(function() {
                if (that.$el.width() != null) {
                  clearInterval(that._timers.render);

                  that.applyPositions(
                      that.options.viewModel,
                      that.options.viewModel.get('positions'),
                      null
                  );

                  // CAUTION:
                  // It is necessary to show the element after completing the positioning to avoid
                  // flicker.
                  that.applyIsHidden(
                      that.options.viewModel,
                      that.options.viewModel.get('isHidden'),
                      null
                  );
                }
              }, 100);

              return that;
            },
            /**
             * @see {@link mwa.View.remove}
             * @public
             * @override
             */
            remove: function(options) {
              if (typeof options === 'undefined' || options == null || !options.silent) {
                // CAUTION:
                // It is important to clear the all timers when remove in order not to execute the
                // unnecessary processing after removal.
                for (var propertyName in this._timers) {
                  if (this._timers.hasOwnProperty(propertyName)) {
                    clearInterval(this._timers[propertyName]);
                  }
                }

                var type = this.options.viewModel.get('type');
                if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                    type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                  // INFORMATION:
                  // There is a case that this function is called multiple times (twice) through
                  // applyActivityParameterCollection function that will be called when updating
                  // activity parameter collection but it isn't necessary to consider about infinite
                  // loop because removal target activity parameter model won't be removed from
                  // activity parameter collection multiple times and update event won't be
                  // occurred in this implementation.
                  this.options.viewModel
                      .get('workflowStepViewModel')
                      .get(type.workflowStepParameterName)
                      // CAUTION:
                      // It is necessary to use "key" in consideration of that idAttribute of
                      // activity parameter model is "key", not "id".
                      .remove(this.options.viewModel.get('key'));
                }
              }

              return mwa.View.prototype.remove.apply(this, [options]);
            },
            /**
             * @see {@link mwa.View.applyChanges}
             * @public
             * @override
             */
            applyChanges: function(model, options) {
              mwa.View.prototype.applyChanges.apply(this, [model, options]);

              // CAUTION:
              // It is necessary to call applyCanConnect function because applyChanges function is
              // called when isDisabled attribute of view model is changed and draggable and
              // droppable settings should be updated according to the values of isDisabled,
              // isConnected and canConnect.
              this.applyCanConnect(
                  this.options.viewModel,
                  this.options.viewModel.get('canConnect'),
                  null
              );
            },
            /**
             * The event handler for changing isHidden attribute of view model. This function adds
             * or removes hidden style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsHidden: function(model, value, options) {
              var isHidden = this.options.viewModel.get('isHidden');

              this.$el.toggleClass('proui-state-hidden', isHidden);
            },
            /**
             * The event handler for changing isDragging attribute of view model. This function adds
             * or removes dragging style settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsDragging: function(model, value, options) {
              var isDragging = this.options.viewModel.get('isDragging');

              this.$el.toggleClass('proui-state-dragging', isDragging);
            },
            /**
             * The event handler for changing isConnected attribute of view model. This function
             * adds or removes draggable or droppable settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsConnected: function(model, value, options) {
              // CAUTION:
              // It is important not to call applyCanConnect function if there will be no changes
              // regarding draggable and droppable settings because jQuery UI cannot destroy and
              // create the settings at the same timing.
              //  - JQuery UI draggable destroy, then draggable again not works
              //    (http://stackoverflow.com/questions/29124160/jquery-ui-draggable-destroy-then-draggable-again-not-works)
              var workflowStepActivityName =
                  this.options.viewModel
                      .get('workflowStepViewModel').get('activityModel').get('name');
              if (workflowStepActivityName !==
                  mwa.enumeration.WorkflowStepType.CONVERGING.templateName) {
                this.applyCanConnect(
                    this.options.viewModel,
                    this.options.viewModel.get('canConnect'),
                    null
                );
              }
            },
            /**
             * The event handler for changing canConnect attribute of view model. This function adds
             * or removes draggable or droppable settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanConnect: function(model, value, options) {
              var that = this;

              var isDisabled = that.options.viewModel.get('isDisabled');
              var isConnected = that.options.viewModel.get('isConnected');
              var canConnect = !isDisabled && that.options.viewModel.get('canConnect');
              var workflowStepActivityName =
                  that.options.viewModel
                      .get('workflowStepViewModel').get('activityModel').get('name');
              var type = that.options.viewModel.get('type');
              var canDrag = !isConnected && canConnect &&
                  // TODO: To remove this condition to be able to connect data workflow connection port.
                  type === mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT;
              var canDrop =
                  (
                      (
                          workflowStepActivityName ===
                          mwa.enumeration.WorkflowStepType.CONVERGING.templateName
                      ) ?
                          canConnect : !isConnected && canConnect
                  ) &&
                  // TODO: To remove this condition to be able to connect data workflow connection port.
                  type === mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT;

              if (that.$el.hasClass('proui-state-draggable')) {
                that.$el.draggable('destroy');
              }
              that.$el.toggleClass('proui-state-draggable', canDrag);

              if (that.$el.hasClass('proui-state-droppable')) {
                that.$el.droppable('destroy');
              }
              that.$el.toggleClass('proui-state-droppable', canDrop);

              if (canDrag) {
                that.$el.draggable({
                  containment: 'parent',
                  snap: '.mwa-view-workflow-connection-port.proui-state-droppable' +
                      (
                          (type === mwa.enumeration.WorkflowConnectionPortType.CONTROL_OUTPUT) ?
                              // CAUTION:
                              // It is important not to check the color for control workflow
                              // connection port in consideration of conditional branch case.
                              '[data-type=' +
                              mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT.name +
                              ']' :
                              '.' + that.options.viewModel.get('color').styleClass +
                              '[data-type=' +
                              mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT.name +
                              ']'
                      ),
                  snapMode: 'inner',
                  helper: function(event) {
                    var workflowConnectionPortView =
                        // INFORMATION:
                        // Actually, global.WorkflowConnectionPortView and
                        // global.WorkflowConnectionPortViewModel is available for the following
                        // codes.
                        new mwa.view.workflowConnectionPort.WorkflowConnectionPortView({
                          viewModel: new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel({
                            id: mwa.proui.util.Uuid.generateUuidV4(),
                            key: mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT.name,
                            // CAUTION:
                            // It is important not to make temporary workflow connection port
                            // connectable because there is a case that it has droppable settings if
                            // it is connectable and corresponding workflow step is conversion step.
                            // If dummy workflow connection port has droppable settings, drop
                            // callback will be called twice when the workflow connection port is
                            // put on droppable workflow connection port.
                            canConnect: false,
                            // INFORMATION:
                            // To be careful about attribute name.
                            positions: mwa.proui._.extend(
                                {}, that.options.viewModel.get('positions')
                            ),
                            type: mwa.enumeration.WorkflowConnectionPortType.CONTROL_INPUT,
                            typeName: mwa.enumeration.WorkflowConnectionPortColor.GRAY.activityParameterType.typeName,
                            color: mwa.enumeration.WorkflowConnectionPortColor.GRAY,
                            // CAUTION:
                            // It is necessary to prepare a new instance because it is temporary and
                            // there shouldn't be side effect between workflow connection ports
                            // especially for pipeline.
                            workflowStepViewModel:
                                new mwa.view.workflowStep.WorkflowStepViewModel(
                                    mwa.proui._.mapObject(
                                        that.options.viewModel
                                            .get('workflowStepViewModel').attributes,
                                        function(value, key) {
                                          return (
                                              // CAUTION: NVX-5190 (https://www.tool.sony.biz/jira/browse/NVX-5190)
                                              // It is necessary to prepare different instances for
                                              // activity parameters in order not to initialize the
                                              // values involuntarily.
                                              (
                                                  value instanceof
                                                  mwa.proui.Backbone.Collection
                                              ) ?
                                                  // CAUTION:
                                                  // It is better to use constructor function
                                                  // instead of clone function because unnecessary
                                                  // initialization sequence will be happened with
                                                  // the models by clone function.
                                                  new value.constructor() :
                                                  value
                                          );
                                        }
                                    )
                                )
                          })
                        });

                    return workflowConnectionPortView.render().$el.data(
                        'workflowConnectionPortView', workflowConnectionPortView
                    );
                  },
                  start: function(event, ui) {
                    // CAUTION:
                    // It is necessary to set data to actual draggable element, not helper element,
                    // because the first argument of jQuery UI droppable accept function is actual
                    // draggable element.
                    that.$el.data('view', that);

                    var workflowConnectionPortView = ui.helper.data('workflowConnectionPortView');
                    // INFORMATION:
                    // The workflow connection port classes shouldn't be dependent on workflow
                    // connection classes in consideration of entire dependency relationship.
                    // However, the following codes are no problem because this function is called
                    // after completing all classes declaration.
                    var workflowConnectionView =
                        new mwa.view.workflowConnection.WorkflowConnectionView({
                          viewModel: new mwa.view.workflowConnection.WorkflowConnectionViewModel({
                            canSelect: true,
                            canDrag: true,
                            canDelete: true,
                            // CAUTION:
                            // It is necessary to clone the view model because isConnected attribute
                            // will be updated to true in the initialization of workflow connection.
                            // In other words, this draggable settings will be destroyed by
                            // change:isConnected listener if the original view model is passed to
                            // workflow connection.
                            fromWorkflowConnectionPortViewModel:
                                that.options.viewModel.clone(),
                            toWorkflowConnectionPortViewModel:
                                workflowConnectionPortView.options.viewModel
                          })
                        });
                    ui.helper
                        .data('workflowConnectionView', workflowConnectionView)
                        // CAUTION:
                        // It is necessary to use prepend function instead of append function
                        // because workflow connections has large size and should be rendered at
                        // back to be able to click any element.
                        .parent().prepend(workflowConnectionView.render().$el);
                  },
                  drag: function(event, ui) {
                    var workflowConnectionPortView = ui.helper.data('workflowConnectionPortView');
                    workflowConnectionPortView.options.viewModel.set(
                        'positions',
                        {
                          x: ui.position.left + that.$el.width() / 2,
                          y: ui.position.top + that.$el.height() / 2,
                          z: workflowConnectionPortView.options.viewModel.get('positions').z
                        }
                    );
                  },
                  stop: function(event, ui) {
                    // INFORMATION:
                    // This callback is called after drop callback of jQuery UI Droppable.

                    var options = (ui.helper.data('droppableView') || {options: {}}).options;
                    var workflowConnectionPortView = ui.helper.data('workflowConnectionPortView');
                    var workflowConnectionView = ui.helper.data('workflowConnectionView');

                    if (options.viewModel == null ||
                        // INFORMATION:
                        // Actually, global.WorkflowConnectionPortViewModel is available for the
                        // following code.
                        !(options.viewModel instanceof
                        mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel)) {
                      workflowConnectionView.remove();
                    } else {
                      workflowConnectionView.options.viewModel.set({
                        // CAUTION:
                        // It is also necessary to set actual (not cloned) view model to
                        // fromWorkflowConnectionPortViewModel (not only
                        // toWorkflowConnectionPortViewModel) because the current value is cloned
                        // view model of this view (that.options.viewModel.clone()).
                        fromWorkflowConnectionPortViewModel: that.options.viewModel,
                        toWorkflowConnectionPortViewModel: options.viewModel
                      });

                      // CAUTION:
                      // It is important to call connect callback of destination workflow connection
                      // port view model, not source workflow connection port view model, in
                      // accordance with other implementation (e.g. workflow parameter editor).
                      options.viewModel.connect(workflowConnectionView);
                    }

                    // CAUTION:
                    // It is necessary to remove temporary workflow connection port after updating
                    // workflow connection because it is also removed when the connected workflow
                    // connection port is removed.
                    workflowConnectionPortView.remove();

                    // INFORMATION:
                    // It isn't necessary to remove data of workflowConnectionPortView and
                    // workflowConnectionView from helper element because it will be removed with
                    // the data in jQuery UI processing.
                    that.$el.removeData('view');
                  }
                });
              }

              if (canDrop) {
                that.$el.droppable({
                  greedy: true,
                  tolerance: 'touch',
                  activeClass: 'proui-state-selecting',
                  accept: function($draggable) {
                    var options = ($draggable.data('view') || {options: {}}).options;

                    // INFORMATION:
                    // Actually, global.WorkflowConnectionPortView and
                    // global.WorkflowConnectionPortViewModel is available for the following codes.
                    return (options.viewModel || {}) instanceof
                        mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel &&
                        options.viewModel.get('workflowStepViewModel') !==
                            that.options.viewModel.get('workflowStepViewModel');
                  },
                  drop: function(event, ui) {
                    // CAUTION:
                    // It isn't possible to update fromWorkflowConnectionPortViewModel attribute
                    // of workflowConnectionView by actual (not cloned) view model of dragged
                    // workflow connection port in this function because its draggable settings will
                    // be destroyed and an error will occur in jQuery UI processing to call its stop
                    // callback. Therefore, the update should be done in the stop callback, not
                    // here.
                    //  - Error When Destroying Draggable After Drop
                    //    (http://stackoverflow.com/questions/15505763/error-when-destroying-draggable-after-drop)

                    ui.helper.data('droppableView', that);
                  }
                });
              }
            },
            /**
             * The event handler for changing positions attribute of view model. This function moves
             * element positions according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {object} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyPositions: function(model, value, options) {
              var positions = this.options.viewModel.get('positions');
              this.$el.css({
                left: (positions.x - this.$el.width() / 2) + 'px',
                top: (positions.y - this.$el.height() / 2) + 'px',
                zIndex: positions.z
              });
            },
            /**
             * The event handler for changing inputActivityParameterCollection or
             * outputActivityParameterCollection attribute of workflow step view model and changing,
             * adding, removing, updating and resetting models in ActivityParameterCollection. This
             * function removes the view itself according to the value.
             * @protected
             */
            applyActivityParameterCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:inputActivityParameterCollection"  (model, value, options)
              //  - "change:outputActivityParameterCollection" (model, value, options)
              // inputActivityParameterCollection
              // outputActivityParameterCollection
              //  - "change"                                   (model, options)
              //  - "change:[attribute]"                       (model, value, options)
              //  - "add", "remove"                            (model, collection, options)
              //  - "update", "reset"                          (collection, options)
              var that = this;
              var args = {};

              var type = that.options.viewModel.get('type');
              var workflowStepViewModel = that.options.viewModel.get('workflowStepViewModel');
              var activityParameterCollection =
                  workflowStepViewModel.get(type.workflowStepParameterName);

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === workflowStepViewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.model.activityParameter.ActivityParameterCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.model.activityParameter.ActivityParameterModel) {
                  if (arguments[1] !== activityParameterCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.model.activityParameter.ActivityParameterModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.model.activityParameter.ActivityParameterModel} model
                     * The added or removed model.
                     * @property {mwa.model.activityParameter.ActivityParameterCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === activityParameterCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.model.activityParameter.ActivityParameterCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  // INFORMATION:
                  // It is better to check the type not to make unintended processing in future
                  // implementation.
                  if (type === mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT ||
                      type === mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                    // CAUTION:
                    // It is important to find activity parameter model by "key", not "id", in
                    // consideration of a case that the relational ID was lost during update.
                    var activityParameterModel =
                        args.collection.findWhere({key: that.options.viewModel.get('key')});
                    if (!(activityParameterModel instanceof
                        mwa.model.activityParameter.ActivityParameterModel)) {
                      // INFORMATION:
                      // The view should be removed as synchronization if there is no corresponding
                      // activity parameter.
                      that.remove();
                    }
                  }
                }
              }
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowConnectionPort;
}));
