/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.view == null || mwa.view.workflowStep == null) ? './mwa.view.workflow-step' : {},
      (mwa.view == null || mwa.view.workflowConnectionPort == null) ? './mwa.view.workflow-connection-port' : {},
      (mwa.view == null || mwa.view.workflowConnection == null) ? './mwa.view.workflow-connection' : {},
      (mwa.model == null || mwa.model.directory == null) ? './mwa.model.directory' : {},
      (mwa.model == null || mwa.model.activityParameterType == null) ? './mwa.model.activity-parameter-type' : {},
      (mwa.model == null || mwa.model.activityCondition == null) ? './mwa.model.activity-condition' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.view == null || mwa.view.workflowStep == null) ? require('./mwa.view.workflow-step') : {},
        (mwa.view == null || mwa.view.workflowConnectionPort == null) ? require('./mwa.view.workflow-connection-port') : {},
        (mwa.view == null || mwa.view.workflowConnection == null) ? require('./mwa.view.workflow-connection') : {},
        (mwa.model == null || mwa.model.directory == null) ? require('./mwa.model.directory') : {},
        (mwa.model == null || mwa.model.activityParameterType == null) ? require('./mwa.model.activity-parameter-type') : {},
        (mwa.model == null || mwa.model.activityCondition == null) ? require('./mwa.model.activity-condition') : {}
    );
  } else {
    root.mwa.view.workflowParameterEditor = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.view.workflowStep,
        root.mwa.view.workflowConnectionPort,
        root.mwa.view.workflowConnection,
        root.mwa.model.directory,
        root.mwa.model.activityParameterType,
        root.mwa.model.activityCondition
    );
  }
}(this, function(base, subBase, workflowStep, workflowConnectionPort, workflowConnection, directory, activityParameterType, activityCondition) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.view.workflowStep namespace.
  if (mwa.view.workflowStep == null) {
    mwa.view.workflowStep = workflowStep;
  }
  // To initialize the mwa.view.workflowConnectionPort namespace.
  if (mwa.view.workflowConnectionPort == null) {
    mwa.view.workflowConnectionPort = workflowConnectionPort;
  }
  // To initialize the mwa.view.workflowConnection namespace.
  if (mwa.view.workflowConnection == null) {
    mwa.view.workflowConnection = workflowConnection;
  }
  // To initialize the mwa.model.directory namespace.
  if (mwa.model.directory == null) {
    mwa.model.directory = directory;
  }
  // To initialize the mwa.model.activityParameterType namespace.
  if (mwa.model.activityParameterType == null) {
    mwa.model.activityParameterType = activityParameterType;
  }
  // To initialize the mwa.model.activityCondition namespace.
  if (mwa.model.activityCondition == null) {
    mwa.model.activityCondition = activityCondition;
  }

  /**
   * The mwa.view.workflowParameterEditor namespace.
   * @namespace
   */
  mwa.view.workflowParameterEditor = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowParameterEditor alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    var ACTIVITY_PARAMETER_TYPE_ARRAY =
        mwa.proui._.values(mwa.enumeration.ActivityParameterType);
    var WORKFLOW_STEP_TYPE_ARRAY =
        mwa.proui._.values(mwa.enumeration.WorkflowStepType);
    var WORKFLOW_CONNECTION_PORT_COLOR_ARRAY =
        mwa.proui._.values(mwa.enumeration.WorkflowConnectionPortColor);
    var ACTIVITY_CONDITION_CONDITION_OPERATOR_ARRAY =
        mwa.proui._.sortBy(
            mwa.proui._.values(mwa.enumeration.ActivityConditionConditionOperator),
            'index'
        );
    var ACTIVITY_CONDITION_COMPARISON_OPERATOR_ARRAY =
        mwa.proui._.sortBy(
            mwa.proui._.values(mwa.enumeration.ActivityConditionComparisonOperator),
            'index'
        );

    // CAUTION:
    // It is better to prepare singleton instances for predefined workflow steps to be handled same
    // as actual workflow steps.
    var predefinedWorkflowStepViewModelArray =
        mwa.proui._.values(mwa.enumeration.WorkflowStep).map(
            function(workflowStep, index, array) {
              return new mwa.view.workflowStep.WorkflowStepViewModel(
                  mwa.proui._.extend(
                      {},
                      mwa.proui._.omit(workflowStep, 'name'),
                      {
                        positions: {
                          x: 0,
                          y: 0,
                          z: 0
                        },
                        activityModel: new mwa.model.activity.ActivityModel(
                            null, {validate: false}
                        )
                      }
                  )
              );
            }
        );
    var findPredefinedWorkflowStepViewModel = function(activityParameterModel) {
      return mwa.proui._.findWhere(
          predefinedWorkflowStepViewModelArray,
          {
            id:
                (activityParameterModel.get('type').isSetting) ?
                    mwa.enumeration.WorkflowStep.SETTING_VALUE.id :
                    mwa.enumeration.WorkflowStep.IMMEDIATE_VALUE.id
          }
      );
    };
    var excludedWorkflowStepTypeTemplateNameArray =
        mwa.proui._.pluck(
            WORKFLOW_STEP_TYPE_ARRAY.filter(
                function(workflowStepType, inde, array) {
                  return !workflowStepType.hasParameter;
                }
            ),
            'templateName'
        );
    var filterPipelineWorkflowStepViewModelCollection = function(pipelineWorkflowStepViewModel, index, array) {
      return excludedWorkflowStepTypeTemplateNameArray.indexOf(
          pipelineWorkflowStepViewModel.get('activityModel').get('name')
      ) === -1;
    };

    // CAUTION:
    // This function is different from _.isEmpty function with respect to number handling.
    var isEmpty = function(value) {
      // CAUTION:
      // This code means treating with undefined value, null and empty string as the same.
      return (
          typeof value === 'undefined' || value == null || value === '' ||
          (mwa.proui.util.Object.isArray(value) && value.length === 0)
      );
    };

    /**
     * The function to update drop down button maximum height.
     * @protected
     *
     * @param {!Backbone.View} view
     * The view that has specified drop down button view as sub view.
     * @param {!proui.dropDownButton.DropDownButtonView} dropDownButtonView
     * The drop down button view to be updated.
     */
    var updateDropDownButtonMaxHeight = function(view, dropDownButtonView) {
      if (dropDownButtonView instanceof mwa.proui.dropDownButton.DropDownButtonView) {
        // CAUTION:
        // It is important to clear the timer before updating it in consideration of a case that
        // this processing is executed multiple times during the interval.
        clearInterval(view._timers[dropDownButtonView.cid]);
        view._timers[dropDownButtonView.cid] = setInterval(function() {
          var backgroundImage = dropDownButtonView.$el.css('background-image');
          if (typeof backgroundImage === 'string' && backgroundImage !== '') {
            clearInterval(view._timers[dropDownButtonView.cid]);

            dropDownButtonView.options.viewModel.set(
                'maxHeight',
                // CAUTION:
                // It is necessary to obtain max height style setting using this approach in
                // consideration of a case that the element is hidden and the height cannot be
                // calculated.
                parseInt(mwa.proui.util.Url.findQuery(backgroundImage, 'maxHeight'), 10)
            );
          }
        }, 100);
      }
    };

    /**
     * The mwa workflow parameter editor view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.ViewModel}
     */
    global.WorkflowParameterEditorViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    isReadOnly: false,
                    activityParameterTypeCollection:
                        new mwa.model.activityParameterType.ActivityParameterTypeCollection(),
                    workflowStepViewModelCollection:
                        new mwa.view.workflowStep.WorkflowStepViewModelCollection(),
                    workflowConnectionPortViewModelCollection:
                        new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection(),
                    workflowConnectionViewModelCollection:
                        new mwa.view.workflowConnection.WorkflowConnectionViewModelCollection()
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  isReadOnly: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  activityParameterTypeCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityParameterType.ActivityParameterTypeCollection
                    }
                  ],
                  workflowStepViewModelCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowStep.WorkflowStepViewModelCollection
                    }
                  ],
                  workflowConnectionPortViewModelCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection
                    }
                  ],
                  workflowConnectionViewModelCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowConnection.WorkflowConnectionViewModelCollection
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The function to check whether selected workflow step view model has activity instance
             * model.
             * @public
             *
             * @returns {!boolean}
             * The result whether selected workflow step view model has activity instance model.
             */
            hasSelectedActivityInstanceModel: function() {
              return (
                  this.getSelectedWorkflowStepViewModel() ||
                  new mwa.view.workflowStep.WorkflowStepViewModel(null, {validate: false})
              ).has('activityInstanceModel');
            },
            /**
             * The function to get selected workflow step view model.
             * @public
             *
             * @returns {?mwa.view.workflowStep.WorkflowStepViewModel}
             * The selected workflow step view model.
             */
            getSelectedWorkflowStepViewModel: function() {
              return (
                  this.get('workflowStepViewModelCollection').findWhere(
                      {isSelected: true}
                  ) ||
                  null
              );
            },
            /**
             * The function to get selected activity model.
             * @public
             *
             * @returns {?mwa.model.activity.ActivityModel}
             * The selected activity model.
             */
            getSelectedActivityModel: function() {
              var selectedWorkflowStepViewModel =
                  this.getSelectedWorkflowStepViewModel() ||
                  new mwa.view.workflowStep.WorkflowStepViewModel(null, {validate: false});

              return (
                  selectedWorkflowStepViewModel.get('activityModel') ||
                  null
              );
            }
          }
      );
    })();

    /**
     * The mwa workflow parameter editor view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowParameterEditorView
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.View}
     */
    global.WorkflowParameterEditorView = (function() {
      /**
       * The function to calculate expander content minimum height.
       *
       * @param {!proui.expander.ExpanderView} expanderView
       * The expander view to be calculated.
       *
       * @returns {!number}
       * The calculated expander content minimum height.
       */
      var calculateExpanderContentMinHeight = function(expanderView) {
        var $workflowParameterExpanderContent = expanderView.$el.find('.proui-expander-content');

        return (
            expanderView.options.viewModel.get('headerSize') +
            (
                parseInt($workflowParameterExpanderContent.css('top'), 10) -
                expanderView.options.viewModel.get('headerSize') +
                parseInt($workflowParameterExpanderContent.css('bottom'), 10)
            )
        );
      };

      /**
       * The function to filter data workflow connection view model collection.
       *
       * @param {!mwa.view.workflowConnection.WorkflowConnectionViewModel} workflowConnectionViewModel
       * The workflow connection view model to be filtered.
       * @param {!number} index
       * The number to specify the index of specified workflow connection view model.
       * @param {!mwa.view.workflowConnection.WorkflowConnectionViewModel[]} array
       * The array of workflow connection view model including specified one.
       *
       * @returns {!boolean}
       * The result whether specified one will be preserved.
       */
      var filterDataWorkflowConnectionViewModelCollection = function(workflowConnectionViewModel, index, array) {
        return (
            workflowConnectionViewModel
                .get('fromWorkflowConnectionPortViewModel')
                .get('type') ===
            mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT &&
            workflowConnectionViewModel
                .get('toWorkflowConnectionPortViewModel')
                .get('type') ===
            mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT
        );
      };

      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowParameterEditorView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-parameter-editor">' +
                    '<div class="mwa-view-workflow-parameter-editor-header proui-layout-border-box">' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-step-drop-down-button"></div>' +
                    '</div>' +
                    '<div class="mwa-view-workflow-parameter-editor-body proui-layout-border-box">' +
                        '<div class="mwa-view-workflow-parameter-editor-information-activity-instance-information"></div>' +
                        '<div class="mwa-view-workflow-parameter-editor-input-workflow-parameter-expander"></div>' +
                        '<div class="mwa-view-workflow-parameter-editor-output-workflow-parameter-expander"></div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-condition-expander"></div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-add-button"></div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-condition-add-button"></div>' +
                    '</div>' +
                    '<div class="mwa-view-workflow-parameter-editor-footer proui-layout-border-box">' +
                        '<div class="mwa-view-workflow-parameter-editor-information-expander"></div>' +
                    '</div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              var selectedWorkflowStepViewModel =
                  this.options.viewModel.getSelectedWorkflowStepViewModel() ||
                  new mwa.view.workflowStep.WorkflowStepViewModel(null, {validate: false});

              this.models = {
                previousSelectedPipelineWorkflowStepViewModelCollection: null,
                inputDataWorkflowConnectionPortViewModelCollection:
                    new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection(
                        this.options.viewModel
                            .get('workflowConnectionPortViewModelCollection')
                            .where({
                              type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT,
                              workflowStepViewModel: selectedWorkflowStepViewModel
                            })
                    ),
                inputDataWorkflowConnectionViewModelCollection:
                    new mwa.view.workflowConnection.WorkflowConnectionViewModelCollection(
                        this.options.viewModel
                            .get('workflowConnectionViewModelCollection')
                            // CAUTION:
                            // It is important to pass all data workflow connection regardless of
                            // displayed data workflow connection port in order not to update
                            // dataWorkflowConnectionViewModelCollection attribute of workflow
                            // parameter view model when selected workflow step is changed.
                            .filter(filterDataWorkflowConnectionViewModelCollection)
                    ),
                outputDataWorkflowConnectionPortViewModelCollection:
                    new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection(
                        this.options.viewModel
                            .get('workflowConnectionPortViewModelCollection')
                            .where({
                              type: mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT,
                              workflowStepViewModel: selectedWorkflowStepViewModel
                            })
                    ),
                outputDataWorkflowConnectionViewModelCollection:
                    // INFORMATION:
                    // It isn't necessary to collect data workflow connection view models for output
                    // workflow parameter because there is no case to display the connection.
                    new mwa.view.workflowConnection.WorkflowConnectionViewModelCollection(),
                activityConditionCollection:
                    new mwa.model.activityCondition.ActivityConditionCollection()
              };

              // Data Workflow Connection Port

              this.listenTo(
                  this.models.inputDataWorkflowConnectionPortViewModelCollection,
                  'update',
                  this.applyDataWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.inputDataWorkflowConnectionPortViewModelCollection,
                  'reset',
                  this.applyDataWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.inputDataWorkflowConnectionPortViewModelCollection,
                  'destroy',
                  this.applyDataWorkflowConnectionPortViewModelCollection
              );

              this.listenTo(
                  this.models.outputDataWorkflowConnectionPortViewModelCollection,
                  'update',
                  this.applyDataWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.outputDataWorkflowConnectionPortViewModelCollection,
                  'reset',
                  this.applyDataWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.models.outputDataWorkflowConnectionPortViewModelCollection,
                  'destroy',
                  this.applyDataWorkflowConnectionPortViewModelCollection
              );

              // Data Workflow Connection

              // If you need new listeners, add here.

              // Activity Condition

              this.listenTo(
                  this.models.activityConditionCollection,
                  'update',
                  this.applyActivityConditionCollection
              );
              this.listenTo(
                  this.models.activityConditionCollection,
                  'reset',
                  this.applyActivityConditionCollection
              );
              this.listenTo(
                  this.models.activityConditionCollection,
                  'destroy',
                  this.applyActivityConditionCollection
              );

              // View Model

              this.listenTo(
                  this.options.viewModel,
                  'change:isReadOnly',
                  this.applyIsReadOnly
              );

              this.listenToWorkflowStepViewModelCollection(
                  this.options.viewModel,
                  this.options.viewModel.get('workflowStepViewModelCollection'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:workflowStepViewModelCollection',
                  this.listenToWorkflowStepViewModelCollection
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:workflowStepViewModelCollection',
                  this.applyWorkflowStepViewModelCollection
              );

              // INFORMATION:
              // It is better not to prepare the listeners for
              // workflowConnectionPortViewModelCollection attribute of view model in consideration
              // of performance because there is a situation that control workflow connection port
              // view models will be added frequently (e.g. in workflow canvas). When the following
              // listener settings are uncommented, save callback implementation of workflow
              // parameter add dialog should be modified.

              // this.listenToWorkflowConnectionPortViewModelCollection(
              //     this.options.viewModel,
              //     this.options.viewModel.get('workflowConnectionPortViewModelCollection'),
              //     null
              // );
              // this.listenTo(
              //     this.options.viewModel,
              //     'change:workflowConnectionPortViewModelCollection',
              //     this.listenToWorkflowConnectionPortViewModelCollection
              // );
              // this.listenTo(
              //     this.options.viewModel,
              //     'change:workflowConnectionPortViewModelCollection',
              //     this.applyWorkflowConnectionPortViewModelCollection
              // );

              this.listenToWorkflowConnectionViewModelCollection(
                  this.options.viewModel,
                  this.options.viewModel.get('workflowConnectionViewModelCollection'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:workflowConnectionViewModelCollection',
                  this.listenToWorkflowConnectionViewModelCollection
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:workflowConnectionViewModelCollection',
                  this.applyWorkflowConnectionViewModelCollection
              );

              this.bindingResizeWindow = mwa.proui._.bind(this.resizeWindow, this);
              this.listenToWindowEvent('resize', 'bindingResizeWindow');
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * selected workflow step view model. This function removes old listeners and adds new
             * listeners.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToSelectedPipelineWorkflowStepViewModelCollection: function(model, value, options) {
              var previousValue =
                  this.models.previousSelectedPipelineWorkflowStepViewModelCollection;
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }
              this.models.previousSelectedPipelineWorkflowStepViewModelCollection = value;

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(
                    value,
                    'update',
                    this.applySelectedPipelineWorkflowStepViewModelCollection
                );
                this.listenTo(
                    value,
                    'reset',
                    this.applySelectedPipelineWorkflowStepViewModelCollection
                );
              }
            },
            /**
             * The event handler for changing workflowStepViewModelCollection attribute of view
             * model. This function removes old listeners and adds new listeners.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel} model
             * The mwa workflow parameter editor view model.
             * @param {mwa.view.workflowStep.WorkflowStepViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowStepViewModelCollection: function(model, value, options) {
              var previousValue = model.previous('workflowStepViewModelCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'change:isSelected', this.applyWorkflowStepViewModelCollection);
                this.listenTo(value, 'add', this.applyWorkflowStepViewModelCollection);
                this.listenTo(value, 'remove', this.applyWorkflowStepViewModelCollection);
                this.listenTo(value, 'update', this.applyWorkflowStepViewModelCollection);
                this.listenTo(value, 'reset', this.applyWorkflowStepViewModelCollection);
              }
            },
            /**
             * The event handler for changing workflowConnectionPortViewModelCollection attribute of
             * view model. This function removes old listeners and adds new listeners.
             * @protected
             * @deprecated since version 2.11.0
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel} model
             * The mwa workflow parameter editor view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowConnectionPortViewModelCollection: function(model, value, options) {
              var previousValue = model.previous('workflowConnectionPortViewModelCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'update', this.applyWorkflowConnectionPortViewModelCollection);
              }
            },
            /**
             * The event handler for changing workflowConnectionViewModelCollection attribute of
             * view model. This function removes old listeners and adds new listeners.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel} model
             * The mwa workflow parameter editor view model.
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowConnectionViewModelCollection: function(model, value, options) {
              var previousValue = model.previous('workflowConnectionViewModelCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'update', this.applyWorkflowConnectionViewModelCollection);
                this.listenTo(value, 'reset', this.applyWorkflowConnectionViewModelCollection);
              }
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              mwa.View.prototype.render.apply(this, []);

              this.$body = this.$el.find('.mwa-view-workflow-parameter-editor-body');
              this.$footer = this.$el.find('.mwa-view-workflow-parameter-editor-footer');

              this._workflowParameterItemKeyHeight =
                  parseInt(
                      mwa.proui.util.Url.findQuery(
                          this.$body.css('background-image'), 'workflowParameterItemKeyHeight'
                      ),
                      10
                  );
              this._workflowParameterItemValueHeight =
                  parseInt(
                      mwa.proui.util.Url.findQuery(
                          this.$body.css('background-image'), 'workflowParameterItemValueHeight'
                      ),
                      10
                  );
              this._workflowParameterItemResultHeight =
                  parseInt(
                      mwa.proui.util.Url.findQuery(
                          this.$body.css('background-image'), 'workflowParameterItemResultHeight'
                      ),
                      10
                  );
              this._workflowConditionItemHeight =
                  parseInt(
                      mwa.proui.util.Url.findQuery(
                          this.$body.css('background-image'), 'workflowConditionItemHeight'
                      ),
                      10
                  );
              this._workflowConditionItemOperatorHeight =
                  parseInt(
                      mwa.proui.util.Url.findQuery(
                          this.$body.css('background-image'), 'workflowConditionItemOperatorHeight'
                      ),
                      10
                  );

              this.renderWorkflowStepDropDownButton();
              this.renderWorkflowParameterScrollBar();
              this.renderActivityInstanceInformation();
              this.renderInputWorkflowParameterExpander();
              this.renderOutputWorkflowParameterExpander();
              this.renderWorkflowConditionExpander();
              this.renderWorkflowParameterAddButton();
              this.renderWorkflowConditionAddButton();
              this.renderInformationExpander();

              return this;
            },
            /**
             * The function to render workflow step drop down button.
             * @protected
             *
             * @returns {!proui.dropDownButton.DropDownButtonView}
             * The rendered drop down button view instance.
             */
            renderWorkflowStepDropDownButton: function() {
              var that = this;

              // Initialization
              if (that.views.workflowStepDropDownButtonView != null) {
                that.views.workflowStepDropDownButtonView.remove();
              }
              that.views.workflowStepDropDownButtonView = null;

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-parameter-editor-workflow-step-drop-down-button');
              that.views.workflowStepDropDownButtonView =
                  new mwa.proui.dropDownButton.DropDownButtonView({
                    el: $el,
                    viewModel: new mwa.proui.dropDownButton.DropDownButtonViewModel({
                      isStrong: false,
                      hasTooltip: false,
                      label: 'text',
                      width: '100%',
                      // CAUTION:
                      // It is necessary to obtain max height style setting using this approach in
                      // consideration of a case that the element is hidden and the height cannot be
                      // calculated.
                      maxHeight: parseInt(
                          mwa.proui.util.Url.findQuery($el.css('background-image'), 'maxHeight'),
                          10
                      ),
                      // CAUTION:
                      // It is important not to set initial item because it will be added to
                      // itemCollection implicitly. In other words, the collection instance
                      // shouldn't be changed unnecessarily because the instance might have been
                      // shared with other views.
                      hasInitialItem: false,
                      // INFORMATION:
                      // It is important to set itemCollection and selectedItem in this function
                      // even if similar logic is implemented in
                      // applyWorkflowStepViewModelCollection function because the rendering should
                      // be completed just by calling this function.
                      itemCollection:
                          that.options.viewModel.get('workflowStepViewModelCollection').sort(),
                      selectedItem:
                          that.options.viewModel.get('workflowStepViewModelCollection').findWhere(
                              {isSelected: true}
                          ) ||
                          null,
                      select: function(selectedItemModel) {
                        var previousSelectedItemModel = this.previous('selectedItem');

                        if (previousSelectedItemModel instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                          previousSelectedItemModel.set('isSelected', false);
                        }

                        if (selectedItemModel instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                          selectedItemModel.set('isSelected', true);
                        }
                      }
                    })
                  });

              return that.views.workflowStepDropDownButtonView;
            },
            /**
             * The function to render workflow parameter scroll bar.
             * @protected
             *
             * @returns {!proui.scrollBar.ScrollBarView}
             * The rendered scroll bar view instance.
             */
            renderWorkflowParameterScrollBar: function() {
              var that = this;

              // Initialization
              if (that.views.parameterScrollBarView != null) {
                that.views.parameterScrollBarView.remove();
              }
              that.views.parameterScrollBarView = null;

              // Rendering
              that.views.parameterScrollBarView = new mwa.proui.scrollBar.ScrollBarView({
                el: that.$body,
                isExpandContent: true
              });

              return that.views.parameterScrollBarView;
            },
            /**
             * The function to render activity instance information.
             * @protected
             *
             * @returns {!mwa.view.workflowParameterEditor.ActivityInstanceInformationView}
             * The rendered activity instance information view instance.
             */
            renderActivityInstanceInformation: function() {
              var that = this;

              // Initialization
              if (that.views.activityInstanceInformationView != null) {
                that.views.activityInstanceInformationView.remove();
              }
              that.views.activityInstanceInformationView = null;

              // Rendering
              var $el =
                  that.$el.find('.mwa-view-workflow-parameter-editor-information-activity-instance-information');
              that.views.activityInstanceInformationView =
                  new mwa.view.workflowParameterEditor.ActivityInstanceInformationView({
                    el: $el,
                    viewModel:
                        new mwa.view.workflowParameterEditor.ActivityInstanceInformationViewModel({
                          activityInstanceModel:
                              (
                                  that.options.viewModel.getSelectedWorkflowStepViewModel() ||
                                  new mwa.view.workflowStep.WorkflowStepViewModel(
                                      null, {validate: false}
                                  )
                              ).get('activityInstanceModel')
                        })
                  });

              return that.views.activityInstanceInformationView;
            },
            /**
             * The function to render input workflow parameter expander.
             * @protected
             *
             * @returns {!proui.expander.ExpanderView}
             * The rendered expander view instance.
             */
            renderInputWorkflowParameterExpander: function() {
              var that = this;

              // Initialization
              if (that.views.inputWorkflowParameterExpanderView != null) {
                that.views.inputWorkflowParameterExpanderView.remove();
              }
              that.views.inputWorkflowParameterExpanderView = null;

              // Rendering
              var selectedActivityModel =
                  that.options.viewModel.getSelectedActivityModel() ||
                  new mwa.model.activity.ActivityModel(null, {validate: false});
              var $el =
                  that.$el.find('.mwa-view-workflow-parameter-editor-input-workflow-parameter-expander');
              that.views.inputWorkflowParameterExpanderView =
                  new mwa.proui.expander.ExpanderView({
                    el: $el,
                    viewModel: new mwa.proui.expander.ExpanderViewModel({
                      title: mwa.enumeration.Message['TITLE_INPUT'],
                      isExpanded: true,
                      hasTooltip: false,
                      position: mwa.proui.expander.Position.TOP,
                      offset: that.$body.outerHeight() - that.$body.height(),
                      create: function(contentView, viewModel) {
                        that.views.inputWorkflowParameterView = contentView;
                      }
                    }),
                    contentModel: new mwa.proui.expander.ExpanderContentModel({
                      viewClass: mwa.view.workflowParameterEditor.WorkflowParameterView,
                      viewOptions: {
                        viewModel: new mwa.view.workflowParameterEditor.WorkflowParameterViewModel({
                          isReadOnly: that.options.viewModel.get('isReadOnly'),
                          canDelete:
                              !that.options.viewModel.get('isReadOnly') &&
                              selectedActivityModel.get('type') ===
                              mwa.enumeration.ActivityType.START,
                          dataWorkflowConnectionPortViewModelCollection:
                              that.models.inputDataWorkflowConnectionPortViewModelCollection,
                          dataWorkflowConnectionViewModelCollection:
                              that.models.inputDataWorkflowConnectionViewModelCollection
                        })
                      },
                      canResize: false,
                      width: '100%',
                      // INFORMATION:
                      // This is tentative value and should be updated after rendering.
                      height: 0
                    })
                  });

              that.updateWorkflowParameterExpanderContentHeight(
                  that.views.inputWorkflowParameterExpanderView
              );

              return that.views.inputWorkflowParameterExpanderView;
            },
            /**
             * The function to render output workflow parameter expander.
             * @protected
             *
             * @returns {!proui.expander.ExpanderView}
             * The rendered expander view instance.
             */
            renderOutputWorkflowParameterExpander: function() {
              var that = this;

              // Initialization
              if (that.views.outputWorkflowParameterExpanderView != null) {
                that.views.outputWorkflowParameterExpanderView.remove();
              }
              that.views.outputWorkflowParameterExpanderView = null;

              // Rendering
              var selectedActivityModel =
                  that.options.viewModel.getSelectedActivityModel() ||
                  new mwa.model.activity.ActivityModel(null, {validate: false});
              var $el =
                  that.$el.find('.mwa-view-workflow-parameter-editor-output-workflow-parameter-expander');
              that.views.outputWorkflowParameterExpanderView =
                  new mwa.proui.expander.ExpanderView({
                    el: $el,
                    viewModel: new mwa.proui.expander.ExpanderViewModel({
                      title: mwa.enumeration.Message['TITLE_OUTPUT'],
                      isExpanded: true,
                      hasTooltip: false,
                      position: mwa.proui.expander.Position.TOP,
                      offset: that.$body.outerHeight() - that.$body.height(),
                      create: function(contentView, viewModel) {
                        that.views.outputWorkflowParameterView = contentView;

                        that.views.outputWorkflowParameterView.$el.children().addClass(
                            'proui-state-hidden'
                        );
                      }
                    }),
                    contentModel: new mwa.proui.expander.ExpanderContentModel({
                      viewClass: mwa.view.workflowParameterEditor.WorkflowParameterView,
                      viewOptions: {
                        viewModel: new mwa.view.workflowParameterEditor.WorkflowParameterViewModel({
                          // INFORMATION:
                          // The output workflow parameter should always be read only.
                          isReadOnly: true,
                          canDelete:
                              !that.options.viewModel.get('isReadOnly') &&
                              selectedActivityModel.get('type') ===
                              mwa.enumeration.ActivityType.END,
                          dataWorkflowConnectionPortViewModelCollection:
                              that.models.outputDataWorkflowConnectionPortViewModelCollection,
                          dataWorkflowConnectionViewModelCollection:
                              that.models.outputDataWorkflowConnectionViewModelCollection
                        })
                      },
                      canResize: false,
                      width: '100%',
                      // INFORMATION:
                      // This is tentative value and should be updated after rendering.
                      height: 0
                    })
                  });

              that.updateWorkflowParameterExpanderContentHeight(
                  that.views.outputWorkflowParameterExpanderView
              );

              return that.views.outputWorkflowParameterExpanderView;
            },
            /**
             * The function to render workflow condition expander.
             * @protected
             *
             * @returns {!proui.expander.ExpanderView}
             * The rendered expander view instance.
             */
            renderWorkflowConditionExpander: function() {
              var that = this;

              // Initialization
              if (that.views.workflowConditionExpanderView != null) {
                that.views.workflowConditionExpanderView.remove();
              }
              that.views.workflowConditionExpanderView = null;

              // Rendering
              var selectedActivityModel =
                  that.options.viewModel.getSelectedActivityModel() ||
                  new mwa.model.activity.ActivityModel(null, {validate: false});
              var $el =
                  that.$el.find('.mwa-view-workflow-parameter-editor-workflow-condition-expander');
              that.views.workflowConditionExpanderView =
                  new mwa.proui.expander.ExpanderView({
                    el: $el,
                    viewModel: new mwa.proui.expander.ExpanderViewModel({
                      title: mwa.enumeration.Message['TITLE_CONDITION'],
                      isExpanded: true,
                      hasTooltip: false,
                      position: mwa.proui.expander.Position.TOP,
                      offset: that.$body.outerHeight() - that.$body.height(),
                      create: function(contentView, viewModel) {
                        that.views.workflowConditionView = contentView;
                      }
                    }),
                    contentModel: new mwa.proui.expander.ExpanderContentModel({
                      viewClass: mwa.view.workflowParameterEditor.WorkflowConditionView,
                      viewOptions: {
                        viewModel: new mwa.view.workflowParameterEditor.WorkflowConditionViewModel({
                          isReadOnly: that.options.viewModel.get('isReadOnly'),
                          canDelete:
                              !that.options.viewModel.get('isReadOnly') &&
                              selectedActivityModel.get('name') ===
                              mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH.templateName,
                          activityConditionCollection: that.models.activityConditionCollection
                        })
                      },
                      canResize: false,
                      width: '100%',
                      // INFORMATION:
                      // This is tentative value and should be updated after rendering.
                      height: 0
                    })
                  });

              that.updateWorkflowConditionExpanderContentHeight(
                  that.views.workflowConditionExpanderView
              );

              return that.views.workflowConditionExpanderView;
            },
            /**
             * The function to render workflow parameter add button.
             * @protected
             *
             * @returns {proui.commandButton.CommandButtonView}
             * The rendered command button view instance.
             */
            renderWorkflowParameterAddButton: function() {
              var that = this;

              // Initialization
              if (that.views.workflowParameterAddCommandButtonView != null) {
                that.views.workflowParameterAddCommandButtonView.remove();
              }
              that.views.workflowParameterAddCommandButtonView = null;

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-parameter-editor-workflow-parameter-add-button');
              that.views.workflowParameterAddCommandButtonView =
                  new mwa.proui.commandButton.CommandButtonView({
                    el: $el,
                    viewModel: new mwa.proui.commandButton.CommandButtonViewModel({
                      icon: $el.css('background-image'),
                      type: mwa.proui.commandButton.ButtonType.NONE,
                      size: mwa.proui.commandButton.ButtonSize.SMALL,
                      tooltip: mwa.enumeration.Message['TITLE_ADD'],
                      command: function() {
                        that.renderWorkflowParameterAddDialog();
                      }
                    })
                  });
              $el.css('background-image', 'none');

              return that.views.workflowParameterAddCommandButtonView;
            },
            /**
             * The function to render workflow parameter add dialog.
             * @protected
             *
             * @returns {prouiExt.metadataEditorDialog.MetadataEditorDialogView}
             * The rendered metadata editor dialog view instance.
             */
            renderWorkflowParameterAddDialog: function() {
              var that = this;

              // Initialization
              if (that.views.workflowParameterAddDialogView != null) {
                that.views.workflowParameterAddDialogView.remove();
              }
              that.views.workflowParameterAddDialogView = null;

              // Rendering
              var selectedActivityModel =
                  that.options.viewModel.getSelectedActivityModel() ||
                  new mwa.model.activity.ActivityModel(null, {validate: false});
              that.views.workflowParameterAddDialogView =
                  new mwa.prouiExt.metadataEditorDialog.MetadataEditorDialogView({
                    viewModel: new mwa.prouiExt.metadataEditorDialog.MetadataEditorDialogViewModel({
                      title: mwa.enumeration.Message['TITLE_NEW'],
                      texts: {
                        saveButtonText: mwa.enumeration.Message['TITLE_ADD'],
                        cancelButtonText: mwa.enumeration.Message['TITLE_CANCEL']
                      },
                      itemCollection: new mwa.model.activityParameter.ActivityParameterCollection(
                          [
                            // CAUTION:
                            // It is important to prepare the different model definition by
                            // extending activity parameter model with keeping validation settings
                            // in order to change idAttribute setting only because key attribute of
                            // activity parameter model will be changed and an error will occur in
                            // metadata editor (dialog) for update if the value of idAttribute is
                            // changed.
                            new (mwa.model.activityParameter.ActivityParameterModel.extend({
                              idAttribute: 'id'
                            }))({
                              id: mwa.proui.util.Uuid.generateUuidV4(),
                              visibleFlag: true,
                              // CAUTION:
                              // The required attribute of activity parameter model should be always
                              // false in end workflow step case.
                              required:
                                  selectedActivityModel.get('type') ===
                                  mwa.enumeration.ActivityType.START,
                              typeName: mwa.enumeration.ActivityParameterType.STRING.typeName,
                              type: mwa.enumeration.ActivityParameterType.STRING,
                              parentKey: null,
                              parentValue: null,
                              key: ''
                            })
                          ]
                      ),
                      metadataFieldCollection: new mwa.prouiExt.metadataEditor.MetadataFieldCollection([
                        {
                          key: 'key',
                          index: 0,
                          title: mwa.enumeration.Message['SENTENCE_KEY'],
                          type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                          valueCollection: null,
                          referenceValueCollection: null,
                          metadataFieldValidatorCollection:
                              new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                                // INFORMATION: NVXA-2144 (https://acropolis.atlassian.net/browse/NVXA-2144)
                                // This specification is described and required in NVXA-2144.
                                {
                                  type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MAX_LENGTH,
                                  value: 256,
                                  text:
                                      mwa.enumeration.Message['VALIDATION_MAXIMUM_LENGTH'].replace(
                                          /\{0\}/,
                                          mwa.enumeration.Message['SENTENCE_KEY'].toLowerCase()
                                      ).replace(
                                          /\{1\}/,
                                          '256'
                                      )
                                },
                                {
                                  // INFORMATION: NVXA-2144 (https://acropolis.atlassian.net/browse/NVXA-2144)
                                  //              NVXN-1362 (https://acropolis.atlassian.net/browse/NVXN-1362)
                                  // This specification is described and required in NVXA-2144.
                                  type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.PATTERN,
                                  value: /^.+$/,
                                  text:
                                      // INFORMATION: NVXA-2403 (https://acropolis.atlassian.net/browse/NVXN-2403)
                                      // This is same as validation error message for required case
                                      // but it is OK because validation error will occur only when
                                      // any number of space characters is specified only.
                                      mwa.enumeration.Message['VALIDATION_REQUIRED'].replace(
                                          /\{0\}/,
                                          mwa.enumeration.Message['SENTENCE_KEY'].toLowerCase()
                                      )
                                },
                                {
                                  type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.FUNCTION,
                                  value: function(value, attr, computedState) {
                                    var duplicatedParameterArray =
                                        mwa.proui._.where(
                                            [].concat(
                                                selectedActivityModel.get('inputs')
                                            ).concat(
                                                selectedActivityModel.get('outputs')
                                            ),
                                            {key: value}
                                        );

                                    return (duplicatedParameterArray.length === 0) ?
                                        null :
                                        (
                                            (
                                                duplicatedParameterArray.some(
                                                    function(duplicatedParameters, index, array) {
                                                      return duplicatedParameters.visibleFlag;
                                                    }
                                                )
                                            ) ?
                                                mwa.enumeration.Message['VALIDATION_UNIQUE'].replace(
                                                    /\{0\}/,
                                                    mwa.enumeration.Message['SENTENCE_KEY'].toLowerCase()
                                                ).replace(
                                                    /\{1\}/,
                                                    mwa.enumeration.Message['SENTENCE_INPUT_OUTPUT'].toLowerCase()
                                                ) :
                                                mwa.enumeration.Message['VALIDATION_RESERVED']
                                        );
                                  },
                                  text: null
                                }
                              ]),
                          isArray: false,
                          isHidden: false,
                          isRequired: true,
                          isReadOnly: false
                        },
                        {
                          key: 'typeName',
                          index: 1,
                          title: mwa.enumeration.Message['SENTENCE_TYPE'],
                          type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                          valueCollection: new mwa.proui.Backbone.Collection(
                              [].concat(
                                  mwa.proui._.values(mwa.enumeration.ActivityParameterType).filter(
                                      function(activityParameterType, index, array) {
                                        return activityParameterType.isImplemented;
                                      }
                                  ),
                                  that.options.viewModel.get('activityParameterTypeCollection').map(
                                      function(activityParameterTypeModel, index, array) {
                                        var name = activityParameterTypeModel.get('name');
                                        return {label: name, property: name};
                                      }
                                  )
                              )
                          ),
                          referenceValueCollection: null,
                          metadataFieldValidatorCollection:
                              new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                          isArray: false,
                          isHidden: false,
                          isRequired: true,
                          isReadOnly: false
                        },
                        {
                          key: 'listFlag',
                          index: 2,
                          title: mwa.enumeration.Message['SENTENCE_LIST'],
                          type: mwa.prouiExt.metadataEditor.MetadataFieldType.BOOLEAN,
                          valueCollection: null,
                          referenceValueCollection: null,
                          metadataFieldValidatorCollection:
                              new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                          isArray: false,
                          isHidden: true, // TODO: To display in future version.
                          isRequired: true,
                          isReadOnly: false
                        },
                        {
                          key: 'required',
                          index: 3,
                          title: mwa.enumeration.Message['SENTENCE_REQUIRED'],
                          type: mwa.prouiExt.metadataEditor.MetadataFieldType.BOOLEAN,
                          valueCollection: null,
                          referenceValueCollection: null,
                          metadataFieldValidatorCollection:
                              new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                          isArray: false,
                          // CAUTION:
                          // The required attribute of activity parameter model should be always
                          // hidden in end workflow step case.
                          isHidden:
                              selectedActivityModel.get('type') ===
                              mwa.enumeration.ActivityType.END,
                          isRequired: true,
                          // CAUTION:
                          // The required attribute of activity parameter model should be always
                          // read only in end workflow step case.
                          isReadOnly:
                              selectedActivityModel.get('type') ===
                              mwa.enumeration.ActivityType.END
                        }
                      ]),
                      save: function(itemAttributes) {
                        var workflowParameterModel =
                            that.views.workflowParameterAddDialogView.options.viewModel
                                .get('itemCollection').first();

                        var workflowConnectionPortViewModelArray = [
                          {
                            id: mwa.proui.util.Uuid.generateUuidV4(),
                            type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT
                          },
                          {
                            id: mwa.proui.util.Uuid.generateUuidV4(),
                            type: mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT
                          }
                        ].map(
                            // INFORMATION:
                            // The first argument is object type, not array. But we use plural
                            // name based on our JavaScript coding rule.
                            function(workflowParameters, index, array) {
                              return new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel(
                                  mwa.proui._.extend(
                                      workflowParameters,
                                      {
                                        key: workflowParameterModel.get('key'),
                                        isRequired: workflowParameterModel.get('required'),
                                        // INFORMATION:
                                        // To be careful about attribute name.
                                        positions: {
                                          x: 0,
                                          y: 0,
                                          z: 0
                                        },
                                        typeName: workflowParameterModel.get('typeName'),
                                        color: mwa.proui._.findWhere(
                                            WORKFLOW_CONNECTION_PORT_COLOR_ARRAY,
                                            {
                                              isArray: workflowParameterModel.get('listFlag'),
                                              activityParameterType:
                                                  mwa.proui._.findWhere(
                                                      ACTIVITY_PARAMETER_TYPE_ARRAY,
                                                      {
                                                        typeName:
                                                            workflowParameterModel.get('typeName')
                                                      }
                                                  ) ||
                                                  mwa.enumeration.ActivityParameterType.SETTING
                                            }
                                        ),
                                        workflowStepViewModel:
                                            that.options.viewModel
                                                .getSelectedWorkflowStepViewModel()
                                      }
                                  )
                              );
                            }
                        );

                        that.options.viewModel.get('workflowConnectionPortViewModelCollection').add(
                            workflowConnectionPortViewModelArray
                        );
                        // INFORMATION:
                        // It is better not to prepare the listeners for
                        // workflowConnectionPortViewModelCollection attribute of view model in
                        // consideration of performance because there is a situation that control
                        // workflow connection port view models will be added frequently (e.g. in
                        // workflow canvas).
                        that.models.inputDataWorkflowConnectionPortViewModelCollection.add(
                            workflowConnectionPortViewModelArray[0]
                        );
                        that.models.outputDataWorkflowConnectionPortViewModelCollection.add(
                            workflowConnectionPortViewModelArray[1]
                        );

                        // CAUTION:
                        // It is important to call remove function for close in consideration of
                        // memory leak.
                        that.views.workflowParameterAddDialogView.remove();
                      }
                    })
                  });

              return that.views.workflowParameterAddDialogView;
            },
            /**
             * The function to render workflow condition add button.
             * @protected
             *
             * @returns {proui.commandButton.CommandButtonView}
             * The rendered command button view instance.
             */
            renderWorkflowConditionAddButton: function() {
              var that = this;

              // Initialization
              if (that.views.workflowConditionAddCommandButtonView != null) {
                that.views.workflowConditionAddCommandButtonView.remove();
              }
              that.views.workflowConditionAddCommandButtonView = null;

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-parameter-editor-workflow-condition-add-button');
              that.views.workflowConditionAddCommandButtonView =
                  new mwa.proui.commandButton.CommandButtonView({
                    el: $el,
                    viewModel: new mwa.proui.commandButton.CommandButtonViewModel({
                      icon: $el.css('background-image'),
                      type: mwa.proui.commandButton.ButtonType.NONE,
                      size: mwa.proui.commandButton.ButtonSize.SMALL,
                      tooltip: mwa.enumeration.Message['TITLE_ADD'],
                      command: function() {
                        var firstPipelineWorkflowStepViewModel =
                            that.options.viewModel
                                .getSelectedWorkflowStepViewModel()
                                .get('pipelineWorkflowStepViewModelCollection')
                                .first();
                        var firstPipelineOutputActivityParameterModel =
                            firstPipelineWorkflowStepViewModel
                                .get('outputActivityParameterCollection')
                                .first();

                        that.models.activityConditionCollection.add(
                            {
                              conditionOperator:
                                  ACTIVITY_CONDITION_CONDITION_OPERATOR_ARRAY[0].operator,
                              leftValue:
                                  (
                                      firstPipelineOutputActivityParameterModel instanceof
                                      mwa.model.activityParameter.ActivityParameterModel
                                  ) ?
                                      (
                                          '$' +
                                          firstPipelineWorkflowStepViewModel.get('id') +
                                          '.' +
                                          firstPipelineOutputActivityParameterModel.get('key')
                                      ) :
                                      null,
                              comparisonOperator:
                                  ACTIVITY_CONDITION_COMPARISON_OPERATOR_ARRAY[0].operator,
                              rightValue: null
                            },
                            // CAUTION:
                            // It is necessary to enable parse to pass the validation.
                            {parse: true}
                        );
                      }
                    })
                  });
              $el.css('background-image', 'none');

              return that.views.workflowConditionAddCommandButtonView;
            },
            /**
             * The function to render information expander.
             * @protected
             *
             * @returns {!proui.expander.ExpanderView}
             * The rendered expander view instance.
             */
            renderInformationExpander: function() {
              var that = this;

              // Initialization
              if (that.views.informationExpanderView != null) {
                that.views.informationExpanderView.remove();
              }
              that.views.informationExpanderView = null;

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-parameter-editor-information-expander');
              var selectedActivityModel =
                  that.options.viewModel.getSelectedActivityModel() ||
                  new mwa.model.activity.ActivityModel(null, {validate: false});
              that.views.informationExpanderView =
                  new mwa.proui.expander.ExpanderView({
                    el: $el,
                    autoSyncObjectRef: that.$body,
                    viewModel: new mwa.proui.expander.ExpanderViewModel({
                      title: that.createInformationTitle(),
                      isExpanded: false,
                      hasTooltip: true,
                      position: mwa.proui.expander.Position.BOTTOM,
                      offset: that.$footer.outerHeight() - that.$footer.height(),
                      create: function(contentView, viewModel) {
                        that.views.informationTextBlockView = contentView;
                      }
                    }),
                    contentModel: new mwa.proui.expander.ExpanderContentModel({
                      viewClass: mwa.proui.textBlock.TextBlockView,
                      viewOptions: {
                        viewModel: new mwa.proui.textBlock.TextBlockViewModel({
                          text: selectedActivityModel.get('description'), // TODO: NVX-3487
                          hasTooltip: true,
                          enableMultiline: true
                        })
                      },
                      canResize: true,
                      width: '100%',
                      height: 200
                      // CAUTION:
                      // It is important not to set maxHeight attribute based on the outer height of
                      // body element in consideration of a case that this view isn't displayed in
                      // initial rendering. And the value will be updated correctly by resizeWindow
                      // function when resize is happen.
                    })
                  });

              return that.views.informationExpanderView;
            },
            /**
             * The function to update workflow parameter expander content height.
             * @protected
             *
             * @param {!proui.expander.ExpanderView} workflowParameterExpanderView
             * The workflow parameter expander view to be updated.
             */
            updateWorkflowParameterExpanderContentHeight: function(workflowParameterExpanderView) {
              if (workflowParameterExpanderView instanceof mwa.proui.expander.ExpanderView) {
                var itemLength =
                    workflowParameterExpanderView.options.contentModel
                        .get('viewOptions').viewModel
                        .get('dataWorkflowConnectionPortViewModelCollection')
                        .filter(
                            function(dataWorkflowConnectionPortViewModel, index, array) {
                              return (
                                  dataWorkflowConnectionPortViewModel
                                      .getActivityParameterModel()
                                      .get('visibleFlag')
                              );
                            }
                        ).length;

                workflowParameterExpanderView.options.contentModel.set(
                    'height',
                    calculateExpanderContentMinHeight(workflowParameterExpanderView) +
                    itemLength *
                    (
                        this._workflowParameterItemKeyHeight +
                        (
                            (
                                workflowParameterExpanderView ===
                                this.views.inputWorkflowParameterExpanderView
                            ) ?
                                this._workflowParameterItemValueHeight :
                                0
                        ) +
                        (
                            (
                                this.options.viewModel.hasSelectedActivityInstanceModel()
                            ) ?
                                this._workflowParameterItemResultHeight :
                                0
                        )
                    )
                );
              }
            },
            /**
             * The function to update workflow condition expander content height.
             * @protected
             *
             * @param {!proui.expander.ExpanderView} workflowConditionExpanderView
             * The workflow condition expander view to be updated.
             */
            updateWorkflowConditionExpanderContentHeight: function(workflowConditionExpanderView) {
              if (workflowConditionExpanderView instanceof mwa.proui.expander.ExpanderView) {
                var itemLength =
                    workflowConditionExpanderView.options.contentModel
                        .get('viewOptions').viewModel
                        .get('activityConditionCollection').length;

                workflowConditionExpanderView.options.contentModel.set(
                    'height',
                    calculateExpanderContentMinHeight(workflowConditionExpanderView) +
                    itemLength *
                    (
                        this._workflowConditionItemHeight +
                        (
                            (
                                this.options.viewModel.hasSelectedActivityInstanceModel()
                            ) ?
                                2 * this._workflowParameterItemResultHeight :
                                0
                        )
                    ) -
                    // CAUTION:
                    // It is necessary to reduce workflow condition item operator height because the
                    // first element always won't be displayed but it is also necessary to consider
                    // about a case that there is no workflow condition item.
                    Math.min(1, itemLength) * this._workflowConditionItemOperatorHeight
                );
              }
            },
            /**
             * The event handler of resize event on window.
             * @protected
             *
             * @param {object} event
             * The resize event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            resizeWindow: function(event) {
              if (this.views != null && this.views.informationExpanderView != null) {
                this.views.informationExpanderView.options.contentModel.set(
                    'maxHeight', this.$body.outerHeight()
                );
              }

              return true;
            },
            /**
             * The function to create information title based on selected workflow step.
             * @public
             *
             * @returns {?string}
             * The created information title.
             */
            createInformationTitle: function() {
              var that = this;

              var selectedActivityModel =
                  that.options.viewModel.getSelectedActivityModel() ||
                  new mwa.model.activity.ActivityModel(null, {validate: false});

              return (selectedActivityModel.has('alias')) ?
                  selectedActivityModel.get('alias') +
                  (
                      (mwa.util.WorkflowStep.isProcessing(selectedActivityModel)) ?
                      ' ' + '(' + mwa.enumeration.Message['TITLE_VERSION'] +
                      ' : ' + selectedActivityModel.get('version') + ')' :
                          ''
                  ) :
                  null;
            },
            /**
             * The event handler for changing, adding, removing, updating, resetting and destroying
             * models in inputDataWorkflowConnectionPortViewModelCollection or
             * outputDataWorkflowConnectionPortViewModelCollection. This function updates the height
             * of workflow parameter expander content and synchronize input workflow parameter and
             * output workflow parameter according to the value.
             * @protected
             */
            applyDataWorkflowConnectionPortViewModelCollection: function() {
              // arguments:
              // inputDataWorkflowConnectionPortViewModelCollection
              // outputDataWorkflowConnectionPortViewModelCollection
              //  - "change"                   (model, options)
              //  - "change:[attribute]"       (model, value, options)
              //  - "add", "remove", "destroy" (model, collection, options)
              //  - "update", "reset"          (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] instanceof mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel) {
                  if (arguments[1] !== that.models.inputDataWorkflowConnectionPortViewModelCollection &&
                      arguments[1] !== that.models.outputDataWorkflowConnectionPortViewModelCollection &&
                      // CAUTION:
                      // It is necessary to add this condition mainly for detecting destroy event
                      // correctly because the second argument is the first belonging collection.
                      arguments[1] !== that.options.viewModel.get('workflowConnectionPortViewModelCollection')) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add, remove or destroy event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      if (args.options.index != null) {
                        // Remove Case

                        // If you need any processing, add here.
                      } else {
                        // Destroy Case

                        // INFORMATION:
                        // This is for synchronizing input workflow parameter and output workflow
                        // parameter regarding user defined workflow parameters.
                        [
                          that.models.inputDataWorkflowConnectionPortViewModelCollection,
                          that.models.outputDataWorkflowConnectionPortViewModelCollection
                        ].forEach(
                            function(dataWorkflowConnectionPortViewModelCollection, index, array) {
                              var dataWorkflowConnectionPortViewModel =
                                  dataWorkflowConnectionPortViewModelCollection.findWhere({
                                    key: args.model.get('key')
                                  });

                              if (dataWorkflowConnectionPortViewModel instanceof
                                  mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel) {
                                dataWorkflowConnectionPortViewModelCollection.remove(
                                    dataWorkflowConnectionPortViewModel
                                );
                                // CAUTION:
                                // It is necessary to destroy the data workflow connection port view
                                // model explicitly in consideration of that an another data
                                // workflow connection port view model hasn't been destroyed yet.
                                dataWorkflowConnectionPortViewModel.destroy();
                              }
                            }
                        );
                      }
                    }
                  }
                } else if (arguments[0] === that.models.inputDataWorkflowConnectionPortViewModelCollection ||
                    arguments[0] === that.models.outputDataWorkflowConnectionPortViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  if (that.views != null) {
                    switch (args.collection) {
                      case that.models.inputDataWorkflowConnectionPortViewModelCollection:
                        that.updateWorkflowParameterExpanderContentHeight(
                            that.views.inputWorkflowParameterExpanderView
                        );
                        break;

                      case that.models.outputDataWorkflowConnectionPortViewModelCollection:
                        that.updateWorkflowParameterExpanderContentHeight(
                            that.views.outputWorkflowParameterExpanderView
                        );
                        break;

                      default:
                        break;
                    }
                  }
                }
              }
            },
            /**
             * The event handler for changing, adding, removing, updating, resetting and destroying
             * models in activityConditionCollection. This function updates the height of workflow
             * condition expander content according to the value.
             * @protected
             */
            applyActivityConditionCollection: function() {
              // arguments:
              // activityConditionCollection
              //  - "change"                   (model, options)
              //  - "change:[attribute]"       (model, value, options)
              //  - "add", "remove", "destroy" (model, collection, options)
              //  - "update", "reset"          (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] instanceof mwa.model.activityCondition.ActivityConditionModel) {
                  if (arguments[1] !== that.models.activityConditionCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.model.activityCondition.ActivityConditionModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.model.activityCondition.ActivityConditionModel} model
                     * The added or removed model.
                     * @property {mwa.model.activityCondition.ActivityConditionCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add, remove or destroy event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      if (args.options.index != null) {
                        // Remove Case

                        // If you need any processing, add here.
                      } else {
                        // Destroy Case

                        // If you need any processing, add here.
                      }
                    }
                  }
                } else if (arguments[0] === that.models.activityConditionCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.model.activityCondition.ActivityConditionCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  if (that.views != null) {
                    that.updateWorkflowConditionExpanderContentHeight(
                        that.views.workflowConditionExpanderView
                    );
                  }
                }
              }
            },
            /**
             * The event handler for changing isReadOnly attribute of view model. This function
             * updates isReadOnly attribute of input workflow parameter view model according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.workflowParameterEditorViewModel} model
             * The mwa workflow parameter editor view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsReadOnly: function(model, value, options) {
              if (this.views != null && this.views.inputWorkflowParameterView != null) {
                // INFORMATION:
                // The output workflow parameter should always be read only.
                this.views.inputWorkflowParameterView.options.viewModel.set(
                    'isReadOnly', this.options.viewModel.get('isReadOnly')
                );
              }
            },
            /**
             * The event handler for changing workflowStepViewModelCollection attribute of view
             * model and changing, adding, removing, updating and resetting models in
             * workflowStepViewModelCollection. This function updates workflow step drop down
             * button, input workflow parameter and output workflow parameter according to the
             * value.
             * @protected
             */
            applyWorkflowStepViewModelCollection: function() {
              // arguments:
              // WorkflowParameterEditorViewModel
              //  - "change:workflowStepViewModelCollection" (model, value, options)
              // workflowStepViewModelCollection
              //  - "change"                                 (model, options)
              //  - "change:[attribute]"                     (model, value, options)
              //  - "add", "remove"                          (model, collection, options)
              //  - "update", "reset"                        (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that.options.viewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel} model
                   * The mwa workflow parameter editor view model.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                  if (arguments[1] !== that.options.viewModel.get('workflowStepViewModelCollection')) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    var selectedWorkflowStepViewModel =
                        that.options.viewModel.getSelectedWorkflowStepViewModel() ||
                        new mwa.view.workflowStep.WorkflowStepViewModel(null, {validate: false});
                    var selectedActivityModel =
                        that.options.viewModel.getSelectedActivityModel() ||
                        new mwa.model.activity.ActivityModel(null, {validate: false});
                    var selectedWorkflowStepType =
                        mwa.proui._.findWhere(
                            WORKFLOW_STEP_TYPE_ARRAY,
                            {templateName: selectedActivityModel.get('name')}
                        ) ||
                        // CAUTION:
                        // It is important to prepare dummy value for processing workflow step.
                        {
                          // CAUTION:
                          // It is necessary to prepare templateName property to detect whether
                          // workflow step is selected.
                          templateName: selectedActivityModel.get('name'),
                          hasParameter: true
                        };

                    that.listenToSelectedPipelineWorkflowStepViewModelCollection(
                        selectedWorkflowStepViewModel,
                        selectedWorkflowStepViewModel
                            .get('pipelineWorkflowStepViewModelCollection'),
                        args.options
                    );

                    if (that.views != null) {
                      if (that.views.workflowStepDropDownButtonView != null) {
                        that.views.workflowStepDropDownButtonView.options.viewModel.set({
                          selectedItem: selectedWorkflowStepViewModel
                        });
                      }

                      if (that.views.activityInstanceInformationView != null) {
                        that.views.activityInstanceInformationView.$el.toggle(
                            selectedWorkflowStepViewModel.has('activityInstanceModel')
                        );

                        that.views.activityInstanceInformationView.options.viewModel.set({
                          activityInstanceModel:
                              selectedWorkflowStepViewModel.get('activityInstanceModel')
                        });
                      }

                      if (that.views.inputWorkflowParameterExpanderView != null) {
                        that.views.inputWorkflowParameterExpanderView.$el.toggle(
                            selectedWorkflowStepType.templateName != null &&
                            selectedWorkflowStepType.hasParameter
                        );
                      }
                      if (that.views.inputWorkflowParameterView != null) {
                        that.models.inputDataWorkflowConnectionPortViewModelCollection
                            .reset(
                                that.options.viewModel
                                    .get('workflowConnectionPortViewModelCollection')
                                    .where({
                                      type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT,
                                      workflowStepViewModel: selectedWorkflowStepViewModel
                                    })
                            );

                        that.views.inputWorkflowParameterView.options.viewModel.set({
                          canDelete:
                              !that.options.viewModel.get('isReadOnly') &&
                              selectedActivityModel.get('type') ===
                              mwa.enumeration.ActivityType.START
                        });
                      }

                      if (that.views.outputWorkflowParameterExpanderView != null) {
                        that.views.outputWorkflowParameterExpanderView.$el.toggle(
                            selectedWorkflowStepType.templateName != null &&
                            selectedWorkflowStepType.hasParameter
                        );
                      }
                      if (that.views.outputWorkflowParameterView != null) {
                        that.models.outputDataWorkflowConnectionPortViewModelCollection
                            .reset(
                                that.options.viewModel
                                    .get('workflowConnectionPortViewModelCollection')
                                    .where({
                                      type: mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT,
                                      workflowStepViewModel: selectedWorkflowStepViewModel
                                    })
                            );

                        that.views.outputWorkflowParameterView.options.viewModel.set({
                          canDelete:
                              !that.options.viewModel.get('isReadOnly') &&
                              selectedActivityModel.get('type') ===
                              mwa.enumeration.ActivityType.END
                        });
                      }

                      if (that.views.workflowParameterAddCommandButtonView != null) {
                        that.views.workflowParameterAddCommandButtonView.$el.hide();

                        if (!that.options.viewModel.get('isReadOnly')) {
                          switch (selectedActivityModel.get('type')) {
                            case mwa.enumeration.ActivityType.START:
                              if (that.views.inputWorkflowParameterExpanderView != null) {
                                that.views.workflowParameterAddCommandButtonView.$el
                                    .show()
                                    .appendTo(
                                        that.views.inputWorkflowParameterExpanderView.$el
                                            .find('.proui-expander-title-background')
                                    );
                              }
                              break;

                            case mwa.enumeration.ActivityType.END:
                              if (that.views.outputWorkflowParameterExpanderView != null) {
                                that.views.workflowParameterAddCommandButtonView.$el
                                    .show()
                                    .appendTo(
                                        that.views.outputWorkflowParameterExpanderView.$el
                                            .find('.proui-expander-title-background')
                                    );
                              }
                              break;

                            default:
                              break;
                          }
                        }
                      }

                      if (that.views.workflowConditionExpanderView != null) {
                        that.views.workflowConditionExpanderView.$el.toggle(
                            selectedWorkflowStepType ===
                            mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH
                        );
                      }
                      if (that.views.workflowConditionView != null) {
                        // CAUTION:
                        // It is important to update conditionDataWorkflowConnectionPortViewModel
                        // attribute of workflow condition view model before updating
                        // activityConditionCollection attribute of workflow condition view model
                        // to apply the new value before re-rendering.
                        that.views.workflowConditionView.options.viewModel.set(
                            'conditionDataWorkflowConnectionPortViewModel',
                            that.options.viewModel
                                .get('workflowConnectionPortViewModelCollection')
                                .findWhere({
                                  key: mwa.enumeration.ActivityParameter.CONDITION.key,
                                  type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT,
                                  workflowStepViewModel: selectedWorkflowStepViewModel
                                })
                        );

                        var conditionActivityParameterModel =
                            selectedWorkflowStepViewModel
                                .get('inputActivityParameterCollection')
                                // INFORMATION:
                                // It is possible to use get function instead of
                                // findWhere function in consideration of that
                                // idAttribute of activity parameter model is "key", not
                                // "id", but it is better to use findWhere function to
                                // make the code maintainable and readable.
                                .findWhere({
                                  key: mwa.enumeration.ActivityParameter.CONDITION.key
                                });
                        that.models.activityConditionCollection
                            .reset(
                                (
                                    selectedWorkflowStepType ===
                                    mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH
                                ) ?
                                    JSON.parse(
                                        conditionActivityParameterModel.get('type').decodeText(
                                            conditionActivityParameterModel.get('value')
                                        )
                                    ) :
                                    [],
                                {parse: true}
                            );

                        that.views.workflowConditionView.options.viewModel.set({
                          canDelete:
                              !that.options.viewModel.get('isReadOnly') &&
                              selectedActivityModel.get('name') ===
                              mwa.enumeration.WorkflowStepType.CONDITIONAL_BRANCH.templateName
                        });
                      }

                      if (that.views.workflowConditionAddCommandButtonView != null) {
                        that.views.workflowConditionAddCommandButtonView.$el.hide();

                        if (!that.options.viewModel.get('isReadOnly')) {
                          if (that.views.workflowConditionExpanderView != null) {
                            that.views.workflowConditionAddCommandButtonView.$el
                                .show()
                                .appendTo(
                                    that.views.workflowConditionExpanderView.$el
                                        .find('.proui-expander-title-background')
                                );
                          }
                        }

                        that.views.workflowConditionAddCommandButtonView.options.viewModel.set({
                          isDisabled:
                              selectedWorkflowStepViewModel
                                  .get('pipelineWorkflowStepViewModelCollection').isEmpty()
                        });
                      }

                      if (that.views.informationExpanderView != null) {
                        that.views.informationExpanderView.$el.toggle(
                            selectedWorkflowStepType.templateName != null
                        );

                        that.views.informationExpanderView.options.viewModel.set({
                          title: that.createInformationTitle()
                        });
                      }

                      if (that.views.informationTextBlockView != null) {
                        that.views.informationTextBlockView.options.viewModel.set({
                          text: selectedActivityModel.get('description')
                        });
                      }
                    }
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // To do same processing as change event on model.
                      that.applyWorkflowStepViewModelCollection(args.model, args.options);
                    }
                  }
                } else if (arguments[0] === that.options.viewModel.get('workflowStepViewModelCollection')) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }
                }
              }
            },
            /**
             * The event handler for changing workflowConnectionPortViewModelCollection attribute of
             * view model and changing, adding, removing, updating and resetting models in
             * workflowConnectionPortViewModelCollection. This function updates input and output
             * workflow parameter according to the value.
             * @protected
             * @deprecated since version 2.11.0
             */
            applyWorkflowConnectionPortViewModelCollection: function() {
              // arguments:
              // WorkflowParameterEditorViewModel
              //  - "change:workflowConnectionPortViewModelCollection" (model, value, options)
              // workflowConnectionPortViewModelCollection
              //  - "change"                                           (model, options)
              //  - "change:[attribute]"                               (model, value, options)
              //  - "add", "remove"                                    (model, collection, options)
              //  - "update", "reset"                                  (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] instanceof mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel) {
                  if (arguments[1] !== that.options.viewModel.get('workflowConnectionPortViewModelCollection')) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add, remove or destroy event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === that.options.viewModel.get('workflowConnectionPortViewModelCollection')) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }
                }
              }
            },
            /**
             * The event handler for changing workflowConnectionViewModelCollection attribute of
             * view model and changing, adding, removing, updating and resetting models in
             * workflowConnectionViewModelCollection. This function updates input workflow parameter
             * according to the value.
             * @protected
             */
            applyWorkflowConnectionViewModelCollection: function() {
              // arguments:
              // WorkflowParameterEditorViewModel
              //  - "change:workflowConnectionViewModelCollection" (model, value, options)
              // workflowConnectionViewModelCollection
              //  - "change"                                       (model, options)
              //  - "change:[attribute]"                           (model, value, options)
              //  - "add", "remove"                                (model, collection, options)
              //  - "update", "reset"                              (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that.options.viewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowParameterEditor.WorkflowParameterEditorViewModel} model
                   * The mwa workflow parameter editor view model.
                   * @param {mwa.view.workflowConnection.WorkflowConnectionViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.view.workflowConnection.WorkflowConnectionViewModel) {
                  if (arguments[1] !== that.options.viewModel.get('workflowConnectionViewModelCollection')) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowConnection.WorkflowConnectionViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowConnection.WorkflowConnectionViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === that.options.viewModel.get('workflowConnectionViewModelCollection')) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowConnection.WorkflowConnectionViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  that.models.inputDataWorkflowConnectionViewModelCollection
                      .set(
                          // CAUTION:
                          // It is important to pass all data workflow connection regardless of
                          // displayed data workflow connection port in order not to update
                          // dataWorkflowConnectionViewModelCollection attribute of workflow
                          // parameter view model when selected workflow step is changed.
                          args.collection.filter(filterDataWorkflowConnectionViewModelCollection)
                      );
                }
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * selected workflow step view model and changing, adding, removing, updating and
             * resetting models in pipelineWorkflowStepViewModelCollection. This function updates
             * workflow condition add button according to the value.
             * @protected
             */
            applySelectedPipelineWorkflowStepViewModelCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:pipelineWorkflowStepViewModelCollection" (model, value, options)
              // pipelineWorkflowStepViewModelCollection
              //  - "change"                                         (model, options)
              //  - "change:[attribute]"                             (model, value, options)
              //  - "add", "remove"                                  (model, collection, options)
              //  - "update", "reset"                                (collection, options)
              var that = this;
              var args = {};

              var selectedWorkflowStepViewModel =
                  that.options.viewModel.getSelectedWorkflowStepViewModel() ||
                  new mwa.view.workflowStep.WorkflowStepViewModel(null, {validate: false});
              var selectedPipelineWorkflowStepViewModelCollection =
                  selectedWorkflowStepViewModel.get('pipelineWorkflowStepViewModelCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === selectedWorkflowStepViewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                  if (arguments[1] !== selectedPipelineWorkflowStepViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === selectedPipelineWorkflowStepViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  if (that.views != null && that.views.workflowConditionAddCommandButtonView) {
                    that.views.workflowConditionAddCommandButtonView.options.viewModel.set({
                      isDisabled: selectedPipelineWorkflowStepViewModelCollection.isEmpty()
                    });
                  }
                }
              }
            }
          }
      );
    })();

    /**
     * The mwa activity instance information view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.ActivityInstanceInformationViewModel
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.ViewModel}
     */
    global.ActivityInstanceInformationViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.ActivityInstanceInformationViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    activityInstanceModel: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  activityInstanceModel: [
                    {
                      required: false
                    },
                    {
                      instance: mwa.model.activityInstance.ActivityInstanceModel
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa activity instance information view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.ActivityInstanceInformationView
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.View}
     */
    global.ActivityInstanceInformationView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.ActivityInstanceInformationView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-parameter-editor-activity-instance-information">' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.listenTo(
                  this.options.viewModel,
                  'change:activityInstanceModel',
                  this.applyActivityInstanceModel
              );
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              mwa.View.prototype.render.apply(that, []);

              that.$children = that.$el.children();

              that.renderItems();

              return that;
            },
            /**
             * The function to render item.
             * @protected
             *
             * @param {!string} key
             * The string to be displayed as key.
             * @param {!string} value
             * The string to be displayed as value.
             * @param {!number} index
             * The number to specify the index of specified key and value.
             *
             * @returns {!mwa.view.workflowParameterEditor.ActivityInstanceInformationItemView}
             * The rendered activity instance information item view instance.
             */
            renderItem: function(key, value, index) {
              var that = this;

              var itemView =
                  new mwa.view.workflowParameterEditor.ActivityInstanceInformationItemView({
                    viewModel: new mwa.view.workflowParameterEditor.ActivityInstanceInformationItemViewModel({
                      key: key,
                      value: value
                    })
                  });

              var $item = that.$children.children();
              if ($item.length === 0 || index <= 0) {
                that.$children.prepend(itemView.render().$el);
              } else {
                $item.eq(index - 1).after(itemView.render().$el);
              }
              that.views.itemViewArray.splice(index, 0, itemView);

              if (key === mwa.enumeration.Message['SENTENCE_PROGRESS']) {
                that.renderProgressBar(itemView.$el.children().last());
              }

              return itemView;
            },
            /**
             * The function to render items.
             * @protected
             *
             * @returns {!mwa.view.workflowParameterEditor.ActivityInstanceInformationItemView[]}
             * The rendered activity instance information item view instances.
             */
            renderItems: function() {
              var that = this;

              // Initialization
              mwa.proui._.invoke(that.views.itemViewArray, 'remove');
              that.views.itemViewArray = [];

              // Rendering
              var activityInstanceModel = that.options.viewModel.get('activityInstanceModel');
              if (activityInstanceModel instanceof
                  mwa.model.activityInstance.ActivityInstanceModel) {
                var index = 0;

                that.renderItem(
                    mwa.enumeration.Message['SENTENCE_ID'],
                    activityInstanceModel.get('id'),
                    index++
                );
                that.renderItem(
                    mwa.enumeration.Message['SENTENCE_START_DATE_TIME'],
                    mwa.proui.util.Date.formatDate(
                        new Date(activityInstanceModel.get('startTime'))
                    ),
                    index++
                );
                that.renderItem(
                    mwa.enumeration.Message['SENTENCE_END_DATE_TIME'],
                    (activityInstanceModel.get('status').terminated) ?
                        mwa.proui.util.Date.formatDate(
                            new Date(activityInstanceModel.get('endTime'))
                        ) :
                        '',
                    index++
                );
                that.renderItem(
                    mwa.enumeration.Message['SENTENCE_PROGRESS'],
                    '',
                    index++
                );
                that.renderItem(
                    mwa.enumeration.Message['SENTENCE_DETAIL_PLURAL'],
                    (activityInstanceModel.get('statusDetails') || {}).deviceResponseDetails || '',
                    index++
                );
              }

              return that.views.itemViewArray;
            },
            /**
             * The function to render progress bar.
             * @protected
             *
             * @param {!object} $el
             * The jQuery object to be used for target element of rendering.
             *
             * @returns {?proui.progressBar.ProgressBarView}
             * The rendered progress bar view instance.
             */
            renderProgressBar: function($el) {
              var that = this;

              // Initialization
              if (that.views.progressBarView != null) {
                that.views.progressBarView.remove();
              }
              that.views.progressBarView = null;

              // Rendering
              var activityInstanceModel = that.options.viewModel.get('activityInstanceModel');
              if (activityInstanceModel instanceof
                  mwa.model.activityInstance.ActivityInstanceModel) {
                var statuses = activityInstanceModel.get('status');
                var progress = activityInstanceModel.get('progress');
                this.views.progressBarView =
                    new mwa.proui.progressBar.ProgressBarView({
                      el: $el,
                      viewModel: new mwa.proui.progressBar.ProgressBarViewModel({
                        condition:
                            (
                                mwa.enumeration.ActivityInstanceStatus[statuses.name] ||
                                (
                                    (statuses.stable && statuses.terminated) ?
                                        {
                                          progressBarCondition:
                                              mwa.proui.progressBar.ProgressBarCondition.SUCCESS
                                        } :
                                        {
                                          progressBarCondition: null
                                        }
                                )
                            ).progressBarCondition,
                        weight: mwa.proui.progressBar.ProgressBarWeight.THICK,
                        value: progress,
                        text: progress + ' %',
                        tooltip: function(viewModel) {
                          return viewModel.get('value') + ' %';
                        }
                      })
                    });
              }

              return that.views.progressBarView;
            },
            /**
             * The event handler for changing activityInstanceModel attribute of view model. This
             * function re-renders view according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.ActivityInstanceInformationViewModel} model
             * The mwa activity instance information view model.
             * @param {mwa.model.activityInstance.ActivityInstanceModel} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyActivityInstanceModel: function(model, value, options) {
              this.render();
            }
          }
      );
    })();

    /**
     * The mwa activity instance information item view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.ActivityInstanceInformationItemViewModel
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.ViewModel}
     */
    global.ActivityInstanceInformationItemViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.ActivityInstanceInformationItemViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    key: null,
                    value: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  key: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  value: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {string} [attributes.key = null]
             * The string to be displayed as key. 
             * @param {string} [attributes.value = null]
             * The string to be displayed as value.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, [attributes, options]);
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa activity instance information item view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.ActivityInstanceInformationItemView
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.View}
     */
    global.ActivityInstanceInformationItemView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.ActivityInstanceInformationItemView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-parameter-editor-activity-instance-information-item">' +
                    '<div class="proui-text-list-item proui-text-ellipsis"><@- key @></div>' +
                    '<div><@- value @></div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              that.remove({silent: true});
              // INFORMATION:
              // It is important to call remove function instead of empty function for creating this
              // view's element from template.
              that.$el.remove();
              that.setElement(mwa.proui.Backbone.$(that.template(that.presenter())));
              that.cacheElement();

              that.views = {};

              return this;
            },
            /**
             * The function to cache element for internal processing.
             * @protected
             */
            cacheElement: function() {
              // If you need to cache elements, add here.
            }
          }
      );
    })();

    /**
     * The mwa workflow parameter view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowParameterViewModel
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.ViewModel}
     */
    global.WorkflowParameterViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowParameterViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    isReadOnly: false,
                    canDelete: false,
                    dataWorkflowConnectionPortViewModelCollection:
                        new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection(),
                    dataWorkflowConnectionViewModelCollection:
                        new mwa.view.workflowConnection.WorkflowConnectionViewModelCollection()
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  isReadOnly: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDelete: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  dataWorkflowConnectionPortViewModelCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        var firstDataWorkflowConnectionPortViewModelType =
                            (
                                this.get('dataWorkflowConnectionPortViewModelCollection').first() ||
                                new mwa.proui.Backbone.Model({
                                  type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT
                                })
                            ).get('type');
                        if (firstDataWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT &&
                            firstDataWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                          msg =
                              'The "type" of data workflow connection port view model must be ' +
                              'mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT or ' +
                              'mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT.';
                        } else if (this.get('dataWorkflowConnectionPortViewModelCollection').some(
                            function(dataWorkflowConnectionPortViewModel, index, array) {
                              return (
                                  dataWorkflowConnectionPortViewModel.get('type') !==
                                  firstDataWorkflowConnectionPortViewModelType
                              );
                            }
                            )) {
                          msg =
                              'The "type" of data workflow connection port view model ' +
                              'in "' + attr + '" must be the same.';
                        }
                        return msg;
                      }
                    }
                  ],
                  dataWorkflowConnectionViewModelCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowConnection.WorkflowConnectionViewModelCollection
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (this.get('dataWorkflowConnectionViewModelCollection').some(
                                function(dataWorkflowConnectionViewModel, index, array) {
                                  return (
                                      dataWorkflowConnectionViewModel
                                          .get('fromWorkflowConnectionPortViewModel')
                                          .get('type') !==
                                      mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT ||
                                      dataWorkflowConnectionViewModel
                                          .get('toWorkflowConnectionPortViewModel')
                                          .get('type') !==
                                      mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT
                                  );
                                }
                            )) {
                          msg = 'The "' + attr + '" must include data workflow connection only.';
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa workflow parameter view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowParameterView
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.View}
     */
    global.WorkflowParameterView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowParameterView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-parameter-editor-workflow-parameter">' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.listenTo(
                  this.options.viewModel,
                  'change:isReadOnly',
                  this.applyIsReadOnly
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canDelete',
                  this.applyCanDelete
              );

              this.listenToDataWorkflowConnectionPortViewModelCollection(
                  this.options.viewModel,
                  this.options.viewModel.get('dataWorkflowConnectionPortViewModelCollection'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:dataWorkflowConnectionPortViewModelCollection',
                  this.listenToDataWorkflowConnectionPortViewModelCollection
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:dataWorkflowConnectionPortViewModelCollection',
                  this.applyDataWorkflowConnectionPortViewModelCollection
              );
            },
            /**
             * The event handler for changing dataWorkflowConnectionPortViewModelCollection
             * attribute of view model. This function removes listeners from old
             * dataWorkflowConnectionPortViewModelCollection and adds listeners to new
             * dataWorkflowConnectionPortViewModelCollection.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterViewModel} model
             * The mwa workflow parameter view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToDataWorkflowConnectionPortViewModelCollection: function(model, value, options) {
              var previousValue = model.previous('dataWorkflowConnectionPortViewModelCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(
                    value,
                    'add',
                    this.applyDataWorkflowConnectionPortViewModelCollection
                );
                this.listenTo(
                    value,
                    'remove',
                    this.applyDataWorkflowConnectionPortViewModelCollection
                );
                this.listenTo(
                    value,
                    'reset',
                    this.applyDataWorkflowConnectionPortViewModelCollection
                );
              }
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              mwa.View.prototype.render.apply(that, []);

              that.$children = that.$el.children();

              that.renderItems();

              return that;
            },
            /**
             * The function to render item.
             * @protected
             *
             * @param {!mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} dataWorkflowConnectionPortViewModel
             * The data workflow connection port view model to be rendered.
             * @param {!number} index
             * The number to specify the index of specified data workflow connection port view
             * model.
             *
             * @returns {!mwa.view.workflowParameterEditor.WorkflowParameterItemView}
             * The rendered workflow parameter item view instance.
             */
            renderItem: function(dataWorkflowConnectionPortViewModel, index) {
              var that = this;

              var itemView =
                  new mwa.view.workflowParameterEditor.WorkflowParameterItemView({
                    viewModel: new mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel({
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      // CAUTION:
                      // Be careful about not forgetting to set canDelete attribute because
                      // applyWorkflowStepViewModelCollection function of workflow parameter editor
                      // view will be called twice by selection and deselection of workflow step and
                      // renderItems function of workflow parameter view will be also called twice
                      // but applyCanDelete function of workflow parameter view won't be called
                      // twice due to the behavior that canDelete attribute of workflow parameter
                      // view model won't be changed in second call of
                      // applyWorkflowStepViewModelCollection function of workflow parameter editor.
                      canDelete: that.options.viewModel.get('canDelete'),
                      dataWorkflowConnectionPortViewModel: dataWorkflowConnectionPortViewModel,
                      dataWorkflowConnectionViewModel:
                          that.options.viewModel
                              .get('dataWorkflowConnectionViewModelCollection').findWhere({
                                toWorkflowConnectionPortViewModel:
                                    dataWorkflowConnectionPortViewModel
                              })
                    })
                  });

              var $item = that.$children.children();
              if ($item.length === 0 || index <= 0) {
                that.$children.prepend(itemView.render().$el);
              } else {
                $item.eq(index - 1).after(itemView.render().$el);
              }
              that.views.itemViewArray.splice(index, 0, itemView);

              return itemView;
            },
            /**
             * The function to render items.
             * @protected
             *
             * @returns {!mwa.view.workflowParameterEditor.WorkflowParameterItemView[]}
             * The rendered workflow parameter item view instances.
             */
            renderItems: function() {
              var that = this;

              // Initialization
              mwa.proui._.invoke(that.views.itemViewArray, 'remove');
              that.views.itemViewArray = [];

              // Rendering
              that.options.viewModel.get('dataWorkflowConnectionPortViewModelCollection').forEach(
                  mwa.proui._.bind(that.renderItem, that)
              );

              return that.views.itemViewArray;
            },
            /**
             * The event handler for changing isReadOnly attribute of view model. This function
             * updates isReadOnly attribute of workflow parameter item view models according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterViewModel} model
             * The mwa workflow parameter view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsReadOnly: function(model, value, options) {
              var that = this;

              if (that.views != null && that.views.itemViewArray != null) {
                that.views.itemViewArray.forEach(
                    function(itemView, index, array) {
                      itemView.options.viewModel.set(
                          'isReadOnly', that.options.viewModel.get('isReadOnly')
                      );
                    }
                );
              }
            },
            /**
             * The event handler for changing canDelete attribute of view model. This function
             * updates canDelete attribute of workflow parameter item view models according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterViewModel} model
             * The mwa workflow parameter view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDelete: function(model, value, options) {
              var that = this;

              if (that.views != null && that.views.itemViewArray != null) {
                that.views.itemViewArray.forEach(
                    function(itemView, index, array) {
                      itemView.options.viewModel.set(
                          'canDelete', that.options.viewModel.get('canDelete')
                      );
                    }
                );
              }
            },
            /**
             * The event handler for changing dataWorkflowConnectionPortViewModelCollection
             * attribute of view model and changing, adding, removing, updating and resetting models
             * in dataWorkflowConnectionPortViewModelCollection. This function updates workflow
             * parameter item views according to the value.
             * @protected
             */
            applyDataWorkflowConnectionPortViewModelCollection: function() {
              // arguments:
              // WorkflowParameterViewModel
              //  - "change:dataWorkflowConnectionPortViewModelCollection" (model, value, options)
              // dataWorkflowConnectionPortViewModelCollection
              //  - "change"                                               (model, options)
              //  - "add", "remove"                                        (model, collection, options)
              //  - "update", "reset"                                      (collection, options)
              var that = this;
              var args = {};

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that.options.viewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowParameterEditor.WorkflowParameterViewModel} model
                   * The mwa workflow parameter view model.
                   * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  that.renderItems();
                } else if (arguments[0] instanceof mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel) {
                  if (arguments[1] !== that.options.viewModel.get('dataWorkflowConnectionPortViewModelCollection')) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      if (that.views != null && that.views.itemViewArray != null) {
                        that.renderItem(args.model, index);
                      }
                    } else {
                      // Remove Case

                      if (that.views != null && that.views.itemViewArray != null) {
                        that.views.itemViewArray.splice(args.options.index, 1);
                      }
                    }
                  }
                } else if (arguments[0] === that.options.viewModel.get('dataWorkflowConnectionPortViewModelCollection')) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    that.renderItems();
                  }
                }
              }
            }
          }
      );
    })();

    /**
     * The mwa workflow parameter item view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.ViewModel}
     */
    global.WorkflowParameterItemViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    hasPredefinedWorkflowStep: true,
                    isReadOnly: false,
                    canDelete: false,
                    dataWorkflowConnectionPortViewModel: null,
                    dataWorkflowConnectionViewModel: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  hasPredefinedWorkflowStep: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  isReadOnly: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDelete: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  dataWorkflowConnectionPortViewModel: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        var dataWorkflowConnectionPortViewModelType =
                            this.get('dataWorkflowConnectionPortViewModel').get('type');
                        if (dataWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT &&
                            dataWorkflowConnectionPortViewModelType !==
                            mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT) {
                          msg =
                              'The "type" of "' + attr + '" must be ' +
                              'mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT or ' +
                              'mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT';
                        }
                        return msg;
                      }
                    }
                  ],
                  dataWorkflowConnectionViewModel: [
                    {
                      required: false
                    },
                    {
                      instance: mwa.view.workflowConnection.WorkflowConnectionViewModel
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (value instanceof
                            mwa.view.workflowConnection.WorkflowConnectionViewModel) {
                          if (value.get('fromWorkflowConnectionPortViewModel').get('type') !==
                              mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT ||
                              value.get('toWorkflowConnectionPortViewModel').get('type') !==
                              mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT) {
                            msg = 'The "' + attr + '" must be data workflow connection.';
                          }
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {boolean} [attributes.hasPredefinedWorkflowStep = true]
             * The flag whether predefined workflow step is displayed.
             * @param {boolean} [attributes.isReadOnly = false]
             * The flag whether workflow parameter is read only.
             * @param {boolean} [attributes.canDelete = false]
             * The flag whether workflow parameter is deletable.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} [attributes.dataWorkflowConnectionPortViewModel = null]
             * The data workflow connection port view model corresponding to workflow parameter.
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} [attributes.dataWorkflowConnectionViewModel = null]
             * The data workflow connection view model for updating specified workflow parameter.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, [attributes, options]);
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa workflow parameter item view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowParameterItemView
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.View}
     */
    global.WorkflowParameterItemView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowParameterItemView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item' +
                '<@ if (!activityParameterModel.get("visibleFlag")) { @>' +
                    ' proui-state-hidden' +
                '<@ } @>' +
                '<@ if (activityParameterModel.get("required")) { @>' +
                    ' mwa-view-workflow-parameter-editor-state-required' +
                '<@ } @>' +
                '"' +
                '>' +
                    '<div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-key proui-text-list-item">' +
                            '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-key-name proui-text-ellipsis">' +
                                '<@= activityParameterModel.get("key") @>' +
                            '</div>' +
                            '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-key-type">' +
                                '(' +
                                '<@= activityParameterModel.get("type").label || activityParameterModel.get("typeName") @>' +
                                '<@ if (activityParameterModel.get("listFlag")) { @>' +
                                    mwa.enumeration.Message['TITLE_LIST'] +
                                '<@ } @>' +
                                ')' +
                            '</div>' +
                        '</div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-value">' +
                            '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-value-source">' +
                                '<div></div>' +
                            '</div>' +
                            '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-value-target">' +
                                '<div></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-delete-button"></div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-parameter-item-copy-button"></div>' +
                    '</div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.initialize}
             * @protected
             * @override
             */
            initialize: function(options, canRender) {
              this._timers = {};

              mwa.View.prototype.initialize.apply(this, [options, false]);
            },
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              // CAUTION:
              // It is necessary to prepare an empty object because this object will be updated in
              // applyDataWorkflowConnectionPortViewModel function.
              this.models = {};

              this.listenTo(
                  this.options.viewModel,
                  'change:hasPredefinedWorkflowStep',
                  this.applyHasPredefinedWorkflowStep
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:isReadOnly',
                  this.applyIsReadOnly
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canDelete',
                  this.applyCanDelete
              );

              this.listenToDataWorkflowConnectionPortViewModel(
                  this.options.viewModel,
                  this.options.viewModel.get('dataWorkflowConnectionPortViewModel'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:dataWorkflowConnectionPortViewModel',
                  this.listenToDataWorkflowConnectionPortViewModel
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:dataWorkflowConnectionPortViewModel',
                  this.applyDataWorkflowConnectionPortViewModel
              );

              this.applyDataWorkflowConnectionPortViewModel(
                  this.options.viewModel,
                  this.options.viewModel.get('dataWorkflowConnectionPortViewModel'),
                  null
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:dataWorkflowConnectionViewModel',
                  this.applyDataWorkflowConnectionViewModel
              );
            },
            /**
             * The event handler for changing dataWorkflowConnectionPortViewModel attribute of view
             * model. This function removes listeners from old dataWorkflowConnectionPortViewModel
             * and adds listeners to new dataWorkflowConnectionPortViewModel.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel} model
             * The mwa workflow parameter item view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToDataWorkflowConnectionPortViewModel: function(model, value, options) {
              var previousValue = model.previous('dataWorkflowConnectionPortViewModel');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
                this.stopListening(previousValue.get('workflowStepViewModel'));
                this.stopListening(previousValue.get('workflowStepViewModel').get('pipelineWorkflowStepViewModelCollection'));
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);
                this.stopListening(value.get('workflowStepViewModel'));
                this.stopListening(value.get('workflowStepViewModel').get('pipelineWorkflowStepViewModelCollection'));


                this.listenTo(
                    value,
                    'change:typeName',
                    this.applyDataWorkflowConnectionPortTypeName
                );

                this.listenTo(
                    value,
                    'change:color',
                    this.applyDataWorkflowConnectionPortColor
                );

                this.listenToWorkflowStepViewModel(
                    value,
                    value.get('workflowStepViewModel'),
                    options
                );
                this.listenTo(
                    value,
                    'change:workflowStepViewModel',
                    this.listenToWorkflowStepViewModel
                );
                this.listenTo(
                    value,
                    'destroy',
                    this.destroy
                );
              }
            },
            /**
             * The event handler for changing workflowStepViewModel attribute of workflow connection
             * port view model. This function removes listeners from old workflowStepViewModel and
             * adds listeners to new workflowStepViewModel.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToWorkflowStepViewModel: function(model, value, options) {
              var previousValue = model.previous('workflowStepViewModel');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
                this.stopListening(previousValue.get('pipelineWorkflowStepViewModelCollection'));
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);
                this.stopListening(value.get('pipelineWorkflowStepViewModelCollection'));

                this.listenToPipelineWorkflowStepViewModelCollection(
                    value,
                    value.get('pipelineWorkflowStepViewModelCollection'),
                    options
                );
                this.listenTo(
                    value,
                    'change:pipelineWorkflowStepViewModelCollection',
                    this.listenToPipelineWorkflowStepViewModelCollection
                );
                this.listenTo(
                    value,
                    'change:pipelineWorkflowStepViewModelCollection',
                    this.applyPipelineWorkflowStepViewModelCollection
                );
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * workflow step view model. This function removes listeners from old
             * pipelineWorkflowStepViewModelCollection and adds listeners to new
             * pipelineWorkflowStepViewModelCollection.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToPipelineWorkflowStepViewModelCollection: function(model, value, options) {
              var previousValue = model.previous('pipelineWorkflowStepViewModelCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'update', this.applyPipelineWorkflowStepViewModelCollection);
                this.listenTo(value, 'reset', this.applyPipelineWorkflowStepViewModelCollection);
              }
            },
            /**
             * @see {@link mwa.View.presenter}
             * @protected
             * @override
             */
            presenter: function(options) {
              return mwa.proui._.extend(
                  mwa.View.prototype.presenter.apply(this, [options]),
                  {
                    activityParameterModel: this.models.activityParameterModel
                  }
              );
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              that.remove({silent: true});
              // INFORMATION:
              // It is important to call remove function instead of empty function for creating this
              // view's element from template.
              that.$el.remove();
              that.setElement(mwa.proui.Backbone.$(that.template(that.presenter())));
              that.cacheElement();

              // Result Rendering
              // INFORMATION: NVX-7084 (https://www.tool.sony.biz/jira/browse/NVX-7084)
              // It is important to always set "data-result" attribute when a case to display
              // activity instance information even if the result doesn't exist in order to display
              // all workflow parameter item views by the same height.
              if (that.options.viewModel
                      .get('dataWorkflowConnectionPortViewModel')
                      .get('workflowStepViewModel')
                      .has('activityInstanceModel')) {
                var activityParameterType = that.models.activityParameterModel.get('type');
                // CAUTION:
                // It is necessary to set empty string instead of null not to remove data-result
                // attribute.
                var result = that.models.activityParameterModel.get('result') || '';

                that.$el.attr('data-result', activityParameterType.formatText(result));

                if (activityParameterType.isSetting &&
                    that.models.activityParameterModel.has('result')) {
                  var complete = function(model, response, options) {
                    that.$el
                        .attr('data-result', model.get('name'))
                        .toggleClass('proui-text-disabled', (response || {}).name == null);
                  };

                  // CAUTION: NVX-7282 (https://www.tool.sony.biz/jira/browse/NVX-7282)
                  // It is important to fetch model, not collection, in consideration of performance
                  // and memory usage.
                  that.createValueTargetSettingDropDownButtonInitialSelectedItemModel().set({
                    id: result
                  }).fetch({
                    // CAUTION:
                    // It is necessary to use "success" and "error" callbacks instead of "complete"
                    // callback in order to use "model" and "response" objects to update the
                    // information (the interface of "complete" callback is different from others
                    // and it is difficult to refer to the actual response).
                    success: complete,
                    error: complete
                  });
                }
              }

              that.views = {};

              that.renderKeyNameTooltip();

              that.applyIsReadOnly(
                  that.options.viewModel,
                  that.options.viewModel.get('isReadOnly'),
                  null
              );

              // CAUTION:
              // It is important to clear the timer before updating it in consideration of a case
              // that this processing is executed multiple times during the interval.
              clearInterval(that._timers.render);
              // CAUTION:
              // It is important to call renderDeleteCommandButton function after rendering view's
              // element because the background image is unknown before rendering.
              that._timers.render = setInterval(function() {
                if (that.$el.width() != null) {
                  clearInterval(that._timers.render);

                  // CAUTION:
                  // It is necessary to consider about a case that this processing is executed after
                  // removal.
                  if (that.views != null) {
                    that.renderDeleteCommandButton();
                    that.renderCopyCommandButton();
                  }

                  that.applyCanDelete(
                      that.options.viewModel,
                      that.options.viewModel.get('canDelete'),
                      null
                  );
                }
              }, 100);

              return this;
            },
            /**
             * The function to cache element for internal processing.
             * @protected
             */
            cacheElement: function() {
              this.$valueSource =
                  this.$el.find('.mwa-view-workflow-parameter-editor-workflow-parameter-item-value-source');
              this.$valueTarget =
                  this.$el.find('.mwa-view-workflow-parameter-editor-workflow-parameter-item-value-target');
            },
            /**
             * The function to render key name tooltip.
             * @public
             *
             * @returns {proui.tooltip.TooltipView}
             * The rendered tooltip view instance.
             */
            renderKeyNameTooltip: function() {
              var that = this;

              // Initialization
              if (that.views.keyNameTooltipView != null) {
                that.views.keyNameTooltipView.remove();
              }
              that.views.keyNameTooltipView = null;

              // Rendering
              this.views.keyNameTooltipView = new mwa.proui.tooltip.TooltipView({
                el: this.$el.find('.mwa-view-workflow-parameter-editor-workflow-parameter-item-key-name'),
                property: 'key',
                viewModel: this.models.activityParameterModel
              });

              return this.views.keyNameTooltipView;
            },
            /**
             * The function to render value source drop down button.
             * @protected
             *
             * @returns {!proui.dropDownButton.DropDownButtonView}
             * The rendered drop down button view instance.
             */
            renderValueSourceDropDownButton: function() {
              var that = this;

              // Initialization
              that.removeValueSourceControls();

              // Rendering
              var $el = that.$valueSource.children();
              that.views.valueSourceDropDownButtonView =
                  new mwa.proui.dropDownButton.DropDownButtonView({
                    el: $el,
                    subTemplate: mwa.proui._.template(
                        '<div class="proui-drop-down-list' +
                        (
                            (that.options.viewModel.get('hasPredefinedWorkflowStep')) ?
                                ' mwa-view-workflow-parameter-editor-workflow-parameter-item-value-source-drop-down-list' :
                                ''
                        ) +
                        '"></div>'
                    ),
                    viewModel: new mwa.proui.dropDownButton.DropDownButtonViewModel({
                      isStrong: true,
                      hasTooltip: true,
                      label: 'text',
                      width: '100%',
                      // INFORMATION:
                      // This setting will be updated in the following processing.
                      maxHeight: null,
                      // CAUTION:
                      // It is important not to set initial item because it will be added to
                      // itemCollection implicitly. In other words, the collection instance
                      // shouldn't be changed unnecessarily because the instance might have been
                      // shared with other views.
                      hasInitialItem: false,
                      itemCollection: that.models.valueSourceDropDownButtonItemCollection,
                      selectedItem: that.getValueSourceInitialWorkflowStepViewModel(),
                      select: function(selectedItemModel) {
                        switch (selectedItemModel.get('id')) {
                          case mwa.enumeration.WorkflowStep.IMMEDIATE_VALUE.id:
                            switch (that.models.activityParameterModel.get('type')) {
                              case mwa.enumeration.ActivityParameterType.BOOLEAN:
                                that.renderValueTargetBooleanDropDownButton();
                                break;

                              case mwa.enumeration.ActivityParameterType.URI:
                                that.renderValueTargetBrowseCommandButton();
                                break;

                              default:
                                that.renderValueTargetTextBox();
                                break;
                            }
                            break;

                          case mwa.enumeration.WorkflowStep.SETTING_VALUE.id:
                            that.renderValueTargetSettingDropDownButton();
                            break;

                          default:
                            that.renderValueTargetStepDropDownButton();
                            break;
                        }
                      }
                    })
                  });

              // CAUTION:
              // It is important to update max height setting after rendering view's element because
              // the style settings cannot be obtained before rendering.
              updateDropDownButtonMaxHeight(that, that.views.valueSourceDropDownButtonView);

              return that.views.valueSourceDropDownButtonView;
            },
            /**
             * The function to render value source text box.
             * @protected
             *
             * @returns {!proui.textBox.TextBoxView}
             * The rendered text box view instance.
             */
            renderValueSourceTextBox: function() {
              var that = this;

              // Initialization
              that.removeValueSourceControls();

              // Rendering
              var $el = that.$valueSource.children();
              that.views.valueSourceTextBoxView =
                  new mwa.proui.textBox.TextBoxView({
                    el: $el,
                    viewModel: new mwa.proui.textBox.TextBoxViewModel({
                      hasTooltip: false,
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      text: that.getValueSourceInitialWorkflowStepViewModel().get('text')
                    })
                  });

              return that.views.valueSourceTextBoxView;
            },
            /**
             * The function to render value target boolean drop down button.
             * @protected
             *
             * @returns {!proui.dropDownButton.DropDownButtonView}
             * The rendered drop down button view instance.
             */
            renderValueTargetBooleanDropDownButton: function() {
              var that = this;

              // Initialization
              that.removeValueTargetControls();

              // Rendering
              var $el = that.$valueTarget.children();
              var itemCollection =
                  new mwa.proui.Backbone.Collection([
                    {
                      id: null,
                      name: '--' + mwa.enumeration.Message['SENTENCE_SELECT'] + '--'
                    },
                    {
                      id: 'true',
                      name: mwa.enumeration.Message['SENTENCE_YES']
                    },
                    {
                      id: 'false',
                      name: mwa.enumeration.Message['SENTENCE_NO']
                    }
                  ]);
              that.views.valueTargetDropDownButtonView =
                  new mwa.proui.dropDownButton.DropDownButtonView({
                    el: $el,
                    viewModel: new mwa.proui.dropDownButton.DropDownButtonViewModel({
                      isStrong: true,
                      hasTooltip: true,
                      label: 'name',
                      width: '100%',
                      // INFORMATION:
                      // This setting will be updated in the following processing.
                      maxHeight: null,
                      hasInitialItem: false,
                      itemCollection: itemCollection,
                      selectedItem: itemCollection.findWhere({
                        id: that.models.activityParameterModel.get('type').decodeText(
                            that.models.activityParameterModel.get('value')
                        )
                      }),
                      select: function(selectedItemModel) {
                        that.models.activityParameterModel.set(
                            'value',
                            that.models.activityParameterModel.get('type').encodeText(
                                selectedItemModel.get('id')
                            )
                        );
                      }
                    })
                  });

              // CAUTION:
              // It is important to update max height setting after rendering view's element because
              // the style settings cannot be obtained before rendering.
              updateDropDownButtonMaxHeight(that, that.views.valueTargetDropDownButtonView);

              // CAUTION:
              // It is necessary to call select callback explicitly to update workflow parameter
              // control by default selected item because this callback wasn't called in the initial
              // rendering.
              that.views.valueTargetDropDownButtonView.options.viewModel.select(
                  that.views.valueTargetDropDownButtonView.options.viewModel.get('selectedItem')
              );

              return that.views.valueTargetDropDownButtonView;
            },
            /**
             * The function to render value target setting drop down button.
             * @protected
             *
             * @returns {!proui.dropDownButton.DropDownButtonView}
             * The rendered drop down button view instance.
             */
            renderValueTargetSettingDropDownButton: function() {
              var that = this;

              // Initialization
              that.removeValueTargetControls();

              // Rendering
              var $el = that.$valueTarget.children();
              var initialItemModel =
                  new that.models.valueTargetSettingDropDownButtonItemCollection.model(
                      {
                        // CAUTION:
                        // It isn't necessary to consider about a case that activity parameter value
                        // is null because selected item model won't be set to item collection in
                        // such case. In other words, initial item model always will be displayed
                        // even if activity parameter value is null.
                        id: null,
                        name: '--' + mwa.enumeration.Message['SENTENCE_SELECT'] + '--'
                      },
                      {validate: false}
                  );
              var selectedItemModel =
                  that.createValueTargetSettingDropDownButtonInitialSelectedItemModel();
              that.models.valueTargetSettingDropDownButtonItemCollection.reset(
                  (that.models.activityParameterModel.has('value')) ?
                      [selectedItemModel] :
                      []
              );
              that.views.valueTargetDropDownButtonView =
                  new mwa.proui.dropDownButton.DropDownButtonView({
                    el: $el,
                    viewModel: new mwa.proui.dropDownButton.DropDownButtonViewModel({
                      isStrong: true,
                      hasTooltip: true,
                      label: 'name',
                      width: '100%',
                      // INFORMATION:
                      // This setting will be updated in the following processing.
                      maxHeight: null,
                      hasInitialItem: true,
                      initialItem: initialItemModel,
                      itemCollection: that.models.valueTargetSettingDropDownButtonItemCollection,
                      selectedItem:
                          (that.models.activityParameterModel.has('value')) ?
                              selectedItemModel :
                              initialItemModel,
                      select: function(selectedItemModel) {
                        that.models.activityParameterModel.set(
                            'value',
                            that.models.activityParameterModel.get('type').encodeText(
                                selectedItemModel.id
                            )
                        );
                      },
                      open: function(selectedItemModel) {
                        that.models.valueTargetSettingDropDownButtonItemCollection.fetch();
                      },
                      syncCollection: function(itemCollection) {
                        this.set(
                            'selectedItem',
                            that.models.valueTargetSettingDropDownButtonItemCollection
                                .get(this.get('selectedItem').get('id')) ||
                            // CAUTION:
                            // It is important to set initial selected item instead of null if it
                            // wasn't found from actual data in order to keep the display text.
                            this.get('selectedItem')
                        );

                        if (this.get('selectedItem') instanceof mwa.proui.Backbone.Model) {
                          that.views.valueTargetDropDownButtonView.$el
                              .find('.proui-drop-down-button-label')
                              .removeClass('proui-text-disabled');
                        }
                      }
                    })
                  });

              // CAUTION:
              // It is important to update max height setting after rendering view's element because
              // the style settings cannot be obtained before rendering.
              updateDropDownButtonMaxHeight(that, that.views.valueTargetDropDownButtonView);

              // CAUTION:
              // It is necessary to call select callback explicitly to update workflow parameter
              // control by default selected item because this callback wasn't called in the initial
              // rendering.
              that.views.valueTargetDropDownButtonView.options.viewModel.select(
                  that.views.valueTargetDropDownButtonView.options.viewModel.get('selectedItem')
              );

              that.views.valueTargetDropDownButtonView.$el
                  .find('.proui-drop-down-button-label')
                  .addClass('proui-text-disabled');
              that.models.valueTargetSettingDropDownButtonItemCollection.fetch();

              return that.views.valueTargetDropDownButtonView;
            },
            /**
             * The function to render value target step drop down button.
             * @protected
             *
             * @returns {!proui.dropDownButton.DropDownButtonView}
             * The rendered drop down button view instance.
             */
            renderValueTargetStepDropDownButton: function() {
              var that = this;

              // Initialization
              that.removeValueTargetControls();

              // Rendering
              var $el = that.$valueTarget.children();
              // CAUTION:
              // It is important to prepare a new instance not to synchronize the selected item
              // between workflow parameter item views. And also, it is important to prepare new
              // instances with respect to model to be able to set the different value for
              // isDisabled attribute between workflow parameter item views. By the way, it isn't
              // necessary to consider about a case that activity parameter models will be added to
              // or removed from outputActivityParameterCollection (e.g. Start) because any workflow
              // parameter view will be re-rendered by re-selecting the workflow step in such case.
              var itemCollection =
                  new mwa.proui.Backbone.Collection(
                      that.views.valueSourceDropDownButtonView.options.viewModel
                          .get('selectedItem')
                          .get('outputActivityParameterCollection').toJSON().map(
                          // INFORMATION:
                          // The first argument is object type, not array. But we use plural name
                          // based on our JavaScript coding rule.
                          function(outputActivityParameters, index, array) {
                            var activityParameterTypeName =
                                that.models.activityParameterModel.get('typeName');

                            return mwa.proui._.extend(
                                // INFORMATION:
                                // It is better to pass an empty object to make a new instance
                                // certainly.
                                {},
                                outputActivityParameters,
                                {
                                  isDisabled: (
                                      // CAUTION: NVX-5190 (https://www.tool.sony.biz/jira/browse/NVX-5190?focusedCommentId=1988534&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-1988534)
                                      // It is necessary for activity condition item view to always
                                      // enable all activity parameters if the type of source
                                      // activity parameter model is TypeUnknown.
                                      activityParameterTypeName !==
                                      mwa.enumeration.ActivityParameterType.UNKNOWN.typeName &&
                                      (
                                          that.models.activityParameterModel.get('listFlag') !==
                                          outputActivityParameters.listFlag ||
                                          // CAUTION:
                                          // It is necessary to compare typeName attribute, not type
                                          // attribute, of activity parameter model in consideration
                                          // of that all non-predefined activity parameter types
                                          // were assigned to SETTING.
                                          activityParameterTypeName !==
                                          outputActivityParameters.typeName
                                      )
                                  )
                                }
                            );
                          }
                      )
                  );
              that.views.valueTargetDropDownButtonView =
                  new mwa.proui.dropDownButton.DropDownButtonView({
                    el: $el,
                    viewModel: new mwa.proui.dropDownButton.DropDownButtonViewModel({
                      isStrong: true,
                      hasTooltip: true,
                      label: 'key',
                      width: '100%',
                      // INFORMATION:
                      // This setting will be updated in the following processing.
                      maxHeight: null,
                      hasInitialItem: false,
                      itemCollection: itemCollection,
                      selectedItem:
                          // INFORMATION:
                          // It is better to use findWhere function instead of get function in
                          // consideration of that idAttribute of activity parameter model is "key",
                          // not "id".
                          itemCollection.findWhere(
                              that.createValueTargetOutputActivityParameterModelFinder()
                          ) ||
                          // CAUTION:
                          // It is important to consider about a case that there is no selectable
                          // activity parameter due to type mismatch.
                          itemCollection.findWhere({isDisabled: false}) ||
                          null,
                      select: function(selectedItemModel) {
                        // CAUTION:
                        // It is necessary to ignore explicit call in rendering processing in order
                        // not to make other views detecting unnecessary modification.
                        if (!that.options.viewModel.has('dataWorkflowConnectionViewModel') ||
                            that.options.viewModel.get('dataWorkflowConnectionViewModel')
                                .get('fromWorkflowConnectionPortViewModel')
                                .get('id') !== selectedItemModel.get('id')) {
                          that.options.viewModel.set(
                              'dataWorkflowConnectionViewModel',
                              (selectedItemModel instanceof mwa.proui.Backbone.Model) ?
                                  new mwa.view.workflowConnection.WorkflowConnectionViewModel({
                                    // INFORMATION:
                                    // The fromWorkflowConnectionPortViewModel instance will be
                                    // replaced with an another instance that is managed by workflow
                                    // canvas view.
                                    fromWorkflowConnectionPortViewModel:
                                        new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel({
                                          id: selectedItemModel.get('id'),
                                          key: selectedItemModel.get('key'),
                                          isRequired: selectedItemModel.get('required'),
                                          // INFORMATION:
                                          // To be careful about attribute name.
                                          positions: {
                                            x: 0,
                                            y: 0,
                                            z: 0
                                          },
                                          type: mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT,
                                          typeName: selectedItemModel.get('typeName'),
                                          color: mwa.proui._.findWhere(
                                              WORKFLOW_CONNECTION_PORT_COLOR_ARRAY,
                                              {
                                                isArray: selectedItemModel.get('listFlag'),
                                                activityParameterType: selectedItemModel.get('type')
                                              }
                                          ),
                                          workflowStepViewModel:
                                              that.views.valueSourceDropDownButtonView.options.viewModel
                                                  .get('selectedItem')
                                        }),
                                    toWorkflowConnectionPortViewModel:
                                        that.options.viewModel
                                            .get('dataWorkflowConnectionPortViewModel')
                                  }) :
                                  null
                          );
                        }
                      }
                    })
                  });

              // CAUTION:
              // It is important to update max height setting after rendering view's element because
              // the style settings cannot be obtained before rendering.
              updateDropDownButtonMaxHeight(that, that.views.valueTargetDropDownButtonView);

              // CAUTION:
              // It is necessary to call select callback explicitly to update workflow parameter
              // control by default selected item because this callback wasn't called in the initial
              // rendering.
              that.views.valueTargetDropDownButtonView.options.viewModel.select(
                  that.views.valueTargetDropDownButtonView.options.viewModel.get('selectedItem')
              );

              return that.views.valueTargetDropDownButtonView;
            },
            /**
             * The function to render value target browse command button.
             * @protected
             *
             * @returns {!proui.commandButton.CommandButtonView}
             * The rendered command button view instance.
             */
            renderValueTargetBrowseCommandButton: function() {
              var that = this;

              // Initialization
              that.removeValueTargetControls();

              // Rendering
              var $el =
                  that.$valueTarget
                      // CAUTION:
                      // It is important to always update data-uri attribute even if the value was
                      // set by template processing in consideration of that value attribute of
                      // activity parameter model will be initialized when re-rendering of target
                      // controls (in removeValueTargetControls function).
                      .attr(
                          'data-uri',
                          that.models.activityParameterModel.get('type').formatText(
                              that.models.activityParameterModel.get('type').decodeText(
                                  that.models.activityParameterModel.get('value')
                              ) ||
                              // CAUTION:
                              // It is necessary to set empty string instead of null not to remove
                              // data-uri attribute.
                              ''
                          )
                      )
                      .children();
              that.views.valueTargetCommandButtonView =
                  new mwa.proui.commandButton.CommandButtonView({
                    el: $el,
                    viewModel: new mwa.proui.commandButton.CommandButtonViewModel({
                      icon: mwa.EXTERNAL_IMAGE_BASE_PATH + 'proui.util.directory.svg',
                      tooltip: mwa.enumeration.Message['TITLE_BROWSE'],
                      type: mwa.proui.commandButton.ButtonType.NONE,
                      command: function() {
                        if (that.views != null) {
                          if (that.views.valueTargetDialogView == null) {
                            var itemCollection = new mwa.model.directory.DirectoryCollection();
                            // CAUTION:
                            // It is important to use file system entity collection for storing
                            // selected item models because directory model or file model will be
                            // stored.
                            var selectedItemCollection =
                                new mwa.model.fileSystemEntity.FileSystemEntityCollection();

                            that.views.valueTargetDialogView =
                                new mwa.proui.dialog.DialogView({
                                  viewModel: new mwa.proui.dialog.DialogViewModel({
                                    isOpen: true,
                                    canMinimize: false,
                                    // INFORMATION:
                                    // It is important to set false to autoDestroy property in order
                                    // to reuse the same view instance after closing.
                                    autoDestroy: false,
                                    title: mwa.enumeration.Message['TITLE_BROWSE'],
                                    width: 800,
                                    height: 600,
                                    content: new mwa.proui.dialog.DialogContentModel({
                                      viewClass: mwa.proui.listView.ListViewView,
                                      viewOptions: {
                                        itemTemplates: {
                                          name:
                                              '<div tooltip="<%- data.name %>">' +
                                                  '<%- data.name %>' +
                                              '</div>',
                                          nativePath:
                                              '<div tooltip="<%- data.nativePath %>">' +
                                                  '<%- data.nativePath %>' +
                                              '</div>'
                                        },
                                        viewModel: new mwa.proui.listView.ListViewViewModel({
                                          hasSelectBox: false,
                                          hasColumnSettingButton: false,
                                          canSelect: true,
                                          canMultiSelect: false,
                                          isAscending: true,
                                          key: itemCollection.comparator,
                                          listViewType: mwa.proui.listView.ListViewType.TREE,
                                          treeDefinition: new mwa.proui.listView.ListViewTreeDefinitionModel({
                                            rootType: 'DIRECTORY',
                                            definitions: {
                                              DIRECTORY: {
                                                modelClass: mwa.model.directory.DirectoryModel,
                                                children: {
                                                  DIRECTORY: 'itemGroups',
                                                  FILE: 'items'
                                                }
                                              },
                                              FILE: {
                                                modelClass: mwa.model.file.FileModel
                                              }
                                            }
                                          }),
                                          headerArray: [
                                            {
                                              key: 'name',
                                              label: mwa.enumeration.Message['SENTENCE_NAME'],
                                              width: 200,
                                              canShift: false,
                                              canSort: false,
                                              canFilter: false,
                                              canResize: true
                                            },
                                            {
                                              key: 'nativePath',
                                              label: mwa.enumeration.Message['SENTENCE_PATH'],
                                              width: 500,
                                              canShift: false,
                                              canSort: false,
                                              canFilter: false,
                                              canResize: true
                                            }
                                          ],
                                          itemCollection: itemCollection,
                                          selectedItems: selectedItemCollection
                                        })
                                      }
                                    }),
                                    commandButtonViewModelArray: [
                                      new mwa.proui.commandButton.CommandButtonViewModel({
                                        isDisabled: false,
                                        isStrong: true,
                                        text: mwa.enumeration.Message['TITLE_SELECT'],
                                        minWidth: 100,
                                        size: mwa.proui.commandButton.ButtonSize.SMALL,
                                        type: mwa.proui.commandButton.ButtonType.CTA,
                                        command: function(event) {
                                          var value =
                                              (selectedItemCollection.isEmpty()) ?
                                                  null :
                                                  selectedItemCollection.first().get('id');

                                          // CAUTION:
                                          // It is necessary to set empty string instead of null not
                                          // to remove data-uri attribute.
                                          that.$valueTarget.attr(
                                              'data-uri',
                                              that.models.activityParameterModel
                                                  .get('type')
                                                  .formatText(value || '')
                                          );
                                          that.models.activityParameterModel.set(
                                              'value',
                                              that.models.activityParameterModel
                                                  .get('type')
                                                  .encodeText(value)
                                          );

                                          that.views.valueTargetDialogView.options.viewModel
                                              .set('isOpen', false);
                                        }
                                      }),
                                      new mwa.proui.commandButton.CommandButtonViewModel({
                                        isDisabled: false,
                                        isStrong: true,
                                        text: mwa.enumeration.Message['TITLE_CANCEL'],
                                        minWidth: 100,
                                        size: mwa.proui.commandButton.ButtonSize.SMALL,
                                        type: mwa.proui.commandButton.ButtonType.ALT,
                                        command: function(event) {
                                          that.views.valueTargetDialogView.options.viewModel
                                              .set('isOpen', false);
                                        }
                                      })
                                    ],
                                    create: function(contentView, viewModel) {
                                      contentView.options.viewModel.fetch();
                                    },
                                    close: function(contentView, viewModel) {
                                      selectedItemCollection.reset();
                                    }
                                  })
                                });
                          } else {
                            that.views.valueTargetDialogView.options.viewModel.set('isOpen', true);
                          }
                        }
                      }
                    })
                  });

              that.views.valueTargetTooltipView =
                  new mwa.proui.tooltip.TooltipView({
                    el: that.$valueTarget,
                    viewModel: new mwa.proui.Backbone.Model({
                      tooltip: function(viewModel) {
                        var formattedValue =
                            that.models.activityParameterModel.get('type').formatText(
                                that.models.activityParameterModel.get('type').decodeText(
                                    that.models.activityParameterModel.get('value')
                                )
                            );

                        return (that.models.activityParameterModel.get('required')) ?
                            formattedValue ||
                            mwa.enumeration.Message['VALIDATION_REQUIRED'].replace(
                                /\{0\}/,
                                mwa.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                            ) :
                            formattedValue;
                      }
                    })
                  });

              return that.views.valueTargetCommandButtonView;
            },
            /**
             * The function to render value target text box.
             * @protected
             *
             * @returns {!proui.textBox.TextBoxView}
             * The rendered text box view instance.
             */
            renderValueTargetTextBox: function() {
              var that = this;

              // Initialization
              that.removeValueTargetControls();

              // Rendering
              var $el = that.$valueTarget.children();
              that.views.valueTargetTextBoxView =
                  new mwa.proui.textBox.TextBoxView({
                    el: $el,
                    viewModel: new mwa.proui.textBox.TextBoxViewModel({
                      hasTooltip: false,
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      text:
                          that.models.activityParameterModel.get('type').formatText(
                              that.models.activityParameterModel.get('type').decodeText(
                                  that.models.activityParameterModel.get('value')
                              )
                          ) ||
                          (
                              that.getValueSourceInitialWorkflowStepViewModel()
                                  .get('outputActivityParameterCollection')
                                  // INFORMATION:
                                  // It is better to use findWhere function instead of get function
                                  // in consideration of that idAttribute of activity parameter
                                  // model is "key", not "id".
                                  .findWhere(
                                      that.createValueTargetOutputActivityParameterModelFinder()
                                  ) ||
                              new mwa.proui.Backbone.Model({key: null})
                          ).get('key'),
                      validateText: function(text) {
                        var validationTextError =
                            (isEmpty(text)) ?
                                (
                                    (that.models.activityParameterModel.get('required')) ?
                                        mwa.enumeration.Message['VALIDATION_REQUIRED'].replace(
                                            /\{0\}/,
                                            mwa.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                                        ) :
                                        null
                                ) :
                                (
                                    (that.options.viewModel.get('isReadOnly')) ?
                                        null :
                                        that.models.activityParameterModel
                                            .get('type').validateText(text)
                                );

                        this.set('hasTooltip', typeof validationTextError === 'string');

                        return validationTextError;
                      },
                      changeText: function(text) {
                        // INFORMATION:
                        // The changeText callback is called in the initial rendering but there is
                        // no problem to execute the following processing because value attribute of
                        // activity parameter model will be updated the same value in this case.

                        // CAUTION:
                        // It is important not to set value to activity parameter model in read only
                        // case because there is a case that text is workflow parameter name just
                        // for displaying the information.
                        if (!that.options.viewModel.get('isReadOnly')) {
                          that.models.activityParameterModel.set(
                              'value',
                              that.models.activityParameterModel.get('type').encodeText(text)
                          );
                        }
                      }
                    })
                  });

              if (that.models.activityParameterModel.get('type').isSetting &&
                  that.models.activityParameterModel.has('value')) {
                var complete = function(model, response, options) {
                  that.views.valueTargetTextBoxView.options.viewModel.set({
                    isDisabled: (response || {}).name == null,
                    text: model.get('name')
                  });
                };

                // CAUTION: NVX-7282 (https://www.tool.sony.biz/jira/browse/NVX-7282)
                // It is important to fetch model, not collection, in consideration of performance
                // and memory usage.
                that.createValueTargetSettingDropDownButtonInitialSelectedItemModel().fetch({
                  // CAUTION:
                  // It is necessary to use "success" and "error" callbacks instead of "complete"
                  // callback in order to use "model" and "response" objects to update the
                  // information (the interface of "complete" callback is different from others and
                  // it is difficult to refer to the actual response).
                  success: complete,
                  error: complete
                });
              }

              return that.views.valueTargetTextBoxView;
            },
            /**
             * The function to render delete command button.
             * @public
             *
             * @returns {proui.commandButton.CommandButtonView}
             * The rendered command button view instance.
             */
            renderDeleteCommandButton: function() {
              var that = this;

              // Initialization
              if (that.views.deleteCommandButtonView != null) {
                that.views.deleteCommandButtonView.remove();
              }
              that.views.deleteCommandButtonView = null;

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-parameter-editor-workflow-parameter-item-delete-button');
              that.views.deleteCommandButtonView =
                  new mwa.proui.commandButton.CommandButtonView({
                    el: $el,
                    viewModel: new mwa.proui.commandButton.CommandButtonViewModel({
                      icon: $el.css('background-image'),
                      type: mwa.proui.commandButton.ButtonType.NONE,
                      size: mwa.proui.commandButton.ButtonSize.SMALL,
                      tooltip: mwa.enumeration.Message['TITLE_DELETE'],
                      command: function() {
                        // CAUTION:
                        // It is important not to destroy the data workflow connection port view
                        // model in remove function because it should be destroyed only in this
                        // case in consideration of re-rendering.
                        that.options.viewModel.get('dataWorkflowConnectionPortViewModel').destroy();

                        // INFORMATION:
                        // It isn't necessary to remove the view itself explicitly here because
                        // remove function will be called in response to destroy event of
                        // dataWorkflowConnectionPortViewModel attribute of view model.
                      }
                    })
                  });
              $el.css('background-image', 'none');

              return that.views.deleteCommandButtonView;
            },
            /**
             * The function to render copy command button.
             * @public
             *
             * @returns {proui.commandButton.CommandButtonView}
             * The rendered command button view instance.
             */
            renderCopyCommandButton: function() {
              var that = this;

              // Initialization
              if (that.views.copyCommandButtonView != null) {
                that.views.copyCommandButtonView.remove();
              }
              that.views.copyCommandButtonView = null;

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-parameter-editor-workflow-parameter-item-copy-button');
              that.views.copyCommandButtonView =
                  new mwa.proui.commandButton.CommandButtonView({
                    el: $el,
                    viewModel: new mwa.proui.commandButton.CommandButtonViewModel({
                      icon: $el.css('background-image'),
                      type: mwa.proui.commandButton.ButtonType.NONE,
                      size: mwa.proui.commandButton.ButtonSize.SMALL,
                      // CAUTION: NVX-7084 (https://www.tool.sony.biz/jira/browse/NVX-7084)
                      // It is important to define tooltip by function in consideration of that
                      // data-result attribute will be updated asynchronously after this rendering.
                      tooltip: function(viewModel) {
                        return (
                            mwa.enumeration.Message['TITLE_COPY'] + ' : ' + '\n' +
                            that.$el.attr('data-result')
                        );
                      },
                      command: function() {
                        mwa.proui.util.String.copyString(that.$el.attr('data-result'));
                      }
                    })
                  });
              $el.css('background-image', 'none');

              return that.views.copyCommandButtonView;
            },
            /**
             * @see {@link mwa.View.remove}
             * @public
             * @override
             */
            remove: function(options) {
              if (typeof options === 'undefined' || options == null || !options.silent) {
                // CAUTION:
                // It is important to clear the all timers when remove in order not to execute the
                // unnecessary processing after removal.
                for (var propertyName in this._timers) {
                  if (this._timers.hasOwnProperty(propertyName)) {
                    clearInterval(this._timers[propertyName]);
                  }
                }
              }

              return mwa.View.prototype.remove.apply(this, [options]);
            },
            /**
             * The function to remove value source controls and initialize DOM structure.
             * @protected
             */
            removeValueSourceControls: function() {
              var that = this;

              for (var propertyName in that.views) {
                if (that.views.hasOwnProperty(propertyName) && /^valueSource/.test(propertyName)) {
                  if (that.views[propertyName] != null) {
                    that.views[propertyName].remove();
                    // CAUTION: NVX-6903 (https://www.tool.sony.biz/jira/browse/NVX-6903)
                    // It is important to consider about that there are multiple controls not to
                    // append unnecessary elements additionally.
                    if (that.$valueSource.children().length === 0) {
                      mwa.proui.Backbone.$('<div></div>').appendTo(that.$valueSource);
                    }
                  }
                  that.views[propertyName] = null;
                }
              }
            },
            /**
             * The function to remove value target controls and initialize DOM structure.
             * @protected
             */
            removeValueTargetControls: function() {
              var that = this;

              for (var propertyName in that.views) {
                if (that.views.hasOwnProperty(propertyName) && /^valueTarget/.test(propertyName)) {
                  if (that.views[propertyName] != null) {
                    that.views[propertyName].remove();
                    // CAUTION: NVX-6903 (https://www.tool.sony.biz/jira/browse/NVX-6903)
                    // It is important to consider about that there are multiple controls not to
                    // append unnecessary elements additionally.
                    if (that.$valueTarget.children().length === 0) {
                      mwa.proui.Backbone.$('<div></div>').appendTo(that.$valueTarget);
                    }
                    // CAUTION: NVX-6903 (https://www.tool.sony.biz/jira/browse/NVX-6903)
                    // It is important to remove all attributes not to display unnecessary pseudo
                    // elements.
                    //  - jQuery function to remove all "data-" attributes from a given element
                    //    (https://gist.github.com/salcode/6912619)
                    var attributes = that.$valueTarget.get(0).attributes; // NamedNodeMap, not Array
                    for (var i = 0; i < attributes.length; i++) {
                      var attributeName = attributes[i].name;
                      if (/^data-/.test(attributeName)) {
                        that.$valueTarget.removeAttr(attributeName);
                      }
                    }

                    // CAUTION:
                    // It is important to initialize dataWorkflowConnectionViewModel attribute of
                    // view model in order to always display selectable first item when changing the
                    // selected item of value source of drop down button because value target step
                    // drop down button will be rendered based on this value.
                    that.options.viewModel.set('dataWorkflowConnectionViewModel', null);
                    // CAUTION:
                    // It is important to initialize value attribute of activity parameter model
                    // in order to always display null value or first item when changing the
                    // selected item of value source of drop down button because value target text
                    // box and value target setting drop down button will be rendered based on this
                    // value.
                    that.models.activityParameterModel.set('value', null);
                  }
                  that.views[propertyName] = null;
                }
              }
            },
            /**
             * The function to create value source drop down button item collection.
             * @public
             *
             * @returns {mwa.view.workflowStep.WorkflowStepViewModelCollection}
             * The created value source drop down button item collection instance.
             */
            createValueSourceDropDownButtonItemCollection: function() {
              return new mwa.view.workflowStep.WorkflowStepViewModelCollection(
                  [].concat(
                      (
                          (this.options.viewModel.get('hasPredefinedWorkflowStep')) ?
                              findPredefinedWorkflowStepViewModel(
                                  this.models.activityParameterModel
                              ) :
                              []
                      ),
                      this.options.viewModel
                          .get('dataWorkflowConnectionPortViewModel')
                          .get('workflowStepViewModel')
                          // CAUTION:
                          // It is important to clone the instance not to affect the original
                          // instance by sort.
                          .get('pipelineWorkflowStepViewModelCollection').clone()
                          .sort()
                          .models
                          .filter(filterPipelineWorkflowStepViewModelCollection)
                  ),
                  // CAUTION:
                  // It is necessary to set sort option to disable extra sort including predefined
                  // workflow steps.
                  {sort: false}
              );
            },
            /**
             * The function to create value target setting drop down button item collection.
             * @public
             *
             * @returns {mwa.view.workflowStep.WorkflowStepViewModelCollection}
             * The created value target setting drop down button item collection instance.
             */
            createValueTargetSettingDropDownButtonItemCollection: function() {
              return (
                  (
                      mwa.enumeration.ActivityParameterSettingType[
                          this.models.activityParameterModel.get('type').name
                      ] ||
                      mwa.enumeration.ActivityParameterSettingType.NONE
                  ).createItemCollection(
                      this.options.viewModel
                          .get('dataWorkflowConnectionPortViewModel')
                          .get('workflowStepViewModel')
                          .get('activityModel'),
                      this.models.activityParameterModel
                  )
              );
            },
            /**
             * The function to create value target setting drop down button initial selected item
             * model.
             * @public
             *
             * @returns {Backbone.Model}
             * The created value target setting drop down button initial selected item model
             * instance.
             */
            createValueTargetSettingDropDownButtonInitialSelectedItemModel: function() {
              return new this.models.valueTargetSettingDropDownButtonItemCollection.model(
                  {
                    id: this.models.activityParameterModel.get('type').decodeText(
                        this.models.activityParameterModel.get('value')
                    ),
                    // INFORMATION:
                    // This text will be displayed if the setting value has being deleted.
                    name: mwa.enumeration.Message['SENTENCE_NONEXISTENT_VALUE']
                  },
                  mwa.proui._.extend(
                      // CAUTION:
                      // It is necessary to disable validation in consideration of a case that
                      // activity parameter value is null. In other words, ID is null.
                      {validate: false},
                      (
                          this.models.activityParameterModel.get('type') ===
                          mwa.enumeration.ActivityParameterType.SETTING
                      ) ?
                          {typeName: this.models.activityParameterModel.get('typeName')} :
                          null
                  )
              );
            },
            /**
             * The function to create value target output activity parameter model finder.
             * @public
             *
             * @returns {!object}
             * The created value target output activity parameter model finder.
             */
            createValueTargetOutputActivityParameterModelFinder: function() {
              return {
                id: (this.options.viewModel.has('dataWorkflowConnectionViewModel')) ?
                    this.options.viewModel
                        .get('dataWorkflowConnectionViewModel')
                        .get('fromWorkflowConnectionPortViewModel')
                        .get('id') :
                    null
              };
            },
            /**
             * The function to get value source initial workflow step view model.
             * @public
             *
             * @returns {!mwa.view.workflowStep.WorkflowStepViewModel}
             * The value source initial workflow step view model.
             */
            getValueSourceInitialWorkflowStepViewModel: function() {
              return (
                  this.models.valueSourceDropDownButtonItemCollection.get(
                      (this.models.activityParameterModel.has('value')) ?
                          mwa.enumeration.WorkflowStep.IMMEDIATE_VALUE.id :
                          (
                              (this.options.viewModel.has('dataWorkflowConnectionViewModel')) ?
                                  this.options.viewModel
                                      .get('dataWorkflowConnectionViewModel')
                                      .get('fromWorkflowConnectionPortViewModel')
                                      .get('workflowStepViewModel')
                                      .get('id') :
                                  null
                          )
                  ) ||
                  this.models.valueSourceDropDownButtonItemCollection.first()
              );
            },
            /**
             * The event handler for changing hasPredefinedWorkflowStep attribute of view model.
             * This function updates valueSourceDropDownButtonItemCollection according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel} model
             * The mwa workflow parameter item view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyHasPredefinedWorkflowStep: function(model, value, options) {
              var hasPredefinedWorkflowStep =
                  this.options.viewModel.get('hasPredefinedWorkflowStep');

              if (hasPredefinedWorkflowStep) {
                this.models.valueSourceDropDownButtonItemCollection.add(
                    findPredefinedWorkflowStepViewModel(this.models.activityParameterModel),
                    {index: 0}
                );
              } else {
                this.models.valueSourceDropDownButtonItemCollection.remove(
                    findPredefinedWorkflowStepViewModel(this.models.activityParameterModel)
                );
              }

              // CAUTION:
              // It is necessary to re-render value source drop down button to show or hide the
              // separator for separating predefined workflow steps and pipeline workflow steps.
              this.renderValueSourceDropDownButton();
            },
            /**
             * The event handler for changing isReadOnly attribute of view model. This function
             * renders value source and target according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel} model
             * The mwa workflow parameter item view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsReadOnly: function(model, value, options) {
              var isReadOnly = this.options.viewModel.get('isReadOnly');

              this.$el.toggleClass(
                  'mwa-view-workflow-parameter-editor-state-read-only',
                  isReadOnly
              );

              if (isReadOnly) {
                this.renderValueSourceTextBox();
                this.renderValueTargetTextBox();
              } else {
                this.renderValueSourceDropDownButton();
                // CAUTION:
                // It is necessary to call select callback explicitly to render target control
                // because this callback wasn't called in the initial rendering.
                this.views.valueSourceDropDownButtonView.options.viewModel.select(
                    this.views.valueSourceDropDownButtonView.options.viewModel.get('selectedItem')
                );
              }
            },
            /**
             * The event handler for changing canDelete attribute of view model. This function shows
             * or hides delete button according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel} model
             * The mwa workflow parameter item view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDelete: function(model, value, options) {
              var workflowStepViewModel =
                  this.options.viewModel
                      .get('dataWorkflowConnectionPortViewModel')
                      .get('workflowStepViewModel');
              // INFORMATION:
              // It is possible to use get function instead of findWhere function in consideration
              // of that idAttribute of activity parameter model is "key", not "id", because these
              // variables will be used for checking that the same key exists in both input and
              // output activity parameter collection but it is better to use findWhere function to
              // make the code maintainable and readable.
              var inputActivityParameterModel =
                  workflowStepViewModel.get('inputActivityParameterCollection')
                      .findWhere({
                        key: this.models.activityParameterModel.get('key'),
                        // CAUTION: NVX-6154 (https://www.tool.sony.biz/jira/browse/NVX-6154)
                        // It is important to consider about the visibility of activity parameter to
                        // make only created activity parameters deletable.
                        visibleFlag: true
                      });
              var outputActivityParameterModel =
                  workflowStepViewModel.get('outputActivityParameterCollection')
                      .findWhere({
                        key: this.models.activityParameterModel.get('key'),
                        // CAUTION: NVX-6154 (https://www.tool.sony.biz/jira/browse/NVX-6154)
                        // It is important to consider about the visibility of activity parameter to
                        // make only created activity parameters deletable.
                        visibleFlag: true
                      });

              var canDelete =
                  this.options.viewModel.get('canDelete') &&
                  inputActivityParameterModel instanceof
                  mwa.model.activityParameter.ActivityParameterModel &&
                  outputActivityParameterModel instanceof
                  mwa.model.activityParameter.ActivityParameterModel;

              if (this.views != null && this.views.deleteCommandButtonView != null) {
                this.views.deleteCommandButtonView.$el.toggle(canDelete);
              }
            },
            /**
             * The event handler for changing dataWorkflowConnectionPortViewModel attribute of view
             * model. This function updates internal managed activity parameter model according to
             * the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel} model
             * The mwa workflow parameter item view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             * @param {object} [options.$el = null]
             * The jQuery object to specify target element for re-rendering.
             */
            applyDataWorkflowConnectionPortViewModel: function(model, value, options) {
              // INFORMATION:
              // This function is been reused by applyDataWorkflowConnectionPortTypeName and
              // applyDataWorkflowConnectionPortColor functions of view model.

              options = mwa.proui._.extend({$el: null}, options);

              this.models =
                  mwa.proui._.extend(
                      this.models,
                      {
                        activityParameterModel:
                            this.options.viewModel
                                .get('dataWorkflowConnectionPortViewModel')
                                .getActivityParameterModel()
                      }
                  );
              this.models =
                  mwa.proui._.extend(
                      this.models,
                      {
                        // CAUTION:
                        // It is important to prepare a new instance not to synchronize the selected
                        // item between workflow parameter item views. However, it is important to
                        // use the same instances with respect to model to easy to update workflow
                        // step name when rename.
                        valueSourceDropDownButtonItemCollection:
                            this.createValueSourceDropDownButtonItemCollection(),
                        valueTargetSettingDropDownButtonItemCollection:
                            this.createValueTargetSettingDropDownButtonItemCollection()
                      }
                  );

              if (options.$el != null) {
                // CAUTION:
                // It is important to initialize dataWorkflowConnectionViewModel attribute of view
                // model in order to always display selectable first item when changing data
                // workflow connection port view model because value target step drop down button
                // will be rendered based on this value.
                this.options.viewModel.set('dataWorkflowConnectionViewModel', null);
                // CAUTION:
                // It is important to initialize value attribute of activity parameter model in
                // order to always display null value or first item when changing data workflow
                // connection port view model because value target text box and value target setting
                // drop down button will be rendered based on this value.
                this.models.activityParameterModel.set('value', null);

                options.$el.append(this.render().$el);
              }
            },
            /**
             * The event handler for changing typeName attribute of data workflow connection port
             * view model. This function updates value controls according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {string} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyDataWorkflowConnectionPortTypeName: function(model, value, options) {
              this.applyDataWorkflowConnectionPortViewModel(
                  this.options.viewModel,
                  this.options.viewModel.get('dataWorkflowConnectionPortViewModel'),
                  options
              );
            },
            /**
             * The event handler for changing color attribute of data workflow connection port view
             * model. This function updates value controls according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The mwa workflow connection port view model.
             * @param {mwa.enumeration.WorkflowConnectionPortColor} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyDataWorkflowConnectionPortColor: function(model, value, options) {
              this.applyDataWorkflowConnectionPortViewModel(
                  this.options.viewModel,
                  this.options.viewModel.get('dataWorkflowConnectionPortViewModel'),
                  options
              );
            },
            /**
             * The event handler for changing dataWorkflowConnectionViewModel attribute of view
             * model. This function destroys previous dataWorkflowConnectionViewModel and calls
             * connect callback of dataWorkflowConnectionPortViewModel attribute of view model
             * according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel} model
             * The mwa workflow parameter item view model.
             * @param {mwa.view.workflowConnection.WorkflowConnectionViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyDataWorkflowConnectionViewModel: function(model, value, options) {
              var previousDataWorkflowConnectionViewModel =
                  this.options.viewModel.previous('dataWorkflowConnectionViewModel');
              if (previousDataWorkflowConnectionViewModel instanceof
                  mwa.view.workflowConnection.WorkflowConnectionViewModel) {
                previousDataWorkflowConnectionViewModel.destroy(options);
              }

              if (this.options.viewModel.has('dataWorkflowConnectionViewModel')) {
                this.options.viewModel.get('dataWorkflowConnectionPortViewModel').connect(
                    new mwa.view.workflowConnection.WorkflowConnectionView({
                      viewModel: this.options.viewModel.get('dataWorkflowConnectionViewModel')
                    })
                );
              }
            },
            /**
             * The event handler for changing pipelineWorkflowStepViewModelCollection attribute of
             * workflow step view model and changing, adding, removing, updating and resetting
             * models in pipelineWorkflowStepViewModelCollection. This function updates
             * itemCollection attribute of value source drop down button view model according to the
             * value.
             * @protected
             */
            applyPipelineWorkflowStepViewModelCollection: function() {
              // arguments:
              // WorkflowStepViewModel
              //  - "change:pipelineWorkflowStepViewModelCollection" (model, value, options)
              // pipelineWorkflowStepViewModelCollection
              //  - "change"                                         (model, options)
              //  - "change:[attribute]"                             (model, value, options)
              //  - "add", "remove"                                  (model, collection, options)
              //  - "update", "reset"                                (collection, options)
              var that = this;
              var args = {};

              var workflowStepViewModel =
                  that.options.viewModel
                      .get('dataWorkflowConnectionPortViewModel')
                      .get('workflowStepViewModel');
              var pipelineWorkflowStepViewModelCollection =
                  workflowStepViewModel
                      .get('pipelineWorkflowStepViewModelCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === workflowStepViewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                   * The mwa workflow step view model.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // To do same processing as update event.
                  that.applyPipelineWorkflowStepViewModelCollection(
                      pipelineWorkflowStepViewModelCollection,
                      args.options
                  );
                } else if (arguments[0] instanceof mwa.view.workflowStep.WorkflowStepViewModel) {
                  if (arguments[1] !== pipelineWorkflowStepViewModelCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // INFORMATION:
                    // It isn't necessary to update value source drop down button when workflow step
                    // name is changed because the change event will be occurred on the model and
                    // drop down button will be updated by itself.

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.view.workflowStep.WorkflowStepViewModel} model
                     * The added or removed model.
                     * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === pipelineWorkflowStepViewModelCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  if (that.views != null && that.views.valueSourceDropDownButtonView) {
                    // INFORMATION:
                    // The following processing means to sort the order of actual workflow steps and
                    // always display predefined workflow steps at the top.

                    that.models.valueSourceDropDownButtonItemCollection.remove(
                        findPredefinedWorkflowStepViewModel(that.models.activityParameterModel),
                        // INFORMATION:
                        // It is better to set silent option in consideration of performance because
                        // the collection will be update again soon after this processing by reset
                        // operation.
                        {silent: true}
                    );
                    // INFORMATION:
                    // It is better to use reset function instead of set function to update the
                    // collection certainly.
                    that.models.valueSourceDropDownButtonItemCollection.reset(
                        [].concat(
                            (
                                (that.options.viewModel.get('hasPredefinedWorkflowStep')) ?
                                    findPredefinedWorkflowStepViewModel(
                                        that.models.activityParameterModel
                                    ) :
                                    []
                            )
                        ).concat(
                            args.collection
                                .sort().models.filter(filterPipelineWorkflowStepViewModelCollection)
                        ),
                        // CAUTION:
                        // It is necessary to set sort option to disable extra sort including
                        // predefined workflow steps.
                        {sort: false}
                    );
                  }
                }
              }
            }
          }
      );
    })();

    /**
     * The mwa workflow condition view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowConditionViewModel
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.ViewModel}
     */
    global.WorkflowConditionViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowConditionViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    isReadOnly: false,
                    canDelete: false,
                    activityConditionCollection:
                        new mwa.model.activityCondition.ActivityConditionCollection(),
                    conditionDataWorkflowConnectionPortViewModel: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  isReadOnly: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDelete: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  activityConditionCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityCondition.ActivityConditionCollection
                    }
                  ],
                  conditionDataWorkflowConnectionPortViewModel: [
                    {
                      // CAUTION:
                      // It is necessary to consider about a case that the value is null (e.g. when
                      // selecting not conditional branch step).
                      required: false
                    },
                    {
                      instance: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        var conditionDataWorkflowConnectionPortViewModel =
                            this.get('conditionDataWorkflowConnectionPortViewModel');
                        // CAUTION:
                        // It is necessary to add null check because this function will be called in
                        // such case.
                        if (conditionDataWorkflowConnectionPortViewModel instanceof
                            mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel) {
                          if (conditionDataWorkflowConnectionPortViewModel.get('key') !==
                              mwa.enumeration.ActivityParameter.CONDITION.key) {
                            msg =
                                'The "key" of "' + attr + '" must be ' +
                                'mwa.enumeration.ActivityParameter.CONDITION.key';
                          } else if (conditionDataWorkflowConnectionPortViewModel.get('type') !==
                              mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT) {
                            msg =
                                'The "type" of "' + attr + '" must be ' +
                                'mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT';
                          }
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // CAUTION:
              // It is necessary to prepare an empty object because this object will be updated in
              // applyConditionDataWorkflowConnectionPortViewModel function.
              this.models = {};

              this.listenToActivityConditionCollection(
                  this,
                  this.get('activityConditionCollection'),
                  null
              );
              this.listenTo(
                  this,
                  'change:activityConditionCollection',
                  this.listenToActivityConditionCollection
              );
              this.listenTo(
                  this,
                  'change:activityConditionCollection',
                  this.applyActivityConditionCollection
              );

              this.listenTo(
                  this,
                  'change:conditionDataWorkflowConnectionPortViewModel',
                  this.applyConditionDataWorkflowConnectionPortViewModel
              );

              this.applyConditionDataWorkflowConnectionPortViewModel(
                  this,
                  this.get('conditionDataWorkflowConnectionPortViewModel'),
                  null
              );
            },
            /**
             * The event handler for changing activityConditionCollection attribute of view model.
             * This function removes listeners from old activityConditionCollection and adds
             * listeners to new activityConditionCollection.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
             * The mwa workflow condition view model.
             * @param {mwa.model.activityCondition.ActivityConditionCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToActivityConditionCollection: function(model, value, options) {
              var previousValue = model.previous('activityConditionCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(
                    value,
                    'change',
                    this.applyActivityConditionCollection
                );
                this.listenTo(
                    value,
                    'update',
                    this.applyActivityConditionCollection
                );
                this.listenTo(
                    value,
                    'reset',
                    this.applyActivityConditionCollection
                );
              }
            },
            /**
             * The event handler for changing activityConditionCollection attribute of view model
             * and changing, adding, removing, updating and resetting models in
             * activityConditionCollection. This function updates the value of internal managed
             * activity parameter model according to the value.
             * @protected
             */
            applyActivityConditionCollection: function() {
              // arguments:
              // WorkflowConditionViewModel
              //  - "change:activityConditionCollection" (model, value, options)
              // activityConditionCollection
              //  - "change"                             (model, options)
              //  - "add", "remove"                      (model, collection, options)
              //  - "update", "reset"                    (collection, options)
              var that = this;
              var args = {};

              var activityConditionCollection = that.get('activityConditionCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
                   * The mwa workflow condition view model.
                   * @property {mwa.model.activityCondition.ActivityConditionModel} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // To do same processing as update event.
                  that.applyActivityConditionCollection(
                      activityConditionCollection,
                      args.options
                  );
                } else if (arguments[0] instanceof mwa.model.activityCondition.ActivityConditionModel) {
                  if (arguments[1] !== activityConditionCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.model.activityCondition.ActivityConditionModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // To do same processing as update event.
                    that.applyActivityConditionCollection(
                        activityConditionCollection,
                        args.options
                    );
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.model.activityCondition.ActivityConditionModel} model
                     * The added or removed model.
                     * @property {mwa.model.activityCondition.ActivityConditionCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      // If you need any processing, add here.
                    } else {
                      // Remove Case

                      // If you need any processing, add here.
                    }
                  }
                } else if (arguments[0] === activityConditionCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.model.activityCondition.ActivityConditionCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    // If you need any processing, add here.
                  }

                  if (that.models != null && that.models.activityParameterModel != null) {
                    that.models.activityParameterModel.set(
                        'value',
                        that.models.activityParameterModel.get('type').encodeText(
                            JSON.stringify(activityConditionCollection.toObject())
                        )
                    );
                  }
                }
              }
            },
            /**
             * The event handler for changing conditionDataWorkflowConnectionPortViewModel attribute
             * of view model. This function updates internal managed activity parameter model
             * according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
             * The mwa workflow condition view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyConditionDataWorkflowConnectionPortViewModel: function(model, value, options) {
              var conditionDataWorkflowConnectionPortViewModel =
                  this.get('conditionDataWorkflowConnectionPortViewModel');

              this.models =
                  mwa.proui._.extend(
                      this.models,
                      {
                        activityParameterModel:
                            // CAUTION:
                            // It is necessary to consider about a case that
                            // conditionDataWorkflowConnectionPortViewModel attribute of view model
                            // is undefined because it isn't required. And it is important to update
                            // internal managed activity parameter model by null value (null or
                            // undefined) in such case in order not to update the value of activity
                            // parameter model that isn't corresponding to the specified workflow
                            // connection port view model.
                            (
                                conditionDataWorkflowConnectionPortViewModel instanceof
                                mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
                            ) ?
                                conditionDataWorkflowConnectionPortViewModel
                                    .getActivityParameterModel() :
                                null
                            // CAUTION:
                            // It is important not to update the value of activity parameter model
                            // at this timing in consideration of a case that
                            // activityConditionCollection attribute of view model will be updated
                            // after updating conditionDataWorkflowConnectionPortViewModel attribute
                            // of view model (e.g. applyWorkflowStepViewModelCollection function of
                            // workflow parameter editor view).
                      }
                  );
            }
          }
      );
    })();

    /**
     * The mwa workflow condition view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowConditionView
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.View}
     */
    global.WorkflowConditionView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowConditionView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-parameter-editor-workflow-condition">' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.listenTo(
                  this.options.viewModel,
                  'change:isReadOnly',
                  this.applyIsReadOnly
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:canDelete',
                  this.applyCanDelete
              );

              this.listenToActivityConditionCollection(
                  this.options.viewModel,
                  this.options.viewModel.get('activityConditionCollection'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:activityConditionCollection',
                  this.listenToActivityConditionCollection
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:activityConditionCollection',
                  this.applyActivityConditionCollection
              );

              this.listenTo(
                  this.options.viewModel,
                  'change:conditionDataWorkflowConnectionPortViewModel',
                  this.applyConditionDataWorkflowConnectionPortViewModel
              );
            },
            /**
             * The event handler for changing activityConditionCollection attribute of view model.
             * This function removes listeners from old activityConditionCollection and adds
             * listeners to new activityConditionCollection.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
             * The mwa workflow condition view model.
             * @param {mwa.model.activityCondition.ActivityConditionCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToActivityConditionCollection: function(model, value, options) {
              var previousValue = model.previous('activityConditionCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(
                    value,
                    'add',
                    this.applyActivityConditionCollection
                );
                this.listenTo(
                    value,
                    'remove',
                    this.applyActivityConditionCollection
                );
                this.listenTo(
                    value,
                    'reset',
                    this.applyActivityConditionCollection
                );
              }
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              mwa.View.prototype.render.apply(that, []);

              that.$children = that.$el.children();

              that.renderItems();

              return that;
            },
            /**
             * The function to render item.
             * @protected
             *
             * @param {!mwa.model.activityCondition.ActivityConditionModel} activityConditionModel
             * The activity condition model to be rendered.
             * @param {!number} index
             * The number to specify the index of specified activity condition model.
             *
             * @returns {!mwa.view.workflowParameterEditor.WorkflowConditionItemView}
             * The rendered workflow condition item view instance.
             */
            renderItem: function(activityConditionModel, index) {
              var that = this;

              var itemView =
                  new mwa.view.workflowParameterEditor.WorkflowConditionItemView({
                    viewModel: new mwa.view.workflowParameterEditor.WorkflowConditionItemViewModel({
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      // CAUTION:
                      // Be careful about not forgetting to set canDelete attribute because
                      // applyWorkflowStepViewModelCollection function of workflow parameter editor
                      // view will be called twice by selection and deselection of workflow step and
                      // renderItems function of workflow condition view will be also called twice
                      // but applyCanDelete function of workflow parameter view won't be called
                      // twice due to the behavior that canDelete attribute of workflow parameter
                      // view model won't be changed in second call of
                      // applyWorkflowStepViewModelCollection function of workflow parameter editor.
                      canDelete: that.options.viewModel.get('canDelete'),
                      activityConditionModel: activityConditionModel,
                      conditionDataWorkflowConnectionPortViewModel:
                          that.options.viewModel.get('conditionDataWorkflowConnectionPortViewModel')
                    })
                  });

              var $item = that.$children.children();
              if ($item.length === 0 || index <= 0) {
                that.$children.prepend(itemView.render().$el);
              } else {
                $item.eq(index - 1).after(itemView.render().$el);
              }
              that.views.itemViewArray.splice(index, 0, itemView);

              return itemView;
            },
            /**
             * The function to render items.
             * @protected
             *
             * @returns {!mwa.view.workflowParameterEditor.WorkflowConditionItemView[]}
             * The rendered workflow condition item view instances.
             */
            renderItems: function() {
              var that = this;

              // Initialization
              mwa.proui._.invoke(that.views.itemViewArray, 'remove');
              that.views.itemViewArray = [];

              // Rendering
              that.options.viewModel.get('activityConditionCollection').forEach(
                  mwa.proui._.bind(that.renderItem, that)
              );

              return that.views.itemViewArray;
            },
            /**
             * The event handler for changing isReadOnly attribute of view model. This function
             * updates isReadOnly attribute of workflow condition item view models according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
             * The mwa workflow condition view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsReadOnly: function(model, value, options) {
              var that = this;

              if (that.views != null && that.views.itemViewArray != null) {
                that.views.itemViewArray.forEach(
                    function(itemView, index, array) {
                      itemView.options.viewModel.set(
                          'isReadOnly', that.options.viewModel.get('isReadOnly')
                      );
                    }
                );
              }
            },
            /**
             * The event handler for changing canDelete attribute of view model. This function
             * updates canDelete attribute of workflow condition item view models according to the
             * value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
             * The mwa workflow condition view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDelete: function(model, value, options) {
              var that = this;

              if (that.views != null && that.views.itemViewArray != null) {
                that.views.itemViewArray.forEach(
                    function(itemView, index, array) {
                      itemView.options.viewModel.set(
                          'canDelete', that.options.viewModel.get('canDelete')
                      );
                    }
                );
              }
            },
            /**
             * The event handler for changing activityConditionCollection attribute of view model
             * and changing, adding, removing, updating and resetting models in
             * activityConditionCollection. This function updates workflow condition item views
             * according to the value.
             * @protected
             */
            applyActivityConditionCollection: function() {
              // arguments:
              // WorkflowConditionViewModel
              //  - "change:activityConditionCollection" (model, value, options)
              // activityConditionCollection
              //  - "change"                             (model, options)
              //  - "add", "remove"                      (model, collection, options)
              //  - "update", "reset"                    (collection, options)
              var that = this;
              var args = {};

              var activityConditionCollection =
                  that.options.viewModel.get('activityConditionCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that.options.viewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
                   * The mwa workflow condition view model.
                   * @property {mwa.model.activityCondition.ActivityConditionModel} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  that.renderItems();
                } else if (arguments[0] instanceof mwa.model.activityCondition.ActivityConditionModel) {
                  if (arguments[1] !== activityConditionCollection) {
                    /**
                     * @type {object}
                     * @description The arguments of change event.
                     * @property {mwa.model.activityCondition.ActivityConditionModel} model
                     * The changed model.
                     * @property {*} value
                     * The new changed value.
                     * @property {object} options
                     * The options of change event.
                     */
                    args = {
                      model: arguments[0],
                      value: (arguments.length === 2) ? null : arguments[1],
                      options: (arguments.length === 2) ? arguments[1] : arguments[2]
                    };

                    // Change Case

                    // If you need any processing, add here.
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add or remove event.
                     * @property {mwa.model.activityCondition.ActivityConditionModel} model
                     * The added or removed model.
                     * @property {mwa.model.activityCondition.ActivityConditionCollection} collection
                     * The collection that was added or removed the model.
                     * @property {object} options
                     * The options of add or remove event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      if (that.views != null && that.views.itemViewArray != null) {
                        that.renderItem(args.model, index);
                      }
                    } else {
                      // Remove Case

                      if (that.views != null && that.views.itemViewArray != null) {
                        that.views.itemViewArray.splice(args.options.index, 1);
                      }
                    }
                  }
                } else if (arguments[0] === activityConditionCollection) {
                  /**
                   * @type {object}
                   * @description The arguments of update or reset event.
                   * @property {mwa.model.activityCondition.ActivityConditionCollection} collection
                   * The collection that was updated or reset.
                   * @property {object} options
                   * The options of update or reset event.
                   */
                  args = {
                    collection: arguments[0],
                    options: arguments[1]
                  };

                  if (args.options == null || args.options.previousModels == null) {
                    // Update Case

                    // If you need any processing, add here.
                  } else {
                    // Reset Case

                    that.renderItems();
                  }
                }
              }
            },
            /**
             * The event handler for changing conditionDataWorkflowConnectionPortViewModel attribute
             * of view model. This function updates conditionDataWorkflowConnectionPortViewModel
             * attribute of workflow condition item view models according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionViewModel} model
             * The mwa workflow condition view model.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyConditionDataWorkflowConnectionPortViewModel: function(model, value, options) {
              var that = this;

              if (that.views != null && that.views.itemViewArray != null) {
                that.views.itemViewArray.forEach(
                    function(itemView, index, array) {
                      itemView.options.viewModel.set(
                          'conditionDataWorkflowConnectionPortViewModel',
                          that.options.viewModel.get('conditionDataWorkflowConnectionPortViewModel')
                      );
                    }
                );
              }
            }
          }
      );
    })();

    /**
     * The mwa workflow condition item view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowConditionItemViewModel
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.ViewModel}
     */
    global.WorkflowConditionItemViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowConditionItemViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    isReadOnly: false,
                    canDelete: false,
                    activityConditionModel: null,
                    conditionDataWorkflowConnectionPortViewModel: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  isReadOnly: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canDelete: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  activityConditionModel: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityCondition.ActivityConditionModel
                    }
                  ],
                  conditionDataWorkflowConnectionPortViewModel: [
                    {
                      // CAUTION:
                      // It is necessary to consider about a case that the value is null (e.g. when
                      // selecting not conditional branch step).
                      required: false
                    },
                    {
                      instance: mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        var conditionDataWorkflowConnectionPortViewModel =
                            this.get('conditionDataWorkflowConnectionPortViewModel');
                        // CAUTION:
                        // It is necessary to add null check because this function will be called in
                        // such case.
                        if (conditionDataWorkflowConnectionPortViewModel instanceof
                            mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel) {
                          if (conditionDataWorkflowConnectionPortViewModel.get('key') !==
                              mwa.enumeration.ActivityParameter.CONDITION.key) {
                            msg =
                                'The "key" of "' + attr + '" must be ' +
                                'mwa.enumeration.ActivityParameter.CONDITION.key';
                          } else if (conditionDataWorkflowConnectionPortViewModel.get('type') !==
                              mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT) {
                            msg =
                                'The "type" of "' + attr + '" must be ' +
                                'mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT';
                          }
                        }
                        return msg;
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {boolean} [attributes.isReadOnly = false]
             * The flag whether workflow condition is read only.
             * @param {boolean} [attributes.canDelete = false]
             * The flag whether workflow condition is deletable.
             * @param {mwa.model.activityCondition.ActivityConditionModel} [attributes.activityConditionModel = null]
             * The activity condition model for updating specified workflow condition.
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} [attributes.conditionDataWorkflowConnectionPortViewModel = null]
             * The data workflow connection port view model corresponding to "condition" workflow
             * parameter of conditional branch step.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, [attributes, options]);
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa workflow condition item view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowParameterEditor.WorkflowConditionItemView
     * @memberof mwa.view.workflowParameterEditor
     * @extends {mwa.View}
     */
    global.WorkflowConditionItemView = (function() {
      var LEFT_VALUE_ATTRIBUTE_NAME = 'leftValue';
      var RIGHT_VALUE_ATTRIBUTE_NAME = 'rightValue';

      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowParameterEditor.WorkflowConditionItemView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-parameter-editor-workflow-condition-item">' +
                    '<div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-condition-item-condition-operator">' +
                            '<div></div>' +
                        '</div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-condition-item-left-value">' +
                            '<div class="mwa-view-workflow-parameter-editor-workflow-parameter"></div>' +
                        '</div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-condition-item-comparison-operator">' +
                            '<div></div>' +
                        '</div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-condition-item-right-value">' +
                            '<div class="mwa-view-workflow-parameter-editor-workflow-parameter"></div>' +
                        '</div>' +
                        '<div class="mwa-view-workflow-parameter-editor-workflow-condition-item-delete-button"></div>' +
                    '</div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.initialize}
             * @protected
             * @override
             */
            initialize: function(options, canRender) {
              this._timers = {};

              mwa.View.prototype.initialize.apply(this, [options, false]);
            },
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              var that = this;

              mwa.View.prototype.listen.apply(that, []);

              that.models = {};

              [
                {
                  valueAttributeName: LEFT_VALUE_ATTRIBUTE_NAME,
                  propertyName: 'left'
                },
                {
                  valueAttributeName: RIGHT_VALUE_ATTRIBUTE_NAME,
                  propertyName: 'right'
                }
              ].forEach(
                  // INFORMATION:
                  // The first argument is object type, not array. But we use plural name based on
                  // our JavaScript coding rule.
                  function(properties, index, array) {
                    var value =
                        that.options.viewModel
                            .get('activityConditionModel')
                            .get(properties.valueAttributeName);
                    // CAUTION:
                    // It is necessary to consider about a case that value is null. And it is
                    // important not to initialize it by empty string in order not to change the
                    // actual value unnecessarily in the following processing.
                    var hasConnection = mwa.util.ActivityCondition.hasConnection(value);
                    var fromWorkflowStepViewModel = (hasConnection) ?
                        that.options.viewModel
                            .get('conditionDataWorkflowConnectionPortViewModel')
                            .get('workflowStepViewModel')
                            .get('pipelineWorkflowStepViewModelCollection')
                            .get(mwa.util.ActivityCondition.getWorkflowStepId(value)) :
                        null;
                    var fromActivityParameterModel = (hasConnection) ?
                        fromWorkflowStepViewModel
                            .get('outputActivityParameterCollection')
                            // INFORMATION:
                            // It is possible to use get function instead of findWhere function in
                            // consideration of that idAttribute of activity parameter model is
                            // "key", not "id", but it is better to use findWhere function to make
                            // the code maintainable and readable.
                            .findWhere({
                              key: value.slice(value.indexOf('.') + 1)
                            }) :
                        null;
                    var workflowConnectionPortColor = null;

                    workflowConnectionPortColor =
                        (properties.valueAttributeName === LEFT_VALUE_ATTRIBUTE_NAME) ?
                            // CAUTION: NVX-5190 (https://www.tool.sony.biz/jira/browse/NVX-5190?focusedCommentId=1988534&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-1988534)
                            // It is necessary to set GRAY to be able to select all activity
                            // parameters for left value regardless of activity parameter type.
                            mwa.enumeration.WorkflowConnectionPortColor.GRAY :
                            // CAUTION:
                            // It is necessary to set appropriate color in consideration of decoding
                            // immediate value correctly.
                            (that.models.leftDataWorkflowConnectionViewModel == null) ?
                                // CAUTION:
                                // It is necessary to consider about a case that there is no output
                                // activity parameter in the first pipeline workflow step and
                                // leftValue attribute of activity condition model is null. In this
                                // case, any type should be selectable but the condition still be
                                // invalid.
                                that.models.leftDataWorkflowConnectionPortViewModel
                                    .get('color') :
                                that.models.leftDataWorkflowConnectionViewModel
                                    .get('fromWorkflowConnectionPortViewModel')
                                    .get('color');
                    that.models[properties.propertyName + 'DataWorkflowConnectionPortViewModel'] =
                        new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel({
                          id: mwa.proui.util.Uuid.generateUuidV4(),
                          // CAUTION:
                          // The key should be same as a part of value attributes of activity
                          // condition model (leftValue, rightValue) because this value will be used
                          // when updating activityConditionModel attribute of view model.
                          key: properties.valueAttributeName,
                          isRequired: false,
                          positions: {
                            x: 0,
                            y: 0,
                            z: 0
                          },
                          type: mwa.enumeration.WorkflowConnectionPortType.DATA_INPUT,
                          typeName:
                              (properties.valueAttributeName === LEFT_VALUE_ATTRIBUTE_NAME) ?
                                  workflowConnectionPortColor.activityParameterType.typeName :
                                  // CAUTION:
                                  // It is necessary to set appropriate typeName in consideration of
                                  // displaying setting value correctly.
                                  (that.models.leftDataWorkflowConnectionViewModel == null) ?
                                      // CAUTION:
                                      // It is necessary to consider about a case that there is no
                                      // output activity parameter in the first pipeline workflow
                                      // step and leftValue attribute of activity condition model is
                                      // null. In this case, any type should be selectable but the
                                      // condition still be invalid.
                                      that.models.leftDataWorkflowConnectionPortViewModel
                                          .get('typeName') :
                                      that.models.leftDataWorkflowConnectionViewModel
                                          .get('fromWorkflowConnectionPortViewModel')
                                          .get('typeName'),
                          color: workflowConnectionPortColor,
                          // CAUTION:
                          // It is important to prepare the different dummy workflow step view model
                          // instance every time especially without using clone function (because
                          // initialization will be executed by clone function and activity
                          // parameter collection will be updated involuntarily and unnecessarily)
                          // to be able to add a specified activity parameter model certainly in
                          // consideration of a case that the key isn't unique and manage the
                          // activity parameter collection separately.
                          workflowStepViewModel: that.createDummyWorkflowStepViewModel(),
                          connect: function(workflowConnectionView) {
                            var fromWorkflowConnectionPortViewModel =
                                workflowConnectionView.options.viewModel
                                    .get('fromWorkflowConnectionPortViewModel');

                            that.models[that.destroyDataWorkflowConnectionViewModel(this)] =
                                workflowConnectionView.options.viewModel;
                            that.options.viewModel.get('activityConditionModel').set(
                                this.get('key'),
                                '$' +
                                fromWorkflowConnectionPortViewModel
                                    .get('workflowStepViewModel')
                                    .get('id') +
                                '.' +
                                fromWorkflowConnectionPortViewModel.get('key')
                            );
                          }
                        });
                    that.models[properties.propertyName + 'DataWorkflowConnectionPortViewModel']
                        .get('workflowStepViewModel')
                        .get('inputActivityParameterCollection')
                        // INFORMATION:
                        // It is possible to use get function instead of findWhere function in
                        // consideration of that idAttribute of activity parameter model is "key",
                        // not "id", but it is better to use findWhere function to make the code
                        // maintainable and readable.
                        .findWhere({
                          key: properties.valueAttributeName
                        }).set({
                          // Immediate Value
                          value: (hasConnection) ? null : value,
                          result:
                              (
                                  that.options.viewModel
                                      .get('conditionDataWorkflowConnectionPortViewModel')
                                      .get('workflowStepViewModel')
                                      .has('activityInstanceModel')
                              ) ?
                                  (
                                      (hasConnection) ?
                                          fromActivityParameterModel.get('result') :
                                          // CAUTION:
                                          // It is necessary to decode the value because all values
                                          // in activity condition model are encoded.
                                          workflowConnectionPortColor.activityParameterType
                                              .decodeText(value)
                                  ) :
                                  null
                        });
                    // CAUTION:
                    // It is important to create a view instance corresponding to the view model in
                    // order to work the all features (e.g. event listeners) certainly. And it isn't
                    // necessary to manage the view instance and consider about memory leak because
                    // it will be remove correctly when the view model is destroyed in remove
                    // function of view.
                    if (that.models[properties.propertyName + 'DataWorkflowConnectionPortViewModel'] != null) {
                      new mwa.view.workflowConnectionPort.WorkflowConnectionPortView({
                        viewModel:
                            that.models[properties.propertyName + 'DataWorkflowConnectionPortViewModel']
                      });
                    }

                    workflowConnectionPortColor =
                        (hasConnection) ?
                            mwa.proui._.findWhere(
                                WORKFLOW_CONNECTION_PORT_COLOR_ARRAY,
                                {
                                  isArray: fromActivityParameterModel.get('listFlag'),
                                  activityParameterType: fromActivityParameterModel.get('type')
                                }
                            ) :
                            null;
                    that.models[properties.propertyName + 'DataWorkflowConnectionViewModel'] =
                        (hasConnection) ?
                            new mwa.view.workflowConnection.WorkflowConnectionViewModel({
                              fromWorkflowConnectionPortViewModel:
                                  new mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel({
                                    id: fromActivityParameterModel.get('id'),
                                    key: fromActivityParameterModel.get('key'),
                                    isRequired: fromActivityParameterModel.get('required'),
                                    positions: {
                                      x: 0,
                                      y: 0,
                                      z: 0
                                    },
                                    type: mwa.enumeration.WorkflowConnectionPortType.DATA_OUTPUT,
                                    // CAUTION:
                                    // It is necessary to set appropriate typeName instead of the
                                    // same of workflow connection port color in consideration of
                                    // displaying setting value correctly.
                                    typeName: fromActivityParameterModel.get('typeName'),
                                    color: workflowConnectionPortColor,
                                    // CAUTION:
                                    // It is important to prepare the different dummy workflow step
                                    // view model instance every time especially without using clone
                                    // function (because initialization will be executed by clone
                                    // function and activity parameter collection will be updated
                                    // involuntarily and unnecessarily) to be able to add a
                                    // specified activity parameter model certainly in consideration
                                    // of a case that the key isn't unique and manage the activity
                                    // parameter collection separately.
                                    workflowStepViewModel:
                                        that.createDummyWorkflowStepViewModel().set({
                                          // CAUTION:
                                          // It is necessary to set actual existing ID instead of
                                          // dummy ID to correctly display the initial selected item
                                          // of value source drop down button in workflow parameter
                                          // item.
                                          id: fromWorkflowStepViewModel.get('id')
                                        })
                                  }),
                              toWorkflowConnectionPortViewModel:
                                  that.models[properties.propertyName + 'DataWorkflowConnectionPortViewModel']
                            }) :
                            null;
                    // CAUTION:
                    // It is important to create a view instance corresponding to the view model in
                    // order to work the all features (e.g. event listeners) certainly. And it isn't
                    // necessary to manage the view instance and consider about memory leak because
                    // it will be remove correctly when the view model is destroyed in remove
                    // function of view.
                    if (that.models[properties.propertyName + 'DataWorkflowConnectionViewModel'] != null) {
                      new mwa.view.workflowConnection.WorkflowConnectionView({
                        viewModel:
                            that.models[properties.propertyName + 'DataWorkflowConnectionViewModel']
                      });
                    }
                  }
              );

              that.models =
                  mwa.proui._.extend(
                      that.models,
                      {
                        conditionOperatorDropDownButtonItemCollection:
                            that.createConditionOperatorDropDownButtonItemCollection(),
                        // CAUTION:
                        // It is necessary to create comparison operator drop down button item
                        // collection after creating leftDataWorkflowConnectionViewModel instance
                        // because it is necessary for
                        // createComparisonOperatorDropDownButtonItemCollection function.
                        comparisonOperatorDropDownButtonItemCollection:
                            that.createComparisonOperatorDropDownButtonItemCollection()
                      }
                  );

              that.listenTo(
                  that.models.leftDataWorkflowConnectionPortViewModel,
                  'change',
                  that.applyDataWorkflowConnectionPortViewModelChanges
              );
              that.listenTo(
                  that.models.rightDataWorkflowConnectionPortViewModel,
                  'change',
                  that.applyDataWorkflowConnectionPortViewModelChanges
              );

              that.listenTo(
                  that.options.viewModel,
                  'change:isReadOnly',
                  that.applyIsReadOnly
              );

              that.listenTo(
                  that.options.viewModel,
                  'change:canDelete',
                  that.applyCanDelete
              );

              that.listenToActivityConditionModel(
                  that.options.viewModel,
                  that.options.viewModel.get('activityConditionModel'),
                  null
              );
              that.listenTo(
                  that.options.viewModel,
                  'change:activityConditionModel',
                  that.listenToActivityConditionModel
              );
            },
            /**
             * The event handler for changing activityConditionModel attribute of view model. This
             * function removes listeners from old activityConditionModel and adds listeners to new
             * activityConditionModel.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionItemViewModel} model
             * The mwa workflow condition item view model.
             * @param {mwa.model.activityCondition.ActivityConditionModel} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToActivityConditionModel: function(model, value, options) {
              var previousValue = model.previous('activityConditionModel');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(
                    value,
                    'change:' + LEFT_VALUE_ATTRIBUTE_NAME,
                    this.applyActivityConditionLeftValue
                );
                this.listenTo(
                    value,
                    'destroy',
                    this.destroy
                );
              }
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              that.remove({silent: true});
              // INFORMATION:
              // It is important to call remove function instead of empty function for creating this
              // view's element from template.
              that.$el.remove();
              that.setElement(mwa.proui.Backbone.$(that.template(that.presenter())));
              that.cacheElement();

              that.views = {};

              that.renderLeftWorkflowParameterItem();
              that.renderRightWorkflowParameterItem();

              that.applyIsReadOnly(
                  that.options.viewModel,
                  that.options.viewModel.get('isReadOnly'),
                  null
              );

              var workflowStepViewModel =
                  that.options.viewModel
                      .get('conditionDataWorkflowConnectionPortViewModel')
                      .get('workflowStepViewModel');
              that.applyActivityInstanceModel(
                  workflowStepViewModel,
                  workflowStepViewModel.get('activityInstanceModel'),
                  null
              );

              // CAUTION:
              // It is important to clear the timer before updating it in consideration of a case
              // that this processing is executed multiple times during the interval.
              clearInterval(that._timers.render);
              // CAUTION:
              // It is important to call renderDeleteCommandButton function after rendering view's
              // element because the background image is unknown before rendering.
              that._timers.render = setInterval(function() {
                if (that.$el.width() != null) {
                  clearInterval(that._timers.render);

                  // CAUTION:
                  // It is necessary to consider about a case that this processing is executed after
                  // removal.
                  if (that.views != null) {
                    that.renderDeleteCommandButton();
                  }

                  that.applyCanDelete(
                      that.options.viewModel,
                      that.options.viewModel.get('canDelete'),
                      null
                  );
                }
              }, 100);

              return this;
            },
            /**
             * The function to cache element for internal processing.
             * @protected
             */
            cacheElement: function() {
              this.$leftValue =
                  this.$el.find('.mwa-view-workflow-parameter-editor-workflow-condition-item-left-value').children();
              this.$rightValue =
                  this.$el.find('.mwa-view-workflow-parameter-editor-workflow-condition-item-right-value').children();
              this.$conditionOperator =
                  this.$el.find('.mwa-view-workflow-parameter-editor-workflow-condition-item-condition-operator');
              this.$comparisonOperator =
                  this.$el.find('.mwa-view-workflow-parameter-editor-workflow-condition-item-comparison-operator');
            },
            /**
             * The function to render left workflow parameter item.
             * @protected
             *
             * @returns {!mwa.view.workflowParameterEditor.WorkflowParameterItemView}
             * The rendered workflow parameter item view instance.
             */
            renderLeftWorkflowParameterItem: function() {
              var that = this;

              // Initialization
              if (that.views.leftWorkflowParameterItemView != null) {
                that.views.leftWorkflowParameterItemView.remove();
              }
              that.views.leftWorkflowParameterItemView = null;

              // Rendering
              that.views.leftWorkflowParameterItemView =
                  new mwa.view.workflowParameterEditor.WorkflowParameterItemView({
                    viewModel: new mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel({
                      hasPredefinedWorkflowStep: false,
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      canDelete: false,
                      dataWorkflowConnectionPortViewModel:
                          that.models.leftDataWorkflowConnectionPortViewModel,
                      dataWorkflowConnectionViewModel:
                          that.models.leftDataWorkflowConnectionViewModel
                    })
                  });
              that.$leftValue.append(that.views.leftWorkflowParameterItemView.render().$el);

              return that.views.leftWorkflowParameterItemView;
            },
            /**
             * The function to render right workflow parameter item.
             * @protected
             *
             * @returns {!mwa.view.workflowParameterEditor.WorkflowParameterItemView}
             * The rendered workflow parameter item view instance.
             */
            renderRightWorkflowParameterItem: function() {
              var that = this;

              // Initialization
              if (that.views.rightWorkflowParameterItemView != null) {
                that.views.rightWorkflowParameterItemView.remove();
              }
              that.views.rightWorkflowParameterItemView = null;

              // Rendering
              that.views.rightWorkflowParameterItemView =
                  new mwa.view.workflowParameterEditor.WorkflowParameterItemView({
                    viewModel: new mwa.view.workflowParameterEditor.WorkflowParameterItemViewModel({
                      hasPredefinedWorkflowStep: true,
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      canDelete: false,
                      dataWorkflowConnectionPortViewModel:
                          that.models.rightDataWorkflowConnectionPortViewModel,
                      dataWorkflowConnectionViewModel:
                          that.models.rightDataWorkflowConnectionViewModel
                    })
                  });
              that.$rightValue.append(that.views.rightWorkflowParameterItemView.render().$el);

              return that.views.rightWorkflowParameterItemView;
            },
            /**
             * The function to render condition operator drop down button.
             * @protected
             *
             * @returns {!proui.dropDownButton.DropDownButtonView}
             * The rendered drop down button view instance.
             */
            renderConditionOperatorDropDownButton: function() {
              var that = this;

              // Initialization
              that.removeConditionOperatorControls();

              // Rendering
              var $el = that.$conditionOperator.children();
              that.views.conditionOperatorDropDownButtonView =
                  new mwa.proui.dropDownButton.DropDownButtonView({
                    el: $el,
                    viewModel: new mwa.proui.dropDownButton.DropDownButtonViewModel({
                      isStrong: true,
                      hasTooltip: false,
                      label: 'label',
                      // INFORMATION:
                      // This setting will be updated in the following processing.
                      maxHeight: null,
                      hasInitialItem: false,
                      itemCollection: that.models.conditionOperatorDropDownButtonItemCollection,
                      selectedItem: that.getConditionOperatorInitialItemModel(),
                      select: function(selectedItemModel) {
                        that.options.viewModel
                            .get('activityConditionModel')
                            .set(
                                'conditionOperator',
                                mwa.enumeration.ActivityConditionConditionOperator[selectedItemModel.get('name')]
                            );
                      }
                    })
                  });

              // CAUTION:
              // It is important to update max height setting after rendering view's element because
              // the style settings cannot be obtained before rendering.
              updateDropDownButtonMaxHeight(that, that.views.conditionOperatorDropDownButtonView);

              return that.views.conditionOperatorDropDownButtonView;
            },
            /**
             * The function to render condition operator text box.
             * @protected
             *
             * @returns {!proui.textBox.TextBoxView}
             * The rendered text box view instance.
             */
            renderConditionOperatorTextBox: function() {
              var that = this;

              // Initialization
              that.removeConditionOperatorControls();

              // Rendering
              var $el = that.$conditionOperator.children();
              that.views.conditionOperatorTextBoxView =
                  new mwa.proui.textBox.TextBoxView({
                    el: $el,
                    viewModel: new mwa.proui.textBox.TextBoxViewModel({
                      hasTooltip: false,
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      text: that.getConditionOperatorInitialItemModel().get('label')
                    })
                  });

              return that.views.conditionOperatorTextBoxView;
            },
            /**
             * The function to render comparison operator drop down button.
             * @protected
             *
             * @returns {!proui.dropDownButton.DropDownButtonView}
             * The rendered drop down button view instance.
             */
            renderComparisonOperatorDropDownButton: function() {
              var that = this;

              // Initialization
              that.removeComparisonOperatorControls();

              // Rendering
              var $el = that.$comparisonOperator.children();
              that.views.comparisonOperatorDropDownButtonView =
                  new mwa.proui.dropDownButton.DropDownButtonView({
                    el: $el,
                    viewModel: new mwa.proui.dropDownButton.DropDownButtonViewModel({
                      isStrong: true,
                      hasTooltip: false,
                      label: 'label',
                      // INFORMATION:
                      // This setting will be updated in the following processing.
                      maxHeight: null,
                      hasInitialItem: false,
                      itemCollection: that.models.comparisonOperatorDropDownButtonItemCollection,
                      selectedItem: that.getComparisonOperatorInitialItemModel(),
                      select: function(selectedItemModel) {
                        that.options.viewModel
                            .get('activityConditionModel')
                            .set(
                                'comparisonOperator',
                                mwa.enumeration.ActivityConditionComparisonOperator[selectedItemModel.get('name')]
                            );
                      }
                    })
                  });

              // CAUTION:
              // It is important to update max height setting after rendering view's element because
              // the style settings cannot be obtained before rendering.
              updateDropDownButtonMaxHeight(that, that.views.comparisonOperatorDropDownButtonView);

              return that.views.comparisonOperatorDropDownButtonView;
            },
            /**
             * The function to render comparison operator text box.
             * @protected
             *
             * @returns {!proui.textBox.TextBoxView}
             * The rendered text box view instance.
             */
            renderComparisonOperatorTextBox: function() {
              var that = this;

              // Initialization
              that.removeComparisonOperatorControls();

              // Rendering
              var $el = that.$comparisonOperator.children();
              that.views.comparisonOperatorTextBoxView =
                  new mwa.proui.textBox.TextBoxView({
                    el: $el,
                    viewModel: new mwa.proui.textBox.TextBoxViewModel({
                      hasTooltip: false,
                      isReadOnly: that.options.viewModel.get('isReadOnly'),
                      text: that.getComparisonOperatorInitialItemModel().get('label')
                    })
                  });

              return that.views.comparisonOperatorTextBoxView;
            },
            /**
             * The function to render delete command button.
             * @public
             *
             * @returns {proui.commandButton.CommandButtonView}
             * The rendered command button view instance.
             */
            renderDeleteCommandButton: function() {
              var that = this;

              // Initialization
              if (that.views.deleteCommandButtonView != null) {
                that.views.deleteCommandButtonView.remove();
              }
              that.views.deleteCommandButtonView = null;

              // Rendering
              var $el = that.$el.find('.mwa-view-workflow-parameter-editor-workflow-condition-item-delete-button');
              that.views.deleteCommandButtonView =
                  new mwa.proui.commandButton.CommandButtonView({
                    el: $el,
                    viewModel: new mwa.proui.commandButton.CommandButtonViewModel({
                      icon: $el.css('background-image'),
                      type: mwa.proui.commandButton.ButtonType.NONE,
                      size: mwa.proui.commandButton.ButtonSize.SMALL,
                      tooltip: mwa.enumeration.Message['TITLE_DELETE'],
                      command: function() {
                        // CAUTION:
                        // It is important not to destroy the activity condition model in remove
                        // function because it should be destroyed only in this case in
                        // consideration of re-rendering.
                        that.options.viewModel.get('activityConditionModel').destroy();

                        // INFORMATION:
                        // It isn't necessary to remove the view itself explicitly here because
                        // remove function will be called in response to destroy event of
                        // activityConditionModel attribute of view model.
                      }
                    })
                  });
              $el.css('background-image', 'none');

              return that.views.deleteCommandButtonView;
            },
            /**
             * @see {@link mwa.View.remove}
             * @public
             * @override
             */
            remove: function(options) {
              if (typeof options === 'undefined' || options == null || !options.silent) {
                var propertyName = null;

                // CAUTION:
                // It is important to clear the all timers when remove in order not to execute the
                // unnecessary processing after removal.
                for (propertyName in this._timers) {
                  if (this._timers.hasOwnProperty(propertyName)) {
                    clearInterval(this._timers[propertyName]);
                  }
                }

                // CAUTION:
                // It is important to destroy the all view models when remove in order not to make
                // memory leak regarding the corresponding view.
                for (propertyName in this.models) {
                  if (this.models.hasOwnProperty(propertyName)) {
                    if (this.models[propertyName] instanceof mwa.ViewModel) {
                      // CAUTION:
                      // It is important to stop the listeners for view model before destroying it
                      // not to execute unnecessary processing.
                      this.stopListening(this.models[propertyName]);
                      this.models[propertyName].destroy();
                    }
                  }
                }
              }

              return mwa.View.prototype.remove.apply(this, [options]);
            },
            /**
             * The function to remove condition operator controls and initialize DOM structure.
             * @protected
             */
            removeConditionOperatorControls: function() {
              var that = this;

              var views = {
                conditionOperatorDropDownButtonView:
                    that.views.conditionOperatorDropDownButtonView,
                conditionOperatorTextBoxView:
                    that.views.conditionOperatorTextBoxView
              };
              for (var propertyName in views) {
                if (views.hasOwnProperty(propertyName)) {
                  if (views[propertyName] != null) {
                    views[propertyName].remove();
                    mwa.proui.Backbone.$('<div></div>').appendTo(that.$conditionOperator);
                  }
                  that.views[propertyName] = null;
                }
              }
            },
            /**
             * The function to remove comparison operator controls and initialize DOM structure.
             * @protected
             */
            removeComparisonOperatorControls: function() {
              var that = this;

              var views = {
                comparisonOperatorDropDownButtonView:
                    that.views.comparisonOperatorDropDownButtonView,
                comparisonOperatorTextBoxView:
                    that.views.comparisonOperatorTextBoxView
              };
              for (var propertyName in views) {
                if (views.hasOwnProperty(propertyName)) {
                  if (views[propertyName] != null) {
                    views[propertyName].remove();
                    mwa.proui.Backbone.$('<div></div>').appendTo(that.$comparisonOperator);
                  }
                  that.views[propertyName] = null;
                }
              }
            },
            /**
             * The function to destroy data workflow connection view model based on specified data
             * workflow connection port view model.
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} dataWorkflowConnectionPortViewModel
             * The data workflow connection port view model to be used for finding data workflow
             * connection view model to be destroyed.
             *
             * @returns {string}
             * The destroyed data workflow connection view model name.
             */
            destroyDataWorkflowConnectionViewModel: function(dataWorkflowConnectionPortViewModel) {
              var dataWorkflowConnectionViewModelName =
                  dataWorkflowConnectionPortViewModel.get('key').replace('Value', '') +
                  'DataWorkflowConnectionViewModel';

              // // CAUTION:
              // // It is necessary to stop the listener that is for updating activityConditionModel
              // // attribute of view model before destroying previous workflow connection view model
              // // that became to be unnecessary in consideration of a case that change events (e.g.
              // // change:isConnected) will be occurred and not to update activityConditionModel
              // // attribute of view model unnecessarily in such case.
              // this.stopListening(
              //     dataWorkflowConnectionPortViewModel,
              //     'change',
              //     this.applyDataWorkflowConnectionPortViewModelChanges
              // );
              if (this.models[dataWorkflowConnectionViewModelName] != null) {
                this.models[dataWorkflowConnectionViewModelName].destroy();
              }
              // // CAUTION:
              // // It is necessary to revert the listener setting for next operation.
              // this.listenTo(
              //     dataWorkflowConnectionPortViewModel,
              //     'change',
              //     this.applyDataWorkflowConnectionPortViewModelChanges
              // );

              return dataWorkflowConnectionViewModelName;
            },
            /**
             * The function to create dummy workflow step view model.
             * @protected
             *
             * @returns {mwa.view.workflowStep.WorkflowStepViewModel}
             * The created workflow step view model instance.
             */
            createDummyWorkflowStepViewModel: function() {
              var workflowStepViewModel =
                  this.options.viewModel
                      .get('conditionDataWorkflowConnectionPortViewModel')
                      .get('workflowStepViewModel');

              return new mwa.view.workflowStep.WorkflowStepViewModel({
                id: mwa.proui.util.Uuid.generateUuidV4(),
                text: 'DUMMY',
                positions: {
                  x: 0,
                  y: 0,
                  z: 0
                },
                activityModel: new mwa.model.activity.ActivityModel(
                    null, {validate: false}
                ),
                // CAUTION: NVX-5281 (https://www.tool.sony.biz/jira/browse/NVX-5281)
                // It is necessary to set activity instance model to display the result in left and
                // right workflow parameter items.
                activityInstanceModel: (workflowStepViewModel.has('activityInstanceModel')) ?
                    new mwa.model.activityInstance.ActivityInstanceModel(
                        null, {validate: false}
                    ) :
                    null,
                pipelineWorkflowStepViewModelCollection:
                    workflowStepViewModel
                        // CAUTION:
                        // It is important not to clone the instance in order to synchronously
                        // update value source drop down button of workflow parameter item view.
                        .get('pipelineWorkflowStepViewModelCollection')
              });
            },
            /**
             * The function to create condition operator drop down button item collection.
             * @public
             *
             * @returns {Backbone.Collection}
             * The created condition operator drop down button item collection instance.
             */
            createConditionOperatorDropDownButtonItemCollection: function() {
              return new mwa.proui.Backbone.Collection(
                  ACTIVITY_CONDITION_CONDITION_OPERATOR_ARRAY
              );
            },
            /**
             * The function to create comparison operator drop down button item collection.
             * @protected
             *
             * @returns {Backbone.Collection}
             * The created comparison operator drop down button item collection instance.
             */
            createComparisonOperatorDropDownButtonItemCollection: function() {
              var that = this;

              return new mwa.proui.Backbone.Collection(
                  (that.models.leftDataWorkflowConnectionViewModel == null) ?
                      null :
                      ACTIVITY_CONDITION_COMPARISON_OPERATOR_ARRAY.filter(
                          function(activityConditionComparisonOperator, index, array) {
                            return activityConditionComparisonOperator.canUse(
                                that.models.leftDataWorkflowConnectionViewModel
                                    .get('fromWorkflowConnectionPortViewModel')
                                    .get('color').activityParameterType
                            );
                          }
                      )
              );
            },
            /**
             * The function to get condition operator initial item model.
             * @public
             *
             * @returns {!Backbone.Model}
             * The condition operator initial item model.
             */
            getConditionOperatorInitialItemModel: function() {
              return (
                  this.models.conditionOperatorDropDownButtonItemCollection.findWhere({
                    operator:
                        this.options.viewModel
                            .get('activityConditionModel')
                            .get('conditionOperator').operator
                  }) ||
                  this.models.conditionOperatorDropDownButtonItemCollection.first()
              );
            },
            /**
             * The function to get comparison operator initial item model.
             * @public
             *
             * @returns {!Backbone.Model}
             * The comparison operator initial item model.
             */
            getComparisonOperatorInitialItemModel: function() {
              return (
                  this.models.comparisonOperatorDropDownButtonItemCollection.findWhere({
                    operator:
                        this.options.viewModel
                            .get('activityConditionModel')
                            .get('comparisonOperator').operator
                  }) ||
                  this.models.comparisonOperatorDropDownButtonItemCollection.first()
              );
            },
            /**
             * The event handler for changing of leftDataWorkflowConnectionPortViewModel or
             * rightDataWorkflowConnectionPortViewModel. This function updates
             * activityConditionModel attribute of view model according to the value.
             * @protected
             *
             * @param {mwa.view.workflowConnectionPort.WorkflowConnectionPortViewModel} model
             * The changed model.
             * @param {object} options
             * The options of change event.
             */
            applyDataWorkflowConnectionPortViewModelChanges: function(model, options) {
              // INFORMATION:
              // The data workflow connection view model always should be set to null because an
              // immediate or setting value was set in this case.
              this.models[this.destroyDataWorkflowConnectionViewModel(model)] = null;
              this.options.viewModel.get('activityConditionModel').set(
                  model.get('key'),
                  model.toObject().parameter.value
              );
            },
            /**
             * The event handler for changing isReadOnly attribute of view model. This function
             * renders value source and target according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionItemViewModel} model
             * The mwa workflow condition item view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyIsReadOnly: function(model, value, options) {
              var isReadOnly = this.options.viewModel.get('isReadOnly');

              this.$el.toggleClass(
                  'mwa-view-workflow-parameter-editor-state-read-only',
                  isReadOnly
              );

              if (this.views != null) {
                [
                  {
                    workflowParameterItemView: this.views.leftWorkflowParameterItemView
                  },
                  {
                    workflowParameterItemView: this.views.rightWorkflowParameterItemView
                  }
                ].forEach(
                    // INFORMATION:
                    // The first argument is object type, not array. But we use plural name based on
                    // our JavaScript coding rule.
                    function(properties, index, array) {
                      if (properties.workflowParameterItemView instanceof
                          mwa.view.workflowParameterEditor.WorkflowParameterItemView) {
                        properties.workflowParameterItemView.options.viewModel.set(
                            'isReadOnly', isReadOnly
                        );
                      }
                    }
                );
              }

              if (isReadOnly) {
                this.renderConditionOperatorTextBox();
                this.renderComparisonOperatorTextBox();
              } else {
                this.renderConditionOperatorDropDownButton();
                this.renderComparisonOperatorDropDownButton();
              }
            },
            /**
             * The event handler for changing canDelete attribute of view model. This function shows
             * or hides delete button according to the value.
             * @protected
             *
             * @param {mwa.view.workflowParameterEditor.WorkflowConditionItemViewModel} model
             * The mwa workflow condition item view model.
             * @param {boolean} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyCanDelete: function(model, value, options) {
              if (this.views != null && this.views.deleteCommandButtonView != null) {
                this.views.deleteCommandButtonView.$el.toggle(
                    this.options.viewModel.get('canDelete')
                );
              }
            },
            /**
             * The event handler for changing leftValue attribute of activity condition model. This
             * function updates comparison operator drop down button and right workflow parameter
             * item according to the value.
             * @protected
             *
             * @param {mwa.model.activityCondition.ActivityConditionModel} model
             * The mwa activity condition model.
             * @param {string} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            applyActivityConditionLeftValue: function(model, value, options) {
              var that = this;

              if (that.views != null) {
                if (that.views.comparisonOperatorDropDownButtonView != null) {
                  var comparisonOperatorDropDownButtonItemCollection =
                      that.createComparisonOperatorDropDownButtonItemCollection();
                  that.views.comparisonOperatorDropDownButtonView.options.viewModel.set(
                      'itemCollection',
                      comparisonOperatorDropDownButtonItemCollection
                  ).set(
                      // CAUTION:
                      // It is better to set selectedItem attribute of drop down button view model
                      // after updating itemCollection attribute in order to update the selection
                      // correctly by insuring the order of change events.
                      'selectedItem',
                      comparisonOperatorDropDownButtonItemCollection.first()
                  );
                }
              }

              var workflowConnectionPortColor =
                  (that.models.leftDataWorkflowConnectionViewModel == null) ?
                      mwa.enumeration.WorkflowConnectionPortColor.GRAY :
                      // CAUTION:
                      // It is necessary to set appropriate color in consideration of decoding
                      // immediate value correctly.
                      that.models.leftDataWorkflowConnectionViewModel
                          .get('fromWorkflowConnectionPortViewModel')
                          .get('color');
              that.models.rightDataWorkflowConnectionPortViewModel.set(
                  {
                    // CAUTION:
                    // It is necessary to update typeName attribute of data workflow connection port
                    // view model with color attribute in order to update selectable workflow
                    // parameters.
                    typeName:
                        (that.models.leftDataWorkflowConnectionViewModel == null) ?
                            workflowConnectionPortColor.activityParameterType.typeName :
                            // CAUTION:
                            // It is necessary to set appropriate typeName in consideration of
                            // displaying setting value correctly.
                            that.models.leftDataWorkflowConnectionViewModel
                                .get('fromWorkflowConnectionPortViewModel')
                                .get('typeName'),
                    color: workflowConnectionPortColor
                  },
                  {
                    $el: that.$rightValue
                  }
              );
            },
            /**
             * The event handler for changing activityInstanceModel attribute of workflow step view
             * model. This function shows or hides delete button and adds or removes status style
             * settings according to the value.
             * @protected
             *
             * @param {mwa.view.workflowStep.WorkflowStepViewModel} model
             * The mwa workflow step view model.
             * @param {mwa.model.activityInstance.ActivityInstanceModel} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyActivityInstanceModel: function(model, value, options) {
              var result =
                  (
                      this.options.viewModel
                          .get('conditionDataWorkflowConnectionPortViewModel')
                          .get('workflowStepViewModel')
                          .has('activityInstanceModel')
                  ) ?
                      this.options.viewModel
                          .get('activityConditionModel')
                          .get('comparisonOperator')
                          .evaluate(
                              // INFORMATION:
                              // It is possible to use get function instead of findWhere function in
                              // consideration of that idAttribute of activity parameter model is
                              // "key", not "id", but it is better to use findWhere function to make
                              // the code maintainable and readable.
                              this.models.leftDataWorkflowConnectionPortViewModel
                                  .get('workflowStepViewModel')
                                  .get('inputActivityParameterCollection')
                                  .findWhere({
                                    key: this.models.leftDataWorkflowConnectionPortViewModel.get('key')
                                  })
                                  .get('result'),
                              this.models.rightDataWorkflowConnectionPortViewModel
                                  .get('workflowStepViewModel')
                                  .get('inputActivityParameterCollection')
                                  .findWhere({
                                    key: this.models.rightDataWorkflowConnectionPortViewModel.get('key')
                                  })
                                  .get('result')
                          ) :
                      null;

              this.$el.removeClass('proui-state-success proui-state-error');
              if (typeof result === 'boolean') {
                this.$el.addClass((result === true) ? 'proui-state-success' : 'proui-state-error');
              }
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowParameterEditor;
}));
