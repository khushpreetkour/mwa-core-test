/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.activityProfileGroup = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.activityProfileGroup namespace.
   * @namespace
   */
  mwa.model.activityProfileGroup = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityProfileGroup alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa activity profile group model.
     * @constructor
     * @inner
     * @alias mwa.model.activityProfileGroup.ActivityProfileGroupModel
     * @memberof mwa.model.activityProfileGroup
     * @extends {mwa.Model}
     */
    global.ActivityProfileGroupModel = (function() {
      var MAX_LENGTH_FOR_NAME = 256;

      return mwa.Model.extend(
          /**
           * @lends mwa.model.activityProfileGroup.ActivityProfileGroupModel
           */
          {
            alias: 'mwa.model.activityProfileGroup.ActivityProfileGroupModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activity-profile-groups' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    templateId: null,
                    name: null,
                    icon: mwa.enumeration.Icon.DIRECTORY.dataUri
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  templateId: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    },
                    {
                      maxLength: MAX_LENGTH_FOR_NAME
                    }
                  ],
                  icon: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.Model.createEditingMetadataFieldCollection}
             * @public
             * @override
             */
            createEditingMetadataFieldCollection: function() {
              var modelEditingMetadataFieldCollection =
                  mwa.Model.prototype.createEditingMetadataFieldCollection.apply(this, []);
              var length = modelEditingMetadataFieldCollection.length;

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                  modelEditingMetadataFieldCollection.models.concat([
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'name',
                      index: length++,
                      title: mwa.enumeration.Message['SENTENCE_NAME'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                            {
                              type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MAX_LENGTH,
                              value: MAX_LENGTH_FOR_NAME,
                              text:
                                  mwa.enumeration.Message['VALIDATION_MAXIMUM_LENGTH'].replace(
                                      /\{0\}/,
                                      mwa.enumeration.Message['SENTENCE_NUMBER_OF_CHARACTERS'].toLowerCase()
                                  ).replace(
                                      /\{1\}/,
                                      '' + MAX_LENGTH_FOR_NAME
                                  )
                            }
                          ]),
                      isArray: false,
                      isHidden: false,
                      isRequired: true,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: true,
                      canFilter: false,
                      canResize: true
                    },
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'icon',
                      index: length++,
                      title: mwa.enumeration.Message['SENTENCE_ICON'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.IMAGE,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                            {
                              type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.ONE_OF,
                              value: [
                                'image/gif', 'image/jpeg', 'image/png', 'image/svg+xml'
                              ],
                              text:
                                  mwa.enumeration.Message['VALIDATION_TYPE'].replace(
                                      /\{0\}/,
                                      mwa.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                                  ).replace(
                                      /\{1\}/,
                                      mwa.enumeration.Message['SENTENCE_IMAGE'].toLowerCase() +
                                      ' ' +
                                      '(' +
                                      mwa.enumeration.Message['UPPER_GIF'] + ', ' +
                                      mwa.enumeration.Message['UPPER_JPEG'] + ', ' +
                                      mwa.enumeration.Message['UPPER_PNG'] + ', ' +
                                      mwa.enumeration.Message['UPPER_SVG'] +
                                      ')' +
                                      (
                                          ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                              ' ' : ''
                                      )
                                  )
                            },
                            {
                              type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MAX,
                              value: 4000,
                              text:
                                  mwa.enumeration.Message['VALIDATION_IS_OR_SMALLER'].replace(
                                      /\{0\}/,
                                      mwa.enumeration.Message['SENTENCE_SIZE'].toLowerCase()
                                  ).replace(
                                      /\{1\}/,
                                      (
                                          ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                              ' ' : ''
                                      ) +
                                      '4' +
                                      ' ' +
                                      mwa.enumeration.Message['UPPER_KB'] +
                                      (
                                          ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                              ' ' : ''
                                      )
                                  )
                            }
                          ]),
                      isArray: false,
                      isHidden: false,
                      isRequired: true,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: false,
                      canFilter: false,
                      canResize: false
                    }
                  ])
              );
            }
          }
      );
    })();

    /**
     * The mwa activity profile group collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityProfileGroup.ActivityProfileGroupCollection
     * @memberof mwa.model.activityProfileGroup
     * @extends {mwa.Collection}
     */
    global.ActivityProfileGroupCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activityProfileGroup.ActivityProfileGroupCollection
           */
          {
            url: global.ActivityProfileGroupModel.prototype.urlRoot,
            model: global.ActivityProfileGroupModel,
            comparator: 'name'
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityProfileGroup;
}));
