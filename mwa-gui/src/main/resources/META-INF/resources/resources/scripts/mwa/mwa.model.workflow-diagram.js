/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.workflowDiagram = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.workflowDiagram namespace.
   * @namespace
   */
  mwa.model.workflowDiagram = (function() {
    'use strict';

    /**
     * Defines mwa.model.workflowDiagram alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow diagram model.
     * @constructor
     * @inner
     * @alias mwa.model.workflowDiagram.WorkflowDiagramModel
     * @memberof mwa.model.workflowDiagram
     * @extends {mwa.Model}
     */
    global.WorkflowDiagramModel = (function() {
      var WORKFLOW_ITEM_ATTRIBUTE_NAME_ARRAY = ['steps', 'connectionPorts', 'connections'];
      var MAX_LENGTH_FOR_NAME = 256;

      return mwa.Model.extend(
          /**
           * @lends mwa.model.workflowDiagram.WorkflowDiagramModel
           */
          {
            alias: 'mwa.model.workflowDiagram.WorkflowDiagramModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'workflow-diagrams' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    name: null,
                    version: null,
                    steps: null,
                    connectionPorts: null,
                    connections: null,
                    createTime: -1,
                    updateTime: -1
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    },
                    // INFORMATION:
                    // It isn't necessary to consider that this setting will be applied to
                    // WorkflowModel because the validation setting will be overridden by
                    // ActivityModel.
                    {
                      maxLength: MAX_LENGTH_FOR_NAME
                    }
                  ],
                  version: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  steps: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ],
                  connectionPorts: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ],
                  connections: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ],
                  createTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  updateTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              options = options || {};

              response = mwa.Model.prototype.parse.apply(this, [response, options]);

              var optionAttributeKeyArray = mwa.proui._.keys(options.attrs);

              // CAUTION: NVX-5255 (https://www.tool.sony.biz/jira/browse/NVX-5255)
              // It is necessary to remove unnecessary attributes from the response when rename in
              // order not to override unsaved attributes (e.g. steps, connectionPorts, connections)
              // by the response.
              if (options.patch === true &&
                  optionAttributeKeyArray.length === 1 &&
                  optionAttributeKeyArray.indexOf('name') !== -1) {
                response = mwa.proui._.pick(response, 'name');
              }

              // INFORMATION:
              // It is necessary to check type because this function is executed before validation.
              if (mwa.proui.util.Object.isArray(response.connectionPorts)) {
                response.connectionPorts.forEach(
                    function(connectionPorts, index, array) {
                      // INFORMATION:
                      // It is important to check whether the value should be converted because
                      // there is a case that the value has been already converted.
                      if (typeof connectionPorts.type === 'string') {
                        connectionPorts.type =
                            mwa.enumeration.WorkflowConnectionPortType[connectionPorts.type];
                      }
                    }
                );
              }

              return response;
            },
            /**
             * @see {@link mwa.Model.toJSON}
             * @public
             * @override
             */
            toJSON: function(options) {
              var json = mwa.Model.prototype.toJSON.apply(this, [options]);
              // CAUTION:
              // It is important to use deep copied object in consideration of a case that a part of
              // object properties will be modified through the following processing.
              json = mwa.proui.util.Object.extend(true, {}, json);

              if (mwa.proui.util.Object.isArray(json.connectionPorts)) {
                json.connectionPorts.forEach(
                    function(connectionPorts, index, array) {
                      for (var propertyName in mwa.enumeration.WorkflowConnectionPortType) {
                        if (mwa.enumeration.WorkflowConnectionPortType.hasOwnProperty(propertyName)) {
                          // INFORMATION:
                          // It is important to check whether the value should be converted because
                          // there is a case that the value has been already converted.
                          if (mwa.proui._.isEqual(connectionPorts.type, mwa.enumeration.WorkflowConnectionPortType[propertyName])) {
                            connectionPorts.type =
                                mwa.enumeration.WorkflowConnectionPortType[propertyName].name;
                            break;
                          }
                        }
                      }
                    }
                );
              }

              return json;
            },
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              var that = this;

              mwa.Model.prototype.listen.apply(that, []);

              WORKFLOW_ITEM_ATTRIBUTE_NAME_ARRAY.forEach(
                  function(workflowItemAttributeName, index, array) {
                    that.listenTo(
                        that,
                        'change:' + workflowItemAttributeName,
                        that.applyWorkflowItems
                    );
                  }
              );
            },
            /**
             * @see {@link mwa.Model.fetch}
             * @public
             * @override
             */
            fetch: function(options) {
              var that = this;

              var success = (options || {}).success;

              return mwa.Model.prototype.fetch.apply(
                  that,
                  [
                    mwa.proui._.extend(
                        {},
                        options,
                        {
                          // CAUTION:
                          // It is necessary to override success callback to update internal managed
                          // flag instead of achieving the same by using done callback because done
                          // callback will be called after success callback and sync event. This
                          // update requires to be executed before such processing.
                          success: function(model, response, options) {
                            that._hasChangedWorkflowItems = false;

                            if (typeof success === 'function') {
                              success(model, response, options);
                            }
                          }
                        }
                    )
                  ]
              );
            },
            /**
             * @see {@link mwa.Model.save}
             * @public
             * @override
             */
            save: function(attributes, options) {
              var that = this;

              var success = (options || {}).success;

              return mwa.Model.prototype.save.apply(
                  that,
                  [
                    attributes,
                    mwa.proui._.extend(
                        {},
                        options,
                        {
                          // CAUTION:
                          // It is necessary to override success callback to update internal managed
                          // flag instead of achieving the same by using done callback because done
                          // callback will be called after success callback and sync event. This
                          // update requires to be executed before such processing.
                          success: function(model, response, options) {
                            if (options.patch === true) {
                              that._hasChangedWorkflowItems =
                                  WORKFLOW_ITEM_ATTRIBUTE_NAME_ARRAY.some(
                                      function(workflowItemAttributeName, index, array) {
                                        return (
                                            !attributes.hasOwnProperty(workflowItemAttributeName)
                                        );
                                      }
                                  );
                            } else {
                              that._hasChangedWorkflowItems = false;
                            }

                            if (typeof success === 'function') {
                              success(model, response, options);
                            }
                          }
                        }
                    )
                  ]
              );
            },
            /**
             * @see {@link mwa.Model.createEditingMetadataFieldCollection}
             * @public
             * @override
             */
            createEditingMetadataFieldCollection: function() {
              var modelEditingMetadataFieldCollection =
                  mwa.Model.prototype.createEditingMetadataFieldCollection.apply(this, []);
              var length = modelEditingMetadataFieldCollection.length;

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                  modelEditingMetadataFieldCollection.models.concat([
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'name',
                      index: length++,
                      title: mwa.enumeration.Message['SENTENCE_NAME'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                            {
                              type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MAX_LENGTH,
                              value: MAX_LENGTH_FOR_NAME,
                              text:
                                  mwa.enumeration.Message['VALIDATION_MAXIMUM_LENGTH'].replace(
                                      /\{0\}/,
                                      mwa.enumeration.Message['SENTENCE_NUMBER_OF_CHARACTERS'].toLowerCase()
                                  ).replace(
                                      /\{1\}/,
                                      '' + MAX_LENGTH_FOR_NAME
                                  )
                            }
                          ]),
                      isArray: false,
                      isHidden: false,
                      isRequired: true,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: true,
                      canFilter: false,
                      canResize: true
                    }
                  ])
              );
            },
            /**
             * The function to return the result of whether the model has changed regarding workflow
             * items (steps, connectionPorts, connections) since its last save.
             * @public
             *
             * @returns {?boolean}
             * The check result.
             */
            hasChangedWorkflowItems: function() {
              return this._hasChangedWorkflowItems;
            },
            /**
             * The event handler for changing workflow items of model. This function updates
             * internal managed flag according to the value.
             * @protected
             *
             * @param {mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel} model
             * The mwa workflow diagram detail model.
             * @param {*} value
             * The new changed value.
             * @param {object} options
             * The options of change event.
             */
            applyWorkflowItems: function(model, value, options) {
              this._hasChangedWorkflowItems = true;
            }
          }
      );
    })();

    /**
     * The mwa workflow diagram collection.
     * @constructor
     * @inner
     * @alias mwa.model.workflowDiagram.WorkflowDiagramCollection
     * @memberof mwa.model.workflowDiagram
     * @extends {mwa.Collection}
     */
    global.WorkflowDiagramCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.workflowDiagram.WorkflowDiagramCollection
           */
          {
            url: global.WorkflowDiagramModel.prototype.urlRoot,
            model: global.WorkflowDiagramModel,
            comparator: 'name',
            /**
             * The function to operate the models for controlling actual processing.
             * @deprecated since version 2.13.0.
             * @public
             *
             * @param {mwa.enumeration.WorkflowDiagramOperation} operation
             * The enumeration for workflow diagram operation.
             * @param {object} [options]
             * The options of the Ajax request.
             *
             * @returns {object}
             * The deferred object of the request.
             */
            operate: function(operation, options) {
              var message = '';

              var canOperate = false;
              for (var propertyName in mwa.enumeration.WorkflowDiagramOperation) {
                if (mwa.enumeration.WorkflowDiagramOperation.hasOwnProperty(propertyName)) {
                  if (operation === mwa.enumeration.WorkflowDiagramOperation[propertyName]) {
                    canOperate = true;
                    break;
                  }
                }
              }

              if (!canOperate) {
                message = this.alias + '.operate : ' +
                    'The "operation" is required and must be mwa.enumeration.WorkflowDiagramOperation.';

                mwa.Logger.error(message);

                throw Error(message);
              }

              options = mwa.proui._.extend(
                  {},
                  options,
                  {
                    type: 'POST',
                    url: mwa.proui._.result(this, 'url') + '/' + encodeURIComponent(this.pluck('id').join(',')) +
                        '?' + 'operation=' + operation.name,
                    dataType: 'json',
                    contentType: 'application/json;charset=utf-8'
                  }
                  );
              this.wrapCallback(options);

              var xhr = mwa.proui.Backbone.$.ajax();
              if (this.isEmpty()) {
                message = this.alias + '.operate : ' +
                    'The "models" is empty.';

                mwa.Logger.info(message);
              } else {
                xhr = mwa.proui.Backbone.$.ajax(options);
                this.trigger('request', this, xhr, options);
              }

              return xhr;
            }
          }
      );
    })();

    return global;
  })();

  return mwa.model.workflowDiagram;
}));
