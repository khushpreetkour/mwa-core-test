/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.activityCondition = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.activityCondition namespace.
   * @namespace
   */
  mwa.model.activityCondition = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityCondition alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    var ACTIVITY_CONDITION_CONDITION_OPERATOR_ARRAY =
        mwa.proui._.values(mwa.enumeration.ActivityConditionConditionOperator);
    var ACTIVITY_CONDITION_COMPARISON_OPERATOR_ARRAY =
        mwa.proui._.values(mwa.enumeration.ActivityConditionComparisonOperator);

    /**
     * The mwa activity condition model.
     * @constructor
     * @inner
     * @alias mwa.model.activityCondition.ActivityConditionModel
     * @memberof mwa.model.activityCondition
     * @extends {mwa.Model}
     */
    global.ActivityConditionModel = (function() {
      var validateValue = function(value, attr, computedState) {
        // CAUTION:
        // This function expects that "this" variable is bound to model instance.

        var msg = null;

        var pipelineWorkflowStepViewModelCollection =
            this.get('pipelineWorkflowStepViewModelCollection');
        if (typeof value === 'string' &&
            pipelineWorkflowStepViewModelCollection instanceof
            mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection) {
          var workflowStepId = mwa.util.ActivityCondition.getWorkflowStepId(value);
          if (typeof workflowStepId === 'string' &&
              !(pipelineWorkflowStepViewModelCollection.get(workflowStepId) instanceof
              mwa.view.workflowStep.WorkflowStepViewModel)) {
            msg = 'The "' + attr + '" must be constructed by pipeline workflow step.';
          }
        }

        return msg;
      };

      return mwa.Model.extend(
          /**
           * @lends mwa.model.activityCondition.ActivityConditionModel
           */
          {
            alias: 'mwa.model.activityCondition.ActivityConditionModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activity-conditions' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    conditionOperator: null,
                    leftValue: null,
                    comparisonOperator: null,
                    rightValue: null,
                    pipelineWorkflowStepViewModelCollection: null // Client Data
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  conditionOperator: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = 'The "' + attr + '" must be mwa.enumeration.ActivityConditionConditionOperator.';
                        for (var propertyName in mwa.enumeration.ActivityConditionConditionOperator) {
                          if (mwa.enumeration.ActivityConditionConditionOperator.hasOwnProperty(propertyName)) {
                            // INFORMATION:
                            // Don't check whether the instance is matching with enumeration object
                            // because it is difficult for development phase to fit it.
                            if (mwa.proui._.isEqual(value, mwa.enumeration.ActivityConditionConditionOperator[propertyName])) {
                              msg = null;
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  leftValue: [
                    {
                      // INFORMATION:
                      // There is a case that any value cannot be set when pipeline is updated.
                      required: false
                    },
                    {
                      type: 'string'
                    },
                    {
                      fn: function(value, attr, computedState) {
                        return mwa.proui._.bind(validateValue, this)(value, attr, computedState);
                      }
                    }
                  ],
                  comparisonOperator: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = 'The "' + attr + '" must be mwa.enumeration.ActivityConditionComparisonOperator.';
                        for (var propertyName in mwa.enumeration.ActivityConditionComparisonOperator) {
                          if (mwa.enumeration.ActivityConditionComparisonOperator.hasOwnProperty(propertyName)) {
                            // INFORMATION:
                            // Don't check whether the instance is matching with enumeration object
                            // because it is difficult for development phase to fit it.
                            if (mwa.proui._.isEqual(value, mwa.enumeration.ActivityConditionComparisonOperator[propertyName])) {
                              msg = null;
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  rightValue: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    },
                    {
                      fn: function(value, attr, computedState) {
                        return mwa.proui._.bind(validateValue, this)(value, attr, computedState);
                      }
                    }
                  ],
                  pipelineWorkflowStepViewModelCollection: [
                    {
                      required: false
                    },
                    // CAUTION:
                    // It is necessary to use fn validator instead of instance validator because
                    // the class isn't been defined yet when this class is defined.
                    {
                      fn: function(value, attr, computedState) {
                        return (
                            (
                                typeof value === 'undefined' || value == null ||
                                value instanceof
                                mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection
                            ) ?
                                null :
                                'The "' + attr + '" must be an instance of ' +
                                'mwa.view.workflowStep.PipelineWorkflowStepViewModelCollection.'
                        );
                      }
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.Model.prototype.parse.apply(this, [response, options]);

              response.conditionOperator =
                  mwa.proui._.findWhere(
                      ACTIVITY_CONDITION_CONDITION_OPERATOR_ARRAY,
                      {operator: response.conditionOperator}
                  );

              response.comparisonOperator =
                  mwa.proui._.findWhere(
                      ACTIVITY_CONDITION_COMPARISON_OPERATOR_ARRAY,
                      {operator: response.comparisonOperator}
                  );

              return response;
            },
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              var json = this.toJSON();
              return {
                conditionOperator: json.conditionOperator.operator,
                leftValue: json.leftValue,
                comparisonOperator: json.comparisonOperator.operator,
                rightValue: json.rightValue
              };
            }
          }
      );
    })();

    /**
     * The mwa activity condition collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityCondition.ActivityConditionCollection
     * @memberof mwa.model.activityCondition
     * @extends {mwa.Collection}
     */
    global.ActivityConditionCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activityCondition.ActivityConditionCollection
           */
          {
            url: global.ActivityConditionModel.prototype.urlRoot,
            model: global.ActivityConditionModel,
            /**
             * The function to convert to object.
             * @public
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function() {
              return this.map(
                  function(activityConditionModel, index, array) {
                    return activityConditionModel.toObject();
                  }
              );
            }
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityCondition;
}));
