/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.fileSystemEntity = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.fileSystemEntity namespace.
   * @namespace
   */
  mwa.model.fileSystemEntity = (function() {
    'use strict';

    /**
     * Defines mwa.model.fileSystemEntity alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa file system entity model.
     * @constructor
     * @inner
     * @alias mwa.model.fileSystemEntity.FileSystemEntityModel
     * @memberof mwa.model.fileSystemEntity
     * @extends {mwa.Model}
     */
    global.FileSystemEntityModel = (function() {
      return mwa.Model.extend(
          /**
           * @lends mwa.model.fileSystemEntity.FileSystemEntityModel
           */
          {
            alias: 'mwa.model.fileSystemEntity.FileSystemEntityModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'file-system-entities' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    property: null,   // Client Data (same as id)
                    name: null,
                    uri: null,
                    nativePath: null,
                    label: null       // Client Data (same as nativePath)
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  property: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  uri: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  nativePath: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  label: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.Model.prototype.parse.apply(this, [response, options]);

              // CAUTION: NVX-6149 (https://www.tool.sony.biz/jira/browse/NVX-6149)
              //          NVX-6150 (https://www.tool.sony.biz/jira/browse/NVX-6150)
              // The following attributes are necessary for displaying the structure using metadata
              // editor.
              response.property = response.id;
              response.label = response.nativePath;

              return response;
            },
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa file system entity collection.
     * @constructor
     * @inner
     * @alias mwa.model.fileSystemEntity.FileSystemEntityCollection
     * @memberof mwa.model.fileSystemEntity
     * @extends {mwa.Collection}
     */
    global.FileSystemEntityCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.fileSystemEntity.FileSystemEntityCollection
           */
          {
            url: global.FileSystemEntityModel.prototype.urlRoot,
            model: global.FileSystemEntityModel,
            comparator: 'name'
          }
      );
    })();

    return global;
  })();

  return mwa.model.fileSystemEntity;
}));
