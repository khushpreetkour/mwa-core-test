/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.activityParameterTypeValue = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.activityParameterTypeValue namespace.
   * @namespace
   */
  mwa.model.activityParameterTypeValue = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityParameterTypeValue alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa activity parameter type value model.
     * @constructor
     * @inner
     * @alias mwa.model.activityParameterTypeValue.ActivityParameterTypeValueModel
     * @memberof mwa.model.activityParameterTypeValue
     * @extends {mwa.Model}
     */
    global.ActivityParameterTypeValueModel = (function() {
      return mwa.Model.extend(
          /**
           * @lends mwa.model.activityParameterTypeValue.ActivityParameterTypeValueModel
           */
          {
            alias: 'mwa.model.activityParameterTypeValue.ActivityParameterTypeValueModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ?
                  'activity-parameter-types/' + this.typeName + '/values' :
                  resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            typeName: null,
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    name: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.initialize}
             * @protected
             * @override
             *
             * @param {object} attributes
             * @param {string} [attributes.id = null]
             * The string of ID.
             * @param {string} [attributes.name = null]
             * The string of name.
             *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              this.typeName = (options || {}).typeName || null;

              mwa.Model.prototype.initialize.apply(this, [attributes, options]);
            },
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            }
          }
      );
    })();

    /**
     * The mwa activity parameter type value collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityParameterTypeValue.ActivityParameterTypeValueCollection
     * @memberof mwa.model.activityParameterTypeValue
     * @extends {mwa.Collection}
     */
    global.ActivityParameterTypeValueCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activityParameterTypeValue.ActivityParameterTypeValueCollection
           */
          {
            url: global.ActivityParameterTypeValueModel.prototype.urlRoot,
            model: global.ActivityParameterTypeValueModel,
            comparator: 'name',
            typeName: null,
            /**
             * @see {@link mwa.Collection.initialize}
             * @protected
             * @override
             */
            initialize: function(models, options) {
              this.typeName = (options || {}).typeName || null;

              mwa.Collection.prototype.initialize.apply(this, [models, options]);
            }
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityParameterTypeValue;
}));
