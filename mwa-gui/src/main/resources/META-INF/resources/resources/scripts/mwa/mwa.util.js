/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.enumeration == null) ? './mwa.enumeration' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.enumeration == null) ? require('./mwa.enumeration') : {}
    );
  } else {
    root.mwa.util = factory(
        root.mwa,
        root.mwa.enumeration
    );
  }
}(this, function(base, enumeration) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.enumeration namespace.
  if (mwa.enumeration == null) {
    mwa.enumeration = enumeration;
  }

  /**
   * The mwa.util namespace.
   * @namespace
   */
  mwa.util = (function() {
    'use strict';

    /**
     * Defines mwa.util alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The utility for activity condition.
     * @readonly
     * @enum {object}
     * @alias mwa.util.ActivityCondition
     * @memberof mwa.util
     */
    global.ActivityCondition = (function(base) {
      /**
       * Defines mwa.util.ActivityCondition alias name.
       */
      var global = {};

      /**
       * The function to confirm whether specified value has connection.
       * @public
       *
       * @param {?string} value
       * The string of value to be confirmed.
       *
       * @returns {!boolean}
       * The confirmation result.
       */
      global.hasConnection = function(value) {
        return (value || '').lastIndexOf('$', 0) !== -1;
      };

      /**
       * The function to get workflow step ID from specified value.
       * @public
       *
       * @param {?string} value
       * The string of value to be used for detecting workflow step ID.
       *
       * @returns {?string}
       * The workflow step ID.
       */
      global.getWorkflowStepId = function(value) {
        return (mwa.util.ActivityCondition.hasConnection(value)) ?
            value.slice(1, value.indexOf('.')) :
            null;
      };

      return global;
    })(global);

    /**
     * The utility for workflow step.
     * @readonly
     * @enum {object}
     * @alias mwa.util.WorkflowStep
     * @memberof mwa.util
     */
    global.WorkflowStep = (function(base) {
      /**
       * Defines mwa.util.WorkflowStep alias name.
       */
      var global = {};

      var workflowStepTypeTemplateNameArray =
          mwa.proui._.pluck(
              mwa.proui._.values(mwa.enumeration.WorkflowStepType), 'templateName'
          );

      /**
       * The function to validate whether specified activity is processing workflow step.
       * @public
       *
       * @param {!mwa.model.activity.ActivityModel} activityModel
       * The activity model to be validated.
       *
       * @returns {!boolean}
       * The validation result.
       */
      global.isProcessing = function(activityModel) {
        var isProcessing = false;

        if (activityModel instanceof mwa.model.activity.ActivityModel) {
          var activityType = activityModel.get('type');

          isProcessing =
              workflowStepTypeTemplateNameArray.indexOf(activityModel.get('name')) === -1 &&
              activityType !== mwa.enumeration.ActivityType.START &&
              activityType !== mwa.enumeration.ActivityType.END;
        }

        return isProcessing;
      };

      return global;
    })(global);

    return global;
  })();

  return mwa.util;
}));
