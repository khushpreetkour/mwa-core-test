/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.activity == null) ? './mwa.model.activity' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.activity == null) ? require('./mwa.model.activity') : {}
    );
  } else {
    root.mwa.model.workflow = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.activity
    );
  }
}(this, function(base, subBase, activity) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.activity namespace.
  if (mwa.model.activity == null) {
    mwa.model.activity = activity;
  }

  /**
   * The mwa.model.workflow namespace.
   * @namespace
   */
  mwa.model.workflow = (function() {
    'use strict';

    /**
     * Defines mwa.model.workflow alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow model.
     * @constructor
     * @inner
     * @alias mwa.model.workflow.WorkflowModel
     * @memberof mwa.model.workflow
     * @extends {mwa.model.workflowDiagram.WorkflowDiagramModel}
     * @extends {mwa.model.activity.ActivityModel}
     */
    global.WorkflowModel = (function() {
      return mwa.model.workflowDiagram.WorkflowDiagramModel.extend(
          mwa.proui._.omit(mwa.model.activity.ActivityModel.prototype, 'constructor')
      ).extend(
          /**
           * @lends mwa.model.workflow.WorkflowModel
           */
          {
            alias: 'mwa.model.workflow.WorkflowModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'workflows' : resource;
              return mwa.model.activity.ActivityModel.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.model.workflowDiagram.WorkflowDiagramModel.prototype, 'defaults'),
                  mwa.proui._.result(mwa.model.activity.ActivityModel.prototype, 'defaults'),
                  {
                    // model.workflowDiagram.WorkflowDiagramModel
                    // If you need to change default values, add here.
                    // mwa.model.activity.ActivityModel
                    type: mwa.enumeration.ActivityType.WORKFLOW,
                    // mwa.model.workflow.WorkflowModel
                    diagramId: null,
                    predefineFlag: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.validation,
                mwa.model.activity.ActivityModel.prototype.validation,
                {
                  // model.workflowDiagram.WorkflowDiagramModel
                  // If you need to change validators, add here.
                  // mwa.model.activity.ActivityModel
                  type: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = 'The "' + attr + '" must be mwa.enumeration.ActivityType.WORKFLOW.';
                        // INFORMATION:
                        // Don't check whether the instance is matching with enumeration object
                        // because it is difficult for development phase to fit it.
                        if (mwa.proui._.isEqual(value, mwa.enumeration.ActivityType.WORKFLOW)) {
                          msg = null;
                        }
                        return msg;
                      }
                    }
                  ],
                  // mwa.model.workflow.WorkflowModel
                  diagramId: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  predefineFlag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.model.workflowDiagram.WorkflowDiagramModel.initialize}
             * @see {@link mwa.model.activity.ActivityModel.initialize}
             * @protected
             * @override
             */
            initialize: function(attributes, options) {
              mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.initialize.apply(
                  this, [attributes, options]
              );
              mwa.model.activity.ActivityModel.prototype.initialize.apply(
                  this, [attributes, options]
              );
            },
            /**
             * @see {@link mwa.model.workflowDiagram.WorkflowDiagramModel.parse}
             * @see {@link mwa.model.activity.ActivityModel.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.parse.apply(
                  this, [response, options]
                  );
              response = mwa.model.activity.ActivityModel.prototype.parse.apply(
                  this, [response, options]
                  );

              return response;
            },
            /**
             * @see {@link mwa.model.workflowDiagram.WorkflowDiagramModel.listen}
             * @see {@link mwa.model.activity.ActivityModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.model.workflowDiagram.WorkflowDiagramModel.prototype.listen.apply(this, []);
              mwa.model.activity.ActivityModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.model.workflowDiagram.WorkflowDiagramModel.createEditingMetadataFieldCollection}
             * @see {@link mwa.model.activity.ActivityModel.createEditingMetadataFieldCollection}
             * @public
             * @override
             */
            createEditingMetadataFieldCollection: function() {
              var workflowDiagramEditingMetadataFieldCollection =
                  mwa.model.workflowDiagram.WorkflowDiagramModel.prototype
                      .createEditingMetadataFieldCollection.apply(this, []);
              // CAUTION:
              // It is necessary to remove a metadata field for name attribute because metadata
              // field for alias attribute that is provided by createEditingMetadataFieldCollection
              // function of activity model is the substitution for it in workflow model.
              workflowDiagramEditingMetadataFieldCollection.remove('name');
              var activityEditingMetadataFieldCollection =
                  new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                      mwa.model.activity.ActivityModel.prototype
                          .createEditingMetadataFieldCollection.apply(this, [])
                          .map(
                              function(metadataFieldModel, index, array) {
                                return metadataFieldModel.set(
                                    'index',
                                    workflowDiagramEditingMetadataFieldCollection.length + index
                                );
                              }
                          )
                  );
              var length =
                  workflowDiagramEditingMetadataFieldCollection.length +
                  activityEditingMetadataFieldCollection.length;

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                  workflowDiagramEditingMetadataFieldCollection.models.concat(
                      activityEditingMetadataFieldCollection.models
                  ).concat([
                    // If you need new editing metadata fields, add here.
                  ])
              );
            },
            /**
             * @see {@link mwa.model.workflowDiagram.WorkflowDiagramModel.hasChangedWorkflowItems}
             * @public
             * @override
             * // TODO: NVXN-1733 (https://acropolis.atlassian.net/browse/NVXN-1733)
             */
            hasChangedWorkflowItems: function() {
              return false;
            }
          }
      );
    })();

    /**
     * The mwa workflow collection.
     * @constructor
     * @inner
     * @alias mwa.model.workflow.WorkflowCollection
     * @memberof mwa.model.workflow
     * @extends {mwa.model.workflowDiagram.WorkflowDiagramCollection}
     * @extends {mwa.model.activity.ActivityCollection}
     */
    global.WorkflowCollection = (function() {
      return mwa.model.workflowDiagram.WorkflowDiagramCollection.extend(
          mwa.proui._.omit(mwa.model.activity.ActivityCollection.prototype, 'constructor')
      ).extend(
          /**
           * @lends mwa.model.workflow.WorkflowCollection
           */
          {
            url: global.WorkflowModel.prototype.urlRoot,
            model: global.WorkflowModel,
            comparator: 'alias'
          }
      );
    })();

    return global;
  })();

  return mwa.model.workflow;
}));
