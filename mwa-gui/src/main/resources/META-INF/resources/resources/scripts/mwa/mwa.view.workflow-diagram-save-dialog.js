/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.model == null || mwa.model.workflowDiagram == null) ? './mwa.model.workflow-diagram' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.model == null || mwa.model.workflowDiagram == null) ? require('./mwa.model.workflow-diagram') : {}
    );
  } else {
    root.mwa.view.workflowDiagramSaveDialog = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.model.workflowDiagram
    );
  }
}(this, function(base, subBase, workflowDiagram) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.model.workflowDiagram namespace.
  if (mwa.model.workflowDiagram == null) {
    mwa.model.workflowDiagram = workflowDiagram;
  }

  /**
   * The mwa.view.workflowDiagramSaveDialog namespace.
   * @namespace
   */
  mwa.view.workflowDiagramSaveDialog = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowDiagramSaveDialog alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow diagram save dialog view model.
     * @deprecated since version 2.11.0
     * @constructor
     * @inner
     * @alias mwa.view.workflowDiagramSaveDialog.WorkflowDiagramSaveDialogViewModel
     * @memberof mwa.view.workflowDiagramSaveDialog
     * @extends {mwa.ViewModel}
     */
    global.WorkflowDiagramSaveDialogViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowDiagramSaveDialog.WorkflowDiagramSaveDialogViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    title: null,
                    workflowDiagramModel: null,
                    save: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  title: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  workflowDiagramModel: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.workflowDiagram.WorkflowDiagramModel
                    }
                  ],
                  save: [
                    {
                      required: false
                    },
                    {
                      type: 'function'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             * *
             * @param {object} options
             * The options for initialization.
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, [attributes, options]);

              for (var propertyName in attributes) {
                if (attributes.hasOwnProperty(propertyName)) {
                  if ((propertyName === 'save') &&
                      mwa.proui._.isFunction(attributes[propertyName])) {
                    this[propertyName] = attributes[propertyName];
                  }
                }
              }
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The callback function. This function is called when save button is pressed.
             * @public
             *
             * @param {mwa.model.workflowDiagram.WorkflowDiagramModel} workflowDiagramModel
             * The edited workflow diagram. If save button is pressed, this instance is same as
             * workflowDiagramModel attribute of view model. If save as new button is pressed, this
             * instance is a new instance without ID and different from workflowDiagramModel
             * attribute of view model.
             */
            save: function(workflowDiagramModel) {
            }
          }
      );
    })();

    /**
     * The mwa workflow diagram save dialog view.
     * @deprecated since version 2.11.0
     * @constructor
     * @inner
     * @alias mwa.view.workflowDiagramSaveDialog.WorkflowDiagramSaveDialogView
     * @memberof mwa.view.workflowDiagramSaveDialog
     * @extends {mwa.View}
     */
    global.WorkflowDiagramSaveDialogView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowDiagramSaveDialog.WorkflowDiagramSaveDialogView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-diagram-save-dialog"></div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The function to execute common processing for text change and set the value to
             * itemAttributes attribute of view model.
             *
             * @private
             * @param {*} value
             * The value to be set to itemAttributes attribute of view model.
             */
            _changeText: function(value, key) {
              mwa.proui.util.Object.setValueByDsv(
                  this._itemAttributes,
                  key,
                  value
              );
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              mwa.View.prototype.render.apply(this, []);

              that._itemAttributes = {};
              that.viewModels = {};

              that.viewModels.saveCommandButtonViewModel =
                  new mwa.proui.commandButton.CommandButtonViewModel({
                    text: mwa.enumeration.Message['TITLE_SAVE'],
                    isStrong: true,
                    type: mwa.proui.commandButton.ButtonType.CTA,
                    minWidth: 100,
                    command: function() {
                      var workflowDiagramModel = that.options.viewModel.get('workflowDiagramModel');
                      workflowDiagramModel.set(that._itemAttributes);
                      that.options.viewModel.save(workflowDiagramModel);
                    }
                  });

              that.viewModels.saveAsNewCommandButtonViewModel =
                  new mwa.proui.commandButton.CommandButtonViewModel({
                    text: mwa.enumeration.Message['TITLE_SAVE_AS_NEW'],
                    isStrong: false,
                    type: mwa.proui.commandButton.ButtonType.CTA,
                    minWidth: 100,
                    command: function() {
                      // INFORMATION:
                      // This codes mean to pass a new instance that has been edited without ID to
                      // save it as a new workflow diagram.
                      var workflowDiagramModel = new mwa.model.workflowDiagram.WorkflowDiagramModel(
                          that.options.viewModel.get('workflowDiagramModel').attributes
                      );
                      workflowDiagramModel.set(that._itemAttributes);
                      workflowDiagramModel.set(workflowDiagramModel.idAttribute, null);
                      that.options.viewModel.save(workflowDiagramModel);
                    }
                  });

              that.viewModels.cancelCommandButtonViewModel =
                  new mwa.proui.commandButton.CommandButtonViewModel({
                    text: mwa.enumeration.Message['TITLE_CANCEL'],
                    isStrong: true,
                    type: mwa.proui.commandButton.ButtonType.ALT,
                    minWidth: 100,
                    command: function() {
                      that.views.dialogView.options.viewModel.set('isOpen', false);
                    }
                  });

              that.views.dialogView = new mwa.proui.dialog.DialogView({
                viewModel: new mwa.proui.dialog.DialogViewModel({
                  canMinimize: false,
                  canMaximize: false,
                  canRestoreDown: false,
                  hasCloseButton: true,
                  title: mwa.enumeration.Message['TITLE_SAVE'],
                  width: 450,
                  height: 220,
                  commandButtonViewModelArray: [
                    that.viewModels.saveCommandButtonViewModel,
                    that.viewModels.saveAsNewCommandButtonViewModel,
                    that.viewModels.cancelCommandButtonViewModel
                  ].filter(
                      function(commandButtonViewModel, index, array) {
                        switch (commandButtonViewModel) {
                          case that.viewModels.saveAsNewCommandButtonViewModel:
                            return !that.options.viewModel.get('workflowDiagramModel').isNew();
                          default:
                            return true;
                        }
                      }
                  ),
                  content: new mwa.proui.dialog.DialogContentModel({
                    viewClass: mwa.proui.textBox.TextBoxView,
                    viewOptions: {
                      viewModel: new mwa.proui.textBox.TextBoxViewModel({
                        isDisabled: false,
                        title: mwa.enumeration.Message['SENTENCE_NAME'],
                        titleWidth: 80,
                        text: that.options.viewModel.get('workflowDiagramModel').get('name'),
                        // CAUTION:
                        // The validateText callback is called in initialization. Therefore, it isn't necessary to
                        // set initial value of isDisabled attribute for Save and Save as New buttons.
                        validateText: function(text) {

                          var validationError =
                              that.options.viewModel.get('workflowDiagramModel').validate(
                                  {name: text}, null
                              );
                          var isValid =
                              typeof validationError === 'undefined' || validationError == null;

                          that.viewModels.saveCommandButtonViewModel.set('isDisabled', !isValid);
                          that.viewModels.saveAsNewCommandButtonViewModel.set('isDisabled', !isValid);

                          return validationError;
                        },
                        changeText: function(text) {
                          that._itemAttributes.name = text;
                        }
                      })
                    }
                  })
                })
              });

              return this;
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowDiagramSaveDialog;
}));
