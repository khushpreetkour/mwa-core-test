/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

// INFORMATION:
// The following steps are an overview for adding a new Model.
// 2. Add the path of new JavaScript file to
//    src/main/resources/META-INF/resources/resources/gulpfile.js for build.
//    in consideration of the dependency.

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.activityProfileGroup == null) ? './mwa.model.activity-profile-group' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.activityProfileGroup == null) ? require('./mwa.model.activity-profile-group') : {}
    );
  } else {
    root.mwa.model.activityProfile = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.activityProfileGroup
    );
  }
}(this, function(base, subBase, activityProfileGroup) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.activityProfileGroup namespace.
  if (mwa.model.activityProfileGroup == null) {
    mwa.model.activityProfileGroup = activityProfileGroup;
  }

  /**
   * The mwa.model.activityProfile namespace.
   * @namespace
   */
  mwa.model.activityProfile = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityProfile alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The special required mark information.
     * @constant
     * @type {object}
     * @default
     * @alias mwa.model.activityProfile.SPECIAL_REQUIRED_MARKS
     * @memberof mwa.model.activityProfile
     */
    global.SPECIAL_REQUIRED_MARKS = {
      string: ' (*)',
      regExp: new RegExp(' \\(\\*\\)$')
    };

    /**
     * The mwa activity profile model.
     * @constructor
     * @inner
     * @alias mwa.model.activityProfile.ActivityProfileModel
     * @memberof mwa.model.activityProfile
     * @extends {mwa.Model}
     */
    global.ActivityProfileModel = (function() {
      var MAX_LENGTH_FOR_NAME = 256;

      return mwa.Model.extend(
          /**
           * @lends mwa.model.activityProfile.ActivityProfileModel
           */
          {
            alias: 'mwa.model.activityProfile.ActivityProfileModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activity-profiles' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              var activityInstanceModelDefaults =
                  mwa.proui._.result(
                      mwa.model.activityInstance.ActivityInstanceModel.prototype,
                      'defaults'
                  );

              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    groupId: null,
                    templateId: null,
                    name: null,
                    icon: mwa.enumeration.Icon.TASK_PROFILE.dataUri,
                    description: null,
                    priority: activityInstanceModelDefaults.priority,
                    optionalFieldEditableFlag: true,
                    schedulableFlag: false,
                    parameter: null,
                    createTime: -1,
                    updateTime: -1,
                    activity: null                           // Client Data
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  groupId: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  templateId: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    },
                    {
                      maxLength: MAX_LENGTH_FOR_NAME
                    }
                  ],
                  icon: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  description: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  priority: [
                    {
                      required: false
                    },
                    {
                      type: 'number'
                    },
                    {
                      min: 0
                    },
                    {
                      max: 100
                    }
                  ],
                  optionalFieldEditableFlag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  schedulableFlag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  parameter: [
                    {
                      required: true
                    },
                    {
                      type: 'object'
                    }
                  ],
                  createTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  updateTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  activity: [
                    {
                      required: false
                    },
                    {
                      type: 'object'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.Model.createEditingMetadataFieldCollection}
             * @public
             * @override
             */
            createEditingMetadataFieldCollection: function() {
              // INFORMATION:
              // It isn't necessary to change that maximum length setting for name attribute because
              // it is same as ActivityProfileGroupModel.
              var activityProfileGroupEditingMetadataFieldCollection =
                  new mwa.model.activityProfileGroup.ActivityProfileGroupModel(
                      null,
                      {validate: false}
                  ).createEditingMetadataFieldCollection();
              var activityModel =
                  new mwa.model.activity.ActivityModel(
                      this.get('activity'),
                      {parse: true, validate: false}
                  );
              var updateParameterMetadataFieldModel = function(metadataFieldModel, index, array) {
                return metadataFieldModel.set({
                  index:
                      activityProfileGroupEditingMetadataFieldCollection.length + index,
                  // CAUTION: NVX-6939 (https://www.tool.sony.biz/jira/browse/NVX-6939)
                  // The all parameter metadata fields should be always optional to be able to save
                  // activity profile anytime. And it is important to display the special required
                  // mark to be able to understand the original setting.
                  title:
                      metadataFieldModel.get('title') +
                      (
                          (metadataFieldModel.get('isRequired')) ?
                              mwa.model.activityProfile.SPECIAL_REQUIRED_MARKS.string :
                              ''
                      ),
                  isRequired: false,
                  // CAUTION: NVX-7046 (https://www.tool.sony.biz/jira/browse/NVX-7046)
                  // It is necessary to consider about the children parameter metadata fields to
                  // treat all parameter metadata fields as optional.
                  metadataFields:
                      mwa.proui._.mapObject(
                          metadataFieldModel.get('metadataFields'),
                          function(value, key) {
                            return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                                value.map(updateParameterMetadataFieldModel).map(
                                    function(metadataFieldModel, index, array) {
                                      // CAUTION:
                                      // It is important to re-set the index that was set by
                                      // updateParameterMetadataFieldModel function without
                                      // considering about anything because the index always should
                                      // be started from 0 for children metadata fields.
                                      return metadataFieldModel.set('index', index);
                                    }
                                )
                            );
                          }
                      )
                });
              };
              var activityParameterMetadataFieldCollection =
                  new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                      activityModel.createParameterMetadataFieldCollection().map(
                          updateParameterMetadataFieldModel
                      )
                  );
              var length = activityProfileGroupEditingMetadataFieldCollection.length;

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                  activityProfileGroupEditingMetadataFieldCollection.models.concat([
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'priority',
                      index: length++,
                      title: mwa.enumeration.Message['SENTENCE_PRIORITY'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.LONG,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                            {
                              type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.RANGE,
                              value: [0, 100],
                              text:
                                  mwa.enumeration.Message['VALIDATION_IS_IN_RANGE'].replace(
                                      /\{0\}/,
                                      (
                                          ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                              ' ' : ''
                                      ) +
                                      '0' +
                                      (
                                          ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                              ' ' : ''
                                      )
                                  ).replace(
                                      /\{1\}/,
                                      (
                                          ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                              ' ' : ''
                                      ) +
                                      '100' +
                                      (
                                          ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                              ' ' : ''
                                      )
                                  )
                            }
                          ]),
                      isArray: false,
                      isHidden: false,
                      isRequired: false,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: true,
                      canFilter: false,
                      canResize: true
                    },
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'optionalFieldEditableFlag',
                      index: length++,
                      title: mwa.enumeration.Message['FOOTNOTE_MAKE_OPTIONAL_FIELDS_CONFIGURABLE_WHEN_EXECUTION'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.BOOLEAN,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                      isArray: false,
                      // CAUTION: NVX-6139 (https://www.tool.sony.biz/jira/browse/NVX-6139)
                      // It is necessary to compare type attribute of activity model with WORKFLOW,
                      // not TASK, because there is a case that activity model has different type
                      // (e.g. TASK, START) if it is task.
                      isHidden: activityModel.get('type') !== mwa.enumeration.ActivityType.WORKFLOW,
                      isRequired: true,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: true,
                      canFilter: false,
                      canResize: true
                    },
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'schedulableFlag',
                      index: length++,
                      title: mwa.enumeration.Message['FOOTNOTE_MAKE_REQUEST_SCHEDULABLE_WHEN_EXECUTION'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.BOOLEAN,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                      isArray: false,
                      // CAUTION: NVX-6139 (https://www.tool.sony.biz/jira/browse/NVX-6139)
                      // It is necessary to compare type attribute of activity model with WORKFLOW,
                      // not TASK, because there is a case that activity model has different type
                      // (e.g. TASK, START) if it is task.
                      isHidden: activityModel.get('type') !== mwa.enumeration.ActivityType.WORKFLOW,
                      isRequired: true,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: true,
                      canFilter: false,
                      canResize: true
                    },
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'description',
                      index: length++,
                      title: mwa.enumeration.Message['SENTENCE_DESCRIPTION'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.TEXT,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                      isArray: false,
                      isHidden: false,
                      isRequired: false,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: true,
                      canFilter: false,
                      canResize: true
                    }
                  ]).concat(
                      (activityParameterMetadataFieldCollection.isEmpty()) ?
                          mwa.proui._.pairs(this.get('parameter')).map(
                              function(parameterArray, index, array) {
                                var key = parameterArray[0];

                                return {
                                  // prouiExt.metadataEditor.MetadataFieldModel
                                  key: 'parameter' + '.' + key,
                                  index: length++,
                                  title: key,
                                  type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                                  valueCollection: null,
                                  referenceValueCollection: null,
                                  metadataFieldValidatorCollection:
                                      new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                                  isArray: false,
                                  isHidden: false,
                                  // CAUTION:
                                  // The all parameter metadata fields should be always optional to be
                                  // able to save activity profile anytime.
                                  isRequired: false,
                                  isReadOnly: false,
                                  // prouiExt.metadataEditorListView.MetadataFieldModel
                                  value: null,
                                  canShift: true,
                                  canSort: false,
                                  canFilter: false,
                                  canResize: true
                                };
                              }
                          ) :
                          // CAUTION:
                          // It is necessary to update the index of activity parameter metadata
                          // field models by incrementing length value, in order to display the
                          // common metadata fields first and then the parameter metadata fields.
                          activityParameterMetadataFieldCollection.map(
                              function(activityParameterMetadataFieldModel, index, array) {
                                return activityParameterMetadataFieldModel.set('index', length++);
                              }
                          )
                  ).concat([
                    // If you need new editing metadata fields, add here.
                  ])
              );
            },
            /**
             * @see {@link mwa.Model.createEditingMetadataGroupCollection}
             * @public
             * @override
             */
            createEditingMetadataGroupCollection: function() {
              return new mwa.prouiExt.metadataEditorDialog.MetadataGroupCollection([
                {
                  regExp: new RegExp('^((?!(parameter)).)*$'),
                  metadataGroupCollection: new mwa.prouiExt.metadataEditor.MetadataGroupCollection([
                    {
                      title: null,
                      regExp: new RegExp('^((?!(parameter)).)*$'),
                      isHidden: false,
                      isExpanded: true
                    }
                  ])
                },
                {
                  regExp: new RegExp('^parameter.*$'),
                  metadataGroupCollection: new mwa.prouiExt.metadataEditor.MetadataGroupCollection([
                    {
                      title: null,
                      regExp: new RegExp('^parameter.*$'),
                      isHidden: false,
                      isExpanded: true
                    }
                  ])
                }
              ]);
            }
          }
      );
    })();

    /**
     * The mwa activity profile collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityProfile.ActivityProfileCollection
     * @memberof mwa.model.activityProfile
     * @extends {mwa.Collection}
     */
    global.ActivityProfileCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activityProfile.ActivityProfileCollection
           */
          {
            url: global.ActivityProfileModel.prototype.urlRoot,
            model: global.ActivityProfileModel,
            comparator: 'name'
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityProfile;
}));
