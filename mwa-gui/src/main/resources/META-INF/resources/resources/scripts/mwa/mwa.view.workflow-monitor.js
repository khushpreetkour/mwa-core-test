/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.model == null || mwa.model.activityInstance == null) ? './mwa.model.activity-instance' : {},
      (mwa.model == null || mwa.model.workflow == null) ? './mwa.model.workflow' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.model == null || mwa.model.activityInstance == null) ? require('./mwa.model.activity-instance') : {},
        (mwa.model == null || mwa.model.workflow == null) ? require('./mwa.model.workflow') : {}
    );
  } else {
    root.mwa.view.workflowMonitor = factory(
        root.mwa,
        root.mwa.view,
        mwa.model.activityInstance,
        root.mwa.model.workflow
    );
  }
}(this, function(base, subBase, activityInstance, workflow) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.model.activityInstance namespace.
  if (mwa.model.activityInstance == null) {
    mwa.model.activityInstance = activityInstance;
  }
  // To initialize the mwa.model.workflow namespace.
  if (mwa.model.workflow == null) {
    mwa.model.workflow = workflow;
  }

  /**
   * The mwa.view.workflowMonitor namespace.
   * @namespace
   */
  mwa.view.workflowMonitor = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowMonitor alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa workflow monitor view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowMonitor.WorkflowMonitorViewModel
     * @memberof mwa.view.workflowMonitor
     * @extends {mwa.ViewModel}
     */
    global.WorkflowMonitorViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowMonitor.WorkflowMonitorViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    workflowInstanceCollection: new mwa.model.activityInstance.ActivityInstanceCollection(),
                    selectedWorkflowInstanceCollection: new mwa.model.activityInstance.ActivityInstanceCollection(),
                    lastSelectedWorkflowInstanceModel: null,
                    select: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  workflowInstanceCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityInstance.ActivityInstanceCollection
                    }
                  ],
                  selectedWorkflowInstanceCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activityInstance.ActivityInstanceCollection
                    }
                  ],
                  lastSelectedWorkflowInstanceModel: [
                    {
                      required: false
                    },
                    {
                      instance: mwa.model.activityInstance.ActivityInstanceModel
                    }
                  ],
                  select: [
                    {
                      required: false
                    },
                    {
                      type: 'function'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.initialize}
             * @protected
             * @override
             */
            initialize: function(attributes, options) {
              mwa.ViewModel.prototype.initialize.apply(this, []);

              for (var propertyName in attributes) {
                if (attributes.hasOwnProperty(propertyName)) {
                  if ((propertyName === 'select') &&
                      mwa.proui._.isFunction(attributes[propertyName])) {
                    this[propertyName] = attributes[propertyName];
                  }
                }
              }
            },
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The callback function. This function is called when workflow instance is selected or
             * deselected.
             * @public
             *
             * @param {mwa.model.activityInstance.ActivityInstanceModel} workflowInstanceModel
             * The selected or deselected workflow instance model instance.
             * @param {!boolean} isSelected
             * The flag whether workflow instance is selected.
             */
            select: function(workflowInstanceModel, isSelected) {
            }
          }
      );
    })();

    /**
     * The mwa workflow monitor view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowMonitor.WorkflowMonitorView
     * @memberof mwa.view.workflowMonitor
     * @extends {mwa.View}
     */
    global.WorkflowMonitorView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowMonitor.WorkflowMonitorView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-monitor">' +
                    '<div class="mwa-view-workflow-monitor-header">' +
                    '</div>' +
                    '<div class="mwa-view-workflow-monitor-body">' +
                        '<div class="mwa-view-workflow-monitor-workflow-instance-list-view"></div>' +
                    '</div>' +
                    '<div class="mwa-view-workflow-monitor-footer">' +
                    '</div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              mwa.View.prototype.render.apply(this, []);

              this.renderWorkflowInstanceListView();

              return this;
            },
            /**
             * The function to render workflow instance list view view.
             * @protected
             *
             * @returns {!proui.listView.ListViewView}
             * The rendered workflow instance list view view instance.
             */
            renderWorkflowInstanceListView: function() {
              var that = this;

              var INITIAL_SORT_SETTINGS = {
                key: 'createTime',
                isAscending: false
              };

              if (that.views.workflowInstanceListViewView != null) {
                that.views.workflowInstanceListViewView.remove();
              }

              that.views.workflowInstanceListViewView = new mwa.proui.listView.ListViewView({
                el: that.$el.find('.mwa-view-workflow-monitor-workflow-instance-list-view'),
                itemTemplates: {
                  id:
                      '<div>' +
                          '<@- data.id @>' +
                      '</div>',
                  name:
                      '<div>' +
                          '<@- data.name @>' +
                      '</div>',
                  templateName:
                      '<div>' +
                          '<@- data.templateName @>' +
                      '</div>',
                  status_name:
                      '<div>' +
                          '<@- data.status.name @>' +
                      '</div>',
                  progress:
                      '<div style="text-align: right;">' +
                          '<@- data.progress @> % &nbsp;' +
                      '</div>',
                  createTime:
                      '<div>' +
                          '<@ if (data.createTime != null) { @>' +
                              '<@- mwa.proui.util.Date.formatDate(new Date(data.createTime)) @>' +
                          '<@ } @>' +
                      '</div>',
                  endTime:
                      '<div>' +
                          '<@ if (data.endTime != null) { @>' +
                              '<@- mwa.proui.util.Date.formatDate(new Date(data.endTime)) @>' +
                          '<@ } @>' +
                      '</div>'
                },
                viewModel: new mwa.proui.listView.ListViewViewModel({
                  hasPager: true,
                  hasSelectBox: false,
                  headerArray: [
                    {
                      key: 'id',
                      label: mwa.enumeration.Message['SENTENCE_ID'],
                      canSort: true,
                      canResize: false,
                      width: 250
                    },
                    {
                      key: 'name',
                      label: mwa.enumeration.Message['SENTENCE_NAME'],
                      canSort: true,
                      canResize: true,
                      width: 200
                    },
                    {
                      key: 'templateName',
                      label: mwa.enumeration.Message['SENTENCE_WORKFLOW'],
                      canSort: true,
                      canResize: true,
                      width: 250
                    },
                    {
                      label: mwa.enumeration.Message['SENTENCE_STATUS'],
                      // CAUTION:
                      // This setting means sorting by "name" of "status" object. It is necessary to
                      // take care about this key setting because the "status" of
                      // WorkflowInstanceModel is object type.
                      key: 'status_name',
                      canSort: true,
                      canResize: false,
                      width: 90
                    },
                    {
                      label: mwa.enumeration.Message['SENTENCE_PROGRESS'],
                      key: 'progress',
                      canSort: true,
                      canResize: false,
                      width: 70
                    },
                    {
                      label: mwa.enumeration.Message['SENTENCE_CREATION_DATE_TIME'],
                      key: INITIAL_SORT_SETTINGS.key,
                      canSort: true,
                      canResize: false,
                      width: 120
                    },
                    {
                      label: mwa.enumeration.Message['SENTENCE_END_TIME'],
                      key: 'endTime',
                      canSort: true,
                      canResize: false,
                      width: 120
                    }
                  ],
                  displayRows: 100,
                  itemCollection: that.options.viewModel.get('workflowInstanceCollection'),
                  selectedItems: that.options.viewModel.get('selectedWorkflowInstanceCollection'),
                  filterArray: [
                    'parentId' + '==' + ''
                  ],
                  createRequest: function(key, isAscending, offset, limit, options) {
                    var data = mwa.proui.listView.ListViewViewModel.prototype.createRequest.apply(
                        this, [key, isAscending, offset, limit, options]
                    );

                    // INFORMATION:
                    // These codes mean adding second sort settings.
                    if (key == null) {
                      data.sort = [INITIAL_SORT_SETTINGS.key + ((INITIAL_SORT_SETTINGS.isAscending) ? '+' : '-')];
                      // INFORMATION:
                      // It is important to add ID sort setting because the value of createTime
                      // isn't unique.
                      data.sort.push('id' + ((INITIAL_SORT_SETTINGS.isAscending) ? '+' : '-'));
                    } else if (key !== 'id') {
                      data.sort.push('id' + ((isAscending) ? '+' : '-'));
                    }

                    return data;
                  },
                  fetch: function(options) {
                    // INFORMATION:
                    // It is important to set options for disabling validation because validation
                    // isn't required for fetch.
                    options = mwa.proui._.extend({ validate: false }, options);

                    return mwa.proui.listView.ListViewViewModel.prototype.fetch.apply(
                        this, [options]
                    );
                  },
                  select: function(selectedItemModel, isSelected) {
                    that.options.viewModel.set(
                        'lastSelectedWorkflowInstanceModel', this.get('lastSelectedItem')
                    );
                    that.options.viewModel.select(selectedItemModel, isSelected);
                  }
                })
              });

              // INFORMATION:
              // This codes mean fetching the items with initial sort setting.
              that.views.workflowInstanceListViewView.options.viewModel.set(INITIAL_SORT_SETTINGS);

              return that.views.workflowInstanceListViewView;
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowMonitor;
}));
