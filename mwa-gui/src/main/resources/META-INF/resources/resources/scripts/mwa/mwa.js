/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

var mwa;

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      'log4js',
      'proui',
      'proui-ext'
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        require('log4js'),
        require('proui'),
        require('proui-ext')
    );
  } else {
    root.mwa = factory(
        null,
        root.proui,
        root.prouiExt
    );
  }
}(this, function(Log4js, proui, prouiExt) {
  /**
   * The mwa namespace.
   * @namespace
   */
  mwa = (function() {
    'use strict';

    /**
     * Defines mwa alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa logger.
     * @alias mwa.Logger
     * @memberof mwa
     */
    global.Logger = (function() {
      /**
       * Defines mwa.Logger alias name.
       */
      var global = {};

      /**
       * The function to log a fatal message.
       */
      global.fatal = function(message) {};

      /**
       * The function to log an error message.
       */
      global.error = function(message) {};

      /**
       * The function to log a warning message.
       */
      global.warn = function(message) {};

      /**
       * The function to log an information message.
       */
      global.info = function(message) {};

      /**
       * The function to log a debug message.
       */
      global.debug = function(message) {};

      /**
       * The function to log a trace message.
       */
      global.trace = function(message) {};

      // To override the log functions if Log4js is available.
      if (typeof Log4js !== 'undefined' && Log4js != null) {
        if (typeof __dirname !== 'undefined' && __dirname != null) {
          Log4js.configure(__dirname + '/../../logs/mwa/log4js.json', {
            cwd: __dirname + '/../../logs/mwa'
          });
          global = Log4js.getLogger('${project.build.finalName}');
        }
      }

      return global;
    })();

    /**
     * The mwa Pro-UI.
     * @alias mwa.proui
     * @memberof mwa
     */
    global.proui = (function(base) {
      /**
       * Defines mwa.proui alias name.
       */
      var global = proui;

      // CAUTION:
      // To enable cross-domain requests.
      global.Backbone.$.support.cors = true;
      global.Backbone.sync = (function() {
        var sync = global.Backbone.sync;

        return function(method, model, options) {
          options = global._.extend(
              {},
              options,
              {
                xhr: function() {
                  return new XMLHttpRequest();
                }
              }
          );

          return sync.apply(this, [method, model, options]);
        };
      })();

      global.View.prototype.presenter = (function() {
        var presenter = global.View.prototype.presenter;

        return function(options) {
          return global._.extend(
              presenter.apply(this, [options]),
              {
                mwa: base
              }
          );
        };
      })();

      global.tooltip.TooltipView.prototype.defaults = (function() {
        var caches = {
          defaults: global.tooltip.TooltipView.prototype.defaults
        };

        return global.util.Object.extend(true,
            {},
            global._.result(caches, 'defaults'),
            {
              delay: 1000
            }
        );
      })();

      global.dropDownButton.DropDownButtonViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.dropDownButton.DropDownButtonViewModel.prototype.defaults
        };

        return function() {
          return global.util.Object.extend(true,
              {},
              global._.result(caches, 'defaults'),
              {
                initialItem: '--' + base.enumeration.Message['SENTENCE_SELECT'] + '--'
              }
          );
        };
      })();

      global.datePicker.DatePickerViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.datePicker.DatePickerViewModel.prototype.defaults
        };

        return function() {
          return global.util.Object.extend(true,
              {},
              global._.result(caches, 'defaults'),
              {
                texts: {
                  requiredValidationText:
                      base.enumeration.Message['VALIDATION_REQUIRED'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                      ),
                  patternValidationText:
                      base.enumeration.Message['VALIDATION_PATTERN'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                      ).replace(
                          /\{1\}/,
                          'YYYY-MM-DD'
                      )
                }
              }
          );
        };
      })();

      global.swapListBox.SwapListBoxViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.swapListBox.SwapListBoxViewModel.prototype.defaults
        };

        return function() {
          return global.util.Object.extend(true,
              {},
              global._.result(caches, 'defaults'),
              {
                texts: {
                  leftSelectBoxTitle: base.enumeration.Message['TITLE_AVAILABLE_ITEM_PLURAL'],
                  rightSelectBoxTitle: base.enumeration.Message['TITLE_SELECTED_ITEM_PLURAL'],
                  showAllText: base.enumeration.Message['FOOTNOTE_SHOW_ALL'],
                  topButtonTooltip: base.enumeration.Message['TITLE_TOP'],
                  upButtonTooltip: base.enumeration.Message['TITLE_UP'],
                  downButtonTooltip: base.enumeration.Message['TITLE_DOWN'],
                  bottomButtonTooltip: base.enumeration.Message['TITLE_BOTTOM'],
                  deleteButtonTooltip: base.enumeration.Message['TITLE_DELETE']
                }
              }
          );
        };
      })();

      global.listView.ListViewViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.listView.ListViewViewModel.prototype.defaults
        };

        return function() {
          return global.util.Object.extend(true,
              {},
              global._.result(caches, 'defaults'),
              {
                texts: {
                  ascendingText: base.enumeration.Message['SENTENCE_ASCENDING'],
                  descendingText: base.enumeration.Message['SENTENCE_DESCENDING'],
                  columnSettingDialogTitle: base.enumeration.Message['TITLE_COLUMN_SETTING'],
                  availableHeaderTitle: base.enumeration.Message['TITLE_AVAILABLE_COLUMN_PLURAL'],
                  selectedHeaderTitle: base.enumeration.Message['TITLE_SELECTED_COLUMN_PLURAL'],
                  showAllText: base.enumeration.Message['FOOTNOTE_SHOW_ALL'],
                  topButtonTooltip: base.enumeration.Message['TITLE_TOP'],
                  upButtonTooltip: base.enumeration.Message['TITLE_UP'],
                  downButtonTooltip: base.enumeration.Message['TITLE_DOWN'],
                  bottomButtonTooltip: base.enumeration.Message['TITLE_BOTTOM'],
                  deleteButtonTooltip: base.enumeration.Message['TITLE_DELETE'],
                  applyButtonText: base.enumeration.Message['TITLE_APPLY'],
                  cancelButtonText: base.enumeration.Message['TITLE_CANCEL']
                }
              }
          );
        };
      })();

      return global;
    })(global);

    /**
     * The mwa Pro-UI Extension.
     * @alias mwa.prouiExt
     * @memberof mwa
     */
    global.prouiExt = (function(base) {
      /**
       * Defines mwa.prouiExt alias name.
       */
      var global = prouiExt;

      global.login.LoginViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.login.LoginViewModel.prototype.defaults
        };

        return function() {
          return base.proui.util.Object.extend(true,
              {},
              base.proui._.result(caches, 'defaults'),
              {
                texts: {
                  loginText: base.enumeration.Message['TITLE_LOGIN'],
                  usernameText: base.enumeration.Message['SENTENCE_USER'],
                  passwordText: base.enumeration.Message['SENTENCE_PASSWORD']
                }
              }
          );
        };
      })();

      global.uploader.UploaderViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.uploader.UploaderViewModel.prototype.defaults
        };

        return function() {
          return base.proui.util.Object.extend(true,
              {},
              base.proui._.result(caches, 'defaults'),
              {
                texts: {
                  uploadDestinationText: base.enumeration.Message['FOOTNOTE_UPLOAD_TO'],
                  addButtonText: base.enumeration.Message['TITLE_ADD'],
                  cancelButtonText: base.enumeration.Message['TITLE_CANCEL'],
                  closeButtonText: base.enumeration.Message['TITLE_CLOSE'],
                  uploadButtonText: base.enumeration.Message['TITLE_UPLOAD'],
                  dragAndDropText: base.enumeration.Message['INFORMATION_DROP_FILE_PLURAL'],
                  itemsSelectedText: base.enumeration.Message['LOWER_ITEM_SELECTED_PLURAL'],
                  completeStatusText: 'COMPLETED',
                  cancelStatusText: 'CANCELLED', // not 'CANCELED' (ref. ActivityInstanceStatus)
                  errorStatusText: 'ERROR',
                  defaultErrorText: base.enumeration.Message['ERROR_DEFAULT']
                }
              }
          );
        };
      })();

      global.metadataEditor.MetadataEditorViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.metadataEditor.MetadataEditorViewModel.prototype.defaults
        };

        return function() {
          return base.proui.util.Object.extend(true,
              {},
              base.proui._.result(caches, 'defaults'),
              {
                texts: {
                  editButtonTooltip: base.enumeration.Message['TITLE_EDIT'],
                  saveButtonText: base.enumeration.Message['TITLE_SAVE'],
                  browseButtonText: base.enumeration.Message['TITLE_BROWSE'],
                  selectButtonText: base.enumeration.Message['TITLE_SELECT'],
                  cancelButtonText: base.enumeration.Message['TITLE_CANCEL'],
                  closeButtonText: base.enumeration.Message['TITLE_CLOSE'],
                  defaultMetadataSchemaTitle: base.enumeration.Message['TITLE_CUSTOM_METADATA'],
                  initialItemText: '-- ' + base.enumeration.Message['SENTENCE_SELECT'] + ' --',
                  trueValueText: base.enumeration.Message['SENTENCE_YES'],
                  falseValueText: base.enumeration.Message['SENTENCE_NO'],
                  nullValueText: base.enumeration.Message['SENTENCE_UNSET'],
                  multipleValueText: base.enumeration.Message['SENTENCE_MULTIPLE_VALUE'],
                  multipleValueWithEmptyValueText: base.enumeration.Message['FOOTNOTE_INCLUDING_EMPTY_VALUE'],
                  multipleValueWithInvalidValueText: base.enumeration.Message['FOOTNOTE_INCLUDING_INVALID_VALUE'],
                  nameListViewHeader: base.enumeration.Message['SENTENCE_NAME'],
                  pathListViewHeader: base.enumeration.Message['SENTENCE_PATH'],
                  validationText: base.enumeration.Message['VALIDATION_ERROR'],
                  maxLengthValidationText:
                      base.enumeration.Message['VALIDATION_MAXIMUM_LENGTH_SIMPLE'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_NUMBER_OF_CHARACTERS'].toLowerCase()
                      ),
                  requiredValidationText:
                      base.enumeration.Message['VALIDATION_REQUIRED'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                      ),
                  numberTypeValidationText:
                      base.enumeration.Message['VALIDATION_TYPE'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                      ).replace(
                          /\{1\}/,
                          base.enumeration.Message['SENTENCE_NUMBER'].toLowerCase()
                      ),
                  dateTimeTypeValidationText:
                      base.enumeration.Message['VALIDATION_PATTERN'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                      ).replace(
                          /\{1\}/,
                          'YYYY-MM-DD' + ' ' +
                          base.enumeration.Message['LOWER_OR'] + ' ' +
                          'YYYY-MM-DD hh:mm:ss'
                      ),
                  timecodeTypeValidationText:
                      base.enumeration.Message['VALIDATION_PATTERN'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                      ).replace(
                          /\{1\}/,
                          'hh:mm:ss:ff, hh:mm:ss.ff, hh:mm:ss;ff, hh.mm.ss.ff' + ' ' +
                          base.enumeration.Message['LOWER_OR'] + ' ' +
                          'hh;mm;ss;ff'
                      ),
                  imageTypeValidationText:
                      base.enumeration.Message['VALIDATION_TYPE'].replace(
                          /\{0\}/,
                          base.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                      ).replace(
                          /\{1\}/,
                          base.enumeration.Message['SENTENCE_IMAGE'].toLowerCase()
                      ),
                  validationErrorMessageBoxTitle: base.enumeration.Message['TITLE_ERROR']
                }
              }
          );
        };
      })();

      global.metadataEditorDialog.MetadataEditorDialogViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.metadataEditorDialog.MetadataEditorDialogViewModel.prototype.defaults
        };

        return function() {
          return base.proui.util.Object.extend(true,
              {},
              base.proui._.result(caches, 'defaults'),
              {
                texts: {
                  saveButtonText: base.enumeration.Message['TITLE_SAVE'],
                  cancelButtonText: base.enumeration.Message['TITLE_CANCEL']
                }
              }
          );
        };
      })();

      global.metadataEditorListView.MetadataEditorListViewViewModel.prototype.defaults = (function() {
        var caches = {
          defaults: global.metadataEditorListView.MetadataEditorListViewViewModel.prototype.defaults
        };

        return function() {
          return base.proui.util.Object.extend(true,
              {},
              base.proui._.result(caches, 'defaults'),
              {
                texts: {
                  addButtonTooltip: base.enumeration.Message['TITLE_ADD'],
                  deleteButtonTooltip: base.enumeration.Message['TITLE_DELETE'],
                  refreshButtonTooltip: base.enumeration.Message['TITLE_REFRESH'],
                  informationButtonTooltip: base.enumeration.Message['TITLE_INFORMATION'],
                  confirmationDialogTitle: base.enumeration.Message['TITLE_CONFIRMATION'],
                  deleteItemConfirmationText:
                      base.enumeration.Message['CONFIRMATION_DELETE'],
                  deleteItemsConfirmationText:
                      base.enumeration.Message['CONFIRMATION_DELETE_PLURAL'],
                  yesButtonText: base.enumeration.Message['TITLE_YES'],
                  noButtonText: base.enumeration.Message['TITLE_NO']
                }
              }
          );
        };
      })();

      return global;
    })(global);

    /**
     * The mwa model.
     * @constructor
     * @inner
     * @alias mwa.Model
     * @memberof mwa
     * @extends {proui.Model}
     */
    global.Model = (function() {
      return global.proui.Model.extend(
          /**
           * @lends mwa.Model
           */
          {
            alias: 'mwa.Model',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? '' : resource;
              return '/mwa/api/v2/' + resource;
            },
            /**
             * The function to define the default values of the model.
             * CAUTION:
             * If defaults is an object literal ({}) instead of a function, the same object is used
             * as the default attributes for each model instance.
             * {@link http://stackoverflow.com/questions/17775822/backbone-js-model-defaults-and-parse|Backbone.js Model defaults and parse}
             * @protected
             *
             * @returns {!object}
             * The default values.
             */
            defaults: function() {
              return global.proui.util.Object.extend(true,
                  {},
                  global.proui._.result(global.proui.Model.prototype, 'defaults'),
                  {
//                    id: null
                  }
              );
            },
            validation: global.proui.util.Object.extend(true,
                {},
                global.proui.Model.prototype.validation,
                {
//                  id: [
//                    {
//                      required: true
//                    }
//                  ]
                }
            ),
            /**
             * @see {@link proui.Model.initialize}
             * @protected
             * @override
             */
            initialize: function(attributes, options) {
              var that = this;

              that.listenTo(that, 'invalid', function(model, errors) {
                for (var error in errors) {
                  if (errors.hasOwnProperty(error)) {
                    global.Logger.error(
                        that.alias + '.validate : ' +
                        'property = ' + error + ', ' +
                        'message = ' + errors[error]
                    );
                  }
                }

                throw Error(that.alias + '.validate : ' + global.proui._.values(errors)[0]);
              });

              global.proui.Model.prototype.initialize.apply(this, [attributes, options]);

              var isValid =
                  (typeof options !== 'undefined' && options != null && options.validate === false) ?
                      true : that.isValid(true);
              if (isValid) {
                that.listen();
              }
            },
            /**
             * The function to set event handlers for the model.
             * @protected
             */
            listen: function() {
              // If you need new listeners, add here.
            },
            /**
             * The function to create editing metadata field collection.
             * @public
             *
             * @return {prouiExt.metadataEditorListView.MetadataFieldCollection}
             * The created metadata field collection instance.
             */
            createEditingMetadataFieldCollection: function() {
              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection();
            },
            /**
             * The function to create editing metadata group collection.
             * @public
             *
             * @return {prouiExt.metadataEditorDialog.MetadataGroupCollection}
             * The created metadata group collection instance.
             */
            createEditingMetadataGroupCollection: function() {
              return new mwa.prouiExt.metadataEditorDialog.MetadataGroupCollection([
                {
                  regExp: new RegExp('.*'),
                  metadataGroupCollection: new mwa.prouiExt.metadataEditor.MetadataGroupCollection([
                    {
                      title: null,
                      regExp: new RegExp('.*'),
                      isHidden: false,
                      isExpanded: true
                    }
                  ])
                }
              ]);
            }
          }
      );
    })();

    /**
     * The mwa collection.
     * @constructor
     * @inner
     * @alias mwa.Collection
     * @memberof mwa
     * @extends {proui.Collection}
     */
    global.Collection = (function() {
      return global.proui.Collection.extend(
          /**
           * @lends mwa.Collection
           */
          {
            url: global.Model.prototype.urlRoot,
            model: global.Model,
            /**
             * @see {@link proui.Collection.destroy}
             * @public
             * @override
             */
            destroy: function(models, options) {
              options = global.proui._.extend(
                  {
                    // CAUTION:
                    // It is necessary to make query parameter manually instead of using "data"
                    // setting in DELETE method.
                    url: global.proui._.result(this, 'url') + '?' + mwa.proui.Backbone.$.param(
                        {
                          filter: [].concat(models).reduce(
                              function(memo, model, index, array) {
                                return memo + model.idAttribute + '==' + model.id + ',';
                              },
                              ''
                          ).slice(0, -1)
                        },
                        true
                    )
                  },
                  options
                  );

              return global.proui.Collection.prototype.destroy.apply(this, [models, options]);
            }
          }
      );
    })();

    /**
     * The mwa view model.
     * @constructor
     * @inner
     * @alias mwa.ViewModel
     * @memberof mwa
     * @extends {proui.ViewModel}
     */
    global.ViewModel = (function() {
      return global.proui.ViewModel.extend(
          /**
           * @lends mwa.ViewModel
           */
          {
            /**
             * The function to define the default values of the view model.
             * CAUTION:
             * If defaults is an object literal ({}) instead of a function, the same object is used
             * as the default attributes for each model instance.
             * {@link http://stackoverflow.com/questions/17775822/backbone-js-model-defaults-and-parse|Backbone.js Model defaults and parse}
             * @protected
             *
             * @returns {!object}
             * The default values.
             */
            defaults: function() {
              return global.proui.util.Object.extend(true,
                  {},
                  global.proui._.result(global.proui.ViewModel.prototype, 'defaults'),
                  {
//                    id: null
                  }
              );
            },
            validation: global.proui.util.Object.extend(true,
                {},
                global.proui.ViewModel.prototype.validation,
                {
//                  id: [
//                    {
//                      required: true
//                    }
//                  ]
                }
            ),
            /**
             * @see {@link proui.ViewModel.initialize}
             * @protected
             * @override
             */
            initialize: function(attributes, options) {
              var that = this;

              that.listenTo(that, 'invalid', function(viewModel, errors) {
                for (var error in errors) {
                  if (errors.hasOwnProperty(error)) {
                    global.Logger.error(
                        'mwa.ViewModel.validate : ' +
                        'property = ' + error + ', ' +
                        'message = ' + errors[error]
                    );
                  }
                }

                throw Error('mwa.ViewModel.validate : ' + global.proui._.values(errors)[0]);
              });

              global.proui.ViewModel.prototype.initialize.apply(this, [attributes, options]);
            },
            /**
             * @see {@link proui.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              global.proui.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link proui.ViewModel.destroy}
             * @protected
             * @override
             */
            destroy: function(options) {
              // INFORMATION:
              // This function is almost same as original Backbone.destroy function but the only
              // last part is different.

              options = (options) ? global.proui._.clone(options) : {};
              var model = this;
              var success = options.success;
              var wait = options.wait;

              var destroy = function() {
                model.stopListening();
                model.trigger('destroy', model, model.collection, options);
              };

              options.success = function(resp) {
                if (wait) destroy();
                if (success) success.call(options.context, model, resp, options);
                if (!model.isNew()) model.trigger('sync', model, resp, options);
              };

              // INFORMATION:
              // The isNew check has been removed in order to achieve expected behavior.
              var xhr = false;
              global.proui._.defer(options.success);
              if (!wait) { destroy(); }

              return xhr;
            }
          }
      );
    })();

    /**
     * The mwa view.
     * @constructor
     * @inner
     * @alias mwa.View
     * @memberof mwa
     * @extends {proui.View}
     */
    global.View = (function() {
      return global.proui.View.extend(
          /**
           * @lends mwa.View
           */
          {
            defaults: global.proui.util.Object.extend(true,
                {},
                global.proui.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: global.proui._.template(
                // TODO: To define the DOM structure.
                '<div></div>'
            ),
            events: global.proui.util.Object.extend(true,
                {},
                global.proui.View.prototype.events,
                {
                  // If you need new events, add here.
                }
            ),
            /**
             * @see {@link proui.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              global.proui.View.prototype.listen.apply(this, []);

              this.listenTo(this.options.viewModel, 'destroy', this.destroy);
            },
            /**
             * @see {@link proui.View.render}
             * @public
             * @override
             */
            render: function() {
              global.proui.View.prototype.render.apply(this, []);

              // If you need new sub views, add here.
            },
            /**
             * @see {@link proui.View.remove}
             * @public
             * @override
             */
            remove: function(options) {
              if (typeof options === 'undefined' || options == null || !options.silent) {
                for (var propertyName in this.models) {
                  if (this.models.hasOwnProperty(propertyName)) {
                    // CAUTION:
                    // It is important to stop listeners for internal managed models in order to
                    // prevent memory leak.
                    this.stopListening(this.models[propertyName]);
                  }
                }

                if (this.options != null && this.options.viewModel != null &&
                    this.constructor.canStop === true) {
                  // CAUTION:
                  // It is important to stop a listener for destroy event of view model in order to
                  // prevent infinite loop.
                  this.stopListening(this.options.viewModel, 'destroy', this.destroy);

                  this.options.viewModel.destroy(options);
                }
              }

              return global.proui.View.prototype.remove.apply(this, [options]);
            },
            /**
             * The event handler for destroying view model. This function removes the view itself in
             * response to the event.
             *
             * @param {mwa.ViewModel} model
             * The destroyed model.
             * @param {Backbone.Collection} collection
             * The collection that was destroyed the model.
             * @param {object} options
             * The options of destroy event.
             *
             * @returns {mwa.View}
             * The removed view instance.
             */
            destroy: function(model, collection, options) {
              return this.remove(options);
            }
          }
      );
    })();
    // CAUTION:
    // The flag whether that this view class supports view model listeners stop.
    global.View.canStop = true;

    var $mwa =
        global.proui.Backbone.$(
            '<div id="mwa">' +
                '<div id="mwa-internal-image"></div>' +
                '<div id="mwa-external-image"></div>' +
                '<div id="mwa-internal-locale"></div>' +
            '</div>'
        // CAUTION:
        // It is necessary to append the tentative element to html, not body, because any element
        // under body cannot be accessed during script initialization.
        ).appendTo('html');

    /**
     * The internal image base path.
     * @constant
     * @type {string}
     * @default
     * @alias mwa.INTERNAL_IMAGE_BASE_PATH
     * @memberof mwa
     */
    global.INTERNAL_IMAGE_BASE_PATH = (typeof __dirname === 'undefined' || __dirname == null) ?
        global.proui.util.Url.sanitize(
            $mwa.find('#mwa-internal-image').css('background-image')
        // CAUTION:
        // It is necessary to add the end slash at this timing because it is automatically removed
        // by browser even if it is written in the style setting.
        ) + '/' :
        'file://' + __dirname.replace(/\\/g, '/') + '/../../images/mwa/';

    /**
     * The external image base path.
     * @constant
     * @type {string}
     * @default
     * @alias mwa.EXTERNAL_IMAGE_BASE_PATH
     * @memberof mwa
     */
    global.EXTERNAL_IMAGE_BASE_PATH = (typeof __dirname === 'undefined' || __dirname == null) ?
        global.proui.util.Url.sanitize(
            $mwa.find('#mwa-external-image').css('background-image')
        // CAUTION:
        // It is necessary to add the end slash at this timing because it is automatically removed
        // by browser even if it is written in the style setting.
        ) + '/' :
        'file://' + __dirname.replace(/\\/g, '/') + '/../../node_modules/proui-style-' +
        // CAUTION: NVX-2754 (https://www.tool.sony.biz/jira/browse/NVX-2754)
        // It is necessary to switch style module directory based on the applied style theme.
        global.proui.util.Url.findQuery(
            global.proui.Backbone.$('html').css('background-image'), 'style'
        ) + '/images/proui-flat/';

    /**
     * The internal locale base path.
     * @constant
     * @type {string}
     * @default
     * @alias mwa.INTERNAL_LOCALE_BASE_PATH
     * @memberof mwa
     */
    global.INTERNAL_LOCALE_BASE_PATH = (typeof __dirname === 'undefined' || __dirname == null) ?
        global.proui.util.Url.sanitize(
            $mwa.find('#mwa-internal-locale').css('background-image')
        // CAUTION:
        // It is necessary to add the end slash at this timing because it is automatically removed
        // by browser even if it is written in the style setting.
        ) + '/' :
        __dirname + '/../../locales/mwa/';

    $mwa.remove();

    return global;
  })();

  return mwa;
}));
