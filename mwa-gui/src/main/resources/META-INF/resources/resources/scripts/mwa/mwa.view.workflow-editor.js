/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.view == null) ? './mwa.view' : {},
      (mwa.view == null || mwa.view.activityProfileListView == null) ? './mwa.view.activity-profile-list-view' : {},
      (mwa.view == null || mwa.view.workflowCanvas == null) ? './mwa.view.workflow-canvas' : {},
      (mwa.model == null || mwa.model.directory == null) ? './mwa.model.directory' : {},
      (mwa.model == null || mwa.model.workflowDiagramDetail == null) ? './mwa.model.workflow-diagram-detail' : {},
      (mwa.model == null || mwa.model.workflowDetail == null) ? './mwa.model.workflow-detail' : {},
      (mwa.model == null || mwa.model.activityProfileGroupDetail == null) ? './mwa.model.activity-profile-group-detail' : {},
      (mwa.model == null || mwa.model.activityProfile == null) ? './mwa.model.activity-profile' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.view == null) ? require('./mwa.view') : {},
        (mwa.view == null || mwa.view.activityProfileListView == null) ? require('./mwa.view.activity-profile-list-view') : {},
        (mwa.view == null || mwa.view.workflowCanvas == null) ? require('./mwa.view.workflow-canvas') : {},
        (mwa.model == null || mwa.model.directory == null) ? require('./mwa.model.directory') : {},
        (mwa.model == null || mwa.model.workflowDiagramDetail == null) ? require('./mwa.model.workflow-diagram-detail') : {},
        (mwa.model == null || mwa.model.workflowDetail == null) ? require('./mwa.model.workflow-detail') : {},
        (mwa.model == null || mwa.model.activityProfileGroupDetail == null) ? require('./mwa.model.activity-profile-group-detail') : {},
        (mwa.model == null || mwa.model.activityProfile == null) ? require('./mwa.model.activity-profile') : {}
    );
  } else {
    root.mwa.view.workflowEditor = factory(
        root.mwa,
        root.mwa.view,
        root.mwa.view.activityProfileListView,
        root.mwa.view.workflowCanvas,
        root.mwa.model.directory,
        root.mwa.model.workflowDiagramDetail,
        root.mwa.model.workflowDetail,
        root.mwa.model.activityProfileGroupDetail,
        root.mwa.model.activityProfile
    );
  }
}(this, function(base, subBase, activityProfileListView, workflowCanvas, directory, workflowDiagramDetail, workflowDetail, activityProfileGroupDetail, activityProfile) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.view namespace.
  if (mwa.view == null) {
    mwa.view = subBase;
  }
  // To initialize the mwa.view.activityProfileListView namespace.
  if (mwa.view.activityProfileListView == null) {
    mwa.view.activityProfileListView = activityProfileListView;
  }
  // To initialize the mwa.view.workflowCanvas namespace.
  if (mwa.view.workflowCanvas == null) {
    mwa.view.workflowStep = workflowCanvas;
  }
  // To initialize the mwa.model.directory namespace.
  if (mwa.model.directory == null) {
    mwa.model.directory = directory;
  }
  // To initialize the mwa.model.workflowDiagramDetail namespace.
  if (mwa.model.workflowDiagramDetail == null) {
    mwa.model.workflowDiagramDetail = workflowDiagramDetail;
  }
  // To initialize the mwa.model.workflowDetail namespace.
  if (mwa.model.workflowDetail == null) {
    mwa.model.workflowDetail = workflowDetail;
  }
  // To initialize the mwa.model.activityProfileGroupDetail namespace.
  if (mwa.model.activityProfileGroupDetail == null) {
    mwa.model.activityProfileGroupDetail = activityProfileGroupDetail;
  }
  // To initialize the mwa.model.activityProfile namespace.
  if (mwa.model.activityProfile == null) {
    mwa.model.activityProfile = activityProfile;
  }

  /**
   * The mwa.view.workflowEditor namespace.
   * @namespace
   */
  mwa.view.workflowEditor = (function() {
    'use strict';

    /**
     * Defines mwa.view.workflowEditor alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The enumeration for display type.
     * @readonly
     * @enum {object}
     * @alias mwa.view.workflowEditor.DisplayType
     * @memberof mwa.view.workflowEditor
     */
    global.DisplayType = {
      WORKFLOW: {
        name: 'WORKFLOW',
        label: mwa.enumeration.Message['TITLE_WORKFLOW'],
        index: 0,
        template:
            '<div class="mwa-view-workflow-editor-body">' +
                '<div class="mwa-view-workflow-editor-body-left proui-pane proui-state-strong proui-state-left">' +
                    '<div class="mwa-view-workflow-editor-workflow-navigation"></div>' +
                    '<@ if (hasWorkflowDiagramNavigationTreeView) { @>' +
                        '<div class="mwa-view-workflow-editor-workflow-diagram-add-button"></div>' +
                    '<@ } @>' +
                '</div>' +
                '<div class="mwa-view-workflow-editor-body-center">' +
                    '<div class="mwa-view-workflow-editor-workflow-canvas-tab"></div>' +
                    '<div class="mwa-view-workflow-editor-annotation">' +
                        '<@ if (hasWorkflowDiagramNavigationTreeView) { @>' +
                            '<a class="mwa-view-workflow-editor-workflow-diagram-add-annotation" href="javascript:void(0);"><@- mwa.enumeration.Message["FOOTNOTE_ADD_NEW_WORKFLOW_DIAGRAM"]@></a>' +
                        '<@ } @>' +
                        '<div href="javascript:void(0);"><@- mwa.enumeration.Message["FOOTNOTE_DOUBLE_CLICK_WORKFLOW"] || "Double click workflow to open" @></div>' +
                    '</div>' +
                '</div>' +
                '<div class="mwa-view-workflow-editor-body-right"></div>' +
            '</div>'
      },
      TASK: {
        name: 'TASK',
        label: mwa.enumeration.Message['TITLE_TASK'],
        index: 1,
        template:
            '<div class="mwa-view-workflow-editor-body">' +
                '<div class="mwa-view-workflow-editor-body-left"></div>' +
                '<div class="mwa-view-workflow-editor-body-center">' +
                    '<div class="mwa-view-workflow-editor-activity-profile-list-view"></div>' +
                '</div>' +
                '<div class="mwa-view-workflow-editor-body-right"></div>' +
            '</div>'
      }
    };

    /**
     * The enumeration for workflow navigation tree view definition.
     * @readonly
     * @enum {object}
     * @alias mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition
     * @memberof mwa.view.workflowEditor
     */
    global.WorkflowNavigationTreeViewDefinition = {
      WORKFLOW_DIAGRAM_ROOT: {
        name: 'WORKFLOW_DIAGRAM_ROOT',
        icon: mwa.EXTERNAL_IMAGE_BASE_PATH + 'proui.util.directory.svg',
        label: 'name',
        contextMenuItemCollection: new mwa.proui.Backbone.Collection(
            mwa.proui._.sortBy(
                mwa.proui._.values(mwa.enumeration.WorkflowDiagramRootContextMenu),
                'index'
            )
        ),
        children: {
          WORKFLOW_DIAGRAM: 'children'
        },
        canSelect: true,
        canEdit: false,
        canDrag: false,
        canDrop: false
      },
      WORKFLOW_DIAGRAM: {
        name: 'WORKFLOW_DIAGRAM',
        icon: mwa.EXTERNAL_IMAGE_BASE_PATH + 'proui.util.workflow.svg',
        label: 'name',
        collectionClass: mwa.model.workflowDiagramDetail.WorkflowDiagramDetailCollection,
        contextMenuItemCollection: new mwa.proui.Backbone.Collection(
            mwa.proui._.sortBy(
                mwa.proui._.values(mwa.enumeration.WorkflowDiagramContextMenu),
                'index'
            )
        ),
        // children: {
        //   WORKFLOW: 'workflows'
        // },
        canSelect: true,
        canEdit: false,
        canDrag: false,
        canDrop: false
      },
      WORKFLOW: {
        name: 'WORKFLOW',
        icon: mwa.EXTERNAL_IMAGE_BASE_PATH + 'proui.util.workflow-playable.svg',
        label: 'alias',
        collectionClass: mwa.model.workflowDetail.WorkflowDetailCollection,
        contextMenuItemCollection: new mwa.proui.Backbone.Collection(
            mwa.proui._.sortBy(
                mwa.proui._.values(mwa.enumeration.WorkflowContextMenu),
                'index'
            )
        ),
        children: {
          WORKFLOW_PRESET_GROUP: 'profileGroups',
          WORKFLOW_PRESET: 'profiles'
        },
        canSelect: true,
        canEdit: false,
        canDrag: false,
        canDrop: true
      },
      WORKFLOW_PRESET_GROUP: {
        name: 'WORKFLOW_PRESET_GROUP',
        icon: null,
        label: 'name',
        collectionClass: mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailCollection,
        contextMenuItemCollection: new mwa.proui.Backbone.Collection(
            mwa.proui._.sortBy(
                mwa.proui._.values(mwa.enumeration.WorkflowPresetGroupContextMenu),
                'index'
            )
        ),
        children: {
          WORKFLOW_PRESET: 'profiles'
        },
        canSelect: true,
        canEdit: false,
        canDrag: false,
        canDrop: true
      },
      WORKFLOW_PRESET: {
        name: 'WORKFLOW_PRESET',
        icon: null,
        label: 'name',
        collectionClass: mwa.model.activityProfile.ActivityProfileCollection,
        contextMenuItemCollection: new mwa.proui.Backbone.Collection(
            mwa.proui._.sortBy(
                mwa.proui._.values(mwa.enumeration.WorkflowPresetContextMenu),
                'index'
            )
        ),
        canSelect: true,
        canEdit: false,
        canDrag: true,
        canDrop: false
      }
    };

    var displayTypeArray =
        mwa.proui._.sortBy(mwa.proui._.values(global.DisplayType), 'index');
    var workflowNavigationTreeViewDefinitionArray =
        mwa.proui._.values(global.WorkflowNavigationTreeViewDefinition);

    /**
     * The function to find tab index based on specified tab content.
     *
     * @param {!proui.tab.TabView} tabView
     * The tab view to be used for finding the index.
     * @param {!Backbone.View} tabContentView
     * The tab content view to specify one of the tab contents of specified tab.
     *
     * @returns {!number}
     * The found tab index.
     */
    var findTabIndex = function(tabView, tabContentView) {
      return (
          tabView.options.viewModel
              .get('contentList')
              .findIndex(
                  function(tabContentModel, index, array) {
                    return (
                        tabContentModel.get('viewOptions').viewModel ===
                        tabContentView.options.viewModel
                    );
                  }
              )
      );
    };

    /**
     * The function to get workflow navigation tree view definition based on selected item.
     *
     * @param {!Backbone.Model} selectedItemModel
     * The selected item model of workflow navigation tree view.
     *
     * @returns {mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition}
     * The workflow navigation tree view definition for selected item.
     */
    var getWorkflowNavigationTreeViewDefinition = function(selectedItemModel) {
      return mwa.proui._.find(
          workflowNavigationTreeViewDefinitionArray,
          function(workflowNavigationTreeViewDefinition, index, array) {
            return (
                selectedItemModel.alias ===
                (
                    workflowNavigationTreeViewDefinition.modelClass ||
                    (
                        workflowNavigationTreeViewDefinition.collectionClass ||
                        mwa.Collection
                    ).prototype.model ||
                    mwa.Model
                ).prototype.alias
            );
          }
      ) || mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition.WORKFLOW_DIAGRAM_ROOT;
    };

    /**
     * The function to get workflow navigation tree view context menu based on selected item.
     *
     * @param {!Backbone.Model} selectedItemModel
     * The selected item model of workflow navigation tree view.
     *
     * @returns {mwa.enumeration.WorkflowDiagramRootContextMenu|mwa.enumeration.WorkflowDiagramContextMenu|mwa.enumeration.WorkflowContextMenu}
     * The workflow navigation tree view context menu for selected item.
     */
    var getWorkflowNavigationTreeViewContextMenu = function(selectedItemModel) {
      var workflowNavigationTreeViewDefinition =
          getWorkflowNavigationTreeViewDefinition(selectedItemModel);

      var contextMenuName =
          mwa.proui.util.String.toCamelCase(workflowNavigationTreeViewDefinition.name) +
          'ContextMenu';
      contextMenuName =
          contextMenuName.charAt(0).toUpperCase() +
          contextMenuName.slice(1);

      return mwa.enumeration[contextMenuName];
    };

    /**
     * The function to update workflow navigation tree view context menu based on selected item.
     *
     * @param {!Backbone.Model} selectedItemModel
     * The selected item model of workflow navigation tree view.
     */
    var updateWorkflowNavigationTreeViewContextMenu = function(selectedItemModel) {
      var workflowNavigationTreeViewDefinition =
          getWorkflowNavigationTreeViewDefinition(selectedItemModel);
      var workflowNavigationTreeViewContextMenu =
          getWorkflowNavigationTreeViewContextMenu(selectedItemModel);

      workflowNavigationTreeViewDefinition.contextMenuItemCollection.forEach(
          function(contextMenuItemModel, index, array) {
            contextMenuItemModel.set(
                'isDisabled',
                workflowNavigationTreeViewContextMenu[contextMenuItemModel.get('name')]
                    .isDisabled(selectedItemModel)
            );
          }
      );
    };

    /**
     * The mwa workflow editor view model.
     * @constructor
     * @inner
     * @alias mwa.view.workflowEditor.WorkflowEditorViewModel
     * @memberof mwa.view.workflowEditor
     * @extends {mwa.ViewModel}
     */
    global.WorkflowEditorViewModel = (function() {
      return mwa.ViewModel.extend(
          /**
           * @lends mwa.view.workflowEditor.WorkflowEditorViewModel
           */
          {
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.ViewModel.prototype, 'defaults'),
                  {
                    hasDisplayTypeTab: true,
                    hasActivityPalette: true,
                    hasWorkflowDiagramNavigationTreeView: true,
                    canSchedule: true,
                    activityCollection: new mwa.model.activity.ActivityCollection(),
                    workflowDiagramDetailCollection: new mwa.model.workflowDiagramDetail.WorkflowDiagramDetailCollection(),
                    workflowDetailCollection: new mwa.model.workflowDetail.WorkflowDetailCollection(),
                    selectedWorkflowDiagramCollection: new mwa.model.workflowDiagram.WorkflowDiagramCollection()
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.ViewModel.prototype.validation,
                {
                  hasDisplayTypeTab: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  hasActivityPalette: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  hasWorkflowDiagramNavigationTreeView: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  canSchedule: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  activityCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.activity.ActivityCollection
                    }
                  ],
                  workflowDiagramDetailCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.workflowDiagramDetail.WorkflowDiagramDetailCollection
                    }
                  ],
                  workflowDetailCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.workflowDetail.WorkflowDetailCollection
                    }
                  ],
                  selectedWorkflowDiagramCollection: [
                    {
                      required: true
                    },
                    {
                      instance: mwa.model.workflowDiagram.WorkflowDiagramCollection
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.ViewModel.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.ViewModel.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The function to create workflow diagram detail model.
             * @public
             *
             * @returns {mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel}
             * The created workflow diagram detail model instance.
             */
            createWorkflowDiagramDetailModel: function() {
              return new mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel(
                  {
                    name: mwa.enumeration.Message['SENTENCE_NEW_WORKFLOW_DIAGRAM']
                  },
                  {validate: false}
              );
            },
            /**
             * The function to create workflow detail model.
             * @public
             *
             * @returns {mwa.model.workflowDetail.WorkflowDetailModel}
             * The created workflow detail model instance.
             */
            createWorkflowDetailModel: function() {
              return new mwa.model.workflowDetail.WorkflowDetailModel(
                  {
                    name: mwa.proui.util.Uuid.generateUuidV4(),
                    alias: mwa.enumeration.Message['SENTENCE_NEW_WORKFLOW'],
                    predefineFlag: false
                  },
                  {validate: false}
              );
            },
            /**
             * The function to create activity profile group detail model.
             * @public
             *
             * @returns {mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailModel}
             * The created activity profile group detail model instance.
             */
            createActivityProfileGroupDetailModel: function() {
              return new mwa.model.activityProfileGroupDetail.ActivityProfileGroupDetailModel(
                  {
                    name: mwa.enumeration.Message['SENTENCE_NEW_ACTIVITY_PROFILE_GROUP']
                  },
                  {validate: false}
              );
            },
            /**
             * The function to create activity profile model.
             * @public
             *
             * @returns {mwa.model.activityProfile.ActivityProfileModel}
             * The created activity profile model instance.
             */
            createActivityProfileModel: function() {
              return new mwa.model.activityProfile.ActivityProfileModel(
                  {
                    name: mwa.enumeration.Message['SENTENCE_NEW_ACTIVITY_PROFILE'],
                    icon: mwa.enumeration.Icon.WORKFLOW_PROFILE.dataUri
                  },
                  {validate: false}
              );
            }
          }
      );
    })();

    /**
     * The mwa workflow editor view.
     * @constructor
     * @inner
     * @alias mwa.view.workflowEditor.WorkflowEditorView
     * @memberof mwa.view.workflowEditor
     * @extends {mwa.View}
     */
    global.WorkflowEditorView = (function() {
      return mwa.View.extend(
          /**
           * @lends mwa.view.workflowEditor.WorkflowEditorView
           */
          {
            defaults: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.defaults,
                {
                  // If you need new options, add here.
                }
            ),
            template: mwa.proui._.template(
                '<div class="mwa-view-workflow-editor">' +
                    '<div class="mwa-view-workflow-editor-header">' +
                        '<div class="mwa-view-workflow-editor-header-center">' +
                            '<@ if (hasActivityPalette) { @>' +
                                '<div class="mwa-view-workflow-editor-activity-palette"></div>' +
                            '<@ } @>' +
                        '</div>' +
                    '</div>' +
                    displayTypeArray.reduce(
                        function(memo, displayType, index, array) {
                          return memo + displayType.template;
                        },
                        ''
                    ) +
                    '<div class="mwa-view-workflow-editor-footer">' +
                        '<div class="mwa-view-workflow-editor-footer-center"></div>' +
                    '</div>' +
                '</div>'
            ),
            events: mwa.proui.util.Object.extend(true,
                {},
                mwa.View.prototype.events,
                {
                  'click .mwa-view-workflow-editor-workflow-diagram-add-annotation':
                      'clickWorkflowDiagramAddAnnotation'
                }
            ),
            /**
             * @see {@link mwa.View.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.View.prototype.listen.apply(this, []);

              this.models = {
                activityParameterTypeCollection:
                    new mwa.model.activityParameterType.ActivityParameterTypeCollection()
              };

              this.listenToSelectedWorkflowDiagramCollection(
                  this.options.viewModel,
                  this.options.viewModel.get('selectedWorkflowDiagramCollection'),
                  null
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:selectedWorkflowDiagramCollection',
                  this.listenToSelectedWorkflowDiagramCollection
              );
              this.listenTo(
                  this.options.viewModel,
                  'change:selectedWorkflowDiagramCollection',
                  this.applySelectedWorkflowDiagramCollection
              );

              this.bindingBeforeUnloadWindow = mwa.proui._.bind(this.beforeUnloadWindow, this);
              mwa.proui.Backbone.$(window)
                  // INFORMATION:
                  // It is better to use on (off) function instead of bind (unbind) function in
                  // accordance with other implementation.
                  // CAUTION: NVX-5751 (https://www.tool.sony.biz/jira/browse/NVX-5751)
                  // The event handler for beforeunload event won't be called correctly in NW.js
                  // environment.
                  //  - window.onbeforeunload not work!
                  //    (https://github.com/nwjs/nw.js/issues/5872)
                  .off('beforeunload', this.bindingBeforeUnloadWindow)
                  .on('beforeunload', this.bindingBeforeUnloadWindow);
            },
            /**
             * The event handler for changing selectedWorkflowDiagramCollection attribute of view
             * model. This function removes old listeners and adds new listeners.
             * @protected
             *
             * @param {mwa.view.workflowEditor.WorkflowEditorViewModel} model
             * The mwa workflow editor view model.
             * @param {mwa.model.workflowDiagram.WorkflowDiagramCollection} value
             * The new changed value.
             * @param {object} [options]
             * The options of change event.
             */
            listenToSelectedWorkflowDiagramCollection: function(model, value, options) {
              var previousValue = model.previous('selectedWorkflowDiagramCollection');
              if (typeof previousValue !== 'undefined' && previousValue != null) {
                this.stopListening(previousValue);
              }

              if (typeof value !== 'undefined' && value != null) {
                this.stopListening(value);

                this.listenTo(value, 'change', this.applySelectedWorkflowDiagramCollection);
                this.listenTo(value, 'add', this.applySelectedWorkflowDiagramCollection);
                this.listenTo(value, 'remove', this.applySelectedWorkflowDiagramCollection);
                this.listenTo(value, 'update', this.applySelectedWorkflowDiagramCollection);
                // this.listenTo(value, 'reset', this.applySelectedWorkflowDiagramCollection);
                this.listenTo(value, 'request', this.applySelectedWorkflowDiagramCollection);
                this.listenTo(value, 'sync', this.applySelectedWorkflowDiagramCollection);
                this.listenTo(value, 'error', this.applySelectedWorkflowDiagramCollection);
                this.listenTo(value, 'destroy', this.applySelectedWorkflowDiagramCollection);
              }
            },
            /**
             * @see {@link mwa.View.render}
             * @public
             * @override
             */
            render: function() {
              var that = this;

              mwa.View.prototype.render.apply(that, []);

              that.renderLoading();

              mwa.proui.Backbone.$.when(
                  that.models.activityParameterTypeCollection.fetch({
                    data: {
                      sort: [
                        that.models.activityParameterTypeCollection.comparator + '+'
                      ]
                    }
                  })
              ).done(
                  // INFORMATION:
                  // The arguments are changed based on passed arguments for when function.
                  //  - jQuery.when() (http://myjquery.blog.fc2.com/blog-entry-10.html)
                  function(data, textStatus, xhr) {
                    that.renderDisplayTypeTab();
                    that.renderActivityPalette();
                    that.renderActivityProfileListView();
                    that.renderWorkflowNavigationExpander();
                    that.renderWorkflowCanvasTab();
                    that.renderWorkflowDiagramAddButton();

                    // To do same processing as update event.
                    that.applySelectedWorkflowDiagramCollection(
                        that.options.viewModel.get('selectedWorkflowDiagramCollection'),
                        null
                    );
                  }
              ).always(
                  // INFORMATION:
                  // The arguments are changed based on the result. If success, the arguments are
                  // same as done callback. If failure, the arguments are same as fail callback.
                  function() {
                    that.removeLoading();
                  }
              );

              return that;
            },
            /**
             * The function to render display type tab.
             * @protected
             *
             * @returns {proui.tab.TabView}
             * The rendered tab view instance.
             */
            renderDisplayTypeTab: function() {
              var that = this;

              if (that.views.displayTypeTabView != null) {
                that.views.displayTypeTabView.remove();
                that.views.displayTypeTabView = null;
              }

              // INFORMATION:
              // if it isn't necessary, it is better not to manage tab content view in consideration
              // of a case that the views will be added or removed in the processing.

              that.views.displayTypeTabView = new mwa.proui.tab.TabView({
                el: mwa.proui.Backbone.$('.proui-global-header-center'),
                viewModel: new mwa.proui.tab.TabViewModel({
                  isStrong: false,
                  type: mwa.proui.tab.TabType.WINDOW,
                  size: mwa.proui.tab.TabSize.NORMAL,
                  minWidth: 140,
                  selectedIndex: 0,
                  contentList: new mwa.proui.tab.TabContentCollection(
                      displayTypeArray.map(
                          function(displayType, inde, array) {
                            return new mwa.proui.tab.TabContentModel({
                              title: displayType.label,
                              viewClass: mwa.View,
                              viewOptions: {
                                viewModel: new mwa.ViewModel()
                              }
                            });
                          }
                      )
                  ),
                  create: function(contentView, index, viewModel, contentViewOptions) {
                    // Do Nothing
                  },
                  select: function(contentView, index, viewModel, contentViewOptions, isSelected) {
                    if (isSelected) {
                      that.$el.find('.mwa-view-workflow-editor-body')
                          .removeClass('proui-state-selected')
                          .eq(index)
                          .addClass('proui-state-selected');
                    }
                  }
                })
              });

              // CAUTION: NVX-7418 (https://www.tool.sony.biz/jira/browse/NVX-7418)
              // It is important to update the display state of children elements instead of the
              // rendering target element ($el) because the element isn't managed by this view, so
              // any style setting of the element shouldn't be updated.
              that.views.displayTypeTabView.$el.children().toggle(
                  that.options.viewModel.get('hasDisplayTypeTab')
              );

              var width =
                  that.views.displayTypeTabView.options.viewModel.get('minWidth') *
                  that.views.displayTypeTabView.options.viewModel.get('contentList').length;
              that.views.displayTypeTabView.$el.css({
                'margin-top':
                    0.5 *
                    (
                        parseInt(
                            mwa.proui.Backbone.$('.proui-global-header').outerHeight(true),
                            10
                        ) -
                        parseInt(
                            that.views.displayTypeTabView.$el.find('.proui-tab-header').outerHeight(true),
                            10
                        )
                    ),
                'margin-left':
                    -0.5 * width,
                width: width,
                height: '100%'
              }).find('.proui-tab-content').hide();

              return that.views.displayTypeTabView;
            },
            /**
             * The function to render activity palette.
             * @protected
             *
             * @returns {prouiExt.palette.PaletteView}
             * The rendered palette view instance.
             */
            renderActivityPalette: function() {
              var that = this;

              if (that.views.activityPaletteView != null) {
                that.views.activityPaletteView.remove();
                that.views.activityPaletteView = null;
              }

              that.views.activityPaletteView = new mwa.prouiExt.palette.PaletteView({
                el: that.$el.find('.mwa-view-workflow-editor-activity-palette'),
                viewModel: new mwa.prouiExt.palette.PaletteViewModel({
                  hasHeader: false,
                  canSelect: true,
                  canDrag: true,
                  label: 'alias',
                  itemCollection: that.options.viewModel.get('activityCollection'),
                  select: function(selectedItemModel) {
                    if (that.views != null && that.views.activityProfileListViewView != null) {
                      that.views.activityProfileListViewView.options.viewModel.set(
                          'activityModel', selectedItemModel
                      );
                    }
                  }
                })
              });

              return that.views.activityPaletteView;
            },
            /**
             * The function to render activity profile list view.
             * @protected
             *
             * @returns {mwa.view.activityProfileListView.ActivityProfileListViewView}
             * The rendered activity profile list view view instance.
             */
            renderActivityProfileListView: function() {
              var that = this;

              if (that.views.activityProfileListViewView != null) {
                that.views.activityProfileListViewView.remove();
                that.views.activityProfileListViewView = null;
              }

              that.views.activityProfileListViewView =
                  new mwa.view.activityProfileListView.ActivityProfileListViewView({
                    el: that.$el.find('.mwa-view-workflow-editor-activity-profile-list-view'),
                    viewModel:
                        new mwa.view.activityProfileListView.ActivityProfileListViewViewModel({
                        })
                  });

              return that.views.activityProfileListViewView;
            },
            /**
             * The function to render workflow navigation expander.
             * @protected
             *
             * @returns {proui.expander.ExpanderView}
             * The rendered expander view instance.
             */
            renderWorkflowNavigationExpander: function() {
              var that = this;

              if (that.views.workflowNavigationExpanderView != null) {
                that.views.workflowNavigationExpanderView.remove();
                that.views.workflowNavigationExpanderView = null;
              }

              var workflowDiagramNavigationTreeViewViewModelAttributes = {
                itemCollection: that.options.viewModel.get('workflowDiagramDetailCollection'),
                rootType: mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition.WORKFLOW_DIAGRAM.name
              };
              var workflowNavigationTreeViewViewModelAttributes = {
                itemCollection: that.options.viewModel.get('workflowDetailCollection'),
                rootType: mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition.WORKFLOW.name
              };

              that.views.workflowNavigationExpanderView =
                  new mwa.proui.expander.ExpanderView({
                    el: that.$el.find('.mwa-view-workflow-editor-workflow-navigation'),
                    viewModel: new mwa.proui.expander.ExpanderViewModel({
                      title: mwa.enumeration.Message['TITLE_NAVIGATION'],
                      position: mwa.proui.expander.Position.LEFT,
                      create: function(contentView, viewModel) {
                        if (contentView instanceof mwa.proui.tab.TabView) {
                          that.views.workflowNavigationTabView = contentView;
                        } else if (contentView instanceof mwa.proui.treeView.TreeViewView) {
                          that.views.workflowNavigationTreeViewView = contentView;
                        }
                      },
                      expand: function(contentView, viewModel) {
                        that.updateWorkflowDiagramAddCommandButton();
                      },
                      collapse: function(contentView, viewModel) {
                        if (that.views != null &&
                            that.views.workflowDiagramAddCommandButtonView != null) {
                          that.views.workflowDiagramAddCommandButtonView.$el.hide();
                        }
                      }
                    }),
                    contentModel: new mwa.proui.expander.ExpanderContentModel({
                      viewClass:
                          (that.options.viewModel.get('hasWorkflowDiagramNavigationTreeView')) ?
                              mwa.proui.tab.TabView :
                              mwa.proui.treeView.TreeViewView,
                      viewOptions: {
                        viewModel:
                            (that.options.viewModel.get('hasWorkflowDiagramNavigationTreeView')) ?
                                new mwa.proui.tab.TabViewModel({
                                  isStrong: true,
                                  type: mwa.proui.tab.TabType.PANE,
                                  size: mwa.proui.tab.TabSize.NORMAL,
                                  contentList: new mwa.proui.tab.TabContentCollection([
                                    new mwa.proui.tab.TabContentModel({
                                      title: mwa.enumeration.Message['TITLE_EDIT'],
                                      viewClass: mwa.proui.treeView.TreeViewView,
                                      viewOptions: {
                                        viewModel: that.createWorkflowNavigationTreeViewViewModel(
                                            workflowDiagramNavigationTreeViewViewModelAttributes
                                        )
                                      }
                                    }),
                                    new mwa.proui.tab.TabContentModel({
                                      title: mwa.enumeration.Message['TITLE_EXECUTION'],
                                      viewClass: mwa.proui.treeView.TreeViewView,
                                      viewOptions: {
                                        viewModel: that.createWorkflowNavigationTreeViewViewModel(
                                            workflowNavigationTreeViewViewModelAttributes
                                        )
                                      }
                                    })
                                  ]),
                                  create: function(contentView, index, viewModel, contentViewOptions) {
                                    var workflowNavigationTreeViewDefinition =
                                        mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition[
                                            contentView.options.viewModel.get('rootType')
                                        ];

                                    switch (workflowNavigationTreeViewDefinition) {
                                      case mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition.WORKFLOW_DIAGRAM:
                                        that.views.workflowDiagramNavigationTreeViewView = contentView;
                                        break;

                                      case mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition.WORKFLOW:
                                        that.views.workflowNavigationTreeViewView = contentView;
                                        break;

                                      default:
                                        break;
                                    }
                                  },
                                  select: function(contentView, index, viewModel, contentViewOptions, isSelected) {
                                    that.updateWorkflowDiagramAddCommandButton();
                                  }
                                }) :
                                that.createWorkflowNavigationTreeViewViewModel(
                                    workflowNavigationTreeViewViewModelAttributes
                                )
                      },
                      canResize: true,
                      width: 300,
                      maxWidth: 500,
                      height: '100%'
                    })
                  });

              return that.views.workflowNavigationExpanderView;
            },
            /**
             * The function to render workflow canvas tab.
             * @protected
             *
             * @returns {proui.tab.TabView}
             * The rendered tab view instance.
             */
            renderWorkflowCanvasTab: function() {
              var that = this;

              if (that.views.workflowCanvasTabView != null) {
                that.views.workflowCanvasTabView.remove();
                that.views.workflowCanvasTabView = null;
              }

              // INFORMATION:
              // If it isn't necessary, it is better not to manage tab content view in consideration
              // of a case that the views will be added or removed in the processing.

              that.views.workflowCanvasTabView = new mwa.proui.tab.TabView({
                el: that.$el.find('.mwa-view-workflow-editor-workflow-canvas-tab'),
                viewModel: new mwa.proui.tab.TabViewModel({
                  hasTooltip: true,
                  hasCloseButton: true,
                  isStrong: true,
                  type: mwa.proui.tab.TabType.NOBACKGROUND,
                  size: mwa.proui.tab.TabSize.NORMAL,
                  maxContent: 10,
                  contentList: new mwa.proui.tab.TabContentCollection(
                      that.options.viewModel.get('selectedWorkflowDiagramCollection').map(
                          mwa.proui._.bind(that.createWorkflowCanvasTabContentModel, that)
                      )
                  ),
                  create: function(contentView, index, viewModel, contentViewOptions) {
                    // Do Nothing
                  },
                  confirm: function(contentView, index, viewModel, contentViewOptions) {
                    return (
                        (this.get('maxContent') < this.get('contentList').length) ?
                            window.alert(mwa.enumeration.Message['INFORMATION_CLOSE_CANVAS']) :
                            (
                                contentView.options.viewModel.get('workflowDiagramModel')
                                    .hasChangedWorkflowItems()
                            ) ?
                                window.confirm(
                                    mwa.enumeration.Message['CONFIRMATION_CLOSE_CHANGE']
                                ) :
                                true
                    );
                  },
                  close: function(contentView, index, viewModel, contentViewOptions) {
                    // CAUTION:
                    // It is important to update selectedWorkflowDiagramCollection attribute of view
                    // model to be able to reopen workflow diagrams.
                    that.options.viewModel.get('selectedWorkflowDiagramCollection').remove(
                        contentView.options.viewModel.get('workflowDiagramModel')
                    );
                  },
                  select: function(contentView, index, viewModel, contentViewOptions, isSelected) {
                    // CAUTION:
                    // It is necessary to consider about that contentView is undefined because there
                    // is a case that select callback will be called with index that is out of range
                    // in the content list (e.g. -1).
                    if (contentView instanceof mwa.proui.Backbone.View && isSelected) {
                      contentView.$el.find('.mwa-view-workflow-canvas').focus();
                    }
                  }
                })
              });

              // CAUTION:
              // It is necessary to set "position: absolute;" style to child element of flex item
              // element to have height in the element.
              //  - How to make flexbox children 100% height of their parent?
              //    (http://stackoverflow.com/questions/15381172/how-to-make-flexbox-children-100-height-of-their-parent)
              that.views.workflowCanvasTabView.$el.children().css('position', 'absolute');

              return that.views.workflowCanvasTabView;
            },
            /**
             * The function to render workflow diagram add button.
             * @protected
             *
             * @returns {proui.commandButton.CommandButtonView}
             * The rendered command button view instance.
             */
            renderWorkflowDiagramAddButton: function() {
              var that = this;

              if (that.views.workflowDiagramAddCommandButtonView != null) {
                that.views.workflowDiagramAddCommandButtonView.remove();
                that.views.workflowDiagramAddCommandButtonView = null;
              }

              var $el = that.$el.find('.mwa-view-workflow-editor-workflow-diagram-add-button');
              that.views.workflowDiagramAddCommandButtonView =
                  new mwa.proui.commandButton.CommandButtonView({
                    el: $el,
                    viewModel: new mwa.proui.commandButton.CommandButtonViewModel({
                      icon: $el.css('background-image'),
                      type: mwa.proui.commandButton.ButtonType.NONE,
                      size: mwa.proui.commandButton.ButtonSize.SMALL,
                      tooltip: mwa.enumeration.Message['FOOTNOTE_ADD_NEW_WORKFLOW_DIAGRAM'],
                      command: function() {
                        that.renderItemSaveDialog(
                            new mwa.model.workflowDiagram.WorkflowDiagramCollection(
                                // CAUTION:
                                // It is necessary to create workflow diagram detail model instead
                                // of workflow diagram model because the instance will be checked in
                                // createWorkflowCanvasTabContentModel and
                                // createWorkflowCanvasTabTitle functions to resolve workflow canvas
                                // icon and title.
                                that.options.viewModel.createWorkflowDiagramDetailModel()
                            ),
                            {
                              success: function(collection, response, options) {
                                // CAUTION: NVX-5669 (https://www.tool.sony.biz/jira/browse/NVX-5669)
                                // It is necessary to consider about multiple errors case.
                                if (response.errors == null ||
                                    response.errors.length === 0) {
                                  var workflowDiagramDetailModel =
                                      that.options.viewModel
                                          .get('workflowDiagramDetailCollection')
                                          // CAUTION:
                                          // It is necessary to use response.models instead of
                                          // collection.models because the generated ID isn't set to
                                          // the saved model.
                                          .get(response.models[0].id);

                                  if (workflowDiagramDetailModel instanceof
                                      mwa.model.workflowDiagram.WorkflowDiagramModel &&
                                      that.views != null) {
                                    if (that.views.workflowNavigationTabView != null &&
                                        that.views.workflowDiagramNavigationTreeViewView != null) {
                                      that.views.workflowNavigationTabView.options.viewModel
                                          .set(
                                              'selectedIndex',
                                              findTabIndex(
                                                  that.views.workflowNavigationTabView,
                                                  that.views.workflowDiagramNavigationTreeViewView
                                              )
                                          );
                                    }

                                    if (that.views.workflowDiagramNavigationTreeViewView != null) {
                                      that.views.workflowDiagramNavigationTreeViewView.options.viewModel
                                          .execute(workflowDiagramDetailModel);
                                    }
                                  }
                                }
                              }
                            }
                        );
                      }
                    })
                  });
              $el.css('background-image', 'none');

              return that.views.workflowDiagramAddCommandButtonView;
            },
            /**
             * The function to render confirmation message box.
             * @protected
             *
             * @param {?object} [options]
             * The options for message box view model.
             *
             * @returns {prouiExt.messageBox.MessageBoxView}
             * The rendered message box view instance.
             */
            renderConfirmationMessageBox: function(options) {
              var that = this;

              if (that.views.confirmationMessageBoxView != null) {
                that.views.confirmationMessageBoxView.remove();
                that.views.confirmationMessageBoxView = null;
              }

              that.views.confirmationMessageBoxView =
                  new mwa.prouiExt.messageBox.MessageBoxView({
                    viewModel: new mwa.prouiExt.messageBox.MessageBoxViewModel(
                        mwa.proui._.extend(
                            {
                              type: mwa.prouiExt.messageBox.MessageBoxType.CONFIRMATION
                            },
                            options,
                            {
                              title: mwa.enumeration.Message['TITLE_CONFIRMATION'],
                              // CAUTION:
                              // It is important to disable auto destroy to reopen message box in
                              // return callback after pressing any button mainly for displaying
                              // loading during any request.
                              autoDestroy: false,
                              buttonArray: [
                                mwa.enumeration.Message['TITLE_YES'],
                                mwa.enumeration.Message['TITLE_NO']
                              ],
                              // CAUTION:
                              // It is necessary to use string declaration because "return" is a
                              // reserved name in JavaScript and an error will occur in build
                              // processing (e.g. JSDoc generation, YUI Compressor).
                              'return': function(index) {
                                // CAUTION:
                                // It is necessary to open message box explicitly because it has
                                // been automatically closed as default behavior before calling this
                                // callback.
                                that.views.confirmationMessageBoxView.options.viewModel.set(
                                    'isOpen', true
                                );

                                // CAUTION:
                                // It is necessary to use string declaration because "return" is a
                                // reserved name in JavaScript and an error will occur in build
                                // processing (e.g. JSDoc generation, YUI Compressor).
                                if (typeof (options || {})['return'] === 'function') {
                                  options['return'](index);
                                }

                                switch (index) {
                                  case 1:
                                    that.views.confirmationMessageBoxView.remove();
                                    break;

                                  default:
                                    break;
                                }
                              }
                            }
                        )
                    )
                  });

              return that.views.confirmationMessageBoxView;
            },
            /**
             * The function to render item save dialog.
             * @protected
             *
             * @param {mwa.model.workflowDiagram.WorkflowDiagramCollection|mwa.model.workflow.WorkflowCollection|mwa.model.activityProfileGroup.ActivityProfileGroupCollection|mwa.model.activityProfile.ActivityProfileCollection} itemCollection
             * The collection that has item model to be saved.
             * @param {object} [options]
             * The options of the save request.
             *
             * @returns {prouiExt.metadataEditorDialog.MetadataEditorDialogView}
             * The rendered metadata editor dialog view instance.
             */
            renderItemSaveDialog: function(itemCollection, options) {
              var that = this;

              if (that.views.itemSaveDialogView != null) {
                that.views.itemSaveDialogView.remove();
                that.views.itemSaveDialogView = null;
              }

              var success = (options || {}).success;
              var itemModel = itemCollection.first();
              var metadataFieldCollection =
                  new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                      itemModel.createEditingMetadataFieldCollection().filter(
                          function(editingMetadataFieldModel, index, array) {
                            switch (true) {
                              // INFORMATION: NVXN-1711 (https://acropolis.atlassian.net/browse/NVXN-1711)
                              case itemModel instanceof mwa.model.activityProfile.ActivityProfileModel:
                                return (editingMetadataFieldModel.get('key') === 'schedulableFlag') ?
                                    that.options.viewModel.get('canSchedule') :
                                    true;

                              default:
                                return true;
                            }
                          }
                      )
                  );
              var metadataGroupCollection = itemModel.createEditingMetadataGroupCollection();
              // CAUTION:
              // It is necessary to cache selected item of workflow navigation tree view at this
              // timing in consideration of asynchronous request in order to use it after save.
              var workflowNavigationTreeViewSelectedItemModel =
                  (that.views.workflowNavigationTreeViewView == null) ?
                      null :
                      that.views.workflowNavigationTreeViewView.options.viewModel
                          .get('lastSelectedItemModel');

              that.views.itemSaveDialogView =
                  new mwa.prouiExt.metadataEditorDialog.MetadataEditorDialogView({
                    viewModel: new mwa.prouiExt.metadataEditorDialog.MetadataEditorDialogViewModel({
                      hasEditingMark: !itemModel.isNew(),
                      title: (itemModel.isNew()) ?
                          mwa.enumeration.Message['TITLE_ADD'] :
                          (
                              (
                                  metadataFieldCollection.length === 1 &&
                                  metadataFieldCollection.first().get('title') ===
                                  mwa.enumeration.Message['SENTENCE_NAME']
                              ) ?
                                  mwa.enumeration.Message['TITLE_RENAME'] :
                                  mwa.enumeration.Message['TITLE_EDIT']
                          ),
                      width: 500 * metadataGroupCollection.length,
                      itemCollection: itemCollection,
                      itemAttributes:
                          // CAUTION:
                          // It is necessary to set itemAttributes attribute of metadata editor
                          // dialog view model to save all parameters even if any parameter won't be
                          // edited.
                          (itemModel instanceof mwa.model.activityProfile.ActivityProfileModel) ?
                              // CAUTION: NVX-5721 (https://www.tool.sony.biz/jira/browse/NVX-5721)
                              // It is necessary to prepare a new object not to affect the original
                              // instance.
                              {parameter: mwa.proui._.extend({}, itemModel.get('parameter'))} :
                              {},
                      metadataFieldCollection: metadataFieldCollection,
                      metadataGroupCollection: metadataGroupCollection,
                      save: function(itemAttributes) {
                        that.views.itemSaveDialogView.renderLoading();

                        var deferred = null;
                        if (itemModel.isNew()) {
                          deferred =
                              itemCollection.save(
                                  null, // not partial update
                                  mwa.proui._.extend(
                                      {
                                        // If you need default options, add here.
                                      },
                                      options,
                                      {
                                        // CAUTION: NVX-6111 (https://www.tool.sony.biz/jira/browse/NVX-6111)
                                        //          NVX-7374 (https://www.tool.sony.biz/jira/browse/NVX-7374)
                                        // It is important to disable any update to be able to
                                        // continue the operation even after an error occurs because
                                        // the state of metadata editor dialog is changed to
                                        // read-only when update is happened and editing values are
                                        // lost accidentally due to empty response if any update
                                        // logic exists (e.g. NVX).
                                        add: false,
                                        remove: false,
                                        merge: false,
                                        // CAUTION:
                                        // It is necessary to prepare success callback instead of
                                        // done callback in consideration of that done callback will
                                        // be executed after success callback because the following
                                        // processing should be executed before anything else.
                                        success: function(collection, response, options) {
                                          // CAUTION: NVX-5669 (https://www.tool.sony.biz/jira/browse/NVX-5669)
                                          // It is necessary to consider about multiple errors case.
                                          if (response.errors == null ||
                                              response.errors.length === 0) {
                                            itemModel.set(
                                                // CAUTION:
                                                // It is necessary to parse the response manually
                                                // because set function doesn't support parse.
                                                itemModel.parse(response.models[0])
                                            );

                                            if (itemModel instanceof mwa.model.workflowDiagram.WorkflowDiagramModel &&
                                                // CAUTION:
                                                // It is important to consider about that workflow
                                                // model is extended from workflow diagram model.
                                                !(itemModel instanceof mwa.model.workflow.WorkflowModel)) {
                                              that.options.viewModel
                                                  .get('workflowDiagramDetailCollection')
                                                  .add(itemModel);

                                              if (that.views != null &&
                                                  that.views.workflowDiagramNavigationTreeViewView != null) {
                                                that.views.workflowDiagramNavigationTreeViewView.options.viewModel
                                                    .set('lastSelectedItemModel', itemModel);
                                              }
                                            } else {
                                              for (var propertyName in
                                                  mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition) {
                                                if (mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition.hasOwnProperty(propertyName)) {
                                                  var workflowNavigationTreeViewDefinition =
                                                      mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition[propertyName];
                                                  var workflowNavigationTreeViewDefinitionModelClass =
                                                      workflowNavigationTreeViewDefinition.modelClass ||
                                                      (
                                                          workflowNavigationTreeViewDefinition.collectionClass ||
                                                          {prototype: {model: null}}
                                                      ).prototype.model;
                                                  if (typeof workflowNavigationTreeViewDefinitionModelClass !== 'undefined' &&
                                                      workflowNavigationTreeViewDefinitionModelClass != null &&
                                                      workflowNavigationTreeViewSelectedItemModel instanceof
                                                      workflowNavigationTreeViewDefinitionModelClass) {
                                                    for (propertyName in
                                                        workflowNavigationTreeViewDefinition.children) {
                                                      if (workflowNavigationTreeViewDefinition.children.hasOwnProperty(propertyName)) {
                                                        var workflowNavigationTreeViewDefinitionChildren =
                                                            mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition[propertyName];
                                                        var workflowNavigationTreeViewDefinitionChildrenModelClass =
                                                            workflowNavigationTreeViewDefinitionChildren.modelClass ||
                                                            (
                                                                workflowNavigationTreeViewDefinitionChildren.collectionClass ||
                                                                {prototype: {model: null}}
                                                            ).prototype.model;
                                                        if (typeof workflowNavigationTreeViewDefinitionChildrenModelClass !== 'undefined' &&
                                                            workflowNavigationTreeViewDefinitionChildrenModelClass != null &&
                                                            itemModel instanceof
                                                            workflowNavigationTreeViewDefinitionChildrenModelClass) {
                                                          workflowNavigationTreeViewSelectedItemModel
                                                              .get(workflowNavigationTreeViewDefinition.children[propertyName])
                                                              .add(itemModel);

                                                          break;
                                                        }
                                                      }
                                                    }

                                                    break;
                                                  }
                                                }
                                              }

                                              if (that.views != null &&
                                                  that.views.workflowNavigationTreeViewView != null) {
                                                that.views.workflowNavigationTreeViewView.options.viewModel
                                                    .set('lastSelectedItemModel', itemModel);
                                              }
                                            }
                                          }

                                          if (typeof success === 'function') {
                                            success(collection, response, options);
                                          }
                                        }
                                      }
                                  )
                              );
                        } else {
                          deferred =
                              itemModel.save(
                                  itemAttributes,
                                  mwa.proui._.extend(
                                      {},
                                      options,
                                      {
                                        patch: true
                                      }
                                  )
                              );
                        }

                        deferred.done(
                            function(data, textStatus, xhr) {
                              // CAUTION:
                              // It is necessary to consider about multiple errors case.
                              if (data.errors == null || data.errors.length === 0) {
                                that.views.itemSaveDialogView.remove();
                                // CAUTION:
                                // It is important not to assign null because the instance will be
                                // referred in always callback after this processing.
                              }
                            }
                        ).always(
                            // INFORMATION:
                            // The arguments are changed based on the result. If success, the
                            // arguments are same as done callback. If failure, the arguments are
                            // same as fail callback.
                            function() {
                              that.views.itemSaveDialogView.removeLoading();
                            }
                        );
                      }
                    })
                  });

              return that.views.itemSaveDialogView;
            },
            /**
             * The function to render item move copy dialog.
             * @protected
             *
             * @param {mwa.model.activityProfile.ActivityProfileCollection} itemCollection
             * The collection that has item model to be saved.
             * @param {object} [options]
             * The options of the save request.
             *
             * @returns {prouiExt.messageBox.MessageBoxView}
             * The rendered message box view instance.
             */
            renderItemMoveCopyDialog: function(itemCollection, options) {
              var that = this;

              options = options || {};

              var success = options.success;
              var error = options.error;
              var itemModel = itemCollection.first();

              return that.renderConfirmationMessageBox(
                  {
                    type: mwa.prouiExt.messageBox.MessageBoxType.INFORMATION,
                    text: (itemModel.isNew()) ?
                        mwa.enumeration.Message['CONFIRMATION_COPY'] :
                        mwa.enumeration.Message['CONFIRMATION_MOVE'],
                    'return': function(index) {
                      if (index === 0) {
                        that.views.confirmationMessageBoxView.renderLoading();

                        itemCollection.save(
                            null, // not partial update
                            mwa.proui._.extend(
                                {
                                  // If you need default options, add here.
                                },
                                options,
                                {
                                  // CAUTION:
                                  // It is necessary to prepare success callback instead of done
                                  // callback in consideration of that done callback will be
                                  // executed after success callback because the following
                                  // processing should be executed before anything else.
                                  success: function(collection, response, options) {
                                    // CAUTION: NVX-5669 (https://www.tool.sony.biz/jira/browse/NVX-5669)
                                    // It is necessary to consider about multiple errors case.
                                    if (response.errors == null ||
                                        response.errors.length === 0) {
                                      itemModel.set(
                                          // CAUTION:
                                          // It is necessary to parse the response manually because
                                          // set function doesn't support parse.
                                          itemModel.parse(response.models[0])
                                      );

                                      if (itemModel instanceof
                                          mwa.model.activityProfileGroup.ActivityProfileGroupModel ||
                                          itemModel instanceof
                                          mwa.model.activityProfile.ActivityProfileModel) {
                                        if (that.views != null &&
                                            that.views.workflowNavigationTreeViewView != null) {
                                          that.views.workflowNavigationTreeViewView.options.viewModel
                                              .set('lastSelectedItemModel', itemModel);
                                        }
                                      }
                                    }

                                    if (typeof success === 'function') {
                                      success(collection, response, options);
                                    }
                                  }
                                }
                            )
                        ).done(
                            function(data, textStatus, xhr) {
                              // CAUTION:
                              // It is necessary to consider about multiple errors case.
                              if (data.errors == null || data.errors.length === 0) {
                                that.views.confirmationMessageBoxView.remove();
                                // CAUTION:
                                // It is important not to assign null because the instance will be
                                // referred in always callback after this processing.
                              }
                            }
                        ).always(
                            // INFORMATION:
                            // The arguments are changed based on the result. If success, the
                            // arguments are same as done callback. If failure, the arguments are
                            // same as fail callback.
                            function() {
                              that.views.confirmationMessageBoxView.removeLoading();
                            }
                        );
                      } else {
                        // CAUTION: NVX-5266 (https://www.tool.sony.biz/jira/browse/NVX-5266)
                        //          NVX-5662 (https://www.tool.sony.biz/jira/browse/NVX-5662)
                        // This code means to revert workflow navigation tree view state to that
                        // before the operation.
                        if (typeof error === 'function') {
                          // INFORMATION:
                          // It is better to pass an empty object instead of null for the second
                          // argument in consideration of actual response as much as possible.
                          error(itemCollection, {}, options);
                        }
                      }
                    }
                  }
              );
            },
            /**
             * The function to render item delete dialog.
             * @protected
             *
             * @param {mwa.model.workflowDiagram.WorkflowDiagramCollection|mwa.model.workflow.WorkflowCollection|mwa.model.activityProfileGroup.ActivityProfileGroupCollection|mwa.model.activityProfile.ActivityProfileCollection} itemCollection
             * The collection that has item model to be deleted.
             * @param {object} [options]
             * The options of the delete request.
             *
             * @returns {prouiExt.messageBox.MessageBoxView}
             * The rendered message box view instance.
             */
            renderItemDeleteDialog: function(itemCollection, options) {
              var that = this;

              return that.renderConfirmationMessageBox(
                  {
                    text: mwa.enumeration.Message['CONFIRMATION_DELETE'],
                    'return': function(index) {
                      if (index === 0) {
                        itemCollection.destroy(
                            itemCollection.models,
                            mwa.proui._.extend(
                                {
                                  // CAUTION:
                                  // It is necessary to set "wait" option (e.g. to close
                                  // corresponding workflow canvas tab after that delete will be
                                  // succeeded).
                                  wait: true
                                },
                                options
                            )
                        );

                        if (that.views != null && that.views.confirmationMessageBoxView != null) {
                          that.views.confirmationMessageBoxView.remove();
                        }
                      }
                    }
                  }
              );
            },
            /**
             * The event handler of beforeunload event on window.
             * @protected
             *
             * @param {object} event
             * The beforeunload event object.
             *
             * @returns {string|undefined}
             * The string to display as confirmation message.
             */
            beforeUnloadWindow: function(event) {
              return (
                  (
                      this.options.viewModel.get('selectedWorkflowDiagramCollection').some(
                          function(selectedWorkflowDiagramModel, index, array) {
                            return selectedWorkflowDiagramModel.hasChangedWorkflowItems();
                          }
                      )
                  ) ?
                      // CAUTION: NVX-5751 (https://www.tool.sony.biz/jira/browse/NVX-5751)
                      //  - Starting with Chrome 51 custom message in onbeforeunload is deprecated
                      //    (https://github.com/nwjs/nw.js/issues/5872#issuecomment-341619749)
                      //  - Remove custom messages in onbeforeunload dialogs
                      //    (https://developers.google.com/web/updates/2016/04/chrome-51-deprecations#remove_custom_messages_in_onbeforeunload_dialogs)
                      mwa.enumeration.Message['CONFIRMATION_LEAVE_CHANGE'] :
                      // CAUTION: NVX-5751 (https://www.tool.sony.biz/jira/browse/NVX-5751)
                      // It is necessary to return undefined instead of null in order not to show
                      // browser confirmation.
                      //  - Any bad side effects to window.onbeforeunload returning no value?
                      //    (https://stackoverflow.com/a/42906835)
                      undefined
              );
            },
            /**
             * The event handler of click event on workflow diagram add annotation element.
             * @protected
             *
             * @param {object} event
             * The click event object.
             *
             * @returns {?boolean}
             * The flag whether event bubbling continue.
             */
            clickWorkflowDiagramAddAnnotation: function(event) {
              var that = this;

              if (that.views != null && that.views.workflowDiagramAddCommandButtonView != null) {
                that.views.workflowDiagramAddCommandButtonView.options.viewModel.command();
              }

              return true;
            },
            /**
             * The function to create workflow canvas tab title based on specified workflow diagram.
             * @public
             *
             * @param {!mwa.model.workflowDiagram.WorkflowDiagramModel} workflowDiagramModel
             * The workflow diagram model to be created workflow canvas tab title.
             *
             * @returns {string}
             * The created workflow canvas tab title.
             */
            createWorkflowCanvasTabTitle: function(workflowDiagramModel) {
              var isWorkflowDiagramDetailModel =
                  workflowDiagramModel instanceof
                  mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel;

              return (
                  (
                      (isWorkflowDiagramDetailModel) ?
                          workflowDiagramModel.get('name') :
                          workflowDiagramModel.get('alias')
                  ) +
                  (
                      (isWorkflowDiagramDetailModel) ?
                          '' :
                          ' ' + '[' +
                          mwa.enumeration.Message['TITLE_READ_ONLY'] +
                          ']'
                  ) +
                  (
                      (workflowDiagramModel.hasChangedWorkflowItems()) ?
                          ' *' :
                          ''
                  )
              );
            },
            /**
             * The function to create workflow navigation tree view view model.
             * @protected
             *
             * @param {?object} [attributes]
             * The object including various attribute values of tree view view model.
             *
             * @returns {proui.treeView.TreeViewViewModel}
             * The created tree view view model instance.
             */
            createWorkflowNavigationTreeViewViewModel: function(attributes) {
              var that = this;

              return new mwa.proui.treeView.TreeViewViewModel(
                  mwa.proui._.extend(
                      {
                        definitions: mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition,
                        itemCollection: new mwa.proui.Backbone.Collection([
                          {
                            name: mwa.enumeration.Message['TITLE_DEFAULT'],
                            children:
                                that.options.viewModel.get('workflowDiagramDetailCollection')
                          }
                        ]),
                        rootType: mwa.view.workflowEditor.WorkflowNavigationTreeViewDefinition.WORKFLOW_DIAGRAM_ROOT.name,
                        canSelect: true,
                        canEdit: false,
                        canDrag: true,
                        canDrop: true,
                        dropAction: mwa.proui.treeView.DropAction.COPY_MOVE,
                        select: function(selectedItemModel, isSelected) {
                          updateWorkflowNavigationTreeViewContextMenu(selectedItemModel);
                        },
                        execute: function(selectedItemModel) {
                          // CAUTION:
                          // It is important to check whether the selected item model is workflow
                          // diagram model in consideration of that workflow diagram detail model
                          // and workflow model are extended from workflow diagram model.
                          if (selectedItemModel instanceof
                              mwa.model.workflowDiagram.WorkflowDiagramModel) {
                            var selectedWorkflowDiagramCollection =
                                that.options.viewModel.get('selectedWorkflowDiagramCollection');

                            selectedWorkflowDiagramCollection.add(selectedItemModel);

                            // INFORMATION:
                            // It is necessary to implement the following codes here, not event
                            // handler of add event for selectedWorkflowDiagramCollection, in
                            // consideration of a case that selectedItemModel has already been
                            // added to selectedWorkflowDiagramCollection.
                            if (that.views != null && that.views.workflowCanvasTabView != null) {
                              var contentCollection =
                                  that.views.workflowCanvasTabView.options.viewModel
                                      .get('contentList');

                              // CAUTION:
                              // It is important not to update "selectedIndex" attribute of workflow
                              // canvas tab view model unnecessarily if the selected workflow
                              // diagram hasn't been displayed yet in consideration of that it will
                              // be asynchronously displayed after fetch.
                              if (selectedWorkflowDiagramCollection.length ===
                                  contentCollection.length) {
                                that.views.workflowCanvasTabView.options.viewModel.set(
                                    'selectedIndex',
                                    contentCollection.findIndex(
                                        function(tabContentModel, index, array) {
                                          return (
                                              tabContentModel.get('id') ===
                                              selectedItemModel.get('id')
                                          );
                                        }
                                    )
                                );
                              }
                            }
                          }
                        },
                        command: function(selectedItemModel, selectedContextMenuItemModel) {
                          var selectedWorkflowNavigationTreeViewDefinition =
                              getWorkflowNavigationTreeViewDefinition(
                                  selectedItemModel
                              );
                          var selectedContextMenu =
                              getWorkflowNavigationTreeViewContextMenu(
                                  selectedItemModel
                              )[selectedContextMenuItemModel.get('name')];
                          var selectedItemCollection =
                              new (selectedWorkflowNavigationTreeViewDefinition.collectionClass)(
                                  selectedItemModel
                              );
                          var selectedWorkflowModel =
                              (selectedItemModel instanceof mwa.model.workflow.WorkflowModel) ?
                                  selectedItemModel :
                                  that.options.viewModel
                                      .get('workflowDetailCollection')
                                      .get(selectedItemModel.get('templateId')) ||
                                  // CAUTION:
                                  // It is necessary to consider about a case that selected item is
                                  // workflow diagram.
                                  new mwa.model.workflow.WorkflowModel(null, {validate: false});
                          // INFORMATION:
                          // The following variables are object type, not array. But we use plural
                          // name based on our JavaScript coding rule.
                          var selectedWorkflowParameters =
                              selectedWorkflowModel.getParameter();
                          // CAUTION:
                          // It is necessary to pick the attributes only regarding activity because
                          // stack overflow will be happened in the processing for flattening values
                          // of metadata editor if there are additional attributes (e.g. workflow).
                          var selectedActivities =
                              mwa.proui._.pick(
                                  selectedWorkflowModel.toJSON(),
                                  mwa.proui._.keys(
                                      mwa.proui._.result(
                                          mwa.model.activity.ActivityModel.prototype,
                                          'defaults'
                                      )
                                  )
                              );

                          switch (selectedContextMenu) {
                            case mwa.enumeration.WorkflowDiagramRootContextMenu.ADD:
                              that.renderItemSaveDialog(
                                  new mwa.model.workflowDiagram.WorkflowDiagramCollection(
                                      // CAUTION:
                                      // It is necessary to create workflow diagram detail model
                                      // instead of workflow diagram model because the instance will
                                      // be checked in createWorkflowCanvasTabContentModel and
                                      // createWorkflowCanvasTabTitle functions to resolve workflow
                                      // canvas icon and title.
                                      that.options.viewModel.createWorkflowDiagramDetailModel()
                                  )
                              );
                              break;

                            //// Workflow Diagram ////

                            case mwa.enumeration.WorkflowDiagramContextMenu.RENAME:
                              // TODO(yasuda): NVX-5255
                              that.renderItemSaveDialog(selectedItemCollection);
                              break;

                            case mwa.enumeration.WorkflowDiagramContextMenu.SAVE:
                              selectedItemModel.save();
                              break;

                            case mwa.enumeration.WorkflowDiagramContextMenu.DEPLOY:
                              that.renderConfirmationMessageBox(
                                  {
                                    type: mwa.prouiExt.messageBox.MessageBoxType.INFORMATION,
                                    text: mwa.enumeration.Message['CONFIRMATION_DEPLOY'],
                                    'return': function(index) {
                                      if (index === 0) {
                                        that.views.confirmationMessageBoxView.renderLoading();

                                        selectedItemModel.save(
                                            null,
                                            {
                                              success: function(model, response, options) {
                                                var workflowDetailModel =
                                                    that.options.viewModel.createWorkflowDetailModel().set(
                                                        mwa.proui._.extend(
                                                            selectedItemModel.toJSON(),
                                                            {
                                                              // CAUTION:
                                                              // It is important to set null to id
                                                              // attribute of workflow detail model
                                                              // to send the request as POST.
                                                              id: null,
                                                              diagramId:
                                                                  selectedItemModel.get('id'),
                                                              name:
                                                                  selectedItemModel.get('name') +
                                                                  '_' +
                                                                  (new Date().getTime()),
                                                              alias:
                                                                  selectedItemModel.get('name'),
                                                              inputs:
                                                                  (
                                                                      mwa.proui._.find(
                                                                          selectedItemModel
                                                                              .get('steps'),
                                                                          // INFORMATION:
                                                                          // The first argument is
                                                                          // object type, not array.
                                                                          // But we use plural name
                                                                          // based on our JavaScript
                                                                          // coding rule.
                                                                          function(steps, index, array) {
                                                                            return (
                                                                                steps.activity.type ===
                                                                                mwa.enumeration.ActivityType.START.name
                                                                            );
                                                                          }
                                                                      ) ||
                                                                      {
                                                                        activity: {
                                                                          // CAUTION:
                                                                          // The value should be
                                                                          // specified in
                                                                          // consideration of
                                                                          // validation setting of
                                                                          // ActivityModel.
                                                                          inputs: []
                                                                        }
                                                                      }
                                                                  ).activity.inputs,
                                                              outputs:
                                                                  (
                                                                      mwa.proui._.find(
                                                                          selectedItemModel
                                                                              .get('steps'),
                                                                          // INFORMATION:
                                                                          // The first argument is
                                                                          // object type, not array.
                                                                          // But we use plural name
                                                                          // based on our JavaScript
                                                                          // coding rule.
                                                                          function(steps, index, array) {
                                                                            return (
                                                                                steps.activity.type ===
                                                                                mwa.enumeration.ActivityType.END.name
                                                                            );
                                                                          }
                                                                      ) ||
                                                                      {
                                                                        activity: {
                                                                          // CAUTION:
                                                                          // The value should be
                                                                          // specified in
                                                                          // consideration of
                                                                          // validation setting
                                                                          // of ActivityModel.
                                                                          outputs: []
                                                                        }
                                                                      }
                                                                  ).activity.outputs
                                                            }
                                                        )
                                                    );

                                                new mwa.model.workflow.WorkflowCollection(
                                                    workflowDetailModel
                                                ).save(
                                                    {
                                                      success: function(collection, response, options) {
                                                        // CAUTION:
                                                        // It is necessary to consider about
                                                        // multiple errors case.
                                                        if (response.errors == null ||
                                                            response.errors.length === 0) {
                                                          workflowDetailModel.set(
                                                              // CAUTION:
                                                              // It is necessary to parse the
                                                              // response manually because set
                                                              // function doesn't support parse.
                                                              workflowDetailModel.parse(
                                                                  response.models[0]
                                                              )
                                                          );

                                                          that.options.viewModel
                                                              .get('workflowDetailCollection')
                                                              .add(workflowDetailModel);

                                                          if (that.views != null) {
                                                            if (that.views.workflowNavigationTabView != null &&
                                                                that.views.workflowNavigationTreeViewView != null) {
                                                              that.views.workflowNavigationTabView.options.viewModel
                                                                  .set(
                                                                      'selectedIndex',
                                                                      findTabIndex(
                                                                          that.views.workflowNavigationTabView,
                                                                          that.views.workflowNavigationTreeViewView
                                                                      )
                                                                  );
                                                            }

                                                            if (that.views.workflowNavigationTreeViewView != null) {
                                                              that.views.workflowNavigationTreeViewView.options.viewModel
                                                                  .set(
                                                                      'lastSelectedItemModel',
                                                                      workflowDetailModel
                                                                  )
                                                                  .execute(workflowDetailModel);
                                                            }
                                                          }
                                                        }
                                                      },
                                                      complete: function(collection, response, options) {
                                                        // TODO NVXN-2488 (https://acropolis.atlassian.net/browse/NVXN-2488)
                                                        // This process will be fixed when NVXN-2488 finished.
                                                        var data = response.responseJSON;

                                                        if (typeof data !== 'undefined' && data != null &&
                                                            data.errors != null && data.errors.length !== 0) {
                                                          new mwa.prouiExt.messageBox.MessageBoxView({
                                                            viewModel: new mwa.prouiExt.messageBox.MessageBoxViewModel({
                                                              type: mwa.prouiExt.messageBox.MessageBoxType.ERROR,
                                                              title: mwa.enumeration.Message['TITLE_ERROR'],
                                                              buttonArray: [mwa.enumeration.Message['TITLE_CLOSE']],
                                                              text: data.errors[0].message
                                                            })
                                                          });
                                                        }

                                                        if (that.views != null &&
                                                            that.views.confirmationMessageBoxView != null) {
                                                          that.views.confirmationMessageBoxView.remove();
                                                        }
                                                      }
                                                    }
                                                );
                                              },
                                              error: function(model, response, options) {
                                                if (that.views != null &&
                                                    that.views.confirmationMessageBoxView != null) {
                                                  that.views.confirmationMessageBoxView.remove();
                                                }
                                              }
                                            }
                                        );
                                      }
                                    }
                                  }
                              );
                              break;

                            case mwa.enumeration.WorkflowDiagramContextMenu.DELETE:
                              that.renderItemDeleteDialog(selectedItemCollection);
                              break;

                            //// Workflow ////

                            case mwa.enumeration.WorkflowContextMenu.RENAME:
                              that.renderItemSaveDialog(selectedItemCollection);
                              break;

                            case mwa.enumeration.WorkflowContextMenu.ADD_ACTIVITY_PROFILE_GROUP:
                              that.renderItemSaveDialog(
                                  new mwa.model.activityProfileGroup.ActivityProfileGroupCollection(
                                      that.options.viewModel.createActivityProfileGroupDetailModel().set({
                                        templateId: selectedItemModel.get('id'),
                                        name: mwa.enumeration.Message['SENTENCE_NEW_WORKFLOW_PRESET_GROUP']
                                      })
                                  )
                              );
                              break;

                            case mwa.enumeration.WorkflowContextMenu.ADD_ACTIVITY_PROFILE:
                              that.renderItemSaveDialog(
                                  new mwa.model.activityProfile.ActivityProfileCollection(
                                      that.options.viewModel.createActivityProfileModel().set({
                                        templateId: selectedItemModel.get('id'),
                                        name: mwa.enumeration.Message['SENTENCE_NEW_WORKFLOW_PRESET'],
                                        // INFORMATION:
                                        // To be careful about attribute name.
                                        parameter: selectedWorkflowParameters,
                                        // INFORMATION:
                                        // To be careful about attribute name.
                                        activity: selectedActivities
                                      })
                                  )
                              );
                              break;

                            case mwa.enumeration.WorkflowContextMenu.DELETE:
                              that.renderItemDeleteDialog(selectedItemCollection);
                              break;

                            //// Workflow Preset Group ////

                            case mwa.enumeration.WorkflowPresetGroupContextMenu.EDIT:
                              that.renderItemSaveDialog(selectedItemCollection);
                              break;

                            case mwa.enumeration.WorkflowPresetGroupContextMenu.ADD_ACTIVITY_PROFILE:
                              that.renderItemSaveDialog(
                                  new mwa.model.activityProfile.ActivityProfileCollection(
                                      that.options.viewModel.createActivityProfileModel().set({
                                        groupId: selectedItemModel.get('id'),
                                        templateId: selectedItemModel.get('templateId'),
                                        name: mwa.enumeration.Message['SENTENCE_NEW_WORKFLOW_PRESET'],
                                        // INFORMATION:
                                        // To be careful about attribute name.
                                        parameter: selectedWorkflowParameters,
                                        // INFORMATION:
                                        // To be careful about attribute name.
                                        activity: selectedActivities
                                      })
                                  )
                              );
                              break;

                            case mwa.enumeration.WorkflowPresetGroupContextMenu.DELETE:
                              that.renderItemDeleteDialog(selectedItemCollection);
                              break;

                            //// Workflow Preset ////

                            case mwa.enumeration.WorkflowPresetContextMenu.EDIT:
                              // CAUTION:
                              // It is necessary to set activity to create editing metadata field
                              // collection in consideration of type.
                              selectedItemModel.set({
                                // INFORMATION:
                                // To be careful about attribute name.
                                activity: selectedActivities
                              });

                              that.renderItemSaveDialog(selectedItemCollection);
                              break;

                            case mwa.enumeration.WorkflowPresetContextMenu.DELETE:
                              that.renderItemDeleteDialog(selectedItemCollection);
                              break;

                            default:
                              break;
                          }
                        },
                        drop: function(draggedItemModelArray, droppedItemModel, dropAction) {
                          // INFORMATION:
                          // The drop call back function may be passed a model or an array of models
                          // based on whether single item is dropped or multiple items are dropped.
                          draggedItemModelArray = [].concat(draggedItemModelArray);

                          var draggedWorkflowNavigationTreeViewDefinition =
                              getWorkflowNavigationTreeViewDefinition(
                                  draggedItemModelArray[0]
                              );
                          var draggedItemCollection =
                              new (draggedWorkflowNavigationTreeViewDefinition.collectionClass)();
                          var selectedWorkflowModelArray =
                              [
                                draggedItemModelArray[0], // for Source
                                droppedItemModel          // for Target
                              ].map(
                                  function(itemModel, index, array) {
                                    return (
                                        (itemModel instanceof mwa.model.workflow.WorkflowModel) ?
                                            itemModel :
                                            that.options.viewModel
                                                .get('workflowDetailCollection')
                                                .get(itemModel.get('templateId')) ||
                                            // CAUTION:
                                            // It is necessary to consider about a case that
                                            // selected item is workflow diagram.
                                            new mwa.model.workflow.WorkflowModel(
                                                null, {validate: false}
                                            )
                                    );
                                  }
                              );

                          switch (true) {
                            case draggedItemCollection instanceof
                                mwa.model.activityProfile.ActivityProfileCollection:
                              draggedItemCollection.add(
                                  draggedItemModelArray.map(
                                      function(draggedItemModel, index, array) {
                                        var sourceInputArray =
                                            selectedWorkflowModelArray[0].get('inputs');
                                        var sourceParameters =
                                            draggedItemModel.get('parameter');

                                        // CAUTION: NVX-5266 (https://www.tool.sony.biz/jira/browse/NVX-5266)
                                        //          NVX-5662 (https://www.tool.sony.biz/jira/browse/NVX-5662)
                                        // It is necessary to update passed dragged item model
                                        // directly that is considered about drop action (move or
                                        // copy) instead of creating a new instance because the
                                        // existing item model should be updated to be able to use
                                        // it correctly especially after move operation.
                                        return draggedItemModel.set(
                                            mwa.proui._.extend(
                                                {},
                                                draggedItemModel.attributes,
                                                {
                                                  id:
                                                      (
                                                          dropAction ===
                                                          mwa.proui.treeView.DropAction.MOVE
                                                      ) ?
                                                          draggedItemModel.get('id') :
                                                          null,
                                                  groupId:
                                                      (
                                                          droppedItemModel instanceof
                                                          mwa.model.activityProfileGroup.ActivityProfileGroupModel
                                                      ) ?
                                                          droppedItemModel.get('id') :
                                                          null,
                                                  templateId:
                                                      selectedWorkflowModelArray[1].get('id'),
                                                  name:
                                                      // CAUTION: NVX-5662 (https://www.tool.sony.biz/jira/browse/NVX-5662)
                                                      // It is important to rename in copy case in
                                                      // consideration of application specification
                                                      // (e.g. NVX) regarding uniqueness for name
                                                      // attribute.
                                                      (
                                                          dropAction ===
                                                          mwa.proui.treeView.DropAction.COPY
                                                      ) ?
                                                          draggedItemModel.get('name') + ' ' +
                                                          '(' +
                                                          mwa.proui.util.Date.formatDate(new Date()) +
                                                          ')' :
                                                          draggedItemModel.get('name'),
                                                  parameter:
                                                      (
                                                          selectedWorkflowModelArray[1].get('inputs') ||
                                                          []
                                                      ).reduce(
                                                          // INFORMATION:
                                                          // The first argument is object type, not
                                                          // array. But we use plural name based on
                                                          // our JavaScript coding rule.
                                                          function(memo, inputs, index, array) {
                                                            var sourceInputs =
                                                                mwa.proui._.findWhere(
                                                                    sourceInputArray,
                                                                    {key: inputs.key}
                                                                ) ||
                                                                {};

                                                            memo[inputs.key] =
                                                                (
                                                                    sourceInputs.typeName ===
                                                                    inputs.typeName
                                                                ) ?
                                                                    sourceParameters[inputs.key] :
                                                                    null;

                                                            return memo;
                                                          },
                                                          {}
                                                      )
                                                }
                                            )
                                        );
                                      }
                                  )
                              );
                              break;

                            default:
                              break;
                          }

                          var error = function(collection, response, options) {
                            if (draggedItemCollection instanceof
                                mwa.model.activityProfileGroup.ActivityProfileGroupCollection ||
                                draggedItemCollection instanceof
                                mwa.model.activityProfile.ActivityProfileCollection) {
                              mwa.proui._.invoke(selectedWorkflowModelArray, 'fetch');
                            }
                          };

                          switch (dropAction) {
                            case mwa.proui.treeView.DropAction.MOVE:
                            case mwa.proui.treeView.DropAction.COPY:
                              that.renderItemMoveCopyDialog(
                                  draggedItemCollection,
                                  {
                                    success: function(collection, response, options) {
                                      // CAUTION: NVX-5662 (https://www.tool.sony.biz/jira/browse/NVX-5662)
                                      // It is necessary to consider about multiple errors case.
                                      if (response.errors == null || response.errors.length === 0) {
                                        // Do Nothing
                                      } else {
                                        error(collection, response, options);
                                      }
                                    },
                                    error: error
                                  }
                              );
                              break;

                            case mwa.proui.treeView.DropAction.LINK:
                              // Do Nothing
                              break;

                            default:
                              break;
                          }
                        }
                      },
                      attributes
                  )
              );
            },
            /**
             * The function to create workflow canvas tab content model based on specified workflow
             * diagram.
             * @protected
             *
             * @param {!mwa.model.workflowDiagram.WorkflowDiagramModel} workflowDiagramModel
             * The workflow diagram model to be displayed as part of workflow canvas tabs.
             *
             * @returns {proui.tab.TabContentModel}
             * The created tab content model instance.
             */
            createWorkflowCanvasTabContentModel: function(workflowDiagramModel) {
              var isWorkflowDiagramDetailModel =
                  workflowDiagramModel instanceof
                  mwa.model.workflowDiagramDetail.WorkflowDiagramDetailModel;
              var isPredefinedWorkflowDetailModel =
                  workflowDiagramModel instanceof
                  mwa.model.workflowDetail.WorkflowDetailModel &&
                  workflowDiagramModel.get('predefineFlag');

              return new mwa.proui.tab.TabContentModel({
                // CAUTION:
                // It is important to set "id" to close the canvas tab when corresponding workflow
                // diagram is destroyed. This setting is also working for not to display the same
                // workflow diagram in different canvas tabs but this point is also achieved by
                // setting the workflow diagram model to selectedWorkflowDiagramCollection because
                // add event isn't triggered for the same model.
                id: workflowDiagramModel.get('id'),
                title: this.createWorkflowCanvasTabTitle(workflowDiagramModel),
                icon: (isWorkflowDiagramDetailModel) ?
                    mwa.EXTERNAL_IMAGE_BASE_PATH + 'proui.util.workflow.svg' :
                    mwa.EXTERNAL_IMAGE_BASE_PATH + 'proui.util.workflow-playable.svg',
                viewClass: mwa.view.workflowCanvas.WorkflowCanvasView,
                viewOptions: {
                  viewModel: new mwa.view.workflowCanvas.WorkflowCanvasViewModel({
                    hasGrid: true,
                    hasInformationButton: !isPredefinedWorkflowDetailModel,
                    hasWorkflowMetadataEditor: true,
                    isReadOnly: !isWorkflowDiagramDetailModel,
                    canSelect: !isPredefinedWorkflowDetailModel,
                    canDrag: isWorkflowDiagramDetailModel,
                    canDrop: isWorkflowDiagramDetailModel,
                    canDelete: isWorkflowDiagramDetailModel,
                    canConnect: isWorkflowDiagramDetailModel,
                    enlargementFactor: 100,
                    // CAUTION:
                    // It is important to set the same instance to all workflow canvas view models
                    // in order to use the same activity parameter types between each workflow
                    // parameter editor in workflow canvas.
                    activityParameterTypeCollection: this.models.activityParameterTypeCollection,
                    workflowDiagramModel: workflowDiagramModel,
                    workflowInstanceModel: null
                  })
                }
              });
            },
            /**
             * The function to add workflow canvas tab content based on specified workflow diagram.
             * @protected
             *
             * @param {!mwa.model.workflowDiagram.WorkflowDiagramModel} workflowDiagramModel
             * The workflow diagram model to add workflow canvas tab content.
             */
            addWorkflowCanvasTabContent: function(workflowDiagramModel) {
              if (this.views != null && this.views.workflowCanvasTabView != null) {
                // CAUTION:
                // The workflow canvas tab must be displayed before rendering the content (workflow
                // canvas) in order to calculate the position of resize handle for workflow
                // information pane correctly.
                this.views.workflowCanvasTabView.$el.show();

                this.views.workflowCanvasTabView.options.viewModel
                    .get('contentList')
                    .add(this.createWorkflowCanvasTabContentModel(workflowDiagramModel));
              }
            },
            /**
             * The function to update workflow canvas tab title based on specified workflow diagram.
             * @protected
             *
             * @param {!mwa.model.workflowDiagram.WorkflowDiagramModel} workflowDiagramModel
             * The workflow diagram model to update workflow canvas tab title.
             */
            updateWorkflowCanvasTabTitle: function(workflowDiagramModel) {
              if (this.views != null && this.views.workflowCanvasTabView != null) {
                // CAUTION:
                // It is necessary to check whether there is the tab even if tab must be opened for
                // each selected workflow diagram because there is a case that this processing is
                // executed when the selected workflow diagram is deleted. In this case, there is no
                // opened tab.
                var canvasTabContentModel =
                    this.views.workflowCanvasTabView.options.viewModel
                        .get('contentList')
                        .findWhere({id: workflowDiagramModel.get('id')});
                if (canvasTabContentModel instanceof mwa.proui.tab.TabContentModel) {
                  canvasTabContentModel.set(
                      'title', this.createWorkflowCanvasTabTitle(workflowDiagramModel)
                  );
                }
              }
            },
            /**
             * The function to update workflow diagram add command button based on the selected
             * workflow navigation tab.
             * @protected
             *
             * @param {!mwa.model.workflowDiagram.WorkflowDiagramModel} workflowDiagramModel
             * The workflow diagram model to be updated workflow canvas tab title.
             */
            updateWorkflowDiagramAddCommandButton: function() {
              if (this.views != null &&
                  this.views.workflowDiagramAddCommandButtonView != null &&
                  this.views.workflowNavigationTabView != null &&
                  this.views.workflowDiagramNavigationTreeViewView != null) {
                this.views.workflowDiagramAddCommandButtonView.$el.toggle(
                    this.views.workflowNavigationTabView.options.viewModel
                        .get('contentList')
                        .at(
                            this.views.workflowNavigationTabView.options.viewModel
                                .get('selectedIndex')
                        ).get('viewOptions').viewModel ===
                    this.views.workflowDiagramNavigationTreeViewView.options.viewModel
                );
              }
            },
            /**
             * The event handler for changing selectedWorkflowDiagramCollection attribute of view
             * model and changing, adding, removing, updating, resetting, destroying, requesting,
             * syncing and failed models in selectedWorkflowDiagramCollection. This function updates
             * canvas tab according to the value.
             * @protected
             */
            applySelectedWorkflowDiagramCollection: function() {
              // arguments:
              // WorkflowEditorViewModel
              //  - "change:selectedWorkflowDiagramCollection" (model, value, options)
              // selectedWorkflowDiagramCollection
              //  - "change"                                   (model, options)
              //  - "change:[attribute]"                       (model, value, options)
              //  - "add", "remove", "destroy"                 (model, collection, options)
              //  - "update", "reset"                          (collection, options)
              //  - "request"                                  (model, xhr, options)
              //  - "request"                                  (collection, xhr, options)
              //  - "sync", "error"                            (model, response, options)
              //  - "sync", "error"                            (collection, response, options)
              var that = this;
              var args = {};

              var selectedWorkflowDiagramCollection =
                  that.options.viewModel.get('selectedWorkflowDiagramCollection');

              if (2 <= arguments.length && arguments.length <= 3) {
                if (arguments[0] === that.options.viewModel) {
                  /**
                   * @type {object}
                   * @description The arguments of change event.
                   * @property {mwa.view.workflowEditor.WorkflowEditorViewModel} model
                   * The mwa workflow editor view model.
                   * @property {mwa.model.workflowDiagram.WorkflowDiagramCollection} value
                   * The new changed value.
                   * @property {object} options
                   * The options of change event.
                   */
                  args = {
                    model: arguments[0],
                    value: arguments[1],
                    options: arguments[2]
                  };

                  // Change Case

                  // If you need any processing, add here.
                } else if (arguments[0] instanceof mwa.model.workflowDiagram.WorkflowDiagramModel) {
                  if (arguments[1] !== selectedWorkflowDiagramCollection) {
                    if (arguments.length === 2) { // TODO: To detect change:[attribute] event.
                      /**
                       * @type {object}
                       * @description The arguments of change event.
                       * @property {mwa.model.workflowDiagram.WorkflowDiagramModel} model
                       * The changed model.
                       * @property {*} value
                       * The new changed value.
                       * @property {object} options
                       * The options of change event.
                       */
                      args = {
                        model: arguments[0],
                        value: (arguments.length === 2) ? null : arguments[1],
                        options: (arguments.length === 2) ? arguments[1] : arguments[2]
                      };

                      // Change Case

                      if (that.views != null) {
                        if (that.views.workflowCanvasTabView != null) {
                          that.updateWorkflowCanvasTabTitle(args.model);
                        }

                        if (that.views.workflowDiagramNavigationTreeViewView != null) {
                          if (args.model ===
                              that.views.workflowDiagramNavigationTreeViewView.options.viewModel.get('lastSelectedItemModel')) {
                            updateWorkflowNavigationTreeViewContextMenu(args.model);
                          }
                        }

                        if (that.views.workflowNavigationTreeViewView != null) {
                          if (args.model ===
                              that.views.workflowNavigationTreeViewView.options.viewModel.get('lastSelectedItemModel')) {
                            updateWorkflowNavigationTreeViewContextMenu(args.model);
                          }
                        }
                      }
                    } else {
                      if (arguments[2].xhr != null && arguments[2].xhr === arguments[1]) {
                        /**
                         * @type {object}
                         * @description The arguments of request event.
                         * @property {mwa.model.workflowDiagram.WorkflowDiagramModel} model
                         * The model that was requested.
                         * @property {object} xhr
                         * The XMLHttpRequest object.
                         * @property {object} options
                         * The options of request event.
                         */
                        args = {
                          model: arguments[0],
                          xhr: arguments[1],
                          options: arguments[2]
                        };

                        // Request Case

                        if (that.views != null) {
                          if (that.views.workflowCanvasTabView != null) {
                            that.views.workflowCanvasTabView.renderLoading();
                          }
                        }
                      } else {
                        /**
                         * @type {object}
                         * @description The arguments of sync or error event.
                         * @property {mwa.model.workflowDiagram.WorkflowDiagramModel} model
                         * The model that was synced or failed.
                         * @property {object} response
                         * The response of request.
                         * @property {object} options
                         * The options of sync or error event.
                         */
                        args = {
                          model: arguments[0],
                          response: arguments[1],
                          options: arguments[2]
                        };

                        if (mwa.proui.util.Http.isRequestSuccess(args.response)) {
                          // Sync Case

                          if (that.views != null) {
                            if (that.views.workflowCanvasTabView != null) {
                              that.updateWorkflowCanvasTabTitle(args.model);
                            }
                          }
                        } else {
                          // Error Case

                          // If you need any processing, add here.
                        }

                        that.views.workflowCanvasTabView.removeLoading();
                      }
                    }
                  } else {
                    /**
                     * @type {object}
                     * @description The arguments of add, remove or destroy event.
                     * @property {mwa.model.workflowDiagram.WorkflowDiagramModel} model
                     * The added, removed or destroyed model.
                     * @property {mwa.model.workflowDiagram.WorkflowDiagramCollection} collection
                     * The collection that was added removed or destroyed the model.
                     * @property {object} options
                     * The options of add, remove or destroy event.
                     */
                    args = {
                      model: arguments[0],
                      collection: arguments[1],
                      options: arguments[2]
                    };

                    // INFORMATION:
                    // If args.model doesn't exist in args.collection, this case is that remove
                    // event occurred and args.options.index is required in this case.
                    var index = args.collection.indexOf(args.model);
                    if (index !== -1) {
                      // Add Case

                      if (that.views != null) {
                        if (that.views.workflowCanvasTabView != null) {
                          // CAUTION:
                          // It is important to consider about that workflow model is extended from
                          // workflow diagram model.
                          if (args.model instanceof mwa.model.workflow.WorkflowModel) {
                            // CAUTION: NVX-6935 (https://www.tool.sony.biz/jira/browse/NVX-6935)
                            // It is important not to call fetch function in workflow model case in
                            // order not to expand workflow navigation tree view item automatically
                            // by sync event handler. And there is no way to do silent fetch in
                            // Backbone.js specification.
                            //  - Backbone.Model silent “fetch”
                            //    (https://stackoverflow.com/questions/23582820/backbone-model-silent-fetch)
                            that.addWorkflowCanvasTabContent(args.model);
                          } else {
                            args.model.fetch({
                              success: function(model, response, options) {
                                that.addWorkflowCanvasTabContent(args.model);
                              },
                              error: function(model, response, options) {
                                // CAUTION:
                                // It isn't necessary to consider about updating "selectedIndex"
                                // attribute of workflow canvas tab view model in this case because
                                // it hasn't been updated in the former processing.

                                // CAUTION:
                                // It is important to remove the selected workflow diagram model at
                                // here instead of in error event handler in consideration of a case
                                // that the error may be going to happen in rename case.
                                selectedWorkflowDiagramCollection.remove(args.model);

                                // CAUTION:
                                // It is necessary to remove loading manually even if the same code
                                // is implemented in error event handler because error event won't
                                // be detected on selectedWorkflowDiagramCollection attribute of
                                // view model by removing the selected workflow diagram model from
                                // it at the above code.
                                that.views.workflowCanvasTabView.removeLoading();
                              }
                            });
                          }
                        }
                      }
                    } else {
                      if (args.options.index != null) {
                        // Remove Case

                        // If you need any processing, add here.
                      } else {
                        // Destroy Case

                        // If you need any processing, add here.
                      }

                      if (that.views != null) {
                        if (that.views.workflowCanvasTabView != null) {
                          that.views.workflowCanvasTabView.options.viewModel
                              .get('contentList')
                              .remove(args.model.get('id'));
                        }
                      }
                    }
                  }
                } else if (arguments[0] === selectedWorkflowDiagramCollection) {
                  if (arguments.length === 2) {
                    /**
                     * @type {object}
                     * @description The arguments of update or reset event.
                     * @property {mwa.model.workflowDiagram.WorkflowDiagramCollection} collection
                     * The collection that was updated or reset.
                     * @property {object} options
                     * The options of update or reset event.
                     */
                    args = {
                      collection: arguments[0],
                      options: arguments[1]
                    };

                    if (args.options == null || args.options.previousModels == null) {
                      // Update Case

                      if (that.views != null) {
                        if (that.views.workflowCanvasTabView != null) {
                          that.views.workflowCanvasTabView.$el.toggle(
                              !selectedWorkflowDiagramCollection.isEmpty()
                          );
                        }
                      }
                    } else {
                      // Reset Case

                      // If you need any processing, add here.
                    }
                  } else {
                    if (arguments[2].xhr != null && arguments[2].xhr === arguments[1]) {
                      /**
                       * @type {object}
                       * @description The arguments of request event.
                       * @property {mwa.model.workflowDiagram.WorkflowDiagramCollection} collection
                       * The collection that was requested.
                       * @property {object} xhr
                       * The XMLHttpRequest object.
                       * @property {object} options
                       * The options of request event.
                       */
                      args = {
                        collection: arguments[0],
                        xhr: arguments[1],
                        options: arguments[2]
                      };

                      // Request Case

                      // If you need any processing, add here.
                    } else {
                      /**
                       * @type {object}
                       * @description The arguments of sync or error event.
                       * @property {mwa.model.workflowDiagram.WorkflowDiagramCollection} collection
                       * The collection that was synced or failed.
                       * @property {object} response
                       * The response of request.
                       * @property {object} options
                       * The options of sync or error event.
                       */
                      args = {
                        collection: arguments[0],
                        response: arguments[1],
                        options: arguments[2]
                      };

                      if (mwa.proui.util.Http.isRequestSuccess(args.response)) {
                        // Sync Case

                        // If you need any processing, add here.
                      } else {
                        // Error Case

                        // If you need any processing, add here.
                      }
                    }
                  }
                }
              }
            }
          }
      );
    })();

    return global;
  })();

  return mwa.view.workflowEditor;
}));
