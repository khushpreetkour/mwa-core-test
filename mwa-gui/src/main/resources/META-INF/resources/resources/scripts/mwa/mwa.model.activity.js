/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {},
      (mwa.model == null || mwa.model.activityParameter == null) ? './mwa.model.activity-parameter' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {},
        (mwa.model == null || mwa.model.activityParameter == null) ? require('./mwa.model.activity-parameter') : {}
    );
  } else {
    root.mwa.model.activity = factory(
        root.mwa,
        root.mwa.model,
        root.mwa.model.activityParameter
    );
  }
}(this, function(base, subBase, activityParameter) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }
  // To initialize the mwa.model.activityParameter namespace.
  if (mwa.model.activityParameter == null) {
    mwa.model.activityParameter = activityParameter;
  }

  /**
   * The mwa.model.activity namespace.
   * @namespace
   */
  mwa.model.activity = (function() {
    'use strict';

    /**
     * Defines mwa.model.activity alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa activity model.
     * @constructor
     * @inner
     * @alias mwa.model.activity.ActivityModel
     * @memberof mwa.model.activity
     * @extends {mwa.Model}
     */
    global.ActivityModel = (function() {
      var activityParameterModel =
          new mwa.model.activityParameter.ActivityParameterModel(null, {validate: false});

      var validateActivityParameter = function(attributes) {
        var msg = null;

        var validationError =
            activityParameterModel.set(
                activityParameterModel.parse(
                    // CAUTION:
                    // It is necessary to clone the instance not to change the original values by
                    // parse.
                    mwa.proui.util.Object.extend(true, {}, attributes),
                    null
                )
            ).validate();
        if (typeof validationError === 'string') {
          msg = validationError;
        } else if (typeof validationError === 'object') {
          for (var propertyName in validationError) {
            if (validationError.hasOwnProperty(propertyName)) {
              msg = validationError[propertyName];
              break;
            }
          }
        }

        return msg;
      };

      return mwa.Model.extend(
          /**
           * @lends mwa.model.activity.ActivityModel
           */
          {
            alias: 'mwa.model.activity.ActivityModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activities' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    name: null,
                    alias: null,
                    description: null,
                    type: null,
                    version: null,
                    icon: null,
                    inputs: null,
                    outputs: null,
                    tags: null,
                    deleteFlag: null,
                    syncFlag: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  alias: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  description: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  type: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = 'The "' + attr + '" must be mwa.enumeration.ActivityType.';
                        for (var propertyName in mwa.enumeration.ActivityType) {
                          if (mwa.enumeration.ActivityType.hasOwnProperty(propertyName)) {
                            // INFORMATION:
                            // Don't check whether the instance is matching with enumeration object
                            // because it is difficult for development phase to fit it.
                            if (mwa.proui._.isEqual(value, mwa.enumeration.ActivityType[propertyName])) {
                              msg = null;
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  version: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  icon: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  inputs: [
                    {
                      required: false // TODO: To change to "required: true" after BL implementation.
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        } else {
                          for (var i = 0; i < value.length; i++) {
                            msg = validateActivityParameter(value[i]);
                            if (typeof msg !== 'undefined' && msg != null) {
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  outputs: [
                    {
                      required: false // TODO: To change to "required: true" after BL implementation.
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        } else {
                          for (var i = 0; i < value.length; i++) {
                            msg = validateActivityParameter(value[i]);
                            if (typeof msg !== 'undefined' && msg != null) {
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  tags: [
                    {
                      required: false // TODO: To change to "required: true" after BL implementation.
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ],
                  deleteFlag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  syncFlag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.Model.prototype.parse.apply(this, [response, options]);

              // TODO: To remove this after completing BL implementation.
              if (response.alias == null) {
                response.alias = response.name;
              }

              if (response.type != null) {
                // INFORMATION:
                // It is important to check whether the value should be converted because there is
                // a case that the value has been already converted.
                if (typeof response.type === 'string') {
                  response.type = mwa.enumeration.ActivityType[response.type];
                }
              }

              return response;
            },
            /**
             * @see {@link mwa.Model.toJSON}
             * @public
             * @override
             */
            toJSON: function(options) {
              var json = mwa.Model.prototype.toJSON.apply(this, [options]);
              // CAUTION:
              // It is important to use deep copied object in consideration of a case that a part of
              // object properties will be modified through the following processing.
              json = mwa.proui.util.Object.extend(true, {}, json);

              for (var propertyName in mwa.enumeration.ActivityType) {
                if (mwa.enumeration.ActivityType.hasOwnProperty(propertyName)) {
                  if (mwa.proui._.isEqual(json.type, mwa.enumeration.ActivityType[propertyName])) {
                    json.type = mwa.enumeration.ActivityType[propertyName].name;
                    break;
                  }
                }
              }

              return json;
            },
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * @see {@link mwa.Model.fetch}
             * @public
             * @override
             */
            fetch: function(options) {
              var message = '';

              options = mwa.proui._.extend({}, options, {url: this.urlRoot()});

              if (typeof this.get('id') === 'string') {
                options.url += '/' + this.get('id');
              } else if (typeof this.get('name') === 'string' &&
                  typeof this.get('version') === 'string') {
                options.url += '/' + this.get('name') + '/' + this.get('version') + '/';
              } else {
                message = this.alias + '.fetch : ' +
                    'The "name" and "version" is required to fetch the details.';

                mwa.Logger.error(message);

                throw Error(message);
              }

              return mwa.Model.prototype.fetch.apply(this, [options]);
            },
            /**
             * @see {@link mwa.Model.createEditingMetadataFieldCollection}
             * @public
             * @override
             */
            createEditingMetadataFieldCollection: function() {
              var modelEditingMetadataFieldCollection =
                  mwa.Model.prototype.createEditingMetadataFieldCollection.apply(this, []);
              var length = modelEditingMetadataFieldCollection.length;

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                  modelEditingMetadataFieldCollection.models.concat([
                    {
                      // prouiExt.metadataEditor.MetadataFieldModel
                      key: 'alias',
                      index: length++,
                      title: mwa.enumeration.Message['SENTENCE_NAME'],
                      type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                      valueCollection: null,
                      referenceValueCollection: null,
                      metadataFieldValidatorCollection:
                          new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                      isArray: false,
                      isHidden: false,
                      isRequired: true,
                      isReadOnly: false,
                      // prouiExt.metadataEditorListView.MetadataFieldModel
                      value: null,
                      canShift: true,
                      canSort: true,
                      canFilter: false,
                      canResize: true
                    }
                  ])
              );
            },
            /**
             * The function to create parameter metadata field collection.
             * @public
             *
             * @return {prouiExt.metadataEditorListView.MetadataFieldCollection}
             * The created metadata field collection instance.
             */
            createParameterMetadataFieldCollection: function() {
              var that = this;

              var activityParameterSettingItemCollections =
                  mwa.proui._.mapObject(
                      mwa.enumeration.ActivityParameterSettingType,
                      function(value, key) {
                        return value.createItemCollection(that);
                      }
                  );
              for (var propertyName in activityParameterSettingItemCollections) {
                if (activityParameterSettingItemCollections.hasOwnProperty(propertyName) &&
                    activityParameterSettingItemCollections[propertyName] instanceof
                    mwa.proui.Backbone.Collection) {
                  activityParameterSettingItemCollections[propertyName].fetch({async: false});
                }
              }

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection(
                  (that.get('inputs') || []).filter(
                      // INFORMATION:
                      // The first argument is object type, not array. But we use plural name based
                      // on our JavaScript coding rule.
                      function(inputs, index, array) {
                        var parentKey = inputs.parentKey;
                        return (
                            typeof parentKey === 'undefined' ||
                            parentKey == null ||
                            parentKey === ''
                        );
                      }
                  ).map(
                      // INFORMATION:
                      // The first argument is object type, not array. But we use plural name based
                      // on our JavaScript coding rule.
                      function(inputs, index, array) {
                        return that.createParameterMetadataFieldModel(
                            inputs, index, activityParameterSettingItemCollections
                        );
                      }
                  )
              );
            },
            /**
             * The function to create parameter metadata field model based on specified activity
             * parameter input.
             * @public
             *
             * @param {!object} inputs
             * The activity parameter input object to be created metadata field model.
             * @param {!number} index
             * The number to specify the index of specified activity parameter input object.
             * @param {!object} [activityParameterSettingItemCollections]
             * The object including various activity parameter setting item collections to create
             * metadata field model.
             *
             * @returns {prouiExt.metadataEditorListView.MetadataFieldModel}
             * The created metadata field model.
             */
            createParameterMetadataFieldModel: function(inputs, index, activityParameterSettingItemCollections) {
              var that = this;

              var inputActivityParameterModel =
                  new mwa.model.activityParameter.ActivityParameterModel(
                      inputs,
                      {parse: true}
                  );
              var key = inputActivityParameterModel.get('key');
              var type = inputActivityParameterModel.get('type');
              var title = inputActivityParameterModel.get('title') || key;

              return new mwa.prouiExt.metadataEditorListView.MetadataFieldModel({
                // prouiExt.metadataEditor.MetadataFieldModel
                key: 'parameter' + '.' + key,
                index: index,
                title: title,
                type: type.metadataFieldType,
                valueCollection:
                    (
                        mwa.enumeration.ActivityParameterSettingType[type.name] ||
                        mwa.enumeration.ActivityParameterSettingType.NONE
                    ).createValueCollection(
                        (type === mwa.enumeration.ActivityParameterType.SETTING) ?
                            new mwa.model.activityParameterTypeValue.ActivityParameterTypeValueCollection(
                                (
                                    activityParameterSettingItemCollections[type.name].findWhere({
                                      name: inputActivityParameterModel.get('typeName')
                                    }) ||
                                    new mwa.model.activityParameterType.ActivityParameterTypeModel(
                                        null,
                                        {validate: false}
                                    )
                                ).get('values')
                            ) :
                            activityParameterSettingItemCollections[type.name],
                        that,
                        inputActivityParameterModel
                    ),
                referenceValueCollection: null,
                metadataFieldValidatorCollection: type.metadataFieldValidatorCollection,
                isArray: inputActivityParameterModel.get('listFlag'),
                isHidden: !inputActivityParameterModel.get('visibleFlag'),
                isRequired: inputActivityParameterModel.get('required'),
                isReadOnly: false,
                metadataFields:
                    (that.get('inputs') || []).filter(
                        // INFORMATION:
                        // The first argument is object type, not array. But we use plural name
                        // based on our JavaScript coding rule.
                        function(inputs, index, array) {
                          return inputs.parentKey === key;
                        }
                    ).reduce(
                        // INFORMATION:
                        // The second argument is object type, not array. But we use plural name
                        // based on our JavaScript coding rule.
                        function(memo, inputs, index, array) {
                          var parentValue = JSON.stringify(inputs.parentValue);

                          if (memo[parentValue] == null) {
                            memo[parentValue] =
                                new mwa.prouiExt.metadataEditorListView.MetadataFieldCollection();
                          }

                          memo[parentValue].add(
                              that.createParameterMetadataFieldModel(
                                  inputs, index, activityParameterSettingItemCollections
                              )
                          );

                          return memo;
                        },
                        {}
                    ),
                // prouiExt.metadataEditorListView.MetadataFieldModel
                value: null,
                canShift: true,
                canSort: false,
                canFilter: false,
                canResize: true
              });
            },
            /**
             * The function to create an instance for starting actual processing.
             * @public
             *
             * @param {mwa.model.keyValue.KeyValueCollection} [parameters]
             * The input key value parameters.
             * @param {object} [options]
             * The options of the Ajax request.
             *
             * @returns {object}
             * The deferred object of the request.
             */
            createInstance: function(parameters, options) {
              var message = '';

              if (typeof parameters !== 'undefined' && parameters != null) {
                if (!(parameters instanceof mwa.model.keyValue.KeyValueCollection)) {
                  message = this.alias + '.createInstance : ' +
                      'The "parameters" must be an instance of mwa.model.keyValue.KeyValueCollection.';

                  mwa.Logger.error(message);

                  throw Error(message);
                }
              }

              var options = mwa.proui._.extend(
                  {},
                  options,
                  {
                    type: 'POST',
                    url: this.urlRoot(),
                    data: JSON.stringify(parameters),
                    dataType: 'json',
                    contentType: 'application/json;charset=utf-8'
                  }
              );
              this.wrapCallback(options);

              if (this.id != null) {
                options.url += this.id + '/';
              } else if (this.get('name') != null && this.get('version') != null) {
                options.url += this.get('name') + '/' + this.get('version') + '/';
              } else {
                message = this.alias + '.createInstance : ' +
                    'The "id" otherwise "name" and "version" is required to create an instance.';

                mwa.Logger.error(message);

                throw Error(message);
              }

              var xhr = mwa.proui.Backbone.$.ajax(options);
              this.trigger('request', this, xhr, options);

              return xhr;
            },
            /**
             * The function to get parameter.
             *
             * @returns {!object}
             * The parameter.
             */
            getParameter: function() {
              return (this.get('inputs') || []).reduce(
                  // INFORMATION:
                  // The first argument is object type, not array. But we use plural name based on
                  // our JavaScript coding rule.
                  function(memo, inputs, index, array) {
                    memo[inputs.key] = null;
                    return memo;
                  },
                  {}
              );
            }
          }
      );
    })();

    /**
     * The mwa activity collection.
     * @constructor
     * @inner
     * @alias mwa.model.activity.ActivityCollection
     * @memberof mwa.model.activity
     * @extends {mwa.Collection}
     */
    global.ActivityCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activity.ActivityCollection
           */
          {
            url: global.ActivityModel.prototype.urlRoot,
            model: global.ActivityModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.activity;
}));
