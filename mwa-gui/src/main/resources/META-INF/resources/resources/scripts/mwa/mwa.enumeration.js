/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      'properties'
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        require('properties')
    );
  } else {
    root.mwa.enumeration = factory(
        root.mwa,
        root.$.i18n
    );
  }
}(this, function(base, properties) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }

  /**
   * The mwa.enumeration namespace.
   * @namespace
   */
  mwa.enumeration = (function() {
    'use strict';

    /**
     * Defines mwa.enumeration alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    if (properties.properties == null) {
      var fs = require('fs');
      var languageCode = mwa.proui.util.Platform.normalizeLanguageCode() || 'en';
      var path = mwa.INTERNAL_LOCALE_BASE_PATH + 'messages_' + languageCode + '.properties';

      // INFORMATION:
      // This codes mean to use message resource file without country code if the specified file
      // doesn't exist. In other words, message resource file that doesn't have country code will be
      // used as alternative resource.
      try {
        fs.readFileSync(path);
      } catch (error) {
        if (error.code === 'ENOENT') {
          path = path.replace(languageCode, languageCode.substring(0, 2));
        }
      }

      /**
       * The enumeration for message.
       * @readonly
       * @enum {object}
       * @alias mwa.enumeration.Message
       * @memberof mwa.enumeration
       */
      global.Message = properties.parse(
          fs.readFileSync(
              path,
              {
                encoding: 'utf8'
              },
              function(error, data) {
                if (error != null) {
                  var message = 'mwa.enumeration.Message.parse : ' + error;

                  mwa.Logger.error(message);

                  throw Error(message);
                }
              }
          ),
          {
            path: false,
//            comments: ['#', '!'],
//            separators: ['=', ':'],
//            strict: true,
//            sections: true,
            namespaces: false,
//            variables: true,
//            vars: true,
            include: false,
            reviver: function(key, value, section) {
              return value.replace(/''/g, '\'');
            }
          }
      );
    } else {
      properties.properties({
        namespace: 'mwa',
        name: 'messages',
        path: mwa.INTERNAL_LOCALE_BASE_PATH,
        // INFORMATION:
        // If "language" isn't specified, language reported by the browser will be used instead.
        // - jQuery.i18n.properties (https://github.com/jquery-i18n-properties/jquery-i18n-properties)
        async: false,
        mode: 'map',
        cache: false,
        encoding: 'UTF-8',
        callback: function() {
          var messages = properties.map['mwa'];
          for (var propertyName in messages) {
            if (messages.hasOwnProperty(propertyName)) {
              messages[propertyName] = messages[propertyName].replace(/''/g, '\'');
            }
          }
          global.Message = messages;
        }
      });
    }

    /**
     * The enumeration for icon.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.Icon
     * @memberof mwa.enumeration
     */
    global.Icon = {
      DIRECTORY: {
        name: 'DIRECTORY',
        dataUri: 'data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2224px%22%20height%3D%2224px%22%20viewBox%3D%220%200%2024%2024%22%20enable-background%3D%22new%200%200%2024%2024%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22icon_75_%22%3E%20%3Cg%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M20.5%2C7h-10L9.7%2C5.4C9.6%2C5.2%2C9.3%2C5%2C9%2C5H4.5C4.2%2C5%2C3.9%2C5.2%2C3.8%2C5.5L3.2%2C7.1C3.1%2C7.3%2C3%2C7.7%2C3%2C8v10.5%20C3%2C18.8%2C3.2%2C19%2C3.5%2C19h17c0.3%2C0%2C0.5-0.2%2C0.5-0.5v-11C21%2C7.2%2C20.8%2C7%2C20.5%2C7z%22%2F%3E%20%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E'
      },
      FILE: {
        name: 'FILE',
        dataUri: 'data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2224px%22%20height%3D%2224px%22%20viewBox%3D%220%200%2024%2024%22%20enable-background%3D%22new%200%200%2024%2024%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22icon_7_%22%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M13.8%2C2H5v20h14V7.6L13.8%2C2z%20M6.5%2C20.5v-17H13V8h4.5v12.5H6.5z%22%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E'
      },
      WORKFLOW_PROFILE: {
        name: 'WORKFLOW_PROFILE',
        dataUri: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0i44Os44Kk44Ok44O8XzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCINCgkgeT0iMHB4IiB3aWR0aD0iMjRweCIgaGVpZ2h0PSIyNHB4IiB2aWV3Qm94PSIwIDAgMjQgMjQiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDI0IDI0IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwb2x5Z29uIGZpbGw9IiNFMEUwRTAiIHBvaW50cz0iMTQsMjAgMjEuMiwxNyAxNCwxNCAJIi8+DQoJPHBhdGggZmlsbD0iI0UwRTBFMCIgZD0iTTE3LDEwYy0wLjIsMC0wLjMsMC0wLjUsMFY0SDE1VjJoLTV2Mkg4LjV2Ny41SDcuOUM3LjcsMTAuMSw2LjUsOSw1LDljLTEuNywwLTMsMS4zLTMsM3MxLjMsMywzLDMNCgkJYzEuNSwwLDIuNy0xLjEsMi45LTIuNWgxLjZWNUgxMHYyaDVWNWgwLjV2NS4yQzEyLjQsMTAuOSwxMCwxMy42LDEwLDE3YzAsMy45LDMuMSw3LDcsN2MzLjksMCw3LTMuMSw3LTdDMjQsMTMuMSwyMC45LDEwLDE3LDEweg0KCQkgTTE3LDIzYy0zLjMsMC02LTIuNy02LTZzMi43LTYsNi02czYsMi43LDYsNlMyMC4zLDIzLDE3LDIzeiIvPg0KPC9nPg0KPC9zdmc+DQo='
      },
      TASK_PROFILE: {
        name: 'TASK_PROFILE',
        // TODO: PROUI-120 (https://www.tool.sony.biz/jira/browse/PROUI-120)
        dataUri: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0i44Os44Kk44Ok44O8XzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCINCgkgeT0iMHB4IiB3aWR0aD0iMzZweCIgaGVpZ2h0PSIzNnB4IiB2aWV3Qm94PSIwIDAgMzYgMzYiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDM2IDM2IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnIGlkPSLjg6zjgqTjg6Tjg7xfMV8xXyI+DQoJPGc+DQoJCTxwYXRoIGZpbGw9IiNFMEUwRTAiIGQ9Ik0yOC40LDE3LjhjMC0wLjYtMC4xLTEuMi0wLjEtMS43bDMuNy0zLjNsLTIuNi00LjVsLTQuNywxLjVjLTAuOS0wLjctMS45LTEuNC0zLTEuOWwtMS00LjloLTUuMmwtMSw0LjkNCgkJCWMtMS4xLDAuNC0yLjEsMS4xLTMsMS44TDYuNiw4LjJMNCwxMi43TDcuNywxNmMtMC4xLDAuNi0wLjEsMS4yLTAuMSwxLjdjMCwwLjYsMC4xLDEuMiwwLjEsMS43TDQsMjIuOGwyLjYsNC41bDQuNy0xLjUNCgkJCWMwLjksMC43LDEuOSwxLjcsMywyLjFsMSw1LjFoNS4ybDEtNS4xYzEuMS0wLjQsMi4xLTEuMiwzLTEuOWw0LjcsMS41bDIuNi00LjVsLTMuNy0zLjRDMjguMywxOSwyOC40LDE4LjQsMjguNCwxNy44eiBNMTgsMjMNCgkJCWMtMi45LDAtNS4yLTIuMy01LjItNS4yczIuMy01LjIsNS4yLTUuMnM1LjIsMi4zLDUuMiw1LjJTMjAuOCwyMywxOCwyM3oiLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg=='
      }
    };

    /**
     * The enumeration for cron day of week.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.CronDayOfWeek
     * @memberof mwa.enumeration
     */
    global.CronDayOfWeek = {
      // CAUTION: NVX-7666 (https://www.tool.sony.biz/jira/browse/NVX-7666)
      // The "property" field is required to display these definitions as check box in metadata
      // editor.
      '0': {
        name: '0',
        property: 0,
        label: global.Message['SENTENCE_SUNDAY'],
        shortLabel: global.Message['SENTENCE_SUNDAY_ABBREVIATION'],
        index: 0,
        specialCharacter: 'SUN'
      },
      '1': {
        name: '1',
        property: 1,
        label: global.Message['SENTENCE_MONDAY'],
        shortLabel: global.Message['SENTENCE_MONDAY_ABBREVIATION'],
        index: 1,
        specialCharacter: 'MON'
      },
      '2': {
        name: '2',
        property: 2,
        label: global.Message['SENTENCE_TUESDAY'],
        shortLabel: global.Message['SENTENCE_TUESDAY_ABBREVIATION'],
        index: 2,
        specialCharacter: 'TUE'
      },
      '3': {
        name: '3',
        property: 3,
        label: global.Message['SENTENCE_WEDNESDAY'],
        shortLabel: global.Message['SENTENCE_WEDNESDAY_ABBREVIATION'],
        index: 3,
        specialCharacter: 'WED'
      },
      '4': {
        name: '4',
        property: 4,
        label: global.Message['SENTENCE_THURSDAY'],
        shortLabel: global.Message['SENTENCE_THURSDAY_ABBREVIATION'],
        index: 4,
        specialCharacter: 'THU'
      },
      '5': {
        name: '5',
        property: 5,
        label: global.Message['SENTENCE_FRIDAY'],
        shortLabel: global.Message['SENTENCE_FRIDAY_ABBREVIATION'],
        index: 5,
        specialCharacter: 'FRI'
      },
      '6': {
        name: '6',
        property: 6,
        label: global.Message['SENTENCE_SATURDAY'],
        shortLabel: global.Message['SENTENCE_SATURDAY_ABBREVIATION'],
        index: 6,
        specialCharacter: 'SAT'
      }
    };
    global.CronDayOfWeek['7'] =
        mwa.proui._.extend({}, global.CronDayOfWeek['0'], {name: '7', index: 7});

    /**
     * The enumeration for Quartz day of week.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.QuartzDayOfWeek
     * @memberof mwa.enumeration
     */
    // INFORMATION:
    // The QuartzDayOfWeek is almost same as the CronDayOfWeek except for that it has +1 value of
    // name and property.
    global.QuartzDayOfWeek =
        mwa.proui._.pairs(global.CronDayOfWeek).reduce(
            function(memo, pairArray, index, array) {
              var property = parseInt(pairArray[0], 10) + 1;
              if (property <= 7) {
                var name = property.toString(10);
                memo[name] = mwa.proui._.extend({}, pairArray[1], {name: name, property: property});
              }
              return memo;
            },
            {}
        );

    /**
     * The enumeration for scheduler request type.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.SchedulerRequestType
     * @memberof mwa.enumeration
     */
    global.SchedulerRequestType = {
      GET: {
        name: 'GET'
      },
      POST: {
        name: 'POST'
      },
      PUT: {
        name: 'PUT'
      },
      PATCH: {
        name: 'PATCH'
      },
      DELETE: {
        name: 'DELETE'
      }
    };

    /**
     * The enumeration for scheduler pattern start time.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.SchedulerPatternStartTime
     * @memberof mwa.enumeration
     */
    global.SchedulerPatternStartTime =
        mwa.proui._.range(0, 48).reduce(
            function(memo, property, index, array) {
              var baseDate = new Date();
              // CAUTION: SDO-1208 (https://acropolis.atlassian.net/browse/SDO-1208)
              // It is necessary to specify 00:00:00 time by using setHours function instead of constructor with 0
              // in order to specify 00:00:00 time correctly regardless of any time zone.
              baseDate.setHours(0, 0, 0, 0);
              baseDate.setMinutes(property * 30);

              var time = mwa.proui.util.Date.formatDate(baseDate).slice(11);

              // CAUTION: NVXN-1295 (https://acropolis.atlassian.net/browse/NVXN-1295)
              // The "property" field is required to display these definitions as combo box in
              // metadata editor.
              memo[time] = {
                name: time,
                property: time,
                label: time,
                index: property
              };

              return memo;
            },
            {}
        );

    /**
     * The enumeration for scheduler pattern mode.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.SchedulerPatternMode
     * @memberof mwa.enumeration
     */
    global.SchedulerPatternMode = {
      // CAUTION: NVX-7666 (https://www.tool.sony.biz/jira/browse/NVX-7666)
      // The "property" field is required to display these definitions as drop down button in
      // metadata editor.
      DAY: {
        name: 'DAY',
        property: 'DAY',
        label: global.Message['SENTENCE_DAY'],
        index: 0
      },
      DAY_OF_WEEK: {
        name: 'DAY_OF_WEEK',
        property: 'DAY_OF_WEEK',
        label: global.Message['SENTENCE_DAY_OF_WEEK'],
        index: 1
      }
    };

    /**
     * The enumeration for scheduler pattern phase.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.SchedulerPatternPhase
     * @memberof mwa.enumeration
     */
    global.SchedulerPatternPhase = {
      // CAUTION: NVX-7666 (https://www.tool.sony.biz/jira/browse/NVX-7666)
      // The "property" field is required to display these definitions as drop down button in
      // metadata editor.
      FIRST: {
        name: 'FIRST',
        property: 'FIRST',
        label: global.Message['SENTENCE_FIRST'],
        index: 0
      },
      SECOND: {
        name: 'SECOND',
        property: 'SECOND',
        label: global.Message['SENTENCE_SECOND'],
        index: 1
      },
      THIRD: {
        name: 'THIRD',
        property: 'THIRD',
        label: global.Message['SENTENCE_THIRD'],
        index: 2
      },
      FOURTH: {
        name: 'FOURTH',
        property: 'FOURTH',
        label: global.Message['SENTENCE_FOURTH'],
        index: 3
      },
      LAST: {
        name: 'LAST',
        property: 'LAST',
        label: global.Message['SENTENCE_LAST'],
        index: 4
      }
    };

    /**
     * The enumeration for scheduler pattern day.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.SchedulerPatternDay
     * @memberof mwa.enumeration
     */
    global.SchedulerPatternDay =
        mwa.proui._.range(1, 33).reduce(
            function(memo, property, index, array) {
              var day = property.toString(10);

              // CAUTION: NVX-7666 (https://www.tool.sony.biz/jira/browse/NVX-7666)
              // The "property" field is required to display these definitions as drop down button
              // in metadata editor.
              memo[day] = {
                name: day,
                property: property,
                label: (property === 32) ?
                    global.Message['SENTENCE_LAST_DAY'] :
                    (
                        (29 <= property && property <= 31) ?
                            (
                                day +
                                ' ' +
                                '(' +
                                global.Message['INFORMATION_NOT_RUN_ON_NON_EXISTENT_DAY'] +
                                ')'
                            ) :
                            day
                    ),
                index: property - 1
              };

              return memo;
            },
            {}
        );

    /**
     * The enumeration for scheduler pattern day of week.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.SchedulerPatternDayOfWeek
     * @memberof mwa.enumeration
     */
    global.SchedulerPatternDayOfWeek =
        // CAUTION: NVX-7666 (https://www.tool.sony.biz/jira/browse/NVX-7666)
        // The "property" field is required to display these definitions as check box in metadata
        // editor.
        mwa.proui._.extend(
            {
              // TODO: NVXN-1280 (https://acropolis.atlassian.net/browse/NVXN-1280)
              // To uncomment after clarifying the way to achieve the following patterns.
              // '-1': {
              //   name: '-1',
              //   property: -1,
              //   label: global.Message['SENTENCE_WEEKDAY'],
              //   shortLabel: global.Message['SENTENCE_WEEKDAY'],
              //   index: -1,
              //   specialCharacter: 'WD'
              // },
              // '0': {
              //   name: '0',
              //   property: 0,
              //   label: global.Message['SENTENCE_WEEKEND'],
              //   shortLabel: global.Message['SENTENCE_WEEKEND'],
              //   index: 0,
              //   specialCharacter: 'WE'
              // }
            },
            global.QuartzDayOfWeek
        );

    /**
     * The enumeration for scheduler pattern type.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.SchedulerPatternType
     * @memberof mwa.enumeration
     */
    global.SchedulerPatternType = {
      // CAUTION: NVX-7666 (https://www.tool.sony.biz/jira/browse/NVX-7666)
      // The "property" field is required to display these definitions as drop down button in
      // metadata editor.
      DATE: {
        name: 'DATE',
        property: 'DATE',
        label: global.Message['SENTENCE_DATE'],
        index: 0,
        unit: null,
        max: 1,
        metadataFieldArray: [
          'pattern.startTime',
          'pattern.startDate'
        ]
      },
      HOURLY: {
        name: 'HOURLY',
        property: 'HOURLY',
        label: global.Message['SENTENCE_HOURLY'],
        index: 1,
        unit: 'HOUR',
        max: 23,
        metadataFieldArray: [
          'pattern.startTime',
          'pattern.startDate',
          'pattern.endDate',
          'pattern.interval'
        ]
      },
      DAILY: {
        name: 'DAILY',
        property: 'DAILY',
        label: global.Message['SENTENCE_DAILY'],
        index: 2,
        unit: 'DAY',
        max: 30,
        metadataFieldArray: [
          'pattern.startTime',
          'pattern.startDate',
          'pattern.endDate',
          'pattern.interval'
        ]
      },
      WEEKLY: {
        name: 'WEEKLY',
        property: 'WEEKLY',
        label: global.Message['SENTENCE_WEEKLY'],
        index: 3,
        unit: 'WEEK',
        max: 4,
        metadataFieldArray: [
          'pattern.startTime',
          'pattern.startDate',
          'pattern.endDate',
          'pattern.interval',
          'pattern.dayOfWeek'
        ]
      },
      MONTHLY: {
        name: 'MONTHLY',
        property: 'MONTHLY',
        label: global.Message['SENTENCE_MONTHLY'],
        index: 4,
        unit: 'MONTH',
        max: 11,
        metadataFieldArray: [
          'pattern.startTime',
          'pattern.startDate',
          'pattern.endDate',
          'pattern.interval',
          'pattern.mode'
        ]
      }
    };
    global.SchedulerPatternType =
        mwa.proui._.mapObject(
            global.SchedulerPatternType,
            function(value, key) {
              var today = new Date();
              today =
                  new Date(
                      today.getFullYear(),
                      today.getMonth(),
                      today.getDate(),
                      0,
                      0,
                      0
                  );

              return mwa.proui._.extend(
                  value,
                  {
                    metadataFieldArray: [
                      {
                        key: 'pattern.startTime',
                        index: 0,
                        title: global.Message['SENTENCE_START_TIME'],
                        type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                        valueCollection: null,
                        referenceValueCollection:
                            new mwa.proui.Backbone.Collection(
                                mwa.proui._.sortBy(
                                    mwa.proui._.values(global.SchedulerPatternStartTime),
                                    'index'
                                )
                            ),
                        metadataFieldValidatorCollection:
                            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                              {
                                type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.PATTERN,
                                value: /^([01]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/,
                                text:
                                    global.Message['VALIDATION_PATTERN'].replace(
                                        /\{0\}/,
                                        global.Message['SENTENCE_VALUE'].toLowerCase()
                                    ).replace(
                                        /\{1\}/,
                                        'hh:mm:ss'
                                    )
                              }
                            ]),
                        isArray: false,
                        isHidden: value.metadataFieldArray.indexOf('pattern.startTime') === -1,
                        isRequired: true,
                        isReadOnly: false
                      },
                      {
                        key: 'pattern.startDate',
                        index: 1,
                        title: global.Message['SENTENCE_START_DATE'],
                        type: mwa.prouiExt.metadataEditor.MetadataFieldType.DATE,
                        valueCollection: null,
                        referenceValueCollection: null,
                        metadataFieldValidatorCollection:
                            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                              {
                                type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MIN,
                                value: today.getTime(),
                                text:
                                    global.Message['VALIDATION_ON_OR_AFTER'].replace(
                                        /\{0\}/,
                                        global.Message['SENTENCE_DATE'].toLowerCase()
                                    ).replace(
                                        /\{1\}/,
                                        global.Message['SENTENCE_TODAY'].toLowerCase()
                                    )
                              }
                            ]),
                        isArray: false,
                        isHidden: value.metadataFieldArray.indexOf('pattern.startDate') === -1,
                        isRequired: true,
                        isReadOnly: false
                      },
                      {
                        key: 'pattern.endDate',
                        index: 2,
                        title: global.Message['SENTENCE_END_DATE'],
                        type: mwa.prouiExt.metadataEditor.MetadataFieldType.DATE,
                        valueCollection: null,
                        referenceValueCollection: null,
                        metadataFieldValidatorCollection:
                            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                              {
                                type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MIN,
                                value: today.getTime(),
                                text:
                                    global.Message['VALIDATION_ON_OR_AFTER'].replace(
                                        /\{0\}/,
                                        global.Message['SENTENCE_DATE'].toLowerCase()
                                    ).replace(
                                        /\{1\}/,
                                        global.Message['SENTENCE_TODAY'].toLowerCase()
                                    )
                              }
                            ]),
                        isArray: false,
                        isHidden: value.metadataFieldArray.indexOf('pattern.endDate') === -1,
                        isRequired: false,
                        isReadOnly: false
                      },
                      {
                        key: 'pattern.interval',
                        index: 3,
                        title: (value.unit == null) ?
                            global.Message['SENTENCE_INTERVAL'] :
                            global.Message['SENTENCE_INTERVAL_' + value.unit],
                        type: mwa.prouiExt.metadataEditor.MetadataFieldType.LONG,
                        valueCollection: null,
                        referenceValueCollection: null,
                        metadataFieldValidatorCollection:
                            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection([
                              {
                                type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MIN,
                                value: 1,
                                text:
                                    global.Message['VALIDATION_IS_OR_GREATER'].replace(
                                        /\{0\}/,
                                        global.Message['SENTENCE_VALUE'].toLowerCase()
                                    ).replace(
                                        /\{1\}/,
                                        (
                                            ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                                ' ' : ''
                                        ) +
                                        '1' +
                                        (
                                            ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                                ' ' : ''
                                        )
                                    )
                              },
                              {
                                type: mwa.prouiExt.metadataEditor.MetadataFieldValidatorType.MAX,
                                value: value.max,
                                text:
                                    global.Message['VALIDATION_IS_OR_SMALLER'].replace(
                                        /\{0\}/,
                                        global.Message['SENTENCE_VALUE'].toLowerCase()
                                    ).replace(
                                        /\{1\}/,
                                        (
                                            ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                                ' ' : ''
                                        ) +
                                        value.max.toString(10) +
                                        (
                                            ((mwa.proui.util.Platform.normalizeLanguageCode() || 'en') === 'ja') ?
                                                ' ' : ''
                                        )
                                    )
                              }
                            ]),
                        isArray: false,
                        isHidden: value.metadataFieldArray.indexOf('pattern.interval') === -1,
                        isRequired: true,
                        isReadOnly: value.name !== global.SchedulerPatternType.HOURLY.name
                      },
                      {
                        key: 'pattern.dayOfWeek',
                        index: 4,
                        title: global.Message['SENTENCE_DAY_OF_WEEK'],
                        type: mwa.prouiExt.metadataEditor.MetadataFieldType.LONG,
                        valueCollection:
                            new mwa.proui.Backbone.Collection(
                                mwa.proui._.sortBy(
                                    mwa.proui._.values(global.QuartzDayOfWeek),
                                    'index'
                                )
                            ),
                        referenceValueCollection: null,
                        metadataFieldValidatorCollection:
                            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                        isArray: true,
                        isHidden: value.metadataFieldArray.indexOf('pattern.dayOfWeek') === -1,
                        isRequired: true,
                        isReadOnly: false
                      },
                      {
                        key: 'pattern.mode',
                        index: 5,
                        title: global.Message['SENTENCE_MODE'],
                        type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                        valueCollection:
                            new mwa.proui.Backbone.Collection(
                                mwa.proui._.sortBy(
                                    mwa.proui._.values(global.SchedulerPatternMode),
                                    'index'
                                )
                            ),
                        referenceValueCollection: null,
                        metadataFieldValidatorCollection:
                            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                        isArray: false,
                        isHidden: value.metadataFieldArray.indexOf('pattern.mode') === -1,
                        isRequired: true,
                        isReadOnly: false,
                        metadataFields: {
                          '"DAY"': new mwa.prouiExt.metadataEditor.MetadataFieldCollection([
                            {
                              key: 'pattern.day',
                              index: 0,
                              title: global.Message['SENTENCE_DAY'],
                              type: mwa.prouiExt.metadataEditor.MetadataFieldType.LONG,
                              valueCollection:
                                  new mwa.proui.Backbone.Collection(
                                      mwa.proui._.sortBy(
                                          mwa.proui._.values(global.SchedulerPatternDay),
                                          'index'
                                      )
                                  ),
                              referenceValueCollection: null,
                              metadataFieldValidatorCollection:
                                  new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                              isArray: false,
                              isHidden: false,
                              isRequired: true,
                              isReadOnly: false
                            }
                          ]),
                          '"DAY_OF_WEEK"': new mwa.prouiExt.metadataEditor.MetadataFieldCollection([
                            {
                              key: 'pattern.phase',
                              index: 0,
                              title: global.Message['SENTENCE_PHASE'],
                              type: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
                              valueCollection:
                                  new mwa.proui.Backbone.Collection(
                                      mwa.proui._.sortBy(
                                          mwa.proui._.values(global.SchedulerPatternPhase),
                                          'index'
                                      )
                                  ),
                              referenceValueCollection: null,
                              metadataFieldValidatorCollection:
                                  new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                              isArray: false,
                              isHidden: false,
                              isRequired: true,
                              isReadOnly: false
                            },
                            {
                              key: 'pattern.dayOfWeek',
                              index: 1,
                              title: global.Message['SENTENCE_DAY_OF_WEEK'],
                              type: mwa.prouiExt.metadataEditor.MetadataFieldType.LONG,
                              valueCollection:
                                  new mwa.proui.Backbone.Collection(
                                      mwa.proui._.sortBy(
                                          mwa.proui._.values(global.SchedulerPatternDayOfWeek),
                                          'index'
                                      )
                                  ),
                              referenceValueCollection: null,
                              metadataFieldValidatorCollection:
                                  new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
                              isArray: false,
                              isHidden: false,
                              isRequired: true,
                              isReadOnly: false
                            }
                          ])
                        }
                      }
                    ]
                  }
              );
            }
        );

    /**
     * The enumeration for activity type.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.ActivityType
     * @memberof mwa.enumeration
     */
    global.ActivityType = {
      WORKFLOW: {
        name: 'WORKFLOW',
        pluginType: 'BPMN'
      },
      TASK: {
        name: 'TASK',
        pluginType: 'BUNDLE'
      },
      START: {
        name: 'START',
        pluginType: 'BUNDLE'
      },
      END: {
        name: 'END',
        pluginType: 'BUNDLE'
      },
      CONVERTER: {
        name: 'CONVERTER',
        pluginType: 'BUNDLE'
      },
      DTABLE: {
        name: 'DTABLE',
        pluginType: 'RULE'
      },
      DSL: {
        name: 'DSL',
        pluginType: 'RULE'
      },
      DSLR: {
        name: 'DSLR',
        pluginType: 'RULE'
      },
      DRL: {
        name: 'DRL',
        pluginType: 'RULE'
      }
    };

    /**
     * The enumeration for activity parameter.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.ActivityParameter
     * @memberof mwa.enumeration
     */
    global.ActivityParameter = {
      PROFILE_ID: {
        name: 'PROFILE_ID',
        key: 'ProfileId'
      },
      CONDITION_CONNECTION_PORT_ID: {
        name: 'CONDITION_CONNECTION_PORT_ID',
        key: 'conditionConnectionPortId'
      },
      CONDITION: {
        name: 'CONDITION',
        key: 'condition'
      }
    };

    /**
     * The enumeration for activity parameter type.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.ActivityParameterType
     * @memberof mwa.enumeration
     */
    global.ActivityParameterType = {
      // CAUTION: NVX-4601 (https://www.tool.sony.biz/jira/browse/NVX-4601)
      // The "property" field is required to display these definitions as drop down button in
      // metadata editor.
      // Server Data
      ACTIVITY_PROFILE_ID: {
        isImplemented: true,
        name: 'ACTIVITY_PROFILE_ID',
        property: 'TypeActivityProfileId',
        typeName: 'TypeActivityProfileId',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_PROFILE'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: true,
        validateText: function(text) {
          return mwa.enumeration.ActivityParameterType.STRING.validateText(text);
        },
        encodeText: function(text) {
          return mwa.enumeration.ActivityParameterType.STRING.encodeText(text);
        },
        decodeText: function(text) {
          return mwa.enumeration.ActivityParameterType.STRING.decodeText(text);
        },
        formatText: function(text) {
          return mwa.enumeration.ActivityParameterType.STRING.formatText(text);
        }
      },
      ACTIVITY_PROVIDER_ID: {
        isImplemented: false,
        name: 'ACTIVITY_PROVIDER_ID',
        property: 'TypeActivityProviderId',
        typeName: 'TypeActivityProviderId',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_PROVIDER'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: true,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      APPROVAL: {
        isImplemented: false,
        name: 'APPROVAL',
        property: 'TypeApproval',
        typeName: 'TypeApproval',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.BOOLEAN,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_APPROVAL'],
        isBoolean: true,
        isNumber: false,
        isString: false,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      BASE: {
        isImplemented: false,
        name: 'BASE',
        property: 'TypeBase',
        typeName: 'TypeBase',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_BASE'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      BOOLEAN: {
        isImplemented: true,
        name: 'BOOLEAN',
        property: 'TypeBoolean',
        typeName: 'TypeBoolean',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.BOOLEAN,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_BOOLEAN'],
        isBoolean: true,
        isNumber: false,
        isString: false,
        isSetting: false,
        validateText: function(text) {
          var validationTextError = null;

          if (typeof text !== 'string' || (text !== 'true' && text !== 'false')) {
            validationTextError =
                mwa.enumeration.Message['VALIDATION_TYPE'].replace(
                    /\{0\}/,
                    mwa.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                ).replace(
                    /\{1\}/,
                    mwa.enumeration.Message['SENTENCE_BOOLEAN'].toLowerCase()
                );
          }

          return validationTextError;
        },
        encodeText: function(text) {
          var validationTextError =
              mwa.enumeration.ActivityParameterType.BOOLEAN.validateText(text);

          return (typeof validationTextError !== 'string') ?
              text : null;
        },
        decodeText: function(text) {
          return (typeof text === 'string') ?
              text : null;
        },
        formatText: function(text) {
          return (typeof text === 'string') ?
              (
                  (text === 'true') ?
                      mwa.enumeration.Message['SENTENCE_YES'] :
                      mwa.enumeration.Message['SENTENCE_NO']
              ) :
              null;
        }
      },
      COST: {
        isImplemented: false,
        name: 'COST',
        property: 'TypeCost',
        typeName: 'TypeCost',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.LONG,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_COST'],
        isBoolean: false,
        isNumber: true,
        isString: false,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      DATE_TIME: {
        isImplemented: false,
        name: 'DATE_TIME',
        property: 'TypeDateTime',
        typeName: 'TypeDateTime',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.DATE_TIME,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_DATE_TIME'],
        isBoolean: false,
        isNumber: true,
        isString: false,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      ENUM: {
        isImplemented: false,
        name: 'ENUM',
        property: 'TypeEnum',
        typeName: 'TypeEnum',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.OBJECT,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_ENUMERATION'],
        isBoolean: true,
        isNumber: true,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      LIST_STRING: {
        isImplemented: false,
        name: 'LIST_STRING',
        property: 'TypeListString',
        typeName: 'TypeListString',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.TEXT,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_STRING_LIST'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      LONG: {
        isImplemented: true,
        name: 'LONG',
        property: 'TypeLong',
        typeName: 'TypeLong',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.LONG,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_NUMBER'],
        isBoolean: false,
        isNumber: true,
        isString: false,
        isSetting: false,
        validateText: function(text) {
          var validationTextError = null;

          if (!mwa.proui.util.String.isValidLong(text)) {
            validationTextError =
                mwa.enumeration.Message['VALIDATION_TYPE'].replace(
                    /\{0\}/,
                    mwa.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                ).replace(
                    /\{1\}/,
                    mwa.enumeration.Message['SENTENCE_NUMBER'].toLowerCase()
                );
          }

          return validationTextError;
        },
        encodeText: function(text) {
          var validationTextError =
              mwa.enumeration.ActivityParameterType.LONG.validateText(text);

          return (typeof validationTextError !== 'string') ?
              text : null;
        },
        decodeText: function(text) {
          return (typeof text === 'string') ?
              text : null;
        },
        formatText: function(text) {
          return text;
        }
      },
      MAP: {
        isImplemented: false,
        name: 'MAP',
        property: 'TypeMap',
        typeName: 'TypeMap',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_MAP'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      MWA_URI: {
        isImplemented: false,
        name: 'MWA_URI',
        property: 'TypeMwaUri',
        typeName: 'TypeMwaUri',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.URL,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_URI'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return (typeof text === 'string') ?
              text.replace(/^(mwa:\/)(\/.+)$/, '$2') : null;
        }
      },
      PASSWORD: {
        isImplemented: false,
        name: 'PASSWORD',
        property: 'TypePassword',
        typeName: 'TypePassword',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_PASSWORD'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      RESOURCE: {
        isImplemented: false,
        name: 'RESOURCE',
        property: 'TypeResource',
        typeName: 'TypeResource',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_RESOURCE'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: true,
        validateText: function(text) {
          return null; // TODO: To implement.
        },
        encodeText: function(text) {
          return text; // TODO: To implement.
        },
        decodeText: function(text) {
          return text; // TODO: To implement.
        },
        formatText: function(text) {
          return text; // TODO: To implement.
        }
      },
      STRING: {
        isImplemented: true,
        name: 'STRING',
        property: 'TypeString',
        typeName: 'TypeString',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.STRING,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_STRING'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          var validationTextError = null;

          if (typeof text !== 'string') {
            validationTextError =
                mwa.enumeration.Message['VALIDATION_TYPE'].replace(
                    /\{0\}/,
                    mwa.enumeration.Message['SENTENCE_VALUE'].toLowerCase()
                ).replace(
                    /\{1\}/,
                    mwa.enumeration.Message['SENTENCE_STRING'].toLowerCase()
                );
          }

          return validationTextError;
        },
        encodeText: function(text) {
          var validationTextError =
              mwa.enumeration.ActivityParameterType.STRING.validateText(text);
          return (typeof validationTextError !== 'string') ?
              '\\"' + text + '\\"' : null;
        },
        decodeText: function(text) {
          return (typeof text === 'string') ?
              text.substr(2, text.length - 4) : null;
        },
        formatText: function(text) {
          return text;
        }
      },
      URI: {
        isImplemented: true,
        name: 'URI',
        property: 'TypeUri',
        typeName: 'TypeUri',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.URL,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_PATH'],
        isBoolean: false,
        isNumber: false,
        isString: true,
        isSetting: false,
        validateText: function(text) {
          return mwa.enumeration.ActivityParameterType.STRING.validateText(text);
        },
        encodeText: function(text) {
          return mwa.enumeration.ActivityParameterType.STRING.encodeText(text);
        },
        decodeText: function(text) {
          return mwa.enumeration.ActivityParameterType.STRING.decodeText(text);
        },
        formatText: function(text) {
          return (typeof text === 'string') ?
              text.replace(/^(.+:\/)(\/.+)$/, '$2') : null;
        }
      },
      // Client Data
      SETTING: {
        isImplemented: false,
        name: 'SETTING',
        property: 'TypeSetting',
        typeName: 'TypeSetting',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.NONE,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        // CAUTION:
        // IT is important to set null in order to display the type name directly in workflow
        // parameter editor.
        label: null,
        isBoolean: false,
        isNumber: false,
        isString: false,
        isSetting: true,
        validateText: null, // To be same as STRING.
        encodeText: null,   // To be same as STRING.
        decodeText: null,   // To be same as STRING.
        formatText: null    // To be same as STRING.
      },
      UNKNOWN: {
        isImplemented: false,
        name: 'UNKNOWN',
        property: 'TypeUnknown',
        typeName: 'TypeUnknown',
        metadataFieldType: mwa.prouiExt.metadataEditor.MetadataFieldType.NONE,
        metadataFieldValidatorCollection:
            new mwa.prouiExt.metadataEditor.MetadataFieldValidatorCollection(),
        label: global.Message['TITLE_UNKNOWN'],
        isBoolean: false,
        isNumber: false,
        isString: false,
        isSetting: false,
        validateText: function(text) {
          return null;
        },
        encodeText: function(text) {
          return text;
        },
        decodeText: function(text) {
          return text;
        },
        formatText: function(text) {
          return text;
        }
      }
    };
    global.ActivityParameterType.SETTING =
        mwa.proui._.extend(
            global.ActivityParameterType.SETTING,
            mwa.proui._.pick(
                global.ActivityParameterType.STRING,
                'validateText',
                'encodeText',
                'decodeText',
                'formatText'
            )
        );

    /**
     * The enumeration for activity parameter setting type.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.ActivityParameterSettingType
     * @memberof mwa.enumeration
     */
    global.ActivityParameterSettingType = {
      // CAUTION:
      // The key must be same as mwa.enumeration.ActivityParameterType because the value will be
      // decided by it (e.g. in createParameterMetadataFieldCollection function of activity model).
      // Server Data
      ACTIVITY_PROFILE_ID: {
        name: 'ACTIVITY_PROFILE_ID',
        createItemCollection: function(activityModel, activityParameterModel) {
          var itemCollection =
              new mwa.model.activityProfile.ActivityProfileCollection();
          itemCollection.url = function(resource) {
            return (
                itemCollection.__proto__.url.apply(this, [resource]) + '?' +
                mwa.proui.Backbone.$.param(
                    {
                      sort: [itemCollection.comparator + '+'],
                      filter:
                          (
                              activityParameterModel instanceof
                              mwa.model.activityParameter.ActivityParameterModel &&
                              activityParameterModel.get('key') ===
                              mwa.enumeration.ActivityParameter.PROFILE_ID.key
                          ) ?
                              ['templateId' + '==' + activityModel.get('id')] :
                              []
                    },
                    true
                )
            );
          };

          return itemCollection;
        },
        createValueCollection: function(itemCollection, activityModel, activityParameterModel) {
          return (itemCollection instanceof mwa.model.activityProfile.ActivityProfileCollection) ?
              new mwa.proui.Backbone.Collection(
                  itemCollection.filter(
                      function(itemModel, index, array) {
                        return (
                            (
                                activityParameterModel instanceof
                                mwa.model.activityParameter.ActivityParameterModel &&
                                activityParameterModel.get('key') ===
                                mwa.enumeration.ActivityParameter.PROFILE_ID.key
                            ) ?
                                itemModel.get('templateId') === activityModel.get('id') :
                                true
                        );
                      }
                  ).map(
                      function(itemModel, index, array) {
                        return new mwa.proui.Backbone.Model({
                          label: itemModel.get('name'),
                          property: itemModel.id
                        });
                      }
                  )
              ) :
              null;
        }
      },
      ACTIVITY_PROVIDER_ID: {
        name: 'ACTIVITY_PROVIDER_ID',
        createItemCollection: function(activityModel, activityParameterModel) {
          return null; // TODO: To implement.
        },
        createValueCollection: function(itemCollection, activityModel, activityParameterModel) {
          return null; // TODO: To implement.
        }
      },
      URI: {
        name: 'URI',
        createItemCollection: function(activityModel, activityParameterModel) {
          return null; // TODO: To implement.
        },
        createValueCollection: function(itemCollection, activityModel, activityParameterModel) {
          // CAUTION: NVXN-2154 (https://acropolis.atlassian.net/browse/NVXN-2154)
          // It is necessary to define new collection class in order to set specific query
          // parameters correctly because this instance will be cloned in metadata editor and
          // url setting won't be turned over to cloned instance.
          return new (
              mwa.model.directory.DirectoryCollection.extend(
                  {
                    url: function(resource) {
                      return (
                          mwa.model.directory.DirectoryCollection.prototype.url.apply(
                              this, [resource]
                          ) + '?' +
                          mwa.proui.Backbone.$.param(
                              {
                                templateId: activityModel.id,
                                key: activityParameterModel.get('key')
                              },
                              true
                          )
                      );
                    }
                  }
              )
          )();
        }
      },
      // Client Data
      SETTING: {
        name: 'SETTING',
        createItemCollection: function(activityModel, activityParameterModel) {
          var itemCollection =
              (
                  activityParameterModel instanceof
                  mwa.model.activityParameter.ActivityParameterModel
              ) ?
                  new mwa.model.activityParameterTypeValue.ActivityParameterTypeValueCollection(
                      null,
                      {typeName: activityParameterModel.get('typeName')}
                  ) :
                  new mwa.model.activityParameterType.ActivityParameterTypeCollection();
          itemCollection.url = function(resource) {
            return (
                itemCollection.__proto__.url.apply(this, [resource]) + '?' +
                mwa.proui.Backbone.$.param(
                    {
                      sort: [itemCollection.comparator + '+']
                    },
                    true
                )
            );
          };

          return itemCollection;
        },
        createValueCollection: function(itemCollection, activityModel, activityParameterModel) {
          return (itemCollection instanceof mwa.model.activityParameterTypeValue.ActivityParameterTypeValueCollection) ?
              new mwa.proui.Backbone.Collection(
                  itemCollection.map(
                      function(itemModel, index, array) {
                        return new mwa.proui.Backbone.Model({
                          label: itemModel.get('name'),
                          property: itemModel.id
                        });
                      }
                  )
              ) :
              null;
        }
      },
      NONE: {
        name: 'NONE',
        createItemCollection: function(activityModel, activityParameterModel) {
          return null;
        },
        createValueCollection: function(itemCollection, activityModel, activityParameterModel) {
          return null;
        }
      }
    };
    global.ActivityParameterSettingType.MWA_URI = // Deprecated
        mwa.proui._.extend(
            {},
            global.ActivityParameterSettingType.URI,
            {
              name: 'MWA_URI'
            }
        );

    /**
     * The enumeration for activity condition condition operator.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.ActivityConditionConditionOperator
     * @memberof mwa.enumeration
     */
    global.ActivityConditionConditionOperator = {
      AND: {
        name: 'AND',
        label: global.Message['LOWER_AND'],
        index: 0,
        operator: '&&'
      },
      OR: {
        name: 'OR',
        label: global.Message['LOWER_OR'],
        index: 1,
        operator: '||'
      }
    };

    /**
     * The enumeration for activity condition comparison operator.
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators|Comparison operators - JavaScript | MDN}
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.ActivityConditionComparisonOperator
     * @memberof mwa.enumeration
     */
    global.ActivityConditionComparisonOperator = {
      EQUALITY: {
        name: 'EQUALITY',
        label: global.Message['LOWER_IS'],
        index: 0,
        operator: '==',
        canUse: function(activityParameterType) {
          return (
              activityParameterType.isBoolean ||
              activityParameterType.isNumber ||
              activityParameterType.isString ||
              activityParameterType.isSetting
          );
        },
        evaluate: function(leftValue, rightValue) {
          return leftValue === rightValue;
        }
      },
      INEQUALITY: {
        name: 'INEQUALITY',
        label: global.Message['LOWER_IS_NOT'],
        index: 1,
        operator: '!=',
        canUse: function(activityParameterType) {
          return (
              activityParameterType.isBoolean ||
              activityParameterType.isNumber ||
              activityParameterType.isString ||
              activityParameterType.isSetting
          );
        },
        evaluate: function(leftValue, rightValue) {
          return leftValue !== rightValue;
        }
      },
      GREATER_THAN: {
        name: 'GREATER_THAN',
        label: global.Message['LOWER_GREATER'],
        index: 2,
        operator: '>',
        canUse: function(activityParameterType) {
          return (
              activityParameterType.isNumber
          );
        },
        evaluate: function(leftValue, rightValue) {
          return parseInt(leftValue, 10) > parseInt(rightValue, 10);
        }
      },
      GREATER_THAN_OR_EQUAL: {
        name: 'GREATER_THAN_OR_EQUAL',
        label: global.Message['LOWER_IS_OR_GREATER'],
        index: 3,
        operator: '>=',
        canUse: function(activityParameterType) {
          return (
              activityParameterType.isNumber
          );
        },
        evaluate: function(leftValue, rightValue) {
          return parseInt(leftValue, 10) >= parseInt(rightValue, 10);
        }
      },
      LESS_THAN: {
        name: 'LESS_THAN',
        label: global.Message['LOWER_LESS'],
        index: 4,
        operator: '<',
        canUse: function(activityParameterType) {
          return (
              activityParameterType.isNumber
          );
        },
        evaluate: function(leftValue, rightValue) {
          return parseInt(leftValue, 10) < parseInt(rightValue, 10);
        }
      },
      LESS_THAN_OR_EQUAL: {
        name: 'LESS_THAN_OR_EQUAL',
        label: global.Message['LOWER_IS_OR_LESS'],
        index: 5,
        operator: '<=',
        canUse: function(activityParameterType) {
          return (
              activityParameterType.isNumber
          );
        },
        evaluate: function(leftValue, rightValue) {
          return parseInt(leftValue, 10) <= parseInt(rightValue, 10);
        }
      }
    };

    /**
     * The enumeration for activity instance status.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.ActivityInstanceStatus
     * @memberof mwa.enumeration
     */
    global.ActivityInstanceStatus = {
      QUEUED: {
        name: 'QUEUED',
        stable: true,
        terminated: false,
        error: false,
        cancel: false,
        styleClass: '',
        progressBarCondition: null
      },
      READY: {
        name: 'READY',
        stable: true,
        terminated: false,
        error: false,
        cancel: false,
        styleClass: '',
        progressBarCondition: null
      },
      CANCELLED: {
        name: 'CANCELLED',
        stable: true,
        terminated: true,
        error: false,
        cancel: true,
        styleClass: 'proui-state-warning',
        progressBarCondition: mwa.proui.progressBar.ProgressBarCondition.CANCELED
      },
      ERROR: {
        name: 'ERROR',
        stable: true,
        terminated: true,
        error: true,
        cancel: false,
        styleClass: 'proui-state-error',
        progressBarCondition: mwa.proui.progressBar.ProgressBarCondition.ERROR
      }
    };

    /**
     * The enumeration for workflow step.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowStep
     * @memberof mwa.enumeration
     */
    global.WorkflowStep = {
      // CAUTION:
      // To change
      // mwa-view-workflow-parameter-editor-workflow-parameter-item-value-source-drop-down-list
      // class style setting when predefined workflow steps is increased.
      IMMEDIATE_VALUE: {
        name: 'IMMEDIATE_VALUE',
        id: '4a54c1be-a234-40e8-9f95-55d255345546',
        text: global.Message['SENTENCE_INPUT_VALUE']
      },
      SETTING_VALUE: {
        name: 'SETTING_VALUE',
        id: 'a6fa3fce-f237-4b94-b8ee-85d1ca1aaaad',
        text: global.Message['SENTENCE_SELECTIVE_VALUE']
      }
    };

    /**
     * The enumeration for workflow step type.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowStepType
     * @memberof mwa.enumeration
     */
    global.WorkflowStepType = {
      START: {
        name: 'START',
        templateName: 'com.sony.pro.mwa.control.activity.StartTask',
        hasParameter: true
      },
      END: {
        name: 'END',
        templateName: 'com.sony.pro.mwa.control.activity.EndTask',
        hasParameter: true
      },
      CONDITIONAL_BRANCH: {
        name: 'CONDITIONAL_BRANCH',
        templateName: 'com.sony.pro.mwa.control.activity.ConditionalBranchTask',
        hasParameter: false
      },
      PARALLEL_BRANCH: {
        name: 'PARALLEL_BRANCH',
        templateName: 'com.sony.pro.mwa.control.activity.ParallelBranchTask',
        hasParameter: false
      },
      // TODO: To use only one converging step for both conditional branch step and parallel branch step.
      // CONVERGING: {
      //   name: 'CONVERGING',
      //   templateName: 'com.sony.pro.mwa.control.activity.ConvergingTask',
      //   hasParameter: false
      // }
      CONVERGING: {
        name: 'CONVERGING',
        templateName: 'com.sony.pro.mwa.control.activity.ConditionalConvergingTask',
        hasParameter: false
      }
    };

    /**
     * The enumeration for workflow connection port type.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowConnectionPortType
     * @memberof mwa.enumeration
     */
    global.WorkflowConnectionPortType = {
      CONTROL_INPUT: {
        name: 'CONTROL_INPUT',
        workflowStepParameterName: null
      },
      CONTROL_OUTPUT: {
        name: 'CONTROL_OUTPUT',
        workflowStepParameterName: null
      },
      DATA_INPUT: {
        name: 'DATA_INPUT',
        workflowStepParameterName: 'inputActivityParameterCollection'
      },
      DATA_OUTPUT: {
        name: 'DATA_OUTPUT',
        workflowStepParameterName: 'outputActivityParameterCollection'
      }
    };

    /**
     * The enumeration for workflow connection port color.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowConnectionPortColor
     * @memberof mwa.enumeration
     */
    global.WorkflowConnectionPortColor = {
      // Control Workflow Connection
      GRAY: {
        name: 'GRAY',
        isArray: false,
        // CAUTION:
        // It is important to use different value from GREEN and RED definitions without using null
        // to detect this color for control workflow connection port basically.
        activityParameterType: global.ActivityParameterType.UNKNOWN,
        styleClass: 'mwa-view-workflow-connection-port-color-gray'
      },
      GREEN: {
        name: 'GREEN',
        isArray: false,
        activityParameterType: null,
        styleClass: 'mwa-view-workflow-connection-port-color-green'
      },
      RED: {
        name: 'RED',
        isArray: false,
        activityParameterType: null,
        styleClass: 'mwa-view-workflow-connection-port-color-red'
      },
      // Data Workflow Connection
      WHITE: {
        name: 'WHITE',
        isArray: false,
        activityParameterType: global.ActivityParameterType.SETTING,
        styleClass: 'mwa-view-workflow-connection-port-color-white'
      },
      ORANGE: {
        name: 'ORANGE',
        isArray: false,
        activityParameterType: global.ActivityParameterType.ACTIVITY_PROFILE_ID,
        styleClass: 'mwa-view-workflow-connection-port-color-orange'
      },
      YELLOW: {
        name: 'YELLOW',
        isArray: false,
        activityParameterType: global.ActivityParameterType.URI,
        styleClass: 'mwa-view-workflow-connection-port-color-yellow'
      },
      BROWN: {
        name: 'BROWN',
        isArray: false,
        activityParameterType: global.ActivityParameterType.BOOLEAN,
        styleClass: 'mwa-view-workflow-connection-port-color-brown'
      },
      BLUE: {
        name: 'BLUE',
        isArray: false,
        activityParameterType: global.ActivityParameterType.STRING,
        styleClass: 'mwa-view-workflow-connection-port-color-blue'
      },
      PURPLE: {
        name: 'PURPLE',
        isArray: false,
        activityParameterType: global.ActivityParameterType.LONG,
        styleClass: 'mwa-view-workflow-connection-port-color-purple'
      }
    };
    global.WorkflowConnectionPortColor =
        mwa.proui._.extend(
            global.WorkflowConnectionPortColor,
            mwa.proui._.keys(global.WorkflowConnectionPortColor).reduce(
                function(memo, workflowConnectionPortColorName, index, array) {
                  var workflowConnectionPortColor =
                      global.WorkflowConnectionPortColor[workflowConnectionPortColorName];
                  var name = 'DARK_' + workflowConnectionPortColor.name;
                  var styleClass = workflowConnectionPortColor.styleClass + '-dark';

                  memo[name] =
                      mwa.proui._.extend(
                          // CAUTION:
                          // It is necessary to pass an empty object not to change the original
                          // values.
                          {},
                          workflowConnectionPortColor,
                          {
                            name: name,
                            isArray: true,
                            styleClass: styleClass
                          }
                      );

                  return memo;
                },
                {}
            )
        );

    /**
     * The enumeration for workflow diagram operation.
     * @deprecated since version 2.13.0.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowDiagramOperation
     * @memberof mwa.enumeration
     */
    global.WorkflowDiagramOperation = {
      DEPLOY: {
        name: 'DEPLOY'
      }
    };

    /**
     * The enumeration for workflow diagram root context menu.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowDiagramRootContextMenu
     * @memberof mwa.enumeration
     */
    global.WorkflowDiagramRootContextMenu = {
      ADD: {
        name: 'ADD',
        label: global.Message['FOOTNOTE_ADD_NEW_WORKFLOW_DIAGRAM'],
        index: 0,
        isDisabled: function() {
          return false;
        }
      }
    };

    /**
     * The enumeration for workflow diagram context menu.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowDiagramContextMenu
     * @memberof mwa.enumeration
     */
    global.WorkflowDiagramContextMenu = {
      RENAME: {
        name: 'RENAME',
        label: global.Message['SENTENCE_RENAME'],
        index: 0,
        isDisabled: function(workflowDiagramModel) {
          return false;
        }
      },
      SAVE: {
        name: 'SAVE',
        label: global.Message['SENTENCE_SAVE'],
        index: 1,
        isDisabled: function(workflowDiagramModel) {
          return !workflowDiagramModel.hasChangedWorkflowItems();
        }
      },
      DEPLOY: {
        name: 'DEPLOY',
        label: global.Message['SENTENCE_DEPLOY'],
        index: 2,
        isDisabled: function(workflowDiagramModel) {
          return false;
        }
      },
      DELETE: {
        name: 'DELETE',
        label: global.Message['SENTENCE_DELETE'],
        index: 3,
        isDisabled: function(workflowDiagramModel) {
          return false;
        }
      }
    };

    /**
     * The enumeration for workflow context menu.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowContextMenu
     * @memberof mwa.enumeration
     */
    global.WorkflowContextMenu = {
      RENAME: {
        name: 'RENAME',
        label: global.Message['SENTENCE_RENAME'],
        index: 0,
        isDisabled: function(workflowModel) {
          return false;
        }
      },
      ADD_ACTIVITY_PROFILE_GROUP: {
        name: 'ADD_ACTIVITY_PROFILE_GROUP',
        label: global.Message['FOOTNOTE_ADD_NEW_WORKFLOW_PRESET_GROUP'],
        index: 1,
        isDisabled: function(workflowModel) {
          return false;
        }
      },
      ADD_ACTIVITY_PROFILE: {
        name: 'ADD_ACTIVITY_PROFILE',
        label: global.Message['FOOTNOTE_ADD_NEW_WORKFLOW_PRESET'],
        index: 2,
        isDisabled: function(workflowModel) {
          return false;
        }
      },
      DELETE: {
        name: 'DELETE',
        label: global.Message['SENTENCE_DELETE'],
        index: 3,
        isDisabled: function(workflowModel) {
          return workflowModel.get('predefineFlag');
        }
      }
    };

    /**
     * The enumeration for workflow preset group context menu.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowPresetGroupContextMenu
     * @memberof mwa.enumeration
     */
    global.WorkflowPresetGroupContextMenu = {
      EDIT: {
        name: 'EDIT',
        label: global.Message['SENTENCE_EDIT'],
        index: 0,
        isDisabled: function(activityProfileGroupModel) {
          return false;
        }
      },
      ADD_ACTIVITY_PROFILE: {
        name: 'ADD_ACTIVITY_PROFILE',
        label: global.Message['FOOTNOTE_ADD_NEW_WORKFLOW_PRESET'],
        index: 1,
        isDisabled: function(activityProfileGroupModel) {
          return false;
        }
      },
      DELETE: {
        name: 'DELETE',
        label: global.Message['SENTENCE_DELETE'],
        index: 2,
        isDisabled: function(activityProfileGroupModel) {
          return false;
        }
      }
    };

    /**
     * The enumeration for workflow preset context menu.
     * @readonly
     * @enum {object}
     * @alias mwa.enumeration.WorkflowPresetContextMenu
     * @memberof mwa.enumeration
     */
    global.WorkflowPresetContextMenu = {
      EDIT: {
        name: 'EDIT',
        label: global.Message['SENTENCE_EDIT'],
        index: 0,
        isDisabled: function(activityProfileModel) {
          return false;
        }
      },
      DELETE: {
        name: 'DELETE',
        label: global.Message['SENTENCE_DELETE'],
        index: 1,
        isDisabled: function(activityProfileModel) {
          return false;
        }
      }
    };

    return global;
  })();

  return mwa.enumeration;
}));
