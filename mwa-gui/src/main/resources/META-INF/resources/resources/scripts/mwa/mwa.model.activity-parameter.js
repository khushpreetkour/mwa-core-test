/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.activityParameter = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.activityParameter namespace.
   * @namespace
   */
  mwa.model.activityParameter = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityParameter alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    var ACTIVITY_PARAMETER_TYPE_ARRAY =
        mwa.proui._.values(mwa.enumeration.ActivityParameterType);

    /**
     * The mwa activity parameter model.
     * @constructor
     * @inner
     * @alias mwa.model.activityParameter.ActivityParameterModel
     * @memberof mwa.model.activityParameter
     * @extends {mwa.Model}
     */
    global.ActivityParameterModel = (function() {
      return mwa.Model.extend(
          /**
           * @lends mwa.model.activityParameter.ActivityParameterModel
           */
          {
            alias: 'mwa.model.activityParameter.ActivityParameterModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activity-parameters' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            // CAUTION:
            // It is important to use "key" as ID attribute in consideration of initial rendering of
            // workflow because input and output activity parameter collection will be initialized
            // in workflow step view model from server response that doesn't have ID. And the ID
            // will be specified by workflow canvas using the ID of workflow connection port when
            // rendering such workflow step.
            idAttribute: 'key',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: mwa.proui.util.Uuid.generateUuidV4(), // Client Data
                    visibleFlag: true,
                    listFlag: false,
                    required: true,
                    typeName: null,
                    type: null,                               // Client Data
                    parentKey: null,
                    parentValue: null,
                    key: null,
                    value: null,
                    result: null                              // Client Data
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  visibleFlag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  listFlag: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  required: [
                    {
                      required: true
                    },
                    {
                      type: 'boolean'
                    }
                  ],
                  // INFORMATION:
                  // Actually, there is no order in object regarding property name but it is better
                  // to define the validator for typeName attribute before type attribute in order
                  // to throw an error that has understandable message when validation error occurs.
                  typeName: [
                    {
                      required: true
                    },
                    {
                      type: 'string'
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        // INFORMATION:
                        // The "type" was set based on the value of "typeName". Therefore, the
                        // "type" isn't specified if "typeName" is incorrect.
                        var type = this.get('type');
                        if (typeof type === 'undefined' || type == null) {
                          msg =
                              'The value of "' + attr + '" is not included in [' +
                              mwa.proui._.keys(mwa.enumeration.ActivityParameterType).join(', ') +
                              '].';
                        }
                        return msg;
                      }
                    }
                  ],
                  type: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = 'The "' + attr + '" must be mwa.enumeration.ActivityParameterType.';
                        for (var propertyName in mwa.enumeration.ActivityParameterType) {
                          if (mwa.enumeration.ActivityParameterType.hasOwnProperty(propertyName)) {
                            // INFORMATION:
                            // Don't check whether the instance is matching with enumeration object
                            // because it is difficult for development phase to fit it.
                            if (mwa.proui._.isEqual(value, mwa.enumeration.ActivityParameterType[propertyName])) {
                              msg = null;
                              break;
                            }
                          }
                        }
                        return msg;
                      }
                    }
                  ],
                  parentKey: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  parentValue: [
                    {
                      required: false
                    }
                    // CAUTION:
                    // The type of this value isn't able to specified because there is any case.
                  ],
                  key: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  value: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  result: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.parse}
             * @public
             * @override
             */
            parse: function(response, options) {
              response = mwa.Model.prototype.parse.apply(this, [response, options]);

              response.type =
                  mwa.proui._.findWhere(
                      ACTIVITY_PARAMETER_TYPE_ARRAY,
                      {typeName: response.typeName}
                  ) ||
                  mwa.enumeration.ActivityParameterType.SETTING;

              return response;
            },
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The function to convert to object.
             * @public
             *
             * @param {?boolean} [hasValue = true]
             * The flag whether the converted object should have value property.
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function(hasValue) {
              var json = this.toJSON();
              return mwa.proui._.omit(
                  json,
                  'id',
                  'type',
                  'result',
                  // CAUTION:
                  // It is important to use strict inequality in order to treat with undefined value
                  // with true.
                  (hasValue !== false) ? null : 'value'
              );
            }
          }
      );
    })();

    /**
     * The mwa activity parameter collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityParameter.ActivityParameterCollection
     * @memberof mwa.model.activityParameter
     * @extends {mwa.Collection}
     */
    global.ActivityParameterCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activityParameter.ActivityParameterCollection
           */
          {
            url: global.ActivityParameterModel.prototype.urlRoot,
            model: global.ActivityParameterModel,
            /**
             * The function to convert to object.
             * @public
             *
             * @param {?boolean} [hasValue = true]
             * The flag whether the converted object should have value property.
             *
             * @returns {object}
             * The converted object.
             */
            toObject: function(hasValue) {
              return this.map(
                  function(activityParameterModel, index, array) {
                    return activityParameterModel.toObject(hasValue);
                  }
              );
            }
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityParameter;
}));
