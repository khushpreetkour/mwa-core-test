/**
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */
/*!
 * ${project.build.finalName}
 * ${copyright}
 * ${project.version}.${svn.revision}
 */

;

if (typeof mwa === 'undefined' || mwa == null) {
  var mwa = {};
}

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([
      (Object.keys(mwa).length === 0) ? './mwa' : {},
      (mwa.model == null) ? './mwa.model' : {}
    ], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(
        (Object.keys(mwa).length === 0) ? require('./mwa') : {},
        (mwa.model == null) ? require('./mwa.model') : {}
    );
  } else {
    root.mwa.model.activityInstance = factory(
        root.mwa,
        root.mwa.model
    );
  }
}(this, function(base, subBase) {
  // To initialize the mwa namespace.
  if (Object.keys(mwa).length === 0) {
    mwa = base;
  }
  // To initialize the mwa.model namespace.
  if (mwa.model == null) {
    mwa.model = subBase;
  }

  /**
   * The mwa.model.activityInstance namespace.
   * @namespace
   */
  mwa.model.activityInstance = (function() {
    'use strict';

    /**
     * Defines mwa.model.activityInstance alias name.
     * @constructor
     */
    var global = function() {
      return global;
    };

    /**
     * The mwa activity instance model.
     * @constructor
     * @inner
     * @alias mwa.model.activityInstance.ActivityInstanceModel
     * @memberof mwa.model.activityInstance
     * @extends {mwa.Model}
     */
    global.ActivityInstanceModel = (function() {
      return mwa.Model.extend(
          /**
           * @lends mwa.model.activityInstance.ActivityInstanceModel
           */
          {
            alias: 'mwa.model.activityInstance.ActivityInstanceModel',
            urlRoot: function(resource) {
              resource = (typeof resource === 'undefined' || resource == null) ? 'activity-instances' : resource;
              return mwa.Model.prototype.urlRoot.apply(this, [resource]);
            },
            idAttribute: 'id',
            defaults: function() {
              return mwa.proui.util.Object.extend(true,
                  {},
                  mwa.proui._.result(mwa.Model.prototype, 'defaults'),
                  {
                    id: null,
                    parentId: null,
                    name: null,
                    templateId: null,
                    templateName: null,
                    templateVersion: null,
                    status: {
                      name: null,
                      stable: true,
                      cancel: false,
                      terminated: false,
                      error: false
                    },
                    statusDetails: null,
                    priority: 50,
                    progress: -1,
                    inputDetailsList: null,
                    outputDetailsList: null,
                    properties: null,
                    createTime: -1,
                    startTime: null,
                    updateTime: -1,
                    endTime: null
                  }
              );
            },
            validation: mwa.proui.util.Object.extend(true,
                {},
                mwa.Model.prototype.validation,
                {
                  id: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  parentId: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  name: [
                    {
                      required: false
                    },
                    {
                      type: 'string'
                    }
                  ],
                  templateId: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  templateName: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  templateVersion: [
                    {
                      specified: true
                    },
                    {
                      type: 'string'
                    }
                  ],
                  status: [
                    {
                      required: true
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isPlainObject(value))) {
                          msg = 'The "' + attr + '" must be object.';
                        }
                        return msg;
                      }
                    }
                  ],
                  statusDetails: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isPlainObject(value))) {
                          msg = 'The "' + attr + '" must be object.';
                        }
                        return msg;
                      }
                    }
                  ],
                  priority: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    },
                    {
                      min: 0
                    },
                    {
                      max: 100
                    }
                  ],
                  progress: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    },
                    {
                      min: 0
                    },
                    {
                      max: 100
                    }
                  ],
                  inputDetailsList: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ],
                  outputDetailsList: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isArray(value))) {
                          msg = 'The "' + attr + '" must be an array.';
                        }
                        return msg;
                      }
                    }
                  ],
                  properties: [
                    {
                      required: false
                    },
                    {
                      fn: function(value, attr, computedState) {
                        var msg = null;
                        if (!(mwa.proui.util.Object.isPlainObject(value))) {
                          msg = 'The "' + attr + '" must be object.';
                        }
                        return msg;
                      }
                    }
                  ],
                  createTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  startTime: [
                    {
                      required: false
                    },
                    {
                      type: 'number'
                    }
                  ],
                  updateTime: [
                    {
                      required: true
                    },
                    {
                      type: 'number'
                    }
                  ],
                  endTime: [
                    {
                      required: false
                    },
                    {
                      type: 'number'
                    }
                  ]
                }
            ),
            /**
             * @see {@link mwa.Model.listen}
             * @protected
             * @override
             */
            listen: function() {
              mwa.Model.prototype.listen.apply(this, []);

              // If you need new listeners, add here.
            },
            /**
             * The function to operate the model for controlling actual processing.
             * @public
             *
             * @param {string} operation
             * The operation name.
             * @param {string} [childName]
             * The child activity name (workflow step name).
             * @param {mwa.model.keyValue.KeyValueCollection} [parameters]
             * The input key value parameters.
             * @param {object} [options]
             * The options of the Ajax request.
             *
             * @returns {object}
             * The deferred object of the request.
             */
            operate: function(operation, childName, parameters, options) {
              var message = '';

              if (typeof operation !== 'string') {
                message = this.alias + '.operate : ' +
                    'The "operation" is required and must be string.';

                mwa.Logger.error(message);

                throw Error(message);
              }
              if (typeof parameters !== 'undefined' && parameters != null) {
                if (!(parameters instanceof mwa.model.keyValue.KeyValueCollection)) {
                  message = this.alias + '.operate : ' +
                      'The "parameters" must be an instance of mwa.model.keyValue.KeyValueCollection.';

                  mwa.Logger.error(message);

                  throw Error(message);
                }
              }

              options = mwa.proui._.extend(
                  {},
                  options,
                  {
                    type: 'POST',
                    url: this.urlRoot(),
                    data: JSON.stringify(parameters),
                    dataType: 'json',
                    contentType: 'application/json;charset=utf-8'
                  }
                  );
              this.wrapCallback(options);

              if (this.id != null) {
                // CAUTION:
                // The "endsWith" function must not be used because it isn't supported by all modern
                // browsers (e.g. Internet Explorer 11).
                //  - endsWith in JavaScript
                //    (http://stackoverflow.com/questions/280634/endswith-in-javascript)
                options.url += ((/\/$/.test(options.url)) ? '' : '/') + this.id;
                if (typeof childName === 'string') {
                  options.url += '/' + childName;
                }
                options.url += '?' + 'operation=' + operation;
              } else {
                message = this.alias + '.operate : ' +
                    'The "id" is required to operate an instance.';

                mwa.Logger.error(message);

                throw Error(message);
              }

              var xhr = mwa.proui.Backbone.$.ajax(options);
              this.trigger('request', this, xhr, options);

              return xhr;
            }
          }
      );
    })();

    /**
     * The mwa activity instance collection.
     * @constructor
     * @inner
     * @alias mwa.model.activityInstance.ActivityInstanceCollection
     * @memberof mwa.model.activityInstance
     * @extends {mwa.Collection}
     */
    global.ActivityInstanceCollection = (function() {
      return mwa.Collection.extend(
          /**
           * @lends mwa.model.activityInstance.ActivityInstanceCollection
           */
          {
            url: global.ActivityInstanceModel.prototype.urlRoot,
            model: global.ActivityInstanceModel
          }
      );
    })();

    return global;
  })();

  return mwa.model.activityInstance;
}));
