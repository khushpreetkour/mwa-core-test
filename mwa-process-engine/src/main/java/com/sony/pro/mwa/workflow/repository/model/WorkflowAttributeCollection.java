package com.sony.pro.mwa.workflow.repository.model;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;
import com.sony.pro.mwa.workflow.jbpm.WorkflowAttribute;

import java.util.List;

/**
 * Collection of WorkflowAttributeModel.
 */
public class WorkflowAttributeCollection extends Collection<WorkflowAttribute> {

    public WorkflowAttributeCollection() {
        super();
    }

    public WorkflowAttributeCollection(List<WorkflowAttribute> models) {
        super(models);
    }

    public WorkflowAttributeCollection(List<WorkflowAttribute> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public WorkflowAttributeCollection(Long totalCount, Long count, List<WorkflowAttribute> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<WorkflowAttribute> getModels() {
        return super.getModels();
    }
}