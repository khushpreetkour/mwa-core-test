package com.sony.pro.mwa.workflow.jbpm;

import java.io.Serializable;
import java.util.ArrayList;

public class WorkItemNodeAttribute implements Serializable {
    protected final static String SIGNAL_CANCEL = "SIG_CANCEL";
    protected final static String SIGNAL_ERROR = "SIG_ERROR";

    String name;
    ArrayList<String> boundarySignalList;

    public WorkItemNodeAttribute() {
        boundarySignalList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }
    public WorkItemNodeAttribute setName(String name) {
        this.name = name;
        return this;
    }

    public boolean hasErrorSignal() {
        return this.boundarySignalList.contains(SIGNAL_ERROR);
    }
    public boolean hasCancelSignal() {
        return this.boundarySignalList.contains(SIGNAL_CANCEL);
    }

    public WorkItemNodeAttribute setSignal(String signal) {
        if (signal != null && !this.boundarySignalList.contains(signal)) {
            this.boundarySignalList.add(signal);
        }
        return this;
    }

}
