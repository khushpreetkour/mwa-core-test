package com.sony.pro.mwa.workflow.jbpm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.WorkItemHandler;
import org.jbpm.process.audit.AbstractAuditLogger;
import org.jbpm.runtime.manager.impl.DefaultRegisterableItemsFactory;

public class RegisterableItemsFactoryWithoutAudit extends DefaultRegisterableItemsFactory {
	
	List<ProcessEventListener> listenersForMwa = null;
	Map<String, WorkItemHandler> additionalHandler = new HashMap<>();
	
    @Override
    public List<ProcessEventListener> getProcessEventListeners(RuntimeEngine runtime) {
    	
        List<ProcessEventListener> defaultListeners = super.getProcessEventListeners(runtime);
        Object target = null;

        //AuditLog出力抑制のため、Factory処理をフックして、AuditLoggerを削除。AuditLogが不要、また、DB登録されると削除が面倒なため、登録しないという方針を採る。
        for (ProcessEventListener listener : defaultListeners) {
        	if (listener instanceof AbstractAuditLogger) {
        		target = listener;
        		break;
        	}
        }
        
        if (target != null)
        	defaultListeners.remove(target);
        
        if (listenersForMwa != null) {
        	defaultListeners.addAll(listenersForMwa);
        }
        
        return defaultListeners;
    }
    
    public RegisterableItemsFactoryWithoutAudit setDefaultListenersForMwa(List<ProcessEventListener> listeners) {
    	listenersForMwa = listeners;
		return this;
	}
    
    @Override
    public Map<String, WorkItemHandler> getWorkItemHandlers(RuntimeEngine runtime) {
    	Map<String, WorkItemHandler> result = super.getWorkItemHandlers(runtime);
    	result.putAll(additionalHandler);
    	return result;
    }
    
    public Map<String, WorkItemHandler> getAdditionalWorkItemHandler() {
    	return this.additionalHandler;
    }
}
