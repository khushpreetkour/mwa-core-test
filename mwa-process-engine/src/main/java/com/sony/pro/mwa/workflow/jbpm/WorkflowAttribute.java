package com.sony.pro.mwa.workflow.jbpm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.enumeration.PresetParameter;

/**
 * @author xi.ou
 *
 */
public class WorkflowAttribute {
    private Long id;
    private String name = null;
    private String instanceId = null;
    private Integer totalItems = null;
    private HashSet<String> completedItemList = null;
    private Map<String, Object> variables = null;

    public WorkflowAttribute() {
    }

    public WorkflowAttribute(WorkflowAttribute attribute) {
        this.id = attribute.id;
        this.name = attribute.name;
        this.instanceId = attribute.instanceId;
        this.totalItems = attribute.totalItems;
        this.completedItemList = new HashSet<String>(attribute.completedItemList);
        this.variables = new HashMap<String, Object>(attribute.variables);
    }

    public WorkflowAttribute(Long id, String name, String instanceId, Integer totalItems, HashSet<String> completedItemList, Map<String, Object> variables) {
        this.id = id;
        this.name = name;
        this.instanceId = instanceId;
        this.totalItems = totalItems;
        this.completedItemList = completedItemList;
        this.variables = variables;
    }

    // id
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    // Name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    // instanceId
    public String getInstanceId() {
        return instanceId;
    }
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    // totalItems
    public Integer getTotalItems() {
        return totalItems;
    }
    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    // completedItemList
    public HashSet<String> getCompletedItemList() {
        return completedItemList;
    }
    public void setCompletedItemList(List<String> completedItemList) {
        this.completedItemList = new HashSet<String>(completedItemList);
    }
    public void setCompletedItemList(HashSet<String> completedItemList) {
        this.completedItemList =completedItemList;
    }
    public Integer getCompletedItems() {
        return completedItemList.size();
    }

    // variables
    public Map<String, Object> getVariables() {
        return variables;
    }
    public void setVariables(Map<String, Object> variables) {
        this.variables = new HashMap<String, Object>(variables);
        this.instanceId = (String)this.variables.get(PresetParameter.ParentInstanceId.getKey());
    }
}
