package com.sony.pro.mwa.workflow.jbpm;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.workflow.repository.WorkflowAttributeDao;
import org.drools.core.command.impl.GenericCommand;
import org.drools.core.command.impl.KnowledgeCommandContext;
import org.jbpm.workflow.instance.node.WorkItemNodeInstance;
import org.kie.api.definition.process.Node;
import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEnvironment;
import org.kie.api.runtime.manager.RuntimeEnvironmentBuilder;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.manager.RuntimeManagerFactory;
import org.kie.api.runtime.process.NodeInstance;
import org.kie.api.runtime.process.NodeInstanceContainer;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkflowProcessInstance;
import org.kie.internal.command.Context;
import org.kie.internal.executor.api.ExecutorService;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.runtime.manager.SessionNotFoundException;
import org.kie.internal.runtime.manager.context.EmptyContext;
import org.kie.internal.runtime.manager.context.ProcessInstanceIdContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.NamedLocks;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.workflow.IProcessEngine;
import com.sony.pro.mwa.workflow.decisionrule.RuleSessionHelper;
import com.sony.pro.mwa.workflow.jbpm.RegisterableItemsFactoryWithoutAudit;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * This is a sample file to launch a process.
 */
public class ProcessEngineImpl implements IProcessEngine {
    private ExecutorService executorService = null;

    private EntityManagerFactory emf;
    private RuntimeManager manager;
    private static NamedLocks locks = new NamedLocks();
    private RegisterableItemsFactoryWithoutAudit itemFactory;

    private WorkflowAttributeDao workflowAttributeDao;

    protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

    protected List<ProcessEventListener> listeners;

    private Object ruleExecuteLock = new Object();

    //Singleton Impl --
    private static final class ProcessEngineHolder {
        private static final ProcessEngineImpl instance = new ProcessEngineImpl();
    }
    private Object readResolve() {
        return ProcessEngineHolder.instance;
    }

    @Autowired
    @PersistenceUnit
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }

    @Autowired
    @Qualifier("workflowAttributeDao")
    public void setWorkflowAttributeDao(WorkflowAttributeDao dao) {
        this.workflowAttributeDao = dao;
    }
    public WorkflowAttributeDao getWorkflowAttributeDao() {
        return this.workflowAttributeDao;
    }

    private ProcessEngineImpl() {
        listeners = new ArrayList<>();
        //workflowAttributeList = new HashMap<Long, WorkflowAttribute>();
    }

    @Override
    protected void finalize() throws Throwable {
      try {
          logger.info("ProcessEngine finalize ...");
          this.workflowAttributeDao.cleanup();
          super.finalize();
      } finally {
          destroy();
      }
    }

    public ExecutorService getExecutorService() {return executorService;}
    //public KieBase getKieBase() {return kbase;}
    public KieSession getKieSession(Long processInstanceId) {
    	if (processInstanceId != null) 
    		return manager.getRuntimeEngine(ProcessInstanceIdContext.get(processInstanceId)).getKieSession();
    	else
    		return manager.getRuntimeEngine(ProcessInstanceIdContext.get()).getKieSession();
    }

    public void addListener(ProcessEventListener listener) {
        //this.getKieSession().addEventListener(listener);
        listeners.add(listener);
    }

    @Override
    public void sendCancelSignal(Long instanceId) {
        logger.debug("[" + Thread.currentThread().getId() + "] sendCancelSignal(" + instanceId + ") : lock");
        synchronized (locks.get(instanceId)) {
            if (this.getKieSession(instanceId).getProcessInstance(instanceId) != null) {
                logger.info("send " + WorkItemNodeAttribute.SIGNAL_CANCEL + ": id=" + instanceId);
                this.getKieSession(instanceId).signalEvent(WorkItemNodeAttribute.SIGNAL_CANCEL, "test object", instanceId);
            }
        }
        logger.debug("[" + Thread.currentThread().getId() + "] sendCancelSignal(" + instanceId + ") : unlock");
    }
    public void sendErrorSignal(String instanceId) {
    	sendErrorSignal(Long.parseLong(instanceId));
    }
    public void sendErrorSignal(Long instanceId) {
        logger.debug("[" + Thread.currentThread().getId() + "] sendErrorSignal(" + instanceId + ") : lock");
        synchronized (locks.get(instanceId)) {
            if (this.getKieSession(instanceId).getProcessInstance(instanceId) != null) {
                logger.info("send " + WorkItemNodeAttribute.SIGNAL_ERROR + ": id=" + instanceId);
                this.getKieSession(instanceId).signalEvent(WorkItemNodeAttribute.SIGNAL_ERROR, "test object", instanceId);
            }
        }
        logger.debug("[" + Thread.currentThread().getId() + "] sendErrorSignal(" + instanceId + ") : unlock");
    }

    @Override
    public void reloadBpmns(List<String> bpmnPaths) {
        manager.close();

        //KieSessionがらみの処理
        RuntimeEnvironmentBuilder envBuilder = RuntimeEnvironmentBuilder.Factory.get()
                .newDefaultBuilder()
                .persistence(true)
                .entityManagerFactory(emf)
                .registerableItemsFactory(this.itemFactory)
        ;

        if (bpmnPaths != null) {
            for (String bpmnPath : bpmnPaths) {
                try {
                    envBuilder = envBuilder.addAsset(ResourceFactory.newFileResource(bpmnPath), ResourceType.BPMN2);
                } catch (Exception e) {
                    logger.warn("Can't reload bpmn: " + bpmnPath, e);
                }
            }
        }

        RuntimeEnvironment environment = envBuilder.get();
        manager = RuntimeManagerFactory.Factory.get().newPerProcessInstanceRuntimeManager(environment);
        //manager = RuntimeManagerFactory.Factory.get().newSingletonRuntimeManager(environment);

        logger.info("Reloaded BPMN files ...");
    }

    @Override
    public void reloadRules(List<String> rulePaths) {
        RuleSessionHelper.SINGLETON.reloadRules(rulePaths);

        logger.info("Reloaded RULE files ...");
    }

    public void initialize() throws Exception {
        this.itemFactory = new RegisterableItemsFactoryWithoutAudit().setDefaultListenersForMwa(listeners);

        //KieSessionがらみの処理
        RuntimeEnvironment environment = RuntimeEnvironmentBuilder.Factory.get()
                .newDefaultBuilder()
                .persistence(true)
                .entityManagerFactory(emf)
                .registerableItemsFactory(this.itemFactory)
                .get();

        manager = RuntimeManagerFactory.Factory.get().newPerProcessInstanceRuntimeManager(environment);
        //manager = RuntimeManagerFactory.Factory.get().newSingletonRuntimeManager(environment);

        // init rule session
        RuleSessionHelper.SINGLETON.initialize();

        logger.info("ProcessEngine initialized ...");
    }

    public void destroy() {
        if (executorService != null) {
            executorService.destroy();
        }
        if (emf != null) {
            try {
                emf.close();
            } catch (org.hibernate.HibernateException e) {

            }
        }
        if (manager != null) {
            manager.close();
        }
        RuleSessionHelper.SINGLETON.shutdown();
    }

    @Override
    public Long startProcess(String processName, Map<String, Object> parameters) {
        Long instanceId = null;
        try {
            KieSession ks = this.getKieSession(null);
            ProcessInstance processInstance = ks.startProcess(processName, parameters);
            instanceId = processInstance.getId();
            logger.debug("Process(name=" + processName + ", id=" + instanceId + ") started ...");
        } catch (java.lang.IllegalArgumentException e) {
            logger.error("Unknown process name (BPMN not registered?): processName=" + processName + ", e=" + e.getMessage(), e);
            throw new MwaInstanceError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("startProcess failed", e);
        }
        return instanceId;
    }

    @Override
    public OperationResult startRule(String rulePackage, Map<String, Object> parameters) {
        OperationResult result;
        synchronized(ruleExecuteLock) {
            result = RuleSessionHelper.SINGLETON.startRule(rulePackage, parameters);
        }
        return result;
    }
    @Override
    public ResourceRequestModel startResourceRule(String rulePackage, Map<String, Object> parameters) {
        ResourceRequestModel result;
        synchronized(ruleExecuteLock) {
            result = RuleSessionHelper.SINGLETON.startResourceRule(rulePackage, parameters);
        }
        return result;
    }

    @Override
    public Integer getState(Long instanceId) {
    	KieSession session = this.getKieSession(instanceId);
    	if (session != null && instanceId != null) {
            ProcessInstance processInstance = session.getProcessInstance(instanceId);
            if (processInstance != null)
                return processInstance.getState();
    	}
        return null;
    }

    //インスタンス削除後のSignal発行はExceptionとなるため、synchronizedで排他を取る
    @Override
    public void deleteProcessInstance(Long instanceId) {
        logger.debug("[" + Thread.currentThread().getId() + "] deleteProcessInstance(" + instanceId + ") : lock");
        synchronized (locks.get(instanceId)) {
        	try {
	            ProcessInstance processInstance = this.getKieSession(instanceId).getProcessInstance(instanceId);
	            if (processInstance != null) {
	                //★jBPM上のProcessInstance削除処理。削除用の公開メソッドがないため、正式手順ではないか・・・？
	                org.drools.core.command.impl.CommandBasedStatefulKnowledgeSession cbsks = (org.drools.core.command.impl.CommandBasedStatefulKnowledgeSession)this.getKieSession(instanceId);
	                org.drools.persistence.SingleSessionCommandService sscs = (org.drools.persistence.SingleSessionCommandService)cbsks.getCommandService();
	                org.drools.core.impl.StatefulKnowledgeSessionImpl session = (org.drools.core.impl.StatefulKnowledgeSessionImpl)sscs.getKieSession();
	                org.jbpm.process.instance.ProcessRuntimeImpl internalRuntime = (org.jbpm.process.instance.ProcessRuntimeImpl)session.getProcessRuntime();
	                internalRuntime.removeProcessInstance(this.getKieSession(instanceId).getProcessInstance(instanceId));
	            }
        	} catch (org.kie.internal.runtime.manager.SessionNotFoundException|org.drools.persistence.SessionNotFoundException e) {
        		//do nothing
        	} catch (IllegalStateException e) {
        		if ( e.getCause() instanceof InvocationTargetException ) {
        			if (e.getCause().getCause() instanceof org.drools.persistence.SessionNotFoundException ||
        					e.getCause().getCause() instanceof org.kie.internal.runtime.manager.SessionNotFoundException ) {
                		//do nothing
        			} else
            			throw e;
        		} else
        			throw e;
        	}
        }
        logger.debug("[" + Thread.currentThread().getId() + "] deleteProcessInstance(" + instanceId + ") : unlock");
    }

    //NodeInstanceを経由してWorkItemの名前を取得できるが、コミットタイミングとの兼ね合いでWorkItemとしてNullが返ることがある -> 再現しないので利用再開
    public WorkItemNodeAttribute getWorkItemNodeAttribute(Long instanceId, Long workItemId) {
        ProcessInstance processInstance = this.getKieSession(instanceId).getProcessInstance(instanceId);
        if (!(processInstance instanceof WorkflowProcessInstance)) {
                return null;
        }
        WorkItemNodeAttribute result = null;
        WorkflowAttribute workflowAttribute = null;
        NodeInstance nodeInstance = findWorkItemNodeInstance(workItemId, ((WorkflowProcessInstance) processInstance).getNodeInstances());

        if (nodeInstance != null) { //Nodeに関連づいたBoundaryEventを取得してます。
            result = new WorkItemNodeAttribute().setName(nodeInstance.getNodeName());
            workflowAttribute = new WorkflowAttribute();
            String nodeId = null;

            if (nodeInstance != null && nodeInstance.getNode() != null && nodeInstance.getNode().getMetaData() != null) {
                nodeId = (String) nodeInstance.getNode().getMetaData().get("UniqueId");
            }

            if (nodeId != null) {
                Node[] nodes = ((org.jbpm.ruleflow.instance.RuleFlowProcessInstance)processInstance).getNodeContainer().getNodes();
                workflowAttribute.setId(instanceId);
                workflowAttribute.setName(result.getName());
                workflowAttribute.setInstanceId(nodeId);
                workflowAttribute.setCompletedItemList(((org.jbpm.ruleflow.instance.RuleFlowProcessInstance)processInstance).getCompletedNodeIds());
                workflowAttribute.setTotalItems(nodes.length);
                workflowAttribute.setVariables(((org.jbpm.ruleflow.instance.RuleFlowProcessInstance)processInstance).getVariables());

                this.workflowAttributeDao.insertOrUpdate(instanceId, workflowAttribute);

                for (Node node : nodes) {
                    if ( node instanceof org.jbpm.workflow.core.node.BoundaryEventNode) {
                        if ( nodeId.equals(((org.jbpm.workflow.core.node.BoundaryEventNode)node).getAttachedToNodeId()) ) {
                            result.setSignal((String)((org.jbpm.workflow.core.node.BoundaryEventNode)node).getMetaData("SignalName"));
                        }
                    }
                }
            }
        }
        return result;
    }

    public void removeWorkflowAttributeFromList(final Long procId) {
        WorkflowAttribute removedModel = this.workflowAttributeDao.deleteModel(procId);

        if (removedModel == null) {
            logger.warn("The attribute of process instane [" + procId + "] not store in workflow attribute list!!");
        }

    }

    public WorkflowAttribute getWorkflowAttribute(final Long procId) {
        WorkflowAttribute model = workflowAttributeDao.getModel(procId.toString(), null);
        return model;
    }

    // don't use below method because the process session return null when you execute a complecate workflow...
    public Map<String, Object> getProcessVariables(final Long instanceId) throws Exception {
        try {
            Map<String, Object> variables = this.getKieSession(instanceId).execute(new GenericCommand<Map<String, Object>>() {

                @Override
                public Map<String, Object> execute(Context context) {
                    KieSession ksession = ((KnowledgeCommandContext) context).getKieSession();
                    ProcessInstance processInstance = ksession.getProcessInstance(instanceId);
                    int totalStep;
                    if (processInstance != null) {
                        totalStep = ((org.jbpm.ruleflow.instance.RuleFlowProcessInstance)processInstance).getNodeContainer().getNodes().length;
                    }
                    else {
                        logger.info("Process instance is null! proInstanceId = " + instanceId);
                    }
                    Map<String, Object> variables = ((org.jbpm.ruleflow.instance.RuleFlowProcessInstance)processInstance).getVariables();
                    return variables;
                }
            });

            return variables;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // not use
    public List<String> getCompletedWorkItems(final Long instanceId) {
        ProcessInstance processInstance = this.getKieSession(instanceId).getProcessInstance(instanceId);
        if (!(processInstance instanceof WorkflowProcessInstance)) {
                return null;
        }

        return ((org.jbpm.ruleflow.instance.RuleFlowProcessInstance)processInstance).getCompletedNodeIds();
    }

    public WorkItemNodeInstance findWorkItemNodeInstance(Long instanceId, Long workItemId) {
        ProcessInstance processInstance = this.getKieSession(instanceId).getProcessInstance(instanceId);
        if (!(processInstance instanceof WorkflowProcessInstance)) {
                return null;
        }
        return findWorkItemNodeInstance(workItemId, ((WorkflowProcessInstance) processInstance).getNodeInstances());
    }

    private WorkItemNodeInstance findWorkItemNodeInstance(long workItemId, Collection<NodeInstance> nodeInstances) {
        for (NodeInstance nodeInstance: nodeInstances) {
            if (nodeInstance instanceof WorkItemNodeInstance) {
                WorkItemNodeInstance workItemNodeInstance = (WorkItemNodeInstance) nodeInstance;
                //非同期だと
                WorkItem wi = workItemNodeInstance.getWorkItem();
                if (wi != null && workItemId == wi.getId()) {
                    return workItemNodeInstance;
                }
            }
            if (nodeInstance instanceof NodeInstanceContainer) {
                WorkItemNodeInstance result = findWorkItemNodeInstance(workItemId, ((NodeInstanceContainer) nodeInstance).getNodeInstances());
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public void registerWorkItem(String workItemName, Object workItemHandler) {
        if (workItemHandler instanceof org.kie.api.runtime.process.WorkItemHandler) {
            this.itemFactory.getAdditionalWorkItemHandler().put(workItemName, (org.kie.api.runtime.process.WorkItemHandler)workItemHandler);
        } else {
            logger.error("WorkItem registration failure. Invalid work item: name=" + workItemName);
            throw new MwaError(MWARC.INTEGRATION_ERROR);
        }
    }

    //
    public org.kie.api.runtime.process.WorkItemHandler getWorkItem(String workItemName) {
        return this.itemFactory.getAdditionalWorkItemHandler().get(workItemName);
    }
}
