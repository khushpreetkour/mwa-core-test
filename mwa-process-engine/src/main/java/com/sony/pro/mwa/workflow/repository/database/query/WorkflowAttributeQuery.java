package com.sony.pro.mwa.workflow.repository.database.query;

import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.workflow.jbpm.WorkflowAttribute;
import com.sony.pro.mwa.workflow.repository.model.WorkflowAttributeCollection;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class WorkflowAttributeQuery extends Query<WorkflowAttribute, WorkflowAttributeCollection, QueryCriteria> {

    public enum COLUMN {
        ID("i.id", "id", Integer.class, true),
        NAME("i.name", "name", Integer.class, true),
        INSTANCE_ID("i.instance_id", "instanceId", UUID.class, true),
        TOTAL_ITEMS("i.total_items", "totalItems", String.class, true),
        COMPLETED_ITEM_LIST("i.completed_item_list", "completedItemList", String.class, false),
        VARIABLES("i.variables", "variables", String.class, false),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    // Query文の生成に関してActivityEventQueryを参考にした。
    // MYSQL, PostgreSQL, H2それぞれのQuery文を生成するロジックがあるが
    // 結果的にすべて同じ文が生成されるように見えるので、ひとまず一本化しておく
    private static final String SELECT_CLAUSE_FOR_LIST;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }
        SELECT_CLAUSE_FOR_LIST = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }
    private static final String COUNT_ALIAS = "count";
    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.INSTANCE_ID.name + ") AS " + COUNT_ALIAS + " ";
    private static final String FROM_WHERE_CLAUSE =
            "FROM " +
                "mwa.workflow_attribute AS i "+
            "WHERE " +
                "TRUE ";


    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST + FROM_WHERE_CLAUSE);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST + FROM_WHERE_CLAUSE);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST + FROM_WHERE_CLAUSE);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        for (COLUMN enumColumn : COLUMN.values()) {
            map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }


    private static final class ModelMapper implements RowMapper<WorkflowAttribute> {
        public WorkflowAttribute mapRow(ResultSet rs, int rowNum) throws SQLException {
            Long id = rs.getLong(COLUMN.ID.alias);
            String name = rs.getString(COLUMN.NAME.alias);
            String instanceId = rs.getString(COLUMN.INSTANCE_ID.alias);
            Integer totalItems = rs.getInt(COLUMN.TOTAL_ITEMS.alias);

            String comletedItemList = rs.getString(COLUMN.COMPLETED_ITEM_LIST.alias);
            HashSet<String> hashSet = null;
            if (comletedItemList != null && !comletedItemList.isEmpty()) {
                hashSet = Converter.jsonTo(comletedItemList, HashSet.class);
            }

            String variables = rs.getString(COLUMN.VARIABLES.alias);
            Map<String, Object> hashMap = null;
            if (variables != null && !variables.isEmpty()) {
                hashMap = Converter.jsonToMap(variables);
            }

            WorkflowAttribute model = new WorkflowAttribute(id, name, instanceId, totalItems, hashSet, hashMap);
            return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }

    public WorkflowAttributeQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }

    @Override
    protected RowMapper<WorkflowAttribute> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }



}
