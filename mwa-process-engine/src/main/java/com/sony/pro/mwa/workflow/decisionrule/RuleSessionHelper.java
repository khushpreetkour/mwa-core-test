package com.sony.pro.mwa.workflow.decisionrule;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.definition.KnowledgePackage;
import org.drools.io.ResourceFactory;
import org.drools.logger.KnowledgeRuntimeLogger;
import org.drools.logger.KnowledgeRuntimeLoggerFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.kbase.model.rule.MwaRule;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.model.resource.ResourceRuleModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;

/**
 * @author Xi.Ou
 *
 */
public enum RuleSessionHelper {
    SINGLETON;

    private String field;

    private KnowledgeBuilder kBuilder = null;
    private StatefulKnowledgeSession kRuleSession = null;
    private KnowledgeRuntimeLogger kRuleLogger = null;
    Collection<KnowledgePackage> knowledgePackages = null;

    protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

    private static final String XLS_TYPE_DECISION_RULE_EXT = "xls";
    private static final String DSL_TYPE_DECISION_RULE_EXT = "dsl";
    private static final String DSLR_TYPE_DECISION_RULE_EXT = "dslr";
    private static final String DRL_TYPE_DECISION_RULE_EXT = "drl";


    private RuleSessionHelper() {
    }

    protected static RuleSessionHelper getInstance() {
        return SINGLETON;
    }

    public void initialize() {
        logger.info("Initializing...");

        kBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

        logger.info("Initialized.");
    }

    public void start() {
        logger.info("Starting...");

        logger.info("Started.");
    }

    public void reloadRules(List<String> rulePaths) {
        destory();

        if (kBuilder == null) {
            kBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
            if (knowledgePackages == null) {
                knowledgePackages = kBuilder.getKnowledgePackages();
            }
        }
        try {
            if (rulePaths != null) {
                for (String rulePath : rulePaths) {
                    String fileExt = FilenameUtils.getExtension(rulePath);
                    if (XLS_TYPE_DECISION_RULE_EXT.equals(fileExt)) {
                        final DecisionTableConfiguration dtableconfiguration =
                                KnowledgeBuilderFactory.newDecisionTableConfiguration();
                        dtableconfiguration.setInputType(DecisionTableInputType.XLS);
                        kBuilder.add(
                                ResourceFactory.newFileResource(rulePath),
                                ResourceType.DTABLE,
                                dtableconfiguration);
                    }
                    else if (DSL_TYPE_DECISION_RULE_EXT.equals(fileExt)) {
                        kBuilder.add(
                                ResourceFactory.newFileResource(rulePath),
                                ResourceType.DSL);
                    }
                    else if (DSLR_TYPE_DECISION_RULE_EXT.equals(fileExt)) {
                        kBuilder.add(
                                ResourceFactory.newFileResource(rulePath),
                                ResourceType.DSLR);
                    }
                    else if (DRL_TYPE_DECISION_RULE_EXT.equals(fileExt)) {
                        kBuilder.add(
                                ResourceFactory.newFileResource(rulePath),
                                ResourceType.DRL);
                    }
                    else {
                        logger.error("Unsupport decision rule format. file: " + FilenameUtils.getName(rulePath));
                    }

                    // output parser error log
                    KnowledgeBuilderErrors kErrors = kBuilder.getErrors();
                    if (kErrors.size() > 0) {
                        for (KnowledgeBuilderError error: kErrors) {
                            logger.error(error.getMessage());
                        }
                        logger.error("Could not parse rule. file: " + FilenameUtils.getName(rulePath));
                    }
                }
                knowledgePackages = kBuilder.getKnowledgePackages();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ResourceRequestModel startResourceRule(final String rulePackage, final Map<String, Object> parameters) {
        ResourceRequestModel result = null;

        if (!checkRulePackageOfSession(rulePackage)) {
            refreshSession(rulePackage);
        }
        if (kRuleSession != null) {
            try {
                int ruleNumber = 0;
                ResourceRuleModel rule = new ResourceRuleModel();
                if (rule != null) rule.setParameters(parameters);
                kRuleSession.getKnowledgeBase().getKnowledgePackages();

                kRuleSession.insert(rule);
                ruleNumber = kRuleSession.fireAllRules();
                /*
                 * TODO: should be removed in future.
                 * because session will refresh every time for each rule package
                 *
                ruleNumber = kRuleSession.fireAllRules(new AgendaFilter() {
                    @Override
                    public boolean accept(Activation activation) {
                        if (activation.getRule().getPackageName().equals(rulePackage)) {
                            logger.debug("Execute " +
                                    activation.getRule().getKnowledgeType() + ":" +
                                    activation.getRule().getPackageName() + ":" +
                                    activation.getRule().getName());
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                });
                 */

                result = rule.getResourceRequest();

                logger.debug("Rule(name=" + rulePackage + ", id=" + ruleNumber + ") started ...");
            } catch (IllegalArgumentException e) {
                logger.error("Unknown rule name (RULE not registered?): rulePackage=" + rulePackage + ", e=" + e.getMessage(), e);
                throw new MwaInstanceError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
            } catch (Exception e) {
                logger.error("startRule failed", e);
            }
        }

        return result;
    }

    public OperationResult startRule(final String rulePackage, final Map<String, Object> parameters) {
        OperationResult result = OperationResult.newInstance();

        if (!checkRulePackageOfSession(rulePackage)) {
            refreshSession(rulePackage);
        }
        if (kRuleSession != null) {
            try {
                int ruleNumber = 0;
                MwaRule rule = new MwaRule();
                if (rule != null) rule.setKeyValues(parameters);
                kRuleSession.getKnowledgeBase().getKnowledgePackages();

                kRuleSession.insert(rule);
                ruleNumber = kRuleSession.fireAllRules();
                /*
                 * TODO: should be removed in future.
                 * because session will refresh every time for each rule package
                 *
                ruleNumber = kRuleSession.fireAllRules(new AgendaFilter() {
                    @Override
                    public boolean accept(Activation activation) {
                        if (activation.getRule().getPackageName().equals(rulePackage)) {
                            logger.debug("Execute " +
                                    activation.getRule().getKnowledgeType() + ":" +
                                    activation.getRule().getPackageName() + ":" +
                                    activation.getRule().getName());
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                });
                 */

                result.putAll(rule.getActionResult());

                logger.debug("Rule(name=" + rulePackage + ", id=" + ruleNumber + ") started ...");
            } catch (IllegalArgumentException e) {
                logger.error("Unknown rule name (RULE not registered?): rulePackage=" + rulePackage + ", e=" + e.getMessage(), e);
                throw new MwaInstanceError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
            } catch (Exception e) {
                logger.error("startRule failed", e);
            }
        }

        return result;
    }

    public void shutdown() {
        destory();
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    private boolean checkRulePackageOfSession(final String rulePackage) {
        boolean result = false;

        if (rulePackage == null || rulePackage.isEmpty()) {
            logger.error("Rule package name is null!");
            throw new MwaInstanceError(MWARC.INVALID_INPUT_TEMPLATE_NAME, "", "Rule package name is null!");
        }
        if (kRuleSession != null) {
            Iterator<KnowledgePackage> kpItr =
                    kRuleSession.getKnowledgeBase().getKnowledgePackages().iterator();
            while (kpItr.hasNext()) {
                KnowledgePackage knowledgePackage = kpItr.next();
                if (rulePackage.equals(knowledgePackage.getName())) {
                    result = true;
                }
            }
        }

        return result;
    }

    private void refreshSession(final String rulePackage) {
        if (rulePackage == null || rulePackage.isEmpty()) {
            logger.error("Rule package name is null!");
            throw new MwaInstanceError(MWARC.INVALID_INPUT_TEMPLATE_NAME, "", "Rule package name is null!");
        }
        if (kRuleSession != null) {
            kRuleSession.dispose();
        }

        final KnowledgeBase kBase = KnowledgeBaseFactory.newKnowledgeBase();
        Iterator<KnowledgePackage> kpItr = knowledgePackages.iterator();
        while (kpItr.hasNext()) {
            KnowledgePackage knowledgePackage = kpItr.next();
            if (rulePackage.equals(knowledgePackage.getName())) {
                KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
                Collection<KnowledgePackage> kPackages = kbuilder.getKnowledgePackages();
                kPackages.add(knowledgePackage);

                kBase.addKnowledgePackages(kPackages);
                kRuleSession = kBase.newStatefulKnowledgeSession();
                logger.info("Loaded rule knowledge package: " + knowledgePackage.getName());


                if (kRuleLogger == null) {
                    // Disabled working memory log of rule process session
                    kRuleLogger = KnowledgeRuntimeLoggerFactory.newConsoleLogger(kRuleSession);
                }


                break;
            }
        }

        logger.info("Refreshed rule engine session.");
    }

    private void destory() {
        if (kRuleLogger != null) {
            kRuleLogger.close();
            kRuleLogger = null;
        }
        if (kRuleSession != null) {
            kRuleSession.dispose();
            kRuleSession = null;
        }
        if (kBuilder != null) {
            kBuilder = null;
        }
        if (knowledgePackages != null) {
            knowledgePackages.clear();
            knowledgePackages = null;
        }
    }
}
