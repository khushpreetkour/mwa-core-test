package com.sony.pro.mwa.workflow.repository.database;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.workflow.jbpm.WorkflowAttribute;
import com.sony.pro.mwa.workflow.repository.WorkflowAttributeDao;
import com.sony.pro.mwa.workflow.repository.database.query.WorkflowAttributeQuery;
import com.sony.pro.mwa.workflow.repository.model.WorkflowAttributeCollection;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

@Repository
public class WorkflowAttributeDaoImpl extends DatabaseBaseDao<WorkflowAttribute, WorkflowAttributeCollection, QueryCriteria> implements WorkflowAttributeDao {

    private final static String INSERT_SQL =
            "INSERT INTO mwa.workflow_attribute (" +
                    "id, " +
                    "name, " +
                    "instance_id, " +
                    "total_items, " +
                    "completed_item_list, " +
                    "variables) " +
            "VALUES (" +
                    "?, " +
                    "?, " +
                    "?, " +
                    "?, " +
                    "?, " +
                    "?) ";

    private final static String UPDATE_SQL =
            "UPDATE mwa.workflow_attribute " +
            "SET " +
                    "instance_id = ?, " +
                    "name = ?, " +
                    "total_items = ?, " +
                    "completed_item_list = ?, " +
                    "variables = ?" +
            "WHERE " +
                    "id = ?";

    private final static String DELETE_SQL =
            "DELETE FROM mwa.workflow_attribute " +
            "WHERE id = ?";

    private final static String DELETE_ALL_SQL = "DELETE FROM mwa.workflow_attribute";

    protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

    public WorkflowAttributeCollection getModels(QueryCriteria criteria) {
        return super.collection(criteria);
    }

    public WorkflowAttribute getModel(String id, Principal principal) {
        return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
    }

    public WorkflowAttribute addModel(WorkflowAttribute model) {
        int insertRows = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(INSERT_SQL, new String[] {});
                        int column = 1;

                        ps.setLong(column++, model.getId());
                        // name
                        ps.setString(column++, model.getName());

                        // instanceid
                        ps.setObject(column++, UUID.fromString(model.getInstanceId()));

                        // totalItems
                        ps.setInt(column++, model.getTotalItems());

                        // completedItemList
                        HashSet<String> completedItemList = model.getCompletedItemList();
                        if (completedItemList != null) {
                            ps.setString(column++, Converter.objToJson(completedItemList));
                        } else {
                            ps.setNull(column++, Types.VARCHAR);
                        }

                        // variables
                        Map<String, Object> variables = model.getVariables();
                        if (variables != null) {
                            ps.setString(column++, Converter.mapToJson(variables));
                        } else {
                            ps.setNull(column++, Types.VARCHAR);
                        }

                        String delegate = ((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString();
                        logger.debug(delegate);

                        return ps;
                    }
                },
                new GeneratedKeyHolder()
        );
        return insertRows != 0 ? model : null;
    }

    public WorkflowAttribute updateModel(WorkflowAttribute model) {
        int updatedRows = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(UPDATE_SQL);
                        int column = 1;

                        // instanceId
                        ps.setObject(column++, UUID.fromString(model.getInstanceId()));

                        // name
                        ps.setString(column++, model.getName());

                        // totalItems
                        ps.setInt(column++, model.getTotalItems());

                        // completedItemList
                        HashSet<String> completedItemList = model.getCompletedItemList();
                        if (completedItemList != null) {
                            ps.setString(column++, Converter.objToJson(completedItemList));
                        } else {
                            ps.setNull(column++, Types.VARCHAR);
                        }

                        // variables
                        Map<String, Object> variables = model.getVariables();
                        if (variables != null) {
                            ps.setString(column++, Converter.mapToJson(variables));
                        } else {
                            ps.setNull(column++, Types.VARCHAR);
                        }

                        // Id
                        ps.setLong(column++, model.getId());

                        String delegate = ((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString();
                        logger.debug(delegate);

                        return ps;
                    }
                },
                new GeneratedKeyHolder()
        );
        return model;
    }

    public WorkflowAttribute insertOrUpdate(Long id, WorkflowAttribute attribute) {
        WorkflowAttribute model = this.getModel(id.toString(), null);
        if (model == null) {
            model = new WorkflowAttribute(
                    id,
                    attribute.getName(),
                    attribute.getInstanceId(),
                    attribute.getTotalItems(),
                    attribute.getCompletedItemList(),
                    attribute.getVariables());
            this.addModel(model);
        } else {
            model.setName(attribute.getName());
            model.setInstanceId(attribute.getInstanceId());
            model.setTotalItems(attribute.getTotalItems());
            model.setCompletedItemList(attribute.getCompletedItemList());
            model.setVariables(attribute.getVariables());
            this.updateModel(model);
        }
        return model;
    }

    public WorkflowAttribute deleteModel(WorkflowAttribute model) {
        String sql = DELETE_SQL;
        Object[] params = new Object[] {model.getId()};
        return this.jdbcTemplate.update(sql, params) == 0 ? null : model;
    }

    public WorkflowAttribute deleteModel(Long id) {
        WorkflowAttribute model = this.getModel(id.toString(), null);
        return model == null ? null : this.deleteModel(model);
    }

    public void cleanup() {
        this.jdbcTemplate.update(DELETE_ALL_SQL);
    }

    @Override
    protected IQuery<WorkflowAttribute, WorkflowAttributeCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
        return new WorkflowAttributeQuery(jdbcTemplate);
    }

}
