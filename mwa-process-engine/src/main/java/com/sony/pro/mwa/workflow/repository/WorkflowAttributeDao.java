package com.sony.pro.mwa.workflow.repository;

import com.sony.pro.mwa.repository.ModelBaseDao;
import com.sony.pro.mwa.workflow.jbpm.WorkflowAttribute;
import com.sony.pro.mwa.workflow.repository.model.WorkflowAttributeCollection;

public interface WorkflowAttributeDao extends ModelBaseDao<WorkflowAttribute, WorkflowAttributeCollection> {

    WorkflowAttribute deleteModel(Long id);
    WorkflowAttribute insertOrUpdate(Long id, WorkflowAttribute attribute);
    void cleanup();
}
