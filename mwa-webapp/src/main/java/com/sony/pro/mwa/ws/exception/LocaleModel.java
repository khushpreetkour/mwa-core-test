package com.sony.pro.mwa.ws.exception;

import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 14/01/30
 * Time: 10:22
 * To change this template use File | Settings | File Templates.
 */
public class LocaleModel {

    /** id */
    private String id;
    /** name */
    private String name;

    public LocaleModel() {}
    // Constructor
    public LocaleModel(LocaleModel backendModel) {
        this.setId(backendModel.getId());
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
