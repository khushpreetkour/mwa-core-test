package com.sony.pro.mwa.ws.internal;

import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;
import java.util.List;

/**
 * TODO
 */
public class StringToListConverter implements Converter<String, List<String>> {
    @Override
    public List<String> convert(String source) {
        return Arrays.asList(source);
    }
}
