package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.event.EventCollection;
import com.sony.pro.mwa.model.event.EventModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(EventController.PATH)
@Api(value = "event", 
	description = "Events for activities."
	)
@Controller(EventController.NAME+EventController.VERSION)
@RequestMapping(value=EventController.PATH)
public class EventController {
	static final String NAME = "events";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get detailed information of specified event.", response=EventModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/{event-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody EventModel getEvent(
			@ApiParam(value = "ID of event.") @PathVariable("event-id") String id
			){
		String message = "event(id=" + id + ")";
		logger.info("getEvent[operateActivity]:" + message);
		
		EventModel model = CoreModules.eventManager().getEvent(id);
		return model;
	}
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate events. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", 
			response=EventCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody EventCollection getResources(
			@ApiParam( name="sort", value = "The sort of the plugin.") @RequestParam(value = "sort", required = false) List<String> sorts,
			@ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
			@ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
			@ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		EventCollection result = CoreModules.eventManager().getEvents(sorts, filters, offset, limit);
		return result;
	}

	@POST
	@Path(value="")
	@ApiOperation(value = "Register a new event.", response=EventModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody EventModel addResource(
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) EventModel model
			){
		EventModel result = CoreModules.eventManager().addEvent(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return result;
	}

	@PUT
	@Path(value="")
	@ApiOperation(value = "Update specified event.", response=EventModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.PUT, value = "/{event-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody EventModel updateResource(
			@ApiParam(value = "The event id to update it.") @PathVariable("event-id") String id,
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) EventModel model
			){

		if (id == null || model == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		model.setId(id);
		EventModel result = CoreModules.eventManager().updateEvent(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return result;
	}
	
	@DELETE
	@Path(value="")
	@ApiOperation(value = "Delete specified event.", response=EventModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/{event-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody EventModel deleteResource(
			@ApiParam(value = "The event id to delete it.") @PathVariable("event-id") String id
			){
		
		String message = "event(id=" + id + ")";
		logger.info("[" + new Object(){}.getClass().getEnclosingMethod().getName() + "]:" + message);
		EventModel ret = CoreModules.eventManager().deleteEvent(id);
		if (ret == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return ret;
	}

	@DELETE
	@Path(value="")
	@ApiOperation(value = "Get appropriate events. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ",  response=EventCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody EventCollection deleteResources(
			@ApiParam(name="sort", value = "The sort of the events.") @RequestParam(value = "sort", required = false) List<String> sorts,
			@ApiParam(value = "The filter of the events.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
			@ApiParam(value = "The start number of the events.") @RequestParam(value = "offset", required = false) Integer offset,
			@ApiParam(value = "The maximum number of the events.") @RequestParam(value = "limit", required = false) Integer limit
			){
		logger.info("[" + new Object(){}.getClass().getEnclosingMethod().getName() + "]: called!!");
		EventCollection result = CoreModules.eventManager().deleteEvents(sorts, filters, offset, limit);
		return result;
	}
}
