package com.sony.pro.mwa.ws.internal;

import org.springframework.beans.factory.annotation.Autowired;

import com.sony.pro.mwa.internal.service.IKnowledgeBaseInternal;
import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.kbase.IKnowledgeBase;
import com.sony.pro.mwa.service.provider.IActivityProviderManager;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.service.scheduler.ISchedulerManager;
import com.sony.pro.mwa.service.storage.IStorageManager;

public class CoreModules {
	
	private static CoreModules INSTANCE = new CoreModules();
	
	private CoreModules() {}
	
	protected static CoreModules getInstance() {
		return INSTANCE;
	}
	
	IActivityManager activityManager;
	IKnowledgeBase knowledgeBase;
	IActivityProviderManager activityProviderManager;
	IStorageManager storageManager;
	ISchedulerManager schedulerManager;
	IResourceManager resourceManager;
	IEventManager eventManager;

	public void initialize() {
	}
	

	@Autowired
	public void setStorageManager(IStorageManager storageManager) {
		this.storageManager = storageManager;
	}
	public IStorageManager getStorageManager() {
		return this.storageManager;
	}
	
	@Autowired
	public void setActivityManager(IActivityManager activityManager) {
		this.activityManager = activityManager;
	}
	public IActivityManager getActivityManager() {
		return this.activityManager;
	}

	@Autowired
	public void setKnowledgeBase(IKnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}
	public IKnowledgeBase getKnowledgeBase() {
		return this.knowledgeBase;
	}
	
	@Autowired
	public void setActivityProviderManager(IActivityProviderManager activityProviderManager) {
		this.activityProviderManager = activityProviderManager;
	}
	public IActivityProviderManager getActivityProviderManager() {
		return this.activityProviderManager;
	}
	
	@Autowired
	public void setSchedulerManager(ISchedulerManager schedulerManager){
		this.schedulerManager = schedulerManager;
	}
	
	public ISchedulerManager getSchedulerManager(){
		return schedulerManager;
	}

	@Autowired
	public void setResourceManager(IResourceManager resourceManager){
		this.resourceManager = resourceManager;
	}
	
	public IResourceManager getResourceManager(){
		return resourceManager;
	}

	@Autowired
	public void setEventManager(IEventManager eventManager){
		this.eventManager = eventManager;
	}
	
	public IEventManager getEventManager(){
		return eventManager;
	}

	public static IActivityManager activityManager() {
		return INSTANCE.getActivityManager();
	}
	public static IKnowledgeBase knowledgeBase() {
		return INSTANCE.getKnowledgeBase();
	}
	public static IActivityProviderManager activityProviderManager() {
		return INSTANCE.getActivityProviderManager();
	}
	public static IStorageManager storageManager() {
		return INSTANCE.getStorageManager();
	}
	public static ISchedulerManager schedulerManager() {
		return INSTANCE.getSchedulerManager();
	}
	public static IResourceManager resourceManager() {
		return INSTANCE.getResourceManager();
	}
	public static IEventManager eventManager() {
		return INSTANCE.getEventManager();
	}
}
