package com.sony.pro.mwa.ws.rest.v2;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.CronTriggerInputModel;
import com.sony.pro.mwa.model.scheduler.SelectTriggerInfoInputModel;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInputModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.scheduler.enm.Operation;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(SchedulerController.PATH)
@Api(value = "scheduler", 
	description = "TODO (Scheduler Controller)"
	)
@Controller(SchedulerController.NAME+SchedulerController.VERSION)
@RequestMapping(value=SchedulerController.PATH)
public class SchedulerController {
	
	static final String NAME = "scheduler";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;
	
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	public SchedulerController(){
	}
	
	@POST
	@Path(value="")
	@ApiOperation(value = "Operation scheduler. ", response=KeyValueModel.class,responseContainer="List")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel doSchedulerStart(
			@ApiParam(value = "Operation about specified scheduler.Valid operation -> [ START , SHUTDOWN ]") @RequestParam(value = "operation", required = true) String operation){
		
		KeyValueModel result = null;
		
		if(Operation.START.name().equals(operation)){
			result = CoreModules.schedulerManager().startScheduler();
		}else if(Operation.SHUTDOWN.name().equals(operation)) {
			result = CoreModules.schedulerManager().shutdownScheduler();
		}else{
			logger.error("Not accepted Operation[" +operation + "]" );
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}

	@DELETE
	@Path(value="/simple/{sched-id}")
	@ApiOperation(value = "Delete SimpleTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/simple/{sched-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel deleteSimpleTriggerJob(
			@ApiParam(value = "SCHED_ID") @PathVariable("sched-id") String schedId) {
		
		String message = "sched_id=" + schedId;
		logger.info("Delete [TriggerJob]: " + message);
		
		KeyValueModel result = CoreModules.schedulerManager().deleteSimpleTrigger(schedId);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;

	}
	
	@DELETE
	@Path(value="/simple")
	@ApiOperation(value = "Delete all SimpleTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/simple",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel deleteAllSimpleTriggerJob(){
		
		logger.info("Delete all SimpleTrigger.");
		
		KeyValueModel result = CoreModules.schedulerManager().deleteSimpleTrigger();
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}
	
	@DELETE
	@Path(value="/cron/{sched-id}")
	@ApiOperation(value = "Delete CronTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/cron/{sched-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel deleteCronTriggerJob(
			@ApiParam(value = "SCHED_ID") @PathVariable("sched-id") String schedId) {
		
		String message = "sched_id=" + schedId;
		logger.info("Delete  [TriggerJob]: " + message);
		
		KeyValueModel result = CoreModules.schedulerManager().deleteCronTrigger(schedId);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}
	
	@DELETE
	@Path(value="/cron")
	@ApiOperation(value = "Delete all CronTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/cron",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel deleteAllCronTriggerJob(){
		
		logger.info("Delete all CronTrigger.");
		
		KeyValueModel result = CoreModules.schedulerManager().deleteCronTrigger();
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}
	
	@POST
	@Path(value="/simple/{sched-id}")
	@ApiOperation(value = "Operation SimpleTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "/simple/{sched-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel operationSimpleTriggerJob(
			@ApiParam(value = "SCHED_ID") @PathVariable("sched-id") String schedId,
			@ApiParam(value = "Operation about specified simple-trigger.Valid operation -> [ PAUSE , RESUME ]") @RequestParam(value = "operation", required = true) String operation){
		
		String message = "sched_id=" + schedId;
		KeyValueModel result = null;
		if(Operation.PAUSE.name().equals(operation)){
			logger.info("Pause  [TriggerJob]: " + message);
			result = CoreModules.schedulerManager().pauseSimpleTrigger(schedId);
		}else if(Operation.RESUME.name().equals(operation)){
			logger.info("Resume  [TriggerJob]: " + message);
			result = CoreModules.schedulerManager().resumeSimpleTrigger(schedId);
		}else{
			logger.error("Not accepted Operation[" +operation + "]" );
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}
	
	@POST
	@Path(value="/cron/{sched-id}")
	@ApiOperation(value = "Operation CronTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "/cron/{sched-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel operationCronTriggerJob(
			@ApiParam(value = "SCHED_ID") @PathVariable("sched-id") String schedId,
			@ApiParam(value = "Operation about specified cron-trigger.Valid operation -> [ PAUSE , RESUME ]") @RequestParam(value = "operation", required = true) String operation){
		
		String message = "sched_id=" + schedId;
	
		KeyValueModel result = null;
		if(Operation.PAUSE.name().equals(operation)){
			logger.info("Pause [TriggerJob]: " + message);
			result = CoreModules.schedulerManager().pauseCronTrigger(schedId);
		}else if(Operation.RESUME.name().equals(operation)){
			logger.info("Resume [TriggerJob]: " + message);
			result = CoreModules.schedulerManager().resumeCronTrigger(schedId);
		}else{
			logger.error("Not accepted Operation[" +operation + "]" );
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}
	
	@POST
	@Path(value="/simple")
	@ApiOperation(value = "Insert SimpleTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "/simple",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel addSimpleTriggerJob(
			@ApiParam(value = "The ") @RequestBody(required = true) SimpleTriggerInputModel model) {
		
		String message = "Http-Method-Type=" + model.getHttpMethodType() + ", Endpoint=" + model.getEndpoint();
		logger.info("SimpleTriggerJob [operateInstance]: " + message);
		
		KeyValueModel result = CoreModules.schedulerManager().registSimpleScheduleJob(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}
	
	
	@POST
	@Path(value="/cron")
	@ApiOperation(value = "Insert CronTriggerJob. ")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "/cron",headers = "Accept=application/json,application/xml")
	public @ResponseBody KeyValueModel addCronTriggerJob(
			@ApiParam(value = "The ") @RequestBody(required = true) CronTriggerInputModel model) {
		
		String message = "Http-Method-Type=" + model.getHttpMethodType() + ", Endpoint=" + model.getEndpoint() + "Cron-Expression=" + model.getCronExpression();
		logger.info("CronTriggerJob [operateInstance]: " + message);
		
		KeyValueModel result = CoreModules.schedulerManager().registCronScheduleJob(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		
		return result;
	}
	
	
	@GET
	@Path(value="/cron/{sched-id}")
	@ApiOperation(value = "Search CronTrigger Info. ",response = CronTriggerInfoCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/cron/{sched-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody CronTriggerInfoCollection selectCronTriggerInfo(
			@ApiParam(value = "SCHED_ID") @PathVariable(value="sched-id") String schedId
			//@ApiParam(value = "TAG") @RequestParam(value = "tag", required = false)  String tag
			){
		
		SelectTriggerInfoInputModel inputModel = new SelectTriggerInfoInputModel();
		inputModel.setSchedId(schedId);
		//inputModel.setTag(tag);
		
		CronTriggerInfoCollection resultCol = CoreModules.schedulerManager().selectCronTriggerInfo(inputModel);
		
		return resultCol;

	}
	
	
	@GET
	@Path(value="/cron")
	@ApiOperation(value = "Search CronTrigger Info. ",response = CronTriggerInfoCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/cron",headers = "Accept=application/json,application/xml")
	public @ResponseBody CronTriggerInfoCollection selectCronTriggerInfo(){
		
		SelectTriggerInfoInputModel inputModel = new SelectTriggerInfoInputModel();
		
		CronTriggerInfoCollection resultCol = CoreModules.schedulerManager().selectCronTriggerInfo(inputModel);
		
		return resultCol;

	}
	
	@GET
	@Path(value="/simple/{sched-id}")
	@ApiOperation(value = "Search SimpleTrigger Info. ", response = SimpleTriggerInfoCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/simple/{sched-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody SimpleTriggerInfoCollection selectSimpleTriggerInfo(
			@ApiParam(value = "SCHED_ID") @PathVariable(value="sched-id") String schedId
			//@ApiParam(value = "TAG") @RequestParam(value = "tag", required = false) String tag
			) {

		SelectTriggerInfoInputModel inputModel = new SelectTriggerInfoInputModel();
		inputModel.setSchedId(schedId);
		//inputModel.setTag(tag);
		
		SimpleTriggerInfoCollection resultCol = CoreModules.schedulerManager().selectSimpleTriggerInfo(inputModel);
		
		return resultCol;
		
	}
	
	@GET
	@Path(value="/simple")
	@ApiOperation(value = "Search SimpleTrigger Info. ", response = SimpleTriggerInfoCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/simple", headers = "Accept=application/json,application/xml")
	public @ResponseBody SimpleTriggerInfoCollection selectSimpleTriggerInfo() {

		SelectTriggerInfoInputModel inputModel = new SelectTriggerInfoInputModel();
		
		SimpleTriggerInfoCollection resultCol = CoreModules.schedulerManager().selectSimpleTriggerInfo(inputModel);
		
		return resultCol;
		
	}

}
