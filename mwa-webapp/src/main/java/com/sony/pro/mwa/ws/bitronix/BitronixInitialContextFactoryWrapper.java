package com.sony.pro.mwa.ws.bitronix;

import javax.naming.NamingException;

public class BitronixInitialContextFactoryWrapper extends bitronix.tm.jndi.BitronixInitialContextFactory {
	
	private final static String NAMING_FACTORY_URL_PKGS = "java.naming.factory.url.pkgs";
	
	@Override
	 public javax.naming.Context getInitialContext(java.util.Hashtable hashtable) throws NamingException {
		// System Property として、"java.naming.factory.url.pkgs" が定義されていると
		// lookupがうまくいかないため、Context生成段階でフィルタをかけておくという苦肉の策
		if (hashtable != null && hashtable.containsKey(NAMING_FACTORY_URL_PKGS)) {
			hashtable.remove(NAMING_FACTORY_URL_PKGS);
		}
		return super.getInitialContext(hashtable);
	 }
}
