package com.sony.pro.mwa.ws.rest.v1;

import java.util.ArrayList;
import java.util.List;

/*import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;*/


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.ws.datatype.SystemInfo;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.definite.SystemComponent;
import com.sony.pro.mwa.ws.internal.AbsBaseController;
import com.wordnik.swagger.annotations.Api;


// Swagger
@Api(value = "versions", description = "System Controller")
// Spring
@Controller(VersionController.NAME+VersionController.VERSION)
@RequestMapping(value=VersionController.PATH)
public class VersionController extends AbsBaseController {
	static final String NAME = "versions";
	static final String VERSION = PreDefine.V1;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	@RequestMapping(method = RequestMethod.GET, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody SystemInfo getSystemInfo() {
		String version = properties.getProperty("mwa.module.version.webapp");
		logger.info("getSystemInfo: mwa.version=" + version);

		SystemInfo result = new SystemInfo();
		List<KeyValueModel> versions = new ArrayList<>();
		versions.add(new KeyValueModel(SystemComponent.MWA_CORE.key(), version));
		result.setVersions(versions);

		return result;
	}
}
