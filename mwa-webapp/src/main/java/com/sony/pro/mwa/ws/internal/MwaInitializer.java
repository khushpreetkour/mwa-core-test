package com.sony.pro.mwa.ws.internal;

import javax.servlet.http.HttpServlet;

import com.sony.pro.mwa.common.log.MwaLogger;

public class MwaInitializer extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	@Override
	public void init() {
/*    	ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
    	EntityManagerFactory emf = (EntityManagerFactory)ctx.getBean("entityManagerFactory");*/
//		ProcessEngineImpl.initializeEmf(emf);
        
		//ToDo: この辺はDIで処理したい。EntityManagerFactoryBeanを上手いことProcessEngineに引き渡せなかったため暫定対応
/*		ProcessEngineImpl procEngine = (ProcessEngineImpl)ProcessEngineImpl.getInstance();
		procEngine.addListener(new EventListener());
		WorkflowManagerImpl.getInstance().setProcessEngine(procEngine);*/
		
        //ActivityManagerImpl.getInstance(); //ActivityManagerのコンストラクタ呼び出し
        logger.info("mwa-core initialized!!");
	}
	
	@Override
	public void destroy() {
	}
}
