package com.sony.pro.mwa.ws.exception;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Locale;

import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.rc.MWARC_HTTP;

public class ExceptionResolver implements HandlerExceptionResolver, Ordered {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExceptionResolver.class);
    private int order;

//    @Autowired(required = true)
    private LocalizedMessageResolver messageResolver = new LocalizedMessageResolver();

    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, java.lang.Exception ex) {
        outputLog(request, ex);

        int responseStatusCode = getResponseStatusCode(ex);
        try {
            response.setStatus(responseStatusCode);
        } catch (java.lang.Exception e) {
            logger.error("Failed to set response status code to {}.", responseStatusCode);
        }

        com.sony.pro.mwa.ws.exception.Message message = null;
        IMWARC rc = getMWARC(ex, responseStatusCode);
        if (rc == null) {
            logger.warn("The error message key is null.");
        } else {
            message = messageResolver.resolveMessage(rc, ex instanceof com.sony.pro.mwa.ws.exception.Exception ? ((com.sony.pro.mwa.ws.exception.Exception) ex).getArgs() : null,  request);

            if (message == null) {
                logger.warn("Not found the localized error message for '{}'.", rc.name());
            }
        }

        ModelAndView mav = new ModelAndView();
        mav.setViewName("error");
        mav.addObject("code", rc != null ? rc.code() : null);
        mav.addObject("message", message != null ? message.getMessage()
        										: rc != null ? rc.name() : null);
        if (ex instanceof MwaError) {
        	mav.addObject("deviceCode", ((MwaError)ex).getDeviceResponseCode());
        	mav.addObject("deviceMessage", ((MwaError)ex).getDeviceResponseDetails());
        }

        return mav;
    }

    private void outputLog(HttpServletRequest request, java.lang.Exception ex) {
        if (ex instanceof MwaError) {
            MwaError mwaEx = (MwaError)ex;
            if (StringUtils.isEmpty(mwaEx.getDescription()) == true) {
                com.sony.pro.mwa.ws.exception.Message msg = messageResolver.resolveMessage(mwaEx.getMWARC(), null /*mwaEx.getArgs()*/, new Locale(LocaleCollection.DEFAULT_LOCALE));
                if (msg == null) {
                    logger.warn("Not found the error message in English for '{}'.", mwaEx.getMWARC().name());
                } else {
                    mwaEx.setDescription(msg.getMessage());
                }
            }
            logger.warn(String.format("[%s]::[%s]::[%s]", getRequestURLString(request), mwaEx.getMWARC().name(), mwaEx.getDescription()));
        } else {
            logger.error(String.format("[%s]::[%s]::[%s]", getRequestURLString(request), ex.getMessage(), ex.getLocalizedMessage()));
            logger.error("The exception stack trace ...", ex);
        }
    }

    private String getRequestURLString(HttpServletRequest request) {
        return request != null && request.getRequestURL() != null ? request.getRequestURL().toString() : StringUtils.EMPTY;
    }

    private int getResponseStatusCode(java.lang.Exception ex) {
        int responseStatusCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        if (ex instanceof AccessDeniedException) {
            responseStatusCode = HttpServletResponse.SC_FORBIDDEN;
        } else if (ex instanceof AuthenticationException) {
            responseStatusCode = HttpServletResponse.SC_FORBIDDEN;
        } else if (ex instanceof com.sony.pro.mwa.ws.exception.Exception) {
            if (((com.sony.pro.mwa.ws.exception.Exception) ex).getStatusCode() != null) {
                responseStatusCode = ((com.sony.pro.mwa.ws.exception.Exception) ex).getStatusCode();
            }
		}
        return responseStatusCode;
    }

    private IMWARC getMWARC(java.lang.Exception ex, int responseStatusCode) {
    	IMWARC rc;
        if (ex instanceof MwaError) {
        	rc = ((MwaError)ex).getMWARC();
        } else if (ex instanceof HttpMediaTypeNotSupportedException) {
        	rc = MWARC.INVALID_INPUT_FORMAT;
        } else if (ex instanceof MissingServletRequestParameterException) {
        	rc = MWARC.INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND;
        } else if (ex instanceof org.springframework.dao.DataIntegrityViolationException) {
			//logger.warn("DB exception occurred: e=" + ex.getCause() + ", ex.message=" + ex.getCause().getMessage());
        	rc = MWARC.INVALID_INPUT;
        } else {
        	rc = MWARC_HTTP.valueOfByCode(responseStatusCode);
        }
        if (rc == null) {
        	rc = MWARC.SYSTEM_ERROR;
        }
        return rc;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
