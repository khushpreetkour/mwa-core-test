package com.sony.pro.mwa.ws.definite;

public enum SystemComponent {
	MWA_CORE("mwa-core"),
	;
	
	String key;
	
	private SystemComponent() {
		this.key = this.name();
	}
	private SystemComponent(String key) {
		this.key = key;
	}
	
	public String key() {
		return this.key;
	}
}
