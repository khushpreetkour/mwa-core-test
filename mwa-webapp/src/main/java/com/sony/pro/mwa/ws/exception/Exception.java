package com.sony.pro.mwa.ws.exception;

import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC_HTTP;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/11/12
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class Exception extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private IMWARC errorMessageKey;
    private String args[];
    private Integer statusCode;
    private String logDescription;

    public Exception() {
        super();
    }

    public Exception(String message) {
        super(message);
    }

    public Exception(String message, IMWARC errorMessageKey) {
        super(message);
        this.errorMessageKey = errorMessageKey;
    }

    public Exception(String message, IMWARC errorMessageKey, String logDescription) {
        super(message);
        this.errorMessageKey = errorMessageKey;
        this.logDescription = logDescription;
    }

    public Exception(String message, IMWARC errorMessageKey, String args[], String logDescription) {
        super(message);
        this.errorMessageKey = errorMessageKey;
        this.args = args;
        this.logDescription = logDescription;
    }

    public Exception(IMWARC errorMessageKey) {
        super();
        this.errorMessageKey = errorMessageKey;
    }

    public Exception(IMWARC errorMessageKey, String logDescription) {
        super();
        this.errorMessageKey = errorMessageKey;
        this.logDescription = logDescription;
    }

    public Exception(IMWARC errorMessageKey, String args[]) {
        super();
        this.errorMessageKey = errorMessageKey;
        this.args = args;
    }

    public Exception(IMWARC errorMessageKey, String args[], String logDescription) {
        super();
        this.errorMessageKey = errorMessageKey;
        this.args = args;
        this.logDescription = logDescription;
    }

    public Exception(Integer statusCode) {
        super("The status code is " + statusCode);
        this.statusCode = statusCode;
        this.errorMessageKey = MWARC_HTTP.valueOfByCode(statusCode);
    }

    public Exception(Message message) {
        super(message.getMessage());
    }

    public IMWARC getErrorMessageKey() {
        return errorMessageKey;
    }

    public void setErrorMessageKey(IMWARC errorMessageKeyEnum) {
        this.errorMessageKey = errorMessageKeyEnum;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
        this.errorMessageKey = MWARC_HTTP.valueOfByCode(statusCode);
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public String getLogDescription() {
        return this.logDescription;
    }

    public void setLogDescription(String logDescription) {
        this.logDescription = logDescription;
    }
}
