package com.sony.pro.mwa.ws.rest.v1;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.storage.LocationModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiParam;


@Api(value = "locations", description = "Location Controller")
@Controller(StorageController.NAME+StorageController.VERSION)
@RequestMapping(value=StorageController.PATH)
public class StorageController {
	static final String NAME = "locations";
	static final String VERSION = PreDefine.V1;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	public StorageController() {
		//do nothing
	}

	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<LocationModel> getLocations() {
		List<LocationModel> result = CoreModules.storageManager().getLocations(null,null,null,null).getModels();
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{location-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel getLocation(
			@ApiParam(value = "ID of location(uuid)") @PathVariable("location-id") String locationId
			) {
		
		String message = "location(id=" + locationId + "), parameters=...";
		logger.info("RestProvider[operateActivity]:" + message);
		
		LocationModel location = CoreModules.storageManager().getLocation(locationId);
		return location;
	}

	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel addLocation(
			@ApiParam(value = "The ") @RequestBody(required = true) LocationModel model
			) {
		
		LocationModel result = CoreModules.storageManager().addLocation(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/{location-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel updateLocation(
			@ApiParam(value = "The location id to update it.") @PathVariable("location-id") String locationId,
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) LocationModel model) {

		if (locationId == null || model == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		model.setId(locationId);
		LocationModel result = CoreModules.storageManager().updateLocation(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return result;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{location-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel deleteLocation(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("location-id") String locationId) {
		
		String message = "location(id=" + locationId + ")";
		logger.info("RestProvider[deletePlugin]:" + message);
		LocationModel ret = CoreModules.storageManager().deleteLocation(locationId);
		
		if (ret == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return ret;
	}
}
