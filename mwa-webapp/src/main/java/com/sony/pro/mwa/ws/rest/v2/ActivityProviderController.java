package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(ActivityProviderController.PATH)
@Api(value = "activity-provider",
	description = "Activity provider resouce. Activity Provider is actual executor activity and it is registered at the same time with plug-in registration. "
			+ "For example, transcode device is a provider of transcode activity. "
			+ "If necssary, MWA user needs to register provider to ask activity execution."
	)
@Controller(ActivityProviderController.NAME+ActivityProviderController.VERSION)
@RequestMapping(value=ActivityProviderController.PATH)
public class ActivityProviderController {
	static final String NAME = "activity-providers";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;
	
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate activity providers. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", 
			response=ActivityProviderCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderCollection getProviders(
		    @ApiParam( name="sort", value = "The sort of the instances.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
		) {
		
		ActivityProviderCollection result = CoreModules.activityProviderManager().getProviders(sorts, filters, offset, limit);
		return result;
	}

	@GET
	@Path(value="/{provider-id}")
	@ApiOperation(value = "Get detailed information of specified provider.", response=ActivityProviderModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/{provider-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel getProvider(
			@ApiParam(value = "The service provider id") @PathVariable("provider-id") String providerId) {
		
		ActivityProviderModel result = CoreModules.activityProviderManager().getProvider(providerId);
		if (result == null)
			throw new MwaError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND);
		return result;
	}

	@POST
	@Path(value="")
	@ApiOperation(value = "Register a new activity provider.", response=ActivityProviderModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel addProvider(
			@ApiParam(value = "The ") @RequestBody(required = true) ActivityProviderModel model
			) {
		
		ActivityProviderModel result = CoreModules.activityProviderManager().addProvider(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return result;
	}

	@PUT
	@Path(value="/{provider-id}")
	@ApiOperation(value = "Update information of specified provider.", response=ActivityProviderModel.class)
	@RequestMapping(method = RequestMethod.PUT, value = "/{provider-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel updateProvider(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("provider-id") String providerId,
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) ActivityProviderModel model) {

		if (providerId == null || model == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		model.setId(providerId);
		ActivityProviderModel result = CoreModules.activityProviderManager().updateProvider(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return new ActivityProviderModel(result);
	}

	@DELETE
	@Path(value="/{provider-id}")
	@ApiOperation(value = "Delete information of specified provider.", response=ActivityProviderModel.class)
	@RequestMapping(method = RequestMethod.DELETE, value = "/{provider-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel deleteProvider(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("provider-id") String providerId) {
		
		String message = "provider(id=" + providerId + ")";
		logger.info("RestProvider[deletePlugin]:" + message);
		ActivityProviderModel ret = CoreModules.activityProviderManager().deleteProvider(providerId);
		
		if (ret == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return ret;
	}
	
	@DELETE
	@Path(value="")
	@ApiOperation(value = "Delete appropriate activity providers. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", 
			response=ActivityProviderCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderCollection deleteProviders(
		    @ApiParam( name="sort", value = "The sort of the instances.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
		) {
		
		ActivityProviderCollection result = CoreModules.activityProviderManager().deleteProviders(sorts, filters, offset, limit);
		return result;
	}
}
