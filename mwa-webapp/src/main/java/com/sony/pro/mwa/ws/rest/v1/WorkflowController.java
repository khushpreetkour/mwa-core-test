package com.sony.pro.mwa.ws.rest.v1;

import java.util.List;

/*import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;*/


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.kbase.ExtensionModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.utils.Converter;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.AbsBaseController;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiParam;


@Api(value = "workflows", description = "System Controller")
@Controller(WorkflowController.NAME+WorkflowController.VERSION)
@RequestMapping(value=WorkflowController.PATH)
public class WorkflowController extends AbsBaseController {
	static final String NAME = "workflows";
	static final String VERSION = PreDefine.V1;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<ExtensionModel> getWorkflows() {
		List<ExtensionModel> result = CoreModules.knowledgeBase().getBpmns(null, null, null, null).getModels();
		return result;
	}

	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> operateWorkflow(
			@ApiParam(value = "The comma-delimited IDs of the tasks that should be retrieved.") @RequestParam(value = "operation", required = true) String operation) {
		
		//CoreModules.knowledgeBase().loadPlugins();
		CoreModules.knowledgeBase().loadBpmns();
		return Converter.mapToKeyValueList(OperationResult.newInstance());
	}

}
