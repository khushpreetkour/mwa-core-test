package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.kbase.ExtensionCollection;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.utils.Converter;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.AbsBaseController;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(RuleController.PATH)
@Api(value = "rules", description = "Rule Resources")
@Controller(RuleController.NAME+RuleController.VERSION)
@RequestMapping(value=RuleController.PATH)
public class RuleController extends AbsBaseController {
    static final String NAME = "rules";
    static final String VERSION = PreDefine.V2;
    static final String PATH = "/" + VERSION + "/" + NAME;

    protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

    @GET
    @Path(value="")
    @ApiOperation(value = "Get appropriate rules (Decision Tables). Please see detailed information about search. " + PreDefine.SEARCH_URL + " ",  response=ExtensionCollection.class)
    @ApiResponses(value = {
                    @ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
                })
    @RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
    public @ResponseBody ExtensionCollection getRules(
            @ApiParam( name="sort", value = "The sort of the rules.") @RequestParam(value = "sort", required = false) List<String> sorts,
            @ApiParam(value = "The filter of the rules.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
            @ApiParam(value = "The start number of the rules.") @RequestParam(value = "offset", required = false) Integer offset,
            @ApiParam(value = "The maximum number of the rules.") @RequestParam(value = "limit", required = false) Integer limit
            ) {
        ExtensionCollection result = CoreModules.knowledgeBase().getRules(sorts, filters, offset, limit);
        return result;
    }

    @POST
    @Path(value="")
    @ApiOperation(value = "Reload registered MWA rules.", response=KeyValueModel.class, responseContainer="List")
    @ApiResponses(value = {
                    @ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
                })
    @RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
    public @ResponseBody List<KeyValueModel> operaterule(
            @ApiParam(value = "The comma-delimited IDs of the tasks that should be retrieved.") @RequestParam(value = "operation", required = true) String operation) {

        CoreModules.knowledgeBase().loadRules();
        return Converter.mapToKeyValueList(OperationResult.newInstance());
    }

}
