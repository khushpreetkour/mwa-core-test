package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.kbase.ExtensionCollection;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.Converter;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.AbsBaseController;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;


@Path(PluginController.PATH)
@Api(value = "plugins", description = "Plugin resouce")
@Controller(PluginController.NAME+PluginController.VERSION)
@RequestMapping(value=PluginController.PATH)
public class PluginController extends AbsBaseController {
	static final String NAME = "plugins";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate plugins. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", response=ExtensionCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody ExtensionCollection getPlugins(
		    @ApiParam( name="sort", value = "The sort of the plugins.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the plugins.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the plugins.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the plugins.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		ExtensionCollection result = CoreModules.knowledgeBase().getPlugins(sorts, filters, offset, limit);
		return result;
	}

	@POST
	@Path(value="")
	@ApiOperation(value = "Reload registered MWA plugins.", response=KeyValueModel.class, responseContainer="List")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> operatePlugin(
			@ApiParam(value = "The comma-delimited IDs of the tasks that should be retrieved.") @RequestParam(value = "operation", required = true) String operation) {
		
		CoreModules.knowledgeBase().loadPlugins();
		//CoreModules.knowledgeBase().loadBpmns();
		return Converter.mapToKeyValueList(OperationResult.newInstance());
	}

	@POST
	@Path(value="/{plugin-name}/{plugin-version:.+}")
	@ApiOperation(value = "Register MWA plugin, to register it, please locate plugin jar file into MWA_HOME.", response=KeyValueModel.class, responseContainer="List")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "/{plugin-name}/{plugin-version:.+}", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> loadPlugin(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("plugin-name") String pluginName, 
			@ApiParam(value = "The version number of the activity template (Currently it's being ignored).") @PathVariable("plugin-version") String pluginVersion) {
		
		OperationResult result = CoreModules.knowledgeBase().loadPlugin(pluginName, pluginVersion);
		return Converter.mapToKeyValueList(result);
	}

	@DELETE
	@Path(value="/{plugin-name}/{plugin-version:.+}")
	@ApiOperation(value = "Delete MWA plugin.", response=KeyValueModel.class, responseContainer="List")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/{plugin-name}/{plugin-version:.+}", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> deletePlugin(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("plugin-name") String pluginName, 
			@ApiParam(value = "The version number of the activity template.") @PathVariable("plugin-version") String pluginVersion) {
		
		String message = "plugin(name=" + pluginName + ", version=" + pluginVersion + "), parameters=...";
		logger.info("RestProvider[deletePlugin]:" + message);
		OperationResult result = CoreModules.knowledgeBase().deletePlugin(pluginName, pluginVersion);
		
		if (result == null) {
			result = OperationResult.newInstance(MWARC.SYSTEM_ERROR, "Activity Adapter returns invalid response.");
		}

		return Converter.mapToKeyValueList(result);
	}
}
