package com.sony.pro.mwa.ws.rest.v1;

import java.util.List;

/*import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;*/


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.kbase.ExtensionModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.Converter;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.AbsBaseController;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiParam;


@Api(value = "plugins", description = "Plugin resouce")
@Controller(PluginController.NAME+PluginController.VERSION)
@RequestMapping(value=PluginController.PATH)
public class PluginController extends AbsBaseController {
	static final String NAME = "plugins";
	static final String VERSION = PreDefine.V1;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<ExtensionModel> getPlugins() {
		List<ExtensionModel> result = CoreModules.knowledgeBase().getPlugins(null,null,null,null).getModels();
		return result;
	}

	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> operatePlugin(
			@ApiParam(value = "The comma-delimited IDs of the tasks that should be retrieved.") @RequestParam(value = "operation", required = true) String operation) {
		
		CoreModules.knowledgeBase().loadPlugins();
		//CoreModules.knowledgeBase().loadBpmns();
		return Converter.mapToKeyValueList(OperationResult.newInstance());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{plugin-name}/{plugin-version:.+}", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> loadPlugin(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("plugin-name") String pluginName, 
			@ApiParam(value = "The version number of the activity template (Currently it's being ignored).") @PathVariable("plugin-version") String pluginVersion) {
		
		OperationResult result = CoreModules.knowledgeBase().loadPlugin(pluginName, pluginVersion);
		return Converter.mapToKeyValueList(result);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{plugin-name}/{plugin-version:.+}", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> deletePlugin(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("plugin-name") String pluginName, 
			@ApiParam(value = "The version number of the activity template.") @PathVariable("plugin-version") String pluginVersion) {
		
		String message = "plugin(name=" + pluginName + ", version=" + pluginVersion + "), parameters=...";
		logger.info("RestProvider[deletePlugin]:" + message);
		OperationResult result = CoreModules.knowledgeBase().deletePlugin(pluginName, pluginVersion);
		
		if (result == null) {
			result = OperationResult.newInstance(MWARC.SYSTEM_ERROR, "Activity Adapter returns invalid response.");
		}

		return Converter.mapToKeyValueList(result);
	}
}
