package com.sony.pro.mwa.ws.docs;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class ControllerForSwagger extends AbstractController {
	private String content;

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("index", "text", content);
	}

	public void setMessage(String message) {
		this.content = message;
	}

}
