package com.sony.pro.mwa.ws.exception;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;

import com.sony.pro.mwa.rc.IMWARC;

public class LocalizedMessageResolver {
//    @Autowired
    private ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();

//    @Autowired
    private LocaleResolver localeResolver = new com.sony.pro.mwa.ws.exception.LocaleResolver();

    public Message resolveMessage(String key, Locale locale) {
        return resolveMessage(key, null, locale);
    }

    public Message resolveMessage(String key, String[] args, Locale locale) {
        try {
            return new Message(messageSource.getMessage(key, args, locale));
        } catch (NoSuchMessageException e) {
            return null;
        }
    }

    public Message resolveMessage(IMWARC rc, HttpServletRequest request) {
        return resolveMessage(rc, null, localeResolver.resolveLocale(request));
    }

    public Message resolveMessage(IMWARC rc, String[] args, HttpServletRequest request) {
        return resolveMessage(rc, args, localeResolver.resolveLocale(request));
    }

    public Message resolveMessage(IMWARC rc, String[] args, Locale locale) {
        try {
            return new Message(messageSource.getMessage(rc.name(), args, locale));
        } catch (NoSuchMessageException e) {
            return null;
        }
    }
}
