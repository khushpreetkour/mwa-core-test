package com.sony.pro.mwa.ws.datatype;

import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;

public class ActivityProviderModelIn extends ActivityProviderModel {
	public ActivityProviderModelIn(){
		super();
	}
	public ActivityProviderModelIn(ActivityProviderModel model){
		super(model);
	}
	
	@Override
	@XmlTransient @JsonIgnore
	public String getId() {
		return super.getId();
	}
}
