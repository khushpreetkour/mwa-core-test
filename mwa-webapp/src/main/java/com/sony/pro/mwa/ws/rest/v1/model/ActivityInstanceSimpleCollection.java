package com.sony.pro.mwa.ws.rest.v1.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceSimpleModel;

import java.util.List;

@XmlRootElement(name = "activity-instances")
public class ActivityInstanceSimpleCollection extends Collection<ActivityInstanceSimpleModel> {
    public ActivityInstanceSimpleCollection() {
        super();
    }

    public ActivityInstanceSimpleCollection(List<ActivityInstanceSimpleModel> models) {
        super(models);
    }

    public ActivityInstanceSimpleCollection(List<ActivityInstanceSimpleModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public ActivityInstanceSimpleCollection(Long totalCount, Long count, List<ActivityInstanceSimpleModel> models) {
        super(totalCount, count, models);
    }

    @Override
    @XmlElement(name="activity-instance")
    public List<ActivityInstanceSimpleModel> getModels() {
        return super.getModels();
    }
}
