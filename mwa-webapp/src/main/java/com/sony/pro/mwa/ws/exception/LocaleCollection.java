package com.sony.pro.mwa.ws.exception;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 14/01/30
 * Time: 10:40
 * To change this template use File | Settings | File Templates.
 */

@XmlRootElement(name = "locales")
public class LocaleCollection extends Collection<LocaleModel>{

    public static final String DEFAULT_LOCALE = "en";
    
    public LocaleCollection() {
        super();
    }

    public LocaleCollection(List<LocaleModel> models) {
        super(models);
    }

    public LocaleCollection(Long totalCount, Long count, List<LocaleModel> models) {
        super(totalCount, count, models);
    }

    public LocaleCollection(LocaleCollection backendCollection) {
        ArrayList<LocaleModel> frontendModels = new ArrayList<>();
        for(LocaleModel backendModel: backendCollection.getModels()) {
            frontendModels.add(new LocaleModel(backendModel));
        }

        this.setTotalCount(backendCollection.getTotalCount());
        this.setCount(backendCollection.getCount());
        this.setModels(frontendModels);
//        this.setErrors(backendCollection.getErrors());
    }

    @Override
    @XmlElement(name="locale")
    public List<LocaleModel> getModels() {
        return super.getModels();
    }

}
