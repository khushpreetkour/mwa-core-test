package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(ResourceController.PATH)
@Api(value = "resource", 
	description = "Resources for activities."
	)
@Controller(ResourceController.NAME+ResourceController.VERSION)
@RequestMapping(value=ResourceController.PATH)
public class ResourceController {
	static final String NAME = "resources";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get detailed information of specified resource.", response=ResourceModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/{resource-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceModel getResource(
			@ApiParam(value = "ID of resource.") @PathVariable("resource-id") String id
			) {
		
		String message = "resource(id=" + id + ")";
		logger.info("getResource[operateActivity]:" + message);
		
		ResourceModel model = CoreModules.resourceManager().getResource(id);
		return model;
	}
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate activity provider types. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", 
			response=ResourceCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceCollection getResources(
		    @ApiParam( name="sort", value = "The sort of the plugin.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		
		ResourceCollection result = CoreModules.resourceManager().getResources(sorts, filters, offset, limit);
		return result;
	}

	@POST
	@Path(value="")
	@ApiOperation(value = "Register a new resource.", response=ResourceModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceModel addResource(
			@ApiParam(value = "The ") @RequestBody(required = true) ResourceModel model
			) {
		
		ResourceModel result = CoreModules.resourceManager().addResource(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return result;
	}

	@PUT
	@Path(value="")
	@ApiOperation(value = "Update specified resource.", response=ResourceModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.PUT, value = "/{resource-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceModel updateResource(
			@ApiParam(value = "The resource id to update it.") @PathVariable("resource-id") String resourceId,
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) ResourceModel model) {

		if (resourceId == null || model == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		model.setId(resourceId);
		ResourceModel result = CoreModules.resourceManager().updateResource(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return result;
	}
	
	@DELETE
	@Path(value="")
	@ApiOperation(value = "Delete specified resource.", response=ResourceModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/{resource-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceModel deleteResource(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("resource-id") String resourceId) {
		
		String message = "resource(id=" + resourceId + ")";
		logger.info("[" + new Object(){}.getClass().getEnclosingMethod().getName() + "]:" + message);
		ResourceModel ret = CoreModules.resourceManager().deleteResource(resourceId);
		
		if (ret == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return ret;
	}

	@DELETE
	@Path(value="")
	@ApiOperation(value = "Get appropriate resources. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ",  response=ResourceCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceCollection deleteResources(
		    @ApiParam(name="sort", value = "The sort of the resources.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the resources.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the resources.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the resources.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		logger.info("[" + new Object(){}.getClass().getEnclosingMethod().getName() + "]: called!!");
		ResourceCollection result = CoreModules.resourceManager().deleteResources(sorts, filters, offset, limit);
		return result;
	}
}
