package com.sony.pro.mwa.ws.internal;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class AbsBaseController {
	
    public static Properties properties;
    
    @Autowired
    public void setProperties(Properties properties) {
    	AbsBaseController.properties = properties;
    }
}
