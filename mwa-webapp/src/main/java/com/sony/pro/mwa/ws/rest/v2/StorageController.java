package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.storage.LocationCollection;
import com.sony.pro.mwa.model.storage.LocationModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;


@Path(StorageController.PATH)
@Api(value = "locations", description = "Location Controller")
@Controller(StorageController.NAME+StorageController.VERSION)
@RequestMapping(value=StorageController.PATH)
public class StorageController {
	static final String NAME = "locations";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	public StorageController() {
		//do nothing
	}

	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate locations. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ",  response=LocationCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationCollection getLocations(
		    @ApiParam(name="sort", value = "The sort of the locations.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the locations.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the locations.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the locations.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		LocationCollection result = CoreModules.storageManager().getLocations(sorts, filters, offset, limit);
		return result;
	}
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get detailed information of specified location.", response=LocationModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/{location-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel getLocation(
			@ApiParam(value = "ID of location(uuid)") @PathVariable("location-id") String locationId
			) {
		
		String message = "location(id=" + locationId + "), parameters=...";
		logger.info("RestProvider[operateActivity]:" + message);
		
		LocationModel location = CoreModules.storageManager().getLocation(locationId);
		return location;
	}

	@POST
	@Path(value="")
	@ApiOperation(value = "Register a new location.", response=LocationModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel addLocation(
			@ApiParam(value = "The ") @RequestBody(required = true) LocationModel model
			) {
		
		LocationModel result = CoreModules.storageManager().addLocation(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return result;
	}
	
	@PUT
	@Path(value="")
	@ApiOperation(value = "Update specified location.", response=LocationModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.PUT, value = "/{location-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel updateLocation(
			@ApiParam(value = "The location id to update it.") @PathVariable("location-id") String locationId,
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) LocationModel model) {

		if (locationId == null || model == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		model.setId(locationId);
		LocationModel result = CoreModules.storageManager().updateLocation(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return result;
	}

	@DELETE
	@Path(value="")
	@ApiOperation(value = "Delete specified location.", response=LocationModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/{location-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationModel deleteLocation(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("location-id") String locationId) {
		
		String message = "location(id=" + locationId + ")";
		logger.info("RestProvider[deletePlugin]:" + message);
		LocationModel ret = CoreModules.storageManager().deleteLocation(locationId);
		
		if (ret == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return ret;
	}

	@DELETE
	@Path(value="")
	@ApiOperation(value = "Get appropriate locations. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ",  response=LocationCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody LocationCollection deleteLocations(
		    @ApiParam(name="sort", value = "The sort of the locations.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the locations.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the locations.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the locations.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		LocationCollection result = CoreModules.storageManager().deleteLocations(sorts, filters, offset, limit);
		return result;
	}
	
}
