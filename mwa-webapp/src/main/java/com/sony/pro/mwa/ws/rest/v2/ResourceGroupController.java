package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.resource.ResourceGroupCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(ResourceGroupController.PATH)
@Api(value = "resource-group", 
	description = "Resource groups for activities."
	)
@Controller(ResourceGroupController.NAME+ResourceGroupController.VERSION)
@RequestMapping(value=ResourceGroupController.PATH)
public class ResourceGroupController {
	static final String NAME = "resource-groups";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get detailed information of specified resource-group.", response=ResourceGroupModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/{resource-group-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceGroupModel getResourceGroup(
			@ApiParam(value = "ID of resource-group.") @PathVariable("resource-group-id") String id
			) {
		
		String message = "resource-group(id=" + id + ")";
		logger.info("getResourceGroup[operateActivity]:" + message);
		
		ResourceGroupModel model = CoreModules.resourceManager().getResourceGroup(id);
		return model;
	}
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate activity provider types. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", response=ResourceGroupCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceGroupCollection getResourceGroups(
		    @ApiParam( name="sort", value = "The sort of the plugin.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		
		ResourceGroupCollection result = CoreModules.resourceManager().getResourceGroups(sorts, filters, offset, limit);
		return result;
	}

	@POST
	@Path(value="")
	@ApiOperation(value = "Register a new resource group.", response=ResourceGroupModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceGroupModel addResourceGroup(
			@ApiParam(value = "The ") @RequestBody(required = true) ResourceGroupModel model
			) {
		
		ResourceGroupModel result = CoreModules.resourceManager().addResourceGroup(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return result;
	}

	@PUT
	@Path(value="")
	@ApiOperation(value = "Update specified resource group.", response=ResourceGroupModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.PUT, value = "/{resource-group-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceGroupModel updateResourceGroup(
			@ApiParam(value = "The resource id to update it.") @PathVariable("resource-group-id") String id,
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) ResourceGroupModel model) {

		if (id == null || model == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		model.setId(id);
		ResourceGroupModel result = CoreModules.resourceManager().updateResourceGroup(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return result;
	}
	
	@DELETE
	@Path(value="")
	@ApiOperation(value = "Delete specified resource.", response=ResourceGroupModel.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "/{resource-group-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceGroupModel deleteResourceGroup(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("resource-group-id") String id) {
		
		String message = "resource-group(id=" + id + ")";
		logger.info("[" + new Object(){}.getClass().getEnclosingMethod().getName() + "]:" + message);
		ResourceGroupModel ret = CoreModules.resourceManager().deleteResourceGroup(id);
		
		if (ret == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return ret;
	}

	@DELETE
	@Path(value="")
	@ApiOperation(value = "Get appropriate resources. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ",  response=ResourceGroupCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody ResourceGroupCollection deleteResourceGroups(
		    @ApiParam(name="sort", value = "The sort of the resources.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the resources.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the resources.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the resources.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		logger.info("[" + new Object(){}.getClass().getEnclosingMethod().getName() + "]: called!!");
		ResourceGroupCollection result = CoreModules.resourceManager().deleteResourceGroups(sorts, filters, offset, limit);
		return result;
	}
}
