package com.sony.pro.mwa.ws.rest.v2;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path(ActivityProviderTypeController.PATH)
@Api(value = "activity-provider-type", 
	description = "Activity Provider Type resource. Activity provider type indicates types of providers."
			+ "For example, user has same kind of 3 transcode devices."
	)
@Controller(ActivityProviderTypeController.NAME+ActivityProviderTypeController.VERSION)
@RequestMapping(value=ActivityProviderTypeController.PATH)
public class ActivityProviderTypeController {
	static final String NAME = "activity-provider-types";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;
	
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate activity provider types. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", 
			response=ActivityProviderTypeCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderTypeCollection getProviderTypes(
		    @ApiParam( name="sort", value = "The sort of the plugin.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		
		ActivityProviderTypeCollection result = CoreModules.knowledgeBase().getActivityProviderTypes(sorts, filters, offset, limit);
		return result;
	}
	
/*	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel addProvider(
			@ApiParam(value = "The ") @RequestBody(required = true) ActivityProviderModelIn model
			) {
		
		ActivityProviderModel result = CoreModules.activityProviderManager().addProvider(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return new ActivityProviderModel(result);
	}
*/
	//まだコメントアウト
/*	@RequestMapping(method = RequestMethod.GET, value = "/{provider-type-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderType getProviderType(
			@ApiParam(value = "The service provider type id") @PathVariable("provider-type-id") String providerTypeId) {
		
		ActivityProviderType result = CoreModules.knowledgeBase().getActitvityProviderTypeService(providerTypeId);
		return result;
	}*/

}
