package com.sony.pro.mwa.ws.rest.v1;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.sony.pro.mwa.internal.activity.IActivityManagerInternal;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceSimpleModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.Converter;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.sony.pro.mwa.ws.rest.v1.model.ActivityInstanceSimpleCollection;


@Api(value = ActivityInstanceController.NAME, description = "Activity instance resource")
@Controller(ActivityInstanceController.NAME+ActivityInstanceController.VERSION)
@RequestMapping(value=ActivityInstanceController.PATH)
public class ActivityInstanceController {
	
	static final String NAME = "activity-instances";
	static final String VERSION = PreDefine.V1;
	static final String PATH = "/" + VERSION + "/" + NAME;
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	/*EntityManagerFactory emf;
	EntityManager em;
	*/
	public ActivityInstanceController() {
	}

	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityInstanceSimpleCollection getInstances(
		    @ApiParam( name="sort", value = "The sort of the instances.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		
		//logger.info("RestProvider [getInstanceDetails]: " + message);
		if (filters != null) {
			for (int i=0; i<filters.size(); i++) {
				try {
					String filter = URLDecoder.decode(filters.get(i), "UTF-8");
					filters.set(i, filter);
				} catch (UnsupportedEncodingException e) {
					logger.error("getInstances failed", e);
					throw new MwaError(MWARC.INVALID_INPUT_FORMAT);
				}
			}
		}
		
		ActivityInstanceCollection result = CoreModules.activityManager().getInstances(sorts, filters, offset, limit);
		return toSimpleColloction(result);
	}
	
	protected ActivityInstanceSimpleCollection toSimpleColloction(ActivityInstanceCollection collection) {
		if (collection == null)
			return null;
		List<ActivityInstanceSimpleModel> returnCollection = new ArrayList<>();
		for (ActivityInstanceModel model : collection.getModels()) {
			returnCollection.add(new ActivityInstanceSimpleModel(model));
		}
		return new ActivityInstanceSimpleCollection(collection.getTotalCount(), collection.getCount(), returnCollection);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{instance-id}/{child-name}",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> operateInstance(
			@ApiParam(value = "Parent Activity Instance ID (e.g. workflow instance id)") @PathVariable("instance-id") String parentInstanceId,
			@ApiParam(value = "Child Activity Name (e.g. step name of workflow)") @PathVariable("child-name")  String childName, 
			@ApiParam(value = "Operation (or event) about specified activity instnace.") @RequestParam(value = "operation", required = true) String operation, 
			@ApiParam(value = "The workflow objects that should be updated.") @RequestBody(required = false) List<KeyValueModel> parameters) {
		
		String message = "instance-id=" + parentInstanceId + ", childName=" + childName + ", operation=" + operation;
		logger.info("RestProvider [operateInstance]: " + message);
		
		
		OperationResult result = CoreModules.activityManager().operateActivity(parentInstanceId, childName, operation, Converter.keyValueListToMap(parameters));
		return Converter.mapToKeyValueList(result);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{instance-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> operateInstance(
			@ApiParam(value = "Activity Inatance ID") @PathVariable(value="instance-id") String instanceId, 
			@ApiParam(value = "The comma-delimited IDs of the tasks that should be retrieved.") @RequestParam(value = "operation", required = true) String operation, 
			@ApiParam(value = "The workflow objects that should be updated.") @RequestBody(required = false) List<KeyValueModel> parameters) {
		
		String message = "instance-id=" + instanceId + ", operation=" + operation;
		logger.info("RestProvider [operateInstance]: " + message);
		
		OperationResult result = CoreModules.activityManager().operateActivity(instanceId, operation, Converter.keyValueListToMap(parameters));
		return Converter.mapToKeyValueList(result);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{instance-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityInstanceModel getInstanceDetails(
			@ApiParam(value = "Activity Inatance ID") @PathVariable(value="instance-id") String instanceId) {
		
		String message = "instance-id=" + instanceId;
		logger.info("RestProvider [getInstanceDetails]: " + message);
		
		ActivityInstanceModel result = CoreModules.activityManager().getInstance(instanceId);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{instance-id}/{child-name}",headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityInstanceModel getInstanceDetails(
			@ApiParam(value = "Parent Activity Instance ID (Workflow instance id)") @PathVariable("instance-id") String parentInstanceId,
			@ApiParam(value = "Child Activity Name (Step name of workflow)") @PathVariable("child-name")  String childName) {
		
		//ほぼ同じ子供の有無だけで、ほぼ同じ実装だが、下回りのDB検索の最適化のため、別I/Fで残す
		ActivityInstanceModel result = CoreModules.activityManager().getChildInstance(parentInstanceId, childName);
		return result;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{instance-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityInstanceModel deleteInstance(
			@ApiParam(value = "Activity Instance ID (Child Instances will be deleted)") @PathVariable("instance-id") String instanceId) {
		
		String message = "instance-id=" + instanceId;
		logger.info("RestProvider [deleteInstance]: " + message);
		
		return ((IActivityManagerInternal)CoreModules.activityManager()).deleteInstance(instanceId);
	}
	
	@DELETE
	@Path(value="")
	@ApiOperation(	value = "Delete appropriate activity instances. Please see detailed information about search. " + PreDefine.SEARCH_URL + " " , 
					response = ActivityInstanceCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.DELETE, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityInstanceCollection deleteInstances(
		    @ApiParam( name="sort", value = "The sort of the instances.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		
		//logger.info("RestProvider [getInstanceDetails]: " + message);
		if (filters != null) {
			for (int i=0; i<filters.size(); i++) {
				try {
					String filter = URLDecoder.decode(filters.get(i), "UTF-8");
					filters.set(i, filter);
				} catch (UnsupportedEncodingException e) {
					logger.error("deleteInstances failed", e);
					throw new MwaError(MWARC.INVALID_INPUT_FORMAT);
				}
			}
		}
		
		ActivityInstanceCollection models = ((IActivityManagerInternal)CoreModules.activityManager()).deleteInstances(sorts, filters, offset, limit);
		return models;
	}
}
