package com.sony.pro.mwa.ws.rest.v1;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.ws.datatype.ActivityProviderModelIn;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Api(value = "activity-provider",
	description = "Activity provider resouce. Activity Provider is actual executor activity, for example, transcoder device is a provider of transcode activity. "
			+ "MWA user needs to register provider to ask activity execution if necessary, and specify it by ProviderId at activity execution."
	)
@Controller(ActivityProviderController.NAME+ActivityProviderController.VERSION)
@RequestMapping(value=ActivityProviderController.PATH)
public class ActivityProviderController {
	static final String NAME = "activity-providers";
	static final String VERSION = PreDefine.V1;
	static final String PATH = "/" + VERSION + "/" + NAME;
	
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@RequestMapping(method = RequestMethod.GET, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody List<ActivityProviderModel> getProviders() {
		
		List<ActivityProviderModel> result = CoreModules.activityProviderManager().getProviders(null,null,null,null).getModels();
		return result;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{provider-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel getProvider(
			@ApiParam(value = "The service provider id") @PathVariable("provider-id") String providerId) {
		
		ActivityProviderModel result = CoreModules.activityProviderManager().getProvider(providerId);
		return result;
	}

	@RequestMapping(method = RequestMethod.POST, value = "", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel addProvider(
			@ApiParam(value = "The ") @RequestBody(required = true) ActivityProviderModel model
			) {
		
		ActivityProviderModel result = CoreModules.activityProviderManager().addProvider(model);
		
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return new ActivityProviderModel(result);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{provider-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel updateProvider(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("provider-id") String providerId,
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = true) ActivityProviderModel model) {

		if (providerId == null || model == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		model.setId(providerId);
		ActivityProviderModel result = CoreModules.activityProviderManager().updateProvider(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}
		return new ActivityProviderModel(result);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{provider-id}", headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityProviderModel deleteProvider(
			@ApiParam(value = "The service provider id to delete it.") @PathVariable("provider-id") String providerId) {
		
		String message = "provider(id=" + providerId + ")";
		logger.info("RestProvider[deletePlugin]:" + message);
		ActivityProviderModel ret = CoreModules.activityProviderManager().deleteProvider(providerId);
		
		if (ret == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		}

		return ret;
	}
}
