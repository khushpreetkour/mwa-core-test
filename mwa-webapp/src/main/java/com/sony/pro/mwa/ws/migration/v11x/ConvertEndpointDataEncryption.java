package com.sony.pro.mwa.ws.migration.v11x;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.sony.pro.mwa.activity.framework.executor.ExecutorServiceManager;
import com.sony.pro.mwa.activity.repository.database.ActivityInstanceDaoImpl;
import com.sony.pro.mwa.activity.repository.database.query.ActivityInstanceQuery;
import com.sony.pro.mwa.activity.repository.database.query.ActivityInstanceQuery.COLUMN;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.CipherUtils;
import com.sony.pro.mwa.internal.definite.PreDefine;
import com.sony.pro.mwa.internal.utils.InternalConverter;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeString;

import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

public class ConvertEndpointDataEncryption implements liquibase.change.custom.CustomTaskChange {

	static MwaLogger logger = MwaLogger.getLogger(ConvertEndpointDataEncryption.class);
	
	@Override
	public String getConfirmationMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFileOpener(ResourceAccessor arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setUp() throws SetupException {
		// TODO Auto-generated method stub

	}

	@Override
	public ValidationErrors validate(Database arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void execute(Database database) throws CustomChangeException {
		// TODO Auto-generated method stub
		final JdbcConnection conn = (JdbcConnection) database.getConnection();
		try {
			conn.setAutoCommit(false);
			final String selectSQL = "SELECT service_endpoint_id,encode(decrypt(decode(service_endpoint_password_bak,'hex'),'salt','aes'),'escape') FROM mwa.service_endpoint";
			final String updateSQL = "UPDATE mwa.service_endpoint SET service_endpoint_password=? WHERE service_endpoint_id=?";
			final String checkSQL = "select column_name from  information_schema.columns where table_name='service_endpoint' and column_name='service_endpoint_password_bak'";
			
			final Statement stmt0 = conn.createStatement();
			ResultSet rs0 = stmt0.executeQuery(checkSQL);
			if ( rs0.next() ) {
				final Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(selectSQL);
				while (rs.next()) {
					int colNum = 1;
					String id = rs.getString(colNum++);
					String pass = rs.getString(colNum++);
	
					if (pass != null) { 
						conn.setAutoCommit(false);
						final PreparedStatement ps = conn.prepareStatement(updateSQL);
						colNum = 1;
						
						ps.setBytes(colNum++, CipherUtils.encrypto(pass, PreDefine.DEFAULT_MAGIC_NUM));
						ps.setObject(colNum++, UUID.fromString(id));
						ps.executeUpdate();
						conn.commit();
					}
				}
				rs.close();
			}
			rs0.close();
		} catch (DatabaseException | SQLException e) {
			logger.error("Data migration failure (ConvertEndpointDataEncryption).", e);
			throw new RuntimeException(e);
		}
	}

}
