package com.sony.pro.mwa.ws.rest.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiParam;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.utils.Converter;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;

@Api(value = "activities", 
	description = "Activity Template resource. Activity template is based on activity instance creation."
			+ "It defines input/output, supported events, state transition of activity"
	)
@Controller(ActivityTemplateController.NAME+ActivityTemplateController.VERSION)
@RequestMapping(value=ActivityTemplateController.PATH)
public class ActivityTemplateController  {
	static final String NAME = "activities";
	static final String VERSION = PreDefine.V1;
	static final String PATH = "/" + VERSION + "/" + NAME;
	
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	public ActivityTemplateController() {
		//do nothing
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{template-name}/{template-version:.+}",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> createInstance(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("template-name") String templateName, 
			@ApiParam(value = "The version number of the activity template (Currently it's being ignored).") @PathVariable("template-version") String templateVersion, 
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = false) List<KeyValueModel> parameters) {
		
		String message = "template(name=" + templateName + ", version=" + templateVersion + "), parameters=...";
		logger.info("RestProvider[operateActivity]:" + message);
		OperationResult result = null;
		
		try {
			result = CoreModules.activityManager().createInstance(templateName, templateVersion, Converter.keyValueListToMap(parameters));
		} catch (Exception e) {
			//暫定エラーハンドリング
			logger.error(e.getMessage());
			if (e instanceof MwaError) {
				throw e;
			} else {
				throw new MwaError(MWARC.SYSTEM_ERROR);
			}
		}
		
		if (result == null) {
			result = OperationResult.newInstance(MWARC.SYSTEM_ERROR, "Activity Adapter returns invalid response.");
		}

		return Converter.mapToKeyValueList(result);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/{template-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> createInstance(
			@ApiParam(value = "The calss name of the activity (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("template-id") String templateId, 
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = false) List<KeyValueModel> parameters) {
		OperationResult result = OperationResult.newInstance(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
		try {
			ActivityTemplateModel template = CoreModules.knowledgeBase().getActitvityTemplate(templateId);
			if (template == null) {
				logger.warn("createInstance: template ID not found: id=" + templateId);
				result = OperationResult.newInstance(MWARC.INVALID_INPUT_TEMPLATE_NAME);
			} else {
				return createInstance(template.getName(), template.getVersion(), parameters);
			}
		} catch (Exception e) {
			//暫定エラーハンドリング
			logger.error(e.getMessage());
			if (e instanceof MwaError) {
				throw e;
			} else {
				throw new MwaError(MWARC.SYSTEM_ERROR);
			}
		}
		return Converter.mapToKeyValueList(result);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<ActivityTemplateModel> getTemplates() {
		List<ActivityTemplateModel> result = CoreModules.knowledgeBase().getActitvityTemplates(null,null,null,null).getModels();
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{template-name}/{template-version:.+}",headers = "Accept=application/json,application/xml")
	public @ResponseBody IActivityTemplate getTemplate(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("template-name") String templateName, 
			@ApiParam(value = "The version number of the activity template (Currently it's being ignored).") @PathVariable("template-version") String templateVersion
			) {
		
		String message = "template(name=" + templateName + ", version=" + templateVersion + "), parameters=...";
		logger.info("RestProvider[operateActivity]:" + message);
		
		IActivityTemplate template = CoreModules.knowledgeBase().getActitvityTemplateService(templateName, templateVersion);
		return template;
	}

}
