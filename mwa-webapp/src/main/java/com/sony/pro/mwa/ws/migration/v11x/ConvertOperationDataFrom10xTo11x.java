package com.sony.pro.mwa.ws.migration.v11x;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.sony.pro.mwa.activity.repository.database.ActivityInstanceDaoImpl;
import com.sony.pro.mwa.activity.repository.database.query.ActivityInstanceQuery;
import com.sony.pro.mwa.activity.repository.database.query.ActivityInstanceQuery.COLUMN;
import com.sony.pro.mwa.internal.utils.InternalConverter;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeString;

import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

public class ConvertOperationDataFrom10xTo11x implements liquibase.change.custom.CustomTaskChange {

	@Override
	public String getConfirmationMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFileOpener(ResourceAccessor arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setUp() throws SetupException {
		// TODO Auto-generated method stub

	}

	@Override
	public ValidationErrors validate(Database arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void execute(Database database) throws CustomChangeException {
		// TODO Auto-generated method stub
		final JdbcConnection conn = (JdbcConnection) database.getConnection();
		try {
			conn.setAutoCommit(false);
			final String selectSQL = "SELECT activity_instance_id,activity_instance_input_details,activity_instance_output_details FROM mwa.activity_instance";
			final String updateSQL = "UPDATE mwa.activity_instance SET activity_instance_input_details=?, activity_instance_output_details=? WHERE activity_instance_id=?";
			final Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(selectSQL);
			while (rs.next()) {
				String id = rs.getString("activity_instance_id");
				SQLXML inputXml = rs.getSQLXML("activity_instance_input_details");
				SQLXML outputXml = rs.getSQLXML("activity_instance_output_details");

				boolean updateFlag = false;
				
				Map<String, Object> inMap = (inputXml != null) ? InternalConverter.xmlToMap(inputXml.getString()) : null;
				if (inMap != null) {
					for (Map.Entry<String, Object> entry : inMap.entrySet()) {
						if (entry.getValue() instanceof List) {
							inMap.put(entry.getKey(), TypeListString.instance().toString(entry.getValue()));
							updateFlag = true;
						}
					}
				}

				Map<String, Object> outMap = (outputXml != null) ? InternalConverter.xmlToMap(outputXml.getString()) : null;
				if (outMap != null) {
					for (Map.Entry<String, Object> entry : outMap.entrySet()) {
						if (entry.getValue() instanceof List) {
							outMap.put(entry.getKey(), TypeListString.instance().toString(entry.getValue()));
							updateFlag = true;
						}
					}
				}

				if (updateFlag) {
					try {
						conn.setAutoCommit(false);
						final PreparedStatement ps = conn.prepareStatement(updateSQL);
						int colNum = 1;

						if (inMap != null) {
							try {
								String encodedStr = InternalConverter.mapToXml(inMap);
								SQLXML inxml = conn.getWrappedConnection().createSQLXML();
								inxml.setString(encodedStr);
								ps.setSQLXML(colNum++, inxml);
							} catch (Exception e) {
								// logger.warn("Can't convert inputDetails: " +
								// e.getMessage());
								e.printStackTrace();
								ps.setNull(colNum++, Types.SQLXML);
							}
						} else {
							ps.setNull(colNum++, Types.SQLXML);
						}
						if (outMap != null) {
							try {
								String encodedStr = InternalConverter.mapToXml(outMap);
								SQLXML outxml = conn.getWrappedConnection().createSQLXML();
								outxml.setString(encodedStr);
								ps.setSQLXML(colNum++, outxml);
							} catch (Exception e) {
								// logger.warn("Can't convert outputDetails: " +
								// e.getMessage());
								e.printStackTrace();
								ps.setNull(colNum++, Types.SQLXML);
							}
						} else {
							ps.setNull(colNum++, Types.SQLXML);
						}
						ps.setObject(colNum++, UUID.fromString(id));
						ps.executeUpdate();
						conn.commit();
					} catch (DatabaseException | SQLException e) {
						e.printStackTrace();
					}
				}
			}
			rs.close();
		} catch (DatabaseException | SQLException e) {
			e.printStackTrace();
		}
	}

}
