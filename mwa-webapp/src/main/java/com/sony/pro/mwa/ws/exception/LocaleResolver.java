package com.sony.pro.mwa.ws.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Locale;

public class LocaleResolver extends AcceptHeaderLocaleResolver {
/*    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao dao) {
        this.userDao = dao;
    }*/

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
/*        Locale locale = null;
        Principal principal = request.getUserPrincipal();

        if (principal instanceof UsernamePasswordAuthenticationToken) {
            locale = org.springframework.util.StringUtils.parseLocaleString(this.userDao.getModel(principal.getName()).getLocale());
        }

        if (locale == null) {
            locale = super.resolveLocale(request);
        }

        return locale;*/
    	return Locale.getDefault();
    }
}
