package com.sony.pro.mwa.ws.rest.v2;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.activity.ActivityTemplateCollection;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.utils.Converter;
import com.sony.pro.mwa.ws.definite.PreDefine;
import com.sony.pro.mwa.ws.internal.CoreModules;

@Path(ActivityTemplateController.PATH)
@Api(value = "activities", 
	description = "Activity Template resource. Activity template is based on activity instance creation."
			+ "It defines input/output, supported events, state transition of activity"
	)
@Controller(ActivityTemplateController.NAME+ActivityTemplateController.VERSION)
@RequestMapping(value=ActivityTemplateController.PATH)
public class ActivityTemplateController  {
	static final String NAME = "activities";
	static final String VERSION = PreDefine.V2;
	static final String PATH = "/" + VERSION + "/" + NAME;
	
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	public ActivityTemplateController() {
		//do nothing
	}

	//@Override
	@POST
	@Path(value="")
	@ApiOperation(value = "Create activity instance from template (by using template name and version).", response=KeyValueModel.class, responseContainer="List")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "/{template-name}/{template-version:.+}",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> createInstance(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("template-name") String templateName, 
			@ApiParam(value = "The version number of the activity template (Currently it's being ignored).") @PathVariable("template-version") String templateVersion, 
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = false) List<KeyValueModel> parameters) {
		
		String message = "template(name=" + templateName + ", version=" + templateVersion + "), parameters=...";
		logger.info("RestProvider[operateActivity]:" + message);
		OperationResult result = null;
		
		try {
			result = CoreModules.activityManager().createInstance(templateName, templateVersion, Converter.keyValueListToMap(parameters));
		} catch (Exception e) {
			//暫定エラーハンドリング
			logger.error(e.getMessage(), e);
			if (e instanceof MwaError) {
				throw e;
			} else {
				throw new MwaError(MWARC.SYSTEM_ERROR);
			}
		}
		
		if (result == null) {
			result = OperationResult.newInstance(MWARC.SYSTEM_ERROR, "Activity Adapter returns invalid response.");
		}

		return Converter.mapToKeyValueList(result);
	}
	
	@POST
	@Path(value="")
	@ApiOperation(value = "Create activity instance from template (by using template id).", response=KeyValueModel.class, responseContainer="List")
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.POST, value = "/{template-id}",headers = "Accept=application/json,application/xml")
	public @ResponseBody List<KeyValueModel> createInstance(
			@ApiParam(value = "The calss name of the activity (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("template-id") String templateId, 
			@ApiParam(value = "The input key-value parameters.") @RequestBody(required = false) List<KeyValueModel> parameters) {
		OperationResult result = OperationResult.newInstance(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
		try {
			ActivityTemplateModel template = CoreModules.knowledgeBase().getActitvityTemplate(templateId);
			if (template == null) {
				logger.warn("createInstance: template ID not found: id=" + templateId);
				result = OperationResult.newInstance(MWARC.INVALID_INPUT_TEMPLATE_NAME);
			} else {
				return createInstance(template.getName(), template.getVersion(), parameters);
			}
		} catch (Exception e) {
			//暫定エラーハンドリング
			logger.error(e.getMessage());
			if (e instanceof MwaError) {
				throw e;
			} else {
				throw new MwaError(MWARC.SYSTEM_ERROR);
			}
		}
		return Converter.mapToKeyValueList(result);
	}
	
	@GET
	@Path(value="")
	@ApiOperation(value = "Get appropriate activity templates. Please see detailed information about search. " + PreDefine.SEARCH_URL + " ", response=ActivityTemplateCollection.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "",headers = "Accept=application/json,application/xml")
	public @ResponseBody ActivityTemplateCollection getTemplates(
		    @ApiParam( name="sort", value = "The sort of the plugin.") @RequestParam(value = "sort", required = false) List<String> sorts,
		    @ApiParam(value = "The filter of the instances.", allowMultiple = true) @RequestParam(value = "filter", required = false) List<String> filters,
		    @ApiParam(value = "The start number of the instances.") @RequestParam(value = "offset", required = false) Integer offset,
		    @ApiParam(value = "The maximum number of the instances.") @RequestParam(value = "limit", required = false) Integer limit
			) {
		ActivityTemplateCollection result = CoreModules.knowledgeBase().getActitvityTemplates(sorts, filters, offset, limit);
		return result;
	}
	
	@GET
	@Path(value="/{template-name}/{template-version:.+}")
	@ApiOperation(value = "Get detailed information of specified activity template.", response=IActivityTemplate.class)
	@ApiResponses(value = {
					@ApiResponse(code = 500, message = "Application errors, please see details of MWARC")
				})
	@RequestMapping(method = RequestMethod.GET, value = "/{template-name}/{template-version:.+}",headers = "Accept=application/json,application/xml")
	public @ResponseBody IActivityTemplate getTemplate(
			@ApiParam(value = "The name of the activity template (e.g. \"com.sony.pro.mwa.activity.sample.SampleTask\").") @PathVariable("template-name") String templateName, 
			@ApiParam(value = "The version number of the activity template (Currently it's being ignored).") @PathVariable("template-version") String templateVersion
			) {
		
		String message = "template(name=" + templateName + ", version=" + templateVersion + "), parameters=...";
		logger.info("RestProvider[operateActivity]:" + message);
		
		IActivityTemplate template = CoreModules.knowledgeBase().getActitvityTemplateService(templateName, templateVersion);
		return template;
	}

}
