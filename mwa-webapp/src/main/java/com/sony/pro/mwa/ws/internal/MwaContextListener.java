package com.sony.pro.mwa.ws.internal;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.sony.pro.mwa.common.log.MwaLogger;

public class MwaContextListener implements ServletContextListener {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	@Override
	public void contextInitialized(ServletContextEvent event) {
		//InitialContectのFactory設定を上書きするため
		//System.setProperty("java.naming.factory.initial", "com.sony.pro.mwa.ws.bitronix.BitronixInitialContextFactoryWrapper");
		//BitronixによるTransaction系のtlog出力を止めるために指定（出力先によってはアクセスエラーになる可能性があるため）
		System.setProperty("bitronix.tm.journal", "null");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
/*		ProcessEngineImpl.getInstance().destroy();
		ActivityManagerImpl.getInstance().destroy();*/
		logger.info("mwa-core: terminated!!");
	}
}