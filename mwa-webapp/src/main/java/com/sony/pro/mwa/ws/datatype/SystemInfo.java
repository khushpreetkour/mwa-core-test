package com.sony.pro.mwa.ws.datatype;

import java.util.List;

import com.sony.pro.mwa.model.KeyValueModel;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "SystemInfo", description = "System Information Model")
public class SystemInfo {

	protected List<KeyValueModel> versions;

    @ApiModelProperty(value = "versions of the system")
	public List<KeyValueModel> getVersions() {
		return versions;
	}

	public void setVersions(List<KeyValueModel> versions) {
		this.versions = versions;
	}
}
