DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_local_perspective' and column_name='location_perspective_relative_path') then
    ALTER TABLE mwa.location_local_perspective RENAME location_perspective_relative_path TO location_perspective_base_path;
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='service_endpoint' and column_name='service_endpoint_password' and data_type = 'character varying') then
    ALTER TABLE mwa.service_endpoint RENAME service_endpoint_password TO service_endpoint_password_bak;
  end if;
END$$;

DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='service_endpoint' and column_name='service_endpoint_password') then
    ALTER TABLE mwa.service_endpoint ADD COLUMN service_endpoint_password bytea;
  end if;
END$$;
