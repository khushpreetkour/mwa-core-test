DROP TABLE IF EXISTS quartz.qrtz_fired_triggers;
DROP TABLE IF EXISTS quartz.qrtz_paused_trigger_grps;
DROP TABLE IF EXISTS quartz.qrtz_scheduler_state;
DROP TABLE IF EXISTS quartz.qrtz_locks;
DROP TABLE IF EXISTS quartz.qrtz_simprop_triggers;
DROP TABLE IF EXISTS quartz.qrtz_simple_triggers;
DROP TABLE IF EXISTS quartz.qrtz_cron_triggers;
DROP TABLE IF EXISTS quartz.qrtz_blob_triggers;
DROP TABLE IF EXISTS quartz.extension_qrtz_triggers;
DROP TABLE IF EXISTS quartz.qrtz_triggers;
DROP TABLE IF EXISTS quartz.qrtz_job_details;
DROP TABLE IF EXISTS quartz.qrtz_calendars;
DROP TABLE IF EXISTS quartz.extension_qrtz_sequence;
DROP TABLE IF EXISTS quartz.extension_qrtz_result_triggers;

drop index IF EXISTS idx_mwa_qrtz_j_req_recovery;
drop index IF EXISTS idx_mwa_qrtz_t_next_fire_time;
drop index IF EXISTS idx_mwa_qrtz_t_state;
drop index IF EXISTS idx_mwa_qrtz_t_nft_st;
drop index IF EXISTS idx_mwa_qrtz_ft_trig_name;
drop index IF EXISTS idx_mwa_qrtz_ft_trig_group;
drop index IF EXISTS idx_mwa_qrtz_ft_trig_nm_gp;
drop index IF EXISTS idx_mwa_qrtz_ft_trig_inst_name;
drop index IF EXISTS idx_mwa_qrtz_ft_job_name;
drop index IF EXISTS idx_mwa_qrtz_ft_job_group;
drop index IF EXISTS idx_mwa_qrtz_ft_job_req_recovery;


CREATE TABLE quartz.qrtz_job_details
  (
    sched_name VARCHAR(100) NOT NULL,
	job_name  VARCHAR(200) NOT NULL,
    job_group VARCHAR(200) NOT NULL,
    description VARCHAR(250) NULL,
    job_class_name   VARCHAR(250) NOT NULL, 
    is_durable BOOL NOT NULL,
    is_nonconcurrent BOOL NOT NULL,
    is_update_data BOOL NOT NULL,
	requests_recovery BOOL NOT NULL,
    job_data BYTEA NULL,
    PRIMARY KEY (sched_name,job_name,job_group)
);

CREATE TABLE quartz.qrtz_triggers
  (
    sched_name VARCHAR(100) NOT NULL,
	trigger_name VARCHAR(150) NOT NULL,
    trigger_group VARCHAR(150) NOT NULL,
    job_name  VARCHAR(200) NOT NULL, 
    job_group VARCHAR(200) NOT NULL,
    description VARCHAR(250) NULL,
    next_fire_time BIGINT NULL,
    prev_fire_time BIGINT NULL,
    priority INTEGER NULL,
    trigger_state VARCHAR(16) NOT NULL,
    trigger_type VARCHAR(8) NOT NULL,
    start_time BIGINT NOT NULL,
    end_time BIGINT NULL,
    calendar_name VARCHAR(200) NULL,
    misfire_instr SMALLINT NULL,
    job_data BYTEA NULL,
    PRIMARY KEY (sched_name,trigger_name,trigger_group),
    FOREIGN KEY (sched_name,job_name,job_group) 
		REFERENCES quartz.qrtz_job_details(sched_name,job_name,job_group) 
);

CREATE TABLE quartz.qrtz_simple_triggers
  (
    sched_name VARCHAR(100) NOT NULL,
	trigger_name VARCHAR(150) NOT NULL,
    trigger_group VARCHAR(150) NOT NULL,
    repeat_count BIGINT NOT NULL,
    repeat_interval BIGINT NOT NULL,
    times_triggered BIGINT NOT NULL,
    PRIMARY KEY (sched_name,trigger_name,trigger_group),
    FOREIGN KEY (sched_name,trigger_name,trigger_group) 
		REFERENCES quartz.qrtz_triggers(sched_name,trigger_name,trigger_group) ON DELETE CASCADE
);

CREATE TABLE quartz.qrtz_SIMPROP_TRIGGERS 
  (
    sched_name VARCHAR (100) NOT NULL,
    trigger_name VARCHAR (150) NOT NULL ,
    trigger_group VARCHAR (150) NOT NULL ,
    str_prop_1 VARCHAR (512) NULL,
    str_prop_2 VARCHAR (512) NULL,
    str_prop_3 VARCHAR (512) NULL,
    int_prop_1 INTEGER NULL,
    int_prop_2 INTEGER NULL,
    long_prop_1 BIGINT NULL,
    long_prop_2 BIGINT NULL,
    dec_prop_1 NUMERIC NULL,
    dec_prop_2 NUMERIC NULL,
    bool_prop_1 BOOL NULL,
    bool_prop_2 BOOL NULL,
	PRIMARY KEY (sched_name,trigger_name,trigger_group),
    FOREIGN KEY (sched_name,trigger_name,trigger_group) 
		REFERENCES quartz.qrtz_triggers(sched_name,trigger_name,trigger_group) ON DELETE CASCADE
);

CREATE TABLE quartz.qrtz_cron_triggers
  (
    sched_name VARCHAR (100) NOT NULL,
    trigger_name VARCHAR(150) NOT NULL,
    trigger_group VARCHAR(150) NOT NULL,
    cron_expression VARCHAR(250) NOT NULL,
    time_zone_id VARCHAR(80),
    PRIMARY KEY (sched_name,trigger_name,trigger_group),
    FOREIGN KEY (sched_name,trigger_name,trigger_group) 
		REFERENCES quartz.qrtz_triggers(sched_name,trigger_name,trigger_group) ON DELETE CASCADE
);

CREATE TABLE quartz.qrtz_blob_triggers
  (
    sched_name VARCHAR (100) NOT NULL,
    trigger_name VARCHAR(150) NOT NULL,
    trigger_group VARCHAR(150) NOT NULL,
    blob_data BYTEA NULL,
    PRIMARY KEY (sched_name,trigger_name,trigger_group),
    FOREIGN KEY (sched_name,trigger_name,trigger_group) 
		REFERENCES quartz.qrtz_triggers(sched_name,trigger_name,trigger_group) ON DELETE CASCADE
);

CREATE TABLE quartz.qrtz_calendars
  (
    sched_name VARCHAR (100) NOT NULL,
    calendar_name  VARCHAR(200) NOT NULL, 
    calendar BYTEA NOT NULL,
    PRIMARY KEY (sched_name,calendar_name)
);

CREATE TABLE quartz.qrtz_paused_trigger_grps
  (
    sched_name VARCHAR (100) NOT NULL,
    trigger_group VARCHAR(150) NOT NULL, 
    PRIMARY KEY (sched_name,trigger_group)
);

CREATE TABLE quartz.qrtz_fired_triggers 
  (
    sched_name VARCHAR (100) NOT NULL,
    entry_id VARCHAR(95) NOT NULL,
    trigger_name VARCHAR(150) NOT NULL,
    trigger_group VARCHAR(150) NOT NULL,
    instance_name VARCHAR(200) NOT NULL,
    fired_time BIGINT NOT NULL,
	sched_time BIGINT NOT NULL,
    priority INTEGER NOT NULL,
    state VARCHAR(16) NOT NULL,
    job_name VARCHAR(200) NULL,
    job_group VARCHAR(200) NULL,
    is_nonconcurrent BOOL NOT NULL,
    requests_recovery BOOL NULL,
    PRIMARY KEY (sched_name,entry_id)
);

CREATE TABLE quartz.qrtz_scheduler_state 
  (
    sched_name VARCHAR (100) NOT NULL,
    instance_name VARCHAR(200) NOT NULL,
    last_checkin_time BIGINT NOT NULL,
    checkin_interval BIGINT NOT NULL,
    PRIMARY KEY (sched_name,instance_name)
);

CREATE TABLE quartz.qrtz_locks
  (
    sched_name VARCHAR (100) NOT NULL,
    lock_name  VARCHAR(40) NOT NULL, 
    PRIMARY KEY (sched_name,lock_name)
);

CREATE TABLE quartz.extension_qrtz_triggers
  (
    sched_id VARCHAR(36) NOT NULL,
    sched_name VARCHAR(100) NOT NULL,
    trigger_name VARCHAR(150) NOT NULL,
    trigger_group VARCHAR(150) NOT NULL,
	trigger_type VARCHAR(8) NOT NULL,
	http_method_type VARCHAR(6) NOT NULL,
	endpoint text NOT NULL,
	parameter text,
	description  VARCHAR(256),
	created_time VARCHAR(30),
	updated_time VARCHAR(30), 
    PRIMARY KEY (sched_id),
    FOREIGN KEY (sched_name,trigger_name,trigger_group) 
		REFERENCES quartz.qrtz_triggers(sched_name,trigger_name,trigger_group) on update no action on delete cascade
);

CREATE TABLE quartz.extension_qrtz_sequence
  (
    seq_name VARCHAR(100) NOT NULL,
    seq_no BIGINT NOT NULL,
    seq_description VARCHAR(256) ,
    updated_time VARCHAR(30), 
    PRIMARY KEY (seq_name)
);

CREATE TABLE quartz.extension_qrtz_result_triggers
  (
    id VARCHAR(36) NOT NULL,
    sched_id VARCHAR(36) NOT NULL,
    trigger_job_status VARCHAR(10) NOT NULL,
    result text,
	created_time VARCHAR(30),
    PRIMARY KEY (id)
);


INSERT INTO quartz.extension_qrtz_sequence (seq_name, seq_no, seq_description, updated_time) 
values ('trigger_seq',0,'schedulerのシーケンス。JobName、TrigerNameの登録に利用','0000-00-00 00:00:00.000');

create index idx_mwa_qrtz_j_req_recovery on quartz.qrtz_job_details(requests_recovery);
create index idx_mwa_qrtz_t_next_fire_time on quartz.qrtz_triggers(next_fire_time);
create index idx_mwa_qrtz_t_state on quartz.qrtz_triggers(trigger_state);
create index idx_mwa_qrtz_t_nft_st on quartz.qrtz_triggers(next_fire_time,trigger_state);
create index idx_mwa_qrtz_ft_trig_name on quartz.qrtz_fired_triggers(trigger_name);
create index idx_mwa_qrtz_ft_trig_group on quartz.qrtz_fired_triggers(trigger_group);
create index idx_mwa_qrtz_ft_trig_nm_gp on quartz.qrtz_fired_triggers(sched_name,trigger_name,trigger_group);
create index idx_mwa_qrtz_ft_trig_inst_name on quartz.qrtz_fired_triggers(instance_name);
create index idx_mwa_qrtz_ft_job_name on quartz.qrtz_fired_triggers(job_name);
create index idx_mwa_qrtz_ft_job_group on quartz.qrtz_fired_triggers(job_group);
create index idx_mwa_qrtz_ft_job_req_recovery on quartz.qrtz_fired_triggers(requests_recovery);

ALTER TABLE quartz.qrtz_fired_triggers OWNER TO quartz;
ALTER TABLE quartz.qrtz_paused_trigger_grps OWNER TO quartz;
ALTER TABLE quartz.qrtz_scheduler_state OWNER TO quartz;
ALTER TABLE quartz.qrtz_locks OWNER TO quartz;
ALTER TABLE quartz.qrtz_simprop_triggers OWNER TO quartz;
ALTER TABLE quartz.qrtz_simple_triggers OWNER TO quartz;
ALTER TABLE quartz.qrtz_cron_triggers OWNER TO quartz;
ALTER TABLE quartz.qrtz_blob_triggers OWNER TO quartz;
ALTER TABLE quartz.qrtz_triggers OWNER TO quartz;
ALTER TABLE quartz.qrtz_job_details OWNER TO quartz;
ALTER TABLE quartz.qrtz_calendars OWNER TO quartz;
ALTER TABLE quartz.extension_qrtz_triggers OWNER TO quartz;
ALTER TABLE quartz.extension_qrtz_sequence OWNER TO quartz;
ALTER TABLE quartz.extension_qrtz_result_triggers OWNER TO quartz;

