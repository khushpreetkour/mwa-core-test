DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='service_endpoint' and column_name='service_endpoint_password_bak') then
    ALTER TABLE mwa.service_endpoint DROP service_endpoint_password_bak;
  end if;
END$$;
