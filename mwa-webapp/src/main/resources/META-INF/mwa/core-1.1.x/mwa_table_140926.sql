CREATE TABLE IF NOT EXISTS mwa.extension
(
  extension_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  extension_type character varying(256) NOT NULL,
  extension_name character varying(256) NOT NULL,
  extension_version character varying(256) NOT NULL,
  extension_created_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  constraint "uq_extension_name_and_version" UNIQUE (extension_name, extension_version)
);

CREATE TABLE IF NOT EXISTS mwa.extension_property
(
  extension_id uuid NOT NULL,
    foreign key (extension_id) references mwa.extension(extension_id) match simple on update no action on delete cascade,
  extension_property_name character varying(64) NOT NULL,
  extension_property_value character varying(256) NOT NULL,
  primary key (extension_id, extension_property_name)
);

CREATE TABLE IF NOT EXISTS mwa.extension_content
(
  extension_id uuid NOT NULL,
    foreign key (extension_id) references mwa.extension(extension_id) match simple on update no action on delete cascade,
  extension_content_type character varying(256) NOT NULL,
  extension_content_id uuid NOT NULL,
  primary key (extension_id, extension_content_id)
);


DO $$ BEGIN
  if exists (SELECT 1 FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE n.nspname = 'mwa' AND c.relname = 'plugin' AND c.relkind = 'r') then
    insert into mwa.extension (extension_id, extension_type, extension_name, extension_version, extension_created_time)
    select plugin_id, 'BUNDLE', plugin_name, plugin_version, plugin_created_time from mwa.plugin;
    insert into mwa.extension_property(extension_id, extension_property_name, extension_property_value)
    select plugin_id, 'BUNDLE_ID', plugin_bundle_id from mwa.plugin;
  end if;
END$$;

DO $$ BEGIN
  if exists (SELECT 1 FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE n.nspname = 'mwa' AND c.relname = 'workflow' AND c.relkind = 'r') then
    insert into mwa.extension (extension_id, extension_type, extension_name, extension_version, extension_created_time)
    select workflow_id, 'BPMN', workflow_name, workflow_version, workflow_created_time from mwa.workflow;
  end if;
END$$;

DO $$ BEGIN
  if exists (SELECT 1 FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE n.nspname = 'mwa' AND c.relname = 'map_template_workflow' AND c.relkind = 'r') then
    insert into mwa.extension_content (extension_id, extension_content_type, extension_content_id)
    select workflow_id, 'TEMPLETE', activity_template_id from mwa.map_template_workflow;
  end if;
END$$;

DO $$ BEGIN
  if exists (SELECT 1 FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE n.nspname = 'mwa' AND c.relname = 'map_template_plugin' AND c.relkind = 'r') then
    insert into mwa.extension_content (extension_id, extension_content_type, extension_content_id)
    select plugin_id, 'TEMPLETE', activity_template_id from mwa.map_template_plugin;
  end if;
END$$;

drop table if exists mwa.activity_provider_property_dictionary cascade;
drop table if exists mwa.service_endpoint_type cascade;
drop table if exists mwa.map_provider_type_template cascade;
drop table if exists mwa.map_template_plugin cascade;
drop table if exists mwa.map_template_workflow cascade;
drop table if exists mwa.plugin cascade;
drop table if exists mwa.workflow cascade;
