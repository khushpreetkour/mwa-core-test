
CREATE TABLE IF NOT EXISTS mwa.resource_request
(
  resource_request_id uuid NOT NULL primary key,
  resource_request_priority integer,
  resource_request_occupant_id uuid NOT NULL unique,
  resource_request_occupant_name character varying(256) NOT NULL,
  resource_request_receiver_id uuid,
  resource_request_cost_type character varying(256) NOT NULL,
  resource_request_cost_values text NOT NULL,
  resource_request_resource_id uuid,
  resource_request_create_time timestamp with time zone
);

CREATE TABLE IF NOT EXISTS mwa.resource_occupation
(
  resource_request_id uuid NOT NULL primary key references mwa.resource_request(resource_request_id) match simple on update no action on delete cascade,
  assigned_resource_id uuid NOT NULL references mwa.resource(resource_id) match simple on update no action on delete cascade,
  exclusion_id character varying(256),
  update_time timestamp with time zone
);

CREATE TABLE IF NOT EXISTS mwa.event
(
  event_id uuid NOT NULL primary key,
  event_name character varying(256) NOT NULL,
  event_params text,
  event_target_id uuid NOT NULL,
  event_target_type character varying(256) NOT NULL,
  event_create_time timestamp with time zone,
  event_exclusion_id character varying(256)
);
