CREATE TABLE IF NOT EXISTS mwa.location
(
  location_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  location_name character varying(256) NOT NULL,
  constraint "uq_location_name" UNIQUE (location_name)
);

CREATE TABLE IF NOT EXISTS mwa.location_perspective
(
  location_id uuid NOT NULL,
    foreign key (location_id) references mwa.location(location_id) match simple on update no action on delete cascade,
  location_perspective_name character varying(256) NOT NULL,
  location_perspective_base_path character varying(256),
  location_perspective_user character varying(256),
  location_perspective_password character varying(256),
  primary key (location_id, location_perspective_name)
);

CREATE TABLE IF NOT EXISTS mwa.location_local_perspective
(
  activity_provider_id uuid NOT NULL,
    foreign key (activity_provider_id) references mwa.activity_provider(activity_provider_id) match simple on update no action on delete cascade,
  location_id uuid NOT NULL,
    foreign key (location_id) references mwa.location(location_id) match simple on update no action on delete cascade,
  location_perspective_name character varying(64) NOT NULL,
  location_perspective_relative_path character varying(256) NOT NULL,
  primary key (activity_provider_id, location_id, location_perspective_name)
);

CREATE TABLE IF NOT EXISTS mwa.location_perspective_property
(
  location_id uuid NOT NULL,
    foreign key (location_id) references mwa.location(location_id) match simple on update no action on delete cascade,
  location_perspective_name character varying(64) NOT NULL,
  location_perspective_property_name character varying(64) NOT NULL,
  location_perspective_property_value character varying(256) NOT NULL,
  primary key (location_id, location_perspective_name, location_perspective_property_name)
);
