INSERT INTO mwa.activity_template (activity_template_id, activity_template_name, activity_template_version, activity_template_type_id)
 SELECT '0ec82e94-13ed-75da-d8e7-470acebc009f', 'com.sony.pro.mwa.activity.ht.approval.ClipReviewHumanTask', '2', 2
 WHERE NOT EXISTS (SELECT activity_template_id FROM mwa.activity_template WHERE activity_template_id = '0ec82e94-13ed-75da-d8e7-470acebc009f');

INSERT INTO mwa.activity_template (activity_template_id, activity_template_name, activity_template_version, activity_template_type_id)
 SELECT '64858626-ee27-ecef-3cfc-e3e0f7987d81', 'com.sony.pro.mwa.activity.tds.DeleteTask', '2', 2
 WHERE NOT EXISTS (SELECT activity_template_id FROM mwa.activity_template WHERE activity_template_id = '64858626-ee27-ecef-3cfc-e3e0f7987d81');

INSERT INTO mwa.activity_template (activity_template_id, activity_template_name, activity_template_version, activity_template_type_id)
 SELECT 'a374bd4a-916b-d5f1-492f-b26731507b0c', 'com.sony.pro.mwa.activity.tds.TdsExportTask', '1', 2
 WHERE NOT EXISTS (SELECT activity_template_id FROM mwa.activity_template WHERE activity_template_id = 'a374bd4a-916b-d5f1-492f-b26731507b0c');

INSERT INTO mwa.activity_template (activity_template_id, activity_template_name, activity_template_version, activity_template_type_id)
 SELECT '5e59fa68-c1d3-b7d9-4459-412b8e44a975', 'com.sony.pro.mwa.activity.tds.TransferTask', '2', 2
 WHERE NOT EXISTS (SELECT activity_template_id FROM mwa.activity_template WHERE activity_template_id = '5e59fa68-c1d3-b7d9-4459-412b8e44a975');

INSERT INTO mwa.activity_template (activity_template_id, activity_template_name, activity_template_version, activity_template_type_id)
 SELECT 'cf13dd4c-39bd-9310-15e1-71a92e7d3b3c', 'com.sony.pro.mwa.activity.vivo.VivoIngestTask', '2', 2
 WHERE NOT EXISTS (SELECT activity_template_id FROM mwa.activity_template WHERE activity_template_id = 'cf13dd4c-39bd-9310-15e1-71a92e7d3b3c');

INSERT INTO mwa.activity_template (activity_template_id, activity_template_name, activity_template_version, activity_template_type_id)
 SELECT 'cd8a5a03-0e9e-86ad-bf44-576ec1a7bb22', 'com.sony.pro.mwa.activity.vivo.VivoQcTask', '2', 2
 WHERE NOT EXISTS (SELECT activity_template_id FROM mwa.activity_template WHERE activity_template_id = 'cd8a5a03-0e9e-86ad-bf44-576ec1a7bb22');

UPDATE mwa.activity_instance SET activity_template_id='0ec82e94-13ed-75da-d8e7-470acebc009f' WHERE activity_template_id='36323dbd-bde5-44f2-a24b-34481af10371';
UPDATE mwa.activity_instance SET activity_template_id='64858626-ee27-ecef-3cfc-e3e0f7987d81' WHERE activity_template_id='743fbe43-41e7-4f7d-ad88-763bba3da38f';
UPDATE mwa.activity_instance SET activity_template_id='a374bd4a-916b-d5f1-492f-b26731507b0c' WHERE activity_template_id='8ad19d75-27e4-4399-9307-a67e161561a5';
UPDATE mwa.activity_instance SET activity_template_id='5e59fa68-c1d3-b7d9-4459-412b8e44a975' WHERE activity_template_id='4511d771-7d33-4591-a800-546d4d7a712b';
UPDATE mwa.activity_instance SET activity_template_id='cf13dd4c-39bd-9310-15e1-71a92e7d3b3c' WHERE activity_template_id='f7c03856-7b8d-41e8-b507-d9fc6b463b9e';
UPDATE mwa.activity_instance SET activity_template_id='cd8a5a03-0e9e-86ad-bf44-576ec1a7bb22' WHERE activity_template_id='8cb1ad30-a0da-4e00-9768-ef96128af60e';

DELETE FROM mwa.activity_instance WHERE activity_template_id='5bf2852d-2ce9-494e-bb4a-d01b02434242';
DELETE FROM mwa.activity_template WHERE activity_template_id='5bf2852d-2ce9-494e-bb4a-d01b02434242';
DELETE FROM mwa.activity_template WHERE activity_template_id='36323dbd-bde5-44f2-a24b-34481af10371';
DELETE FROM mwa.activity_template WHERE activity_template_id='743fbe43-41e7-4f7d-ad88-763bba3da38f';
DELETE FROM mwa.activity_template WHERE activity_template_id='8ad19d75-27e4-4399-9307-a67e161561a5';
DELETE FROM mwa.activity_template WHERE activity_template_id='4511d771-7d33-4591-a800-546d4d7a712b';
DELETE FROM mwa.activity_template WHERE activity_template_id='f7c03856-7b8d-41e8-b507-d9fc6b463b9e';
DELETE FROM mwa.activity_template WHERE activity_template_id='8cb1ad30-a0da-4e00-9768-ef96128af60e';
DELETE FROM mwa.activity_provider_type WHERE activity_provider_type_name = 'IDENTIFICATION' AND activity_provider_type_model_number = '0001';
