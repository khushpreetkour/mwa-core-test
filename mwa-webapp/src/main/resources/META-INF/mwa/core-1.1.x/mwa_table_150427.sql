CREATE TABLE IF NOT EXISTS mwa.resource
(
  resource_id uuid NOT NULL primary key,
  resource_type character varying(256) NOT NULL,
  resource_name character varying(256) NOT NULL,
  resource_values text,
  holder character varying(256),
  holder_type character varying(256)
);
