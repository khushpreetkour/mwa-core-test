DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_provider' and column_name='activity_provider_create_time') then
    ALTER TABLE mwa.activity_provider ADD COLUMN activity_provider_create_time timestamp with time zone;
    ALTER TABLE mwa.activity_provider ADD COLUMN activity_provider_update_time timestamp with time zone;
  end if;
END$$;
DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_provider_type' and column_name='activity_provider_type_create_time') then
    ALTER TABLE mwa.activity_provider_type ADD COLUMN activity_provider_type_create_time timestamp with time zone;
    ALTER TABLE mwa.activity_provider_type ADD COLUMN activity_provider_type_update_time timestamp with time zone;
  end if;
END$$;
DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location' and column_name='location_create_time') then
    ALTER TABLE mwa.location ADD COLUMN location_create_time timestamp with time zone;
    ALTER TABLE mwa.location ADD COLUMN location_update_time timestamp with time zone;
  end if;
END$$;
DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='resource' and column_name='resource_create_time') then
    ALTER TABLE mwa.resource ADD COLUMN resource_create_time timestamp with time zone;
    ALTER TABLE mwa.resource ADD COLUMN resource_update_time timestamp with time zone;
  end if;
END$$;
