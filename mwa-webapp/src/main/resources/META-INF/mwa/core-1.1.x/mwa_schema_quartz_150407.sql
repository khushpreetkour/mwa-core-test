    do
    $do$
    begin

    if exists (SELECT 1 FROM pg_shadow WHERE usename = 'quartz') then
        raise notice 'usename "quartz" already exists';
    else
         CREATE USER quartz WITH PASSWORD 'quartz';
    end if;
    if exists (select 1 from information_schema.schemata where schema_name = 'quartz') then
        raise notice 'schema "quartz" already exists';
    else
        CREATE SCHEMA quartz AUTHORIZATION quartz;
    end if;
    end
    $do$
