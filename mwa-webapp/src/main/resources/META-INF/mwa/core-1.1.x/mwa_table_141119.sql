DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_provider_type' and column_name='activity_provider_type_name') then
    ALTER TABLE mwa.activity_provider_type ALTER COLUMN activity_provider_type_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_template_type' and column_name='activity_template_type_name') then
    ALTER TABLE mwa.activity_template_type ALTER COLUMN activity_template_type_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='extension_property' and column_name='extension_property_name') then
    ALTER TABLE mwa.extension_property ALTER COLUMN extension_property_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='extension_property' and column_name='extension_property_value') then
    ALTER TABLE mwa.extension_property ALTER COLUMN extension_property_value TYPE character varying(1024);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_provider_property' and column_name='activity_provider_property_name') then
    ALTER TABLE mwa.activity_provider_property ALTER COLUMN activity_provider_property_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_provider_property' and column_name='activity_provider_property_value') then
    ALTER TABLE mwa.activity_provider_property ALTER COLUMN activity_provider_property_value TYPE character varying(1024);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_local_perspective' and column_name='location_perspective_name') then
    ALTER TABLE mwa.location_local_perspective ALTER COLUMN location_perspective_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_local_perspective' and column_name='location_perspective_base_path') then
    ALTER TABLE mwa.location_local_perspective ALTER COLUMN location_perspective_base_path TYPE character varying(1024);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_perspective' and column_name='location_perspective_name') then
    ALTER TABLE mwa.location_perspective ALTER COLUMN location_perspective_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_perspective' and column_name='location_perspective_base_path') then
    ALTER TABLE mwa.location_perspective ALTER COLUMN location_perspective_base_path TYPE character varying(1024);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_perspective_property' and column_name='location_perspective_name') then
    ALTER TABLE mwa.location_perspective_property ALTER COLUMN location_perspective_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_perspective_property' and column_name='location_perspective_property_name') then
    ALTER TABLE mwa.location_perspective_property ALTER COLUMN location_perspective_property_name TYPE character varying(256);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='location_perspective_property' and column_name='location_perspective_property_value') then
    ALTER TABLE mwa.location_perspective_property ALTER COLUMN location_perspective_property_value TYPE character varying(1024);
  end if;
END$$;

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_template' and column_name='activity_template_category') then
    ALTER TABLE mwa.activity_template DROP COLUMN activity_template_category;
  end if;
END$$;
