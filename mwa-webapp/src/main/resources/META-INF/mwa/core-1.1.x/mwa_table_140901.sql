
drop table if exists mwa.errorinfo cascade;
drop table if exists mwa.requestinfo cascade;
drop sequence if exists mwa.error_info_id_seq;
drop sequence if exists mwa.request_info_id_seq;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS mwa.activity_provider_type
(
  activity_provider_type_id uuid NOT NULL primary key,
  activity_provider_type_name character varying(64) NOT NULL,
  activity_provider_type_model_number character varying(64) NOT NULL,
  constraint "uq_activity_provider_type" UNIQUE (activity_provider_type_name, activity_provider_type_model_number)
);

CREATE TABLE IF NOT EXISTS mwa.service_endpoint_type
(
  service_endpoint_type_id uuid NOT NULL primary key,
  service_endpoint_type_name character varying(256) NOT NULL,
  activity_provider_type_id uuid NOT NULL,
    foreign key (activity_provider_type_id) references mwa.activity_provider_type(activity_provider_type_id) match simple on update no action on delete cascade,
  constraint "uq_service_endpoint_type" UNIQUE (service_endpoint_type_name, activity_provider_type_id)
);

CREATE TABLE IF NOT EXISTS mwa.map_provider_type_template
(
  activity_provider_type_id uuid NOT NULL,
    foreign key (activity_provider_type_id) references mwa.activity_provider_type(activity_provider_type_id) match simple on update no action on delete cascade,
  activity_template_name character varying(256) NOT NULL,
  activity_template_version character varying(64) NOT NULL,
  primary key(activity_provider_type_id, activity_template_name, activity_template_version)
);

CREATE TABLE IF NOT EXISTS mwa.activity_provider
(
  activity_provider_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_provider_type_id uuid NOT NULL,
    foreign key (activity_provider_type_id) references mwa.activity_provider_type(activity_provider_type_id) match simple on update no action on delete cascade,
  activity_provider_name character varying(256) NOT NULL,
  activity_provider_active boolean,
  activity_provider_exposure boolean,
  constraint "uq_provider_name" UNIQUE (activity_provider_name)
);

CREATE TABLE IF NOT EXISTS mwa.activity_provider_property
(
  activity_provider_id uuid NOT NULL,
    foreign key (activity_provider_id) references mwa.activity_provider(activity_provider_id) match simple on update no action on delete cascade,
  activity_provider_property_name character varying(64) NOT NULL,
  activity_provider_property_value character varying(256) NOT NULL,
  constraint "uq_provider_property" UNIQUE (activity_provider_id, activity_provider_property_name)
);

CREATE TABLE IF NOT EXISTS mwa.activity_provider_property_dictionary
(
  activity_provider_property_name character varying(64) NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_provider_property_type character varying(64) NOT NULL,
  activity_provider_property_category character varying(64) NOT NULL
);

CREATE TABLE IF NOT EXISTS mwa.service_endpoint
(
  service_endpoint_id uuid NOT NULL primary key,
  service_endpoint_provider_id uuid NOT NULL,
    foreign key (service_endpoint_provider_id) references mwa.activity_provider(activity_provider_id) match simple on update no action on delete cascade,
  service_endpoint_type character varying(256) NOT NULL,
  service_endpoint_host character varying(256) NOT NULL,
  service_endpoint_port integer,
  service_endpoint_user character varying(256),
  service_endpoint_password character varying(256),
  constraint "uq_service_endpoint" UNIQUE (service_endpoint_provider_id, service_endpoint_type)
);

DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='activity_instance' and column_name='activity_instance_update_time') then
    ALTER TABLE mwa.activity_instance ADD COLUMN activity_instance_update_time timestamp with time zone;
  end if;
END$$;

CREATE TABLE IF NOT EXISTS mwa.activity_instance_persistent
(
  activity_instance_id uuid NOT NULL primary key,
    foreign key (activity_instance_id) references mwa.activity_instance(activity_instance_id) match simple on update no action on delete cascade,
  activity_instance_persistent_data text NOT NULL
);

CREATE OR REPLACE FUNCTION mwa.activity_instance_update_handler()
RETURNS trigger AS
$BODY$    BEGIN
IF TG_OP = 'INSERT' THEN
NEW.activity_instance_update_time := now();
ELSE
IF TG_OP = 'UPDATE' THEN
NEW.activity_instance_update_time := now();
END IF ;
END IF ;
RETURN NEW;
END ;
$BODY$
LANGUAGE plpgsql;
ALTER FUNCTION mwa.activity_instance_update_handler() OWNER TO postgres;

DROP TRIGGER IF EXISTS activity_insatnce_update_trigger ON mwa.activity_instance;
CREATE TRIGGER activity_insatnce_update_trigger
  BEFORE INSERT OR UPDATE
  ON mwa.activity_instance
  FOR EACH ROW
  EXECUTE PROCEDURE mwa.activity_instance_update_handler();

