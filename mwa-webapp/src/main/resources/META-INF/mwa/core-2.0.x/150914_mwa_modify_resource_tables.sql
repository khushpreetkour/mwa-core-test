-- 一旦削除します。運用的には処理中のものは削除推奨
DROP TABLE mwa.resource_occupation;
DROP TABLE mwa.resource_group_content CASCADE;
DROP TABLE mwa.resource_request CASCADE;

CREATE TABLE mwa.resource_request
(
  resource_request_id uuid NOT NULL,
	CONSTRAINT resource_request_pkey PRIMARY KEY (resource_request_id),
  resource_request_occupant_id uuid NOT NULL,
  resource_request_occupant_name character varying(256) NOT NULL,
  resource_request_receiver_id uuid,
  resource_request_queue_id uuid NOT NULL,
	CONSTRAINT resource_request_queue_id_fkey FOREIGN KEY (resource_request_queue_id) REFERENCES mwa.resource (resource_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE RESTRICT,
  resource_request_metadata xml,
  resource_request_assigned_flag boolean,
  resource_request_create_time timestamp with time zone,
  resource_request_update_time timestamp with time zone
);

CREATE TABLE mwa.resource_request_entry
(
  resource_request_entry_resource_request_id uuid NOT NULL,
	CONSTRAINT resource_request_entry_resource_request_id_fkey FOREIGN KEY (resource_request_entry_resource_request_id) REFERENCES mwa.resource_request (resource_request_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  resource_request_entry_resource_id uuid NOT NULL,
	CONSTRAINT resource_request_entry_pkey PRIMARY KEY (resource_request_entry_resource_request_id, resource_request_entry_resource_id),
  resource_request_entry_cost_type character varying(256) NOT NULL,
  resource_request_entry_cost_values text NOT NULL,
  resource_request_entry_assigned_resource_id uuid,
	CONSTRAINT resource_request_entry_assigned_resource_id_fkey FOREIGN KEY (resource_request_entry_assigned_resource_id) REFERENCES mwa.resource (resource_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE
);

ALTER TABLE mwa.resource_group ADD COLUMN resource_group_type character varying(256) DEFAULT 'GROUP' NOT NULL;
ALTER TABLE mwa.resource_group ADD COLUMN resource_group_sub_type_1 character varying(256);
ALTER TABLE mwa.resource_group ADD COLUMN resource_group_sub_type_2 character varying(256);
ALTER TABLE mwa.resource_group ADD COLUMN resource_group_sub_type_3 character varying(256);
UPDATE mwa.resource_group SET resource_group_sub_type_1 = resource_group_resource_type;
ALTER TABLE mwa.resource_group DROP COLUMN resource_group_resource_type;
ALTER TABLE mwa.resource_group ADD COLUMN resource_group_exclusion_id character varying(256);

CREATE TABLE mwa.resource_group_content
(
  resource_group_content_resource_group_id uuid NOT NULL,
	FOREIGN KEY (resource_group_content_resource_group_id) REFERENCES mwa.resource_group (resource_group_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  resource_group_content_resource_id uuid NOT NULL,
  resource_group_content_resource_group_type character varying(256) NOT NULL,
	FOREIGN KEY (resource_group_content_resource_id) REFERENCES mwa.resource (resource_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  PRIMARY KEY(resource_group_content_resource_group_id, resource_group_content_resource_id),
  UNIQUE(resource_group_content_resource_id, resource_group_content_resource_group_type)
);

ALTER TABLE mwa.resource ADD COLUMN resource_parent_id uuid; 
ALTER TABLE mwa.resource ADD FOREIGN KEY (resource_parent_id) REFERENCES mwa.resource (resource_id) ON DELETE SET NULL;
ALTER TABLE mwa.resource RENAME holder TO resource_holder;
ALTER TABLE mwa.resource RENAME holder_type TO resource_holder_type;
