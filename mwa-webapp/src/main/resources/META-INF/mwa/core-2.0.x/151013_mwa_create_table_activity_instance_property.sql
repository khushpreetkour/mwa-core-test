CREATE TABLE mwa.activity_instance_property
(
   activity_instance_property_id uuid NOT NULL default uuid_generate_v4(),
   activity_instance_id uuid NOT NULL,
   activity_instance_property_name text NOT NULL,
   activity_instance_property_value text,
   CONSTRAINT activity_instance_property_pkey PRIMARY KEY (activity_instance_property_id),
   CONSTRAINT activity_instance_property_activity_instance_id_fkey FOREIGN KEY (activity_instance_id)
      REFERENCES mwa.activity_instance (activity_instance_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
   CONSTRAINT activity_instance_property_name_check CHECK (length(activity_instance_property_name::text) <= 256),
   CONSTRAINT activity_instance_property_value_check CHECK (length(activity_instance_property_value::text) <= 2048),
   CONSTRAINT uq_instance_property UNIQUE (activity_instance_id, activity_instance_property_name, activity_instance_property_value)
);