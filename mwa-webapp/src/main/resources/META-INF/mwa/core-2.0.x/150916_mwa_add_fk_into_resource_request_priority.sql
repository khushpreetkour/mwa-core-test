DROP TABLE mwa.resource_request_priority CASCADE;

CREATE TABLE IF NOT EXISTS mwa.resource_request_priority
(
  resource_request_priority_resource_request_id uuid NOT NULL primary key,
    foreign key (resource_request_priority_resource_request_id) references mwa.resource_request(resource_request_id) match simple on update no action on delete cascade,
  resource_request_priority_0 character varying(256),
  resource_request_priority_1 character varying(256),
  resource_request_priority_2 character varying(256),
  resource_request_priority_3 character varying(256),
  resource_request_priority_4 character varying(256)
);
