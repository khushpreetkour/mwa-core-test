ALTER TABLE mwa.activity_instance ADD COLUMN activity_instance_create_time timestamp with time zone;
UPDATE mwa.activity_instance SET activity_instance_create_time = activity_instance_start_time;
UPDATE mwa.activity_instance SET activity_instance_start_time = null;
