CREATE TABLE IF NOT EXISTS mwa.resource_group
(
  resource_group_id uuid NOT NULL primary key,
  resource_group_name character varying(256) NOT NULL,
  resource_group_resource_type character varying(256) NOT NULL,
  resource_group_create_time timestamp with time zone,
  resource_group_update_time timestamp with time zone
);

CREATE TABLE IF NOT EXISTS mwa.resource_group_content
(
  resource_group_content_resource_group_id uuid NOT NULL,
    foreign key (resource_group_content_resource_group_id) references mwa.resource_group(resource_group_id) match simple on update no action on delete cascade,
  resource_group_content_resource_id uuid NOT NULL,
    foreign key (resource_group_content_resource_id) references mwa.resource(resource_id) match simple on update no action on delete cascade
);

DO $$ BEGIN
  if exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='resource_request' and column_name='resource_request_priority') then
    ALTER TABLE mwa.resource_request DROP COLUMN resource_request_priority;
  end if;
END$$;

DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='sony' and table_name='resource_request' and column_name='resource_request_metadata') then
    ALTER TABLE mwa.resource_request ADD COLUMN resource_request_metadata xml;
  end if;
END$$;

CREATE TABLE IF NOT EXISTS mwa.resource_request_priority
(
  resource_request_priority_resource_request_id uuid NOT NULL primary key,
    foreign key (resource_request_priority_resource_request_id) references mwa.resource_request(resource_request_id) match simple on update no action on delete cascade,
  resource_request_priority_0 character varying(256),
  resource_request_priority_1 character varying(256),
  resource_request_priority_2 character varying(256),
  resource_request_priority_3 character varying(256),
  resource_request_priority_4 character varying(256)
);
