CREATE TABLE IF NOT EXISTS mwa.event_lock
(
  event_processer_id character varying(256) NOT NULL primary key,
  event_exclusion_id character varying(256)
);
