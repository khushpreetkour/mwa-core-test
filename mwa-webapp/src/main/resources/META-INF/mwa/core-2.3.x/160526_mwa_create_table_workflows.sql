

CREATE TABLE IF NOT EXISTS mwa.workflow
(
  workflow_id uuid NOT NULL,
  workflow_version character varying(64) NOT NULL,
  workflow_name character varying(256) NOT NULL,
  workflow_model text NOT NULL,
  workflow_delete_flag boolean NOT NULL,
  extension_id  uuid,
  workflow_create_time timestamp with time zone,
  workflow_update_time timestamp with time zone,
  CONSTRAINT workflow_pkey PRIMARY KEY (workflow_id,workflow_version)
);


DO $$ BEGIN
  if not exists (select column_name from  information_schema.columns where table_catalog='nvx' and table_name='activity_template' and column_name='activity_template_delete_flag') then
    ALTER TABLE mwa.activity_template ADD COLUMN activity_template_delete_flag boolean;
    UPDATE mwa.activity_template SET activity_template_delete_flag = false;
  end if;
END$$;

