CREATE TABLE IF NOT EXISTS mwa.parameter_set
(
  id uuid NOT NULL primary key,
  name character varying(256),
  parameters text NOT NULL
);