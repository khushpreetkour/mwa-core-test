CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS mwa.activity_template_type
(
  activity_template_type_id integer NOT NULL primary key,
  activity_template_type_name character varying(64),
  constraint "activity_template_type_name_key" UNIQUE (activity_template_type_name)
);

CREATE TABLE IF NOT EXISTS mwa.activity_template
(
  activity_template_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_template_name character varying(256) NOT NULL,
  activity_template_version character varying(64) NOT NULL,
  activity_template_category character varying(64),
  activity_template_type_id integer NOT NULL,
  foreign key (activity_template_type_id) references mwa.activity_template_type(activity_template_type_id),
  constraint "activity_template_name_version_key" UNIQUE (activity_template_name, activity_template_version)
);

CREATE TABLE IF NOT EXISTS mwa.activity_instance
(
  activity_instance_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_template_id uuid NOT NULL,
  foreign key (activity_template_id) references mwa.activity_template(activity_template_id) match simple on update no action on delete no action,
  activity_instance_priority integer,
  activity_instance_status character varying(64) NOT NULL,
  activity_instance_status_details character varying(64),
  activity_instance_device_response_code character varying(256),
  activity_instance_device_response_details character varying(1024),
  activity_instance_progress integer,
  activity_instance_input_details xml,
  activity_instance_output_details xml,
  activity_instance_start_time timestamp with time zone,
  activity_instance_end_time timestamp with time zone,
  activity_instance_name character varying(256),
  activity_instance_parent_instance_id uuid,
  foreign key (activity_instance_parent_instance_id) references mwa.activity_instance(activity_instance_id) match simple on update no action on delete cascade,
  st_terminated boolean,
  st_stable boolean,
  st_error boolean,
  st_cancel boolean,
  constraint "activity_instance_name_and_parent_key" UNIQUE (activity_instance_name, activity_instance_parent_instance_id)
);

CREATE TABLE IF NOT EXISTS mwa.activity_event
(
  activity_event_id serial primary key NOT NULL,
  activity_event_activity_instance_id uuid,
    foreign key (activity_event_activity_instance_id) references mwa.activity_instance(activity_instance_id) match simple on update no action on delete cascade,
  activity_event_name character varying(256) NOT NULL,
  activity_event_parameter text,
  activity_event_priority integer,
  activity_event_created_time timestamp with time zone
);
