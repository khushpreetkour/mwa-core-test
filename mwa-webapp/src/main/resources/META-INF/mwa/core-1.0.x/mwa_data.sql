-- activity_template_type
INSERT INTO mwa.activity_template_type (activity_template_type_id, activity_template_type_name)
 SELECT 1, 'workflow'
 WHERE NOT EXISTS (SELECT activity_template_type_id FROM mwa.activity_template_type WHERE activity_template_type_id = 1);

INSERT INTO mwa.activity_template_type (activity_template_type_id, activity_template_type_name)
 SELECT 2, 'task'
 WHERE NOT EXISTS (SELECT activity_template_type_id FROM mwa.activity_template_type WHERE activity_template_type_id = 2);
