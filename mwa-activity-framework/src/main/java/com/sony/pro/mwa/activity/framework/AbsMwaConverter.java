package com.sony.pro.mwa.activity.framework;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.IMWARC;

public abstract class AbsMwaConverter extends SyncTaskBase {

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		
		IMWARC rc = this.getTemplate().verify(params);
		if (!rc.isSuccess()) {
			throw new MwaInstanceError(rc);
		}
		
		Map<String, Object> convResult = this.convertImpl(params);
		this.setOutputDetails(convResult);
		
		return OperationResult.newInstance().put(convResult);
	}
	
	public abstract Map<String, Object> convertImpl(Map<String, Object> params);

}
