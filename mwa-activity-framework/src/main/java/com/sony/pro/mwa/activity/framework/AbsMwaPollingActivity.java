package com.sony.pro.mwa.activity.framework;

import com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsMwaPollingActivity extends AbsMwaActivity {
	public AbsMwaPollingActivity(IState state, IStateMachine stm) {
		super(state, stm);
	}
}
