package com.sony.pro.mwa.activity.framework.standard.sample;

import java.util.Map;
import java.util.HashMap;

import com.sony.pro.mwa.activity.framework.standard.PollingTaskBase;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskEvent;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.provider.IActivityProvider;

public class PollingTask extends PollingTaskBase {
	
	/*-------------------------------------------------------------------------------------------
	 *  Member変数。必ずset/getInstanceDetailsで上位からアクセス可能であること。メンバ変数の永続化と復元のため。
	 -------------------------------------------------------------------------------------------*/

	//workflowからのテスト用入力パラメータ値
	private static String PROGRESS = "PROGRESS@TestTask";
	private static String TESTPARAM = "TESTPARAM@TestTask";

	@SuppressWarnings("serial")
	private Map<String, Object> localMap = new HashMap<String, Object>() {{
		put(PROGRESS, null);
		put(TESTPARAM, null);
	}};
	
	/*-------------------------------------------------------------------------------------------
	 *  コンストラクタ
	 -------------------------------------------------------------------------------------------*/
	public PollingTask() {
		super();
	}

	/*-------------------------------------------------------------------------------------------
	 *  Activity Framework 接続用の Override
	 -------------------------------------------------------------------------------------------*/
	@Override
	public String getPersistentDataImpl() {
		return Converter.mapToJson(localMap);
	}
	
	@Override
	public void setPersistentDataImpl(String details) {
		this.localMap = Converter.jsonToMap(details);
		return;
	}

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		logger.info("requestSubmitImpl called.");
		
		//WFからの入力値を取り出し（テスト用）
		String testParamFromWF = (String)params.get("TestParam");
		logger.info("[ydbg] TestParam=" + testParamFromWF);
		this.localMap.put(TESTPARAM, testParamFromWF);
		
		OperationResult result = OperationResult.newInstance(); //成功時のResultInstance生成
		return result;
	}

	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		Integer progress = this.getProgress() + 20;
		if (progress > 100) progress = 100;
		logger.info("confirmStatusImpl called: status=" + this.getStatus() + ", progress=" + this.getProgress() + " -> " + progress);
		
		OperationResultWithStatus result = null;
		
		//Device.getJobStatus();

/*		if (this.getProgress() >= 40) 
			throw new MwaInstanceError(MWARC.DEVICE_ERROR, "100", "test error");*/

		if (this.getStatus().isCancel()) {
			result = OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_CANCELLED, progress);
		} else if (progress >= 100) {
			progress = 100;
			result = OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_COMPLETED, progress);
			result.put("SAMPLE_CURRENT_DATE", new java.util.Date().toString());
			//WFに値を返却
			this.setOutput("TestParam", this.localMap.get(TESTPARAM));
		//ydbg
		} else {
			result = OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_UPDATED, progress);
			result.put("SAMPLE_CURRENT_DATE", new java.util.Date().toString());
		}
		
		return result;
	}

	@Override
	public OperationResult requestCancelImpl(Map<String, Object> params) {
		logger.info("requestCancelImpl called.");
		//do nothing
		return OperationResult.newInstance();
	}

	@Override
	public OperationResult requestStopImpl(Map<String, Object> params) {
		logger.info("requestStopImpl called.");
		return OperationResult.newInstance();
	}

	@Override
	public OperationResult requestPauseImpl(Map<String, Object> params) {
		logger.info("requestPauseImpl called.");
		return OperationResult.newInstance();
	}
	
	@Override
	protected OperationResult processEventForEventType(IEvent event, Map<String, Object> params) {

		if (params != null) {
			if (params.get("KILL") != null)
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
		}
		
		return super.processEventForEventType(event, params);
	}
}
