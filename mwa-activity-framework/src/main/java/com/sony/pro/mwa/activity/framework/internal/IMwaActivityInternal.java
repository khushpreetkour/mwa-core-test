package com.sony.pro.mwa.activity.framework.internal;

import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.activity.IMwaActivity;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.service.storage.IUriResolver;

public interface IMwaActivityInternal extends IMwaActivity {

	public void setTemplate(IActivityTemplate details);
	public void setProvider(IActivityProvider provider);
	public void setModel(ActivityInstanceModel model);
	@Deprecated
	public IUriResolver getUriResolver();
	@Deprecated
	public void setUriResolver(IUriResolver uriResolver);
	
	public void setCoreModules(ICoreModules coerModules);
}
