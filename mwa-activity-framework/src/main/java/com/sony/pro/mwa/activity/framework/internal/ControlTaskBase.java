package com.sony.pro.mwa.activity.framework.internal;

import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.parameter.OperationResult;

import java.util.Map;

public abstract class ControlTaskBase extends SyncTaskBase {
    @Override
    protected OperationResult requestSubmitImpl(Map<String, Object> params) {
        OperationResult result = OperationResult.newInstance();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            result.put(param.getKey(), param.getValue());
        }
        return result;
    }
}
