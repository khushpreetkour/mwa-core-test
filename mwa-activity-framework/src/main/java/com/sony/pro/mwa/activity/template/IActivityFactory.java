package com.sony.pro.mwa.activity.template;

import com.sony.pro.mwa.activity.framework.internal.IMwaActivityInternal;

public interface IActivityFactory {
	public IMwaActivityInternal createInstance();
}
