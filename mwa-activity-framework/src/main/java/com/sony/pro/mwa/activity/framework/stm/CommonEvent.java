package com.sony.pro.mwa.activity.framework.stm;

import com.sony.pro.mwa.service.activity.IEvent;


public enum CommonEvent implements IEvent {
	REQUEST_SUBMIT		(EventType.REQUEST),
	REQUEST_CANCEL		(EventType.REQUEST),
	CONFIRM_STATUS		(EventType.CONFIRM),
	NOTIFY_ERROR		(EventType.NOTIFY),
	;

	EventType type;
	
	private CommonEvent(EventType type) { this.type = type; }

	public boolean isMatch(String eventName) { return this.toString().equals(eventName); }
	public boolean isRequestEvent() { return this.type.isRequest(); }
	public boolean isNotifyEvent() { return this.type.isNotify(); }
	public boolean isConfirmEvent() { return this.type.isConfirm(); }

	@Override
	public String getName() {
		return super.name();
	}
}
