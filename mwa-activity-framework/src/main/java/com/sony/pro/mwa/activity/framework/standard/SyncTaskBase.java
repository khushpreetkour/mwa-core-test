package com.sony.pro.mwa.activity.framework.standard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.AbsMwaSyncActivity;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public abstract class SyncTaskBase extends AbsMwaSyncActivity {

	public SyncTaskBase() {
		super(SyncTaskState.CREATED, new SyncTaskStateMachine());
	}

	@Override
	public String getPersistentDataImpl() {
		return null;
	}
	
	@Override
	public void setPersistentDataImpl(String details) {
		return;
	}

	@Override
	protected OperationResult processEventImpl(IEvent event, Map<String, Object> params) {
		throw new MwaError(MWARC.INVALID_INPUT_EVENT, null, "not implemented event: " + (event != null ? event.getName() : "null"));
	}

	@Override
	protected List<? extends IEvent> getSupportedEventListImpl() {
		return new ArrayList<>();
	}

	@Override
	protected List<? extends IState> getSupportedStateListImpl() {
		return Arrays.asList(SyncTaskState.values());
	}

}
