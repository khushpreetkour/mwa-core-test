package com.sony.pro.mwa.bundle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.model.kbase.ParameterTypeModel;
import com.sony.pro.mwa.service.provider.ActivityProviderType;

public class MwaBundleContentManager {
	protected Map<Object, List<ActivityProviderType>> providerTypeListMap;
	protected Map<Object, List<ActivityTemplate>> templateListMap;
	protected Map<Object, List<ParameterTypeModel>> parameterTypeListMap;
	
	public MwaBundleContentManager() {
		templateListMap = new HashMap<>();
		providerTypeListMap = new HashMap<>();
	}
	
	protected <T> void addContents(Object key, java.util.List<T> contents, Map<Object, List<T>> contentsMap) {
		if (contents == null) {
			throw new RuntimeException("MwaBundleContentManager: addContent: content is null: ");
		} else {
			contentsMap.put(key, contents);
		}
		return;
	}
	
	protected <T> List<T> getContents(Object key, Map<Object, List<T>> contentsMap) {
		return contentsMap.get(key);
	}
	
	public void addTemplates(Object key, java.util.List<ActivityTemplate> contents) {
		addContents(key, contents, templateListMap);
		return;
	}
	
	public List<ActivityTemplate> getTemplates(Object key) {
		return getContents(key, templateListMap);
	}
	
	public ActivityTemplate getTemplate(Object key, String name, String version) {
		ActivityTemplate result = null;
		List<ActivityTemplate> templates = getContents(key, templateListMap);
		if (templates != null) { 
			for (ActivityTemplate tmplt : templates) {
				if (tmplt.getName().equals(name) && tmplt.getVersion().equals(version))
					result =  tmplt;
			}
		}
		return result;
	}

	public void addProviderTypes(Object key, java.util.List<ActivityProviderType> contents) {
		if (contents != null) //nullの場合はProviderTypeを使用しないという解釈、無視してOK
			addContents(key, contents, providerTypeListMap);
		return;
	}

	public List<ActivityProviderType> getProviderTypes(Object key) {
		return getContents(key, providerTypeListMap);
	}
	
	public ActivityProviderType getProviderType(Object key, String name, String modelNumber) {
		ActivityProviderType result = null;
		List<ActivityProviderType> contents = getContents(key, providerTypeListMap);
		if (contents != null) { 
			for (ActivityProviderType content : contents) {
				if (content.getName().equals(name) && content.getModelNumber().equals(modelNumber))
					result =  content;
			}
		}
		return result;
	}

	public void addParameterTypes(Object key, java.util.List<ParameterTypeModel> contents) {
		if (contents != null) //nullの場合はProviderTypeを使用しないという解釈、無視してOK
			addContents(key, contents, parameterTypeListMap);
		return;
	}

	public List<ParameterTypeModel> getParameterTypes(Object key) {
		return getContents(key, parameterTypeListMap);
	}
	
	public ParameterTypeModel getParameterType(Object key, String name, String version) {
		ParameterTypeModel result = null;
		List<ParameterTypeModel> contents = getContents(key, parameterTypeListMap);
		if (contents != null) { 
			for (ParameterTypeModel content : contents) {
				if (content.getName().equals(name) && content.getVersion().equals(version))
					result =  content;
			}
		}
		return result;
	}
}
