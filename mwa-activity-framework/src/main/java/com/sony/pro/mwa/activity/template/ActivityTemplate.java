package com.sony.pro.mwa.activity.template;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import com.sony.pro.mwa.activity.framework.AbsMwaConverter;
import com.sony.pro.mwa.activity.framework.AbsMwaSyncActivity;
import com.sony.pro.mwa.activity.framework.internal.EndTaskBase;
import com.sony.pro.mwa.activity.framework.internal.StartTaskBase;
import com.sony.pro.mwa.activity.framework.internal.IMwaActivityInternal;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.UUIDUtils;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.activity.StandardCostCalcurator;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.activity.ICostCalcurator;
import com.sony.pro.mwa.service.activity.IMwaActivity;
import com.sony.pro.mwa.utils.ParameterUtils;

//ActivityPluginに加えて、新たに実装責務を伴う概念(widの変わり？)
public class ActivityTemplate extends ActivityTemplateModel implements IActivityTemplate {	//=bpmn2:ioSpecification, 及び、WorkItemDefinitionに相当（widから生成するか？それとも逆か？）
	protected IActivityFactory factory;
	protected ICostCalcurator costCalc;
	protected Class<? extends IMwaActivity> clazz;
	protected List<String> tags;
	protected MwaLogger logger = MwaLogger.getLogger(ActivityTemplate.class);
	private boolean nodeDependencyFlag = false;

	//wid
	protected List<? extends IParameterDefinition> inputs;
	protected List<? extends IParameterDefinition> outputs;	//Userへのプレゼンテーション用。処理には使用しない

	protected ActivityTemplate() {
		this.tags = new ArrayList<>();
	}

	@Deprecated
	public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, String version, String category, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs) {
		this(clazz, name, version, inputs, outputs);
	}

	protected static String removeNamespace(String str) {
		if (str == null)
			return str;
		int pos = str.lastIndexOf(".");
		if (pos != -1) {
			str = str.substring(pos+1);
		}
		return str;
	}

	protected static void sort(List<? extends IParameterDefinition> lists) {
		Collections.sort(lists, new Comparator<IParameterDefinition>() {
            @Override
            public int compare(IParameterDefinition o1, IParameterDefinition o2) {
                return o1.getKey().compareToIgnoreCase(o2.getKey());
            }
        });
	}

	protected static String generateHash(List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs) {
		String source = "";
		if (inputs != null) {
			sort(inputs);
			for (IParameterDefinition input : inputs) {
				source += input.getKey() + input.getTypeName() + input.getRequired();
			}
		}
		source += "/";
		if (outputs != null) {
			sort(outputs);
			for (IParameterDefinition output : outputs) {
				source += output.getKey() + output.getTypeName() + output.getRequired();
			}
		}
		String result = null;
		try {
			byte[] bytes = MessageDigest.getInstance("MD5").digest(source.toLowerCase().getBytes(StandardCharsets.UTF_8));
	        result = DatatypeConverter.printHexBinary(bytes);
		} catch (NoSuchAlgorithmException e) {
		}
		return result;
	}

	@Deprecated
	public ActivityTemplate(String id, Class<? extends IMwaActivityInternal> clazz, String name, String version, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs) {
		this();
		this.clazz = clazz;
		this.setId(UUIDUtils.generateUUID(name, version));
		this.setName(name);
		this.setVersion(version);
		this.setType(getType(clazz));
		this.alias = removeNamespace(name);
		this.icon = getDefaultIcon();
		this.deleteFlag = false;
		this.setTags(new ArrayList());
		this.description = getDefaultDescription();
		{
			List<IParameterDefinition> actualInputs = new ArrayList<>();
			actualInputs.add(ParameterUtils.createParameter(PresetParameter.ProfileId));
			for (IParameterDefinition input : inputs) {
				actualInputs.add(ParameterUtils.createParameter(input));
			}
			this.inputs = actualInputs;
		}
		//this.outputs = outputs;
		{
			List<IParameterDefinition> actualOutputs = new ArrayList<>();
			actualOutputs.add(ParameterUtils.createParameter(PresetParameter.ActivityInstanceStatus));
			actualOutputs.add(ParameterUtils.createParameter(PresetParameter.ActivityInstanceStatusDetails));
			actualOutputs.add(ParameterUtils.createParameter(PresetParameter.ActivityInstanceDeviceResponse));
			actualOutputs.add(ParameterUtils.createParameter(PresetParameter.ActivityInstanceDeviceResponseDetails));
			for (IParameterDefinition output : outputs) {
				actualOutputs.add(ParameterUtils.createParameter(output));
			}
			this.outputs = actualOutputs;
		}

		this.factory = new IActivityFactory(){
			private Class<? extends IMwaActivityInternal> clazz;
			public IActivityFactory setClass(Class<? extends IMwaActivityInternal> clazz) {this.clazz = clazz; return this;}
			@Override
			public IMwaActivityInternal createInstance() {
				try {
					return clazz.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					logger.error("Can't generate template instance: clazz=" + ((clazz!=null)?clazz.toString():"null"), e);
				}
				return null;
			}
		}.setClass(clazz);

		this.costCalc = StandardCostCalcurator.NO_COST;
	}
	@Deprecated
	public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, String version, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs) {
		this(UUIDUtils.generateUUID(name, version), clazz, name, version, inputs, outputs);
	}

	@Deprecated
    public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, String version, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs, boolean nodeDependencyFlag) {
        this(UUIDUtils.generateUUID(name, version), clazz, name, version, inputs, outputs);
        this.nodeDependencyFlag = nodeDependencyFlag;
    }

    @Deprecated
	public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, String version, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs, String alias) {
		this(UUIDUtils.generateUUID(name, version), clazz, name, version, inputs, outputs);
		this.alias = alias;
		this.icon = getDefaultIcon();
		this.description = getDefaultDescription();
	}

	@Deprecated
	public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, String version, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs, String alias, String icon, String description) {
		this(UUIDUtils.generateUUID(name, version), clazz, name, version, inputs, outputs);
		this.alias = alias;
		this.icon = icon;
		this.description = description;
	}

	public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs) {
		this(null, clazz, name, generateHash(inputs, outputs), inputs, outputs);
	}

	public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs, String alias) {
		this(clazz, name, inputs, outputs);
		this.alias = alias;
		this.icon = getDefaultIcon();
		this.description = getDefaultDescription();
	}

	public ActivityTemplate(Class<? extends IMwaActivityInternal> clazz, String name, List<? extends IParameterDefinition> inputs, List<? extends IParameterDefinition> outputs, String alias, String icon, String description) {
		this(clazz, name, inputs, outputs);
		this.alias = alias;
		this.icon = (icon == null) ? getDefaultIcon() : icon;
		this.description = description;
	}


	protected ActivityType getType(Class clazz) {
		//workflowやcomposit、group、を自動判定してセットする

		//AbsMwaConverterを継承していればCONVERTER
		if (AbsMwaConverter.class.isAssignableFrom(clazz)) {
			return ActivityType.CONVERTER;
		} else if (StartTaskBase.class.isAssignableFrom(clazz)) {
			return ActivityType.START;
		} else if (EndTaskBase.class.isAssignableFrom(clazz)) {
			return ActivityType.END;
		}
		return ActivityType.TASK;
	}
	public Boolean getSyncFlag() {
		//AbsMwaConverterを継承していればSync
		if (AbsMwaConverter.class.isAssignableFrom(this.clazz)) {
			return true;
		} else if (AbsMwaSyncActivity.class.isAssignableFrom(this.clazz)) {
			return true;
		}
		return false;
	}

	public IMWARC verify(Map<String, Object> params) {
		IMWARC rc = MWARC.SUCCESS;
		for (IParameterDefinition port : this.inputs) {
			Object value = params.get(port.getKey());
			if (value == null  && port.getRequired()) {	//必須パラメータがない
				logger.info("ActivityTemplate.verify(): Required param not found: key=" + port.getKey());
				rc = MWARC.INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND;
				break;
			}
		}
		return rc;
	}

	public void calcCost(Map<String, Object> params) {

	}

	@Override
	public List<? extends IParameterDefinition> getInputs() {
		return inputs;
	}

	@Override
	public void setInputs(List<IParameterDefinition> inputs) {
		this.inputs = inputs;
	}

	@Override
	public List<? extends IParameterDefinition> getOutputs() {
		return outputs;
	}

	@Override
	public void setOutputs(List<IParameterDefinition> outputs) {
		this.outputs = outputs;
	}

	@Override
	public IMwaActivity createInstance() {
		IMwaActivityInternal result = this.factory.createInstance();
		result.setTemplate(this);
		return result;
	}

	public ActivityTemplate addTag(String tag) {
		// TODO Auto-generated method stub
		this.tags.add(tag);
		return this;
	}

	public ActivityTemplate removeTag(String tag) {
		// TODO Auto-generated method stub
		this.tags.remove(tag);
		return this;
	}

	public ActivityTemplate setCostCalcurator(ICostCalcurator calcurator) {
		this.costCalc = calcurator;
		return this;
	}

	private String getDefaultIcon() {
		return "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-6%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cg%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M27.7%2C17.8c0-0.6%2C0-1.1-0.1-1.6L31%2C13l-2.4-4.2l-4.4%2C1.4c-0.8-0.7-1.8-1.4-2.8-1.7l-1-4.6h-4.8l-1%2C4.6%20c-1%2C0.4-2%2C1-2.8%2C1.7L7.4%2C8.8L5%2C13l3.5%2C3.1c-0.1%2C0.5-0.1%2C1.1-0.1%2C1.6c0%2C0.6%2C0%2C1.1%2C0.1%2C1.6L5%2C22.5l2.4%2C4.2l4.4-1.4%20c0.8%2C0.7%2C1.8%2C1.6%2C2.8%2C2l1%2C4.8h4.8l1-4.8c1-0.4%2C2-1.1%2C2.8-1.8l4.4%2C1.4l2.4-4.2l-3.5-3.1C27.7%2C18.9%2C27.7%2C18.3%2C27.7%2C17.8z%20M18%2C23%20c-2.8%2C0-5-2.2-5-5s2.2-5%2C5-5s5%2C2.2%2C5%2C5S20.8%2C23%2C18%2C23z%22%2F%3E%20%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";
	}

	private String getDefaultDescription() {
		return "To Be Determined.";
	}

	public boolean getNodeDependencyFlag() {
	    return nodeDependencyFlag;
	}
}
