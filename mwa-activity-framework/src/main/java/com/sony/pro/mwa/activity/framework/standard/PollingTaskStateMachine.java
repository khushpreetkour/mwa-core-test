package com.sony.pro.mwa.activity.framework.standard;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.sony.pro.mwa.activity.framework.stm.AbsStateMachine;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class PollingTaskStateMachine extends AbsStateMachine {
	
	//状態遷移表の定義
	private final static Object[][] stTableDef = {
			//CurrentState,			//Event,						//NextState
		{	PollingTaskState.CREATED,		CommonEvent.REQUEST_SUBMIT,			PollingTaskState.IN_PROGRESS},
		{	PollingTaskState.CREATED,		CommonEvent.NOTIFY_ERROR,			PollingTaskState.ERROR},
		{	PollingTaskState.IN_PROGRESS,	CommonEvent.REQUEST_CANCEL,			PollingTaskState.CANCELLING},
		{	PollingTaskState.IN_PROGRESS,	PollingTaskEvent.NOTIFY_COMPLETED,		PollingTaskState.COMPLETED},
		{	PollingTaskState.IN_PROGRESS,	PollingTaskEvent.NOTIFY_UPDATED,		PollingTaskState.IN_PROGRESS},
		{	PollingTaskState.IN_PROGRESS,	CommonEvent.NOTIFY_ERROR,			PollingTaskState.ERROR},
		{	PollingTaskState.CANCELLING,	PollingTaskEvent.NOTIFY_UPDATED,		PollingTaskState.CANCELLING},
		{	PollingTaskState.CANCELLING,	PollingTaskEvent.NOTIFY_COMPLETED,		PollingTaskState.COMPLETED},
		{	PollingTaskState.CANCELLING,	CommonEvent.NOTIFY_ERROR,			PollingTaskState.ERROR},
		{	PollingTaskState.CANCELLING,	PollingTaskEvent.NOTIFY_CANCELLED,		PollingTaskState.CANCELLED},
	};

	private final static HashMap<Pair<IState, IEvent>, IState> stTable = new HashMap<Pair<IState, IEvent>, IState>() {{
		//初期化処理、stTableDefをMapに詰めてるだけ(直接Mapで定義してもよかったが、定義を配列で切り出した方がシンプルで管理しやすいので)
		for (int i = 0; i < stTableDef.length; i++) {
			put(Pair.of((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	@Override
	protected Map<Pair<IState, IEvent>, IState> getStateTransitionTable() {
		return stTable;
	}
}
