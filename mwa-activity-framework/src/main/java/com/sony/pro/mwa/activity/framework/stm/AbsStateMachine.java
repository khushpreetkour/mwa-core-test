package com.sony.pro.mwa.activity.framework.stm;

import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsStateMachine implements IStateMachine {
	
	public boolean isAcceptable(IState current, IEvent event) {
		IState next = getNextState(current, event);
		return (next != null) ? true : false;
	}

	public IState getNextState(IState state, IEvent event) {
		Map<Pair<IState, IEvent>, IState> tbl = this.getStateTransitionTable();
		//Mapから取り出して返す
		if (tbl != null) 
			return tbl.get(Pair.of(state, event));
		else
			return null;
	}
	
	protected abstract Map<Pair<IState, IEvent>, IState> getStateTransitionTable();
}
