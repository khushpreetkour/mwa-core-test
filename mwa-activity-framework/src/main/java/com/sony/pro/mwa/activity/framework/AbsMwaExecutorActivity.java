package com.sony.pro.mwa.activity.framework;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.sony.pro.mwa.activity.framework.executor.ExecutorServiceManager;
import com.sony.pro.mwa.activity.framework.executor.ExecutorTaskCallable;
import com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity;
import com.sony.pro.mwa.activity.framework.standard.ExecutorTaskEvent;
import com.sony.pro.mwa.activity.framework.standard.ExecutorTaskState;
import com.sony.pro.mwa.activity.framework.standard.ExecutorTaskStateMachine;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsMwaExecutorActivity extends AbsMwaActivity  {
	private Integer progress = 0;
	private Map<String, Object> persistentMap = new HashMap<String, Object>();

	public AbsMwaExecutorActivity() {
		super(ExecutorTaskState.CREATED, new ExecutorTaskStateMachine());
	}
	
	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#getPersistentDataImpl()
	 */
	@Override
	public String getPersistentDataImpl() {
		// MWA Core get this persistent data. set "jobId" in the requestSubmitImpl
		return Converter.mapToJson(persistentMap);
	}

	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#setPersistentDataImpl(java.lang.String)
	 */
	@Override
	public void setPersistentDataImpl(String details) {
		this.persistentMap = Converter.jsonToMap(details);
		return;
	}

	
	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#processEventImpl(com.sony.pro.mwa.activity.framework.stm.IEvent, java.util.Map)
	 */
	@Override
	protected OperationResult processEventImpl(IEvent event,
			Map<String, Object> params) {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#getSupportedEventListImpl()
	 */
	@Override
	protected List<? extends IEvent> getSupportedEventListImpl() {
		return Arrays.asList(ExecutorTaskEvent.values());
	}
	
	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#getSupportedStateListImpl()
	 */
	@Override
	protected List<? extends IState> getSupportedStateListImpl() {
		return Arrays.asList(ExecutorTaskState.values());
	}

	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#requestSubmitImpl(java.util.Map)
	 */
	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		// check params
		ExecutorTaskCallable task = this.createTaskCallable();
		task.setMwaCore(this.getCoreModules());
		task.setActivityProvider(this.getProvider());
		
		task.setParams(params);
		try{
			task.validInput(params);
		} catch(MwaError err){
			throw err;
		} catch(Exception e){
			throw new MwaInstanceError(MWARC.INVALID_INPUT, "", e.getMessage());
		}
		
		for (String key : params.keySet()) {
			if (!"passwordList".equals(key)) //passwordを平文出力している箇所があるので暫定でログ出力をやめる
				logger.info("INPUTS KEY VALUE:" + key + ":" + params.get(key));
		}

		Integer threadPoolSize = this.getMaxConcurrentJobs();
		String templateName = this.getTemplate().getName();
		// Create callable job and it is submited in a thread pool.
		
		ExecutorServiceManager.getInstance().submit(this.getId(), templateName, threadPoolSize, task);
		
		return  OperationResult.newInstance();
	}

	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#confirmStatusImpl(java.util.Map)
	 */
	@Override
	public OperationResultWithStatus confirmStatusImpl(
			Map<String, Object> params) {

		// Get Instance of ExecutorServiceManager class.
		 ExecutorTaskCallable task = ExecutorServiceManager.getInstance().getActivityInstance(this.getId());
		if(null == task){
			throw new MwaInstanceError(MWARC.INVALID_INPUT_INSTANCE_ID);
		}
		
		// Decid Event
		IEvent event = ExecutorTaskEvent.NOTIFY_READY;
		
		Integer insProgress = task.getAcitivityProgress();
		if(insProgress != null){
			this.progress = insProgress;
		}
		
		if( task.getFuture().isCancelled()){
			event = ExecutorTaskEvent.NOTIFY_CANCELLED;
		} else if(task.getFuture().isDone()) {
			Map<String, Object> output;

			try {
				output = task.getFuture().get();
				this.setOutputDetails(output);
				event  = ExecutorTaskEvent.NOTIFY_COMPLETED;
				if(this.progress==0){
					this.progress = 100;
				}
			} catch (InterruptedException | ExecutionException e) {
				try{
					output = task.getErrorOutputs();
					if(null != output && output.size() > 0){
						this.setOutputDetails(output);
					}
				}catch( NoSuchMethodError medhodErr ){
					logger.info("Executor ver 1.2.1 does not have getErrorOutputs method. ");
				}
				if(e.getCause()!=null &&
						(e.getCause().getClass().getName().equals(MwaError.class.getName())
						||e.getCause().getClass().getName().equals(MwaInstanceError.class.getName()))){
					throw (MwaError)e.getCause();
				}
				event = CommonEvent.NOTIFY_ERROR;
				ExecutorServiceManager.getInstance().removeActivityInstance(this.getId());
				logger.error(e.getMessage(), e);

				try {
					throw e;
				} catch (Exception e1) {
					throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
				}
			}
		} else if(task.getCalledFlg()){
			event = ExecutorTaskEvent.NOTIFY_IN_PROGRESS;
		}

		if(event.equals(ExecutorTaskEvent.NOTIFY_COMPLETED)
				|| event.equals(ExecutorTaskEvent.NOTIFY_CANCELLED) ){
			// If this job finish, set output and remove the instance from memories(ExecutorServiceManager)
			ExecutorServiceManager.getInstance().removeActivityInstance(this.getId());
		}
		return OperationResultWithStatus.newInstance(event, this.progress);
	}
	


	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaActivity#requestCancelImpl(java.util.Map)
	 */
	@Override
	protected OperationResult requestCancelImpl(Map<String, Object> params) {
		if(this.getStatus().equals(ExecutorTaskState.READY)){
			return OperationResult.newInstance(MWARC.SUCCESS);// READY -> CANCELLED
		}
		
		ExecutorTaskCallable task = ExecutorServiceManager.getInstance().getActivityInstance(this.getId());
		if(null==task){
			throw new MwaInstanceError(MWARC.INVALID_INPUT_INSTANCE_ID);
		}

		Boolean cancelFlg = task.cancel();
		
		if(cancelFlg){
			return OperationResult.newInstance(MWARC.SUCCESS);
		} else {
//			return OperationResult.newInstance(MWARC.OPERATION_FAILED);
			throw new MwaError(MWARC.INVALID_INPUT_EVENT);
		}
	}

	public abstract ExecutorTaskCallable createTaskCallable();
	public abstract Integer getMaxConcurrentJobs();
	

}
