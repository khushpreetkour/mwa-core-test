package com.sony.pro.mwa.activity.framework.stm;

import com.sony.pro.mwa.service.activity.IState;

public enum CommonState implements IState {
				//stable	//terminated	//error		//cancelled
	QUEUED		(true,		false,			false,		false),
	@Deprecated
	READY		(true,		false,			false,		false),
	CANCELLED	(true,		true,			false,		true),
	ERROR		(true,		true,			true,		false),
	;
	
	private boolean stableFlag;
	private boolean terminatedFlag;
	private boolean errorFlag;
	private boolean cancelledFlag;

	private CommonState (boolean stableFlag, boolean terminatedFlag, boolean errorFlag, boolean cancelledFlag) {
		this.stableFlag = stableFlag;
		this.terminatedFlag = terminatedFlag;
		this.errorFlag = errorFlag;
		this.cancelledFlag = cancelledFlag;
	}
	
	public String getName() { return super.name(); }
	
	//Default
	public boolean isTerminated() { return this.terminatedFlag; }
	public boolean isStable() { return this.stableFlag; }
	public boolean isError() { return this.errorFlag; }
	public boolean isCancel() { return this.cancelledFlag; }

}
