package com.sony.pro.mwa.activity.framework.executor;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.service.provider.IActivityProvider;

public abstract class ExecutorTaskCallable implements Callable<Map<String, Object>> {
	private Future<Map<String, Object>> future;
	private Map<String, Object> params;
	private Map<String, Object> errorOutputs;
	private Boolean calledFlg = false;
	private IActivityProvider activityProvider;
	private ICoreModules mwaCore;

	public abstract void validInput(Map<String, Object> input) throws Exception;
	public abstract Map<String, Object> doJob(Map<String, Object> input) throws Exception;
	public abstract Boolean cancelActivity();
	public abstract Integer getAcitivityProgress();
	
	@Override
	public Map<String, Object> call() throws Exception {
		this.calledFlg = true;
		Map<String, Object> output = null;;
		try{
			output = doJob(this.params);
		}catch(MwaError e){
			throw e;
		}catch(Exception e){
			throw e;
		}

		return output;
	}
	
	public Boolean cancel(){
		Boolean cancelOpeFlg = false;
		if(future.isDone() || future.isCancelled()){
			
		}else if(calledFlg){
			cancelOpeFlg = this.cancelActivity();
			if(null == cancelOpeFlg){
				throw new MwaError(MWARC.INVALID_INPUT_EVENT);
			}
		}else{
			cancelOpeFlg = future.cancel(false);
		}
		return cancelOpeFlg;
	}
	
	/**
	 * @return the future
	 */
	public Future<Map<String, Object>> getFuture() {
		return future;
	}
	/**
	 * @param future the future to set
	 */
	public void setFuture(Future<Map<String, Object>> future) {
		this.future = future;
	}
	/**
	 * @return the calledFlg
	 */
	public Boolean getCalledFlg() {
		return calledFlg;
	}
	/**
	 * @param params the params to set
	 */
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	/**
	 * @return the activityProvider
	 */
	public IActivityProvider getActivityProvider() {
		return activityProvider;
	}
	/**
	 * @param activityProvider the activityProvider to set
	 */
	public void setActivityProvider(IActivityProvider activityProvider) {
		this.activityProvider = activityProvider;
	}
	/**
	 * @return the mwaCore
	 */
	public ICoreModules getMwaCore() {
		return mwaCore;
	}
	/**
	 * @param mwaCore the mwaCore to set
	 */
	public void setMwaCore(ICoreModules mwaCore) {
		this.mwaCore = mwaCore;
	}
	/**
	 * @return the errorOutput
	 */
	public Map<String, Object> getErrorOutputs() {
		return errorOutputs;
	}
	/**
	 * @param errorOutputs the errorOutput to set
	 */
	public void setErrorOutputs(Map<String, Object> errorOutputs) {
		this.errorOutputs = errorOutputs;
	}
	
}
