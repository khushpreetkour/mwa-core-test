package com.sony.pro.mwa.activity.framework.internal;

import com.sony.pro.mwa.activity.framework.stm.EventType;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class GeneralEventImpl implements IEvent {

	String name;
	EventType type;
	IState next;

	public GeneralEventImpl(String name, EventType type) {
		this.name = name;
		this.type = type;
	}

	@Override
	public String getName() {
		return name;
	}

	public boolean isRequestEvent() { return this.type.isRequest(); }
	public boolean isNotifyEvent() { return this.type.isNotify(); }
	public boolean isConfirmEvent() { return this.type.isConfirm(); }

	@Override
	public boolean isMatch(String eventName) {
		return (name != null) ? name.equalsIgnoreCase(eventName) : false;
	}
	
	public IState getNextState() {
		return next;
	}
	public GeneralEventImpl setNextState(IState next) {
		this.next = next;
		return this;
	}
}
