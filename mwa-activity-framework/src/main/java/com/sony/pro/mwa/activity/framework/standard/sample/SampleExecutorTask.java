package com.sony.pro.mwa.activity.framework.standard.sample;

import com.sony.pro.mwa.activity.framework.AbsMwaExecutorActivity;
import com.sony.pro.mwa.activity.framework.executor.ExecutorTaskCallable;

public class SampleExecutorTask extends AbsMwaExecutorActivity {
	private static final int CONCURRENT_JOB_SIZE = 2;

	/* (non-Javadoc)
	 * @see com.sony.pro.mwa.activity.framework.AbsMwaExecutorActivity#createTaskCallable()
	 */
	@Override
	public ExecutorTaskCallable createTaskCallable() {
		return new SampleExecutorJob();
	}

	/**
	 * set number of concurrent execution of tasks
	 */
	@Override
	public Integer getMaxConcurrentJobs() {
		return CONCURRENT_JOB_SIZE;
	}
}
