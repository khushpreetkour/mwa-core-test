package com.sony.pro.mwa.activity.framework.executor;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.utils.ParameterUtils;

import java.util.List;
import java.util.Map;

/***
 * MwaActivity用の基底クラス
 */
public abstract class AbsMwaExecutorTaskCallable extends ExecutorTaskCallable {

    /**
     * ログ出力クラス
     */
    protected MwaLogger logger;

    /**
     * 進捗率
     */
    protected int progress = 0;

    /**
     * 入力パラメータをチェックする
     * @param input Jobへの入力パラメータ
     */
    @Deprecated
    @Override
    public void validInput(Map<String, Object> input) {
        // こちらは使用しないでください。必要に応じてvalidateをオーバーライドするようにしてください。
    }
    /**
     * Cancel時の処理
     * @return 一律True
     */
    @Override
    public Boolean cancelActivity() {
        return true;
    }

    /***
     * 進捗率を取得する
     * @return 現在の進捗率
     */
    @Override
    public Integer getAcitivityProgress() {
        return this.progress;
    }

    /***
     * 入力パラメータを検証、変換してその結果を返す
     * こちらを使用してください。必要に応じてoverrideしてください
     * @param paramDefs パラメータ定義
     * @param params
     * @return
     */
    protected Map<String, Object> validate(List<? extends IParameterDefinition> paramDefs, Map<String, Object> params) {
        return ParameterUtils.FROM_STRING.convert(paramDefs, params);
    }

    /***
     * コンストラクタ
     * @param clazz ログを利用するサブクラスの型情報
     */
    public AbsMwaExecutorTaskCallable(Class clazz) {
        this.logger = MwaLogger.getLogger(clazz);
    }
}
