package com.sony.pro.mwa.activity.framework;

import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.service.activity.IMwaSyncActivity;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsMwaSyncActivity extends AbsMwaRunCompleteActivity implements IMwaSyncActivity {
	public AbsMwaSyncActivity(IState state, IStateMachine stm) {
		//state machineも共通にしていいかも
		super(state, stm);
	}
	
	public Boolean isVolatile() {
		return this.getModel().getVolatileFlag();
	};
	public void setVolatile(Boolean volatileFlag) {
		if (volatileFlag != null) 
			this.getModel().setVolatileFlag(volatileFlag);;
	}
}
