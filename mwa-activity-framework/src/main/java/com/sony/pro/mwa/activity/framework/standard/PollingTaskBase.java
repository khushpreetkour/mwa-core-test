package com.sony.pro.mwa.activity.framework.standard;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public abstract class PollingTaskBase extends AbsMwaActivity {

	public PollingTaskBase() {
		//インスタンス生成時の初期状態はQueued
		//状態遷移用にStateMachineForStandardJobを用いる
		this(PollingTaskState.CREATED, new PollingTaskStateMachine());
	}
	public PollingTaskBase(IState state, IStateMachine stm) {
		super(state, stm);
	}

	/*-------------------------------------------------------------------------------------------
	 *  外部I/F (for Activity Manager)
	 -------------------------------------------------------------------------------------------*/
	//AbsMwaActivityで実装済み

	/*-------------------------------------------------------------------------------------------
	 *  継承先の実装メソッド（Activityの実処理）
	 -------------------------------------------------------------------------------------------*/
	public abstract OperationResult requestStopImpl(Map<String, Object> params);
	public abstract OperationResult requestPauseImpl(Map<String, Object> params);

	/*-------------------------------------------------------------------------------------------
	 *  内部の実装コード(レイヤロジックなど)
	 -------------------------------------------------------------------------------------------*/
	@Override
	protected OperationResult processEventImpl(IEvent event, Map<String, Object> params) {
		OperationResult result = null;

		//Eventと実装メソッドのマッピング
		if (PollingTaskEvent.REQUEST_PAUSE.equals(event)) {
			result = requestPauseImpl(params);
		} else if (PollingTaskEvent.REQUEST_STOP.equals(event)) {
			result = requestStopImpl(params);
		}
		
		return result;
	}
	
	@Override
	protected List<? extends IEvent> getSupportedEventListImpl() {
		return Arrays.asList(PollingTaskEvent.values());
	}
	@Override
	protected List<? extends IState> getSupportedStateListImpl() {
		return Arrays.asList(PollingTaskState.values());
	}
}
