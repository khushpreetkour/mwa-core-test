package com.sony.pro.mwa.activity.framework.stm;

public enum EventType {
	REQUEST,
	NOTIFY,
	CONFIRM,
	;
	
	public boolean isRequest() {return REQUEST.equals(this);}
	public boolean isNotify() {return NOTIFY.equals(this);}
	public boolean isConfirm() {return CONFIRM.equals(this);}
}
