package com.sony.pro.mwa.activity.framework.standard.sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.AbsMwaExecutorActivity;
import com.sony.pro.mwa.activity.framework.executor.ExecutorTaskCallable;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;

public class SampleExecutorJob extends ExecutorTaskCallable {
	private static int MAX_COUNT = 10;
	private int progress = 0;
	private Boolean cancelFlg = Boolean.FALSE;
	
	/**
	 * check method of input data
	 */
	@Override
	public void validInput(Map<String, Object> input) {
	}

	/**
	 * main task
	 */
	@Override
	public Map<String, Object> doJob(Map<String, Object> input) {
		for(int index = 1; index <= MAX_COUNT; index ++){
			if(cancelFlg){
				System.out.println("SampleExecutorTask cancelFlg True");
				break;
			}
			try {
				Thread.sleep(1000);
				System.out.println("SampleExecutorTask Count:"+index+"/"+ MAX_COUNT);
				this.progress = index*100/MAX_COUNT;
				
			} catch (InterruptedException e) {
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
			}
			
		}
		
		List<String> list = new ArrayList<String>();
		list.add("AAAAAAA");
		list.add("BBBBBBB");
		list.add("CCCCCCC");
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("result", list);
		return output;
	}

	/**
	 * Called Cancel Event
	 */
	@Override
	public Boolean cancelActivity() {
		cancelFlg = true;
		return true;
	}

	/** 
	 * get progress[0-100]. if can not get progress, should return null. 
	 */
	@Override
	public Integer getAcitivityProgress() {
		// TODO Auto-generated method stub
		
		return this.progress;
	}
	

}
