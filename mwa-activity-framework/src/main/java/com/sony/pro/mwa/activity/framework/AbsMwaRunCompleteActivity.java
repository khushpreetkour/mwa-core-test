package com.sony.pro.mwa.activity.framework;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsMwaRunCompleteActivity extends AbsMwaActivity {
	public AbsMwaRunCompleteActivity(IState state, IStateMachine stm) {
		//state machineも共通にしていいかも
		super(state, stm);
	}
	
	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		throw new MwaError(MWARC.INVALID_INPUT_EVENT);
	}
}
