package com.sony.pro.mwa.activity.framework.standard;

import com.sony.pro.mwa.activity.framework.stm.EventType;
import com.sony.pro.mwa.service.activity.IEvent;

public enum ExecutorTaskEvent implements IEvent {
	//
	//REQUEST_CANCEL		(EventType.REQUEST),

	NOTIFY_COMPLETED	(EventType.NOTIFY),
	NOTIFY_CANCELLED	(EventType.NOTIFY),
	NOTIFY_READY	(EventType.NOTIFY),
	NOTIFY_IN_PROGRESS		(EventType.NOTIFY),
	;
	
	EventType type;
	
	private ExecutorTaskEvent(EventType type) { this.type = type; }

	public boolean isMatch(String eventName) { return this.toString().equals(eventName); }
	public boolean isRequestEvent() { return this.type.isRequest(); }
	public boolean isNotifyEvent() { return this.type.isNotify(); }
	public boolean isConfirmEvent() { return this.type.isConfirm(); }

	@Override
	public String getName() {
		return this.name();
	}
}
