package com.sony.pro.mwa.activity.framework.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.activity.framework.stm.CommonStateMachine;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.common.log.ILoggerSourceWithPrefix;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.ServiceEndpointType;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.rc.MWARC_GENERIC;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IMwaSyncActivity;
import com.sony.pro.mwa.service.activity.IState;
import com.sony.pro.mwa.service.activity.StatusDetails;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.service.storage.IUriResolver;

/**
 *  Activity用のベースクラス
 *
 *  レイヤー責務
 *  ・継承先のActivity実装のため、Activityとしてふるまうための共通実装を提供すること。
 *
 *  要件
 *  ・Activity独自の状態遷移が追加できるよう拡張できること（ManualQCなど特殊なHumanTaskや、iAPDigitizeなど、独自の状態遷移拡張）
 *
 *  ☆継承先で内部のメンバを追加した際のpersistentデータ生成は継承先の責務。永続化自体はManagerが行う。
 *  ☆Device上のJobIdというかインスタンス特定手段は共通概念のような気がする　→　ケースバイケースなので継承先に任せる
 *
 */
public abstract class AbsMwaActivity implements IMwaActivityInternal, ILoggerSourceWithPrefix {

    /*-------------------------------------------------------------------------------------------
     *  メンバ変数
     -------------------------------------------------------------------------------------------*/
    protected ActivityInstanceModel model;
    IStateMachine stm;
    IStateMachine commonStm = CommonStateMachine.SINGLETON;
    Integer retryCount;	//現状、Retryロジックはなし。そのうちModelの方にいくかな。
    //Boolean isUpdated; //Setterは不要。インスタンス生成時から更新が起きたかを管理。trueならManagerがDBを更新。
    protected IState initState = null;//★暫定

    IActivityTemplate template;
    IActivityProvider provider;
    ICoreModules coreModules;

    @SuppressWarnings("serial")
    private final static List<String> dropInParamList = new ArrayList<String>() {{
//    	add(PresetParameter.ActivityTemplateName.getKey());
        add(PresetParameter.ParentInstanceId.getKey());
        add(PresetParameter.LocalName.getKey());
    }};

    @SuppressWarnings("serial")
    private final static List<String> dropOutParamList = new ArrayList<String>() {{
        add("PersistentData"); //★temp
        add(PresetParameter.Event.getKey());
        add(PresetParameter.Progress.getKey());
    }};

    //Loggerは親で生成したインスタンスを使ってもらう。InstanceIdの受け渡しが面倒なのでstaticとはしない。
    protected MwaLogger logger = MwaLogger.getLogger(this);

    /*-------------------------------------------------------------------------------------------
     *  外部I/F (for Activity Manager)
     -------------------------------------------------------------------------------------------*/
    /**
     * コンストラクタ
     * 初期stateと、状態遷移エンジンを指定することで、それらを用いて実装部依存のActivityとしてふるまう
     *
     * @param N/A
     * @return ActivityがサポートするEventのリスト
     */
    public AbsMwaActivity(IState state, IStateMachine stm) {
        this.model = new ActivityInstanceModel();
        this.model.setStatus(state);
        this.initState = state;
        this.stm = stm;
        this.retryCount = 0;
        this.template = null;
    }

    protected IEvent getCorrespondingEvent(String eventName) {
        IEvent event = null;
        List<IEvent> events = getSupportedEventList();
        for (IEvent ev : events) {
            //対応していればEventを処理メソッドに渡す
            if (ev.isMatch(eventName)) {
                event = ev;
                break;
            }
        }
        return event;
    }

    /**
     * Activityに対する操作用のI/F
     * 汎用的な受け口で引数のctrlType指定（内部ではIEvent名に対応）で実処理に振り分ける。
     * また、汎用パラメータの入力値チェックを行う（状態遷移は行わない）
     *
     * @param ctrlType 操作種別
     * @return 操作結果
     */
    @Override
    @SuppressWarnings("static-access")
    public OperationResult operate(String ctrlType, Map<String, Object> params) {
        OperationResult result = null;
        IEvent event = this.getCorrespondingEvent(ctrlType);

        //不要な入力ParameterをDrop
        if (params != null) {
            for (String val : this.dropInParamList) {
                params.remove(val);
            }
        }

        if (event != null) {
            //実処理前後でStateMachineの管理を行う
            if (isAcceptableState(event)) {
            	{
            		//[暫定] cancelを受け付けなくする対応。Cancelを受け付けない状態であることをユーザ通知すべきで、ActivityInstanceModelにもパラメータとして追加したい
            		if (CommonEvent.REQUEST_CANCEL.equals(event)) {
    	        		String ignoreCancel = (String)this.getInput(PresetParameter.IgnoreCancel.getKey());
    	        		//StateModelのケースもあるので状態に関しては文字列比較
    	        		if ("true".equalsIgnoreCase(ignoreCancel) && !CommonState.QUEUED.getName().equals(this.getStatus().getName())) {
    	        			String message = "Ignore CANCEL request: " + ", state=" + this.getStatus().getName() + ", event=" + event.getName();
    	                    logger.warn(message);
    	                    throw new MwaError(MWARC.OPERATION_FAILED_UNACCEPTABLE_STATE, null, message);
    	        		}
            		}
            	}

                //処理可能なためイベント処理
                result = processEventForErrorHandling(event, params);
            } else {
                //操作を受け付けられない状態ということでエラーを返します。
                String message = "Not acceptable state: " + ", state=" + this.getStatus().getName() + ", event=" + event.getName();
                logger.warn(message);
                if (CommonEvent.REQUEST_SUBMIT.equals(event))
                    throw new MwaInstanceError(MWARC.OPERATION_FAILED_UNACCEPTABLE_STATE, null, message);
                else if (this.getStatus().isTerminated()) {
                    //warning message のみとする
                } else
                    throw new MwaError(MWARC.OPERATION_FAILED_UNACCEPTABLE_STATE, null, message);
            }
        } else {
            //未サポートのイベント入力
            String message = "Unsupported event: " + ctrlType;
            logger.warn(message);
            throw new MwaError(MWARC.INVALID_INPUT_EVENT, null, message);
        }

        //不要な出力ParameterをDrop
        if (result != null) {
            for (String val : this.dropOutParamList) {
                result.remove(val);
            }
        }

        return result;
    }

    /**
     * 本ActivityがサポートするEventのリストを返す
     * 内部ではgetSupportedEventListImpl()をコールしており、実装先が返したサポートEventListに、共通Eventを加えてサポートEventListとしている
     *
     * @param N/A
     * @return ActivityがサポートするEventのリスト
     */
    @Override
    public List<IEvent> getSupportedEventList() {
        List<? extends IEvent> subEvents = getSupportedEventListImpl();
        List<IEvent> result = new ArrayList<IEvent>();

        //継承先のサポートEvents
        for (IEvent ev : subEvents) {
            result.add(ev);
        }
        //FrameworkのサポートEvents
        for (CommonEvent event : CommonEvent.values()) {
            result.add(event);
        }
        return result;
    }

    /**
     * 本ActivityがサポートするStateのリストを返す
     * 内部ではgetSupportedStateListImpl()をコールしており、実装先が返したサポートStateListに、共通Eventを加えてサポートStateListとしている
     *
     * @param N/A
     * @return ActivityがサポートするStateのリスト
     */
    public List<IState> getSupportedStateList() {
        List<? extends IState> subStates = getSupportedStateListImpl();
        List<IState> result = new ArrayList<IState>();
        //継承先のサポートStates
        for (IState state : subStates) {
            result.add(state);
        }
        //FrameworkのサポートStates
        for (CommonState state : CommonState.values()) {
            result.add(state);
        }
        return result;
    }

    @Override
    public String getPersistentData() {
        return getPersistentDataImpl();
    }

    @Override
    public void setPersistentData(String details) {
        setPersistentDataImpl(details);
        return;
    }

    //getter
    @Override
    public String getId() { return this.model.getId(); }
    @Override
    public IState getStatus() { return this.model.getStatus(); }
    @Override
    public Integer getProgress() {return this.model.getProgress();}
    @Override
    public Map<String, Object> getInputDetails() { return this.model.getInputDetails(); }
    @Override
    public Map<String, Object> getOutputDetails() { return this.model.getOutputDetails(); }
    @Override
    public Long getStartTime() { return this.model.getStartTime(); }
    @Override
    public Long getEndTime() { return this.model.getEndTime(); }
    @Override
    public Long getUpdateTime() { return this.model.getUpdateTime(); }
    @Override
    public IActivityTemplate getTemplate() { return this.template; }
    @Override
    public IActivityProvider getProvider() { return this.provider; }
    @Override
    public IUriResolver getUriResolver() { return this.getCoreModules().getUriResolver(); }
    @Override
    public String getParentId() { return this.model.getParentId(); }
    @Override
    public String getName() { return this.model.getName(); }
    @Override
    public StatusDetails getStatusDetails() { return this.model.getStatusDetails(); }
    @Override
    public Long getCreateTime() { return this.model.getCreateTime(); }
    @Override
    public Map<String, Object> getProperties() { return this.model.getProperties(); }

    public Object getInput(String key) {
        return this.model.getInputDetails().get(key);
    }
    public Object getOutput(String key) {
        return this.model.getOutputDetails().get(key);
    }
    public void setOutput(String key, Object value) {
        this.model.getOutputDetails().put(key, value);
        return;
    }
    public Object getProperty(String key) {
        return this.model.getProperties().get(key);
    }
    public void setProperty(String key, Object value) {
        if(this.model.getProperties() == null){
            this.model.setProperties(new HashMap<String, Object>());
        }
        this.model.getProperties().put(key, value);
        return;
    }

    @Override
    public void setTemplate(IActivityTemplate template) {
        // TODO Auto-generated method stub
        this.template = template;
        this.model.setTemplateId(template.getId());
        this.model.setTemplateName(template.getName());
        this.model.setTemplateVersion(template.getVersion());
    }

    @Override
    public void setProvider(IActivityProvider provider) {
        // TODO Auto-generated method stub
        this.provider = provider;
    }

    @Override
    public void setUriResolver(IUriResolver uriResolver) {
        // TODO Auto-generated method stub
        //this.uriResolver = uriResolver;
        throw new MwaError(MWARC.INTEGRATION_ERROR);
    }
    @Override
    public void setCoreModules(ICoreModules coreModules) {
        // TODO Auto-generated method stub
        this.coreModules = coreModules;
    }

    @Override
    public Boolean isSync() {return this instanceof IMwaSyncActivity;}

    @Override
    public ActivityInstanceModel getModel() { return this.model; }
    @Override
    public void setModel(ActivityInstanceModel model) {
        this.model = model;
        //Modelからは完全なStateが復元できないため、ここで行う（Templateから復元できればねぇ。。。）
        this.model.setStatus(this.getRealStatus(model.getStatus().getName()));
        if (!this.model.getStatus().isTerminated()) {
            if (model.getPersistentData() != null)
                this.setPersistentData(model.getPersistentData());
        }
    }
    private IState getRealStatus(String stateString) {
        List<? extends IState> subStates = getSupportedStateList();
        boolean isSupportedStateString = false;
        //継承先のサポートEvents
        for (IState st : subStates) {
            if (st.getName().equals(stateString)) {
                return st;
            }
        }
        logger.error("Not found proper state in supportedStateList(): stateString = " + stateString);
        throw new MwaError(MWARC.INTEGRATION_ERROR);
    }



    /*-------------------------------------------------------------------------------------------
     *  継承先との通信用メソッド（Activityからのデータ受け渡し）
     -------------------------------------------------------------------------------------------*/
    protected void setOutputDetails(Map<String, Object> activityOutput) {
        this.model.setOutputDetails(activityOutput);
    }

    protected ServiceEndpointModel getDefaultEndpoint() {
        if (this.provider != null && this.provider.getEndpoints() != null)
            return this.provider.getEndpoints().get(ServiceEndpointType.DEFAULT.name());
        else
            return null;
    }

    @Override
    public ICoreModules getCoreModules() {
        return this.coreModules;
    }

    /*-------------------------------------------------------------------------------------------
     *  継承先の実装メソッド（Activityの実処理）
     -------------------------------------------------------------------------------------------*/
    /**
     * サポートするRequestEventに応じてActivity依存の実処理を行う
     * 具体的には、submitやcancelなどのEventに対して
     *
     * @param event 発生したRequestEvent
     * @param params Eventに伴うパラメータ（key/value構成）
     * @return 操作結果
     */
    protected abstract OperationResult processEventImpl(IEvent event, Map<String, Object> params);

    /**
     * requestSubmitイベントに対する実処理
     * requestSubmitは必須のイベント処理なので継承先の実装を強制している
     *
     * @param N/A
     * @return Activity実装部がサポートするEventのリスト
     */
    protected abstract OperationResult requestSubmitImpl(Map<String, Object> params);

    /**
     * 本ActivityがサポートするEventのリストを返す
     * 継承先の実装部がサポートするEventのみリストとして返せばよい（継承元のFramework側がサポートするEventリストについては含める必要はない）
     *
     * @param N/A
     * @return Activity実装部がサポートするEventのリスト
     */
    protected abstract List<? extends IEvent> getSupportedEventListImpl();

    /**
     * 本ActivityがサポートするStateのリストを返す
     * 継承先の実装部がサポートするStateのみリストとして返せばよい（継承元のFramework側がサポートするStateリストについては含める必要はない）
     *
     * @param N/A
     * @return Activity実装部がサポートするStateのリスト
     */
    protected abstract List<? extends IState> getSupportedStateListImpl();

    public abstract OperationResultWithStatus confirmStatusImpl(Map<String, Object> params);

    public abstract String getPersistentDataImpl();
    public abstract void setPersistentDataImpl(String details);

    /**
     * requestCancelイベントに対する実処理
     * requestCancelは任意実装
     *
     * @param N/A
     * @return Activity実装部がサポートするEventのリスト
     */
    protected OperationResult requestCancelImpl(Map<String, Object> params) {
        throw new MwaError(MWARC.INVALID_INPUT_EVENT);
    }


    protected OperationResult notifyErrorImpl(Map<String, Object> params) {
        if (params != null) {
            //★なぜかここでErrorCodeなどがとれない・・？？？
            String rc = (String)params.get(PresetParameter.ErrorCode.getKey());
            throw new MwaInstanceError(
                    new MWARC_GENERIC(rc, rc),
                    (String)params.get(PresetParameter.DeviceResponseCode.getKey()),
                    (String)params.get(PresetParameter.DeviceResponseDetails.getKey()));
        } else {
            logger.error("NOTIFY_ERROR not include error info");
            throw new MwaError(MWARC.INVALID_INPUT_EVENT);
        }
    }


    /*-------------------------------------------------------------------------------------------
     *  内部の実装コード(レイヤロジックなど)
     -------------------------------------------------------------------------------------------*/
    /**
     * 引数指定のEventに応じてActivity依存の実処理を行う。具体的な依存処理は継承先まかせ。
     * 本メソッドではErrorHandlingを行う
     *
     * @param event 発生したRequestEvent
     * @param params Eventに伴うパラメータ（key/value構成）
     * @return 操作結果
     */
    protected OperationResult processEventForErrorHandling(IEvent event, Map<String, Object> params) {
        OperationResult result = null;
        try {
            result = processEventForEventType(event, params);
        } catch (MwaError e) {
            //MWARCの取り出し
            IMWARC rc = e.getMWARC();

            String message = "MwaError Occurred: rc=" + rc.code() + "("+ rc.name() + ")" + ", message=" + e.getMessage() + ", devCode=" + e.getDeviceResponseCode() + ", devMsg=" + e.getDeviceResponseDetails();
            result = OperationResult.newInstance(rc, message);

            //インスタンスエラーの条件は、InstanceError例外のキャッチ、または、リトライ処理が規定回数以上
            boolean isInstanceError = (this.retryCount > 10) || (e instanceof MwaInstanceError);

            //☆submitの失敗はInstanceErrorとみなす。ロールバックしてなかったことにするという手もあるが、workflow側との兼ね合いなど検討が必要。
            isInstanceError |= CommonEvent.REQUEST_SUBMIT.equals(event);

            if (isInstanceError) {
        		message += ", status=" + this.getStatus().getName() + ", isTerminated=" + this.getStatus().isTerminated();
                logger.error(message, e);
                //インスタンスエラーのため、エラー状態に遷移
                if (!this.getStatus().isTerminated()) 
                	this.stateTransition(CommonEvent.NOTIFY_ERROR, e);
                throw new MwaError(e);	//ハンドルしたのでMwaErrorにrewrap
            } else {
                logger.warn(message);
                //リトライカウントを増やす
                this.retryCount++;
            }
        } catch (Exception e) {
            //ExceptionをNOTIFY_ERRORイベントとみなして処理
            String message = "General Exception Occurred: event=" + uniqueName(event) + ", e=" + e.toString() + "message=" + e.getMessage();
            logger.error(message, e);
            result = OperationResult.newInstance(MWARC.SYSTEM_ERROR, message);

            //インスタンスエラーのため、エラー状態に遷移
            this.stateTransition(CommonEvent.NOTIFY_ERROR, MWARC.SYSTEM_ERROR);
            throw e;
        } finally {
            //modelにPersistentDataを引き渡し
            this.model.setPersistentData(this.getPersistentData());
        }
        return result;
    }

    /**
     * 引数指定のEventに応じてActivity依存の実処理を行う。具体的な依存処理は継承先まかせ。
     * 本メソッドではEventTypeの共通処理を行う
     *
     * @param event 発生したRequestEvent
     * @param params Eventに伴うパラメータ（key/value構成）
     * @return 操作結果
     */
    protected OperationResult processEventForEventType(IEvent event, Map<String, Object> params) {
        OperationResult result = OperationResult.newInstance(MWARC.SYSTEM_ERROR_IMPLEMENTATION);

        if (event.isRequestEvent() || event.isConfirmEvent() || event.isNotifyEvent()) {
            //イベント処理
            result = processEventForCommonEvent(event, params);

            if (result == null) {
                logger.error("Acitivy Invalid Implementation: name=" + this.template.getName() + " returns null as \"" + uniqueName(event) + "\" result.");
                throw new MwaInstanceError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
            }

            //下がEventを返してきた場合 （CallbackやPollingなど、Device側の状態通知イベントで、状態が未決定の際の受け口（例．NotifyState, getJobStatusなど）)
            if (result.containsEvent()) {
                //起きたEventを取り出し
                IEvent decidedEvent = (IEvent)result.get(PresetParameter.Event);
                //返却Eventで状態遷移を行う
                this.stateTransition(decidedEvent);
            } else if (event.isRequestEvent()) {
                //下でEventが生じなければ、RequestEventを正常処理できたものとして状態遷移
                this.stateTransition(event);
            } else {
                //Eventがなければ状態遷移はせず、やることなし
            }

        } else {
            //未知のErrorType。互換性エラーとみなしてINTEGRATION_ERRORとする。
            String message = "Unsupported Event Type: event=" + uniqueName(event);
            logger.error(message);
            throw new MwaError(MWARC.INTEGRATION_ERROR, null, message);
        }

        //Progressを更新
        Integer progress = (Integer)result.get(PresetParameter.Progress);
        if (progress != null)
            this.updateProgress(progress);
        else if (this.isSync() && this.getStatus().isTerminated() && !this.getStatus().isError() && !this.getStatus().isCancel())
            this.updateProgress(100);

        return result;
    }

    /**
     * 引数指定のEventに応じてActivity依存の実処理を行う。具体的な依存処理は継承先まかせ。
     * 本メソッドでは共通Event（Submit/Confirm）の振り分けをし、継承先の実処理を呼び出す。
     *
     * ★AOPでやるという手もあるものの、可読性が下がるわりにそこまでメリットないと判断
     * Activityへの操作に関して、インスタンスごとの排他を考慮する必要あり　→　DB上のデータがマスタであり、その排他の管理はManagerの責務。このレイヤでは意識する必要なし

     * @param event 発生したRequestEvent
     * @param params Eventに伴うパラメータ（key/value構成）
     * @return 操作結果
     */
    protected OperationResult processEventForCommonEvent(IEvent event, Map<String, Object> params) {
        OperationResult result = null;

        //このレイヤでハンドルが必要なものはここで解決
        if (CommonEvent.REQUEST_SUBMIT.equals(event)) {
            //リクエストの実処理
            result = requestSubmitImpl(params);
        } else if (CommonEvent.REQUEST_CANCEL.equals(event)) {
            if (CommonState.QUEUED.equals(this.getStatus())) {
                logger.info("Cancelling from queue.");
                result = OperationResult.newInstance();
            }
            else {
                result = requestCancelImpl(params);
            }
        } else if (CommonEvent.CONFIRM_STATUS.equals(event)) {
            result = confirmStatusImpl(params);
        } else if (CommonEvent.NOTIFY_ERROR.equals(event)) {
            result = notifyErrorImpl(params);
        } else {
            //その他Eventの切り出しは下に任せる。Eventに応じた実処理として継承先のロジックを呼び出し
            result = processEventImpl(event, params);
        }
        return result;
    }

    /**
     * 状態遷移の実行メソッド。引数EventとSTMに応じて内部のstateを更新する
     *
     * @param event 対象のEvent
     * @return 操作結果
     */
    protected void stateTransition(IEvent event) {
        stateTransition(event, MWARC.SUCCESS);
    }
    protected void stateTransition(IEvent event, IMWARC rc) {
    	String deviceResponse = this.getStatusDetails() != null ? this.getStatusDetails().getDeviceResponse() : null;
    	String deviceResponseDetails = this.getStatusDetails() != null ? this.getStatusDetails().getDeviceResponseDetails() : null;
        stateTransition(event, rc.name(), rc.code(), deviceResponse, deviceResponseDetails);
    }
    protected void stateTransition(IEvent event, MwaError e) {
        stateTransition(event, e.getMWARC().name(), e.getMWARC().code(), e.getDeviceResponseCode(), e.getDeviceResponseDetails());
    }
    protected void stateTransition(IEvent event, String errorName, String errorCode, String deviceResponseCode, String deviceResponseDetails) {
        if (event == null) {
            //入力値が不正
            String message = "StateTransition: invalid event - null";
            logger.error(message);
            throw new MwaError(MWARC.INVALID_INPUT_EVENT);
        } else {
            IState newState = getNextState(this.getStatus(), event);

            if (newState == null) {
                if (this.getStatus().isTerminated()) {
                    //Terminated状態からの遷移先がない場合にはwarnログで通知する。
                    String message = "StateTransition: allready terminated: state=" + uniqueName(this.getStatus());
                    logger.warn(message);
                    throw new MwaError(MWARC.INVALID_INPUT_EVENT);
                } else {
                    //状態遷移失敗★当座強制でERRORに遷移させる。ハンドリングで改善するかも
                    newState = CommonState.ERROR;
                    String message = "StateTransition: Cannot find next status from State Transition Machine:" + this.stm.getClass().getName();
                    logger.error(message);
                    message = "StateTransition(forcibly): " + uniqueName(this.getStatus()) + " -> (" + uniqueName(event) + ") -> " + uniqueName(newState);
                    this.model.setStatus(newState);
                    logger.error(message);
                }
            } else {
                if (!newState.equals(this.getStatus())) {
                    //状態遷移成功
                    logger.info("StateTransition: " + uniqueName(this.getStatus())  + " -> (" + uniqueName(event) + ") -> " + uniqueName(newState));
                    this.model.setStatus(newState);

                } else {
                    //状態遷移は起こらず。特にやることなし
                }
            }
        }

        //Terminate処理
        if (this.getStatus().isTerminated()) {
            //EndTimeをセット
            if (this.model.getEndTime() == null) {
                this.model.setEndTime(System.currentTimeMillis());
            }
            //StatusDetails (DeviceError) をセット
            if (this.model.getStatusDetails() == null || this.model.getStatusDetails().getResult() == null)  {
                if (errorCode == null) {
                    if (this.getStatus().isError()) {
                        errorName = MWARC.SYSTEM_ERROR.name();
                        errorCode = MWARC.SYSTEM_ERROR.code();
                    } else {
                        errorName = MWARC.SUCCESS.name();
                        errorCode = MWARC.SUCCESS.code();
                    }
                }
                this.model.setStatusDetails(new StatusDetails(errorName, errorCode, deviceResponseCode, deviceResponseDetails));
            }
        }

        return;
    }

    private String uniqueName(IState state) {
        return (state != null) ? state.getClass().getSimpleName() + "." + state.getName() : null;
    }
    private String uniqueName(IEvent event) {
        return (event != null) ? event.getClass().getSimpleName() + "." + event.getName() : null;
    }

    /**
     * 状態遷移の可否確認メソッド。引数EventとSTMに応じて内部のstateが更新できるか（次のstateがあるか）確認する
     *
     * @param event チェック対象のEvent
     * @return trueなら遷移可能
     */
    protected boolean isAcceptableState(IEvent event) {
        return (this.getNextState(this.getStatus(), event) != null) ? true : false;
        //return isAcceptableStateCommon(event) || stm.isAcceptable(this.getStatus(), event);
    }

    /**
     * 状態遷移の可否確認メソッドの共通処理。
     *
     * @param event チェック対象のEvent
     * @return trueなら遷移可能
     */
/*	protected boolean isAcceptableStateCommon(IEvent event) {
        //暫定対応（RM対応に伴い初期状態からの遷移をフックする）
        return this.commonStm.isAcceptable(this.getStatus(), event);
    }*/

    /**
     * 状態遷移のコア処理。共通イベントの遷移処理を行い、専用イベントは継承先の遷移ロジックを利用する。
     *
     * @param State 遷移元のState
     * @param event 遷移を起こすEvent
     * @return State 遷移先のState
     */
    protected IState getNextState(IState state, IEvent event) {

        //暫定対応（RM対応に伴い初期状態からの遷移をフックする）
        {
            if (!this.getStatus().isTerminated() && this.initState !=null && CommonEvent.REQUEST_SUBMIT.equals(event)) {
                return this.stm.getNextState(this.initState, event);
            }
        }

        IState result = null;
        result = this.commonStm.getNextState(this.getStatus(), event);
        if (result == null) {
            //継承先の状態遷移を使う
            result = this.stm.getNextState(this.getStatus(), event);
        }
        return result;
    }

    protected void updateProgress(Integer progress) {
        this.model.setProgress(progress);
    }

    @Override
    public String getLoggerPrefix() {
        return "[" + this.model.getId() + "]";
    }
}
