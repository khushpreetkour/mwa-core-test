package com.sony.pro.mwa.activity.framework.executor;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sony.pro.mwa.common.log.MwaLogger;

public class ExecutorServiceManager {
	
	static MwaLogger logger = MwaLogger.getLogger(ExecutorServiceManager.class);
	
	// Singleton
	private static final ExecutorServiceManager instance = new ExecutorServiceManager();
	
	// jobMap<jobid, jobCallableClass>
	private static Map<String, ExecutorBean> jobMap = new ConcurrentHashMap<String, ExecutorBean>();
	// executorMap<jobServiceName, ExecutorService>
	private static Map<String, ExecutorService> executorMap = new ConcurrentHashMap<String, ExecutorService>();

	
	private ExecutorServiceManager(){
	}
	
	/**
	 * Get Singleton Instance
	 * @return
	 */
	public static ExecutorServiceManager getInstance(){
		return instance;
	}
	
	/**
	 * Close Manager
	 * This method called from ServletContextListener@contextDestroyed
	 */
	public void closeManager(){

		for(String executeorKey : ExecutorServiceManager.executorMap.keySet()){
			ExecutorService executor = ExecutorServiceManager.executorMap.get(executeorKey);
			if(null != executor && !executor.isShutdown()){
				executor.shutdown();
				System.out.println("Shutdown concurrent@Executor");
				logger.info("Shutdown concurrent@Executor");
			}
		}
	}
	
	public void submit(String instanceId, String jobServiceName, Integer threadPoolSize,ExecutorTaskCallable instance){
		ExecutorBean executorBean = new ExecutorBean();
		executorBean.setjobServiceName(jobServiceName);
		executorBean.setTask(instance);
		jobMap.put(instanceId, executorBean);
		ExecutorService executor = getInstance().getExecutor(jobServiceName, threadPoolSize);
		instance.setFuture(executor.submit(instance));
	}
	
	private  ExecutorService getExecutor(String key, Integer threadPoolSize){
		ExecutorService executor = executorMap.get(key);
		if(null == executor){
			executor = Executors.newScheduledThreadPool(threadPoolSize);
			System.out.println("Create concurrent@Executor "+key);
			logger.info("Create concurrent@Executor "+key);
			executorMap.put(key, executor);
		}else{
			if(executor.isShutdown()){
				executor = Executors.newScheduledThreadPool(threadPoolSize);
				System.out.println("Create concurrent@Executor "+key);
				executorMap.put(key, executor);
			}
		}
		return executor;
	}
	
	public ExecutorTaskCallable getActivityInstance(String jobId){
		ExecutorBean task = jobMap.get(jobId);
		if( null != task){
			return task.getTask();
		} else {
			return null;
		}
	}
	
	public void removeActivityInstance(String jobId){
		String jobServiceName = jobMap.get(jobId).getjobServiceName();
		
		jobMap.remove(jobId);
		for(Map.Entry<String, ExecutorServiceManager.ExecutorBean> entry : jobMap.entrySet()){
			ExecutorBean excutorBean = entry.getValue();
			if(jobServiceName.equals(excutorBean.getjobServiceName())){
				return;
			}
		}
		executorMap.get(jobServiceName).shutdown();
	}
	
	public static Set<String> getJobIdList(){
		return jobMap.keySet();
	}
	
	public static Set<String> getExecutorNameList(){
		return executorMap.keySet();
	}
	
	private class ExecutorBean{
		String jobServiceName;
		ExecutorTaskCallable task;

		public String getjobServiceName() {
			return jobServiceName;
		}
		public void setjobServiceName(String jobServiceName) {
			this.jobServiceName = jobServiceName;
		}
		public ExecutorTaskCallable getTask() {
			return task;
		}
		public void setTask(ExecutorTaskCallable task) {
			this.task = task;
		}
	}
}
