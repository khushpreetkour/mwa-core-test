package com.sony.pro.mwa.activity.framework.standard.sample;

import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.rc.MWARCUtil;

public enum MWARC_SAMPLE implements IMWARC {
	INVALID_TESTNAME("0001"),
	;

	String groupId = "1000"; //Test用の予約済みgroupId
	String localId;
	
	private MWARC_SAMPLE(String localId) {
		this.localId = localId;
	}

	@Override
	public String code() {
		return MWARCUtil.genMWARC(this.groupId, this.localId);
	}

	@Override
	public boolean isSuccess() {
		return false;
	}
}
