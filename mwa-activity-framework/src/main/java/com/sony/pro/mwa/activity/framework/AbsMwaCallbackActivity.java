package com.sony.pro.mwa.activity.framework;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsMwaCallbackActivity extends AbsMwaActivity {
	public AbsMwaCallbackActivity(IState state, IStateMachine stm) {
		super(state, stm);
	}
	
	/**
	 * Callbackの場合本メソッドの実装は不要
	 * 
	 * @param N/A
	 * @return Activity実装部がサポートするStateのリスト
	 */
	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		OperationResultWithStatus result = OperationResultWithStatus.newInstance((IEvent)null, (Integer)null);
		return result;
	}
}
