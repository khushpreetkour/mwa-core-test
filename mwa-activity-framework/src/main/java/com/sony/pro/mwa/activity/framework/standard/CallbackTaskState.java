package com.sony.pro.mwa.activity.framework.standard;

import com.sony.pro.mwa.service.activity.IState;

public enum CallbackTaskState implements IState {
				//stable	//terminated	//error		//cancel
	CREATED		(true,		false,			false,		false),
	IN_PROGRESS	(true,		false,			false,		false),
	COMPLETED	(true,		true,			false,		false),
	ERROR		(true,		true,			true,		false),
	CANCELLING	(true,		false,			false,		true),
	CANCELLED	(true,		true,			false,		true),
	;
	
	private boolean stableFlag;
	private boolean terminatedFlag;
	private boolean errorFlag;
	private boolean cancelledFlag;
	
	private CallbackTaskState (boolean stableFlag, boolean terminatedFlag, boolean errorFlag, boolean cancelledFlag) {
		this.stableFlag = stableFlag;
		this.terminatedFlag = terminatedFlag;
		this.errorFlag = errorFlag;
		this.cancelledFlag = cancelledFlag;
	}
	
	public String getName() { return super.name(); }
	
	//Default
	public boolean isTerminated() { return this.terminatedFlag; }
	public boolean isStable() { return this.stableFlag; }
	public boolean isError() { return this.errorFlag; }
	public boolean isCancel() { return this.cancelledFlag; }

}
