package com.sony.pro.mwa.activity.framework.standard;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.sony.pro.mwa.activity.framework.stm.AbsStateMachine;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class CallbackTaskStateMachine extends AbsStateMachine {
	
	//状態遷移表の定義
	private final static Object[][] stTableDef = {
			//CurrentState,			//Event,						//NextState
		{	CallbackTaskState.CREATED,		CommonEvent.REQUEST_SUBMIT,			CallbackTaskState.IN_PROGRESS},
		{	CallbackTaskState.CREATED,		CommonEvent.NOTIFY_ERROR,			CallbackTaskState.ERROR},
		{	CallbackTaskState.IN_PROGRESS,	CommonEvent.REQUEST_CANCEL,			CallbackTaskState.CANCELLING},
		{	CallbackTaskState.IN_PROGRESS,	CallbackTaskEvent.NOTIFY_COMPLETED,		CallbackTaskState.COMPLETED},
		{	CallbackTaskState.IN_PROGRESS,	CallbackTaskEvent.NOTIFY_UPDATED,		CallbackTaskState.IN_PROGRESS},
		{	CallbackTaskState.IN_PROGRESS,	CommonEvent.NOTIFY_ERROR,			CallbackTaskState.ERROR},
		{	CallbackTaskState.CANCELLING,	CallbackTaskEvent.NOTIFY_UPDATED,		CallbackTaskState.CANCELLING},
		{	CallbackTaskState.CANCELLING,	CallbackTaskEvent.NOTIFY_COMPLETED,		CallbackTaskState.COMPLETED},
		{	CallbackTaskState.CANCELLING,	CommonEvent.NOTIFY_ERROR,			CallbackTaskState.ERROR},
		{	CallbackTaskState.CANCELLING,	CallbackTaskEvent.NOTIFY_CANCELLED,		CallbackTaskState.CANCELLED},
	};

	private final static HashMap<Pair<IState, IEvent>, IState> stTable = new HashMap<Pair<IState, IEvent>, IState>() {{
		//初期化処理、stTableDefをMapに詰めてるだけ(直接Mapで定義してもよかったが、定義を配列で切り出した方がシンプルで管理しやすいので)
		for (int i = 0; i < stTableDef.length; i++) {
			put(Pair.of((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	@Override
	protected Map<Pair<IState, IEvent>, IState> getStateTransitionTable() {
		return stTable;
	}
}
