package com.sony.pro.mwa.bundle;

import java.util.List;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.service.provider.ActivityProviderType;

public abstract class MwaBundle implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {

		MwaBundleContentManager contentMgr = null;
		ServiceReference<MwaBundleContentManager> ref = context.getServiceReference(MwaBundleContentManager.class);
		
		if (ref != null) {
			contentMgr = context.getService(ref);
			context.ungetService(ref);
		}
		if (contentMgr == null) {
			contentMgr = new MwaBundleContentManager();
		}

		contentMgr.addTemplates(context.getBundle().getBundleId(), this.getTemplates());
		contentMgr.addProviderTypes(context.getBundle().getBundleId(), this.getProviderTypes());
		context.registerService(MwaBundleContentManager.class, contentMgr, null);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
	}

	/**
	 * MWA Activity developer need to implement the getTemplates() method for MWA plugin jar (= MWA bundle).
	 * MWA plugin can contains multiple activity templates. (e.g. mwa-activity-dsv.jar can contains IngestTask, QcTask templates)
	 * This method needs to return list of supported template by this plugin.
	 */
	public abstract List<ActivityTemplate> getTemplates();
	
	public List<ActivityProviderType> getProviderTypes() {
		return null;
	}
}
