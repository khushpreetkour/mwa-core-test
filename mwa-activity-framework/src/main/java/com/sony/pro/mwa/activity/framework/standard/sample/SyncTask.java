package com.sony.pro.mwa.activity.framework.standard.sample;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.parameter.OperationResult;

public class SyncTask extends SyncTaskBase {

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		//ActivityのOutputをセット
		this.setOutput("TestOutput", "SyncTaskCompleted");
		//★OperationResultにActivityのOutputをセットした方が、Syncとしては使いやすいはず(だけど、実処理は非同期処理なので現状無理)
		OperationResult result = OperationResult.newInstance();
		result.put(this.getOutputDetails());
		return result;
	}
}
