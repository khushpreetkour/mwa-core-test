package com.sony.pro.mwa.activity.framework.internal;

import com.sony.pro.mwa.service.activity.IState;

public class GeneralStateImpl implements IState {

	public GeneralStateImpl(String name, boolean terminatedFlag, boolean stableFlag, boolean errorFlag, boolean cancelFlag) {
		this.name = name;
		this.terminatedFlag = terminatedFlag;
		this.stableFlag = stableFlag;
		this.errorFlag = errorFlag;
		this.cancelFlag = cancelFlag;
	}
	
	String name;
	boolean terminatedFlag;
	boolean stableFlag;
	boolean errorFlag;
	boolean cancelFlag;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isTerminated() {
		return terminatedFlag;
	}

	@Override
	public boolean isStable() {
		return stableFlag;
	}

	@Override
	public boolean isError() {
		return errorFlag;
	}

	@Override
	public boolean isCancel() {
		return cancelFlag;
	}
	
	public void setTerminatedFlag(boolean terminatedFlag) {
		this.terminatedFlag = terminatedFlag;
	}
	public void setStableFlag(boolean stableFlag) {
		this.stableFlag = stableFlag;
	}
	public void setErrorFlag(boolean errorFlag) {
		this.errorFlag = errorFlag;
	}
	public void setCancelFlag(boolean cancelFlag) {
		this.cancelFlag = cancelFlag;
	}

}
