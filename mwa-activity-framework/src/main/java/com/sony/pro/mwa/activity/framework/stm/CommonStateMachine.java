package com.sony.pro.mwa.activity.framework.stm;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public enum CommonStateMachine implements IStateMachine {
	SINGLETON
	;
	
	//State Transition Table
	private final static Object[][] stTableDef = {
			//CurrentState,					//Event,								//NextState
		{	CommonState.QUEUED,				CommonEvent.REQUEST_CANCEL,				CommonState.CANCELLED},
		{	CommonState.READY,				CommonEvent.REQUEST_CANCEL,				CommonState.CANCELLED},
	};

	private final static HashMap<Pair<IState, IEvent>, IState> stTable = new HashMap<Pair<IState, IEvent>, IState>() {{
		//Initialization
		for (int i = 0; i < stTableDef.length; i++) {
			put(Pair.of((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	protected Map<Pair<IState, IEvent>, IState> getStateTransitionTable() {
		return stTable;
	}

	public boolean isAcceptable(IState current, IEvent event) {
		IState next = getNextState(current, event);
		return (next != null) ? true : false;
	}

	public IState getNextState(IState state, IEvent event) {
		if (!state.isTerminated() && CommonEvent.NOTIFY_ERROR.equals(event))  {
			//Fatal状態に遷移
			return CommonState.ERROR;
		} else if (!state.isTerminated() && CommonEvent.CONFIRM_STATUS.equals(event))  {
			//遷移しない
			return state;
		}

		Map<Pair<IState, IEvent>, IState> tbl = this.getStateTransitionTable();
		//Mapから取り出して返す
		if (tbl != null) 
			return tbl.get(Pair.of(state, event));
		else
			return null;
	}
}
