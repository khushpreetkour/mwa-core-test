package com.sony.pro.mwa.activity.framework.stm;

import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public interface IStateMachine {
	public boolean isAcceptable(IState current, IEvent event);
	public IState getNextState(IState state, IEvent event);
}
