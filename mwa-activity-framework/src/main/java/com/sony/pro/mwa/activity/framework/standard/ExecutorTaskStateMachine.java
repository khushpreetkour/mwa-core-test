package com.sony.pro.mwa.activity.framework.standard;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.sony.pro.mwa.activity.framework.stm.AbsStateMachine;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class ExecutorTaskStateMachine extends AbsStateMachine {
	
	private static final ExecutorTaskStateMachine instance = new ExecutorTaskStateMachine();
	
	//状態遷移表の定義
	private final static Object[][] stTableDef = {
			//CurrentState,			//Event,						//NextState
		{	ExecutorTaskState.CREATED,		CommonEvent.REQUEST_SUBMIT,				ExecutorTaskState.SUBMITTED},
		{	ExecutorTaskState.CREATED,		CommonEvent.NOTIFY_ERROR,				ExecutorTaskState.ERROR},
		
		{	ExecutorTaskState.READY,		CommonEvent.REQUEST_CANCEL,				ExecutorTaskState.CANCELLED},

		{	ExecutorTaskState.SUBMITTED,	ExecutorTaskEvent.NOTIFY_READY,			ExecutorTaskState.SUBMITTED},
		{	ExecutorTaskState.SUBMITTED,	CommonEvent.REQUEST_CANCEL,				ExecutorTaskState.CANCELLING},
		{	ExecutorTaskState.SUBMITTED,	ExecutorTaskEvent.NOTIFY_IN_PROGRESS,	ExecutorTaskState.IN_PROGRESS},
		{	ExecutorTaskState.SUBMITTED,	ExecutorTaskEvent.NOTIFY_CANCELLED,		ExecutorTaskState.CANCELLED},
		{	ExecutorTaskState.SUBMITTED,	ExecutorTaskEvent.NOTIFY_COMPLETED,		ExecutorTaskState.COMPLETED},
		{	ExecutorTaskState.SUBMITTED,	CommonEvent.NOTIFY_ERROR,				ExecutorTaskState.ERROR},

		{	ExecutorTaskState.IN_PROGRESS,	CommonEvent.REQUEST_CANCEL,				ExecutorTaskState.CANCELLING},
		{	ExecutorTaskState.IN_PROGRESS,	ExecutorTaskEvent.NOTIFY_COMPLETED,		ExecutorTaskState.COMPLETED},
		{	ExecutorTaskState.IN_PROGRESS,	ExecutorTaskEvent.NOTIFY_IN_PROGRESS,	ExecutorTaskState.IN_PROGRESS},
		{	ExecutorTaskState.IN_PROGRESS,	CommonEvent.NOTIFY_ERROR,				ExecutorTaskState.ERROR},
		
		{	ExecutorTaskState.CANCELLING,	ExecutorTaskEvent.NOTIFY_IN_PROGRESS,	ExecutorTaskState.CANCELLING},
		{	ExecutorTaskState.CANCELLING,	ExecutorTaskEvent.NOTIFY_COMPLETED,		ExecutorTaskState.CANCELLED},
		{	ExecutorTaskState.CANCELLING,	ExecutorTaskEvent.NOTIFY_CANCELLED,		ExecutorTaskState.CANCELLED},
		{	ExecutorTaskState.CANCELLING,	CommonEvent.NOTIFY_ERROR,				ExecutorTaskState.ERROR},
	};

	private final static HashMap<Pair<IState, IEvent>, IState> stTable = new HashMap<Pair<IState, IEvent>, IState>() {{
		//初期化処理、stTableDefをMapに詰めてるだけ(直接Mapで定義してもよかったが、定義を配列で切り出した方がシンプルで管理しやすいので)
		for (int i = 0; i < stTableDef.length; i++) {
			put(Pair.of((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	@Override
	protected Map<Pair<IState, IEvent>, IState> getStateTransitionTable() {
		return stTable;
	}
}
