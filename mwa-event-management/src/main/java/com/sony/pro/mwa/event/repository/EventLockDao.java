package com.sony.pro.mwa.event.repository;

import com.sony.pro.mwa.event.model.EventLockCollection;
import com.sony.pro.mwa.event.model.EventLockModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface EventLockDao extends ModelBaseDao<EventLockModel, EventLockCollection> {
}
