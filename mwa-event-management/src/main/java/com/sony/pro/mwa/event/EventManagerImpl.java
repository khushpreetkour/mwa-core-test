package com.sony.pro.mwa.event;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.event.EventTargetType;
import com.sony.pro.mwa.event.model.EventLockModel;
import com.sony.pro.mwa.event.repository.EventDao;
import com.sony.pro.mwa.event.repository.EventLockDao;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.event.EventCollection;
import com.sony.pro.mwa.model.event.EventModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.service.workflow.IWorkflowManager;

@Component
public class EventManagerImpl implements IEventManager {

	private final static MwaLogger logger = MwaLogger.getLogger(EventManagerImpl.class);

	private IActivityManager activityManager;
	private IResourceManager resourceManager;
	private IWorkflowManager workflowManager;
	private EventDao eventDao;
	private EventLockDao eventLockDao;
	private boolean releaseAllEventLockFlag = true;

	static int THREAD_COUNT = 40;
	static String eventProcesserId = UUID.randomUUID().toString();
	ScheduledExecutorService executor;

	private static class RunnableEventProcesser implements Runnable {
		EventManagerImpl mgr;

		@Override
		public void run() {
			try {
				mgr.processEvent("" + Thread.currentThread().getId());
			} catch (Exception e) {
				logger.error("[" + Thread.currentThread().getId() + "] Event Thread cansed exception: ", e);
			} finally {
			}
		}
		public Runnable setMgr(EventManagerImpl mgr){
			this.mgr = mgr;
			return this;
		}
	}
	public void initialize() {
	    if(releaseAllEventLockFlag) {
	        eventDao.releaseAllEventLock();   //lockの解除
	    }
		executor = Executors.newScheduledThreadPool(THREAD_COUNT);
		for (int i=0; i < THREAD_COUNT; i++) {
			executor.scheduleAtFixedRate(new RunnableEventProcesser().setMgr(this), 1, 3, TimeUnit.SECONDS);
		}

		logger.info("EventManager started!!");
	}
	public void setReleaseAllEventLockFlag(boolean releaseAllEventLockFlag) {
	    this.releaseAllEventLockFlag = releaseAllEventLockFlag;
	}
	public void destroy() {
		if (executor != null) {
			try {
				executor.shutdown();
				if(!executor.awaitTermination(5*1000, TimeUnit.MILLISECONDS)){
		            // タイムアウトした場合、全てのスレッドを中断(interrupted)してスレッドプールを破棄する。
					executor.shutdownNow();
		        }
		    } catch (InterruptedException e) {
		        // awaitTerminationスレッドがinterruptedした場合も、全てのスレッドを中断する
		        logger.warn("awaitTermination interrupted: " + e);
		        executor.shutdownNow();
		    }
		}
		logger.info("EventManager stoped!!");
		return;
	}

	@Autowired
	@Qualifier("eventDao")
	public void setEventDao(EventDao dao) {
		this.eventDao = dao;
	}

	@Autowired
	@Qualifier("eventLockDao")
	public void setEventLockDao(EventLockDao dao) {
		this.eventLockDao = dao;
	}

	@Autowired
	@Qualifier("activityManager")
	public void setActivityManager(IActivityManager activityManager) {
		this.activityManager = activityManager;
	}

	@Autowired
	@Qualifier("resourceManager")
	public void setResourceManager(IResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}

	@Autowired
	@Qualifier("workflowManager")
	public void setWorkflowManager(IWorkflowManager workflowManager) {
		this.workflowManager = workflowManager;
	}

	public EventModel getEvent(String id) {
		EventModel model;
		try {
			model = this.eventDao.getModel(id, null);
		} catch (Throwable e) {
			logger.error("getEvent: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null)
			throw new MwaError(MWARC.INVALID_INPUT_EVENT);

		return model;
	}
	public EventCollection getEvents(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		EventCollection models;
		try {
			models = this.eventDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getEvents: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return models;
	}

	public EventModel addEvent(EventModel model) {
		if (model == null)
			return null;
		if (model.getId() == null)
			model.setId(UUID.randomUUID().toString());
		model.setCreateTime(new java.util.Date().getTime());
		EventModel result = this.eventDao.addModel(model);
		return result;
	}

	public EventModel updateEvent(EventModel model) {
		EventModel result = this.eventDao.updateModel(model);
		return result;
	}

	@Override
	public EventModel deleteEvent(String id) {
		EventModel result = this.eventDao.deleteModel(this.getEvent(id));
		return result;
	}
	@Override
	public EventCollection deleteEvents(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		EventCollection models = this.eventDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		if (models != null) {
			for (EventModel model : models.getModels()) {
				this.deleteEvent(model.getId());
			}
		}
		return models;
	}

	public Boolean lockEvent(String exclusionId) {
        return this.eventDao.reserveEvents("" + Thread.currentThread().getId(), exclusionId);

	}
    public Boolean unlockEvent(String exclusionId) {
        return this.eventDao.releaseEvents(exclusionId);
    }

	public void processEvent(String threadId) {
		List<EventModel> models = this.eventDao.popTopPriorityModels(threadId, 1);

		if (models != null && !models.isEmpty()) {
			for (EventModel model :  models) {
//logger.info("processEvent start: " + model.getId() + ", " + model.getName() + ", " + model.getExclusionId()); long t1 = System.currentTimeMillis();
				try {
					//とりあえずActivityManagerに投げ込む、RMへの投げ分けはまだ実装しない
					if (EventTargetType.ACTIVITY_INSTANCE.equals(model.getTargetType())) {
						OperationResult result = this.activityManager.operateActivity(model.getTargetId(), model.getName(), model.getParams());
					} else if (EventTargetType.QUEUE.equals(model.getTargetType())) {
						this.resourceManager.tryDispatch(threadId, model.getTargetId(), model.getName());
					} else if (EventTargetType.WORKFLOW.equals(model.getTargetType())) {
					    String targetId = (String)model.getParams().get(PresetParameter.ActivityInstanceId.getKey());
						this.workflowManager.notifyStepDone(targetId);
					} else {
						logger.warn("Unkown event target type: type=" + model.getTargetType());
					}
				} catch (Throwable e) {
					logger.error("processEvent caused exception: event(" + model.getName() + ", " + model.getTargetId() +  "), e=" + e.toString(), e);
				}
//long t2 = System.currentTimeMillis();
//logger.info("processEvent end  : time=" + (t2-t1) + ", " + model.getId() + ", " + model.getName() + ", " + model.getExclusionId());
			}
			this.eventDao.releaseEvents(models.get(0).getExclusionId());
		}
	}
}
