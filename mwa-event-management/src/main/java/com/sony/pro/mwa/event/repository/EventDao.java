package com.sony.pro.mwa.event.repository;

import java.util.List;

import com.sony.pro.mwa.model.event.EventCollection;
import com.sony.pro.mwa.model.event.EventModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface EventDao extends ModelBaseDao<EventModel, EventCollection> {
	public List<EventModel> popTopPriorityModels(String eventProcesserId, int count);
    public Boolean reserveEvents(final String eventProcesserId, String exclusionId);
	public Boolean releaseEvents(String exclusionId);
	public Boolean releaseAllEventLock();
}
