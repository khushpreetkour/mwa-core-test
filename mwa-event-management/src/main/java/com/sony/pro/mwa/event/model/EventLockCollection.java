package com.sony.pro.mwa.event.model;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class EventLockCollection extends Collection<EventLockModel> {
    public EventLockCollection() {
        super();
    }

    public EventLockCollection(List<EventLockModel> models) {
        super(models);
    }

    public EventLockCollection(List<EventLockModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public EventLockCollection(Long totalCount, Long count, List<EventLockModel> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<EventLockModel> getModels() {
        return super.getModels();
    }
}
