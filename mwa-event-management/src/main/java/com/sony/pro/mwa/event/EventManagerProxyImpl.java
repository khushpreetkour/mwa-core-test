package com.sony.pro.mwa.event;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.utils.rest.HttpMethodType;
import com.sony.pro.mwa.utils.rest.IRestInput;
import com.sony.pro.mwa.utils.rest.RestClient;
import com.sony.pro.mwa.utils.rest.RestInputImpl;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.event.EventTargetType;
import com.sony.pro.mwa.utils.RestUtils;
import com.sony.pro.mwa.model.event.EventCollection;
import com.sony.pro.mwa.model.event.EventModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.resource.IResourceManager;

@Component
public class EventManagerProxyImpl implements IEventManager {

	private final static MwaLogger logger = MwaLogger.getLogger(EventManagerProxyImpl.class);

	protected RestClient restClient = new RestClient("");
	private static final String URL_FORMAT = "http://%s:%s/mwa/api/v2/events";
	private static final String URL_ADDED_ID_FORMAT = "http://%s:%s/mwa/api/v2/events/%s";

	// TODO Multi-MWAの通信先の情報、host,portはとりあえずハードコーディング
	private static final String HOST = "localhost";
	private static final String PORT = "8081";

	private IActivityManager activityManager;
	private IResourceManager resourceManager;

	ScheduledExecutorService executor;
	ScheduledFuture<?> future;

	public void initialize() {
		executor = Executors.newScheduledThreadPool(2);
		future = executor.scheduleAtFixedRate(new Runnable() {
			EventManagerProxyImpl mgr;
			@Override
			public void run() {
				try {
					mgr.processEvent("" + Thread.currentThread().getId());
				} catch (Exception e) {
					logger.error("[" + Thread.currentThread().getId() + "] Event Thread cansed exception: ", e);
				}
			}
			public Runnable setMgr(EventManagerProxyImpl mgr){ this.mgr = mgr; return this; }
		}.setMgr(this), 1, 1, TimeUnit.SECONDS);

		logger.info("EventManager started!!");
	}
	public void destroy() {
		if (executor != null) {
			try {
				executor.shutdown();
				if(!executor.awaitTermination(5*1000, TimeUnit.MILLISECONDS)){
		            // タイムアウトした場合、全てのスレッドを中断(interrupted)してスレッドプールを破棄する。
					executor.shutdownNow();
		        }
		    } catch (InterruptedException e) {
		        // awaitTerminationスレッドがinterruptedした場合も、全てのスレッドを中断する
		        logger.warn("awaitTermination interrupted: " + e);
		        executor.shutdownNow();
		    }
		}
		logger.info("EventManager stoped!!");
		return;
	}

	@Autowired
	@Qualifier("activityManager")
	public void setActivityManager(IActivityManager activityManager) {
		this.activityManager = activityManager;
	}

	@Autowired
	@Qualifier("resourceManager")
	public void setResourceManager(IResourceManager resourceManager) {
		this.resourceManager = resourceManager;
	}

	private String getUrl(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlのパスまで生成
		String url = getUrl();
		// queryを生成
		String query = RestUtils.createQuery(sorts, filters, offset, limit);
		// queryがnullでなければ、urlと結合する
		if (null != query) {
			url = url + "?" + query;
		}
		return url;
	}

	private String getUrl() {
		return String.format(URL_FORMAT, HOST, PORT);
	}

	private String getUrl(String id) {
		return String.format(URL_ADDED_ID_FORMAT, HOST, PORT, id);
	}

	public EventModel getEvent(String id) {
		// urlを取得
		String url = getUrl(id);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		EventModel model = (EventModel) RestUtils.convertToObj(restClient.getResponseBody(), EventModel.class);

		return model;
	}
	public EventCollection getEvents(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		EventCollection models = (EventCollection) RestUtils.convertToObj(restClient.getResponseBody(), EventCollection.class);

		return models;
	}

	public EventModel addEvent(EventModel model) {
		if (model == null)
			return null;
		// urlを取得
		String url = getUrl();
		// LocationModelをJson形式に変換
		String json = RestUtils.convertToJson(model);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.POST.name(), url, json);
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		EventModel result = (EventModel) RestUtils.convertToObj(restClient.getResponseBody(), EventModel.class);

		return result;
	}

	public EventModel updateEvent(EventModel model) {
		// urlを取得
		String url = getUrl(model.getId());
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.PUT.name(), url, RestUtils.convertToJson(model));
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		EventModel result = (EventModel) RestUtils.convertToObj(restClient.getResponseBody(), EventModel.class);

		return result;
	}

	@Override
	public EventModel deleteEvent(String id) {
		// urlを取得
		String url = getUrl(id);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		EventModel result = (EventModel) RestUtils.convertToObj(restClient.getResponseBody(), EventModel.class);
		return result;
	}
	@Override
	public EventCollection deleteEvents(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		EventCollection models = (EventCollection) RestUtils.convertToObj(restClient.getResponseBody(), EventCollection.class);
		return models;
	}

	//★Transaction管理がしたいところ
	public void processEvent(String threadId) {
		List<String> sorts = new ArrayList<>();
		sorts.add("createTime" + "+");
		List<String> filters = new ArrayList<>();
		filters.add("exclusionId" + FilterOperatorEnum.EQUAL.toSymbol() + "");
		EventCollection models = this.getEvents(sorts, filters, null, null);
		for (EventModel model :  models.getModels()) {
			try {
				//とりあえずActivityManagerに投げ込む、RMへの投げ分けはまだ実装しない
				if (EventTargetType.ACTIVITY_INSTANCE.equals(model.getTargetType())) {
					OperationResult result = this.activityManager.operateActivity(model.getTargetId(), model.getName(), model.getParams());
				} else if (EventTargetType.RESOURCE.equals(model.getTargetType())) {
					this.resourceManager.tryDispatch(threadId, model.getTargetId(), model.getName());
				} else {
					logger.warn("Unkown event target type: type=" + model.getTargetType());
				}
			} catch (Exception e) {
				logger.error("processEvent caused exception: event(" + model.getName() + ", " + model.getTargetId() +  "), e=" + e.toString(), e);
			} finally {
				this.deleteEvent(model.getId());
			}
		}
	}
    @Override
    public Boolean lockEvent(String exclusionId) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public Boolean unlockEvent(String exclusionId) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public void setReleaseAllEventLockFlag(boolean releaseAllEventLockFlag) {
        // TODO 自動生成されたメソッド・スタブ

    }
}
