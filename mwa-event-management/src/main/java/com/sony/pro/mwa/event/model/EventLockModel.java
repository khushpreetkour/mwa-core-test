package com.sony.pro.mwa.event.model;

public class EventLockModel {
	String processerId;
	String exclusionId;

	public EventLockModel() {
	}
	public EventLockModel(String processerId, String exclusionId) {
		this.processerId = processerId;
		this.exclusionId = exclusionId;
	}

	public String getProcesserId() {
		return processerId;
	}
	public EventLockModel setProcesserId(String processerId) {
		this.processerId = processerId;
		return this;
	}
	public String getExclusionId() {
		return exclusionId;
	}
	public EventLockModel setExclusionId(String exclusionId) {
		this.exclusionId = exclusionId;
		return this;
	}
}
