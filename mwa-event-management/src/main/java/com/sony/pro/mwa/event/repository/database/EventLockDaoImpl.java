package com.sony.pro.mwa.event.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.event.model.EventLockCollection;
import com.sony.pro.mwa.event.model.EventLockModel;
import com.sony.pro.mwa.event.repository.EventLockDao;
import com.sony.pro.mwa.event.repository.database.query.EventLockQuery;
import com.sony.pro.mwa.exception.MwaError;

@Repository
public class EventLockDaoImpl extends DatabaseBaseDao<EventLockModel, EventLockCollection, QueryCriteria> implements EventLockDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public EventLockCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public EventLockModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.event_lock(" + 
					"  event_processer_id" + 
					", event_exclusion_id" +
					") " +
			"VALUES (" +
					"?, ?" +
					") ";

	public EventLockModel addModel(final EventLockModel model) {
		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;
						
						ps.setString(colNum++, model.getProcesserId());
						if (model.getExclusionId() != null) 
							ps.setString(colNum++, model.getExclusionId());
						else
							ps.setString(colNum++, UUID.randomUUID().toString());

						//logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);

		EventLockModel addedModel = model;
		return insertedRowCount != 0 ? addedModel : null;
	}

	public EventLockModel updateModel(final EventLockModel model) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.event_lock " +
			"WHERE " +
					"event_exclusion_id = ? ";
	
	public EventLockModel deleteModel(EventLockModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{model.getExclusionId()};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<EventLockModel, EventLockCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new EventLockQuery(jdbcTemplate);
	}
}
