package com.sony.pro.mwa.common.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.MwaLogger;

@XmlRootElement(name = "parameters")
public  class HashMapWrapper extends HashMap<String, Object> {
	private final static MwaLogger logger = MwaLogger.getLogger(Extract.class);
	
	public HashMapWrapper() {
		super();
	}
	public HashMapWrapper(Map<String, Object> map) {
		super(map);
	}

	@XmlElement(name = "data")
	public MapEntry[] getEntries() {
		List<MapEntry> list = new ArrayList<MapEntry>();
		for (Map.Entry entry : this.entrySet()) {
			if (entry.getValue() instanceof Collection) {
				//Collectionの場合(同一KeyでValを複数登録)
				for (Object val : (Collection)entry.getValue()) {
					MapEntry mapEntry = new MapEntry();
					mapEntry.key = String.class.cast(entry.getKey());
					//ValueをStringに変換
					mapEntry.value = objToString(val);
					list.add(mapEntry);
				}
			} else {
				//Collectioでない場合
				MapEntry mapEntry = new MapEntry();
				mapEntry.key = String.class.cast(entry.getKey());
				//ValueをStringに変換
				mapEntry.value = objToString(entry.getValue());
				list.add(mapEntry);
			}
		}
		return list.toArray(new MapEntry[list.size()]);
	}
	
	private String objToString (Object obj) {
		//ValueをStringに変換
		if (obj == null) {
			return null;
		} else if (!(obj instanceof String)) {
			return obj.toString();
		} else {
			return String.class.cast(obj);
		}
	}

	//再Set時はObjectではなくStringで復元されます。注意。
	public void setEntries(MapEntry[] arr) {
		for (MapEntry entry : arr) {
			if (this.containsKey(entry.key)) {
				//重複登録の場合
				Object listObj = this.get(entry.key);
				if (listObj instanceof Collection) {
					Collection list = Collection.class.cast(listObj);
					list.add(entry.value);
				} else {
					Collection list = new ArrayList<String>();
					list.add(this.get(entry.key));
					list.add(entry.value);
					this.put(entry.key, list);
				}
			} else {
				this.put(entry.key, entry.value);
			}
		}
	}

	public static class MapEntry {
		@XmlElement(name = "key")
		public String key;
		@XmlElement(name = "value")
		public String value;
	}
	
	public String toXml() {
		String result = null;
		javax.xml.bind.JAXBContext jc;
		try {
			jc = javax.xml.bind.JAXBContext.newInstance(HashMapWrapper.class);
			javax.xml.bind.Marshaller mu = jc.createMarshaller();
			java.io.StringWriter writer = new java.io.StringWriter();
			mu.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT,  Boolean.TRUE);
			mu.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			mu.marshal(this, writer);
			result = writer.toString();
		} catch (JAXBException e) {
			logger.error("toXml caused exception", e);
		}
		return result;
	}

	public String toJson() {
    	String jsonString = "Can't conversion";
    	try {
    		ObjectMapper mapper = new ObjectMapper();
        	jsonString = mapper.writeValueAsString(this);
    	} catch(Exception e) {
    	}
		return jsonString;
	}
	
	public String fromXml(String xml) {
		String result = null;
		javax.xml.bind.JAXBContext jc;
		try {
			jc = javax.xml.bind.JAXBContext.newInstance(HashMapWrapper.class);
			javax.xml.bind.Marshaller mu = jc.createMarshaller();
			java.io.StringWriter writer = new java.io.StringWriter();
			mu.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT,  Boolean.TRUE);
			mu.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			mu.marshal(this, writer);
			result = writer.toString();
		} catch (JAXBException e) {
			logger.error("fromXml caused exception", e);
		}
		return result;
	}
}
