package com.sony.pro.mwa.common.http;

import org.apache.http.Header;

public class HttpRequestConfigProviderImpl implements IHttpRequestConfigProvider {
	private Header[] header;

	/***
	 * HTTPリクエストヘッダ情報を取得します。
	 */
	@Override
	public Header[] getRequestHeaders() {
		return header;
	}

	public HttpRequestConfigProviderImpl(Header[] header) {
		this.header = header;
	}
}
