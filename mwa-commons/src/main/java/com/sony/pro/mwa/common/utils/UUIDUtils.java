package com.sony.pro.mwa.common.utils;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.sony.pro.mwa.common.log.MwaLogger;

public class UUIDUtils {
	private final static MwaLogger logger = MwaLogger.getLogger(UUIDUtils.class);
	public static boolean isValid(String uuid) {
		if (uuid == null)
			return false;
		try {
			UUID fromStringUUID = UUID.fromString(uuid);
			String toStringUUID = fromStringUUID.toString();
			return toStringUUID.equals(uuid);
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	public static String generateUUID(String seed) {
		java.security.MessageDigest digest;
		String result = "";
		try {
			digest = java.security.MessageDigest.getInstance("MD5");
			byte[] hash = digest.digest(seed.getBytes());
			for (int i = 0; i < hash.length; i++) {
				if (i == 8/2 || i==12/2 || i==16/2 || i==20/2)
					result += "-";
				result += String.format("%02x", hash[i]);
			}
		} catch (NoSuchAlgorithmException e) {
			logger.error("generateUUID caused exception: ", e);
		}
		return result;
	}

	public static String generateUUID(String seed1, String seed2) {
		String seed = generateSeed(seed1, seed2);
		String uuid = generateUUID(seed);
		return uuid;
	}
	
	public static String generateSeed(String seed1, String seed2) {
		return seed1 + "@@" + seed2;
	}

}
