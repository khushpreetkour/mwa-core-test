package com.sony.pro.mwa.common.utils;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;


public class Base64Utils {
	public static String encode(URI fileUri) {
		File file = new File(fileUri);
		BufferedImage image = null;

		try {
			image = ImageIO.read(file);
		} catch (IOException e) {
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedOutputStream bos = new BufferedOutputStream(baos);
		if (image != null) 
			image.flush();
		try {
			ImageIO.write(image, "png", bos);
			bos.flush();
			bos.close();
		} catch (IOException e) {
		}

		byte[] bImage = baos.toByteArray();

		byte[] encodedBytes = Base64.encodeBase64(bImage);
		return new String(encodedBytes);
	}

}
