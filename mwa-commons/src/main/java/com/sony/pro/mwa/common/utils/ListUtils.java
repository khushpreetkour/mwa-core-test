package com.sony.pro.mwa.common.utils;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */
public class ListUtils {
    public static <T> T getValidFirstItem(List<T> list) {
        if (CollectionUtils.isEmpty(list) == true) {
            return null;
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            throw new java.lang.UnsupportedOperationException();
        }
    }
}
