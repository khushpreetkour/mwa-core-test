package com.sony.pro.mwa.common.utils;

import java.util.concurrent.ConcurrentHashMap;

public class NamedLocks {
    private final ConcurrentHashMap<String, Object> locks = new ConcurrentHashMap<String, Object>();

    // 名前に対応するロックオブジェクトを取得する
    public Object get(String name) {
        locks.putIfAbsent(name, new Object());
        return locks.get(name);
    }
    public Object get(long name) {
    	String key = "LONG:" + name;
        locks.putIfAbsent(key, new Object());
        return locks.get(key);
    }
}
