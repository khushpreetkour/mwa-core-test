package com.sony.pro.mwa.common.utils;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;

public class Extract  {

	private final static MwaLogger logger = MwaLogger.getLogger(Extract.class);
	/**
	 * extract : Get out files in a zip.
	 * @param zipPath : Target zip file full path.
	 * @param filePath : Getting out a file or a directory.
	 *                   Relative path from the zip root.
	 *                   If you would like to get a directory in a zip,
	 *                   the last character need to "/" , like "aaa/bbb/".
	 * @param outputDir : Target zip file full path.
	 * @param mkOutDirFlg : If don't exists outputDir, make the directory or not.
	 * @return List<File> Created files list at the output directory.
	 * @throws IOException
	 */
	
	public static List<File> extract(String zipPath, String filePath, String outputDir) throws IOException {
		return extract(zipPath, filePath, outputDir, Boolean.FALSE);
	}
	
	public static List<File> extract(String zipPath, String filePath, String outputDir, Boolean mkOutDirFlg) throws IOException {
		List<File> outputFileList = new ArrayList<File>();
		
		if(StringUtils.isEmpty(outputDir)){
			throw new FileNotFoundException("cannot find outputDirectory: ");
		}

		File fOutDir = new File(outputDir);
		if(!fOutDir.exists()){
			if(mkOutDirFlg != null && mkOutDirFlg){
				fOutDir.mkdirs();
			}else{
				throw new FileNotFoundException("cannot find outputDirectory:" + outputDir);
			}
		}

		ZipFile zipFile= null;
		try{
			zipFile = new ZipFile(zipPath);
			File fTemp = new File(filePath);
			
			
			ZipEntry entry = zipFile.getEntry(filePath);
			if(entry == null) {
				throw new FileNotFoundException("cannot find file: " + filePath + " in archive: " + zipFile.getName());
			}
	
			if(entry.isDirectory()){

				// get file list In the zip
				Enumeration enumeration = zipFile.entries();
				while( enumeration.hasMoreElements() ){
					ZipEntry enumEntry = ( ZipEntry )enumeration.nextElement();
					if(!enumEntry.getName().startsWith(filePath)){
						continue;
					}
					String enumName = enumEntry.getName().substring(filePath.length());
					if(StringUtils.isEmpty(enumName)){
						continue;
					}

					File newFile = new File(fOutDir, enumName);
					if(enumEntry.isDirectory()){
						newFile.mkdir();
					} else {
						File extractFile = extractOneFile(zipFile, enumEntry, newFile);
						outputFileList.add(extractFile);
					}
				}
			} else {
				File targetFile = new File(fOutDir, fTemp.getName());
				File extractFile = extractOneFile(zipFile, entry, targetFile);
				outputFileList.add(extractFile);
			}
		} finally {
			close(zipFile);
		}
		return (outputFileList);
	}
	
	private static File extractOneFile(ZipFile zipFile, ZipEntry entry, File outputFile) throws IOException{
		InputStream zipStream  = zipFile.getInputStream(entry);
		OutputStream fileStream = null;
		try {
			fileStream = new FileOutputStream(outputFile);
			final byte[] buf = new byte[1024];
			int index = 0;
			while((index = zipStream.read(buf)) != -1) {
				fileStream.write(buf, 0, index);
			}
		} finally {
			close(zipStream);
			close(fileStream);
		}
		return (outputFile);
	}

	private static void close(final Closeable stream) {
		if(stream != null) {
			try {
				stream.close();
			} catch (IOException ex) {
				logger.error("close caused exception", ex);
			}
		}
	}
}
