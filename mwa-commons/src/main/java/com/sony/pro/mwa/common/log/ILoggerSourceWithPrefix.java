package com.sony.pro.mwa.common.log;

public interface ILoggerSourceWithPrefix {
	public String getLoggerPrefix();

	public static ILoggerSourceWithPrefix DEFAULT = new DefaultLoggerSource();
	public class DefaultLoggerSource implements ILoggerSourceWithPrefix{
		@Override
		public String getLoggerPrefix() {
			return "";
		}
	}
}
