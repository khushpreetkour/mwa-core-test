package com.sony.pro.mwa.common.utils;

import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.thoughtworks.xstream.XStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import java.io.StringReader;
import java.io.StringWriter;

public class Converter {
	private final static MwaLogger logger = MwaLogger.getLogger(Converter.class);

    public static String toXmlString(Object obj) {
        try {
            StringWriter sw = new StringWriter();
            Marshaller marshaller = JAXBContext.newInstance(obj.getClass()).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.marshal(obj, sw);
            return sw.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T toObject(String xmlString, Class<T> type) {
        try {
            StringReader sr = new StringReader(xmlString);
            return type.cast(JAXBContext.newInstance(type).createUnmarshaller().unmarshal(sr));
        } catch (Exception e) {
            return null;
        }
    }
    
    public static <T> T fromXml(String listStr) {
    	if (listStr == null)
    		return null;
    	XStream xstream = new XStream();
    	return (T) xstream.fromXML(listStr);
    }
    public static <T> String toXml(T list) {
    	if (list == null)
    		return null;
    	XStream xstream = new XStream();
    	return xstream.toXML(list);
    }

	public static String mapToXml(Map<String, ? extends Object> map) {
    	if (map == null)
    		return null;
    	XStream xstream = new XStream();
    	return xstream.toXML(map);
	}

	public static String mapToJson(Map<String, Object> map) {
    	if (map == null)
    		return null;
    	String jsonString = null;
    	try {
    		ObjectMapper mapper = new ObjectMapper();
        	jsonString = mapper.writeValueAsString(map);
    	} catch(Exception e) {
    	}
		return jsonString;
	}
	public static String objToJson(Object map) {
    	if (map == null)
    		return null;
    	String jsonString = null;
    	try {
    		ObjectMapper mapper = new ObjectMapper();
        	jsonString = mapper.writeValueAsString(map);
    	} catch(Exception e) {
    	}
		return jsonString;
	}
	
	public static Map<String, ? extends Object> xmlToMap(String xml) {
    	if (xml == null)
    		return null;
    	XStream xstream = new XStream();
    	return (Map<String, ? extends Object>) xstream.fromXML(xml);
	}
	
	public static Map<String, Object> jsonToMap(String json) {
		if (json == null || json.isEmpty())
			return null;
		
		Map<String, Object> result = null;
    	try {
    		ObjectMapper mapper = new ObjectMapper();
			result = mapper.readValue(json, Map.class);
    	} catch(Exception e) {
    		logger.warn("jsonToMap caused exception", e);
    	}
		return result;
	}
	public static <T> T  jsonTo(String json, Class clazz) {
		if (json == null || json.isEmpty())
			return null;
		
		T result = null;
    	try {
    		ObjectMapper mapper = new ObjectMapper();
			result = (T)mapper.readValue(json, clazz);
    	} catch(Exception e) {
    		logger.warn("jsonTo caused exception", e);
    	}
		return result;
	}
	
}
