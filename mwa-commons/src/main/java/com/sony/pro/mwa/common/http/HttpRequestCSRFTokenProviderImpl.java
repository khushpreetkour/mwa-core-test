package com.sony.pro.mwa.common.http;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

/***
 * NVXにてCSRF対応が必要なHTTPリクエストに対するヘッダ情報を取得する
 */
public class HttpRequestCSRFTokenProviderImpl implements IHttpRequestConfigProvider {
	private Header[] header;
	private Header csrfHeader = new BasicHeader("X-XSRF-TOKEN","WlUN+HLwt6iDYRJPnnyo5E9LkM+26ilq67zanyGvvfY=.1487828081876.Gsl+K+XBxhJ4Kd6ppaB3eUofGITOvhmulfb5gFlnGQI=");

	/***
	 * CSRFトークン付のHTTPリクエストヘッダを取得します。
	 */
	@Override
	public Header[] getRequestHeaders() {
		List<Header> buffer = new ArrayList<Header>();
		for (Header h : this.header) {
			buffer.add(h);
		}
		buffer.add(this.csrfHeader);
		Header[] response = buffer.toArray(new Header[buffer.size()]);
		return response;
	}

	public HttpRequestCSRFTokenProviderImpl(Header[] header) {
		this.header = header;
	}
}
