package com.sony.pro.mwa.common.http;

import org.apache.http.Header;

/**
 * HTTP リクエストに関わるコンフィグ情報を提供するインタフェース。
 */
public interface IHttpRequestConfigProvider {
	/**
	 * リクエストに必要なHTTPヘッダ情報を取得する。
	 * @return リクエストにセットするHTTPヘッダ情報
	 */
	Header[] getRequestHeaders();
}
