package com.sony.pro.mwa.common.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MwaLogger {

	final static String PREFIX = "[MWA]";
	ILoggerSourceWithPrefix loggerSource;
	Logger logger;
	
	protected MwaLogger(Logger logger) {
		this(logger, ILoggerSourceWithPrefix.DEFAULT);
	}
	protected MwaLogger(Logger logger, ILoggerSourceWithPrefix loggerSource) {
		this.logger = logger;
		this.loggerSource = loggerSource;
	}
	
	public static MwaLogger getLogger(Class clazz) {
		return new MwaLogger(LoggerFactory.getLogger(clazz));
	}
	public static MwaLogger getLogger(ILoggerSourceWithPrefix loggerSource) {
		return new MwaLogger(LoggerFactory.getLogger(loggerSource.getClass()), loggerSource);
	}
	
	protected String threadPrefix() {
		return "[" + Thread.currentThread().getId() + "]";
	}

	public void trace(Object message) {
		logger.trace(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message);
	}
	public void trace(Object message, Throwable t) {
		logger.trace(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message, t);
	}
	public void debug(Object message) {
		logger.debug(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message);
	}
	public void debug(Object message, Throwable t) {
		logger.debug(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message, t);
	}
	public void info(Object message) {
		logger.info(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message);
	}
	public void info(Object message, Throwable t) {
		logger.info(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message, t);
	}
	public void warn(Object message) {
		logger.warn(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message);
	}
	public void warn(Object message, Throwable t) {
		logger.warn(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message, t);
	}
	public void error(Object message) {
		logger.error(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message);
	}
	public void error(Object message, Throwable t) {
		logger.error(threadPrefix() + PREFIX + loggerSource.getLoggerPrefix() + " " + message, t);
	}
}
