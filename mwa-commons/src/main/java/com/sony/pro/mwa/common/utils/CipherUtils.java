package com.sony.pro.mwa.common.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class CipherUtils {
	public static String encryptoWithBase64(String plaintext, String password) {
		return new String(Base64.encodeBase64(encrypto(plaintext, password)));
	}
	public static String decryptoWithBase64(String ciphertext, String password) {
		return decrypto(Base64.decodeBase64(ciphertext.getBytes()), password);
	}
	public static byte[] encrypto(String plaintext, String password) {
		byte[] ciphertext = null;
		if (plaintext != null) { 
			try {
				Cipher cipher = createCipher(password, Cipher.ENCRYPT_MODE);
				byte[] ciphertextBytes = cipher.doFinal(plaintext.getBytes());
				ciphertext = ciphertextBytes;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return ciphertext;
	}
	public static String decrypto(byte[] ciphertext, String password) {
		String plaintext = null;
		if (ciphertext != null) { 
			try {
				Cipher cipher = createCipher(password, Cipher.DECRYPT_MODE);
				plaintext = new String(cipher.doFinal(ciphertext));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return plaintext;
	}
	private static Cipher createCipher(String password, int mode) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException {
		SecretKeySpec key = new SecretKeySpec(password.getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(mode, key);
		return cipher;
	}
}
