package com.sony.pro.mwa.common.utils;

public class SystemUtils {
	public static class MWA {
		protected static final String ENV_MWA_HOME = "MWA_HOME";
		public static final String SEP = java.io.File.separator;
		protected static final String PROPERTY_HOME = System.getProperty(ENV_MWA_HOME);
		private static final String HOME_VAL =
				(PROPERTY_HOME != null) ? PROPERTY_HOME
										: System.getenv(ENV_MWA_HOME);	//環境変数の最終文字がseparatorになっていること（"\\" or "/"）
		public static final String HOME =
				(HOME_VAL != null)	? HOME_VAL + (HOME_VAL.endsWith(SEP) ? "" : SEP)
									: null;
		static {
			if (HOME == null) {
				String message = "Environment Valiable " + ENV_MWA_HOME +  " is not specified";
				System.out.println(message);
				throw new RuntimeException(message);
			}
		}
		public static final String PLUGINS		= HOME + "plugins" + SEP;
		public static final String BPMNS		= HOME + "bpmns" + SEP;
		public static final String CUSTOM_BPMNS		= HOME + "custom_bpmns" + SEP;
		public static final String RULES		= HOME + "rules" + SEP;
		public static final String RESOURCE_RULES = HOME + "resource_rules" + SEP;
		public static final String PLUGIN_FILES_NAME	= "plugin_files";
		public static final String PLUGIN_FILES	= HOME + PLUGIN_FILES_NAME + SEP;
	}

	public static String getPluginResourcePath(String pluginId) {
		return MWA.PLUGIN_FILES + pluginId + MWA.SEP;
	}

	public static class CATALYSTCL {
		public static final String ENV_CATALYSTCL_HOME = "CATALYSTCL_HOME";
		public static final String SEP = java.io.File.separator;
		protected static final String PROPERTY_HOME = System.getProperty(ENV_CATALYSTCL_HOME);
		private static final String HOME_VAL =
				(PROPERTY_HOME != null) ? PROPERTY_HOME
										: System.getenv(ENV_CATALYSTCL_HOME);

		// 環境変数の最終文字がseparatorになっていない場合は、separatorを付ける
		public static String HOME =
				(HOME_VAL != null)	? HOME_VAL + (HOME_VAL.endsWith(SEP) ? "" : SEP)
									: "";
	}

	public static String getCatalystCLPath() {
		return CATALYSTCL.HOME;
	}
}
