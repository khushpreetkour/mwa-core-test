
Lists of 7 third-party dependencies.
     (The Apache Software License, Version 2.0) Commons Codec (commons-codec:commons-codec:1.8 - http://commons.apache.org/proper/commons-codec/)
     (The Apache Software License, Version 2.0) Apache Log4j (log4j:log4j:1.2.17 - http://logging.apache.org/log4j/1.2/)
     (The Apache Software License, Version 2.0) Jackson (org.codehaus.jackson:jackson-core-asl:1.9.13 - http://jackson.codehaus.org)
     (GNU Lesser General Public License (LGPL), Version 2.1) (The Apache Software License, Version 2.0) JAX-RS provider for JSON content type (org.codehaus.jackson:jackson-jaxrs:1.9.13 - http://jackson.codehaus.org)
     (The Apache Software License, Version 2.0) Data Mapper for Jackson (org.codehaus.jackson:jackson-mapper-asl:1.9.13 - http://jackson.codehaus.org)
     (MIT License) SLF4J API Module (org.slf4j:slf4j-api:1.7.2 - http://www.slf4j.org)
     (MIT License) SLF4J LOG4J-12 Binding (org.slf4j:slf4j-log4j12:1.7.2 - http://www.slf4j.org)
