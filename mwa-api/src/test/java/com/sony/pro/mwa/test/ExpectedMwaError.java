package com.sony.pro.mwa.test;

import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sony.pro.mwa.model.StateModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.UriUtils;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
public class ExpectedMwaError implements TestRule {
	ExpectedException thrown = ExpectedException.none();
	
    public static ExpectedMwaError none() {
        return new ExpectedMwaError();
    }
    
	@Override
	public Statement apply(Statement base, Description description) {
		// TODO Auto-generated method stub
		return thrown.apply(base, description);
	}
	
    public void expect(MWARC rc) {
    	thrown.expectMessage(rc.name());
    }
    
    @Test
    public void test() {
    	/*
    	ActivityInstanceModel model = new ActivityInstanceModel();
    	model.setStatus(new StateModel("name", true, true, true, true));
    	toJson(model);
    	*/
    	System.out.println(UriUtils.native2mwa("DEFAULT", "C:\\temp\\test+\\", "C:\\temp\\"));
    }
	
	public void toJson(ActivityInstanceModel obj) {
		/*
		if(serializeNulls){
			Gson gson = new GsonBuilder().serializeNulls().disableHtmlEscaping().create();
			return gson.toJson(obj);
		}else{
			Gson gson = new GsonBuilder().disableHtmlEscaping().create();
			return gson.toJson(obj);
		}
		*/
		JsonObject doc = new JsonObject(Json.encode(obj));
		System.out.println("" + doc.encodePrettily());
	}
}
