/**
 * 
 */
package com.sony.pro.mwa.model.kbase;

import com.sony.pro.mwa.enumeration.ExtensionType;

/**
 * @author Xi.Ou
 *
 */
public class MwaRuleModel extends ExtensionModel {

	@Override
	public String getType() {
		return ExtensionType.RULE.name();
	}

}
