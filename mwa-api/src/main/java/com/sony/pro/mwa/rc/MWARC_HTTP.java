package com.sony.pro.mwa.rc;

public enum MWARC_HTTP implements IMWARC {

    HTTP_RESPONSE_STATUS_CODE_400("0400"),
    HTTP_RESPONSE_STATUS_CODE_401("0401"),
    HTTP_RESPONSE_STATUS_CODE_403("0403"),
    HTTP_RESPONSE_STATUS_CODE_404("0404"),
    HTTP_RESPONSE_STATUS_CODE_405("0405"),
    HTTP_RESPONSE_STATUS_CODE_406("0406"),
    HTTP_RESPONSE_STATUS_CODE_407("0407"),
    HTTP_RESPONSE_STATUS_CODE_408("0408"),
    HTTP_RESPONSE_STATUS_CODE_409("0409"),
    HTTP_RESPONSE_STATUS_CODE_410("0410"),
    HTTP_RESPONSE_STATUS_CODE_411("0411"),
    HTTP_RESPONSE_STATUS_CODE_412("0412"),
    HTTP_RESPONSE_STATUS_CODE_413("0413"),
    HTTP_RESPONSE_STATUS_CODE_414("0414"),
    HTTP_RESPONSE_STATUS_CODE_415("0415"),
    HTTP_RESPONSE_STATUS_CODE_416("0416"),
    HTTP_RESPONSE_STATUS_CODE_417("0417"),
    HTTP_RESPONSE_STATUS_CODE_500("0500"),
    HTTP_RESPONSE_STATUS_CODE_501("0501"),
    HTTP_RESPONSE_STATUS_CODE_502("0502"),
    HTTP_RESPONSE_STATUS_CODE_503("0503"),
    HTTP_RESPONSE_STATUS_CODE_504("0504"),
    HTTP_RESPONSE_STATUS_CODE_505("0505"),
	;
	
	private static final String GROUPID = GroupId.HTTP.id(); //http用の予約済みgroupId
	private static final String REGEX = IMWARC.PREFIX + IMWARC.DELIMITER + GROUPID + "[0-9a-fA-F]{4}";
	private static final java.util.regex.Pattern PATTERN = java.util.regex.Pattern.compile(REGEX);
	private String localId;
	
	private MWARC_HTTP(String localId) {
		this.localId = localId;
	}

	@Override
	public String code() {
		return MWARCUtil.genMWARC(GROUPID, this.localId);
	}
	
	@Override
	public boolean isSuccess() {
		return false;
	}

	static public MWARC_HTTP valueOfByCode(Integer code) {
		String codeStr = "0" + code;
		return MWARC_HTTP.valueOfByCode(codeStr);
	}
	static public MWARC_HTTP valueOfByName(String name) {
		return MWARC_HTTP.valueOf(name);
	}
	static protected MWARC_HTTP valueOfByCode(String code) {
		MWARC_HTTP result = null;
		for (MWARC_HTTP value : MWARC_HTTP.values()) {
			if (value.code().equals(code)) {
				result = value;
				break;
			}
		}
		return result;
	}
	
	static public boolean checkGroup(String code) {
		return MWARCUtil.checkFormat(PATTERN, code);
	}
	
	static public String getGroupId() {
		return GROUPID;
	}
}
