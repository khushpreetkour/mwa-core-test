package com.sony.pro.mwa.model.storage;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.GenericPropertyBase;

public class PerspectivePropertyModel extends GenericPropertyBase {
	String locationId;
	String perspective;

	@XmlTransient @JsonIgnore
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	@XmlTransient @JsonIgnore
	public String getPerspective() {
		return perspective;
	}
	public void setPerspective(String perspective) {
		this.perspective = perspective;
	}
}
