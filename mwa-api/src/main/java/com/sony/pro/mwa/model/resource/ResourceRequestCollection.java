package com.sony.pro.mwa.model.resource;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;

public class ResourceRequestCollection extends Collection<ResourceRequestModel> {

	public ResourceRequestCollection() {
		super();
	}

	public ResourceRequestCollection(List<ResourceRequestModel> models) {
		super(models);
	}

	public ResourceRequestCollection(List<ResourceRequestModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ResourceRequestCollection(Long totalCount, Long count,
			List<ResourceRequestModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ResourceRequestModel> getModels() {
		return super.getModels();
	}
}
