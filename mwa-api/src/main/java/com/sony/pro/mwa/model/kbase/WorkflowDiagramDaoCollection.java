package com.sony.pro.mwa.model.kbase;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class WorkflowDiagramDaoCollection extends Collection<WorkflowDiagramDaoModel> {
	public WorkflowDiagramDaoCollection() {
		super();
	}
	
	public WorkflowDiagramDaoCollection(List<WorkflowDiagramDaoModel> models) {
		super(models);
	}
	
	public WorkflowDiagramDaoCollection(List<WorkflowDiagramDaoModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}
	
	public WorkflowDiagramDaoCollection(Long totalCount, Long count, List<WorkflowDiagramDaoModel> models) {
		super(totalCount, count, models);
	}
	
	@Override
	public List<WorkflowDiagramDaoModel> getModels() {
		return super.getModels();
	}
}
