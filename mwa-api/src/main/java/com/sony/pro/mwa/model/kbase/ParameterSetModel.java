package com.sony.pro.mwa.model.kbase;

import java.util.Map;

import com.wordnik.swagger.annotations.ApiModelProperty;

@Deprecated
public class ParameterSetModel {
	protected String id;
	protected String name;
	Map<String, Object> parameters;

	@ApiModelProperty(value = "ID of the parameter set.")
	public String getId() {
		return id;
	}

	public ParameterSetModel setId(String id) {
		this.id = id;
		return this;
	}

	@ApiModelProperty(value = "Name of the parameter set.")
	public String getName() {
		return name;
	}

	public ParameterSetModel setName(String name) {
		this.name = name;
		return this;
	}

	@ApiModelProperty(value = "Parameters of the parameter set.")
	public Map<String, Object> getParameters() {
		return parameters;
	}

	public ParameterSetModel setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
		return this;
	}
}
