package com.sony.pro.mwa.rc;

public class MWARC_GENERIC implements IMWARC {

	String name;
	String code;

	public MWARC_GENERIC(String name, String code) {
		this.name = name;
		this.code = code;
	}
	
	@Override
	public String name() {
		return name;
	}
	
	@Override
	public String code() {
		return code;
	}
	
	@Override
	public boolean isSuccess() {
		return MWARC.SUCCESS.code().equals(this.code());
	}
}
