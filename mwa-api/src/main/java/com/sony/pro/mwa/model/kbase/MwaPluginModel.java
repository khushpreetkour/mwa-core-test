package com.sony.pro.mwa.model.kbase;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.enumeration.ExtensionType;

public class MwaPluginModel extends ExtensionModel {

	public MwaPluginModel() {
		super();
	}
	public MwaPluginModel(ExtensionModel model) {
		super(model);
	}
	
	@XmlTransient @JsonIgnore
	public Long getBundleId() {
		String bundleIdStr = super.getProperties().get(EXT_PROPERTY.BUNDLE_ID.name());
		Long bundleId = null;
		try {
			bundleId = Long.valueOf(bundleIdStr);
		} catch (NumberFormatException e) {
			//do nothing
		}
		return bundleId;
	}
	public MwaPluginModel setBundleId(Long bundleId) {
		if (bundleId != null) 
			this.getProperties().put(EXT_PROPERTY.BUNDLE_ID.name(), bundleId.toString());
		return this;
	}
	
	protected enum EXT_PROPERTY {
		BUNDLE_ID,
	}

	@Override
	public String getType() {
		return ExtensionType.BUNDLE.name();
	}
}
