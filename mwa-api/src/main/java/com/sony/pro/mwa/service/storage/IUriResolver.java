package com.sony.pro.mwa.service.storage;

import java.util.List;

import com.sony.pro.mwa.model.storage.NativeLocationModel;

public interface IUriResolver {
	@Deprecated
	public NativeLocationModel asNativePath(String uri, String perspective);
	@Deprecated
	public NativeLocationModel asNativePath(String uri, String perspective, String providerId);

	//実処理用（Activityが主ユーザ）
	public NativeLocationModel asNativePath(String uri, String... perspective);
	public NativeLocationModel asNativePathViaProvider(String uri, String providerId, String... perspective); //URIとPathが1:NなのでPerspectiveも指定させる
	public String asMwaUri(String nativePath, String location);
	public String asMwaUri(String nativePath, String location, String providerId);
}
