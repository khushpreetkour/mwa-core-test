package com.sony.pro.mwa.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "property")
public class GenericPropertyCollection extends Collection<GenericPropertyModel> {

	public GenericPropertyCollection() {
		super();
	}

	public GenericPropertyCollection(List<GenericPropertyModel> models) {
		super(models);
	}

	public GenericPropertyCollection(List<GenericPropertyModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public GenericPropertyCollection(Long totalCount, Long count,
			List<GenericPropertyModel> models) {
		super(totalCount, count, models);
	}

	@Override
	@XmlElement(name = "property")
	public List<GenericPropertyModel> getModels() {
		return super.getModels();
	}
}
