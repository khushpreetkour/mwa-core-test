package com.sony.pro.mwa.model.scheduler;

public class SimpleTriggerInfoModel extends TriggerInfoModel{
	
	private String repeatInterval;
	
	private String repeatCount;
	
	private String triggerState;
	
	private Long nextFireTime;

	public String getRepeatInterval()
	{
		return repeatInterval;
	}

	public void setRepeatInterval(String repeatInterval)
	{
		this.repeatInterval = repeatInterval;
	}

	public String getRepeatCount()
	{
		return repeatCount;
	}

	public void setRepeatCount(String repeatCount)
	{
		this.repeatCount = repeatCount;
	}

	public String getTriggerState()
	{
		return triggerState;
	}

	public void setTriggerState(String triggerState)
	{
		this.triggerState = triggerState;
	}

	public Long getNextFireTime()
	{
		return nextFireTime;
	}

	public void setNextFireTime(Long nextFireTime)
	{
		this.nextFireTime = nextFireTime;
	}

	
	
}
