package com.sony.pro.mwa.model.kbase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.wordnik.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityProfileGroupModel {
	private String id;
	private String templateId;
	private String name;
	private String icon;
	private Long createTime;
	private Long updateTime;

	@ApiModelProperty(value = "ID of the parameter set.")
	public String getId() {
		return id;
	}

	public ActivityProfileGroupModel setId(String id) {
		this.id = id;
		return this;
	}

	@ApiModelProperty(value = "TemplateId of the parameter set.")
	public String getTemplateId() {
		return templateId;
	}

	public ActivityProfileGroupModel setTemplateId(String templateId) {
		this.templateId = templateId;
		return this;
	}

	@ApiModelProperty(value = "Name of the parameter set.")
	public String getName() {
		return name;
	}

	public ActivityProfileGroupModel setName(String name) {
		this.name = name;
		return this;
	}

	@ApiModelProperty(value = "Icon of the parameter set.")
	public String getIcon() {
		return icon;
	}

	public ActivityProfileGroupModel setIcon(String icon) {
		this.icon = icon;
		return this;
	}

	@ApiModelProperty(value = "Create Time of the parameter set.")
	public Long getCreateTime() {
		return createTime;
	}

	public ActivityProfileGroupModel setCreateTime(Long createTime) {
		this.createTime = createTime;
		return this;
	}

	@ApiModelProperty(value = "Update Time of the parameter set.")
	public Long getUpdateTime() {
		return updateTime;
	}

	public ActivityProfileGroupModel setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
		return this;
	}
}
