package com.sony.pro.mwa.model;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class GenericPropertyModel extends GenericPropertyBase {
	String id;

	@XmlTransient @JsonIgnore
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
