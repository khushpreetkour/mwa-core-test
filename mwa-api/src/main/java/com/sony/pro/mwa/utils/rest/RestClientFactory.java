package com.sony.pro.mwa.utils.rest;

import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.utils.rest.RestClient;
import com.sony.pro.mwa.utils.rest.RestClientFactory;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;

public class RestClientFactory {

	private final static MwaLogger logger = MwaLogger.getLogger(RestClientFactory.class);

	private static Map<String, RestClient> restClientMap = new HashMap<String, RestClient>();

	private RestClientFactory() {

	}

	/**
	 * RESTClientオブジェクトの取得
	 * 
	 * @param key
	 * @return　RESTClientオブジェクト
	 */
	public static RestClient getInstance(String key)
	{

		if (!restClientMap.containsKey(key)) {
			restClientMap.put(key, new RestClient(key));
		}

		return restClientMap.get(key);
	}

	/**
	 * RESTClientオブジェクト　Mapエントリーの削除
	 * 
	 * @param key
	 */
	public static void removeInstance(String key)
	{

		if (!restClientMap.containsKey(key)) {
			logger.error("Key does not exist.");
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}

		restClientMap.remove(key);
	}
}
