package com.sony.pro.mwa.model.kbase.tempmodels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.ParameterDefinition;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeCost;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeLong;
import com.sony.pro.mwa.parameter.type.TypeMap;
import com.sony.pro.mwa.parameter.type.TypeResource;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.common.log.MwaLogger;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowDiagramModel {

	static MwaLogger logger = MwaLogger.getLogger(WorkflowDiagramModel.class);
	
	private String id;
	private String name;
	private String version;
	private Long createTime;
	private Long updateTime;
	private List<StepModel> steps = new ArrayList<>();
	private List<ConnectionPortModel> connectionPorts = new ArrayList<>();
	private List<ConnectionModel> connections = new ArrayList<>();

	public List<StepModel> getSteps() {
		return steps;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setSteps(List<StepModel> steps) {
		this.steps = steps;
	}

	public void setConnectionPorts(List<ConnectionPortModel> connectionPorts) {
		this.connectionPorts = connectionPorts;
	}

	public void setConnections(List<ConnectionModel> connections) {
		this.connections = connections;
	}

	public List<ConnectionPortModel> getConnectionPorts() {
		return connectionPorts;
	}

	public List<ConnectionModel> getConnections() {
		return connections;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	///終わったら消すこと！！！！
	public static void logMap(Map<String, Object> map) {
		
		Set<String> keys = map.keySet();
		
		StringBuffer buffer = new StringBuffer();
		
		for (String key : keys) {
			Object value = map.get(key);
			buffer.append(String.format("%s : %s\r\n", key, value == null ? "null" : value.toString()));
		}
		logger.info(buffer.toString());
		
	}
	
	public List<WFParameterDefinition> importMap(ArrayList inputList, List<WFParameterDefinition> lists) {
		
		lists.clear();

		for (Object obj : inputList) {
		
			Map<String, Object> inputSrc = (Map<String, Object>)obj;				
			Set<Entry<String, Object>> entrySet = inputSrc.entrySet();
	
			Object requiredValue = inputSrc.get("required");
			Object typeNameValue = inputSrc.get("typeName");
			Object keyValue = inputSrc.get("key");
			boolean required = (Boolean)requiredValue;
			String typeName = (String)typeNameValue;
			String key = (String)keyValue;
			
			String trueType = this.validateParameterTypeName(typeName);
			
			WFParameterDefinition param = new WFParameterDefinition();
			param.setKey(key);
			param.setRequired(required);
			param.setTypeName(trueType);
			lists.add(param);
		}
		return lists;
	}

	/**
	 * ｓｒｃTypeNameに一致するIParameterTypeのインスタンスを検索する。
	 * @param srcTypeName
	 * @return
	 */
	private String validateParameterTypeName(String srcTypeName) {
		IParameterType[] types = new IParameterType[] {
				TypeBoolean.instance(),
				TypeCost.instance(),
				TypeListString.instance(),
				TypeLong.instance(),
				TypeMap.instance(),
				TypeResource.instance(),
				TypeString.instance()
		};
		
		for (IParameterType type : types) {
			if (type.name().equals(srcTypeName)) {
				return type.name();
			}
		}
		throw new MwaInstanceError(
				MWARC.SYSTEM_ERROR,
				null,
				String.format("in createTypeName : cannot convertType name. srcTypeName %s is not found", srcTypeName));
	}
}
