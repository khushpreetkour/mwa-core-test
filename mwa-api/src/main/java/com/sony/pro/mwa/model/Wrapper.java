package com.sony.pro.mwa.model;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/11/06
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */
public abstract class Wrapper<T> {
    public abstract T getBody();
    public abstract void setBody(T body);
}
