package com.sony.pro.mwa.model.resource;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;
import com.sony.pro.mwa.model.resource.ResourceRequestEntryModel;

public class ResourceRequestEntryCollection extends Collection<ResourceRequestEntryModel> {

	public ResourceRequestEntryCollection() {
		super();
	}

	public ResourceRequestEntryCollection(List<ResourceRequestEntryModel> models) {
		super(models);
	}

	public ResourceRequestEntryCollection(List<ResourceRequestEntryModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ResourceRequestEntryCollection(Long totalCount, Long count,
			List<ResourceRequestEntryModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ResourceRequestEntryModel> getModels() {
		return super.getModels();
	}
}
