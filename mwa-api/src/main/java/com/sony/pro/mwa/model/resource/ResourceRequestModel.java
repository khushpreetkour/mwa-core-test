package com.sony.pro.mwa.model.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement
public class ResourceRequestModel {
	String id;
	String occupantId;
	String occupantName;
	String receiverId;
	String queueId;
	List<String> priority = new ArrayList<>(); //多次元で表現(とりあえず最大値は５次元)
	Map<String, Object> metadata = new TreeMap<>();
	List<ResourceRequestEntryModel> entries;
	Long createTime;
	Long updateTime;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getPriority() {
		return priority;
	}
	public void setPriority(List<String> priority) {
		this.priority = priority;
	}
	public String getOccupantId() {
		return occupantId;
	}
	public void setOccupantId(String occupantId) {
		this.occupantId = occupantId;
	}
	public String getOccupantName() {
		return occupantName;
	}
	public void setOccupantName(String occupantName) {
		this.occupantName = occupantName;
	}
	public String getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	@Deprecated
	@XmlTransient @JsonIgnore
	public CostModel getCost() {
		return (this.entries != null && !this.entries.isEmpty()) ? this.entries.get(0).getCost() : null;
	}
	@Deprecated
	public void setCost(CostModel cost) {
		if (this.entries == null || this.entries.isEmpty()) {
			this.entries = new ArrayList<ResourceRequestEntryModel>();
			this.entries.add(new ResourceRequestEntryModel());
		}
		this.entries.get(0).setCost(cost);
	}
	@Deprecated
	@XmlTransient @JsonIgnore
	public String getRequestResourceId() {
		return (this.entries != null && !this.entries.isEmpty()) ? this.entries.get(0).getResourceId() : null;
	}
	@Deprecated
	public void setRequestResourceId(String requestResourceId) {
		if (this.entries == null || this.entries.isEmpty()) {
			this.entries = new ArrayList<ResourceRequestEntryModel>();
			this.entries.add(new ResourceRequestEntryModel());
		}
		this.entries.get(0).setResourceId(requestResourceId);
	}
	@Deprecated
	@XmlTransient @JsonIgnore
	public String getAssignedResourceId() {
		return (this.entries != null && !this.entries.isEmpty()) ? this.entries.get(0).getAssignedResourceId() : null;
	}
	@Deprecated
	public void setAssignedResourceId(String assignedResourceId) {
		if (this.entries == null || this.entries.isEmpty()) {
			this.entries = new ArrayList<ResourceRequestEntryModel>();
			this.entries.add(new ResourceRequestEntryModel());
		}
		this.entries.get(0).setAssignedResourceId(assignedResourceId);
	}

	@XmlElementWrapper(name="resourceRequestEntryList")
    @XmlElement(name="resourceRequestEntry")
	public List<ResourceRequestEntryModel> getResourceRequestEntryList() {
		return this.entries;
	}
	public void setResourceRequestEntryList(List<ResourceRequestEntryModel> entries) {
		this.entries = entries;
	}
	public void addResourceRequestEntry(ResourceRequestEntryModel entry) {
		if (this.entries == null)
			this.entries = new ArrayList<>();
		this.entries.add(entry);
	}

	public String getQueueId() {
		return queueId;
	}
	public void setQueueId(String queueId) {
		this.queueId = queueId;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
	public Map<String, Object> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, Object> metadata) {
		this.metadata = metadata;
	}

	public boolean getAssignedFlag() {
		boolean result = true;
		if  (this.getResourceRequestEntryList() == null)
			return false;
		for (ResourceRequestEntryModel entry : this.getResourceRequestEntryList()) {
			if (entry.getAssignedResourceId() == null) {
				result = false;
				break;
			}
		}
		return result;
	}
	public void releaseAllResources() {
		if  (this.getResourceRequestEntryList() != null) {
			for (ResourceRequestEntryModel entry : this.getResourceRequestEntryList()) {
				entry.setAssignedResourceId(null);
			}
		}
		return;
	}
	
	public String toString() {
		return "{"
				+ "\"id\":\"" + id + "\", " 
				+ "\"occupantId\":\"" + occupantId + "\", "
				+ "\"receiverId\":\"" + receiverId + "\", "
				+ "\"queueId\":\"" + queueId + "\", "
				+ "\"priority\":" + priority + ", "
				+ "\"metadata\":" + metadata + ", "
				+ "\"entries\":" + entries
				+ "}";
	}
}
