package com.sony.pro.mwa.model.provider;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ActivityProviderTypeCollection extends Collection<ActivityProviderTypeModel> {

	public ActivityProviderTypeCollection() {
		super();
	}

	public ActivityProviderTypeCollection(List<ActivityProviderTypeModel> models) {
		super(models);
	}

	public ActivityProviderTypeCollection(List<ActivityProviderTypeModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ActivityProviderTypeCollection(Long totalCount, Long count,
			List<ActivityProviderTypeModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ActivityProviderTypeModel> getModels() {
		return super.getModels();
	}
}
