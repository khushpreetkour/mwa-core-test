package com.sony.pro.mwa.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.ParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;


/**
 * This provides utility methods to convert values in input/output parameter maps to/from strings. 
 * 
 * Example usage:
 * Map<String, Object> inputMap = ParameterUtils.FROM_STRING.convert(getTemplate().getInputs(), inputs);
 * setOutputDetails(ParameterUtils.TO_STRING.convert(getTemplate().getOutputs(), outputs));
 */
public abstract class ParameterUtils {
    
      
    public Map<String, Object> convert(List<? extends IParameterDefinition> typeDefs, Map<String, Object> inputMap) {
        Map<String, Object> outputMap = new HashMap<>();
        for (IParameterDefinition typeDef : typeDefs) {
            String key = typeDef.getKey();
            Object value = inputMap.get(typeDef.getKey());
            if (value != null) {                        
                outputMap.put(key, convertValue(typeDef.getType(), value));
            } else if (typeDef.getRequired()) {
                throw new MwaInstanceError(MWARC.INVALID_INPUT, "", "Missing required parameter: " + (typeDef.getKey()));
            }
        }     
        return outputMap;
    }

    protected abstract Object convertValue(IParameterType valueType, Object value);
   
    
    public static final ParameterUtils TO_STRING = new ParameterUtils() {
        @Override
        protected Object convertValue(IParameterType valueType, Object value) {            
            return valueType.toString(value);
        }        
    };
    
    public static final ParameterUtils FROM_STRING = new ParameterUtils() {
        @Override
        protected Object convertValue(IParameterType valueType, Object value) {            
            return valueType.fromString((String)value);
        }      
    };    
    
    
    public static IParameterDefinition createParameter(String key, IParameterType type, boolean required) {
        ParameterDefinition def = new ParameterDefinition();
        def.setKey(key);
        def.setType(type);
        def.setRequired(required);
        return def;
    }
    public static IParameterDefinition createParameter(IParameterDefinition param) {
        ParameterDefinition def = new ParameterDefinition();
        def.setKey(param.getKey());
        def.setType(param.getType());
        def.setRequired(param.getRequired());
        def.setVisibleFlag(param.getVisibleFlag());
        return def;
    }
}
