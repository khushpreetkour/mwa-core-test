package com.sony.pro.mwa.model.kbase.tempmodels;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowModelCollection extends Collection<WorkflowModel> {

	public WorkflowModelCollection() {
		super();
	}

	public WorkflowModelCollection(List<WorkflowModel> models) {
		super(models);
	}

	public WorkflowModelCollection(List<WorkflowModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public WorkflowModelCollection(Long totalCount, Long count, List<WorkflowModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<WorkflowModel> getModels() {
		return super.getModels();
	}

}
