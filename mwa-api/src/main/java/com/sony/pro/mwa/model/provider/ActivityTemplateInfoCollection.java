package com.sony.pro.mwa.model.provider;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ActivityTemplateInfoCollection extends Collection<ActivityTemplateInfoModel> {

	public ActivityTemplateInfoCollection() {
		super();
	}

	public ActivityTemplateInfoCollection(List<ActivityTemplateInfoModel> models) {
		super(models);
	}

	public ActivityTemplateInfoCollection(List<ActivityTemplateInfoModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public ActivityTemplateInfoCollection(Long totalCount, Long count, List<ActivityTemplateInfoModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ActivityTemplateInfoModel> getModels() {
		return super.getModels();
	}
}
