package com.sony.pro.mwa.service;

import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.kbase.IKnowledgeBase;
import com.sony.pro.mwa.service.provider.IActivityProviderManager;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.service.scheduler.ISchedulerManager;
import com.sony.pro.mwa.service.storage.IStorageManager;
import com.sony.pro.mwa.service.storage.IUriResolver;

public interface ICoreModules {
	public IActivityManager getActivityManager();
	public IKnowledgeBase getKnowledgeBase();
	public IActivityProviderManager getActivityProviderManager();
	public IStorageManager getStorageManager();
	public IUriResolver getUriResolver();
	public IResourceManager getResourceManager();
	public IEventManager getEventManager();
	public ISchedulerManager getSchedulerManager();
}
