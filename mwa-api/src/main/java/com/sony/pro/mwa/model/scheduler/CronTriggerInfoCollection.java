package com.sony.pro.mwa.model.scheduler;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class CronTriggerInfoCollection extends Collection<CronTriggerInfoModel>{

    public CronTriggerInfoCollection() {
        super();
    }

    public CronTriggerInfoCollection(List<CronTriggerInfoModel> models) {
        super(models);
    }

    public CronTriggerInfoCollection(List<CronTriggerInfoModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public CronTriggerInfoCollection(Long totalCount, Long count, List<CronTriggerInfoModel> models) {
        super(totalCount, count, models);
    }
	
    @Override
    public List<CronTriggerInfoModel> getModels() {
        return super.getModels();
    }
}
