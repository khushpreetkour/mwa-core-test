package com.sony.pro.mwa.model.provider;

import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.common.utils.UUIDUtils;
import com.sony.pro.mwa.service.provider.IEndpointType;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value="Activity provider type model", description="Type of the activity provider and MWA utilizes it to judge whether the provider processes activiy or not.")
public class ActivityProviderTypeModel {
	String id;
	String name;
	String modelNumber;
	Long bundleId;
	String pluginId;
	Long createTime;
	Long updateTime;

	public ActivityProviderTypeModel(){}
	public ActivityProviderTypeModel(String name, String modelNumber, final List<? extends IEndpointType> endpointTypes) {
		final String id = UUIDUtils.generateUUID(name, modelNumber);
		this.id = id;
		this.name = name;
		this.modelNumber = modelNumber;
	}

	@ApiModelProperty(dataType = "UUID", value = "ID of the activity provider type")	
	public String getId() {
		return id;
	}
	public ActivityProviderTypeModel setId(String id) {
		this.id = id;
		return this;
	}
	@ApiModelProperty(value = "Name of the activity provider type")	
	public String getName() {
		return name;
	}
	public ActivityProviderTypeModel setName(String name) {
		this.name = name;
		return this;
	}
	@ApiModelProperty(value = "Model number of the activity provider type")	
	public String getModelNumber() {
		return modelNumber;
	}
	public ActivityProviderTypeModel setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
		return this;
	}
	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public Long getBundleId() {
		return bundleId;
	}
	public ActivityProviderTypeModel setBundleId(Long bundleId) {
		this.bundleId = bundleId;
		return this;
	}
	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public String getPluginId() {
		return pluginId;
	}
	public ActivityProviderTypeModel setPluginId(String pluginId) {
		this.pluginId = pluginId;
		return this;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
}
