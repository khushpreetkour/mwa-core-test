package com.sony.pro.mwa.model.storage;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.model.GenericPropertyBase;
import com.sony.pro.mwa.utils.Converter;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value="Perspective model", description="Perspective represents a mapping for one of physical path about the location.")
public class PerspectiveModel {
	protected String perspective;
	protected String basePath;
	protected String user;
	protected String password;
	protected Map<String, Object> properties;
	String providerId;

	@ApiModelProperty(value = "Name of the perspective")
	public String getPerspective() {
		return perspective;
	}
	public void setPerspective(String perspective) {
		this.perspective = perspective;
	}
	@ApiModelProperty(value = "Base path of the perspective")
	public String getBasePath() {
		return basePath;
	}
	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}
	@ApiModelProperty(value = "User name to access location via the perspective")
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	@ApiModelProperty(value = "User password to access location via the perspective")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@XmlTransient @JsonIgnore
	public Map<String, Object> getProperties() {
		return properties;
	}
	@XmlTransient @JsonIgnore
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}
	@ApiModelProperty(value = "Properties of the perspective")
	public List<? extends GenericPropertyBase> getPropertyList() {
		return Converter.mapToPropertyList(properties);
	}
	public void setPropertyList(List<? extends GenericPropertyBase> propertieList) {
		this.properties = Converter.propertyListToMap(propertieList);
	}
	
	@ApiModelProperty(dataType = "UUID", value = "Provider ID of the local perspective owner")
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

}
