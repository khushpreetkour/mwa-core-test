package com.sony.pro.mwa.parameter.type;

import com.sony.pro.mwa.parameter.IParameterType;

public class TypeString extends TypeBase<String> {
	
	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeString;
	}
	@Override
	public String toStringImpl(String value) {
		// TODO Auto-generated method stub
		return value;
	}
	@Override
	protected String fromStringImpl(String valueStr) {
		// TODO Auto-generated method stub
		return valueStr;
	}
	
	public static TypeString instance() {
		return instance;
	}
	static TypeString instance = new TypeString();
}
