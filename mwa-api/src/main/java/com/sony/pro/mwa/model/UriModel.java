package com.sony.pro.mwa.model;

public class UriModel {
	protected String location;
	protected String relativePath;
	protected String query;
	protected String fragment;
	
	public String getLocation() {
		return location;
	}
	public UriModel setLocation(String location) {
		this.location = location;
		return this;
	}
	public String getRelativePath() {
		return relativePath;
	}
	public UriModel setRelativePath(String relativePath) {
		this.relativePath = relativePath;
		return this;
	}
	public String getQuery() {
		return query;
	}
	public UriModel setQuery(String query) {
		this.query = query;
		return this;
	}
	public String getFragment() {
		return fragment;
	}
	public UriModel setFragment(String fragment) {
		this.fragment = fragment;
		return this;
	}
}
