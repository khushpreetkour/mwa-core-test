package com.sony.pro.mwa.model.storage;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Deprecated
@ApiModel(value="Local perspective model", description="Local perspective is special perspective for specific provider.")
public class LocalPerspectiveModel extends PerspectiveModel {

	@ApiModelProperty(value = "Specific base path for the provider")
	public String getLocalBasePath() {
		return this.getBasePath();
	}
	public void setLocalBasePath(String localBasePath) {
		this.setBasePath(localBasePath);
	}
}
