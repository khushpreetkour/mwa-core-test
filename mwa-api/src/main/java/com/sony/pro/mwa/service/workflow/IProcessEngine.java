package com.sony.pro.mwa.service.workflow;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.OperationResult;

public interface IProcessEngine {
    public void reloadBpmns(List<String> bpmnPaths);
    public void reloadRules(List<String> rulePaths);
    public void deleteProcessInstance(Long processId);
    public void sendCancelSignal(Long processId);
    public Integer getState(Long processId);
    public Long startProcess(String name, Map<String, Object> workflowParams);
    public OperationResult startRule(String name, Map<String, Object> workflowParams);
    public ResourceRequestModel startResourceRule(String name, Map<String, Object> workflowParams);
    public void registerWorkItem(String workItemName, Object workItemHandler);
}
