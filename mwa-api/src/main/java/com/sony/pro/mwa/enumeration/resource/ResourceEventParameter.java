package com.sony.pro.mwa.enumeration.resource;

@Deprecated
public enum ResourceEventParameter {
	ASSIGNED_RESOURCE_KEY,
	ASSIGNED_RESOURCE_ID,
	ASSIGNED_RESOURCE_HOLDER_TYPE,
	ASSIGNED_RESOURCE_HOLDER_ID,
	
}
