package com.sony.pro.mwa.model.provider;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ActivityProviderCollection extends Collection<ActivityProviderModel> {

	public ActivityProviderCollection() {
		super();
	}

	public ActivityProviderCollection(List<ActivityProviderModel> models) {
		super(models);
	}

	public ActivityProviderCollection(List<ActivityProviderModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ActivityProviderCollection(Long totalCount, Long count,
			List<ActivityProviderModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ActivityProviderModel> getModels() {
		return super.getModels();
	}
}
