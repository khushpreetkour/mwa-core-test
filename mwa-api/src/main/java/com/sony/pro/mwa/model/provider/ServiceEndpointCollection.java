package com.sony.pro.mwa.model.provider;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ServiceEndpointCollection extends Collection<ServiceEndpointModel> {

	public ServiceEndpointCollection() {
		super();
	}

	public ServiceEndpointCollection(List<ServiceEndpointModel> models) {
		super(models);
	}

	public ServiceEndpointCollection(List<ServiceEndpointModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ServiceEndpointCollection(Long totalCount, Long count,
			List<ServiceEndpointModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ServiceEndpointModel> getModels() {
		return super.getModels();
	}
}
