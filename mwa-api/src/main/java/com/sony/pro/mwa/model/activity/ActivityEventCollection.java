package com.sony.pro.mwa.model.activity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

import java.util.List;

public class ActivityEventCollection extends Collection<ActivityEventModel> {
    public ActivityEventCollection() {
        super();
    }

    public ActivityEventCollection(List<ActivityEventModel> models) {
        super(models);
    }

    public ActivityEventCollection(List<ActivityEventModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public ActivityEventCollection(Long totalCount, Long count, List<ActivityEventModel> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<ActivityEventModel> getModels() {
        return super.getModels();
    }
}
