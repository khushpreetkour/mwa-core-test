package com.sony.pro.mwa.parameter.type;

import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.parameter.IParameterType;

public class TypeResource extends TypeBase<ResourceModel> {

	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeString;
	}
	@Override
	public String toStringImpl(ResourceModel value) {
		// TODO Auto-generated method stub
		//return Converter.listToXml(value);

		return Converter.toXmlString(value);
	}
	@Override
	protected ResourceModel fromStringImpl(String valueStr) {
		return Converter.toObject(valueStr, ResourceModel.class);
	}
	
	public static TypeResource instance() {
		return instance;
	}
	static TypeResource instance = new TypeResource();
}
