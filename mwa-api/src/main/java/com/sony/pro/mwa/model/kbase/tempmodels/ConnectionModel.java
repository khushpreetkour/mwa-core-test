package com.sony.pro.mwa.model.kbase.tempmodels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectionModel {
	private String fromConnectionPortId;
	private String toConnectionPortId;

	public String getFromConnectionPortId() {
		return fromConnectionPortId;
	}

	public void setFromConnectionPortId(String fromConnectionPortId) {
		this.fromConnectionPortId = fromConnectionPortId;
	}

	public String getToConnectionPortId() {
		return toConnectionPortId;
	}

	public void setToConnectionPortId(String toConnectionPortId) {
		this.toConnectionPortId = toConnectionPortId;
	}
}
