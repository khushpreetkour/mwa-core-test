package com.sony.pro.mwa.enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/11/27
 * Time: 3:09
 * To change this template use File | Settings | File Templates.
 */
public enum AxisEnum {
    ANCESTOR_OR_SELF,
    CHILD_OR_SELF,
    DESCENDANT_OR_SELF,
    PARENT_OR_SELF,
    SELF,
    ;
}
