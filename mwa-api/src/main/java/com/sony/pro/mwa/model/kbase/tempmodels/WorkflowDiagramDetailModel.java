package com.sony.pro.mwa.model.kbase.tempmodels;

import java.util.ArrayList;
import java.util.List;

public class WorkflowDiagramDetailModel extends WorkflowDiagramModel {
	private List<WorkflowModel> workflows = new ArrayList<>();
		
	public WorkflowDiagramDetailModel(WorkflowDiagramModel model) {
		this.setId(model.getId());
		this.setName(model.getName());
		this.setVersion(model.getVersion());
		this.setCreateTime(model.getCreateTime());
		this.setUpdateTime(model.getUpdateTime());
		this.setSteps(model.getSteps());
		this.setConnectionPorts(model.getConnectionPorts());
		this.setConnections(model.getConnections());
	}
	
	public void setWorkflows(List<WorkflowModel> workflows) {
		this.workflows = workflows;
	}
	
	public List<WorkflowModel> getWorkflows() {
		return workflows;
	}
}
