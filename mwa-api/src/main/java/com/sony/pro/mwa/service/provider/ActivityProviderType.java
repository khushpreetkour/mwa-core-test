package com.sony.pro.mwa.service.provider;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.enumeration.ServiceEndpointType;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointTypeModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.service.activity.IActivityTemplate;

public class ActivityProviderType {

	protected ActivityProviderTypeModel model;
	protected List<? extends IParameterDefinition> propertyList;
	protected List<IActivityTemplate> activityTemplateList;
	protected List<? extends IEndpointType> endpointTypes;

	public ActivityProviderType(String name, String modelNumber, final List<? extends IEndpointType> endpointTypeList, List<? extends IParameterDefinition> propertyList) {
		this.propertyList = propertyList;
		this.model = new ActivityProviderTypeModel(name, modelNumber, endpointTypeList);
		this.activityTemplateList = new ArrayList<>();
		{
			final String id = this.getId();
			this.endpointTypes = new ArrayList<ServiceEndpointTypeModel>(){{
				if (endpointTypes != null)
					for  (IEndpointType type : endpointTypeList) {
						add(new ServiceEndpointTypeModel(type.getName(), id));
					}
				else
					add(new ServiceEndpointTypeModel(ServiceEndpointType.DEFAULT.name(), id));
			}};
		}
	}
	
	public ActivityProviderTypeModel getModel() {
		return model;
	}
	public void setModel(ActivityProviderTypeModel model) {
		this.model = model;
	}

	public String getId() {
		return this.model.getId();
	}
	public void setId(String id) {
		this.model.setId(id);
	}
	
	public String getName() {
		return this.model.getName();
	}
	public void setName(String name) {
		this.model.setName(name);
	}

	public String getModelNumber() {
		return this.model.getModelNumber();
	}
	public void setModelNumber(String modelNumber) {
		this.model.setModelNumber(modelNumber);
	}

	public List<? extends IEndpointType> getEndpointTypeList() {
		return this.endpointTypes;
	}
	public void setEndpointTypeList(List<? extends IEndpointType> endpointTypeList) {
		this.endpointTypes = endpointTypeList;
	}

	public List<? extends IParameterDefinition> getPropertyList() {
		return propertyList;
	}
	public void setPropertyList(List<? extends IParameterDefinition> propertyList) {
		this.propertyList = propertyList;
	}
	
	public ActivityProviderType addActivityTemplate(IActivityTemplate template) {
		this.activityTemplateList.add(template);
		return this;
	}

	public ActivityProviderType addActivityTemplateList(List<? extends IActivityTemplate> templateList) {
		for (IActivityTemplate template : templateList) { 
			this.activityTemplateList.add(template);
		};
		return this;
	}
	public List<? extends IActivityTemplate> getActivityTemplateList() {
		return this.activityTemplateList;
	}
}
