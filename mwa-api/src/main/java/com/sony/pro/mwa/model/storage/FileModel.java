package com.sony.pro.mwa.model.storage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="File model", description="")
/**
 * ディレクトリ直下に置かれるファイルを示すモデル
 */
public class FileModel {

    // このファイルのID
    private String id;
    // ファイル名
    private String name;
    // このファイルを示すURI
    private String uri;
    // このファイルへのパス
    private String nativePath;

    @ApiModelProperty(value = "ID of this file.")
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }

    @ApiModelProperty(value = "name.")
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "Uri of this file.")
    public String getUri() {
        return this.uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    @ApiModelProperty(value = "nativePath to this file.")
    public String getNativePath() {
        return this.nativePath;
    }
    public void setNativePath(String nativePath) {
        this.nativePath = nativePath;
    }

}
