package com.sony.pro.mwa.enumeration.provider;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeString;

public enum PresetProviderProperties implements IParameterDefinition {
	defaultPerspective {
		@Override
		public IParameterType getType() {return TypeString.instance();}	//TODO:将来的にはPerspective型に変更？
	},
	;

	String key;
	
	private PresetProviderProperties() {
		this.key = this.name();
	}
	private PresetProviderProperties(String key) {
		this.key = key;
	}
	
	@Override
	public String getKey() {
		return this.key;
	}
	@Override
	public boolean getRequired() {
		return false;
	}
	@Override
	public IParameterType getType() {
		return TypeString.instance();
	}
	@Override
	public String getTypeName() {
		return getType().name();
	}
}
