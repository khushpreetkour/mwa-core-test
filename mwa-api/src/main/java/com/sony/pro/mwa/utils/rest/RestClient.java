package com.sony.pro.mwa.utils.rest;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.ILoggerSourceWithPrefix;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;

public class RestClient implements ILoggerSourceWithPrefix {

	private static MwaLogger logger = MwaLogger.getLogger(RestClient.class);

	private String instanceId;

	private String responseBody;

	private int statusCode;

	/**
	 * defaultのHTTPヘッダの設定
	 */
	private final static Header[] defaultHeaders = { new BasicHeader("Content-type", "application/json; charset=UTF-8"), new BasicHeader("Accept", "application/json"),
			new BasicHeader("Accept-Charset", "UTF-8") };

	/*-------------------------------------------------------------------------------------------
	 *  コンストラクタ
	 -------------------------------------------------------------------------------------------*/
	public RestClient(String RestClient) {
		this.instanceId = instanceId;
	}

	public void rest(IRestInput restInput) {
		switch (restInput.getHttpMethod()) {
		case "POST":
			restPost(restInput);
			break;
		case "GET":
			restGet(restInput);
			break;
		case "PUT":
			restPut(restInput);
			break;
		case "DELETE":
			restDelete(restInput);
			break;
		default:
			break;
		}

	}

	private boolean isNotCustomHttpHeaders(Map<String, String> httpHeadersMap) {

		if (httpHeadersMap != null && httpHeadersMap.size() != 0) {
			return true;
		}
		return false;
	}

	private Header[] createHttpHeaders(Map<String, String> httpHeadersMap) {
		List<Header> list = new ArrayList<Header>();
		for (String key : httpHeadersMap.keySet()) {
			list.add(new BasicHeader(key, httpHeadersMap.get(key)));
		}
		return (Header[]) list.toArray(new Header[0]);
	}

	public void restPost(IRestInput restInput) {

		final String url = restInput.getUrl();
		final String jsonParam = restInput.getJsonParam();

		if (isNotCustomHttpHeaders(restInput.getHttpHeadersMap())) {
			executePost(url, jsonParam, createHttpHeaders(restInput.getHttpHeadersMap()));
		} else {
			executePost(url, jsonParam, RestClient.defaultHeaders);
		}
	}

	public void restGet(IRestInput restInput) {
		final String url = restInput.getUrl();

		if (isNotCustomHttpHeaders(restInput.getHttpHeadersMap())) {
			executeGet(url, createHttpHeaders(restInput.getHttpHeadersMap()));
		} else {
			executeGet(url, RestClient.defaultHeaders);
		}
	}

	public void restPut(IRestInput restInput) {

		final String url = restInput.getUrl();
		final String jsonParam = restInput.getJsonParam();

		if (isNotCustomHttpHeaders(restInput.getHttpHeadersMap())) {
			executePut(url, jsonParam, createHttpHeaders(restInput.getHttpHeadersMap()));
		} else {
			executePut(url, jsonParam, RestClient.defaultHeaders);
		}
	}

	public void restDelete(IRestInput restInput) {

		final String url = restInput.getUrl();

		if (isNotCustomHttpHeaders(restInput.getHttpHeadersMap())) {
			executeDelete(url, createHttpHeaders(restInput.getHttpHeadersMap()));
		} else {
			executeDelete(url, RestClient.defaultHeaders);
		}
	}

	private void executePost(String url, String jsonParam, Header... headers) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPost request = new HttpPost(url);
			request.setEntity(new StringEntity(jsonParam, StandardCharsets.UTF_8));
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request, headers);
			httpStatusCodeCheck(this.statusCode);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new MwaError(
					MWARC.CONNECTION_FAILED,
					null,
					"Signals that an I/O exception of some sort has occurred. This class is the general class of exceptions produced by failed or interrupted I/O operations.");
		} catch (Exception e) {
			throw e;
		}
	}

	private void executeGet(String url, Header... headers) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet request = new HttpGet(url);
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request, headers);
			httpStatusCodeCheck(this.statusCode);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new MwaError(
					MWARC.CONNECTION_FAILED,
					null,
					"Signals that an I/O exception of some sort has occurred. This class is the general class of exceptions produced by failed or interrupted I/O operations.");
		} catch (Exception e) {
			throw e;
		}

	}

	private void executePut(String url, String jsonParam, Header... headers) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPut request = new HttpPut(url);
			request.setEntity(new StringEntity(jsonParam, StandardCharsets.UTF_8));
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request, headers);
			httpStatusCodeCheck(this.statusCode);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new MwaError(
					MWARC.CONNECTION_FAILED,
					null,
					"Signals that an I/O exception of some sort has occurred. This class is the general class of exceptions produced by failed or interrupted I/O operations.");
		} catch (Exception e) {
			throw e;
		}
	}

	private void executeDelete(String url, Header... headers) {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpDelete request = new HttpDelete(url);
			// Httpリクエスト実行
			httpReauestExecute(httpClient, request);
			httpStatusCodeCheck(this.statusCode);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new MwaError(
					MWARC.CONNECTION_FAILED,
					null,
					"Signals that an I/O exception of some sort has occurred. This class is the general class of exceptions produced by failed or interrupted I/O operations.");
		} catch (Exception e) {
			throw e;
		}
	}

	private void httpReauestExecute(CloseableHttpClient httpClient, final HttpUriRequest request, Header... headers) throws MwaError, MwaInstanceError {

		// HTTPヘッダの設定
		request.setHeaders(headers);
		try (CloseableHttpResponse response = httpClient.execute(request)) {

			// HttpStatusコードをセット
			setStatusCode(response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();
			setResponseBody(EntityUtils.toString(entity, StandardCharsets.UTF_8));

		} catch (ClientProtocolException e) {
			logger.error(e.getMessage());
			throw new MwaError(MWARC.CONNECTION_FAILED, null, "Signals an error in the HTTP protocol.");
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new MwaError(
					MWARC.CONNECTION_FAILED,
					null,
					"Signals that an I/O exception of some sort has occurred. This class is the general class of exceptions produced by failed or interrupted I/O operations.");
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, e.getMessage());
		}
	}

	public void httpStatusCodeCheck(int statusCode) throws MwaError {

		switch (statusCode) {

		case 200:
			break;
		case 400:
		case 401:
		case 402:
		case 403:
		case 404:
		case 405:
		case 406:
		case 407:
		case 409:
		case 410:
		case 411:
		case 412:
		case 413:
		case 414:
		case 415:
		case 416:
		case 417:
			logger.error("HTTP StatusCode is " + statusCode + "(4XX Client Error).");
			throw createError();
		case 500:
		case 501:
		case 502:
		case 503:
		case 504:
		case 505:
			logger.error("HTTP StatusCode is " + statusCode + "(5XX Server Error).");
			throw createError();

		default:
			logger.error("HTTP StatusCode is " + statusCode + " .");
			throw createError();
		}

	}

	private MwaError createError() {
		Map<String, Object> responseMap = null;
		try {
			responseMap = new ObjectMapper().readValue(getResponseBody(), HashMap.class);
		} catch (JsonParseException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json generation error setPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json mapping error setPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"jason mapping IO error setPersistentDataImpl failed.");
		}
		String target = (String) responseMap.get("message");
		String details = (String) responseMap.get("deviceMessage");
		// 初期値
		MWARC mwarc = MWARC.DEVICE_ERROR;
		for (MWARC elem : MWARC.values()) {
			if (elem.name().equals(target)) {
				mwarc = elem;
				break;
			}
		}
		return new MwaError(mwarc, null, details);
	}

	@Override
	public String getLoggerPrefix() {
		return "[" + this.instanceId + "]";
	}

	public String getResponseBody() {
		return responseBody;
	}

	private void setResponseBody(final String responseBody) {
		logger.debug(responseBody);
		this.responseBody = responseBody;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
}
