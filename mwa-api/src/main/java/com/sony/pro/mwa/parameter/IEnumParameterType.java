package com.sony.pro.mwa.parameter;

import java.util.List;

public interface IEnumParameterType {
	public List<String> values();
}
