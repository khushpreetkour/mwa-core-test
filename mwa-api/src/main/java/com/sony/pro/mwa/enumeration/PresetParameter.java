package com.sony.pro.mwa.enumeration;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeActivityProviderId;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeLong;
import com.sony.pro.mwa.parameter.type.TypeString;

public enum PresetParameter implements IParameterDefinition {
	/** 暫定。エラーコード。将来的にコードでなくExceptionに切り替える可能性あり */
	ErrorCode,
	/** 暫定。DeviceのResponseCode */
	DeviceResponseCode,
	/** 暫定。DeviceのResponse詳細 */
	DeviceResponseDetails,
	/** 暫定。Workflowの終端ステップの名前 */
	EndStepName,
	/** 暫定。エラーの詳細。こちらも廃止の可能性あり */
	@Deprecated
	ErrorDetails,
	/** ActivityInstanceに関して発生したEvent用。ActivityFrameworkとActivity実装側のI/Fを想定。例．ERROR、CANCELLED */
	Event,
	/** ActivityInstanceの進捗用。下から上まで共通で使用 */
	Progress,
	/** ActivityInstanceへのInput値 */
	InputDetails,
	/** ActivityInstanceへのOutput値 */
	OutputDetails,
	/** ActivityInstanceへのProperties値 */
	Properties,
	/** ActivityInstanceの動作中データ */
	InstanceDetails,
	/** ActivityTemplateのID  */
	ActivityTemplateId,
	/** ActivityTemplateの名前、TemplateのName+Versionでユニーク */
	ActivityTemplateName,
	/** ActivityTemplateのVersion、TemplateのName+Versionでユニーク */
	ActivityTemplateVersion,
	/** Activity(Template)の種別  */
	ActivityType,
	/** ActivityのインスタンスID */
	ActivityInstanceId,
    /** ActivityのインスタンスID */
    ActivityInstancePriority,
	/** ActivityのインスタンスID */
	ActivityInstanceStatus,
	/** ActivityのインスタンスID */
	ActivityInstanceStatusDetails,
	/** ActivityのインスタンスID */
	ActivityInstanceDeviceResponse,
	/** ActivityのインスタンスID */
	ActivityInstanceDeviceResponseDetails,
	/** ResourceHolder()形式から取得したResourceId */
	ResourceIdOfHolder,
	/** ResourceHolder()形式から取得したResourceType */
	ResourceTypeOfHolder,
	/** ActivityのParentインスタンスID */
	ParentInstanceId,
	/** ActivityインスタンスのParentインスタンス内の名前、Parent内でユニーク */
	LocalName,
	/** ActivityインスタンスをDBに記録するかどうか。trueなら記録しない。Syncの場合のみ有効 */
	Volatile {
		@Override
		public IParameterType getType() {return TypeBoolean.instance();}
	},
	/** ActivityProvider (= device) のID。このIDのProviderを使ってActivityを実行する */
	ProviderId {
		@Override
		public IParameterType getType() {return TypeActivityProviderId.instance();}
	},
	/** ActivityProvider (= device) のID。このIDのProviderを使ってActivityを実行する(Resource解決前の値) */
	OriginalProviderId {
		@Override
		public IParameterType getType() {return TypeActivityProviderId.instance();}
	},
	/** Proxyとして使用するMWAのActivityProvider (= device) のID。このIDのProviderを使ってActivityを実行する */
	ProxyId {
		@Override
		public IParameterType getType() {return TypeActivityProviderId.instance();}
	},
	NumberOfRuns {
		@Override
		public IParameterType getType() {return TypeLong.instance();}
	},
	/** ActivityTemplateのID  */
	ExternalActivityTemplateId,
	/** ActivityTemplateの名前、TemplateのName+Versionでユニーク */
	ExternalActivityTemplateName,
	/** ActivityTemplateのVersion、TemplateのName+Versionでユニーク */
	ExternalActivityTemplateVersion,
	/** [暫定] 状態遷移上、Cancelを受け付ける場合でも、Cancelable=falseにするとcancelを受け付けず、状態遷移エラーを返す */
	@Deprecated
	IgnoreCancel,
	/** BPMNのプロセス名 */
	@Deprecated
	ProcessName,
	/** ActivityProfile のID。このIDのProviderを使ってActivityを実行する */
	ProfileId {
		@Override
		public IParameterType getType() {return TypeString.instance();}

		public boolean getVisibleFlag() {
			return false;
		}
	},
	;

	Object defaultValue;
	String key;
	protected IParameterType type = TypeString.instance();
	protected boolean required = false;
	protected String description;	//パラメータの説明（多言語やるか？）

	private PresetParameter() {
		this(null);
	}
	private PresetParameter(String key, Object defaultValue) {
		this(defaultValue);
		this.key = key;
	}
	private PresetParameter(Object defaultValue) {
		this.defaultValue = defaultValue;
		this.key = this.name();
	}

	public Object getDefaultValue() {return this.defaultValue;} //いらないかも
	@Override
	public String getKey() {
		return this.key;
	}
	@Override
	public boolean getRequired() {
		return this.required;
	}
	@Override
	public IParameterType getType() {
		return this.type;
	}
	@Override
	public String getTypeName() {
		return getType().name();
	}
}
