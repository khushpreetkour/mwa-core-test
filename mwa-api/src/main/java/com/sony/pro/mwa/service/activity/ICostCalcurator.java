package com.sony.pro.mwa.service.activity;

import java.util.Map;

import com.sony.pro.mwa.model.resource.CostModel;

public interface ICostCalcurator {
	public CostModel calcCost(Map<String, Object> params);
}
