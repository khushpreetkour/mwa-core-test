package com.sony.pro.mwa.utils.rest;

import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.utils.rest.IRestInput;

public class RestInputImpl implements IRestInput {
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	  
	private String httpMethod;
	private String url;
	private String jsonParam;
	private Map<String,String> httpHeadersMap;
	
	public RestInputImpl(String httpMethod,String url,String jsonParam) {

			this.httpMethod = httpMethod; 	
			this.url = url; 
			this.jsonParam = jsonParam; 
	}
	
	public RestInputImpl(String httpMethod,String url,String jsonParam,Map<String,String> httpHeadersMap) {

		this.httpMethod = httpMethod; 	
		this.url = url; 
		this.jsonParam = jsonParam; 
		this.httpHeadersMap = httpHeadersMap;
		
	}
	
	@Override
	public String getHttpMethod() {
		return httpMethod;
	}

	@Override
	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String getJsonParam() {
		return jsonParam;
	}

	@Override
	public void setJsonParam(String jsonParam) {
		this.jsonParam = jsonParam;
	}

	@Override
	public Map<String, String> getHttpHeadersMap()
	{
		return httpHeadersMap;
	}

	@Override
	public void setHttpHeadersMap(Map<String, String> httpHeadersMap)
	{
		this.httpHeadersMap = httpHeadersMap;
	}


}
