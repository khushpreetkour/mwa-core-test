package com.sony.pro.mwa.enumeration.event;

public enum EventPriority {
	HIGHEST(0),
	DEFAULT(50),
	LOWEST(100),
	RESOURCE_DEFAULT(30),
	WORKFLOW_STEP_DONE(40),
	ACTIVITY_INSTANCE_REQUEST_SUBMIT(50),	//特別扱いは止める、FIFOで処理しないと次ステップの実行が進まないことに
	;
	
	EventPriority(Integer priority) {
		this.priority = priority;
	}
	Integer priority;
	
	public Integer getValue() {return this.priority;}
}
