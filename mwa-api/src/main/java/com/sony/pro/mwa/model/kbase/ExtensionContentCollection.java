package com.sony.pro.mwa.model.kbase;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

import java.util.List;

public class ExtensionContentCollection extends Collection<ExtensionContentModel> {
    public ExtensionContentCollection() {
        super();
    }

    public ExtensionContentCollection(List<ExtensionContentModel> models) {
        super(models);
    }

    public ExtensionContentCollection(List<ExtensionContentModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public ExtensionContentCollection(Long totalCount, Long count, List<ExtensionContentModel> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<ExtensionContentModel> getModels() {
        return super.getModels();
    }
}
