package com.sony.pro.mwa.model.storage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="Directory model", description="")
public class DirectoryModel {
    String id;
    String name;
    String uri;
    String nativePath;
    List<DirectoryModel> directories = new ArrayList<DirectoryModel>();
    List<FileModel> files = new ArrayList<FileModel>();

    @ApiModelProperty(value = "ID of the directory")
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }

    @ApiModelProperty(value = "Name of the directory")
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "Uri of the directory")
    public String getUri() {
        return this.uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    @ApiModelProperty(value = "nativePath of the directory")
    public String getNativePath() {
        return this.nativePath;
    }
    public void setNativePath(String nativePath) {
        this.nativePath = nativePath;
    }

    @ApiModelProperty(value = "List of child directories.")
    public List<DirectoryModel> getDirectories() {
        return this.directories;
    }
    public void setDirectories(List<DirectoryModel> directories) {
        this.directories = (directories == null) ? new ArrayList<DirectoryModel>() : directories;
    }

    @ApiModelProperty(value = "List of file paths located in this directory.")
    public List<FileModel> getFiles() { return this.files; }
    public void setFiles(List<FileModel> files) {
        this.files = (files == null) ? new ArrayList<FileModel>() : files;
    }

}
