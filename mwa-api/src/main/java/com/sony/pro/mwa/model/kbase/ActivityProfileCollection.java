package com.sony.pro.mwa.model.kbase;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ActivityProfileCollection extends Collection<ActivityProfileModel> {
	public ActivityProfileCollection() {
		super();
	}

	public ActivityProfileCollection(List<ActivityProfileModel> models) {
		super(models);
	}

	public ActivityProfileCollection(List<ActivityProfileModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public ActivityProfileCollection(Long totalCount, Long count, List<ActivityProfileModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ActivityProfileModel> getModels() {
		return super.getModels();
	}
}
