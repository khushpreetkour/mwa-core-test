package com.sony.pro.mwa.enumeration;

public enum SystemPropertyKey {
	OSGI_CACHE_ROOT("mwa.osgi.cache.root"),
	;
	private SystemPropertyKey(String propertyString) {
		this.propertyString = propertyString;
	}
	private final String propertyString;
	
	@Override
	public String toString() {return propertyString;}
}
