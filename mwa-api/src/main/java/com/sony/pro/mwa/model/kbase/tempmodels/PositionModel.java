package com.sony.pro.mwa.model.kbase.tempmodels;

public class PositionModel {
	private Integer x;
	private Integer y;
	private Integer z;
	
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	public Integer getZ() {
		return z;
	}
	public void setZ(Integer z) {
		this.z = z;
	}
}
