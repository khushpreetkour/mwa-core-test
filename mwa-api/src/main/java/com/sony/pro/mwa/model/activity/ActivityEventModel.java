package com.sony.pro.mwa.model.activity;

import java.util.Map;

import com.wordnik.swagger.annotations.ApiModelProperty;

public class ActivityEventModel {
	
	public ActivityEventModel(){
		this(null,null,null,null,PRIORITY_DEFAULT,null);
	}
	public ActivityEventModel(String instanceId, String operation, Map<String, Object> params){
		this(null, instanceId, operation, params, PRIORITY_DEFAULT, null);
	}
	public ActivityEventModel(String instanceId, String operation, Map<String, Object> params, Integer priority){
		this(null, instanceId, operation, params, priority, null);
	}
	public ActivityEventModel(Integer id, String instanceId, String operation, Map<String, Object> params, Integer priority, Long createdTime){
		this.id = id;
		this.instanceId = instanceId;
		this.priority = priority;
		this.operation = operation;
		this.params = params;
		this.createdTime = createdTime;
	}
	
	protected Integer id;
	protected String instanceId;
	protected String operation;
	protected Integer priority;
	protected Long createdTime;
	protected Map<String, Object> params;
	

	@ApiModelProperty(value = "Activity tempalte ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public Map<String, Object> getParams() {
		return params;
	}
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Long getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Long createdTime) {
		this.createdTime = createdTime;
	}
	
	static public Integer PRIORITY_MAX = 1;
	static public Integer PRIORITY_MIN = 100;
	static public Integer PRIORITY_DEFAULT = 50;
}
