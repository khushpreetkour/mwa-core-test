package com.sony.pro.mwa.model.provider;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value="Service endpoint model", description="Service endpoint is the URL where the service can be accessed by a client application. "
		+ "Some web services can have multiple endpoints, for example in order to make it available using different protocols, different purpose, etc")
public class ServiceEndpointModel {
	String id;
	String providerId;
	String endpointType;
	String host;
	Integer port;
	String user;
	String password;

	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String serviceId) {
		this.providerId = serviceId;
	}
	@ApiModelProperty(value = "Type of the endpoint. And it is used to distinguish each endpoint. "
			+ "For example, one transcode activity may be achieved by endpoint of EXECUTION (http://host/exec) and OBSERVATION (http://host/status). "
			+ "Activity can use different endpoint address internaly based on the endpoint types. "
			+ "MWA assigns \"Default\" as default type if it's not psecified.")
	public String getEndpointType() {
		return endpointType;
	}
	public void setEndpointType(String endpointType) {
		this.endpointType = endpointType;
	}
	@ApiModelProperty(value = "Host name or address of the endpoint. ")
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	@ApiModelProperty(value = "Port number of the endpoint. ")
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	@ApiModelProperty(value = "User name to access the endpoint. ", required=false)
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	@ApiModelProperty(value = "Password to access the endpoint. ", required=false)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
