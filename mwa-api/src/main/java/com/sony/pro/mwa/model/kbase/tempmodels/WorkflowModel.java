package com.sony.pro.mwa.model.kbase.tempmodels;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowModel extends WorkflowDiagramModel{
	private String type;
	private String templateId;
	private boolean deleteFlag;
	private boolean syncFlag;
	private String diagramId;
	private String alias;
	private boolean predefineFlag;
	private List<String> tags = new ArrayList();
	
//	@JsonIgnore
	protected List<WFParameterDefinition> inputs = new ArrayList<>();
//	@JsonIgnore
	protected List<WFParameterDefinition> outputs = new ArrayList<>();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public boolean isSyncFlag() {
		return syncFlag;
	}

	public void setSyncFlag(boolean syncFlag) {
		this.syncFlag = syncFlag;
	}

	public boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getDiagramId() {
		return diagramId;
	}

	public void setDiagramId(String diagramId) {
		this.diagramId = diagramId;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	public boolean getPredefineFlag() {
		return predefineFlag;
	}

	public void setPredefineFlag(boolean predefineFlag) {
		this.predefineFlag = predefineFlag;
	}
	
	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
//	@JsonIgnore
	public List<WFParameterDefinition> getInputs() {
		return inputs;
	}
	
	public void setInputs(List<WFParameterDefinition> inputs) {
		this.inputs = inputs;
	}
	
//	@JsonIgnore
	public List<WFParameterDefinition> getOutputs() {
		return outputs;
	}
	
	public void setOutputs(List<WFParameterDefinition> outputs) {
		this.outputs = outputs;
	}
	
	public List<WFParameterDefinition> importInputs(ArrayList inputList) {
		logger.info("### WorkflowDiaglamModel.importInputs ");
		return this.importMap(inputList, this.inputs);
	}
	public List<WFParameterDefinition> importOutputs(ArrayList outputList) {
		logger.info("### WorkflowDiaglamModel.importOutputs");
		return this.importMap(outputList, this.outputs);
	}
}
