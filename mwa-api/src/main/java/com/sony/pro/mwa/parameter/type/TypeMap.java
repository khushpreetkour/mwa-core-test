package com.sony.pro.mwa.parameter.type;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.parameter.IParameterType;

public class TypeMap extends TypeBase<Map<String, String>> {
	ObjectParameterType<MapEntries> mapType = new ObjectParameterType<MapEntries>(MapEntries.class);
	
	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeMap;
	}

	@Override
	protected Map<String, String> fromStringImpl(String valueStr) {
		MapEntries map = mapType.fromStringImpl(valueStr);
		return map.getEntries();
	}
	
	public static TypeMap instance() {
		return instance;
	}

	@Override
	protected String toStringImpl(Map<String, String> value) {
		MapEntries map = new MapEntries();
		map.setEntries(value);
		return mapType.toString(map);
	}
	
	static TypeMap instance = new TypeMap();
		 
	@XmlRootElement(name = "map")
	@XmlAccessorType(XmlAccessType.FIELD  )
	private static class MapEntries{
		Map<String, String> entries;

		/**
		 * @return the entries
		 */
		public Map<String, String> getEntries() {
			return entries;
		}

		/**
		 * @param entries the entries to set
		 */
		public void setEntries(Map<String, String> entries) {
			this.entries = entries;
		}
	}
}
