package com.sony.pro.mwa.model.scheduler;

public class SelectTriggerInfoInputModel {

	private String schedId;

	// private String tag;

	public String getSchedId()
	{
		return schedId;
	}

	public void setSchedId(String schedId)
	{
		this.schedId = schedId;
	}

}
