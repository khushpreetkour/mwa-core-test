package com.sony.pro.mwa.model.kbase;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.wordnik.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityProfileModel {
	private String id;
	private String groupId;
	private String templateId;
	private String name;
	private String icon;
	private boolean optionalFieldEditableFlag = true;
	private Map<String, Object> parameter;
	private Integer priority;
	private boolean schedulableFlag = false;
	private Long createTime;
	private Long updateTime;

	@ApiModelProperty(value = "ID of the parameter set.")
	public String getId() {
		return id;
	}

	public ActivityProfileModel setId(String id) {
		this.id = id;
		return this;
	}

	@ApiModelProperty(value = "GroupId of the parameter set.")
	public String getGroupId() {
		return groupId;
	}

	public ActivityProfileModel setGroupId(String groupId) {
		this.groupId = groupId;
		return this;
	}

	@ApiModelProperty(value = "TemplateId of the parameter set.")
	public String getTemplateId() {
		return templateId;
	}

	public ActivityProfileModel setTemplateId(String templateId) {
		this.templateId = templateId;
		return this;
	}

	@ApiModelProperty(value = "Name of the parameter set.")
	public String getName() {
		return name;
	}

	public ActivityProfileModel setName(String name) {
		this.name = name;
		return this;
	}

	@ApiModelProperty(value = "Icon of the parameter set.")
	public String getIcon() {
		return icon;
	}

	public ActivityProfileModel setIcon(String icon) {
		this.icon = icon;
		return this;
	}

	@ApiModelProperty(value = "OptionalFieldEditableFlag of the parameter set.")
	public boolean getOptionalFieldEditableFlag() {
		return optionalFieldEditableFlag;
	}

	public ActivityProfileModel setOptionalFieldEditableFlag(boolean optionalFieldEditableFlag) {
		this.optionalFieldEditableFlag = optionalFieldEditableFlag;
		return this;
	}

	@ApiModelProperty(value = "Parameter of the parameter set.")
	public Map<String, Object> getParameter() {
		return this.parameter;
	}

	public ActivityProfileModel setParameter(Map<String, Object> map) {
		this.parameter = map;
		return this;
	}

	@ApiModelProperty(value = "Priority of the parameter set.")
	public Integer getPriority() {
		return this.priority;
	}

	public ActivityProfileModel setPriority(Integer priority) {
		this.priority = priority;
		return this;
	}

	@ApiModelProperty(value = "SchedulableFlag of the parameter set.")
	public boolean getSchedulableFlag() {
		return this.schedulableFlag;
	}

	public ActivityProfileModel setSchedulableFlag(boolean schedulableFlag) {
		this.schedulableFlag = schedulableFlag;
		return this;
	}

	@ApiModelProperty(value = "Create Time of the parameter set.")
	public Long getCreateTime() {
		return createTime;
	}

	public ActivityProfileModel setCreateTime(Long createTime) {
		this.createTime = createTime;
		return this;
	}

	@ApiModelProperty(value = "Update Time of the parameter set.")
	public Long getUpdateTime() {
		return updateTime;
	}

	public ActivityProfileModel setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
		return this;
	}
}
