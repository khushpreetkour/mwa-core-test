package com.sony.pro.mwa.model.storage;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

@Deprecated
public class LocalPerspectiveCollection extends Collection<LocalPerspectiveModel> {

	public LocalPerspectiveCollection() {
		super();
	}

	public LocalPerspectiveCollection(List<LocalPerspectiveModel> models) {
		super(models);
	}

	public LocalPerspectiveCollection(List<LocalPerspectiveModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public LocalPerspectiveCollection(Long totalCount, Long count, List<LocalPerspectiveModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<LocalPerspectiveModel> getModels() {
		return super.getModels();
	}
}
