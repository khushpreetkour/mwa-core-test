package com.sony.pro.mwa.model.activity;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

import java.util.List;

public class ActivityTemplateCollection extends Collection<ActivityTemplateModel> {
    public ActivityTemplateCollection() {
        super();
    }

    public ActivityTemplateCollection(List<ActivityTemplateModel> models) {
        super(models);
    }

    public ActivityTemplateCollection(List<ActivityTemplateModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public ActivityTemplateCollection(Long totalCount, Long count, List<ActivityTemplateModel> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<ActivityTemplateModel> getModels() {
        return super.getModels();
    }
}
