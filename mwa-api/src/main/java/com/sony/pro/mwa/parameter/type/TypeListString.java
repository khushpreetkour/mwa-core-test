package com.sony.pro.mwa.parameter.type;

import java.util.List;

import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.parameter.IParameterType;

public class TypeListString extends TypeBase<List<String>> {

	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeString;
	}
	@Override
	public String toStringImpl(List<String> value) {
		// TODO Auto-generated method stub
		//return Converter.listToXml(value);
		return Converter.toXml(value);
	}
	@Override
	protected List<String> fromStringImpl(String valueStr) {
		return Converter.fromXml(valueStr);
	}
	
	public static TypeListString instance() {
		return instance;
	}
	static TypeListString instance = new TypeListString();
}
