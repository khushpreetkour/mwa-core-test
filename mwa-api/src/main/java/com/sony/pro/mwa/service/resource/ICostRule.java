package com.sony.pro.mwa.service.resource;

import java.util.Collection;
import java.util.Map;

import com.sony.pro.mwa.service.activity.IActivityTemplate;

public interface ICostRule {
	public Collection<String> getCostFromParam(IActivityTemplate template, Map<String, Object> param);
	public void setResourceToParam(Map<String, Object> param);
}
