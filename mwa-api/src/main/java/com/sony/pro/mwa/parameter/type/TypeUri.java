package com.sony.pro.mwa.parameter.type;

/***
 * パラメータ定義「URI型」
 */
public class TypeUri extends TypeString {

    static TypeUri instance = new TypeUri();

    /**
     * インスタンスを取得する
     * @return
     */
    public static TypeUri instance() {
        return instance;
    }
}
