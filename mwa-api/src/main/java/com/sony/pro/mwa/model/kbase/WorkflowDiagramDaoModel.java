package com.sony.pro.mwa.model.kbase;

public class WorkflowDiagramDaoModel {
	private String workflowId;
	private String workflowName;
	private Boolean deleteFlag;
	Long createTime;
	Long updateTime;
	private String version;
	private String model;
	
	public String getWorkflowId() {
		return workflowId;
	}
	public WorkflowDiagramDaoModel setWorkflowId(String workflowId) {
		this.workflowId = workflowId;
		return this;
	}
	public String getWorkflowName() {
		return workflowName;
	}
	public WorkflowDiagramDaoModel setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
		return this;
	}
	public Boolean getDeleteFlag() {
		return deleteFlag;
	}
	public WorkflowDiagramDaoModel setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
		return this;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
	public String getVersion() {
		return version;
	}
	public WorkflowDiagramDaoModel setVersion(String version) {
		this.version = version;
		return this;
	}
	public String getModel() {
		return model;
	}
	public WorkflowDiagramDaoModel setModel(String model) {
		this.model = model;
		return this;
	}
}
