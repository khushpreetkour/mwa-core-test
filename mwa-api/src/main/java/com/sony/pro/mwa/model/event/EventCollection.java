package com.sony.pro.mwa.model.event;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

import java.util.List;

public class EventCollection extends Collection<EventModel> {
    public EventCollection() {
        super();
    }

    public EventCollection(List<EventModel> models) {
        super(models);
    }

    public EventCollection(List<EventModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public EventCollection(Long totalCount, Long count, List<EventModel> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<EventModel> getModels() {
        return super.getModels();
    }
}
