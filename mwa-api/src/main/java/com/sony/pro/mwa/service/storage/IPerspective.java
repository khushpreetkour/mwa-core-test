package com.sony.pro.mwa.service.storage;

public interface IPerspective {
	public String name();
	public boolean isBackSlash();
	public boolean isUri();
	public Boolean isMatch(String path);
}
