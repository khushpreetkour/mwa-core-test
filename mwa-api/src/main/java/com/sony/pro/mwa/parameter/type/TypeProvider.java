package com.sony.pro.mwa.parameter.type;

import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeString;

public class TypeProvider extends TypeString {
	
	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeProvider;
	}

	static TypeProvider instance = new TypeProvider();
	public static TypeProvider instance() { return instance; }
}
