package com.sony.pro.mwa.model.provider;

public class ActivityTemplateInfoModel {
	String providerTypeId;
	String name;
	String version;
	
	public String getProviderTypeId() {
		return providerTypeId;
	}
	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
}
