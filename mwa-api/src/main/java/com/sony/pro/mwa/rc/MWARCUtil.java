package com.sony.pro.mwa.rc;

import java.util.HashMap;
import java.util.regex.Pattern;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;

public class MWARCUtil {

	private final static MwaLogger logger = MwaLogger.getLogger(MWARCUtil.class);
	
	public static String genMWARC(String groupId, String localId) {
		return IMWARC.PREFIX + IMWARC.DELIMITER + groupId + localId;
	}
	
	public static String getGroupId(String code) {
		int beginIndex = IMWARC.PREFIX.length() + IMWARC.DELIMITER.length();
		return code.substring(beginIndex, beginIndex + 4);
	}
	
	public static boolean checkFormat(java.util.regex.Pattern pattern, String code) {
		java.util.regex.Matcher m = pattern.matcher(code);
		return m.matches();
	}
	
	public static boolean checkFormat(String code) {
		return checkFormat(pattern, code);
	}
	
	private static java.util.regex.Pattern pattern = Pattern.compile(IMWARC.PATTERN);
	
	//Factory用のロジック
	private static java.util.Map<String, IMWARCFactory> factoryMap = new HashMap<String, IMWARCFactory>();
	
	//Factory登録用
	public static void addFactory(IMWARCFactory factory) {
		if (factory ==  null) {
			String message = "addFactory: factory is null";
			logger.error(message);
			throw new MwaError(MWARC.INTEGRATION_ERROR);
		} else if (factoryMap.containsKey(factory.getGroupId())) {
			String message = "addFactory: Duplicate GroupId(="+factory.getGroupId()+") is specified for factory(=" + factory.toString() + ")";
			logger.error(message);
			throw new MwaError(MWARC.INTEGRATION_DUPLICATE_FACTORY_REGISTRATION);
		} else {
			factoryMap.put(factory.getGroupId(), factory);
		}
	}
	
	//MWARCインスタンス取得用
	public static IMWARC create(String code) {
		if (checkFormat(code))
			throw new MwaError(MWARC.INTEGRATION_INVALID_MWARC_FORMAT);
		
		String groupId = getGroupId(code);
		IMWARCFactory factory = factoryMap.get(groupId);
		if (factory != null)
			return factory.create(code);
		else
			return new MWARC_GENERIC(code, code);
	}
}
