package com.sony.pro.mwa.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/11/18
 * Time: 1:22
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "models")
@XmlType(propOrder = {"totalCount", "count", "models", "errors"})
@ApiModel(value="Collection of models", description="Collection of models. This is the model to represent search result.")
public abstract class Collection<T> {
    private Long totalCount;
    private Long count;
    private List<T> models;
//    private List<ErrorModel> errors;

    public Collection() {}

    public Collection(List<T> models) {
        this.models = models;
    }

    public Collection(List<T> models, List<ErrorModel> errors) {
        this.models = models;
//        this.errors = errors;
    }

    public Collection(Long totalCount, Long count, List<T> models) {
        this.totalCount = totalCount;
//        this.count = count;
        this.models = models;
    }

    public Collection(Long totalCount, Long count, List<T> models, List<ErrorModel> errors) {
        this.totalCount = totalCount;
//        this.count = count;
        this.models = models;
//        this.errors = errors;
    }

	@ApiModelProperty(value = "Total count of applicable models as get or search result.")
    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

	@ApiModelProperty(value = "Obtained count of applicable models as get or search result.")
    public Long getCount() {
        return (models != null) ? models.size() : 0L;
    }

    public void setCount(Long count) {
//        this.count = count;
    }

    @ApiModelProperty(hidden=true, value = "Applicable models as get or search result.")
    public List<T> getModels() {
        return models;
    }
    
    public void setModels(List<T> models) {
        this.models = models;
    }

/*    @XmlElementWrapper
    @XmlElement(name="error")
    public List<ErrorModel> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorModel> errors) {
        this.errors = errors;
    }*/

    @XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
    public T first() {
        return (models == null || models.isEmpty()) ? null : models.get(0);
    }
}
