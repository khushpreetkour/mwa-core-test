package com.sony.pro.mwa.service.resource;

public interface IResource {
	public String getId();
	public String getType();
	public String getHolder();
	public String getHolderType();
}
