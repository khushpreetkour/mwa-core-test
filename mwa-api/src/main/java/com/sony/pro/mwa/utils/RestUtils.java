package com.sony.pro.mwa.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.UriUtils;

public class RestUtils {
	private final static MwaLogger logger = MwaLogger.getLogger(RestUtils.class);

	// queryを生成する
	public static String createQuery(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String query = null;
		if (null != sorts) {
			query = createQuery4list(sorts, "sort", query);
		}
		if (null != filters) {
			query = createQuery4list(filters, "filter", query);
		}
		if (null != offset) {
			query = createQuery4String(offset.toString(), "offset", query);
		}
		if (null != limit) {
			query = createQuery4String(limit.toString(), "limit", query);
		}
		return query;
	}

	// Stringのパラメータをqueryに連結する
	public static String createQuery4String(String value, String name, String query) {
		try {
			if (null != query) {
				query += "&" + name + "=" + UriUtils.percentEncode(value);
			} else {
				query = name + "=" + UriUtils.percentEncode(value);
			}
		} catch (UnsupportedEncodingException e) {
			final String errMsg = "Encode failed. value->" + value;
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, MWARC.SYSTEM_ERROR.code(), errMsg);
		}
		return query;
	}

	// Listのパラメータをquery用に連結する
	public static String createQuery4list(List<String> values, String name, String query) {
		for (String value : values) {
			try {
				if (null != query) {
					query += "&" + name + "=" + UriUtils.percentEncode(value);
				} else {
					query = name + "=" + UriUtils.percentEncode(value);
				}
			} catch (UnsupportedEncodingException e) {
				final String errMsg = "Encode failed. value->" + value;
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, MWARC.SYSTEM_ERROR.code(), errMsg);
			}
		}
		return query;
	}

	// javaオブジェクトをJsonに変換する
	public static String convertToJson(Object targetObj) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(targetObj);
		} catch (JsonGenerationException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", "json generation error getPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", "json mapping error getPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", "jason mapping IO error getPersistentDataImpl failed.");
		}
	}

	// Jsonをjavaオブジェクトに変換する
	public static Object convertToObj(String json, Class<?> targetclass) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.readValue(json, targetclass);
		} catch (JsonGenerationException e) {
			logger.error("convertToObj caused exception: " + e, e);
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", "json generation error getPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			logger.error("convertToObj caused exception: " + e, e);
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", "json mapping error getPersistentDataImpl failed.");
		} catch (IOException e) {
			logger.error("convertToObj caused exception: " + e, e);
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", "jason mapping IO error getPersistentDataImpl failed.");
		}
	}

}
