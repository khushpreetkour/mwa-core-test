package com.sony.pro.mwa.model.kbase;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

import java.util.List;

public class ExtensionCollection extends Collection<ExtensionModel> {
    public ExtensionCollection() {
        super();
    }

    public ExtensionCollection(List<ExtensionModel> models) {
        super(models);
    }

    public ExtensionCollection(List<ExtensionModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public ExtensionCollection(Long totalCount, Long count, List<ExtensionModel> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<ExtensionModel> getModels() {
        return super.getModels();
    }
}
