package com.sony.pro.mwa.service.scheduler;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.CronTriggerInputModel;
import com.sony.pro.mwa.model.scheduler.SelectTriggerInfoInputModel;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInputModel;

public interface ISchedulerManager {

	/**
	 * スケジューラを開始します。
	 * 
	 * @return KeyValueModel 処理結果
	 * */
	public KeyValueModel startScheduler();

	/**
	 * スケジューラを終了します。
	 * 
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel shutdownScheduler();

	/**
	 * CronTriggerのスケジュールを登録します。
	 * 
	 * @param cronTriggerInputModel
	 *            CronTrigger登録情報
	 * @return KeyValueModel 登録したスケジュールのID
	 */
	public KeyValueModel registCronScheduleJob(CronTriggerInputModel cronTriggerInputModel);

	/**
	 * SimpleTriggerのスケジュールを登録します。
	 * 
	 * @param simpleTriggerInputModel
	 *            SimpleTrigger登録情報
	 * @return KeyValueModel 登録したスケジュールのID
	 */
	public KeyValueModel registSimpleScheduleJob(SimpleTriggerInputModel simpleTriggerInputModel);

	/**
	 * SimpleTriggerのスケジュールを登録します。starttimeが過去日の場合、即時発火します。
	 * 
	 * @param simpleTriggerInputModel
	 *            SimpleTrigger登録情報
	 *            
	 * @return KeyValueModel 登録したスケジュールのID
	 */
	public KeyValueModel registSimpleScheduleJobStartTimeIsPastTimeSuccess(SimpleTriggerInputModel simpleTriggerInputModel);

	/**
	 * スケジューラ終了を監視するリスナー
	 * @throws MwaInstanceError
	 */
	public void schedulerJobCompletedListener() throws MwaInstanceError;
	
	/**
	 * schedIdと一致するのSimpleTriggerを削除します。
	 * 
	 * @param schedId
	 *            スケジュールID
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel deleteSimpleTrigger(String schedId);

	/**
	 * schedIdと一致するCronTriggerを削除します。
	 * 
	 * @param schedId
	 *            スケジュールID
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel deleteCronTrigger(String schedId);

	/**
	 * SimpleTriggerを全て削除します。
	 * 
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel deleteSimpleTrigger();

	/**
	 * CronTriggerを全て削除します。
	 * 
	 * @return ｃ
	 */
	public KeyValueModel deleteCronTrigger();

	/**
	 * schedIdと一致するSimpleTriggerを一時停止します。
	 * 
	 * @param schedId
	 *            スケジュールID
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel pauseSimpleTrigger(String schedId);

	/**
	 * schedIdと一致するSimpleTriggerを再開します。
	 * 
	 * @param schedId
	 *            スケジュールID
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel resumeSimpleTrigger(String schedId);

	/**
	 * schedIdと一致するCronTriggerを一時停止します。
	 * 
	 * @param schedId
	 *            スケジュールID
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel pauseCronTrigger(String schedId);

	/**
	 * schedIdと一致するCronTriggerを再開します。
	 * 
	 * @param schedId
	 *            スケジュールID
	 * @return KeyValueModel 処理結果
	 */
	public KeyValueModel resumeCronTrigger(String schedId);

	/**
	 * selectTriggerInfoInputModelの情報を元にCronTrigger情報を検索し、結果を返します。
	 * selectTriggerInfoInputModelの情報がない場合、CronTriggerの全て検索し、結果を返します。
	 * 
	 * @param selectTriggerInfoInputModel
	 *            検索条件(スケジュールID)
	 * @return CronTriggerInfoCollection 検索結果
	 */
	public CronTriggerInfoCollection selectCronTriggerInfo(SelectTriggerInfoInputModel selectTriggerInfoInputModel);

	/**
	 * selectTriggerInfoInputModelの情報を元にSimpleTrigger情報を検索し、結果を返します。
	 * selectTriggerInfoInputModelの情報がない場合、SimpleTriggerの全て検索し、結果を返します。
	 * 
	 * @param selectTriggerInfoInputModel
	 *            検索条件(スケジュールID)
	 * @return SimpleTriggerInfoCollection 検索結果
	 */
	public SimpleTriggerInfoCollection selectSimpleTriggerInfo(SelectTriggerInfoInputModel selectTriggerInfoInputModel);
	
}
