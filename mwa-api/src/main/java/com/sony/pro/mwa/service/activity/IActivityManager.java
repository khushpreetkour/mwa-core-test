package com.sony.pro.mwa.service.activity;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.OperationResult;

public interface IActivityManager {
	public OperationResult createInstance(String templateId, Map<String, Object> params);
	public OperationResult createInstance(String templateName, String templateVersion, Map<String, Object> params);
	public OperationResult operateActivity(String instanceId, String operation, Map<String, Object> params);
	public OperationResult operateActivity(String parentId, String childName, String operation, Map<String, Object> params);
	public ActivityInstanceModel getInstance(String instanceId);
	public ActivityInstanceCollection getInstances(List<String> sortKey, List<String> filters, Integer offset, Integer limit);
	public ActivityInstanceModel getChildInstance(String parentInstanceId, String childName);
	public ActivityInstanceCollection  getChildInstances(String parentInstanceId);
    public OperationResult updateInstance(ActivityInstanceModel model);
}
