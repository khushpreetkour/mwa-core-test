package com.sony.pro.mwa.model.kbase;

public class ExtensionContentModel {
	String extensionId;
	String extensionContentType;
	String extensionContentId;

	public String getExtensionId() {
		return extensionId;
	}
	public ExtensionContentModel setExtensionId(String extensionId) {
		this.extensionId = extensionId;
		return this;
	}
	public String getExtensionContentType() {
		return extensionContentType;
	}
	public ExtensionContentModel setExtensionContentType(String extensionContentType) {
		this.extensionContentType = extensionContentType;
		return this;
	}
	public String getExtensionContentId() {
		return extensionContentId;
	}
	public ExtensionContentModel setExtensionContentId(String extensionContentId) {
		this.extensionContentId = extensionContentId;
		return this;
	}
}
