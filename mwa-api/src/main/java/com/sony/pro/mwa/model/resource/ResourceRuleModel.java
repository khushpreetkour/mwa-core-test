package com.sony.pro.mwa.model.resource;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ResourceRuleModel {
	String templateName;
	String templateVersion;
	Map<String, Object> parameters;
	ResourceRequestModel request;

	public Map<String, Object> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	public ResourceRequestModel getResourceRequest() {
		return request;
	}
	public void setResourceRequest(ResourceRequestModel request) {
		this.request = request;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTemplateVersion() {
		return templateVersion;
	}
	public void setTemplateVersion(String templateVersion) {
		this.templateVersion = templateVersion;
	}
}
