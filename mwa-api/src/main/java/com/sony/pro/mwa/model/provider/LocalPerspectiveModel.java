package com.sony.pro.mwa.model.provider;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(
	value="Local perspective model", 
	description="\"Perspective\" is to resolve MWA URI into actual path format. "
			+ "But some provider needs to spetial logic to resolve it. Local perspective is for unique path resolver based on provider.")
public class LocalPerspectiveModel {
	protected String activityProviderId;
	protected String locationName;	//ID?? or Name??
	protected String perspective;
	protected String localBasePath;
	protected String user;
	protected String password;

	@XmlTransient @JsonIgnore
	public String getActivityProviderId() {
		return activityProviderId;
	}
	public void setActivityProviderId(String activityProviderId) {
		this.activityProviderId = activityProviderId;
	}
	
	@ApiModelProperty(value = "Location name for local perspective convertion.")	
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	@ApiModelProperty(value = "Perspective name for local perspective convertion.")	
	public String getPerspective() {
		return perspective;
	}
	public void setPerspective(String perspective) {
		this.perspective = perspective;
	}
	@ApiModelProperty(value = "Local base path. It is used for local perspective convertion instead of general base path.")	
	public String getLocalBasePath() {
		return localBasePath;
	}
	public void setLocalBasePath(String localBasePath) {
		this.localBasePath = localBasePath;
	}
	@ApiModelProperty(value = "User name for local perspective.")	//localPerspectiveはpath変換でしか使わないので現状あまり意味ない
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	@ApiModelProperty(value = "Password for local perspective")	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
