package com.sony.pro.mwa.model.storage;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

import java.util.List;

public class DirectoryCollection extends Collection<DirectoryModel> {

    public DirectoryCollection() {
        super();
    }

    public DirectoryCollection(List<DirectoryModel> models) {
        super(models);
    }

    public DirectoryCollection(List<DirectoryModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public DirectoryCollection(Long totalCount, Long count, List<DirectoryModel> models) {
        super(totalCount, count, models);
    }

    @Override
    public List<DirectoryModel> getModels() {
        return super.getModels();
    }
}
