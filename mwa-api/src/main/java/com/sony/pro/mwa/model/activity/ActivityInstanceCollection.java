package com.sony.pro.mwa.model.activity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value="Collection of activity instance models", description="Activity instance is a execution unit of activity, this model represents one instance of it.")
public class ActivityInstanceCollection extends Collection<ActivityInstanceModel> {
    public ActivityInstanceCollection() {
        super();
    }

    public ActivityInstanceCollection(List<ActivityInstanceModel> models) {
        super(models);
    }

    public ActivityInstanceCollection(List<ActivityInstanceModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public ActivityInstanceCollection(Long totalCount, Long count, List<ActivityInstanceModel> models) {
        super(totalCount, count, models);
    }

    @Override
	@ApiModelProperty(value = "Obtained models as get or search result.")
    public List<ActivityInstanceModel> getModels() {
        return super.getModels();
    }
}
