package com.sony.pro.mwa.rc;

import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wordnik.swagger.annotations.ApiModel;

@ApiModel(value="MWARC (= MWA return code)", description="format=MWARC.XXXXYYYY", subTypes={MWARC.class, MWARC_HTTP.class})
public interface IMWARC {
	
	@JsonProperty("code") @XmlAttribute(name="code")
	//@JsonProperty("code") @XmlAttribute(name="code") v3のI/Fに向けてこれを削除したい
	public String code();
	public String name();
	public boolean isSuccess();
	
	public static String PREFIX = "MWARC";
	public static String DELIMITER = ".";
	public static String PATTERN = PREFIX + DELIMITER + "[0-9a-fA-F]{8}";

}
