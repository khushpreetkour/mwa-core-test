package com.sony.pro.mwa.parameter.type;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeBase;
import com.sony.pro.mwa.rc.MWARC;


/**
 * This parameter type can be used with any Class that is is compatible with JAXB XML encoding.
 *
 */
public class ObjectParameterType<T> extends TypeBase<T> {

    private Class<T> type;
    private JAXBContext context;    
  
    /**
     * Construct an ObjectParameterType for the specified class. 
     * It is relatively very slow to create the JAXB context so only do this once for a given type.
     *  
     */
    public ObjectParameterType(Class<T> type)  {        
        this.type = type;
        try {                
            context = JAXBContext.newInstance(type);                      
        } catch (Exception e) {
            throw new IllegalStateException("Failed to create JAXB context for " + type);
        }
    }
    
    

    @Override
    public boolean isLinkable(IParameterType parameterType) { 
        return parameterType instanceof ObjectParameterType 
                && type.equals(((ObjectParameterType<?>)parameterType).type);
    }

    @Override
    protected String toStringImpl(T value) {        
        try {
            StringWriter sw = new StringWriter();
            Marshaller marshaller = context.createMarshaller();            
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
            marshaller.marshal(value, sw);
            return sw.toString();
        } catch (Exception e) {            
            throw new MwaInstanceError(MWARC.INVALID_INPUT, "", "Failed to marshal " + type + ": " + e);
        }
    }

    @Override
    protected synchronized T fromStringImpl(String valueStr) {
        try {            
            StringReader sr = new StringReader(valueStr);            
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return type.cast(unmarshaller.unmarshal(sr));            
        } catch (Exception e) {
            throw new MwaInstanceError(MWARC.INVALID_INPUT, "", "Failed to unmarshal " + type + ": " + e);
        }
    }
  
}
