package com.sony.pro.mwa.model.storage;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class LocationCollection extends Collection<LocationModel> {

	public LocationCollection() {
		super();
	}

	public LocationCollection(List<LocationModel> models) {
		super(models);
	}

	public LocationCollection(List<LocationModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public LocationCollection(Long totalCount, Long count, List<LocationModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<LocationModel> getModels() {
		return super.getModels();
	}
}
