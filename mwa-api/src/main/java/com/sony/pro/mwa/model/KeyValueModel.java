package com.sony.pro.mwa.model;

import com.wordnik.swagger.annotations.ApiModelProperty;

public class KeyValueModel {
	String key;
	String value;
	
	public KeyValueModel(){}
	public KeyValueModel(Enum key, Object value) {
		this(key.name(), value);
	}
	public KeyValueModel(String key, Object value){
		this.key = key;
		this.value = (value != null) ? value.toString() : null;
	}

	@ApiModelProperty(value = "key string")
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	@ApiModelProperty(value = "value string. Structured data should be expressed as XML format in this field and the rule should be shared with .")
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
