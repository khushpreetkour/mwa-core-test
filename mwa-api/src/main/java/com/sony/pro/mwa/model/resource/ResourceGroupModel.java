package com.sony.pro.mwa.model.resource;

import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.enumeration.ResourceGroupType;
import com.sony.pro.mwa.service.resource.IResource;
import com.wordnik.swagger.annotations.ApiModelProperty;

public class ResourceGroupModel implements IResource {
	String id;
	String name;
	String type = ResourceGroupType.GROUP.name();
	String holder;
	String holderType;
	List<String> subTypes;
	Long createTime;
	Long updateTime;
	List<String> resourceIds;
	
	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public ResourceGroupModel add(ResourceModel resource) {
		this.resourceIds.add(resource.getId());
		return this;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlTransient @JsonIgnore
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
	public List<String> getResourceIds() {
		return resourceIds;
	}
	public void setResourceIds(List<String> resourceIds) {
		this.resourceIds = resourceIds;
	}
	public List<String> getSubTypes() {
		return subTypes;
	}

	public void setSubTypes(List<String> subTypes) {
		this.subTypes = subTypes;
	}

	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public String getHolderType() {
		return holderType;
	}
	public void setHolderType(String holderType) {
		this.holderType = holderType;
	}
}
