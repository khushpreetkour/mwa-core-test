package com.sony.pro.mwa.model.scheduler;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * CronTrigger登録のInput情報の格納Beanクラス
 */
@ApiModel(value = "CronTrigger Input model", description = "")
public class CronTriggerInputModel {

	private String httpMethodType;
	private String endpoint;
	private String parameter;
	private String description;
	// private String tag;
	private String cronExpression;
	private String timeZone;
	private Long startTime;
	private Long endTime;

	@ApiModelProperty(value = "HttpMethodType of CronTrigger")
	public String getHttpMethodType()
	{
		return httpMethodType;
	}

	public void setHttpMethodType(String httpMethodType)
	{
		this.httpMethodType = httpMethodType;
	}

	@ApiModelProperty(value = "Endpoint of CronTrigger")
	public String getEndpoint()
	{
		return endpoint;
	}

	public void setEndpoint(String endpoint)
	{
		this.endpoint = endpoint;
	}

	@ApiModelProperty(value = "Parameter of CronTrigger")
	public String getParameter()
	{
		return parameter;
	}

	public void setParameter(String parameter)
	{
		this.parameter = parameter;
	}

	@ApiModelProperty(value = "Description of CronTrigger")
	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	@ApiModelProperty(value = "CronExpression of CronTrigger")
	public String getCronExpression()
	{
		return cronExpression;
	}

	public void setCronExpression(String cronExpression)
	{
		this.cronExpression = cronExpression;
	}

	@ApiModelProperty(value = "TimeZone of CronTrigger")
	public String getTimeZone()
	{
		return timeZone;
	}

	public void setTimeZone(String timeZone)
	{
		this.timeZone = timeZone;
	}

	@ApiModelProperty(value = "StartTime of CronTrigger")
	public Long getStartTime()
	{
		return startTime;
	}

	public void setStartTime(Long startTime)
	{
		this.startTime = startTime;
	}

	@ApiModelProperty(value = "EndTime of CronTrigger")
	public Long getEndTime()
	{
		return endTime;
	}

	public void setEndTime(Long endTime)
	{
		this.endTime = endTime;
	}

}
