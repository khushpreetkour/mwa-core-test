package com.sony.pro.mwa.parameter;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IParameterType {
	public String name();
	
	//public boolean isValid(Object value);
	public boolean isLinkable(IParameterType type);
	public String toString(Object value);
	public Object fromString(String valueStr);
}
