package com.sony.pro.mwa.parameter;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;


public interface IParameterDefinition {
	
	public boolean getRequired();
	public String getKey();
	public String getTypeName();

	@XmlTransient @JsonIgnore
	public IParameterType getType();

	public default boolean getVisibleFlag() {
		return true;
	}
}
