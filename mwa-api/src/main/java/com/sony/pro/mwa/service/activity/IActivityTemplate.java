package com.sony.pro.mwa.service.activity;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.IMWARC;

public interface IActivityTemplate {
	public IMwaActivity createInstance();
	public IMWARC verify(Map<String, Object> params);

	public Boolean getSyncFlag();

	public String getId();
	public ActivityTemplateModel setId(String id);
	
	public String getName();
	public ActivityTemplateModel setName(String name);
	
	public String getVersion();
	public ActivityTemplateModel setVersion(String version);
	
	
	public ActivityType getType();
	public ActivityTemplateModel setType(ActivityType type);
	
	public String getAlias();
	public ActivityTemplateModel setAlias(String alias);

	@XmlTransient @JsonIgnore
	public String getExtensionId();
	public ActivityTemplateModel setExtensionId(String extensionId);
	@XmlTransient @JsonIgnore
	public String getExtensionName();
	public ActivityTemplateModel setExtensionName(String extensionName);

	public List<? extends IParameterDefinition> getInputs();
	public void setInputs(List<IParameterDefinition> inputs);
	public List<? extends IParameterDefinition> getOutputs();
	public void setOutputs(List<IParameterDefinition> outputs);
}
