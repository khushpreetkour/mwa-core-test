package com.sony.pro.mwa.enumeration;

public enum ExtensionContentType {
	ACTIVITY_TEMPLATE,
	ACTIVITY_PROVIDER_TYPE,
	PARAMETER_TYPE,
	;

	public boolean equals(String val) {
		return this.name().equals(val);
	}
}
