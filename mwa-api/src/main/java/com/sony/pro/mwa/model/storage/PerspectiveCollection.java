package com.sony.pro.mwa.model.storage;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class PerspectiveCollection extends Collection<PerspectiveModel> {

	public PerspectiveCollection() {
		super();
	}

	public PerspectiveCollection(List<PerspectiveModel> models) {
		super(models);
	}

	public PerspectiveCollection(List<PerspectiveModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public PerspectiveCollection(Long totalCount, Long count, List<PerspectiveModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<PerspectiveModel> getModels() {
		return super.getModels();
	}
}
