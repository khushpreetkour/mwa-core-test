package com.sony.pro.mwa.parameter;


public class ParameterDefinition implements IParameterDefinition {	//これ自体はDataPortでなく定義。Workflow上などにおいて初めてDataPortになります。なのでこれをつなぐことはできません
	protected String key;	//step上のKeyはStep名を含めたものになるよ
	protected IParameterType type;
	protected boolean required;
	protected String description;	//パラメータの説明（多言語やるか？）
	protected String parentKey;
	protected Object parentValue;
	protected boolean visibleFlag = true;
	protected boolean listFlag = false;

	public ParameterDefinition() {
	}
	
	public ParameterDefinition(String key, IParameterType type, boolean required) {
		this.type = type;
		this.key = key;
		this.required = required;
	}

	public ParameterDefinition(String key, IParameterType type, boolean required, String description) {
		this.type = type;
		this.key = key;
		this.required = required;
		this.description = description;
	}
	public ParameterDefinition(String key, IParameterType type, boolean required, boolean visibleFlag) {
		this.type = type;
		this.key = key;
		this.required = required;
		this.visibleFlag = visibleFlag;
	}

	public ParameterDefinition(IParameterDefinition source) {
		this.type = source.getType();
		this.required = source.getRequired();
		this.key = source.getKey();
	}
	
	@Override
	public boolean getRequired() {
		return required;
	}
	public ParameterDefinition setRequired(boolean optional) {
		this.required = optional;
		return this;
	}
	@Override
	public String getKey() {
		return key;
	}
	public ParameterDefinition setKey(String key) {
		this.key = key;
		return this;
	}
	@Override
	public IParameterType getType() {
		return type;
	}
	public ParameterDefinition setType(IParameterType type) {
		this.type = type;
		return this;
	}

	public String getParentKey() {
		return parentKey;
	}
	public ParameterDefinition setParentKey(String parentKey) {
		this.parentKey = parentKey;
		return this;
	}

	public Object getParentValue() {
		return parentValue;
	}
	public ParameterDefinition setParentValue(Object parentValue) {
		this.parentValue = parentValue;
		return this;
	}

	public boolean getVisibleFlag() {
		return visibleFlag;
	}
	public ParameterDefinition setVisibleFlag(boolean visibleFlag) {
		this.visibleFlag = visibleFlag;
		return this;
	}

	public boolean getListFlag() {
		return listFlag;
	}
	public ParameterDefinition setListFlag(boolean listFlag) {
		this.listFlag = listFlag;
		return this;
	}

	@Override
	public String getTypeName() {
		return this.getType().name();
	}
}
