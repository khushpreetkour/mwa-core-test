package com.sony.pro.mwa.model.kbase.tempmodels;

public class ConditionModel {
	/**
	 * 条件と条件をつなぐ演算子
	 */
	private String conditionOperator;
	/**
	 * 比較対象の変数名
	 */
	private String leftValue;
	/**
	 * 演算子
	 */
	private String comparisonOperator;
	/**
	 * 比較するための値
	 */
	private String rightValue;

	public String getConditionOperator() {
		return conditionOperator;
	}
	public void setConditionOperator(String conditionOperator) {
		this.conditionOperator = conditionOperator;
	}
	public String getLeftValue() {
		return leftValue;
	}
	public void setLeftValue(String leftValue) {
		this.leftValue = leftValue;
	}
	public String getComparisonOperator() {
		return comparisonOperator;
	}
	public void setComparisonOperator(String comparisonOperator) {
		this.comparisonOperator = comparisonOperator;
	}
	public String getRightValue() {
		return rightValue;
	}
	public void setRightValue(String rightValue) {
		this.rightValue = rightValue;
	}
}
