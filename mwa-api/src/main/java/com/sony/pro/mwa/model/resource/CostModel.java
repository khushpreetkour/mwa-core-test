package com.sony.pro.mwa.model.resource;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

//Longに固定しておきます。Genericsのままだと四則演算が使えなかったり、そもそも使う側が面倒なので
//public class CostUnitModel<T extends Number> extends Number implements Map.Entry<IResourceType, T> {
@XmlRootElement
public class CostModel extends CostBase<CostModel> {
	public CostModel(){
		super();
	}
	public CostModel(String type) {
		super();
		this.type = type;
	}
	public CostModel(String type, Map<String, Long> values) {
		super();
		this.type = type;
		this.values = values;
	}
	public CostModel(CostModel cost) {
		super(cost);
	}
}
