package com.sony.pro.mwa.model.kbase;

import java.util.ArrayList;
import java.util.List;

public class ActivityProfileGroupDetailModel extends ActivityProfileGroupModel {
	private List<ActivityProfileModel> profiles = new ArrayList<>();

	public ActivityProfileGroupDetailModel(ActivityProfileGroupModel model) {
		this.setId(model.getId());
		this.setTemplateId(model.getTemplateId());
		this.setName(model.getName());
		this.setIcon(model.getIcon());
		this.setCreateTime(model.getCreateTime());
		this.setUpdateTime(model.getUpdateTime());
	}

	public void setProfiles(List<ActivityProfileModel> profiles) {
		this.profiles = profiles;
	}

	public List<ActivityProfileModel> getProfiles() {
		return profiles;
	}
}
