package com.sony.pro.mwa.parameter;

import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;

public class OperationResultWithStatus extends OperationResult {

	public static OperationResultWithStatus newInstance(IEvent event, Integer progress) {
		OperationResultWithStatus result = new OperationResultWithStatus(event, progress);
		return result;
	}
	
	protected OperationResultWithStatus(IMWARC rc) {
		super(rc);
	}
	protected OperationResultWithStatus(IEvent event, Integer progress) {
		super(MWARC.SUCCESS);
		if (event != null) 
			this.put(PresetParameter.Event, event);
		if (progress != null)
			this.put(PresetParameter.Progress, progress);
	}

	public IEvent getEvent() {
		return (IEvent)this.get(PresetParameter.Event);
	}
	
	public Integer getProgress() {
		return (Integer)this.get(PresetParameter.Progress);
	}
}
