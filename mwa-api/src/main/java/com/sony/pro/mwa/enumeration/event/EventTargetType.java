package com.sony.pro.mwa.enumeration.event;

public enum EventTargetType {
	ACTIVITY_INSTANCE,
	QUEUE,
	RESOURCE,
	WORKFLOW,
	;
	
	public boolean equals(String targetType) {
		return this.name().equals(targetType);
	}
}
