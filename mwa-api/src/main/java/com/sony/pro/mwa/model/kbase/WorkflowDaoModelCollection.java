package com.sony.pro.mwa.model.kbase;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class WorkflowDaoModelCollection extends Collection<WorkflowDaoModel> {

	public WorkflowDaoModelCollection() {
		super();
	}

	public WorkflowDaoModelCollection(List<WorkflowDaoModel> models) {
		super(models);
	}

	public WorkflowDaoModelCollection(List<WorkflowDaoModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public WorkflowDaoModelCollection(Long totalCount, Long count, List<WorkflowDaoModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<WorkflowDaoModel> getModels() {
		return super.getModels();
	}

}
