package com.sony.pro.mwa.enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/11/05
 * Time: 13:28
 * To change this template use File | Settings | File Templates.
 */
public enum FilterOperatorEnum {
    EQUAL("=="),
    NOT_EQUAL("!="),
    GREATER_THAN(">"),
    LESS_THAN("<"),
    GREATER_THAN_OR_EQUAL(">="),
    LESS_THAN_OR_EQUAL("<="),
    PARTIAL_MATCHING("=@"),
    PARTIAL_UNMATCHING("!@"),
    UNKNOWN(""),
    ;

    private String symbol;

    private FilterOperatorEnum(String symbol) {
        this.symbol = symbol;
    }

    public String toSymbol() {
        return symbol;
    }
}
