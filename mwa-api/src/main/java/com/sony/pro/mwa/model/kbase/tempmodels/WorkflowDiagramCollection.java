package com.sony.pro.mwa.model.kbase.tempmodels;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkflowDiagramCollection extends Collection<WorkflowDiagramModel> {
	public WorkflowDiagramCollection() {
		super();
	}

	public WorkflowDiagramCollection(List<WorkflowDiagramModel> models) {
		super(models);
	}

	public WorkflowDiagramCollection(List<WorkflowDiagramModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public WorkflowDiagramCollection(Long totalCount, Long count, List<WorkflowDiagramModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<WorkflowDiagramModel> getModels() {
		return super.getModels();
	}
}
