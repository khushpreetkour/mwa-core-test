package com.sony.pro.mwa.model.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class NativeLocationModel {
	List<String> paths;
	String user;
	String password;
	Map<String, String> properties;

	public NativeLocationModel() {
		paths = new ArrayList<>();
		properties = new HashMap<>();
	}
	
	public List<String> getPaths() {
		return paths;
	}
	public NativeLocationModel setPaths(List<String> paths) {
		this.paths = paths;
		return this;
	}
	@JsonIgnore @XmlTransient
	public String getFirstPath() {
		if (this.paths != null && !this.paths.isEmpty()) 
			return paths.get(0);
		return null;
	}
	
	public String getUser() {
		return user;
	}
	public NativeLocationModel setUser(String user) {
		this.user = user;
		return this;
	}
	public String getPassword() {
		return password;
	}
	public NativeLocationModel setPassword(String password) {
		this.password = password;
		return this;
	}
	public Map<String, String> getProperties() {
		return properties;
	}
	public NativeLocationModel setProperties(Map<String, String> properties) {
		this.properties = properties;
		return this;
	}
}
