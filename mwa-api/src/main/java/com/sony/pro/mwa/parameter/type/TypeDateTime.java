package com.sony.pro.mwa.parameter.type;

import com.sony.pro.mwa.parameter.IParameterType;

public class TypeDateTime extends TypeLong {

	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeDateTime;
	}
	
	public static TypeDateTime instance() {
		return instance;
	}
	static TypeDateTime instance = new TypeDateTime();
}
