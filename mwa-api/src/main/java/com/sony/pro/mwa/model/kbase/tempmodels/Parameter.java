package com.sony.pro.mwa.model.kbase.tempmodels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

// GUIからnullで送られると、ObjectMapperでKeyが落ちるため、コメントアウトしてあります。
// @JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Parameter {
	private String key;
	private String typeName;
	private String value;
	private Boolean required;
	private String parentKey;
	private Object parentValue;
	private boolean visibleFlag = true;
	private boolean listFlag = false;

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Boolean getRequired() {
		return required;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	public String getParentKey() {
		return parentKey;
	}
	public void setParentKey(String parentKey) {
		this.parentKey = parentKey;
	}
	public Object getParentValue() {
		return parentValue;
	}
	public void setParentValue(Object parentValue) {
		this.parentValue = parentValue;
	}
	public boolean getVisibleFlag() {
		return visibleFlag;
	}
	public void setVisibleFlag(boolean visibleFlag) {
		this.visibleFlag = visibleFlag;
	}
	public boolean getListFlag() {
		return listFlag;
	}
	public void setListFlag(boolean listFlag) {
		this.listFlag = listFlag;
	}
}
