package com.sony.pro.mwa.model.kbase.tempmodels.constants;

/**
 * EditorModelでのPortタイプの種別を列挙したクラス
 */
public enum PortType {
	CONTROL_INPUT,
	CONTROL_OUTPUT,
	DATA_INPUT,
	DATA_OUTPUT;
}
