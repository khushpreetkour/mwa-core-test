package com.sony.pro.mwa.model.kbase;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class WorkflowViewModelCollection extends Collection<WorkflowViewModel> {

	public WorkflowViewModelCollection() {
		super();
	}

	public WorkflowViewModelCollection(List<WorkflowViewModel> models) {
		super(models);
	}

	public WorkflowViewModelCollection(List<WorkflowViewModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public WorkflowViewModelCollection(Long totalCount, Long count, List<WorkflowViewModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<WorkflowViewModel> getModels() {
		return super.getModels();
	}

}
