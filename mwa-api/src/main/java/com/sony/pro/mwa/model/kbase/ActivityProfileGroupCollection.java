package com.sony.pro.mwa.model.kbase;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ActivityProfileGroupCollection extends Collection<ActivityProfileGroupModel> {
	public ActivityProfileGroupCollection() {
		super();
	}

	public ActivityProfileGroupCollection(List<ActivityProfileGroupModel> models) {
		super(models);
	}

	public ActivityProfileGroupCollection(List<ActivityProfileGroupModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public ActivityProfileGroupCollection(Long totalCount, Long count, List<ActivityProfileGroupModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ActivityProfileGroupModel> getModels() {
		return super.getModels();
	}
}
