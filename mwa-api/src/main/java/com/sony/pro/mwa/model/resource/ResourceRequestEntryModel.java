package com.sony.pro.mwa.model.resource;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement
public class ResourceRequestEntryModel {
	String resourceRequestId;
	String resourceId;
	String resourceHolder;
	CostModel cost;
	String assignedResourceId;
	Long updateTime;

	public CostModel getCost() {
		return cost;
	}
	public void setCost(CostModel cost) {
		this.cost = cost;
	}
	public String getAssignedResourceId() {
		return assignedResourceId;
	}
	public void setAssignedResourceId(String assignedResourceId) {
		this.assignedResourceId = assignedResourceId;
	}
	public String getResourceRequestId() {
		return resourceRequestId;
	}
	public void setResourceRequestId(String resourceRequestId) {
		this.resourceRequestId = resourceRequestId;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
	public String getResourceHolder() {
		return resourceHolder;
	}
	public void setResourceHolder(String resourceHolder) {
		this.resourceHolder = resourceHolder;
	}
	
	public String toString() {
		return "{\"resourceHolder\": \"" + resourceHolder + "\", "
				+ "\"assignedResourceId\": \"" + assignedResourceId + "\", "
				+ "\"cost\": " + cost
				+ "}";
	}
}
