package com.sony.pro.mwa.enumeration.resource;

import com.sony.pro.mwa.service.activity.IEvent;

public enum ResourceEvent implements IEvent {
	NOTIFY_RESOURCE_ADDITION,
	NOTIFY_RESOURCE_UPDATE,
	NOTIFY_RESOURCE_DELETION,
	NOTIFY_RESOURCE_REQUEST_ADDITION,
	NOTIFY_RESOURCE_REQUEST_UPDATE,
	NOTIFY_RESOURCE_REQUEST_DELETION,
	NOTIFY_RESERVED,
	;

	public boolean isMatch(String eventName) { return this.name().equals(eventName); }
	public boolean isRequestEvent() { return false; }
	public boolean isNotifyEvent() { return true; }
	public boolean isConfirmEvent() { return false; }

	@Override
	public String getName() {
		return super.name();
	}
}
