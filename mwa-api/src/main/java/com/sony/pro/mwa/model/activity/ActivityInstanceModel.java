package com.sony.pro.mwa.model.activity;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.service.activity.StatusDetails;
import com.sony.pro.mwa.utils.Converter;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value="Activity instance model", description="Activity instance is a execution unit of activity, this model represents one instance of it.")
public class ActivityInstanceModel extends ActivityInstanceSimpleModel {

	StatusDetails statusDetails;	//MWARC
	Map<String, Object> inputDetails;
	Map<String, Object> outputDetails;
    String templateId;
	String persistentData;
	boolean volatileFlag;
	Map<String, Object> properties;
//	Integer retryCount;

	@ApiModelProperty(dataType = "UUID", value = "ID of the activity template")
	public String getTemplateId() { return templateId; }
	public void setTemplateId(String templateId) { this.templateId = templateId; }

	@ApiModelProperty(value = "Status details of the activity instance")
	public StatusDetails getStatusDetails() { return this.statusDetails; }
	public void setStatusDetails(StatusDetails statusDetails) { this.statusDetails = statusDetails; }

	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public String getPersistentData() { return persistentData; }
	public void setPersistentData(String persistentData) { this.persistentData = persistentData; }

	@ApiModelProperty(value = "Input parameters of the activity instance.")
	public List<KeyValueModel> getInputDetailsList() {
		return Converter.mapToKeyValueList(inputDetails);
	}
	public void setInputDetailList(List<KeyValueModel> list) {
		this.inputDetails = Converter.keyValueListToMap(list);
	}

	@ApiModelProperty(value = "Output parameters of the activity instance.")
	public List<KeyValueModel> getOutputDetailList() {
		return Converter.mapToKeyValueList(outputDetails);
	}
	public void setOutputDetailList(List<KeyValueModel> list) {
		this.outputDetails = Converter.keyValueListToMap(list);
	}

	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public Map<String, Object> getInputDetails() { return this.inputDetails; }
	public void setInputDetails(Map<String, Object> map) { this.inputDetails = map; }

	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public Map<String, Object> getOutputDetails() { return this.outputDetails; }
	public void setOutputDetails(Map<String, Object> map) { this.outputDetails = map; }

	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public boolean getVolatileFlag() {return this.volatileFlag; }
	public void setVolatileFlag(boolean volatileFlag) { this.volatileFlag = volatileFlag; }

	@ApiModelProperty(position=11, required=false,
			value = "Property of the activity instance.")
	public Map<String, Object> getProperties() { return this.properties; }
	public void setProperties(Map<String, Object> properties) { this.properties = properties; }
}
