package com.sony.pro.mwa.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sony.pro.mwa.enumeration.StandardPerspective;
import com.sony.pro.mwa.enumeration.StandardScheme;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.storage.IPerspective;
import com.sony.pro.mwa.model.UriModel;

public class UriUtils {
	private static final Pattern SEQ_FRAME_PATTERN = Pattern.compile("(\\d+)");
	
	public static IPerspective checkPerspective(String path) {
		return StandardPerspective.checkPerspective(path);
	}
	
	private static String normalizeBasePath(String path) {
		path = toFowardSlash(path);
		path = removeEndSlash(path);
		return path;
	}
	private static String normalizePath(String path) {
		path = toFowardSlash(path);
		return path;
	}
	private static String removeEndSlash(String path) {
		return  path.endsWith("/") ? path.substring(0,path.length()-1) : path;
	}
	public static String toBackSlash(String path) {
		return (path == null) ? path : path.replace("/", "\\"); 
	}
	public static String toFowardSlash(String path) {
		return (path == null) ? path : path.replace("\\", "/"); 
	}
	private static String null2blank(String v) {
		if (v == null) return "";
		return v;
	}

	public static String percentEncode(String s)  throws UnsupportedEncodingException {
		return URLEncoder.encode(null2blank(s), "UTF-8").replace("+", "%20");
	}
	public static String percentDecode(String s)  throws UnsupportedEncodingException {
		return URLDecoder.decode(null2blank(s), "UTF-8");
	}
	
	protected static String generateUri(String scheme, String location, String relativePath) {
		StringBuilder sb = new StringBuilder();
		sb.append(scheme + "://");
		try { 
			sb.append(percentEncode(location));
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
		}
		if (relativePath != "") {
			try { 
			sb.append(percentEncode(relativePath).replace("%2F", "/"));
			} catch (Exception e) {
				throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
			}
		}
		return sb.toString();
	}
	
	public static String native2mwa(String locationName, String nativeFullPath, String nativeBasePath) {
		String relativePath = "";
		
		if (locationName == null || locationName.equals("") || nativeBasePath == null) {
			throw new MwaInstanceError(MWARC.CONFIGURATION_LOCATION_NOT_FOUND); 
		}
		
		if (nativeBasePath == null || nativeFullPath == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT); 
		}
		
		String normBasePath = normalizeBasePath(nativeBasePath);
		String normPath = normalizePath(nativeFullPath);
		
		int bLen = normBasePath.length();
		int pLen = normPath.length();
		if (pLen >= bLen && bLen != -1) {
			relativePath = normPath.substring(bLen, pLen);
		} else {
			//"Input path or base path is not correct: " + "native=" + nativeFullPath + ", base = "+ nativeBasePath
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}
		
		// generate MWA URI
		return generateUri(StandardScheme.mwa.name(), locationName, relativePath);
	}
	
	public static String mwa2native(String mwaUri, String nativeBasePath) {
	
		UriModel uc = parse(mwaUri);
		String mwaRelativePath = (uc != null) ? uc.getRelativePath() : null;
		
		// null check
		if (nativeBasePath == null) {
			//"native base path is null"
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}
		if (mwaRelativePath == null) 
			mwaRelativePath = "";

		// validate format
		nativeBasePath = nativeBasePath.trim();
		IPerspective nativePathFormat = checkPerspective(nativeBasePath);	//URLEncodeかかってる可能性あり
		if (nativePathFormat == null) {
			//"Invalid Path Format : " + nativeBasePath
			throw new MwaInstanceError(MWARC.INVALID_INPUT_FORMAT);
		}
		
		if (!nativePathFormat.isUri()) {
			try {
				mwaRelativePath = percentDecode(mwaRelativePath);
			} catch (UnsupportedEncodingException e) {
			}
		}

		nativeBasePath = normalizeBasePath(nativeBasePath);
		String convPath = nativeBasePath + mwaRelativePath;
		if (nativePathFormat.isBackSlash()) {
			convPath = toBackSlash(convPath);
		}
		
		return null2blank(convPath);
	}
	
	public static UriModel parse(String mwaUri) {
		UriModel result = null;
		
		// null check
		if (mwaUri == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}
		
		// validate format
		Matcher m = StandardPerspective.MWA.getPattern().matcher(mwaUri);
		if (m.matches()) {
			try {
				//★queryの保持の仕方やdecodeのタイミングは要検討
				result = new UriModel()
					.setLocation(percentDecode(m.group(2)))
					.setRelativePath(m.group(3))	//decode不要
					.setQuery(m.group(4));	//decode不要
			} catch (UnsupportedEncodingException e) {
				throw new MwaInstanceError(MWARC.INVALID_INPUT_URI_FORMAT);
			}
		} else {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_FORMAT);
		}
		return result;
	}
}
