package com.sony.pro.mwa.model.kbase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.enumeration.ExtensionType;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(description="Exntesion model. Extension is MWA extended file such as plugin jar, bpmn, etc.")
public abstract class ExtensionModel {
	protected String id;
	protected String name;
	protected String version;
	protected Long createdTime;
	protected Map<String, String> properties;
	protected List<ExtensionContentModel> contents;

	ExtensionModel() {
		this.properties = new HashMap<String, String>();
	}
	ExtensionModel(ExtensionModel model) {
		this.id = model.getId();
		this.name = model.getName();
		this.version = model.getVersion();
		this.createdTime = model.getCreatedTime();
		this.properties = model.getProperties();
		this.contents = model.getContents();
	}

	public abstract String getType();

	@ApiModelProperty(value = "ID of the extension.")
	public String getId() {
		return id;
	}
	public ExtensionModel setId(String id) {
		this.id = id;
		return this;
	}
	@ApiModelProperty(value = "Name of the extension.")
	public String getName() {
		return name;
	}
	public ExtensionModel setName(String name) {
		this.name = name;
		return this;
	}
	@ApiModelProperty(value = "Version of the extension.")
	public String getVersion() {
		return version;
	}
	public ExtensionModel setVersion(String version) {
		this.version = version;
		return this;
	}
	@ApiModelProperty(value = "Created or registered date time of the extension.")
	public Long getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Long createdTime) {
		this.createdTime = createdTime;
	}
	
	@XmlTransient @JsonIgnore @ApiModelProperty(value = "Properties of the extension.")
	public Map<String, String> getProperties() {
		return properties;
	}
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	@XmlTransient @JsonIgnore @ApiModelProperty(value = "Contents of the extension. (such as activity templates, ...)")
	public List<ExtensionContentModel> getContents() {
		return contents;
	}
	public void setContents(List<ExtensionContentModel> contents) {
		this.contents = contents;
	}
	

	public static ExtensionModel newInstance(String type) {
		ExtensionType et = ExtensionType.valueOf(type);
		switch (et) {
		case BPMN:
			return new MwaWorkflowModel();
		case BUNDLE:
			return new MwaPluginModel();
		case RULE:
			return new MwaRuleModel();
		default:
			return null;
		}
	}
}
