package com.sony.pro.mwa.model.kbase;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wordnik.swagger.annotations.ApiModelProperty;

public class ParameterTypeModel {
	protected String id;
	protected String name;	//TypeString, etc
	protected String version;
	protected boolean listFlag = false;
	List<ParameterTypeValueEntry> values;
	protected Long createTime;
	protected Long updateTime;
	
	public ParameterTypeModel() {
	}
	
	public ParameterTypeModel(String id, String name, String version, List<ParameterTypeValueEntry> values) {
		this.id = id;
		this.name = name;
		this.version = version;
		this.values = values;
	}

	@ApiModelProperty(value = "ID of the parameter type.")
	public String getId() {
		return id;
	}

	public ParameterTypeModel setId(String id) {
		this.id = id;
		return this;
	}

	@ApiModelProperty(value = "Name of the parameter type.")
	public String getName() {
		return name;
	}

	public ParameterTypeModel setName(String name) {
		this.name = name;
		return this;
	}

	@ApiModelProperty(value = "Version of the parameter type.")
	public String getVersion() {
		return version;
	}

	public ParameterTypeModel setVersion(String version) {
		this.version = version;
		return this;
	}

	@ApiModelProperty(value = "ListFlag of the parameter type.")
	public boolean isListFlag() {
		return listFlag;
	}

	public ParameterTypeModel setListFlag(boolean listFlag) {
		this.listFlag = listFlag;
		return this;
	}

	@ApiModelProperty(value = "values of parameter type")
	public List<ParameterTypeValueEntry> getValues() {
		return values;
	}

	public ParameterTypeModel setValues(List<ParameterTypeValueEntry> values) {
		this.values = values;
		return this;
	}

	@JsonIgnore
	public String getValuesString() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(values);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@JsonIgnore
	public ParameterTypeModel setValuesString(String valuesString) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
        List<ParameterTypeValueEntry> values = mapper.readValue(valuesString, new TypeReference<List<ParameterTypeValueEntry>>() {});
		this.values = values;
		return this;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
}
