package com.sony.pro.mwa.model.scheduler;

public class TriggerInfoModel {

	private String schedId;

	private String schedName;

	private String triggerName;

	private String triggerGroup;

	private String triggerType;

	private String http_methodType;

	private String endpoint;

	private String parameter;

	private String description;

	// private String tag;

	private Long createdTime;

	private Long updatedTime;

	private Long startTime;

	private Long endTime;


	public String getSchedId()
	{
		return schedId;
	}

	public void setSchedId(String schedId)
	{
		this.schedId = schedId;
	}

	public String getSchedName()
	{
		return schedName;
	}

	public void setSchedName(String schedName)
	{
		this.schedName = schedName;
	}

	public String getTriggerName()
	{
		return triggerName;
	}

	public void setTriggerName(String triggerName)
	{
		this.triggerName = triggerName;
	}

	public String getTriggerGroup()
	{
		return triggerGroup;
	}

	public void setTriggerGroup(String triggerGroup)
	{
		this.triggerGroup = triggerGroup;
	}

	public String getTriggerType()
	{
		return triggerType;
	}

	public void setTriggerType(String triggerType)
	{
		this.triggerType = triggerType;
	}

	public String getHttp_methodType()
	{
		return http_methodType;
	}

	public void setHttp_methodType(String http_methodType)
	{
		this.http_methodType = http_methodType;
	}

	public String getEndpoint()
	{
		return endpoint;
	}

	public void setEndpoint(String endpoint)
	{
		this.endpoint = endpoint;
	}

	public String getParameter()
	{
		return parameter;
	}

	public void setParameter(String parameter)
	{
		this.parameter = parameter;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Long getCreatedTime()
	{
		return createdTime;
	}

	public void setCreatedTime(Long createdTime)
	{
		this.createdTime = createdTime;
	}

	public Long getUpdatedTime()
	{
		return updatedTime;
	}

	public void setUpdatedTime(Long updatedTime)
	{
		this.updatedTime = updatedTime;
	}

	public Long getStartTime()
	{
		return startTime;
	}

	public void setStartTime(Long startTime)
	{
		this.startTime = startTime;
	}

	public Long getEndTime()
	{
		return endTime;
	}

	public void setEndTime(Long endTime)
	{
		this.endTime = endTime;
	}
}
