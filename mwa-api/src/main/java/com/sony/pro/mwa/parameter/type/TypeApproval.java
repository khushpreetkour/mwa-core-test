package com.sony.pro.mwa.parameter.type;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.parameter.IEnumParameterType;

public class TypeApproval extends TypeString implements IEnumParameterType {
	
	
	public static TypeApproval instance() {
		return instance;
	}
	static TypeApproval instance = new TypeApproval();
	
	@Override
	public List<String> values() {
		return values;
	}
	
	protected final static List<String> values = new ArrayList() {{
		add("pass");
		add("fail");
	}};
}
