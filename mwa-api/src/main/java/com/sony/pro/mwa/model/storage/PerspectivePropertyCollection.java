package com.sony.pro.mwa.model.storage;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class PerspectivePropertyCollection extends Collection<PerspectivePropertyModel> {

	public PerspectivePropertyCollection() {
		super();
	}

	public PerspectivePropertyCollection(List<PerspectivePropertyModel> models) {
		super(models);
	}

	public PerspectivePropertyCollection(List<PerspectivePropertyModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public PerspectivePropertyCollection(Long totalCount, Long count, List<PerspectivePropertyModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<PerspectivePropertyModel> getModels() {
		return super.getModels();
	}
}
