package com.sony.pro.mwa.service.storage;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.storage.DirectoryCollection;
import com.sony.pro.mwa.model.storage.DirectoryModel;
import com.sony.pro.mwa.model.storage.LocationCollection;
import com.sony.pro.mwa.model.storage.LocationModel;

public interface IStorageManager {

	//System Setting用
	public LocationCollection getLocations(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public LocationModel getLocation(String locationId);
	public LocationModel getLocationByName(String locationId);
	public DirectoryCollection getDirectories(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public DirectoryModel getDirectory(String path);
	public LocationModel addLocation(LocationModel model);
	public LocationModel updateLocation(LocationModel model);
	public LocationModel deleteLocation(String locationId);
	public LocationCollection deleteLocations(List<String> sorts, List<String> filters, Integer offset, Integer limit);
}
