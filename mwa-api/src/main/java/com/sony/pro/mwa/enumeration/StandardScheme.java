package com.sony.pro.mwa.enumeration;

public enum StandardScheme {
	mwa,
	mbc,
	ftp,
	file,
	http,
	s3,
	vms
}
