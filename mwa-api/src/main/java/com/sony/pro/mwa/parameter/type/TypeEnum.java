package com.sony.pro.mwa.parameter.type;

import java.util.List;

import com.sony.pro.mwa.parameter.IEnumParameterType;
import com.sony.pro.mwa.parameter.IParameterType;

public class TypeEnum extends TypeString implements IEnumParameterType {

	protected List<String> values;
	protected String name;

	public TypeEnum setEnumValues(List<String> values) {
		this.values = values;
		return this;
	}
	public TypeEnum setName(String name) {
		this.name = name;
		return this;
	}
	public String getName() { return this.name; }
	
/*	@Override
	public boolean isValidImpl(String value) {
		return value instanceof String && this.values().contains((String)value);
	}*/
	
	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeEnum 
				&& this.getName() != null 
				&& this.getName().equals(((TypeEnum)type).getName());
	}
	
	@Override
	public List<String> values() { return this.values; }
}
