package com.sony.pro.mwa.parameter.type;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.parameter.IEnumParameterType;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.rc.MWARC;

public class TypeBoolean extends TypeBase<Boolean> implements IEnumParameterType {

	private final static List<String> valueList = new ArrayList<String>(){{
		add(Boolean.TRUE.toString());
		add(Boolean.FALSE.toString());
	}};
	
	public List<String> values() {
		return new ArrayList<>(valueList);
	}
	
	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeBoolean;
	}

	@Override
	protected String toStringImpl(Boolean value) {
		return (value != null) ? value.toString() : null;
	}

	@Override
	protected Boolean fromStringImpl(String valueStr) {
		if (valueStr == null)
			return null;
		else if (Boolean.TRUE.toString().equals(valueStr) || Boolean.FALSE.toString().equals(valueStr))
			return Boolean.valueOf(valueStr);
		else 
			throw new MwaError(MWARC.INVALID_INPUT_FORMAT);
	}
	
	public static TypeBoolean instance() {
		return instance;
	}
	static TypeBoolean instance = new TypeBoolean();
}
