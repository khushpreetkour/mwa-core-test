package com.sony.pro.mwa.service.activity;

public interface IMwaSyncActivity extends IMwaActivity {
	public Boolean isVolatile();
	public void setVolatile(Boolean volatileFlag);
}
