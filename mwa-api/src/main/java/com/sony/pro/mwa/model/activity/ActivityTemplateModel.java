package com.sony.pro.mwa.model.activity;

import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.enumeration.ActivityType;

public class ActivityTemplateModel {
	protected String id;
	protected String name;
	protected String version;
	protected ActivityType type;
	protected Long bundleId;
	protected String extensionId;
	protected String extensionName;
	protected Boolean deleteFlag;
	protected Boolean syncFlag;
	protected String icon;
	protected String alias;
	protected String description;
	protected List<String> tags;

	public ActivityTemplateModel(){}

	public String getId() {
		return id;
	}
	public ActivityTemplateModel setId(String id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public ActivityTemplateModel setName(String name) {
		this.name = name;
		return this;
	}
	public String getVersion() {
		return version;
	}
	public ActivityTemplateModel setVersion(String version) {
		this.version = version;
		return this;
	}
	public ActivityType getType() {
		return this.type;
	}
	public ActivityTemplateModel setType(ActivityType type) {
		this.type = type;
		return this;
	}
	public ActivityTemplateModel setTypeByString(String type) {
		this.setType(ActivityType.valueOf(type));
		return this;
	}

	@XmlTransient @JsonIgnore
	public Long getBundleId() {
		return bundleId;
	}
	public ActivityTemplateModel setBundleId(Long bundleId) {
		this.bundleId = bundleId;
		return this;
	}
	@XmlTransient @JsonIgnore
	public String getExtensionId() {
		return extensionId;
	}
	public ActivityTemplateModel setExtensionId(String extensionId) {
		this.extensionId = extensionId;
		return this;
	}
	@XmlTransient @JsonIgnore
	public String getExtensionName() {
		return extensionName;
	}
	public ActivityTemplateModel setExtensionName(String extensionName) {
		this.extensionName = extensionName;
		return this;
	}
	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public ActivityTemplateModel setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
		return this;
	}
	public Boolean getSyncFlag() {
		return syncFlag;
	}
	public ActivityTemplateModel setSyncFlag(Boolean syncFlag) {
		this.syncFlag = syncFlag;
		return this;
	}
	public String getIcon() {
		return icon;
	}
	public ActivityTemplateModel setIcon(String icon) {
		this.icon = icon;
		return this;
	}
	public String getAlias() {
		return alias;
	}
	public ActivityTemplateModel setAlias(String alias) {
		this.alias = alias;
		return this;
	}
	public String getDescription() {
		return description;
	}
	public ActivityTemplateModel setDescription(String description) {
		this.description = description;
		return this;
	}
	public List<String> getTags() {
		return tags;
	}
	public ActivityTemplateModel setTags(List<String> tags) {
		this.tags = tags;
		return this;
	}
}
