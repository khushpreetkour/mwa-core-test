package com.sony.pro.mwa.model.kbase;

public class WorkflowViewModel {

	private String id;
	private String name;
	private String extensionId;
	private Boolean deleteFlag;
	private String templateId;
	private String alias;
	private Boolean predefineFlag;
	Long createTime;
	Long updateTime;
	public String getId() {
		return id;
	}
	public WorkflowViewModel setId(String id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public WorkflowViewModel setName(String name) {
		this.name = name;
		return this;
	}
	public String getExtensionId() {
		return extensionId;
	}
	public WorkflowViewModel setExtensionId(String extensionId) {
		this.extensionId = extensionId;
		return this;
	}
	public Boolean getDeleteFlag() {
		return deleteFlag;
	}
	public WorkflowViewModel setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
		return this;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getAlias() {
		return alias;
	}
	public WorkflowViewModel setAlias(String alias) {
		this.alias = alias;
		return this;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
	
	public Boolean getPredefineFlag() {
		return predefineFlag;
	}

	public void setPredefineFlag(Boolean predefineFlag) {
		this.predefineFlag = predefineFlag;
	}

}
