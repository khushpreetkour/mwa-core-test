package com.sony.pro.mwa.utils.rest;

public enum HttpMethodType {
	POST,
	GET,
	PUT,
	DELETE
}
