package com.sony.pro.mwa.model.resource;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ResourceGroupCollection extends Collection<ResourceGroupModel> {

	public ResourceGroupCollection() {
		super();
	}

	public ResourceGroupCollection(List<ResourceGroupModel> models) {
		super(models);
	}

	public ResourceGroupCollection(List<ResourceGroupModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ResourceGroupCollection(Long totalCount, Long count,
			List<ResourceGroupModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ResourceGroupModel> getModels() {
		return super.getModels();
	}
}
