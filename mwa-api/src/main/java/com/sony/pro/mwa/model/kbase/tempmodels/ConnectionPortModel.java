package com.sony.pro.mwa.model.kbase.tempmodels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

// GUIからnullで送られると、ObjectMapperでKeyが落ちるため、コメントアウトしてあります。
// @JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectionPortModel {
	private String id;
	private String stepId;
	private String type;
	private Parameter parameter;
	private PositionModel position;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStepId() {
		return stepId;
	}

	public void setStepId(String stepId) {
		this.stepId = stepId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Parameter getParameter() {
		return parameter;
	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}
	
	public PositionModel getPosition() {
		return position;
	}
	
	public void setPosition(PositionModel position) {
		this.position = position;
	}
}
