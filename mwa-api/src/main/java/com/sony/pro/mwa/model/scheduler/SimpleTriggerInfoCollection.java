package com.sony.pro.mwa.model.scheduler;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class SimpleTriggerInfoCollection extends Collection<SimpleTriggerInfoModel>{
    public SimpleTriggerInfoCollection() {
        super();
    }

    public SimpleTriggerInfoCollection(List<SimpleTriggerInfoModel> models) {
        super(models);
    }

    public SimpleTriggerInfoCollection(List<SimpleTriggerInfoModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public SimpleTriggerInfoCollection(Long totalCount, Long count, List<SimpleTriggerInfoModel> models) {
        super(totalCount, count, models);
    }
	
    @Override
    public List<SimpleTriggerInfoModel> getModels() {
        return super.getModels();
    }
}
