package com.sony.pro.mwa.parameter;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;

public class OperationResult extends HashMap<String, Object> {

	public static OperationResult newInstance() {
		OperationResult result = new OperationResult(MWARC.SUCCESS); //SuccessInstance
		return result;
	}
	public static OperationResult newInstance(IMWARC rc) {
		OperationResult result = new OperationResult(rc);
		return result;
	}
	public static OperationResult newInstance(MwaError error) {
		OperationResult result = new OperationResult(error.getMWARC());
		return result;
	}
	public static OperationResult newInstance(IMWARC rc, String errorDetails) {
		OperationResult result = new OperationResult(rc);
		return result;
	}

	//NVX対応で追加
	public OperationResult(){

	}

	//OperationResultはMWARCが必須。コンストラクタは原則MWARCを引数に取ること
	protected OperationResult(IMWARC rc) {
		super();
	}

	public boolean containsEvent() {
		return this.containsKey(PresetParameter.Event.getKey());
	}

	//IParameterDefinitionをInputに取れるように拡張
	public Object get(IParameterDefinition preset) {
		return this.get(preset.getKey());
	}
	public OperationResult put(IParameterDefinition preset, Object val) {
		super.put(preset.getKey(), val);
		return this;
	}
	public OperationResult put(Map<String, Object> params) {
		for (java.util.Map.Entry<String, Object> entry : params.entrySet()) {
			this.put(entry.getKey(), entry.getValue());
		}
		return this;
	}


	public boolean containsKey(IParameterDefinition preset) {
		return super.containsKey(preset.getKey());
	}

	public String toJson() {
    	String jsonString = "Can't conversion";
    	try {
    		ObjectMapper mapper = new ObjectMapper();
        	jsonString = mapper.writeValueAsString(this);
    	} catch(Exception e) {
    	}
		return jsonString;
	}
}
