package com.sony.pro.mwa.model.scheduler;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * SimpleTrigger登録のInput情報の格納Beanクラス
 */
@ApiModel(value = "SimpleTrigger Input model", description = "")
public class SimpleTriggerInputModel {

	private String httpMethodType;
	private String endpoint;
	private String parameter;
	private String description;
	// private String tag;
	private String repeatCount;
	private String repeatIntervalSec;
	private String startTime;

	@ApiModelProperty(value = "HttpMethodType of SimpleTrigger")
	public String getHttpMethodType()
	{
		return httpMethodType;
	}

	public void setHttpMethodType(String httpMethodType)
	{
		this.httpMethodType = httpMethodType;
	}

	@ApiModelProperty(value = "Endpoint of SimpleTrigger")
	public String getEndpoint()
	{
		return endpoint;
	}

	public void setEndpoint(String endpoint)
	{
		this.endpoint = endpoint;
	}

	@ApiModelProperty(value = "Parameter of SimpleTrigger")
	public String getParameter()
	{
		return parameter;
	}

	public void setParameter(String parameter)
	{
		this.parameter = parameter;
	}

	@ApiModelProperty(value = "Description of SimpleTrigger")
	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	@ApiModelProperty(value = "RepeatCount of SimpleTrigger")
	public String getRepeatCount()
	{
		return repeatCount;
	}

	public void setRepeatCount(String repeatCount)
	{
		this.repeatCount = repeatCount;
	}

	@ApiModelProperty(value = "RepeatIntervalSec of SimpleTrigger")
	public String getRepeatIntervalSec()
	{
		return repeatIntervalSec;
	}

	public void setRepeatIntervalSec(String repeatIntervalSec)
	{
		this.repeatIntervalSec = repeatIntervalSec;
	}

	@ApiModelProperty(value = "StartTime of SimpleTrigger")
	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

}
