package com.sony.pro.mwa.model.kbase.tempmodels;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.model.kbase.ActivityProfileGroupModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileModel;

public class WorkflowDetailModel extends WorkflowModel {
	private List<ActivityProfileGroupModel> profileGroups = new ArrayList<>();
	private List<ActivityProfileModel> profiles = new ArrayList<>();

	public WorkflowDetailModel(WorkflowModel model) {
		this.setId(model.getId());
		this.setName(model.getName());
		this.setVersion(model.getVersion());
		this.setCreateTime(model.getCreateTime());
		this.setUpdateTime(model.getUpdateTime());
		this.setSteps(model.getSteps());
		this.setConnectionPorts(model.getConnectionPorts());
		this.setConnections(model.getConnections());
		this.setInputs(model.getInputs());
		this.setOutputs(model.getOutputs());

		this.setType(model.getType());
		this.setTemplateId(model.getTemplateId());
		this.setSyncFlag(model.isSyncFlag());
		this.setDeleteFlag(model.getDeleteFlag());
		this.setDiagramId(model.getDiagramId());
		this.setAlias(model.getAlias());
	    this.setPredefineFlag(model.getPredefineFlag());
	}

	public void setProfileGroups(List<ActivityProfileGroupModel> profileGroups) {
		this.profileGroups = profileGroups;
	}

	public List<ActivityProfileGroupModel> getProfileGroups() {
		return profileGroups;
	}

	public void setProfiles(List<ActivityProfileModel> profiles) {
		this.profiles = profiles;
	}

	public List<ActivityProfileModel> getProfiles() {
		return profiles;
	}
}
