package com.sony.pro.mwa.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.GenericPropertyBase;
import com.sony.pro.mwa.model.KeyValueModel;

public class Converter {

	@SuppressWarnings("unchecked")
	public static List<KeyValueModel> mapToKeyValueList(Map<String, ? extends Object> map) {
		List<KeyValueModel> result = new ArrayList<KeyValueModel>();
		if (map != null) {
			for (Map.Entry<String, ? extends Object> entry : map.entrySet()){
				if (entry != null) {
					String val = (entry.getValue() != null) ? entry.getValue().toString() : null;
					KeyValueModel elem = new KeyValueModel(entry.getKey(), val);
					result.add(elem);
				}
			}
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public static <T> Map<String, T> keyValueListToMap(List<KeyValueModel> list) {
		Map<String, T> result = new TreeMap<String, T>(String.CASE_INSENSITIVE_ORDER);
		if (list != null) {
			for (KeyValueModel model : list){
				result.put(model.getKey(), (T)model.getValue());
			}
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public static Map<String, Long> costValueListToCostValues(List<KeyValueModel> list) {
		Map<String, Long> result = new TreeMap<String, Long>(String.CASE_INSENSITIVE_ORDER);
		if (list != null) {
			for (KeyValueModel model : list){
				result.put(model.getKey(), Long.parseLong(model.getValue()));
			}
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public static List<? extends GenericPropertyBase> mapToPropertyList(Map<String, Object> map) {
		List<GenericPropertyModel> result = new ArrayList<GenericPropertyModel>();
		if (map != null) {
			for (Map.Entry<String, Object> entry : map.entrySet()){
				if (entry != null) {
					String val = (entry.getValue() != null) ? entry.getValue().toString() : null;
					GenericPropertyModel elem = new GenericPropertyModel();
					elem.setPropertyName(entry.getKey());
					elem.setPropertyValue((String)entry.getValue());
					result.add(elem);
				}
			}
		}
		return result;
	}
	@SuppressWarnings("unchecked")
	public static <T> Map<String, Object> propertyListToMap(List<? extends GenericPropertyBase> list) {
		Map<String, Object> result = new TreeMap<String, Object>(String.CASE_INSENSITIVE_ORDER);
		if (list != null) {
			for (GenericPropertyBase model : list){
				result.put(model.getPropertyName(), model.getPropertyValue());
			}
		}
		return result;
	}
}
