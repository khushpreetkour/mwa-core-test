package com.sony.pro.mwa.model.kbase.tempmodels;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StepModel {
	private String id;
	private String name;
	private Map<String, Object> activity;
	private PositionModel position;

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Object> getActivity() {
		return this.activity;
	}

	public void setActivity(Map<String, Object> activity) {
		this.activity = activity;
	}

	public PositionModel getPosition() {
		return position;
	}

	public void setPosition(PositionModel position) {
		this.position = position;
	}
}
