package com.sony.pro.mwa.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sony.pro.mwa.service.activity.IState;

public class StateModel implements IState {

	public StateModel() {}
	public StateModel(IState state) {
		this.name = state.getName();
		this.terminatedFlag = state.isTerminated();
		this.stableFlag = state.isStable();
		this.errorFlag = state.isError();
		this.cancelFlag = state.isCancel();
	}
	public StateModel(String name, boolean terminatedFlag, boolean stableFlag, boolean errorFlag, boolean cancelFlag) {
		this.name = name;
		this.terminatedFlag = terminatedFlag;
		this.stableFlag = stableFlag;
		this.errorFlag = errorFlag;
		this.cancelFlag = cancelFlag;
	}
	
	String name;
	boolean terminatedFlag;
	boolean stableFlag;
	boolean errorFlag;
	boolean cancelFlag;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	@JsonProperty("terminated")
	public boolean isTerminated() {
		return terminatedFlag;
	}

	@Override
	@JsonProperty("stable")
	public boolean isStable() {
		return stableFlag;
	}

	@Override
	@JsonProperty("error")
	public boolean isError() {
		return errorFlag;
	}

	@Override
	@JsonProperty("cancel")
	public boolean isCancel() {
		return cancelFlag;
	}

	@JsonProperty("terminated")
	public void setTerminatedFlag(boolean terminatedFlag) {
		this.terminatedFlag = terminatedFlag;
	}
	@JsonProperty("stable")
	public void setStableFlag(boolean stableFlag) {
		this.stableFlag = stableFlag;
	}
	@JsonProperty("error")
	public void setErrorFlag(boolean errorFlag) {
		this.errorFlag = errorFlag;
	}
	@JsonProperty("cancel")
	public void setCancelFlag(boolean cancelFlag) {
		this.cancelFlag = cancelFlag;
	}
}
