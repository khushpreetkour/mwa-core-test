package com.sony.pro.mwa.parameter.type;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.model.resource.CostModel;
import com.sony.pro.mwa.parameter.IParameterType;

public class TypeCost extends TypeBase<CostModel> {

	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeString;
	}

	@Override
	public String toStringImpl(CostModel value) {
		// TODO Auto-generated method stub
		// return Converter.listToXml(value);

		return Converter.toXmlString(value);
	}

	@Override
	protected CostModel fromStringImpl(String valueStr) {

		//Java8対応
		CostModel costModel = new CostModel();
		{
			XMLConfiguration config = new XMLConfiguration();
			config.setDelimiterParsingDisabled(true);

			try (InputStream is = new ByteArrayInputStream(valueStr.getBytes("utf-8"))) {
				config.load(is);
			} catch (IOException e1) {
				//発生しないはず。
				e1.printStackTrace();
			} catch (ConfigurationException e) {
				//発生しないはず。
				e.printStackTrace();
			}
			final String type = config.getString("type");
			costModel.setType(type);
			List<HierarchicalConfiguration> valueList = config.configurationsAt("valueList");
			for (HierarchicalConfiguration kv : valueList) {
				final String key = kv.getString("key");
				final Long value = Long.parseLong(kv.getString("value"));
				costModel.getValues().put(key, value);
			}
		}
		return costModel;

	}

	public static TypeCost instance() {
		return instance;
	}

	static TypeCost instance = new TypeCost();
}