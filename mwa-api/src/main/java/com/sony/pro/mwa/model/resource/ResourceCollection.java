package com.sony.pro.mwa.model.resource;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;
import com.sony.pro.mwa.model.resource.ResourceModel;

public class ResourceCollection extends Collection<ResourceModel> {

	public ResourceCollection() {
		super();
	}

	public ResourceCollection(List<ResourceModel> models) {
		super(models);
	}

	public ResourceCollection(List<ResourceModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ResourceCollection(Long totalCount, Long count,
			List<ResourceModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ResourceModel> getModels() {
		return super.getModels();
	}
}
