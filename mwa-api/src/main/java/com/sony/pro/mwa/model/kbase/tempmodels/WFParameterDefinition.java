package com.sony.pro.mwa.model.kbase.tempmodels;

/**
 * ParameterDefinitionのtypeNameは、プロパティ「type」のインスタンスが返却する値を使用しており
 * 設計上、typeNameに対するsetを定義することはできない（難がある）
 * 
 * この制約のために、ParameterDefinitionをjacksonでSerialize/Desirializeしようとしても
 * typeNameフィールドをjacksonが認識できずエラーとなる。
 * 
 * そこで、ParameterDefinitionの代替として、単純にrequired, key, typeNameのみを保持するクラスを作成し
 * 問題を回避することとしました。
 * 
 * 2018/3/2 
 */
public class WFParameterDefinition {

	private boolean required;
	private String typeName;
	private String key;
	private String parentKey;
	private Object parentValue;
	private boolean visibleFlag = true;
	private boolean listFlag = false;
	
	public boolean getRequired() {
		return this.required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public String getTypeName() {
		return this.typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getKey() {
		return this.key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getParentKey() {
		return this.parentKey;
	}
	public void setParentKey(String parentKey) {
		this.parentKey = parentKey;
	}
	public Object getParentValue() {
		return this.parentValue;
	}
	public void setParentValue(Object parentValue) {
		this.parentValue = parentValue;
	}
	public boolean getVisibleFlag() {
		return this.visibleFlag;
	}
	public void setVisibleFlag(boolean visibleFlag) {
		this.visibleFlag = visibleFlag;
	}
	public boolean getListFlag() {
		return listFlag;
	}
	public void setListFlag(boolean listFlag) {
		this.listFlag = listFlag;
	}
}
