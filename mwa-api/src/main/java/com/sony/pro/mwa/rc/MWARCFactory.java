package com.sony.pro.mwa.rc;

public class MWARCFactory implements IMWARCFactory {
	@Override
	public IMWARC create(String code) {
		return MWARC.valueOfByCode(code);
	}

	@Override
	public String getGroupId() {
		return MWARC.getGroupId();
	}
	
	static {	//Add myself to static factory list;
		MWARCUtil.addFactory(new MWARCFactory());
	}
}
