package com.sony.pro.mwa.model.activity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.StateModel;
import com.sony.pro.mwa.service.activity.IState;
import com.wordnik.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ActivityInstanceSimpleModel {
	String instanceId;
	String parentId;
	String name;
	private transient IState stateOriginal = new StateModel();
	StateModel state = new StateModel();
	Integer progress;
	Integer priority;
    Long startTime;
    Long endTime;
    String templateName;
    String templateVersion;
    Long createTime;
    Long updateTime;
    
    private final static MwaLogger logger = MwaLogger.getLogger(ActivityInstanceSimpleModel.class);

    public ActivityInstanceSimpleModel(){}
    public ActivityInstanceSimpleModel(ActivityInstanceSimpleModel source){
    	this.instanceId = source.getId();
    	this.parentId = source.getParentId();
    	this.name = source.getName();
    	this.setStatus(source.getStatus());
    	this.progress = source.getProgress();
    	this.startTime = source.getStartTime();
    	this.endTime = source.getEndTime();
    	this.updateTime = source.getUpdateTime();
    	this.templateName = source.getTemplateName();
    	this.templateVersion = source.getTemplateVersion();
    	this.createTime = source.getCreateTime();
    }

	@ApiModelProperty(position=0, dataType = "UUID", required=false, value = "ID of activity instance")
	public String getId() { return this.instanceId; }
	@ApiModelProperty(position=1, dataType = "UUID", required=false, value = "ID of parent activity instance. \"parentId\" and \"name\" idendify activity instance.")
	public String getParentId() { return this.parentId; }
	@ApiModelProperty(position=2, dataType = "String(256)", required=false,
			value = "Name of the activity instance. It's for identification and \"name\" is unique among parent instance.")
	public String getName() { return this.name; }
	@ApiModelProperty(position=3, required=false,
			value = "Activity template name of the acitivty instance.")
	public String getTemplateName() { return templateName; }
	@ApiModelProperty(position=4, required=false,
			value = "Activity template version of the acitivty instance.")
	public String getTemplateVersion() { return templateVersion; }
	@ApiModelProperty(position=5, required=false,
			value = "Status of the activity instance.")
	@JsonIgnore
	public IState getStatus() { return this.stateOriginal; }
	@JsonProperty("status")	//Modelで返したいので苦肉の策
	public IState getStatusModel() { return this.state; }
	@ApiModelProperty(position=6, dataType = "Integer[0-100]", required=false,
			value = "Progress of the activity instance processing.")
	public Integer getProgress() {return this.progress;}
	@ApiModelProperty(position=7, required=false,
			value = "Started time of the activity instance.")
	public Long getStartTime() { return this.startTime; }
	@ApiModelProperty(position=8, required=false,
			value = "Terminated time of the activity instance.")
	public Long getEndTime() { return this.endTime; }
	@ApiModelProperty(position=9, required=false,
			value = "Last updated time of the activity instance.")
	public Long getUpdateTime() { return this.updateTime; }
	@ApiModelProperty(position=10, required=false,
			value = "Created time of the activity instance.")
	public Long getCreateTime() { return this.createTime; }

	public void setId(String id) { this.instanceId = id;}
	public void setParentId(String parentId) { this.parentId = parentId; }
	public void setName(String name) { this.name = name; }
	public void setStatus(StateModel state) {
		this.state = state;
		this.stateOriginal = state;
		if (state != null && state.isTerminated()) {
			this.endTime = System.currentTimeMillis();
		}
	}
	public void setStatus(IState state) { 
		this.state = new StateModel(state);
		this.stateOriginal = state;
		if (state != null && state.isTerminated()) {
			this.endTime = System.currentTimeMillis();
		}
	}
	public void setProgress(Integer progress) { this.progress = progress; }
	public void setStartTime(Long startTime) { this.startTime = startTime; }
	public void setEndTime(Long endTime) { this.endTime = endTime; }
	public void setUpdateTime(Long updateTime) { this.updateTime = updateTime; }
	public void setTemplateName(String templateName) { this.templateName = templateName; }
	public void setTemplateVersion(String templateVersion) { this.templateVersion = templateVersion; }
	public void setCreateTime(Long createTime) { this.createTime = createTime; }
	
    public Integer getPriority() { return this.priority; }
    public void setPriority(Integer priority) { this.priority = priority; }

}
