package com.sony.pro.mwa.model.resource;

import java.util.Map;

import com.sony.pro.mwa.enumeration.ResourceGroupType;

public class ResourceGroupContentModel extends ResourceModel {
	Map<ResourceGroupType, String> groupIds;

	public Map<ResourceGroupType, String> getResourceGroupIds() {
		return groupIds;
	}

	public void setResourceGroupIds(Map<ResourceGroupType, String> groupIds) {
		this.groupIds = groupIds;
	}
}
