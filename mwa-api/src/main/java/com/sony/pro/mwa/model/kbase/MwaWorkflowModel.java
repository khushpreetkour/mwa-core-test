package com.sony.pro.mwa.model.kbase;

import com.sony.pro.mwa.enumeration.ExtensionType;

public class MwaWorkflowModel extends ExtensionModel {

	@Override
	public String getType() {
		return ExtensionType.BPMN.name();
	}
}
