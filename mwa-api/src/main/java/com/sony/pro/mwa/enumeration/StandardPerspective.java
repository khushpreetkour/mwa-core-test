package com.sony.pro.mwa.enumeration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sony.pro.mwa.service.storage.IPerspective;

public enum StandardPerspective implements IPerspective {
    //uri
    MWA		(Pattern.compile( "^(" + StandardScheme.mwa + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    MBC		(Pattern.compile( "^(" + StandardScheme.mbc + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    FTP		(Pattern.compile( "^(" + StandardScheme.ftp + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    FILE	(Pattern.compile( "^(" + StandardScheme.file + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    //path
    WINDOWS(Pattern.compile( "^([a-zA-Z]:(\\\\|(\\\\[^\\\\]+)+))\\\\?"),	false,	"\\"),
    UNIX	(Pattern.compile( "^(/((?:[^/]+)/?)*)"),	false),
    UNC		(Pattern.compile( "^([\\\\\\\\].*)"),		false,	"\\"),
    WINDOWS_FORWARD_SLASH(Pattern.compile( "^([a-zA-Z]:(/|(/[^/]+)+))/?"),	false),
    UNC_FORWARD_SLASH(Pattern.compile( "^(//.*)"),		false),
    ODA            (null, false),
    ODA_LIBRARY    (null, false),
    PFD (null, false),
    YOUTUBE	(Pattern.compile( "^(" + StandardScheme.http + "s?" + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    CI	(Pattern.compile( "^(" + StandardScheme.http + "s?" + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    S3	(Pattern.compile( "^(" + StandardScheme.s3 + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    VMS	(Pattern.compile( "^(" + StandardScheme.vms + ")://(?:([^/?]+))?([^?]*)(?:\\?(.*))?"),	true),
    URI		(Pattern.compile( "^(?:([^:/?#]+):)//(?:([^/?#]*))?([^?#]*)(?:\\?([^#]*))?(?:#(.*))?"),	true),
    ;

    final static String DEFAULT_SEPARATOR = "/";
    private static final String OS_NAME = System.getProperty("os.name").toLowerCase();

    Pattern pattern;
    boolean isUri;
    String separator;
    StandardPerspective(Pattern pattern, boolean isUri) {
        this(pattern, isUri, DEFAULT_SEPARATOR);
    }
    StandardPerspective(Pattern pattern, boolean isUri, String separator) {
        this.separator = separator;
        this.isUri = isUri;
        this.pattern = pattern;
    }

    @Override
    public boolean isBackSlash() {
        return "\\".equals(this.separator);
    }
    @Override
    public boolean isUri() {
        return this.isUri;
    }
    @Override
    public Boolean isMatch(String path) {
        if (this.pattern != null)
            return this.pattern.matcher(path).matches();
        else
            return null;
    }
    public Pattern getPattern() {
        return pattern;
    }

    public static IPerspective checkPerspective(String path) {
        for (StandardPerspective perspective : StandardPerspective.values()) {
            Pattern pattern = perspective.getPattern();
            if (pattern == null)
                continue;
            Matcher m = pattern.matcher(path);
            if (m.matches()) {
                return perspective;
            }
        }
        return null;
    }

    public static String[] getDefaultNativePathPerspectiveArray() {
    	if (OS_NAME.startsWith("windows"))
    		return new String[]{UNC.name(), WINDOWS.name()};
    	else if (OS_NAME.startsWith("linux"))
    		return new String[]{UNIX.name()};
    	else
    		return new String[]{UNC.name(), WINDOWS.name(), UNIX.name()};
    }
}
