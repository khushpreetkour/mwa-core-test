package com.sony.pro.mwa.enumeration;

public enum ActivityType {
    WORKFLOW(PluginType.BPMN),
    TASK(PluginType.BUNDLE),
    START(PluginType.BUNDLE),
    END(PluginType.BUNDLE),
    CONVERTER(PluginType.BUNDLE),
    DTABLE(PluginType.RULE),
    DSL(PluginType.RULE),
    DSLR(PluginType.RULE),
    DRL(PluginType.RULE),
    ;

    private ActivityType(PluginType type) {
        this.type = type;
    }
    PluginType type;

    public boolean equals (String name) {
        return this.name().equals(name);
    }
    public boolean isBundle() {return this.type.equals(PluginType.BUNDLE);}
    public boolean isBpmn() {return this.type.equals(PluginType.BPMN);}
    public boolean isRule() {return this.type.equals(PluginType.RULE);}

    protected enum PluginType {
        BUNDLE,
        BPMN,
        RULE,
    }
}
