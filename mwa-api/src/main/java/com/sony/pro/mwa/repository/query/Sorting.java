package com.sony.pro.mwa.repository.query;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 6:03
 * To change this template use File | Settings | File Templates.
 */
public class Sorting {
    private String name;
    private boolean isAscending = true;

    public Sorting() {}

    /**
     * @param name
     * @param isAscending
     */
    public Sorting(String name, boolean isAscending) {
        this.name = name;
        this.isAscending = isAscending;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the isAscending
     */
    public boolean isAscending() {
        return isAscending;
    }

    /**
     * @param isAscending the isAscending to set
     */
    public void setAscending(boolean isAscending) {
        this.isAscending = isAscending;
    }

    /**
     * @param sorting
     * @return
     */
    public boolean equals(Sorting sorting) {
        return name.equalsIgnoreCase(sorting.name) &&  isAscending == sorting.isAscending;

    }
}
