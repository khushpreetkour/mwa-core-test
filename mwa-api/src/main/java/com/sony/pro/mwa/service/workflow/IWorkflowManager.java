package com.sony.pro.mwa.service.workflow;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.service.activity.IActivityTemplate;

public interface IWorkflowManager {
	public IActivityTemplate getActivityTemplate(String bpmnPath);
	public void reloadProcessEngine(List<String> bpmnPaths);
	public IProcessEngine getProcessEngine();
	void registerTemplate(String templateName, String templateVersion, boolean syncFlag);
	public IRuleManager getRuleManager();
	public void notifyStepDone(String activityParentInstanceId);
}
