package com.sony.pro.mwa.service.provider;

import java.util.List;

import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;

public interface IActivityProviderManager {
	public ActivityProviderCollection getProviders(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ActivityProviderModel getProvider(String providerId);
	public IActivityProvider getProviderService(String providerId);
	public ActivityProviderModel addProvider(ActivityProviderModel provider);
	public ActivityProviderModel updateProvider(ActivityProviderModel provider);
	public ActivityProviderModel deleteProvider(String providerId);
	public ActivityProviderCollection deleteProviders(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	
	public ActivityProviderTypeModel addProvidertype(ActivityProviderTypeModel model);
}
