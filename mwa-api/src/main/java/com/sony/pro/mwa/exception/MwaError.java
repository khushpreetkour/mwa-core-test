package com.sony.pro.mwa.exception;

import com.sony.pro.mwa.rc.IMWARC;

public class MwaError extends RuntimeException {

	public MwaError(IMWARC mwarc) {
		this(mwarc, null);
	}
	public MwaError(IMWARC mwarc, MwaError e) {
		super(e);
		this.mwarc = mwarc;
		this.deviceResponseCode = null;
		this.deviceResponseDetails = null;
		//e は後々ハンドリング
	}
	public MwaError(IMWARC mwarc, String deviceResponseCode, String deviceResponseDetails) {
		this(mwarc, null);
		this.deviceResponseCode = deviceResponseCode;
		this.deviceResponseDetails = deviceResponseDetails;
	}
	public MwaError(MwaError e) {
		super(e);
		this.mwarc = e.getMWARC();
		this.deviceResponseCode = e.getDeviceResponseCode();
		this.deviceResponseDetails = e.getDeviceResponseDetails();
	}
	
	protected IMWARC mwarc;
	protected String deviceResponseCode;
	protected String deviceResponseDetails;
	protected String description;
	
	public IMWARC getMWARC() {
		return mwarc;
	}

	public boolean is(IMWARC mwarc) {
		return this.mwarc.equals(mwarc);
	}
	
	public boolean isInstanceError() {
		return false;
	}
	
	public String getDescription() { 
		if (this.description != null) 
			return this.description;
		else
			return super.getLocalizedMessage();
	}
	public MwaError setDescription(String description) { this.description = description; return this;}
	
	public String getDeviceResponseCode() { return this.deviceResponseCode; }
	public String getDeviceResponseDetails() { return this.deviceResponseDetails; }
}
