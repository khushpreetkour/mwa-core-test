package com.sony.pro.mwa.service.activity;

public interface IEvent {
	/**
	 * Stateの名前を返す。とりあえずログ出力に使う程度
	 * UpperCamelCaseを推奨。特にこだわりなければクラス名で（多言語やるならこのIDから引くイメージ）
	 * 
	 * @param N/A
	 * @return String 本Stateの表示名。
	 */
	public String getName();
	
	public boolean isRequestEvent();
	public boolean isNotifyEvent();
	public boolean isConfirmEvent();
	public boolean isMatch(String eventName);
}
