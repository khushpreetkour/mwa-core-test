package com.sony.pro.mwa.parameter.type;

/***
 * TypeUriの新設に伴い、Deprecatedとする。
 */
@Deprecated
public class TypeMwaUri extends TypeString {

	//可能な範囲でSequenceの接続可否をこのレイヤーでやりたいところだが。。。（動的な可能性もあり、下のレイヤーでチェックするしかないか）
/*	@Override
	public boolean isValidImpl(String value) {
		return super.isValid(value) && true; //MWAURI用のチェックを入れる
	}*/

	public static TypeMwaUri instance() {
		return instance;
	}
	static TypeMwaUri instance = new TypeMwaUri();
}
