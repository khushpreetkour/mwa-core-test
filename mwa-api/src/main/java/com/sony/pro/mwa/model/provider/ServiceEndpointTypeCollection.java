package com.sony.pro.mwa.model.provider;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ServiceEndpointTypeCollection extends Collection<ServiceEndpointTypeModel> {

	public ServiceEndpointTypeCollection() {
		super();
	}

	public ServiceEndpointTypeCollection(List<ServiceEndpointTypeModel> models) {
		super(models);
	}

	public ServiceEndpointTypeCollection(List<ServiceEndpointTypeModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public ServiceEndpointTypeCollection(Long totalCount, Long count, List<ServiceEndpointTypeModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ServiceEndpointTypeModel> getModels() {
		return super.getModels();
	}
}
