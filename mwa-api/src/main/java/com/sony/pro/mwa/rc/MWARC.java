package com.sony.pro.mwa.rc;

public enum MWARC implements IMWARC {
	/** Success */
	SUCCESS										("0000"),

	/** Input Error (Generic) */
	INVALID_INPUT								("1000"),
	/** Input - Invalid instance id */
	INVALID_INPUT_INSTANCE_ID					("1001"),
	/** Input - Invalid operation (Plug-in unsupported event) */
	INVALID_INPUT_EVENT							("1002"),
	/** Input - Invalid template name */
	INVALID_INPUT_TEMPLATE_NAME					("1003"),
	/** Input - Invalid local name (= workflow step name) */
	INVALID_INPUT_LOCAL_NAME					("1004"),
	/** Input - Invalid input parameter type */
	INVALID_INPUT_FORMAT						("1101"),
	/** Input - Required parameter not found */
	INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND		("1102"),
	/** Input - Invalid provider id */
	INVALID_INPUT_PROVIDER_ALREAD_EXISTS		("1201"),
	/** Input - Invalid provider id */
	INVALID_INPUT_PROVIDER_NOT_FOUND			("1202"),
	/** Input - Invalid provider id */
	INVALID_INPUT_ENDPOINT_NOT_FOUND			("1203"),
	/** Input - Invalid provider id */
	INVALID_INPUT_LOCATION						("1301"),
	/** Input - Invalid provider id */
	INVALID_INPUT_PERSPECTIVE					("1302"),
	/** Input - Invalid provider id */
	INVALID_INPUT_URI_FORMAT					("1303"),
	/** Input - Invalid cost for resource request */
	INVALID_INPUT_RESOURCE_REQUEST_COST			("1401"),
	/** Input - Invalid occupant for resource request */
	INVALID_INPUT_RESOURCE_REQUEST_OCCUPANT		("1402"),
	INVALID_INPUT_RESOURCE_REQUEST_MULTIPLE_QUEUE	("1403"),
	INVALID_INPUT_RESOURCE_REQUEST_MISSING_QUEUE	("1404"),
	INVALID_INPUT_RESOURCE_NOT_FOUND			("1405"),
	INVALID_INPUT_QUEUE_ID						("1501"),
	INVALID_INPUT_TEMPLATE_UNDEPLOYED			("1601"),
	/** Input - Invalid cron registration request */
	INVALID_INPUT_SCHEDULER						("1701"),
	INVALID_INPUT_SCHEDULER_AFTER_FIVE_MINUTES	("1702"),

	//General Operation Failed (2xxx)
	/** Operation Error (Generic) */
	OPERATION_FAILED							("2000"),
	/** Operation - Unacceptable instance state */
	OPERATION_FAILED_UNACCEPTABLE_STATE			("2001"),
	/** Operation - Can't create activity instance */
	OPERATION_CANNOT_CREATE_ACTIVITY_INSTANCE	("2002"),
	OPERATION_FAILED_RESOURCE_SHORTAGE			("2101"),
	OPERATION_FAILED_RESOURCE_FULL				("2102"),
	OPERATION_FAILED_RESOURCE_NOT_FOUND			("2103"),
	OPERATION_FAILED_RESOURCE_INCOMPATIBLE		("2104"),
	OPERATION_FAILED_RESOURCE_REQUEST_NOT_FOUND	("2201"),
	OPERATION_FAILED_PROCESS_NOT_FOUND			("2301"),
	OPERATION_FAILED_DEPLOY_NOT_ACCEPT			("2401"),

	//General Connection Failed (3xxx)
	/** Connection Error (Generic) */
	CONNECTION_FAILED							("3000"),
	/** Connection - Host not found */
	CONNECTION_HOST_NOT_FOUND					("3001"),
	/** Connection - Time out */
	CONNECTION_TIMEOUT							("3002"),
	/** Connection - Connection refused */
	CONNECTION_REFUSED							("3003"),
	/** Connection - Service temporary unavailable */
	CONNECTION_SERVICE_TEMPORARY_UNAVAILABLE	("3004"),

	//Authentication Failed (4xxx)
	/** Authentication Error (Generic) */
	AUTHENTICATION_FAILED						("4000"),

	//Authentication Failed (5xxx)
	/** Configuration Error (Generic) */
	CONFIGURATION_ERROR							("5000"),
	/** Configuration Error (Location not found) */
	CONFIGURATION_LOCATION_NOT_FOUND			("5001"),

	//General KnowledgeBase Error (Bxxx)
	/** KnowledgeBase Error (Generic) */
	KBASE_ERROR									("B000"),

	//General Database Error (Cxxx)
	/** Database Error (Generic) */
	DATABASE_ERROR								("C000"),
	/** Database - Unique constraint */
	DB_UNIQUE_CONSTRAINT						("C001"),

	//General Device Error (Dxxx)
	/** Device Error (Generic) */
	DEVICE_ERROR								("D000"),
	/** Device - Unsupported operation */
	DEVICE_UNSUPPORTED_OPERATION				("D001"),

	//General Integration Error (Exxx)
	/** Integration error */
	INTEGRATION_ERROR							("E000"),
	/** Integration - Activity not found */
	INTEGRATION_ACTIVITY_NOT_FOUND				("E001"),

	///** Integration - Unsupported event (Invalid implementation by external Plug-in) */
	//INTEGRATION_UNSUPPORTED_EVENT				("E002"),
	/** Integration - Invalid MWARC format */
	INTEGRATION_INVALID_MWARC_FORMAT			("E003"),
	/** Integration - Plug-in registered duplicate GroupId for MWARC factory */
	INTEGRATION_DUPLICATE_FACTORY_REGISTRATION	("E004"),

	//General System Error (Fxxx)
	/** System error */
	SYSTEM_ERROR								("FF00"),
	/** System - Invalid activity implementation */
	SYSTEM_ERROR_IMPLEMENTATION					("FF01"),
	;

	private static final String GROUPID = GroupId.COMMON.id(); //Common用の予約済みgroupId
	private static final String REGEX = IMWARC.PREFIX + IMWARC.DELIMITER + GROUPID + "[0-9a-fA-F]{4}";
	private static final java.util.regex.Pattern PATTERN = java.util.regex.Pattern.compile(REGEX);
	private final String localId;

	private MWARC(String localId) {
		this.localId = localId;
	}

	@Override
	public String code() {
		return MWARCUtil.genMWARC(GROUPID, this.localId);
	}

	@Override
	public boolean isSuccess() {
		return SUCCESS == this;
	}

	static public MWARC valueOfByName(String name) {
		return MWARC.valueOf(name);
	}
	static public MWARC valueOfByCode(String code) {
		MWARC result = null;
		for (MWARC value : MWARC.values()) {
			if (value.code().equals(code)) {
				result = value;
				break;
			}
		}
		return result;
	}

	static public boolean checkGroup(String code) {
		return MWARCUtil.checkFormat(PATTERN, code);
	}

	static public String getGroupId() {
		return GROUPID;
	}
}
