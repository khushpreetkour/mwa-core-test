package com.sony.pro.mwa.service.workflow;

import java.util.List;

import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.service.activity.IActivityTemplate;

/**
 * @author Xi.Ou
 *
 */
public interface IRuleManager {
    public IActivityTemplate getActivityTemplate(String rulePath, ActivityType type);
    public void reloadRuleEngine(List<String> rulePaths);
    public IProcessEngine getProcessEngine();
    void registerTemplate(String templateName, String templateVersion, boolean syncFlag);

}
