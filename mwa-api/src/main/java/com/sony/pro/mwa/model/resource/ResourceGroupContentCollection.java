package com.sony.pro.mwa.model.resource;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ResourceGroupContentCollection extends Collection<ResourceGroupContentModel> {

	public ResourceGroupContentCollection() {
		super();
	}

	public ResourceGroupContentCollection(List<ResourceGroupContentModel> models) {
		super(models);
	}

	public ResourceGroupContentCollection(List<ResourceGroupContentModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public ResourceGroupContentCollection(Long totalCount, Long count,
			List<ResourceGroupContentModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ResourceGroupContentModel> getModels() {
		return super.getModels();
	}
}
