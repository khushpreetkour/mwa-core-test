package com.sony.pro.mwa.model.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="Location model", description="Location itself is virtual concept, and perspective resolves actual path from location.")
public class LocationModel {

	String id;
	String name;
	Map<String, PerspectiveModel> perspectiveMap;
	Long createTime;
	Long updateTime;

	@ApiModelProperty(dataType = "UUID", value = "ID of the location")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@ApiModelProperty(value = "Name of the location. It's utilized in MWA-URI, e.g. mwa://SampleLocation/shared/")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ApiModelProperty(value = "List of perspectives of the location.")
	public List<PerspectiveModel> getPerspectiveList() {
		if (perspectiveMap == null)
			return null;
		return new ArrayList<>(perspectiveMap.values());
	}
	public void setPerspectiveList(List<PerspectiveModel> perspectiveList) {
		if (perspectiveList != null) { 
			Map<String, PerspectiveModel> pmMap = new HashMap<>();
			for (PerspectiveModel model : perspectiveList) {
				PerspectiveModel pm = new PerspectiveModel();
				pm.setPerspective(model.getPerspective());
				pm.setBasePath(model.getBasePath());
				pm.setUser(model.getUser());
				pm.setPassword(model.getPassword());
				pmMap.put(model.getPerspective(), model);
			}
			this.perspectiveMap = pmMap;
		}
		else
			this.perspectiveMap = null;
	}
	
	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public Map<String, PerspectiveModel> getPerspectiveMap() {
		return perspectiveMap;
	}
	public void setPerspectiveMap(Map<String, PerspectiveModel> perspectiveMap) {
		this.perspectiveMap = perspectiveMap;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
}
