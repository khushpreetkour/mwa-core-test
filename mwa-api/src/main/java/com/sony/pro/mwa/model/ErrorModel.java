package com.sony.pro.mwa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.rc.IMWARC;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 14/01/16
 * Time: 1:45
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "error")
@XmlType(propOrder = {"code", "message"})
public class ErrorModel {
    private IMWARC code;
    private String[] args; // Not Serialized
    private String message;

    public ErrorModel() {}

    public ErrorModel(IMWARC code) {
        this.code = code;
    }

    public ErrorModel(IMWARC code, String message) {
        this.code = code;
        this.message = message;
    }

    public ErrorModel(IMWARC code, String[] args) {
        this.code = code;
        this.args = args;
    }

    public ErrorModel(IMWARC code, String[] args, String message) {
        this.code = code;
        this.args = args;
        this.message = message;
    }

    public IMWARC getCode() {
        return code;
    }

    public void setCode(IMWARC code) {
        this.code = code;
    }

    @XmlTransient @JsonIgnore
    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
