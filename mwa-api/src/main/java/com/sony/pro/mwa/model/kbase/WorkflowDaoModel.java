package com.sony.pro.mwa.model.kbase;

public class WorkflowDaoModel extends WorkflowViewModel {

	private String version;
	private String model;
	private String diagramId;
	public String getVersion() {
		return version;
	}
	public WorkflowDaoModel setVersion(String version) {
		this.version = version;
		return this;
	}
	public String getModel() {
		return model;
	}
	public WorkflowDaoModel setModel(String model) {
		this.model = model;
		return this;
	}
	public String getDiagramId() {
		return diagramId;
	}
	public void setDiagramId(String diagramId) {
		this.diagramId = diagramId;
	}

}
