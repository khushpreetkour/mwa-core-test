package com.sony.pro.mwa.service.activity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.rc.MWARC_GENERIC;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;


@JsonIgnoreProperties(ignoreUnknown=true)
@ApiModel(value="Detailed status of the activity instance", description="Activity instance status. Includes ")
public class StatusDetails {
	
	public StatusDetails() {
		this(null, null, null, null);
	}
	public StatusDetails(String deviceResponse, String deviceResponseDetails) {
		this(MWARC.DEVICE_ERROR.name(), MWARC.DEVICE_ERROR.code(), deviceResponse, deviceResponseDetails);
	}
	public StatusDetails(String rcName, String rcCode, String deviceResponse, String deviceResponseDetails) {
		if (rcName != null && rcCode != null) { 
			if (MWARC.checkGroup(rcCode)) { 
				this.result = MWARC.valueOfByCode(rcCode);
			} else {
				this.result = new MWARC_GENERIC(rcName, rcCode);
			}
		} else if (rcCode != null) {
			this.result = new MWARC_GENERIC(rcCode, rcCode);
		} else {
			this.result = null;
		}
		this.deviceResponse = deviceResponse;
		this.deviceResponseDetails = deviceResponseDetails;
	}
	
	IMWARC	result;
	String	deviceResponse;
	String	deviceResponseDetails;
	
	//v3に向けてこれをいれたい
	@JsonProperty("code") @XmlAttribute(name="code")
	public String getCode() {
		return (this.result != null) ? this.result.code() : null;
	}
	@ApiModelProperty(dataType="MWARC", value = "Return code of activity instance.")	
	@JsonIgnore @XmlTransient//v3に向けてこれをいれたい
	public IMWARC getResult() {
		return result;
	}
	public void setResult(IMWARC result) {
		this.result = result;
	}
	@ApiModelProperty(value = "Device specific response code is returned if actvitiy plugin support it.")	
	public String getDeviceResponse() {
		return deviceResponse;
	}
	public void setDeviceResponse(String deviceResponse) {
		this.deviceResponse = deviceResponse;
	}
	@ApiModelProperty(value = "Device specific response message is returned if actvitiy plugin support it.")	
	public String getDeviceResponseDetails() {
		return deviceResponseDetails;
	}
	public void setDeviceResponseDetails(String deviceResponseDetails) {
		this.deviceResponseDetails = deviceResponseDetails;
	}
}
