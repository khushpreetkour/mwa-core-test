package com.sony.pro.mwa.model.provider;

import com.sony.pro.mwa.common.utils.UUIDUtils;
import com.sony.pro.mwa.service.provider.IEndpointType;

public class ServiceEndpointTypeModel implements IEndpointType {
	String id;
	String name;
	String providerTypeId;
	
	public ServiceEndpointTypeModel() {}
	public ServiceEndpointTypeModel(String name, String providerTypeId) {
		this.id = UUIDUtils.generateUUID(name, providerTypeId);
		this.name = name;
		this.providerTypeId = providerTypeId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProviderTypeId() {
		return providerTypeId;
	}

	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId;
	}
}
