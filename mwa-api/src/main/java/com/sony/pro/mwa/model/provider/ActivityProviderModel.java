package com.sony.pro.mwa.model.provider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value="Activity provider model", description="Activity provider is an actual executor of activity, this model represents one of it.")
public class ActivityProviderModel {
	String id;
	String name;
	Boolean active; 
	Boolean exposure;
	String providerTypeName;	//★V3でtypeNameに修正したい・・・
	String providerModelNumber;	//★V3でmodelNumberに修正したい・・・
	Map<String, Object> properties;
	Map<String, ServiceEndpointModel> endpoints;
	List<LocalPerspectiveModel> localPerspectives;
	
	Long createTime;
	Long updateTime;

	public ActivityProviderModel(){
		localPerspectives = new ArrayList<>();
	}
	public ActivityProviderModel(ActivityProviderModel model){
		this.id = model.getId();
		this.name = model.getName();
		this.active = model.getActive();
		this.exposure = model.getExposure();
		this.providerTypeName = model.getProviderTypeName();
		this.properties = model.getProperties();
		this.endpoints = model.getEndpoints();
		this.localPerspectives = model.getLocalPerspectiveList();
	}

	@ApiModelProperty(dataType = "UUID", value = "ID of the activity provider")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@ApiModelProperty(value = "Name of the activity provider")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@ApiModelProperty(value = "[T.B.D.] Active flag, if this is false, it indicates the provider can't process activity.")
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	@ApiModelProperty(value = "[T.B.D.] Exposure flag under multiple MWA configuration. It indicates the provider is exposured to another MWA and it may use the exposured provider.")
	public Boolean getExposure() {
		return exposure;
	}
	public void setExposure(Boolean exposure) {
		this.exposure = exposure;
	}
	@ApiModelProperty(value = "Type of the activity provider")
	public String getProviderTypeName() {
		return this.providerTypeName;
	}
	public void setProviderTypeName(String name) {
		this.providerTypeName = name;
	}

	public String getProviderModelNumber() {
		return this.providerModelNumber;
	}
	public void setProviderModelNumber(String providerModelNumber) {
		this.providerModelNumber = providerModelNumber;
	}

	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public Map<String, Object> getProperties() {
		return this.properties;
	}
	@XmlTransient @JsonIgnore @ApiModelProperty(hidden=true)
	public Map<String, ServiceEndpointModel> getEndpoints() {
		return this.endpoints;
	}
	
	@ApiModelProperty(required=false, value = "Property list of the activity provider. Some of additional information may be necessary to execute activity. This property preserves multi-purpose information.")
	public List<GenericPropertyModel> getPropertyList() {
		if (this.properties == null)
			return null;
		List<GenericPropertyModel> result = new ArrayList<>();
		for (Map.Entry<String, Object> entry : this.properties.entrySet()) {
			GenericPropertyModel model = new GenericPropertyModel();
			model.setId(this.getId());
			model.setPropertyName(entry.getKey());
			model.setPropertyValue((String)entry.getValue());
			result.add(model);
		}
		return result;
	}
	public void setPropertyList(List<GenericPropertyModel> properties) {
		this.properties = new HashMap<>();
		if (properties != null) {
			for (GenericPropertyModel model : properties) {
				this.properties.put(model.getPropertyName(), model.getPropertyValue());
			}
		}
	}

	@ApiModelProperty(required=false, value = "Endpoint list of the activity provider. Some of endpoint access may be necessary to execute activity. (e.g. endpoints for job service, user management, etc)")
	public List<ServiceEndpointModel> getEndpointList() {
		if (this.endpoints != null) 
			return new ArrayList<ServiceEndpointModel>(this.endpoints.values());
		else
			return null;
	}
	public void setEndpointList(List<ServiceEndpointModel> endpoints) {
		this.endpoints = new HashMap<>();
		if (endpoints != null) {
			for (ServiceEndpointModel model : endpoints) {
				this.endpoints.put(model.getEndpointType(), model);
			}
		}
	}
	
	@ApiModelProperty(required=false, value = "Local perspective list of the activity provider. This is for path resolver and it express unique path resolver logic only for the provider.")
	public List<LocalPerspectiveModel> getLocalPerspectiveList() {
		// TODO Auto-generated method stub
		return this.localPerspectives;
	}

	public void setLocalPerspectiveList(List<LocalPerspectiveModel> localPerspectives) {
		// TODO Auto-generated method stub
		this.localPerspectives = localPerspectives;
	}

	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}
}
