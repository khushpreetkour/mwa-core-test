package com.sony.pro.mwa.model.resource;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.Converter;
import com.wordnik.swagger.annotations.ApiModelProperty;

//Longに固定しておきます。Genericsのままだと四則演算が使えなかったり、そもそも使う側が面倒なので
//public class CostUnitModel<T extends Number> extends Number implements Map.Entry<IResourceType, T> {
@XmlRootElement
public class CostBase<T extends CostBase<T>> {
	protected String type;
	protected Map<String, Long> values;

	public T setValues(Map<String, Long> values) {
		this.values = values;
		return (T) this;
	}
	
	@XmlElement
	public String getType() {
		return this.type;
	}

	public T setType(String type) {
		this.type = type;
		return (T) this;
	}

	@XmlElement
	public List<KeyValueModel> getValueList() {
		List<KeyValueModel>  result = null;
		if (this.values != null) {
			result = Converter.mapToKeyValueList(this.values);
		}
		return result;
	}

	public void setValueList(List<KeyValueModel> valueList) {

		this.values = Converter.costValueListToCostValues(valueList);
	}

	public CostBase() {
		this.values = new TreeMap<String, Long>();
	}

	public CostBase(CostBase source) {

		this.type = source.getType();
		this.values = new TreeMap<>(source.getValues());
	}

	public String toString() {
		return "{\"" + type + "\":" + values + "}";
	}

	@XmlTransient
	@JsonIgnore
	@ApiModelProperty(hidden = true)
	public Map<String, Long> getValues() {
		return this.values;
	}

	public T put(String key, Integer value) {
		return this.put(key, (value != null) ? value.longValue() : null);
	}

	public T put(String key, Long value) {
		this.values.put(key, value);
		return (T) this;
	}

	public void plus(CostBase cost) {
		if (cost != null && cost.values != null) {
			for (Map.Entry<String, Long> entry : ((Map<String, Long>) cost.values)
					.entrySet()) {

				//Java8対応　何故か Long value = entry.getValue(); で String > LongのCast落ちするため、冗長な実装にしている。
				Long value = null;
				Object obj = values.get(entry.getKey());
				if (obj != null) {
					String objStr = obj.toString();
					value = Long.parseLong(objStr);
				}

				if (value == null) {
					break;
				} else {

					//Java8対応　何故か Long value = entry.getValue(); で String > LongのCast落ちするため、冗長な実装にしている。
					Object obj2 = entry.getValue();
					String obj2Str = obj2.toString();
					Long value2 = Long.parseLong(obj2Str);

					this.values.put(entry.getKey(), value + value2);
				}
			}
		}

		return;
	}

	public void minus(CostBase cost) {
		if (cost != null && cost.values != null) {
			for (Map.Entry<String, Long> entry : ((Map<String, Long>) cost.values).entrySet()) {

				//Java8対応　何故か Long value = entry.getValue(); で String > LongのCast落ちするため、冗長な実装にしている。
				Long value = null;
				Object obj = values.get(entry.getKey());
				if (obj != null) {
					String objStr = obj.toString();
					value = Long.parseLong(objStr);
				}

				if (value == null) {
					throw new MwaError(MWARC.OPERATION_FAILED_RESOURCE_SHORTAGE);
				} else {

					//Java8対応　何故か Long value = entry.getValue(); で String > LongのCast落ちするため、冗長な実装にしている。
					Object obj2 = entry.getValue();
					String obj2Str = obj2.toString();
					Long value2 = Long.parseLong(obj2Str);

					if (value - value2 >= 0) {

						this.values.put(entry.getKey(), value - value2);
					} else {
						throw new MwaError(MWARC.OPERATION_FAILED_RESOURCE_SHORTAGE);
					}
				}
			}
		}

		return;
	}

}
