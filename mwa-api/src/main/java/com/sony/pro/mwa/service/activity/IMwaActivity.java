package com.sony.pro.mwa.service.activity;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.service.provider.IActivityProvider;

public interface IMwaActivity {

	public OperationResult operate(String ctrlType, Map<String, Object> params);
	public List<IEvent> getSupportedEventList();

	//exposure model info
	public String getId();
	public String getParentId();
	public String getName();
	public IState getStatus();
	public Integer getProgress();
	public Long getStartTime();
	public Long getEndTime();
	public Long getUpdateTime();

	public StatusDetails getStatusDetails();
	public Map<String, Object> getInputDetails();
	public Map<String, Object> getOutputDetails();

	//for Manager
	public Boolean isSync();

	public String getPersistentData();
	public void setPersistentData(String details);

	public ActivityInstanceModel getModel();
	public IActivityTemplate getTemplate();
	public IActivityProvider getProvider();
	public ICoreModules getCoreModules();
	public Long getCreateTime();
	public Map<String, Object> getProperties();
}
