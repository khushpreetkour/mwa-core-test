package com.sony.pro.mwa.service.event;

import java.util.List;

import com.sony.pro.mwa.model.event.EventCollection;
import com.sony.pro.mwa.model.event.EventModel;

public interface IEventManager {
    public void initialize();
    public void setReleaseAllEventLockFlag(boolean releaseAllEventLockFlag);
	public EventModel addEvent(EventModel model);
	public EventModel updateEvent(EventModel model);
	public EventModel deleteEvent(String id);
	public EventCollection deleteEvents(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public EventModel getEvent(String id);
	public EventCollection getEvents(List<String> sorts, List<String> filters, Integer offset, Integer limit);
    public Boolean lockEvent(String exclusionId);
    public Boolean unlockEvent(String exclusionId);
}
