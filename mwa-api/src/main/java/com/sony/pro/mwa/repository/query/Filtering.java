package com.sony.pro.mwa.repository.query;

import com.sony.pro.mwa.enumeration.FilterOperatorEnum;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 5:59
 * To change this template use File | Settings | File Templates.
 */
public class Filtering {
    private String key;
    private String innerKey;
    private FilterOperatorEnum operator;
    private String value;

    public Filtering() {}

    public Filtering(String key, FilterOperatorEnum operator, String value) {
        this.key = key;
        this.operator = operator;
        this.value = value;
    }

    public Filtering(String key, String innerKey, FilterOperatorEnum operator, String value) {
        this.key = key;
        this.innerKey = innerKey;
        this.operator = operator;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getInnerKey() {
        return innerKey;
    }

    public void setInnerKey(String innerKey) {
        this.innerKey = innerKey;
    }

    public FilterOperatorEnum getOperator() {
        return operator;
    }

    public void setOperator(FilterOperatorEnum operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
