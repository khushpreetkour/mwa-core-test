package com.sony.pro.mwa.model.kbase;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

public class ParameterTypeCollection extends Collection<ParameterTypeModel> {
	public ParameterTypeCollection() {
		super();
	}

	public ParameterTypeCollection(List<ParameterTypeModel> models) {
		super(models);
	}

	public ParameterTypeCollection(List<ParameterTypeModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public ParameterTypeCollection(Long totalCount, Long count, List<ParameterTypeModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ParameterTypeModel> getModels() {
		return super.getModels();
	}
}
