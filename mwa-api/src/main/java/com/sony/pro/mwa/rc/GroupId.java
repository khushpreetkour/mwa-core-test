package com.sony.pro.mwa.rc;

public enum GroupId {
	COMMON		("0000"),
	HTTP		("0801"),
	VIVO		("1101"),
	TDS_EXPORT	("1102"),
	EMAIL_NOTIFICATION		("1103"),
	GENERIC_FILEMANAGEMENT	("1104"),
	RECORDER	("1200"),
	EXTERNAL	("E000"),
	SYSTEM		("FFFF"),
	;
	
	String groupId;
	
	private GroupId(String id) {
		this.groupId = id;
	}
	
	public String id() { return this.groupId; }
}
