package com.sony.pro.mwa.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "keyValue")
public class KeyValueCollection extends Collection<KeyValueModel> {

	public KeyValueCollection() {
		super();
	}

	public KeyValueCollection(List<KeyValueModel> models) {
		super(models);
	}

	public KeyValueCollection(List<KeyValueModel> models,
			List<ErrorModel> errors) {
		super(models, errors);
	}

	public KeyValueCollection(Long totalCount, Long count,
			List<KeyValueModel> models) {
		super(totalCount, count, models);
	}

	@Override
	@XmlElement(name = "keyValue")
	public List<KeyValueModel> getModels() {
		return super.getModels();
	}
}
