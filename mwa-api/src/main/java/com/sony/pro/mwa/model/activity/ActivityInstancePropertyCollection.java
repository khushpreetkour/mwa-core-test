package com.sony.pro.mwa.model.activity;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value="Collection of activity instance property models", description="Activity instance property is a execution unit of activity, this model represents one instance of it.")
public class ActivityInstancePropertyCollection extends Collection<ActivityInstancePropertyModel> {
    public ActivityInstancePropertyCollection() {
        super();
    }

    public ActivityInstancePropertyCollection(List<ActivityInstancePropertyModel> models) {
        super(models);
    }

    public ActivityInstancePropertyCollection(List<ActivityInstancePropertyModel> models, List<ErrorModel> errors) {
        super(models, errors);
    }

    public ActivityInstancePropertyCollection(Long totalCount, Long count, List<ActivityInstancePropertyModel> models) {
        super(totalCount, count, models);
    }

    @Override
	@ApiModelProperty(value = "Obtained models as get or search result.")
    public List<ActivityInstancePropertyModel> getModels() {
        return super.getModels();
    }
}
