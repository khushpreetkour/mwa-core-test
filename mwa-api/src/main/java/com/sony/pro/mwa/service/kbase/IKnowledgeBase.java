package com.sony.pro.mwa.service.kbase;

import com.sony.pro.mwa.model.activity.ActivityTemplateCollection;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupDetailModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileModel;
import com.sony.pro.mwa.model.kbase.ExtensionCollection;
import com.sony.pro.mwa.model.kbase.ExtensionModel;
import com.sony.pro.mwa.model.kbase.ParameterSetCollection;
import com.sony.pro.mwa.model.kbase.ParameterSetModel;
import com.sony.pro.mwa.model.kbase.ParameterTypeCollection;
import com.sony.pro.mwa.model.kbase.ParameterTypeModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDetailModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramCollection;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramDetailModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModelCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.provider.ActivityProviderType;

import java.util.List;
import java.util.Map;

public interface IKnowledgeBase {
    public void storeJarFile(String bundleFilePath);
    public OperationResult loadPlugin(String pluginName, String pluginVersion);
    public OperationResult deletePlugin(String pluginName, String pluginVersion);
    public ExtensionCollection getPlugins(List<String> sorts, List<String> filters, Integer offset, Integer limit);
    public ExtensionCollection getBpmns(List<String> sorts, List<String> filters, Integer offset, Integer limit);
    public ExtensionCollection getRules(List<String> sorts, List<String> filters, Integer offset, Integer limit);
    public ExtensionCollection getExtensions(List<String> sorts, List<String> filters, Integer offset, Integer limit);
    public ExtensionModel getExtension(String id);
    public ActivityTemplateModel getActitvityTemplate(String templateName, String templateVersion);
    public IActivityTemplate getActitvityTemplateService(String id);
    public IActivityTemplate getActitvityTemplateService(String templateName, String templateVersion);
    public ActivityTemplateModel getActitvityTemplate(String id);
    public ActivityTemplateCollection getActitvityTemplates(List<String> sorts, List<String> filters, Integer offset, Integer limit);
    public List<IActivityTemplate> getActitvityTemplatesFull(List<String> sorts, List<String> filters, Integer offset, Integer limit);

    public ActivityProviderType getActitvityProviderTypeService(String providerTypeId);
    public ActivityProviderTypeModel getActitvityProviderType(String providerTypeId);
    public ActivityProviderTypeCollection getActivityProviderTypes(List<String> sorts, List<String> filters, Integer offset, Integer limit);

    public void loadPlugins();
    public void loadBpmns();
    public void loadRules();

    public ResourceRequestModel extractResourceRequest(String templateName, String templateVersion, String activityType, Map<String, Object> parameters);

	public ParameterSetModel getParameterSet(String parameterSetId);
	public ParameterSetCollection getParameterSets(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ParameterSetModel registerParameterSet(ParameterSetModel parameterSet);
	public ParameterSetModel updateParameterSet(ParameterSetModel parameterSet);
	public ParameterSetModel deleteParameterSet(String parameterSetId);
	public ParameterSetCollection deleteParameterSets(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	public ActivityProfileGroupDetailModel getActivityProfileGroup(String activityProfileGroupId);
	public ActivityProfileGroupCollection getActivityProfileGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ActivityProfileGroupModel registerActivityProfileGroup(ActivityProfileGroupModel activityProfileGroup);
	public ActivityProfileGroupModel updateActivityProfileGroup(ActivityProfileGroupModel activityProfileGroup);
	public ActivityProfileGroupCollection deleteActivityProfileGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ActivityProfileModel getActivityProfile(String activityProfileId);
	public ActivityProfileCollection getActivityProfiles(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ActivityProfileModel registerActivityProfile(ActivityProfileModel activityProfile);
	public ActivityProfileModel updateActivityProfile(ActivityProfileModel activityProfile);
	public ActivityProfileCollection deleteActivityProfiles(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ParameterTypeModel registerParameterType(ParameterTypeModel parameterType);
	public ParameterTypeCollection getParameterTypes(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ParameterTypeModel getParameterType(String id);
	public ParameterTypeModel getParameterTypeByName(String name);

	/**
	 * WorkflowDiagramModelを取得する
	 *
	 * @param workflowDiagramId 取得するWorkflowDiagramModelのID
	 * @return 取得したWorkflowDiagramModel
	 */
	public WorkflowDiagramDetailModel getWorkflowDiagram(String workflowDiagramId);

	/**
	 * WorkflowDiagramModelの一覧を取得する。
	 *
	 * @param sorts ソート条件
	 * @param filters フィルター条件
	 * @param offset オフセット
	 * @param limit 取得する数の上限
	 * @return 取得したWorkflowDiagramModelの一覧
	 */
	public WorkflowDiagramCollection getWorkflowDiagrams(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	/**
	 * WorkflowDiagramModelを新規に追加する
	 *
	 * @param workflowDiagramModelJson WorkflowModelDiagramのJson
	 * @return 作成したWorkflowDiagramModel
	 */
	public WorkflowDiagramModel registerWorkflowDiagram(String workflowDiagramModelJson);

	/**
	 * WorkflowDiagramModelの更新
	 *
	 * @param workflowDiagramId 更新するWorkflowDiagramModel
	 * @param workflowDiagramModelJson パラメータ
	 * @return 更新したWorkflowDiagramModel
	 */
	public WorkflowDiagramModel updateWorkflowDiagram(String workflowDiagramId, String workflowDiagramModelJson);

	/**
	 * WorkflowDiagramModelの一括削除
	 *
	 * @param sorts ソート条件
	 * @param filters フィルター条件
	 * @param offset オフセット
	 * @param limit 取得する数の上限
	 * @return 削除したWorkflowDiagramModelの一覧
	 */
	public WorkflowDiagramCollection deleteWorkflowDiagram(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	/**
	 * WorkflowDiagramの実行
	 *
	 * @param operation 実行されるオペレーション
	 * @param workflowDiagramId 実行されるWorkflowDiagramModelのID
	 * @return 実行されたWorkflowDiagramModel
	 */
	public WorkflowDiagramModel operateWorkflowDiagram(String operation, String workflowDiagramId);

    /**
     * workflowテーブルの内容を削除する。
     */
    public WorkflowModelCollection deleteWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit);

    /**
     * workflowテーブルの内容を列挙して取得する。
     */
    public WorkflowModelCollection getWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit);

    /**
     * workflowテーブルの内容を取得する。
     */
    public WorkflowDetailModel getWorkflow(String workflowId);

    /**
     * WorkflowModelの登録と登録したWorkflowModelのBPMN変換
     *
     * @param workflowModelJson 登録されるWorkflowModelのJson
     * @return
     */
    public WorkflowModel registerWorkflow(String workflowModelJson, String workflowDiagramId);

    /**
     * workflowテーブルの内容を更新する。
     */
    public WorkflowModel updateWorkflow(String workflowId, String workflowModelJson);
}
