package com.sony.pro.mwa.exception;

import com.sony.pro.mwa.rc.IMWARC;

public class MwaInstanceError extends MwaError {

	public MwaInstanceError(IMWARC mwarc) {
		super(mwarc);
	}
	public MwaInstanceError(IMWARC mwarc, String deviceErrorCode, String details) {
		super(mwarc, deviceErrorCode, details);
	}
	public MwaInstanceError(IMWARC mwarc, MwaError e) {
		super(mwarc, e);
	}
	
	//instance of で確認でもいい気がするがとりあえず。
	@Override
	public boolean isInstanceError() {
		return true;
	}
}
