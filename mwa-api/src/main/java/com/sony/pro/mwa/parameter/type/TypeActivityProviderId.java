package com.sony.pro.mwa.parameter.type;

import com.sony.pro.mwa.parameter.IParameterType;


@Deprecated
public class TypeActivityProviderId extends TypeString {

/*	@Override
	public boolean isValidImpl(String value) {
		//DeviceManagerに有無を問い合わせ、またTypeが適切かもチェックした方がいいかも（TypeBatonを定義して）
		//静的に決まるものもあれば、動的に接続可否が決まるものもある。
		//Capabilityについて要検討。Baton1はFullQCできるが、Baton2は制限付きライセンスでMXFしかQCできない、など。
		
		//★ここではDeviceの接続可否ロジックは持たせない（TypeBatonなどは定義しない）。Resourceである旨のみチェックをし、詳細な接続可否はDeviceとTemplateに任せるべき
		return value instanceof String;
	}*/
	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeActivityProviderId;
	}
	
	static TypeActivityProviderId instance = new TypeActivityProviderId();
	public static TypeActivityProviderId instance() {return instance;}
}
