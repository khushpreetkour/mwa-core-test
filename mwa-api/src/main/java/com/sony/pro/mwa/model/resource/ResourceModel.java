package com.sony.pro.mwa.model.resource;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sony.pro.mwa.service.resource.IResource;

//★Model中に誰がどれくらい使用しているかを入れるべき（かならずしもHolderがいないため）
@JsonIgnoreProperties(ignoreUnknown=true)
@XmlRootElement
public class ResourceModel extends CostBase<ResourceModel> implements IResource {
	String id;
	String parentId;
	String name;
	String holder;
	String holderType;
	Long createTime;
	Long updateTime;

	public ResourceModel() {
	}
	public ResourceModel(String type, String name) {
		super();
		this.type = type;
		this.name = name;
	}
	public ResourceModel(ResourceModel base) {
		super(base);
		this.id = base.getId();
		this.type = base.getType();
		this.name = base.getName();
		this.holder = base.getHolder();
		this.holderType = base.getHolderType();
		this.createTime = base.getCreateTime();
		this.updateTime = base.getUpdateTime();
	}

	public String getName() {
		return name;
	}
	public ResourceModel setName(String name) {
		this.name = name;
		return this;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public String getHolderType() {
		return holderType;
	}
	public void setHolderType(String holderType) {
		this.holderType = holderType;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public Long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

/*	public void add(ICost<Long> cost) {
		this.setValue(this.getValues() + cost.getValue());
		return;
	}
*/
	//ToDo: 使用量or使用者をどう管理するか
/*
	public void add(CostModel cost) {
		if (this.type.equals(cost.getKey())) {
			this.value = this.value + cost.getValue();
		}
		return;
	}
	*/
}
