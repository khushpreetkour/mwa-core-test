package com.sony.pro.mwa.repository.query;

import java.security.Principal;
import java.util.List;

import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.Sorting;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 6:36
 * To change this template use File | Settings | File Templates.
 */
public class QueryCriteria {
    private List<Sorting> sortings;
    private List<List<Filtering>> filterings;
    private long offset = -1;
    private long limit = -1;
    private Principal principal;

    public QueryCriteria() {}

    public QueryCriteria(List<Sorting> sortings, List<List<Filtering>> filterings, long offset, long limit, Principal principal) {
        this.sortings = sortings;
        this.filterings = filterings;
        this.offset = offset;
        this.limit = limit;
        this.principal = principal;
    }

    public List<Sorting> getSortings() {
        return sortings;
    }

    public void setSortings(List<Sorting> sortings) {
        this.sortings = sortings;
    }

    public List<List<Filtering>> getFilterings() {
        return filterings;
    }

    public void setFilterings(List<List<Filtering>> filterings) {
        this.filterings = filterings;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }
}