package com.sony.pro.mwa.model;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value="Generic property", description="Typical property model")
public class GenericPropertyBase {
	String name;
	String value;

	@ApiModelProperty(value = "Name of the property")	
	public String getPropertyName() {
		return name;
	}
	public void setPropertyName(String name) {
		this.name = name;
	}
	@ApiModelProperty(value = "Value of the property")	
	public String getPropertyValue() {
		return value;
	}
	public void setPropertyValue(String value) {
		this.value = value;
	}
}
