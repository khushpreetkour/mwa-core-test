package com.sony.pro.mwa.model.event;

import java.util.Map;

import com.sony.pro.mwa.enumeration.event.EventPriority;
import com.wordnik.swagger.annotations.ApiModel;

@ApiModel(value="Event model", description="")
public class EventModel {
	String id;
	String name;
	Map<String, Object> params;
	
	String targetId;
	String targetType;
	
	Long createTime;
	String exclusionId;
	Integer priority = EventPriority.DEFAULT.getValue();

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, Object> getParams() {
		return params;
	}
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	public String getTargetId() {
		return targetId;
	}
	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}
	public String getTargetType() {
		return targetType;
	}
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	public Long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}
	public String getExclusionId() {
		return exclusionId;
	}
	public void setExclusionId(String exclusionId) {
		this.exclusionId = exclusionId;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
}
