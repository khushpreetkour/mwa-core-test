package com.sony.pro.mwa.model.scheduler;

public class CronTriggerInfoModel extends TriggerInfoModel{
	
	private String cronExpression;
	
	private String timeZone;
	
	private String triggerState;
	
	private Long startTime;
	
	private Long endTime;
	
	private Long nextFireTime;

	public String getCronExpression()
	{
		return cronExpression;
	}

	public void setCronExpression(String cronExpression)
	{
		this.cronExpression = cronExpression;
	}

	public String getTimeZone()
	{
		return timeZone;
	}

	public void setTimeZone(String timeZone)
	{
		this.timeZone = timeZone;
	}

	public String getTriggerState()
	{
		return triggerState;
	}

	public void setTriggerState(String triggerState)
	{
		this.triggerState = triggerState;
	}

	public Long getStartTime()
	{
		return startTime;
	}

	public void setStartTime(Long startTime)
	{
		this.startTime = startTime;
	}

	public Long getEndTime()
	{
		return endTime;
	}

	public void setEndTime(Long endTime)
	{
		this.endTime = endTime;
	}

	public Long getNextFireTime()
	{
		return nextFireTime;
	}

	public void setNextFireTime(Long nextFireTime)
	{
		this.nextFireTime = nextFireTime;
	}
	
	

}
