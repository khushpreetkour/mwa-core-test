package com.sony.pro.mwa.service.activity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sony.pro.mwa.model.StateModel;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Stateの属性定義用I/F
 * 本I/Fを実装することで新しいStateを定義可能。また、ここでの属性定義に基づいて上位がハンドルする。
 * あまりやみくもに属性を増やすとState追加が大変になるので、考慮して追加すべし。
 * ☆DB上にState情報を保持する場合、検索を容易にするためこのあたりのフラグ情報をフィールドで用意する
 * @version 0.1
 * @since 0.1
 */
@JsonDeserialize(as=StateModel.class)
@ApiModel(value="State of activity instance", description="Activity developer can design unique state for specific activity."
		+ "And the state attribute is represented by flags and MWA core handle instance based on the flags of the state.")
public interface IState {
	/**
	 * Stateの名前を返す。用途はユニークID（なので将来的には被るのを回避することも考慮が必要か。。）
	 * 当座プレゼンテーション用にも使う想定で、UpperCamelCaseを推奨。特にこだわりなければクラス名で（多言語やるならこのIDから引くイメージ）
	 * 
	 * @param N/A
	 * @return String 本Stateの表示名。
	 */
	@ApiModelProperty(value = "Name of the activity state")	
	public String getName();

	/**
	 * State定義メソッド：終端状態
	 * これ以上、状態遷移をしない。上位のJobManagerの実行管理から外される。
	 * 
	 * @param N/A
	 * @return 終端状態ならtrue
	 */
	@ApiModelProperty(value = "Terminated flag of the state. If this state indicates terminated (= activity instance is terminated), it should be \"true\".")	
	public boolean isTerminated();

	/**
	 * State定義メソッド：安定状態
	 * 勝手に状態遷移が起こることはなく、ユーザなど上位からのオペレーション待ち（QueuedやPaused状態などを想定）
	 * Pollingの対象から外れる（方向で検討中、Device側で勝手に状況が変わることもあるのでPollingした方がいいか・・？）、リソースがアサインされていた場合リソースを手放す、
	 * 
	 * @param N/A
	 * @return 安定状態ならtrue
	 */
	@ApiModelProperty(value = "Stable flag of the state. "
			+ "If it's \"true\", it indicates state doesn't change without external event such as user operation, device callback, etc."
			+ "So MWA core doesn't do polling status of the activity instance")
	public boolean isStable();

	/**
	 * State定義メソッド：エラー状態
	 * Failedなどを想定。cancelなど操作のエラーではなく、リソース上でJobがエラーしたような状況を想定。
	 * 上位がエラーハンドリングなどで利用する予定。詳細は検討中。
	 * 
	 * @param N/A
	 * @return エラー状態ならtrue
	 */
	@ApiModelProperty(value = "Error flag of the state. "
			+ "If it's \"true\", it indicates activity instance becomes error."
			+ "So the error handling would be applied against the activity instance.")
	public boolean isError();

	/**
	 * State定義メソッド：キャンセル状態
	 * Cancelling/Cancelledなどを想定。cancel操作を受け付けた状態。
	 * 上位がキャンセルのハンドリングで使用（Activity InstanceがCancelledになった場合に、上位のInstanceにもCancelを発行するなど）
	 * 
	 * @param N/A
	 * @return キャンセル状態ならtrue
	 */
	@ApiModelProperty(value = "Cancel flag of the state. "
			+ "If it's \"true\", it indicates activity instance becomes cancel/cancelling/cancelled state."
			+ "So the cancel handling would be applied against the activity instance.")
	public boolean isCancel();

	//StateTransitionの確認用。チェックしてtrueなら処理して状態を遷移。今回使わないかも。
	//public boolean isValid();
	
	//Inactiveハンドリング用、Activity実行を規定回数試みても失敗が続いた場合、Inactive状態に遷移し、ユーザによる継続・中止の判断を仰ぐ。とりあえず今回は実装しない。
	//public boolean isInacitve();

	//別プロダクトの実装では、QueuedとQueudInProgressという２状態を用意していた。両者の違いはDeviceにJobが乗っているかどうか。
	//Queuedはリソースに１度もsubmitされておらず、QueudInProgressはsubmit後Pause命令などで一時的にリソースを解放した状態
	//例えばキャンセルを行う場合、後者はリソース側にもCancelを通知する必要がある。
	//上位のハンドリングという観点では、リソースに実行を依頼する際、呼び出すAPIがsubmitになるかresumeになるか、という違いがある
	//今回は、リソースとの紐づけだけで解決ができる想定なのでとりあえず本属性は使わない想定。
	//public boolean isInProgress();

	/**
	 * State定義メソッド：リソース待ち状態
	 * 処理に必要なリソースの割り当て待ち。Queudなどを想定。Pausedも似たようにリソース未割当だが、ユーザがResumeするまでリソースは必要としないためfalse。
	 * リソースマネージャはリソースに空きができた場合、リソース待ち状態のActivityにリソース割り当てを試みる
	 * 
	 * @param N/A
	 * @return リソース待ち状態ならtrue
	 */
	//public boolean isWaiting(); //リソースマネージャは外部に持つ想定でこのレイヤでは実装しない（ハンドルしない）
	
	/**
	 * State定義メソッド：リソース使用状態
	 * 処理に必要なリソースを使い実行中の状態。InProgressを想定。
	 * リソースマネージャは、より優先度の高いActivity実行のため、リソース使用状態のActivityからリソース回収（Pause/Cancelなど）を試みる
	 * 
	 * @param N/A
	 * @return リソース待ち状態ならtrue
	 */
	//public boolean isRunning(); //リソースマネージャは外部に持つ想定でこのレイヤでは実装しない（ハンドルしない）
}
