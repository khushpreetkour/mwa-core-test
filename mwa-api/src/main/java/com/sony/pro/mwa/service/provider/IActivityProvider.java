package com.sony.pro.mwa.service.provider;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.LocalPerspectiveModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;

public interface IActivityProvider {
	@XmlTransient @JsonIgnore
	public ActivityProviderModel getModel();
	public void setModel(ActivityProviderModel model);
	
	public String getId();
	public String getName();
	public Boolean getActive();
	public Boolean getExposure();
	@XmlTransient @JsonIgnore
	public String getDefaultPerspective();
	public String getProviderTypeName();
	public List<GenericPropertyModel> getPropertyList();
	public List<ServiceEndpointModel> getEndpointList();
	public List<LocalPerspectiveModel> getLocalPerspectiveList();

	@XmlTransient @JsonIgnore
	public Map<String, Object> getProperties();
	@XmlTransient @JsonIgnore
	public Map<String, ServiceEndpointModel> getEndpoints();
}
