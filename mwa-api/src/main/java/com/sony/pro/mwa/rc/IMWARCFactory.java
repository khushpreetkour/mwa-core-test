package com.sony.pro.mwa.rc;

public interface IMWARCFactory {
	public IMWARC create(String code);
	public String getGroupId();
}
