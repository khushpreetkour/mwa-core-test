package com.sony.pro.mwa.parameter;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.utils.ParameterUtils;

/**
 * Activity実行時に指定される入力パラメータのチェック処理を実装した抽象クラス
 *
 * このクラス自体はもっと汎用的な場所に移動する前提で。
 *
 * assetcopyにもほぼ同じ実装がありますが、class情報の指定の仕方が異なるので、そこはすみませんが今今は保留ということで
 *
 */
public abstract class AbsInputSequence {

	private MwaLogger logger = null;

	/**
	 * validate実行後の入力パラメータテーブルを保持する変数
	 */
	protected Map<String, Object> params = null;

	/**
	 * paramDefsの定義にしたがって、paramsの状態をチェックし、コンバート結果を返す
	 * @param paramDefs
	 * @param params
	 * @return
	 */
	protected Map<String, Object> validate(List<? extends IParameterDefinition> paramDefs, Map<String, Object> params) {
		this.params = ParameterUtils.FROM_STRING.convert(paramDefs, params);
		return this.params;
	}

	/**
	 * コンストラクタ
	 * @param clazz
	 */
	public AbsInputSequence(Class clazz) {
		this.logger = MwaLogger.getLogger(clazz);
	}

}
