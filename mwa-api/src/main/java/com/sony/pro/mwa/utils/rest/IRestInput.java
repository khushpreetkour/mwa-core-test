package com.sony.pro.mwa.utils.rest;

import java.util.Map;

public interface IRestInput {
	
	public String getHttpMethod(); 
	public void setHttpMethod(final String httpMethod); 
	public String getUrl() ;
	public void setUrl(final String url);
	public String getJsonParam() ;
	public void setJsonParam(final String jsonParam) ;
	public Map<String, String> getHttpHeadersMap();
	public void setHttpHeadersMap(Map<String, String> httpHeadersMap);
}
