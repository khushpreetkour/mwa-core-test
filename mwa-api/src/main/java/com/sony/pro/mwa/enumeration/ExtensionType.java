package com.sony.pro.mwa.enumeration;

public enum ExtensionType {
	BUNDLE,
	BPMN,
	RULE,
}
