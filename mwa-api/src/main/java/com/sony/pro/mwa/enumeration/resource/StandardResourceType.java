package com.sony.pro.mwa.enumeration.resource;

import com.sony.pro.mwa.service.resource.IResourceType;

public enum StandardResourceType implements IResourceType {
	TASK_COUNT,
	NETWORK_PORT,
	SESSION,
	BANDWIDTH,
	;

	private StandardResourceType() {
		this(Integer.class);
	}
	private StandardResourceType(Class clazz) {
		this.clazz = clazz;
	}
	public Class getClazz() {return clazz;}
	Class clazz;
}
