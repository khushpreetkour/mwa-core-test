package com.sony.pro.mwa.service.resource;

import java.util.List;

import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupModel;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.rc.MWARC;

public interface IResourceManager {
	//Resourceが確保できたら、対象のIDにNOTIFY_RESERVEDイベントを発行する
	//Resourceが確保できたら、EventReceiverのIDにNOTIFY_RESERVEDイベントを発行する
	
	public ResourceRequestModel addResourceRequest(ResourceRequestModel model);
	public ResourceRequestModel updateResourceRequest(ResourceRequestModel model);
	public ResourceRequestModel getResourceRequest(String id);
	public ResourceRequestCollection getResourceRequests(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ResourceRequestModel deleteResourceRequest(String id);
	default public ResourceRequestModel deleteResourceRequest(String id, Boolean resourceDeletionFlag) {
		return null;	//I/F変更でbuildエラーになるのを避けるため、暫定でdefault実装
	}
	public ResourceRequestCollection deleteResourceRequests(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	public ResourceModel addResource(ResourceModel model);
	public ResourceModel updateResource(ResourceModel model);
	public ResourceModel deleteResource(String id);
	@Deprecated
	default public ResourceModel deleteResourceIfNotUsed(String id) {	//internal use only
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}
	public ResourceCollection deleteResources(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ResourceModel getResource(String id);
	public ResourceCollection getResources(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	public ResourceGroupModel addResourceGroup(ResourceGroupModel model);
	public ResourceGroupModel updateResourceGroup(ResourceGroupModel model);
	public ResourceGroupModel deleteResourceGroup(String id);
	public ResourceGroupCollection deleteResourceGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	public ResourceGroupModel getResourceGroup(String id);
	public ResourceGroupCollection getResourceGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	public void tryDispatch(String threadId, String queueId, String eventType);
}
