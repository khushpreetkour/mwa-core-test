package com.sony.pro.mwa.model.activity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wordnik.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ActivityInstancePropertyModel {

	String propertyId;
	String id;
	String name;
	String value;

	public ActivityInstancePropertyModel(){};
	public ActivityInstancePropertyModel(ActivityInstancePropertyModel source){
		this.propertyId = source.getPropertyId();
		this.id = source.getId();
		this.name = source.getName();
		this.value = source.getValue();
	}

	@ApiModelProperty(position=0, dataType = "UUID", required=false, value = "Activity instance property id of activity instance property.")
	@JsonProperty("propertyId")
	public String getPropertyId() { return propertyId; }
	@JsonProperty("id")
	@ApiModelProperty(position=1, dataType = "UUID", required=false, value = "Activity instance id of activity instance.")
	public String getId() { return id; }
	@ApiModelProperty(position=2, required=false, value = "Activity instance property name of the acitivty instance property.")
	@JsonProperty("name")
	public String getName() { return name; }
	@ApiModelProperty(position=3, required=false, value = "Activity instance property value of the acitivty instance property.")
	@JsonProperty("value")
	public String getValue() { return value; }

	@JsonProperty("propertyId")
	public void setPropertyId(String propertyId) { this.propertyId = propertyId; }
	@JsonProperty("id")
	public void setId(String id) { this.id = id; }
	@JsonProperty("name")
	public void setName(String name) { this.name = name; }
	@JsonProperty("value")
	public void setValue(String value) { this.value = value; }
}
