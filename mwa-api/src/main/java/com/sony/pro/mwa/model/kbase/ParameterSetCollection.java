package com.sony.pro.mwa.model.kbase;

import java.util.List;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.ErrorModel;

@Deprecated
public class ParameterSetCollection extends Collection<ParameterSetModel> {
	public ParameterSetCollection() {
		super();
	}

	public ParameterSetCollection(List<ParameterSetModel> models) {
		super(models);
	}

	public ParameterSetCollection(List<ParameterSetModel> models, List<ErrorModel> errors) {
		super(models, errors);
	}

	public ParameterSetCollection(Long totalCount, Long count, List<ParameterSetModel> models) {
		super(totalCount, count, models);
	}

	@Override
	public List<ParameterSetModel> getModels() {
		return super.getModels();
	}
}
