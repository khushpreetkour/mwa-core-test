package com.sony.pro.mwa.parameter.type;

import com.sony.pro.mwa.parameter.IParameterType;

public class TypeLong extends TypeBase<Long> {

	@Override
	public boolean isLinkable(IParameterType type) {
		return type instanceof TypeLong;
	}

	@Override
	protected String toStringImpl(Long value) {
		return (value != null) ? value.toString() : null;
	}

	@Override
	protected Long fromStringImpl(String valueStr) {
		return Long.valueOf(valueStr);
	}
	
	public static TypeLong instance() {
		return instance;
	}
	static TypeLong instance = new TypeLong();
}
