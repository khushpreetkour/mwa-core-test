package com.sony.pro.mwa.parameter.type;

import javax.xml.bind.annotation.XmlElement;

import com.sony.pro.mwa.parameter.IParameterType;


public abstract class TypeBase<T> implements IParameterType {

	//この名前で一意に決まる

    @XmlElement(name="name")
	public String name() {return this.getClass().getSimpleName();}

	@Override
	public String toString(Object value) {
		return toStringImpl((T) value);
	}
/*	@Override
	public boolean isValid(Object value) {
		return isValidImpl((T)value);
	}*/
	@Override
	public Object fromString(String valueStr) {
		return (T)fromStringImpl(valueStr);
	}
	
	protected abstract String toStringImpl(T value);
	//protected abstract boolean isValidImpl(T value);
	protected abstract T fromStringImpl(String valueStr);
}
