package com.sony.pro.mwa.storage.repository;

import com.sony.pro.mwa.model.storage.LocationCollection;
import com.sony.pro.mwa.model.storage.LocationModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface LocationDao extends ModelBaseDao<LocationModel, LocationCollection> {
}
