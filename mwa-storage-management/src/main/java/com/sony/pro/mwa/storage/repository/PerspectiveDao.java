package com.sony.pro.mwa.storage.repository;

import com.sony.pro.mwa.model.storage.PerspectiveCollection;
import com.sony.pro.mwa.model.storage.PerspectiveModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface PerspectiveDao extends ModelBaseDao<PerspectiveModel, PerspectiveCollection> {
	//検索用。登録更新削除といった処理はLocationDAOか、ProviderDAOで行う
}
