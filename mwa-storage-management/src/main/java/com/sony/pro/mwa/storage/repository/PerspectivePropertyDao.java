package com.sony.pro.mwa.storage.repository;

import com.sony.pro.mwa.model.storage.PerspectivePropertyCollection;
import com.sony.pro.mwa.model.storage.PerspectivePropertyModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface PerspectivePropertyDao extends ModelBaseDao<PerspectivePropertyModel, PerspectivePropertyCollection> {
	//検索用。登録更新削除といった処理はLocationDAOか、ProviderDAOで行う
	public void deleteModelsByPerspective(String locationId, String perspective);
	public void deleteModelsByLocation(String locationId);
}
