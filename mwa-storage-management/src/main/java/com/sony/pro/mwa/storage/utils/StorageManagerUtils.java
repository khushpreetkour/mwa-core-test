package com.sony.pro.mwa.storage.utils;

import com.sony.pro.mwa.enumeration.StandardPerspective;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.MWARC;

import java.net.InetAddress;
import java.util.regex.Matcher;

public class StorageManagerUtils {

    /**
     * MwaPathを取得します
     * @param name DirectoryModelの名前
     * @return MwaPath
     * */
    public static String getMwaPath(String name) {
        if (name == null || name.isEmpty()) {
            throw new MwaError(MWARC.INVALID_INPUT);
        }
        return "mwa://" + name;
    }

    /**
     * WindowsパスをUNCパスに変換します
     * @param filePath Windowsパス
     * @return UNCパス
     * */
    public static String convertFromWindowsToUnc(String filePath) {
        Matcher m = StandardPerspective.WINDOWS.getPattern().matcher(filePath);
        if (m.matches()) {
            filePath = "\\\\" + getIpAddress() + filePath.substring(filePath.indexOf("\\"));
        }
        return filePath;
    }

    /**
     * IPアドレスを取得します
     * @return IPアドレス
     * */
    public static String getIpAddress() {
        String ipAddress = "";
        try {
            ipAddress = InetAddress.getLocalHost().getHostAddress();
        } catch(Exception e ) {
            throw new MwaError(MWARC.CONNECTION_HOST_NOT_FOUND);
        }
        return ipAddress;
    }
}
