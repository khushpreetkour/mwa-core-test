package com.sony.pro.mwa.storage;

import java.io.File;
import java.util.*;

import com.sony.pro.mwa.enumeration.StandardPerspective;
import com.sony.pro.mwa.model.storage.*;
import com.sony.pro.mwa.storage.utils.StorageManagerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.GenericPropertyBase;
import com.sony.pro.mwa.model.UriModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.service.storage.IPerspective;
import com.sony.pro.mwa.service.storage.IStorageManager;
import com.sony.pro.mwa.service.storage.IUriResolver;
import com.sony.pro.mwa.storage.repository.PerspectiveDao;
import com.sony.pro.mwa.storage.repository.LocationDao;
import com.sony.pro.mwa.storage.repository.PerspectivePropertyDao;
import com.sony.pro.mwa.utils.UriUtils;

@Component
public class StorageManagerImpl implements IStorageManager, IUriResolver {
	private final static MwaLogger logger = MwaLogger.getLogger(StorageManagerImpl.class);

	protected LocationDao locationDao;
	protected PerspectiveDao perspectiveDao;
	protected PerspectivePropertyDao perspectivePropertyDao;

	@Autowired
	public void setLocationDao(LocationDao dao) {
		this.locationDao = dao;
	}
	public LocationDao getLocationDao() {
		return this.locationDao;
	}

	@Autowired
	public void setPerspectiveDao(PerspectiveDao dao) {
		this.perspectiveDao = dao;
	}
	public PerspectiveDao getPerspectiveDao() {
		return this.perspectiveDao;
	}

	@Autowired
	public void setPerspectivePropertyDao(PerspectivePropertyDao dao) {
		this.perspectivePropertyDao = dao;
	}
	public PerspectivePropertyDao getPerspectivePropertyDao() {
		return this.perspectivePropertyDao;
	}

	public void initialize() {

	}
	public void destroy() {

	}

	//System Setting用
	public LocationCollection getLocations(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		if (sorts == null) {
			sorts = Arrays.asList("createTime" + "+");
		}

		LocationCollection models = this.locationDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		if (models != null) {
			for (LocationModel model : models.getModels()) {
				model = setLocationModelDetails(model);
			}
		}
		return models;
	}
	public LocationModel getLocation(String locationId) {
		LocationModel model = this.locationDao.getModel(locationId, null);
		if (model == null)
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);

		model = setLocationModelDetails(model);
		return model;
	}

	public LocationModel getLocationByName(String locationName) {
		List<String> filter = new ArrayList<>();
		filter.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + locationName);
		LocationModel model = this.locationDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filter, null, null, null)).first();
		if (model == null)
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);

		return getLocation(model.getId());
	}

	@Override
	public DirectoryCollection getDirectories(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		LocationCollection locationCollection = this.locationDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		List<DirectoryModel> directoryModels = new ArrayList<>();
		for (LocationModel locationModel : locationCollection.getModels()) {
			String mwaPath = StorageManagerUtils.getMwaPath(locationModel.getName()) + "/";
			directoryModels.add(createDirectoryModel(mwaPath, locationModel.getName(), getNativePath(mwaPath), new ArrayList<>()));
		}
		DirectoryCollection directoryCollection = createDirectoryCollection(directoryModels);
		return directoryCollection;
	}

	@Override
	public DirectoryModel getDirectory(String mwaPath) {
		String removedMwa = removeMwaPhrase(mwaPath);
		String baseDirectoryName = removedMwa;
		String selectDirectoryName = removedMwa;

		if (removedMwa.indexOf("/") != -1) {
            baseDirectoryName = removedMwa.substring(0, removedMwa.indexOf("/"));
        }
		if (!(checkExistLocationModel(baseDirectoryName))) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION, null, "Not found the specified directory.");
		};

        if (selectDirectoryName.indexOf("/") != -1) {
            if(selectDirectoryName.endsWith("/")) {
                selectDirectoryName = selectDirectoryName.substring(0, selectDirectoryName.length() -1);
            }
            selectDirectoryName = selectDirectoryName.substring(selectDirectoryName.lastIndexOf("/") + 1, selectDirectoryName.length());
        }

        DirectoryModel directoryModel = createDirectoryModel(mwaPath, selectDirectoryName, this.getNativePath(mwaPath), new ArrayList<>());
		File file = new File(directoryModel.getNativePath());
		File[] lowerPaths = file.listFiles();
		if (lowerPaths != null) {
			String name;
			String uri;
			String path;

			for (File lowerPath : lowerPaths) {
				name = lowerPath.getName();
				uri = String.format("%s%s", mwaPath, name);
				path = this.getNativePath(uri);

				if (lowerPath.isDirectory()) {
					directoryModel.getDirectories().add(createDirectoryModel(uri + "/", name, path, new ArrayList<>()));
				} else {
					directoryModel.getFiles().add(this.createFileModel(uri, name, path));
				}
			}
		}
		return directoryModel;
	}

	/**
	 * DirectoryModelを生成します
	 * @param id id
	 * @param name 名前
	 * @param nativePath 実際のファイルパス
	 * @param directoryList 子階層のList
	 * @return DirectoryModel
	 * */
	private DirectoryModel createDirectoryModel(String id, String name, String nativePath, List<DirectoryModel> directoryList) {
		DirectoryModel directoryModel = new DirectoryModel();
		directoryModel.setId(id);
		directoryModel.setName(name);
		directoryModel.setUri(id);
		directoryModel.setNativePath(nativePath);
		directoryModel.setDirectories(directoryList);
		return directoryModel;
	}

	/***
	 * FileModelのインスタンスを生成する
	 * @param id id/uriに設定する値
	 * @param name filenameに設定する値
	 * @param nativePath このFileModelで示されるファイルの物理パス
	 * @return
	 */
	private FileModel createFileModel(String id, String name, String nativePath) {
		FileModel model = new FileModel();
		model.setId(id);
		model.setName(name);
		model.setUri(id);
		model.setNativePath(nativePath);
		return model;
	}

	/**
	 * DirectoryCollectionを生成します
	 * @param directoryModels DirectoryCollectionに格納されるDirectoryのList
	 * @return DirectoryCollection
	 * */
	private DirectoryCollection createDirectoryCollection(List<DirectoryModel> directoryModels) {
		return new DirectoryCollection((long)directoryModels.size(), null, directoryModels);
	}

	/**
	 * MwaPathから"mwa://"を消去します
	 * @param mwaPath Mwaパス
	 * @return MwaPathから"mwa://"が消去された文字列
	 * */
	private String removeMwaPhrase(String mwaPath) {
		if (mwaPath == null || mwaPath.isEmpty()) {
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		return mwaPath.replace("mwa://", "");
	}

	/**
	 * MwaPathを基に実際のパス("UNC" or "WINDOWS")を取得します
	 * @param mwaPath Mwaパス
	 * @return 実際のパス
	 * */
	private String getNativePath(String mwaPath) {
		String nativePath = "";
		try {
			// UNCパスを優先して取得する
			nativePath = asNativePath(mwaPath, StandardPerspective.getDefaultNativePathPerspectiveArray()).getPaths().get(0);
		} catch(MwaInstanceError e) {
			// パスが取得できなかった場合は例外とせず空文字がreturnされる
			logger.warn("Cannot find a path perspective: " + StandardPerspective.getDefaultNativePathPerspectiveArray());
		}
		return nativePath;
	}

	/**
	 * LocationModelが存在するかをチェックします
	 * @param name LocationModelのName
	 * @return True or False
	 * */
    private boolean checkExistLocationModel (String name) {
        List<String> filter = new ArrayList<>();
        filter.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + name);
        LocationModel model = this.locationDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filter, null, null, null)).first();
        return model != null;
    }

	public LocationModel setLocationModelDetails(LocationModel model) {
		List<String> filter = new ArrayList<>();
		filter.add("id" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId());
		filter.add("providerId" + FilterOperatorEnum.EQUAL.toSymbol() + "");	//localPerspectiveはmodelのgetでは返さない
		List<PerspectiveModel> lpms = this.perspectiveDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filter, null, null, null)).getModels();
		model.setPerspectiveMap(createPerspectiveModelMap(lpms));

		if (model.getPerspectiveList() != null) {
			for (PerspectiveModel pm : model.getPerspectiveList()) {
				List<String> filter3 = new ArrayList<>();
				filter3.add("locationId" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId());
				filter3.add("perspective" + FilterOperatorEnum.EQUAL.toSymbol() + pm.getPerspective());
				List<PerspectivePropertyModel> properties = this.perspectivePropertyDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filter3, null, null, null)).getModels();
				pm.setPropertyList(properties);
			}
		}
		return model;
	}

	protected Map<String, PerspectiveModel> createPerspectiveModelMap(List<PerspectiveModel> models) {
		Map<String, PerspectiveModel> result = new HashMap<>();
		for (PerspectiveModel model : models) {
			result.put(model.getPerspective(), model);
		}
		return result;
	}

	public LocationModel addLocation(LocationModel model) {
		if (model.getId() == null)
			model.setId(UUID.randomUUID().toString());
		LocationModel result = this.locationDao.addModel(model);
		if (model.getPerspectiveList() != null) {
			for (PerspectiveModel pm : model.getPerspectiveList()) {
				if (pm.getPropertyList() != null) {
					for (GenericPropertyBase kvm : pm.getPropertyList()) {
						PerspectivePropertyModel ppm = new PerspectivePropertyModel();
						ppm.setLocationId(model.getId());
						ppm.setPerspective(pm.getPerspective());
						ppm.setPropertyName(kvm.getPropertyName());
						ppm.setPropertyValue(kvm.getPropertyValue());
						this.perspectivePropertyDao.addModel(ppm);
					}
				}
			}
		}
		return result;
	}
	public LocationModel updateLocation(LocationModel model) {
		LocationModel result = this.locationDao.updateModel(model);
		this.perspectivePropertyDao.deleteModelsByLocation(model.getId());
		if (model.getPerspectiveList() != null) {
			for (PerspectiveModel pm : model.getPerspectiveList()) {
				if (pm.getPropertyList() != null) {
					for (GenericPropertyBase gp : pm.getPropertyList()) {
						PerspectivePropertyModel ppm = new PerspectivePropertyModel();
						ppm.setLocationId(model.getId());
						ppm.setPerspective(pm.getPerspective());
						ppm.setPropertyName(gp.getPropertyName());
						ppm.setPropertyValue(gp.getPropertyValue());
						this.perspectivePropertyDao.addModel(ppm);
					}
				}
			}
		}
		return result;
	}
	public LocationModel deleteLocation(String locationId) {
		List<String> filter = new ArrayList<>();
		filter.add("id" + FilterOperatorEnum.EQUAL.toSymbol() + locationId);
		LocationModel model = this.locationDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filter, null, null, null)).first();
		LocationModel result = this.locationDao.deleteModel(model);
		return result;
	}

	public LocationCollection deleteLocations(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		LocationCollection models = this.locationDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		if (models != null) {
			for (LocationModel model : models.getModels()) {
				this.deleteLocation(model.getId());
			}
		}
		return models;
	}

	//System Setting用
/*	public LocationModel addProviderLocationMapping(LocationModel model) {
		return null;
	}
	public LocationModel updateProviderLocationMapping(LocationModel model) {
		return null;
	}*/

	//実処理用（Activityが主ユーザ）
	@Override
	public NativeLocationModel asNativePath(String uri, String... perspectives) {
		return asNativePathViaProvider(uri, null, perspectives);
	}
	@Deprecated
	public NativeLocationModel asNativePath(String uri, String perspective) {
		return asNativePathViaProvider(uri, null, perspective);
	}
	@Deprecated
	public NativeLocationModel asNativePath(String uri, String perspective, String providerId) {
	    String[] perspectives = {perspective, providerId};
		return asNativePath(uri, perspectives);	//asNativePath(String uri, String... perspectives)と誤って呼び出すことが多いので、providerを使わないロジックに差し替え、使いたい場合はViaProvider側を呼び出し
	}

	@Override
	public NativeLocationModel asNativePathViaProvider(String uri, String providerId, String... perspectives) {
		NativeLocationModel result = null;
		MwaError error = null;
		for (String perspective : perspectives) {
			if (perspective == null)
				continue;
			try {
				result = asNativePathCore(uri, providerId, perspective);
			} catch (MwaError e) {
				error = e;
			}
			if (result != null)
				break;
		}
		if (result == null) {
			if (error != null)
				throw error;
			else
				throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
		}
		return result;
	}

	protected NativeLocationModel asNativePathCore(String uri, String providerId, String perspective) {
		UriModel uc = UriUtils.parse(uri);
		if (uc == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_URI_FORMAT);
		}

		LocationModel model = getLocationByName(uc.getLocation());
		if (model == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
		}
		if (model.getPerspectiveMap() == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}

		PerspectiveModel perspectiveModel = null;

		if (providerId != null) {
			List<String> filter = new ArrayList<>();
			filter.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + uc.getLocation());
			filter.add("perspective" + FilterOperatorEnum.EQUAL.toSymbol() + perspective);
			//providerIdがあるか、nullの場合
			filter.add("providerId" + FilterOperatorEnum.EQUAL.toSymbol() + providerId + "," + "providerId" + FilterOperatorEnum.EQUAL.toSymbol() + "");

			List<String> sort = new ArrayList<>();
			sort.add("providerId" + "+");
			perspectiveModel = this.perspectiveDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sort, filter, null, null, null)).first();
		} else {
			perspectiveModel = model.getPerspectiveMap().get(perspective);
		}
		if (perspectiveModel == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}

		String val = UriUtils.mwa2native(uri, perspectiveModel.getBasePath());
		List<String> paths = new ArrayList<>();
		if (val != null)
			paths.add(val);
		NativeLocationModel result = new NativeLocationModel().setUser(perspectiveModel.getUser()).setPassword(perspectiveModel.getPassword()).setPaths(paths);
		return result;
	}
	public String asMwaUri(String nativePath, String location) {
		IPerspective perspective = UriUtils.checkPerspective(nativePath);
		if (perspective == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}
		//uc.g
		LocationModel model = getLocationByName(location);
		if (model == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
		}
		if (model.getPerspectiveMap() == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}
		if (model.getPerspectiveMap().get(perspective.name()) == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}
		String val = UriUtils.native2mwa(location, nativePath, model.getPerspectiveMap().get(perspective.name()).getBasePath());
		return val;
	}
	public String asMwaUri(String nativePath, String location, String providerId) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}
}
