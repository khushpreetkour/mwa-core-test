package com.sony.pro.mwa.storage.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.model.storage.PerspectiveCollection;
import com.sony.pro.mwa.model.storage.PerspectiveModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class PerspectiveQuery extends Query<PerspectiveModel, PerspectiveCollection, QueryCriteria> {
    public enum COLUMN {
        ID("i.location_id", "id", UUID.class, true),
        NAME("i.location_name", "name", String.class, true),
        PERSPECTIVE("j.location_perspective_name", "perspective", String.class, true),
        BASE_PATH("j.location_perspective_base_path", "basePath", String.class, true),
        USER("j.location_perspective_user", "user", String.class, true),
        PASSWORD("j.location_perspective_password", "password", String.class, true),
        PROVIDER_ID("j.location_perspective_activity_provider_id", "providerId", UUID.class, true),
        //RELATIVE_PATH("k.location_perspective_base_path", "basePath", String.class, true),
        ;
        
        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String WITH_RECURSIVE_CLAUSE = "";

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " + 
            "mwa.location AS i "+
            "JOIN mwa.location_perspective as j on i.location_id = j.location_id "+
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        for (COLUMN enumColumn : COLUMN.values()) {
            map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }

    public PerspectiveQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }
    
    @Override
    protected RowMapper<PerspectiveModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<PerspectiveModel> {
        public PerspectiveModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        	PerspectiveModel model = new PerspectiveModel();
        	model.setBasePath(rs.getString(COLUMN.BASE_PATH.alias));
        	model.setPerspective(rs.getString(COLUMN.PERSPECTIVE.alias));
        	model.setUser(rs.getString(COLUMN.USER.alias));
        	model.setPassword(rs.getString(COLUMN.PASSWORD.alias));
        	model.setProviderId(rs.getString(COLUMN.PROVIDER_ID.alias));
        	//model.setLocalBasePath(rs.getString(COLUMN.RELATIVE_PATH.alias));
        	return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }
}
