package com.sony.pro.mwa.storage.repository.database;

import java.security.Principal;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.model.storage.PerspectiveCollection;
import com.sony.pro.mwa.model.storage.PerspectiveModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.storage.repository.PerspectiveDao;
import com.sony.pro.mwa.storage.repository.database.query.PerspectiveQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;

@Repository
public class PerspectiveDaoImpl extends DatabaseBaseDao<PerspectiveModel, PerspectiveCollection, QueryCriteria> implements PerspectiveDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public PerspectiveCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public PerspectiveModel getModel(String id, Principal principal) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
		//return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	public PerspectiveModel addModel(final PerspectiveModel model) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	public PerspectiveModel updateModel(final PerspectiveModel acti) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	public PerspectiveModel deleteModel(PerspectiveModel model) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	@Override
	protected IQuery<PerspectiveModel, PerspectiveCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new PerspectiveQuery(jdbcTemplate);
	}
}
