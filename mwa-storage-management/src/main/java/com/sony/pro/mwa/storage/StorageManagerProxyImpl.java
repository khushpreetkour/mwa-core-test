package com.sony.pro.mwa.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.model.storage.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.sony.pro.mwa.utils.rest.HttpMethodType;
import com.sony.pro.mwa.utils.rest.IRestInput;
import com.sony.pro.mwa.utils.rest.RestClient;
import com.sony.pro.mwa.utils.rest.RestInputImpl;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.utils.RestUtils;
import com.sony.pro.mwa.model.UriModel;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.storage.IPerspective;
import com.sony.pro.mwa.service.storage.IStorageManager;
import com.sony.pro.mwa.service.storage.IUriResolver;
import com.sony.pro.mwa.utils.UriUtils;

@Component
public class StorageManagerProxyImpl implements IStorageManager, IUriResolver {

	protected RestClient restClient = new RestClient("");
	private static final String URL_FORMAT = "http://%s:%s/mwa/api/v2/locations";
	private static final String URL_ADDED_ID_FORMAT = "http://%s:%s/mwa/api/v2/locations/%s";
	private static final String PROVIDER_URL_FORMAT = "http://%s:%s/mwa/api/v2/activity-providers";

	@Autowired
	private Environment env;
	
	// TODO host,portはとりあえずハードコーディング
	private String HOST = "localhost";
	private String PORT = "8080";

	public void initialize() {
		if (env != null) { 
			String envHost = env.getProperty("mwa.master.host");
			String envPort = env.getProperty("mwa.master.port");
			if (envHost != null)
				HOST = envHost;
			if (envPort != null)
				PORT = envPort;
		}
		System.out.println("mwa.master=" + HOST + ":" + PORT);
	}

	public void destroy() {
	}

	private String getUrl(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlのパスまで生成
		String url = getUrl();
		// queryを生成
		String query = RestUtils.createQuery(sorts, filters, offset, limit);
		// queryがnullでなければ、urlと結合する
		if (null != query) {
			url = url + "?" + query;
		}
		return url;
	}

	private String getUrl() {
		return String.format(URL_FORMAT, HOST, PORT);
	}

	private String getUrl(String id) {
		return String.format(URL_ADDED_ID_FORMAT, HOST, PORT, id);
	}

	// System Setting用
	public LocationCollection getLocations(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		LocationCollection models = (LocationCollection) RestUtils.convertToObj(restClient.getResponseBody(), LocationCollection.class);

		return models;
	}

	public LocationModel getLocation(String locationId) {
		// urlを取得
		String url = getUrl(locationId);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		LocationModel model = (LocationModel) RestUtils.convertToObj(restClient.getResponseBody(), LocationModel.class);
		return model;
	}

	public LocationModel getLocationByName(String locationName) {
		List<String> filter = new ArrayList<>();
		filter.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + locationName);
		LocationModel model = getLocations(null, filter, null, null).getModels().get(0);

		if (model == null)
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);

		return model;
	}

	public DirectoryCollection getDirectories(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		DirectoryCollection models = (DirectoryCollection) RestUtils.convertToObj(restClient.getResponseBody(), DirectoryModel.class);

		return models;
	}

	public DirectoryModel getDirectory(String path) {
		// urlを取得
		String url = getUrl();
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);
		return (DirectoryModel) RestUtils.convertToObj(restClient.getResponseBody(), DirectoryModel.class);
	}

	protected Map<String, PerspectiveModel> createPerspectiveModel(List<LocalPerspectiveModel> lpms) {
		Map<String, PerspectiveModel> result = new HashMap<>();
		for (LocalPerspectiveModel lpm : lpms) {
			PerspectiveModel model = new PerspectiveModel();
			model.setPerspective(lpm.getPerspective());
			model.setBasePath(lpm.getBasePath());
			model.setUser(lpm.getUser());
			model.setPassword(lpm.getPassword());
			result.put(lpm.getPerspective(), model);
		}
		return result;
	}

	public LocationModel addLocation(LocationModel model) {
		// urlを取得
		String url = getUrl();
		// LocationModelをJson形式に変換
		String json = RestUtils.convertToJson(model);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.POST.name(), url, json);
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		LocationModel result = (LocationModel) RestUtils.convertToObj(restClient.getResponseBody(), LocationModel.class);

		return result;
	}

	// MultiMWA対
	public LocationModel updateLocation(LocationModel model) {

		// urlを取得
		String url = getUrl(model.getId());
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.PUT.name(), url, RestUtils.convertToJson(model));
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		LocationModel result = (LocationModel) RestUtils.convertToObj(restClient.getResponseBody(), LocationModel.class);

		return result;
	}

	public LocationModel deleteLocation(String locationId) {
		// urlを取得
		String url = getUrl(locationId);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		LocationModel result = (LocationModel) RestUtils.convertToObj(restClient.getResponseBody(), LocationModel.class);
		return result;
	}

	public LocationCollection deleteLocations(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		LocationCollection models = (LocationCollection) RestUtils.convertToObj(restClient.getResponseBody(), LocationCollection.class);
		return models;
	}

	// System Setting用
	/* public LocationModel addProviderLocationMapping(LocationModel model) {
	 * return null; } public LocationModel
	 * updateProviderLocationMapping(LocationModel model) { return null; } */

	// 実処理用（Activityが主ユーザ）
	@Override
	public NativeLocationModel asNativePath(String uri, String... perspectives) {
		return asNativePathViaProvider(uri, null, perspectives);
	}

	public NativeLocationModel asNativePath(String uri, String perspective) {
		return asNativePathViaProvider(uri, null, perspective);
	}

	public NativeLocationModel asNativePath(String uri, String perspective, String providerId) {
		return asNativePathViaProvider(uri, providerId, perspective);
	}

	@Override
	public NativeLocationModel asNativePathViaProvider(String uri, String providerId, String... perspectives) {
		NativeLocationModel result = null;
		MwaError error = null;
		for (String perspective : perspectives) {
			try {
				result = asNativePathCore(uri, providerId, perspective);
			} catch (MwaError e) {
				error = e;
			}
			if (result != null)
				break;
		}
		if (result == null) {
			if (error != null)
				throw error;
			else
				throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
		}
		return result;
	}

	protected NativeLocationModel asNativePathCore(String uri, String providerId, String perspective) {
		UriModel uc = UriUtils.parse(uri);
		if (uc == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_URI_FORMAT);
		}

		LocationModel model = getLocationByName(uc.getLocation());
		if (model == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
		}
		if (model.getPerspectiveMap() == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}

		PerspectiveModel pm = model.getPerspectiveMap().get(perspective);
		if (pm == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}

		LocalPerspectiveModel lpm = null;
		if (providerId != null) {
			// providerId情報を取得
			ActivityProviderModel providerModel = getProvider(providerId);
			// LocalPerspective情報が存在するか確認
			if (providerModel.getLocalPerspectiveList() != null) {
				// Location名とperspectiveを取得
				String locationName = providerModel.getLocalPerspectiveList().get(0).getLocationName();
				String locationPerspective = providerModel.getLocalPerspectiveList().get(0).getPerspective();
				// Location名とperspectiveを確認し、一致したらLocalPerspective情報をlpmに格納
				if (locationName.equals(uc.getLocation()) && locationPerspective.equals(perspective)) {
					lpm = (LocalPerspectiveModel) providerModel.getLocalPerspectiveList();
				}
			}
		}

		String val = null;
		if (lpm != null && lpm.getLocalBasePath() != null)
			val = UriUtils.mwa2native(uri, lpm.getLocalBasePath());
		else
			val = UriUtils.mwa2native(uri, model.getPerspectiveMap().get(perspective).getBasePath());

		List<String> paths = new ArrayList<>();
		paths.add(val);
		NativeLocationModel result = new NativeLocationModel().setUser(pm.getUser()).setPassword(pm.getPassword()).setPaths(paths);
		return result;
	}

	public String asMwaUri(String nativePath, String location) {
		IPerspective perspective = UriUtils.checkPerspective(nativePath);
		if (perspective == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}
		// uc.g
		LocationModel model = getLocationByName(location);
		if (model == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION);
		}
		if (model.getPerspectiveMap() == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}
		if (model.getPerspectiveMap().get(perspective.name()) == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE);
		}
		String val = UriUtils.native2mwa(location, nativePath, model.getPerspectiveMap().get(perspective.name()).getBasePath());
		return val;
	}

	public String asMwaUri(String nativePath, String location, String providerId) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	private ActivityProviderModel getProvider(String providerId) {
		// urlを取得
		String url = String.format(PROVIDER_URL_FORMAT, HOST, PORT);

		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ActivityProviderModel model = (ActivityProviderModel) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderModel.class);
		return model;
	}
}