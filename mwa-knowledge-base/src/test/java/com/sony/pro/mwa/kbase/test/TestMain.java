package com.sony.pro.mwa.kbase.test;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.kbase.KnowledgeBaseImpl;
import com.sony.pro.mwa.kbase.repository.ActivityTemplateDao;
import com.sony.pro.mwa.kbase.repository.database.ActivityTemplateDaoImpl;
import com.sony.pro.mwa.kbase.repository.database.ExtensionDaoImpl;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.service.activity.IMwaActivity;
import com.sony.pro.mwa.service.kbase.IKnowledgeBase;

public class TestMain {

	static DataSource ds;
	
	@Test
	public void test() {/*
		KnowledgeBase kBase = KnowledgeBaseImpl.getInstance();
		{
			//そのうちDIでやりますが暫定
			ActivityTemplateDaoImpl dao1 = new ActivityTemplateDaoImpl();
			MwaPluginDaoImpl dao2 = new MwaPluginDaoImpl();
			dao1.setDataSource(ds);
			dao2.setDataSource(ds);
			((KnowledgeBaseImpl)kBase).setActivityTemplateDao(dao1);
			((KnowledgeBaseImpl)kBase).setMwaPluginDao(dao2);
		}
		
		kBase.storeJarFile("C:\\work\\bundle_test\\mwa-activity-ht-approval-0.2.2.jar");
		kBase.loadBundle("mwa-activity-ht-approval-0.2.2.jar");
		ActivityTemplate template = kBase.getActitvityTemplate("ClipReviewHumanTask", "v1");
		IMwaActivity activity = template.createInstance();
		System.out.println("created instance = " + activity.toString() );*/
		
/*		ActivityTemplateDaoImpl dao = new ActivityTemplateDaoImpl();
		dao.setDataSource(ds);
		ActivityTemplateModel model = dao.getModel("0ec82e94-13ed-75da-d8e7-470acebc009f", null);*/
	}

	@BeforeClass
	public static void beforeClass() {
		Properties properties = new Properties();
		InputStream is = ClassLoader.getSystemResourceAsStream("test.properties");
		try {
			properties.load(is);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			ds = org.apache.commons.dbcp.BasicDataSourceFactory.createDataSource(properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void afterClass() {
		try {
			if (ds != null || ds.getConnection() != null)
				ds.getConnection().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
