package com.sony.pro.mwa.kbase;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.bundle.MwaBundleContentManager;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.Extract;
import com.sony.pro.mwa.common.utils.SystemUtils;
import com.sony.pro.mwa.common.utils.UUIDUtils;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.enumeration.ExtensionContentType;
import com.sony.pro.mwa.enumeration.ExtensionType;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.kbase.WorkFlowOperationEnum;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.internal.activity.ExternalMwaTask;
import com.sony.pro.mwa.internal.service.IKnowledgeBaseInternal;
import com.sony.pro.mwa.kbase.bundle.osgi.FrameworkContloller;
import com.sony.pro.mwa.kbase.internal.IWorkflowService;
import com.sony.pro.mwa.kbase.model.bpmnmodel.converter.WorkflowDiagramModelConverter;
import com.sony.pro.mwa.kbase.repository.ActivityProfileDao;
import com.sony.pro.mwa.kbase.repository.ActivityProfileGroupDao;
import com.sony.pro.mwa.kbase.repository.ActivityProviderTypeDao;
import com.sony.pro.mwa.kbase.repository.ActivityTemplateDao;
import com.sony.pro.mwa.kbase.repository.ExtensionContentDao;
import com.sony.pro.mwa.kbase.repository.ExtensionDao;
import com.sony.pro.mwa.kbase.repository.ParameterSetDao;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.kbase.repository.ParameterTypeDao;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.kbase.utils.WorkflowModelUpdateProvider;
import com.sony.pro.mwa.model.UriModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityTemplateCollection;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupDetailModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileModel;
import com.sony.pro.mwa.model.kbase.ExtensionCollection;
import com.sony.pro.mwa.model.kbase.ExtensionContentCollection;
import com.sony.pro.mwa.model.kbase.ExtensionContentModel;
import com.sony.pro.mwa.model.kbase.ExtensionModel;
import com.sony.pro.mwa.model.kbase.MwaPluginModel;
import com.sony.pro.mwa.model.kbase.MwaRuleModel;
import com.sony.pro.mwa.model.kbase.MwaWorkflowModel;
import com.sony.pro.mwa.model.kbase.ParameterSetCollection;
import com.sony.pro.mwa.model.kbase.ParameterSetModel;
import com.sony.pro.mwa.model.kbase.ParameterTypeCollection;
import com.sony.pro.mwa.model.kbase.ParameterTypeModel;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.tempmodels.Parameter;
import com.sony.pro.mwa.model.kbase.tempmodels.PositionModel;
import com.sony.pro.mwa.model.kbase.tempmodels.ConnectionPortModel;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WFParameterDefinition;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDetailModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramCollection;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramDetailModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModelCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.model.resource.CostModel;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.resource.activity.ResourceTaskDefinition;
import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.provider.ActivityProviderType;
import com.sony.pro.mwa.service.workflow.IRuleManager;
import com.sony.pro.mwa.service.workflow.IWorkflowManager;
import com.sony.pro.mwa.utils.UriUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Date;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class KnowledgeBaseImpl implements IKnowledgeBaseInternal {

    private final static MwaLogger logger = MwaLogger.getLogger(KnowledgeBaseImpl.class);

    // ToDo: 暫定、のちのちDB
    protected java.util.Map<String, String> jarMap = new java.util.HashMap<>();

    protected IWorkflowService workflowService;
    private WorkflowModelUpdateProvider workflowModelUpdateProvider = new WorkflowModelUpdateProvider(this);
    
    protected ActivityTemplateDao activityTemplateDao;
    protected ActivityProviderTypeDao activityProviderTypeDao;
    protected ExtensionDao extensionDao;
    protected ExtensionContentDao extensionContentDao;
    protected ParameterSetDao parameterSetDao;
    protected IWorkflowManager worklflowManager;
    protected IActivityManager activityManager;
    protected IRuleManager ruleManager;
    protected Map<String, Long> bundleIdMap = new ConcurrentHashMap<>();
    protected ActivityProfileGroupDao activityProfileGroupDao;
    protected ActivityProfileDao activityProfileDao;
    protected ParameterTypeDao parameterTypeDao;

    protected Boolean clearCache = false;

    protected Map<String, ActivityTemplate> sysTemplates = new HashMap<>();
    protected Map<String, ActivityProviderType> sysProviderTypes = new HashMap<>();

    public void setWorkflowService(IWorkflowService workflowService) {
    	this.workflowService = workflowService;
    }

    public IWorkflowService getWorkflowService() {
    	return workflowService;
    }

    @Autowired
    public void setActivityTemplateDao(ActivityTemplateDao dao) {
        this.activityTemplateDao = dao;
    }
    public ActivityTemplateDao getActivityTemplateDao() {
        return this.activityTemplateDao;
    }

    @Autowired
    public void setActivityProviderTypeDao(ActivityProviderTypeDao dao) {
        this.activityProviderTypeDao = dao;
    }
    public ActivityProviderTypeDao getActivityProviderTypeDao() {
        return this.activityProviderTypeDao;
    }

    @Autowired
    public void setExtensionDao(ExtensionDao dao) {
        this.extensionDao = dao;
    }
    public ExtensionDao getExtensoinDao() {
        return this.extensionDao;
    }

    @Autowired
    public void setExtensionContentDao(ExtensionContentDao dao) {
        this.extensionContentDao = dao;
    }
    public ExtensionContentDao getExtensoinContentDao() {
        return this.extensionContentDao;
    }

	public IWorkflowManager getWorklflowManager() {
		return worklflowManager;
	}

	public void setWorklflowManager(IWorkflowManager worklflowManager) {
		this.worklflowManager = worklflowManager;
	}
	@Autowired
    public void setWorkflowManager(IWorkflowManager workflowManager) {
        this.worklflowManager = workflowManager;
        ruleManager = worklflowManager.getRuleManager();
    }
    public IWorkflowManager getWorkflowManager() {
        return this.worklflowManager;
    }

    @Autowired
    public void setActivityManager(IActivityManager activityManager) {
        this.activityManager = activityManager;
    }
    public IActivityManager getActivityManager() {
        return activityManager;
    }

    @Autowired
    public void setParameterSetDao(ParameterSetDao parameterSetDao) {
        this.parameterSetDao = parameterSetDao;
        ruleManager = worklflowManager.getRuleManager();
    }
    public ParameterSetDao getParameterSetDao() {
        return this.parameterSetDao;
    }
    @Autowired
    public void setClearCache(Boolean clearCache) {
        this.clearCache = clearCache;
    }
    public Boolean getClearCache() {
        return this.clearCache;
    }

    @Autowired
    public void setActivityProfileGroupDao(ActivityProfileGroupDao dao) {
        this.activityProfileGroupDao = dao;
    }
    public ActivityProfileGroupDao getActivityProfileGroupDao() {
        return this.activityProfileGroupDao;
    }

    @Autowired
    public void setActivityProfileDao(ActivityProfileDao dao) {
        this.activityProfileDao = dao;
    }
    public ActivityProfileDao getActivityProfileDao() {
        return this.activityProfileDao;
    }

    @Autowired
    public void setParameterTypeDao(ParameterTypeDao dao) {
        this.parameterTypeDao = dao;
    }
    public ParameterTypeDao getParameterTypeDao() {
        return this.parameterTypeDao;
    }

    //@PostConstruct
    @Override
    public void initialize() {
        //FrameworkContloller.create(); // OSGIのFW作成

        {	//Register System Plugins
            registerSystemTemplates(ExternalMwaTask.getTemplates());
            registerSystemProviderTypes(ExternalMwaTask.getProviderTypes());
        }
        {	//Register System Plugins
            registerSystemTemplates(ResourceTaskDefinition.getTemplates());
            registerSystemProviderTypes(ResourceTaskDefinition.getProviderTypes());
        }

        FrameworkContloller.create(); // OSGIのFW作成

        List<ActivityTemplateModel> templateModelsOnDB = this.getActitvityTemplates(null,null,null,null).getModels();
        List<ActivityProviderTypeModel> providerTypesOnDB = this.getActivityProviderTypes(null, null, null, null).getModels();
        loadSystemComponents(templateModelsOnDB, providerTypesOnDB);
        loadPlugins(templateModelsOnDB, providerTypesOnDB);
        loadRules();
        loadBpmns();
    }

    @Override
    public void destroy() {
        FrameworkContloller.shutdown();
    }

    @Override
    public void storeJarFile(String bundleFilePath) {
        // TODO Auto-generated method stub
    }
/*	@Override
    public void storeJarFile(String fileName, byte[] jarBytes) {

        try (OutputStream os = Files.newOutputStream(JAR_HOME + fileName, StandardOpenOption.WRITE)) {
              //osを使った書き込み os.write()
                DataInputStream dis = new DataInputStream(request.getInputStream());
                dis.readFully(buffer);
                request.getInputStream().read(buffer);
                os.write(buffer);
            } catch (IOException e) {
            }
    }*/

    @Override
    public ExtensionCollection getPlugins(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        if (filters == null) {
            filters = new ArrayList<>();
        }
        filters.add("type" + FilterOperatorEnum.EQUAL.toSymbol() + ExtensionType.BUNDLE.name());

        return getExtensions(sorts, filters, offset, limit);
    }

    @Override
    public ExtensionCollection getBpmns(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        if (filters == null) {
            filters = new ArrayList<>();
        }
        filters.add("type" + FilterOperatorEnum.EQUAL.toSymbol() + ExtensionType.BPMN.name());
        return getExtensions(sorts, filters, offset, limit);
    }

    @Override
    public ExtensionCollection getRules(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        if (filters == null) {
            filters = new ArrayList<>();
        }
        filters.add("type" + FilterOperatorEnum.EQUAL.toSymbol() + ExtensionType.RULE.name());
        return getExtensions(sorts, filters, offset, limit);
    }

    @Override
    public ExtensionCollection getExtensions(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        return this.extensionDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
    }

    @Override
    public ExtensionModel getExtension(String id) {
        ExtensionModel model = this.extensionDao.getModel(id, null);
        if (model != null) {
            List<String> filters = new ArrayList<>();
            filters.add("extensionId" + FilterOperatorEnum.EQUAL.toSymbol() + id);
            ExtensionContentCollection contents = this.extensionContentDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filters, null, null, null));
            if (contents != null) {
                model.setContents(contents.getModels());
            }
        }
        return model;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void loadPlugins() {
        this.loadPlugins(this.getActitvityTemplates(null,null,null,null).getModels(), this.getActivityProviderTypes(null, null, null, null).getModels());
    }
    public void loadPlugins(List<ActivityTemplateModel> templateModelsOnDB, List<ActivityProviderTypeModel> providerTypesOnDB) {
        {
        	if (clearCache != null && clearCache) {
        		logger.info("Enable \"Clear knowledgebase cache\"");
                FrameworkContloller.deleteBundleAll();	//キャッシュを削除
                this.extensionDao.deleteAllModel(ExtensionType.BUNDLE.name());	//キャッシュ削除に伴いPluginをDBから削除（後続の作業で再登録）
        		logger.info("\"Clear knowledgebase cache\" done");
        	} else {
        		logger.info("Disable \"clear knowledgebase cache\"");
        	}
            FrameworkContloller.start();
            logger.info("osgi-framework: Started!!");
        }

        File pluginsDir = new File(SystemUtils.MWA.PLUGINS);
        if (!pluginsDir.exists()) {
            try {
                if ( !pluginsDir.mkdirs() ) {
                    logger.error("Can't find and create plugins directory:\"" + SystemUtils.MWA.PLUGINS + "\"");
                    return;
                }
            } catch (Exception e) {
                logger.error("Can't find and create plugins directory:\"" + SystemUtils.MWA.PLUGINS + "\"", e);
                return;
            }
        }
        File pluginFilesDir = new File(SystemUtils.MWA.PLUGIN_FILES);
        if (!pluginFilesDir.exists()) {
            try {
                if ( !pluginFilesDir.mkdirs() ) {
                    logger.error("Can't find and create plugin_files directory:\"" + SystemUtils.MWA.PLUGIN_FILES + "\"");
                    return;
                }
            } catch (Exception e) {
                logger.error("Can't find and create plugin_files directory:\"" + SystemUtils.MWA.PLUGIN_FILES + "\"", e);
                return;
            }
        }

        final List<File> fileList = (List<File>) FileUtils.listFiles(pluginsDir, FileFilterUtils.suffixFileFilter(".jar"), FileFilterUtils.trueFileFilter());
        for (File file : fileList) {
            try {
                loadPlugin(templateModelsOnDB, providerTypesOnDB, file.getName());
            } catch (Exception e) {
                logger.error("Plugin load failure: file=" + file.getName() + ", e=" + e.toString() + ", e.message=" + e.getMessage(), e);
            }
        }
    }

    @Override
    public void loadBpmns() {
        {
            this.extensionDao.deleteAllModel(ExtensionType.BPMN.name());
        }
        List<String> bpmnFilePaths = new ArrayList<>();
        loadBpmns(bpmnFilePaths, SystemUtils.MWA.BPMNS);
        loadBpmns(bpmnFilePaths, SystemUtils.MWA.CUSTOM_BPMNS);
        registerTemplatesToWorkflowManager();
        this.worklflowManager.reloadProcessEngine(bpmnFilePaths);
    }

    public void loadBpmns(List<String> bpmnFilePaths, String directoryPath) {
        File bpmnsDir = new File(directoryPath);
        if (!bpmnsDir.exists()) {
            try {
                if ( !bpmnsDir.mkdirs() ) {
                    logger.error("Can't find and create bpmn directory:\"" + directoryPath + "\"");
                    return;
                }
            } catch (Exception e) {
                logger.error("Can't find and create bpmn directory:\"" + directoryPath + "\"", e);
                return;
            }
        }

        final List<File> fileList = (List<File>) FileUtils.listFiles(bpmnsDir, FileFilterUtils.suffixFileFilter(".bpmn"), FileFilterUtils.trueFileFilter());
        for (File file : fileList) {
            try {
                loadBpmn(file.getName(), directoryPath);
                bpmnFilePaths.add(file.getAbsolutePath());
            } catch (Exception e) {
                logger.error("BPMN load failure: file=" + file.getName() + ", e=" + e.toString() + ", e.message=" + e.getMessage(), e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public List<String> getRuleFilePaths(String path) {
        List<String> ruleFilePaths = null;
        File rulesDir = new File(path);

        if (!rulesDir.exists()) {
            try {
                if ( !rulesDir.mkdirs() ) {
                    logger.error("Can't find and create resource_rules directory:\"" + SystemUtils.MWA.BPMNS + "\"");
                    return null;
                }
            } catch (Exception e) {
                logger.error("Can't find and create resource_rules directory:\"" + SystemUtils.MWA.BPMNS + "\"", e);
                return null;
            }
        }

        ruleFilePaths = new ArrayList<>();
        // for excel type table
        List<File> fileList = (List<File>) FileUtils.listFiles(
                rulesDir, FileFilterUtils.suffixFileFilter(".xls"), FileFilterUtils.trueFileFilter());

        fileList.addAll(
                FileUtils.listFiles(rulesDir, FileFilterUtils.suffixFileFilter(".dsl"), FileFilterUtils.trueFileFilter())
                );

        fileList.addAll(
                FileUtils.listFiles(rulesDir, FileFilterUtils.suffixFileFilter(".dslr"), FileFilterUtils.trueFileFilter())
                );

        fileList.addAll(
                FileUtils.listFiles(rulesDir, FileFilterUtils.suffixFileFilter(".drl"), FileFilterUtils.trueFileFilter())
                );

        for (File file : fileList) {
            try {
                ruleFilePaths.add(file.getAbsolutePath());
            } catch (Exception e) {
                logger.error("RULE load failure: file=" + file.getName() + ", e=" + e.toString() + ", e.message=" + e.getMessage(), e);
            }
        }

        return ruleFilePaths;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void loadRules() {
        {
            this.extensionDao.deleteAllModel(ExtensionType.RULE.name());
        }

        File rulesDir = new File(SystemUtils.MWA.RULES);

        if (!rulesDir.exists()) {
            try {
                if ( !rulesDir.mkdirs() ) {
                    logger.error("Can't find and create rules directory:\"" + SystemUtils.MWA.BPMNS + "\"");
                    return;
                }
            } catch (Exception e) {
                logger.error("Can't find and create rules directory:\"" + SystemUtils.MWA.BPMNS + "\"", e);
                return;
            }
        }

        List<String> ruleFilePaths = new ArrayList<>();
        // for excel type table
        List<File> fileList = (List<File>) FileUtils.listFiles(
                rulesDir, FileFilterUtils.suffixFileFilter(".xls"), FileFilterUtils.trueFileFilter());
        for (File file : fileList) {
            try {
                if (loadRule(file.getName(), ActivityType.DTABLE)) {
                    ruleFilePaths.add(file.getAbsolutePath());
                }
            } catch (Exception e) {
                logger.error("RULE load failure: file=" + file.getName() + ", e=" + e.toString() + ", e.message=" + e.getMessage(), e);
            }
        }

        // for dsl type table
        fileList.clear();
        fileList = (List<File>) FileUtils.listFiles(
                rulesDir, FileFilterUtils.suffixFileFilter(".dsl"), FileFilterUtils.trueFileFilter());
        for (File file : fileList) {
            try {
                if (loadRule(file.getName(), ActivityType.DSL)) {
                    ruleFilePaths.add(file.getAbsolutePath());
                }
            } catch (Exception e) {
                logger.error("RULE load failure: file=" + file.getName() + ", e=" + e.toString() + ", e.message=" + e.getMessage(), e);
            }
        }

        // for dslr type table
        fileList.clear();
        fileList = (List<File>) FileUtils.listFiles(
                rulesDir, FileFilterUtils.suffixFileFilter(".dslr"), FileFilterUtils.trueFileFilter());
        for (File file : fileList) {
            try {
                if (loadRule(file.getName(), ActivityType.DSLR)) {
                    ruleFilePaths.add(file.getAbsolutePath());
                }
            } catch (Exception e) {
                logger.error("RULE load failure: file=" + file.getName() + ", e=" + e.toString() + ", e.message=" + e.getMessage(), e);
            }
        }

        // for drl type table
        fileList.clear();
        fileList = (List<File>) FileUtils.listFiles(
                rulesDir, FileFilterUtils.suffixFileFilter(".drl"), FileFilterUtils.trueFileFilter());
        for (File file : fileList) {
            try {
                if (loadRule(file.getName(), ActivityType.DRL)) {
                    ruleFilePaths.add(file.getAbsolutePath());
                }
            } catch (Exception e) {
                logger.error("RULE load failure: file=" + file.getName() + ", e=" + e.toString() + ", e.message=" + e.getMessage(), e);
            }
        }

        List<String> resourceRuleFilePaths = getRuleFilePaths(SystemUtils.MWA.RESOURCE_RULES);
        if (resourceRuleFilePaths != null)
            ruleFilePaths.addAll(resourceRuleFilePaths);
        this.ruleManager.reloadRuleEngine(ruleFilePaths);
    }

    public void registerTemplatesToWorkflowManager() {
        List<ActivityTemplateModel> templateModels = this.getActitvityTemplates(null,null,null,null).getModels();
        for (ActivityTemplateModel model : templateModels) {
            try {
                IActivityTemplate template = this.getActitvityTemplateService(model.getName(), model.getVersion());
                if (template != null) {
                    worklflowManager.registerTemplate(template.getName(), template.getVersion(), template.getSyncFlag());
                    logger.info("[ydbg] register workitem handler: name=" + template.getName() + ", version=" + template.getVersion());
                } else {
                    logger.error("Can't load template: model.name=" + model.getName() + ", model.version=" + model.getVersion());
                }
            } catch (MwaError e) {
                if (MWARC.INTEGRATION_ACTIVITY_NOT_FOUND.equals(e.getMWARC())) {
                    //do nothing
                } else {
                    throw e;
                }
            } catch (Exception e) {
                logger.error("Skip workItem registration: model.name=" + model.getName() + ", model.version=" + model.getVersion(), e);
            }
        }
    }

    private static <T> boolean equals(T a, T b) {
        if (a == null && b == null)
            return true;
        else if (a == null || b == null)
            return false;
        else
            return a.equals(b);
    }
    private void updateTemplatesDB(List<ActivityTemplateModel> templateModelsOnDB, ActivityTemplate template, Long bundleId, String pluginId) {
        for (ActivityTemplateModel model : templateModelsOnDB) {
            if (model.getId().equals(template.getId())) {
                String diff = "";
                if (!template.getName().equals(model.getName()))
                    diff += "name=" + model.getName() + "\" -> \"" + template.getName() + "\"";
                if (!template.getVersion().equals(model.getVersion()))
                    diff += ": version=\"" + model.getVersion() + "\" -> \"" + template.getVersion() + "\"";
                logger.info("ActivityTemplate(" + template.getName() + ", " + template.getVersion() + ") is already registered. Update info.");
                if (!diff.isEmpty())
                    logger.info("ActivityTemplate(" + template.getId() + ") updated: " + diff);
                this.activityTemplateDao.updateModel(template.setBundleId(bundleId).setExtensionId(pluginId).setDeleteFlag(model.getDeleteFlag()));
                return;
            }
        }
        //DB上になければ
        logger.info("Register template: name=" + template.getName() + ", type=" + template.getType().name() + ", version=" + template.getVersion());
        this.activityTemplateDao.addModel(template.setBundleId(bundleId).setExtensionId(pluginId).setDeleteFlag(Boolean.FALSE));
    }

    private void registerTemplatesToDB(List<ActivityTemplateModel> templateModelsOnDB, Collection<ActivityTemplate> templates, Long bundleId, String pluginId) {
        if (templates != null) {
            for (ActivityTemplate template : templates) {
                updateTemplatesDB(templateModelsOnDB, template, bundleId, pluginId);
            }
        } else {
            logger.debug("Plugin("+ pluginId + ") does not have any activity template.");
        }
    }

    private void updateProviderTypeToDB(List<ActivityProviderTypeModel> providerTypesOnDB, ActivityProviderType providerType, Long bundleId, String pluginId) {
        for (ActivityProviderTypeModel model : providerTypesOnDB) {
            if (model.getId().equals(providerType.getId())) {
                if (this.equals(model.getBundleId(), bundleId) && this.equals(model.getPluginId(), pluginId) ) {
                    //do nothing
                	providerType.getModel().setBundleId(bundleId).setPluginId(pluginId);
                } else {
                    String diff = "";
                    if (!providerType.getName().equals(model.getName()))
                        diff += "name=" + model.getName() + "\" -> \"" + providerType.getName() + "\"";
                    if (!providerType.getModelNumber().equals(model.getModelNumber()))
                        diff += ": version=\"" + model.getModelNumber() + "\" -> \"" + providerType.getModelNumber() + "\"";
                    logger.info("ActivityProviderType(" + providerType.getName() + ", " + providerType.getModelNumber() + ") is already registered. Update info.");
                    if (!diff.isEmpty())
                        logger.info("ActivityProviderType(" + providerType.getId() + ") updated: " + diff);
                    this.activityProviderTypeDao.updateModel(providerType.getModel().setBundleId(bundleId).setPluginId(pluginId));
                }
                return;
            }
        }
        //DB上になければ
        logger.info("Register providerType: name=" + providerType.getName() + ", model=" + providerType.getModelNumber());
        this.activityProviderTypeDao.addModel(providerType.getModel().setBundleId(bundleId).setPluginId(pluginId));
    }
    private void registerProviderTypesToDB(List<ActivityProviderTypeModel> providerTypesOnDB, Collection<ActivityProviderType> providerTypes, Long bundleId, String pluginId) {
        if (providerTypes != null) {
            for (ActivityProviderType providerType : providerTypes) {
                updateProviderTypeToDB(providerTypesOnDB, providerType, bundleId, pluginId);
            }
        } else {
            logger.debug("Plugin("+ pluginId + ") does not have any activity provider type.");
        }
    }

    @Override
    public void registerSystemTemplates(List<ActivityTemplate> templates) {
    	for (ActivityTemplate template : templates) {
        	this.sysTemplates.put(template.getId(), template);
        }
    }
    @Override
    public void registerSystemProviderTypes(List<ActivityProviderType> providerTypes) {
        for (ActivityProviderType providerType : providerTypes) {
            this.sysProviderTypes.put(providerType.getId(), providerType);
        }
    }

    public OperationResult loadSystemComponents(List<ActivityTemplateModel> templateModelsOnDB, List<ActivityProviderTypeModel> providerTypesOnDB) {
        ActivityProviderType type1 = new ActivityProviderType("GENERAL", "0000", null, null);
        ActivityProviderType type2 = new ActivityProviderType("MWA", "v2", null, null);
        sysProviderTypes.put(type1.getId(), type1);
        sysProviderTypes.put(type2.getId(), type2);
        registerTemplatesToDB(templateModelsOnDB, sysTemplates.values(), null, null);
        registerProviderTypesToDB(providerTypesOnDB, sysProviderTypes.values(), null, null);
        return OperationResult.newInstance();
    }

    @Override
    public OperationResult loadPlugin(String pluginName, String pluginVersion) {
        String jarName = pluginName + "-" + pluginVersion + ".jar";
        List<ActivityTemplateModel> templateModelsOnDB = this.getActitvityTemplates(null,null,null,null).getModels();
        List<ActivityProviderTypeModel> providerTypesOnDB = this.getActivityProviderTypes(null, null, null, null).getModels();
        return this.loadPlugin(templateModelsOnDB, providerTypesOnDB, jarName);
    }


    @Transactional(readOnly=false, rollbackFor=Exception.class)
    public OperationResult loadPlugin(List<ActivityTemplateModel> templateModelsOnDB, List<ActivityProviderTypeModel> providerTypesOnDB, String jarName) {
        String path = SystemUtils.MWA.PLUGINS + jarName;
        Bundle bundle = FrameworkContloller.addBundle(path);
        BundleContext bContext = bundle.getBundleContext();
        String bundleName = bundle.getSymbolicName();
        String bundleVersion = bundle.getVersion().toString();
        String pluginId = UUIDUtils.generateUUID(bundleName, bundleVersion);

        ServiceReference<MwaBundleContentManager> ref = bContext.getServiceReference(MwaBundleContentManager.class);
        MwaBundleContentManager contents = bContext.getService(ref);
        List<ActivityTemplate> templaesOnBundle = contents.getTemplates(bundle.getBundleId());
        if (templaesOnBundle != null) {
        	for (ActivityTemplate template : templaesOnBundle) {
                //bundleId depends on node. So just manage it on memory instead of DB;
        	    bundleIdMap.put(pluginId, bundle.getBundleId());
        		template.setBundleId(bundle.getBundleId()).setExtensionId(pluginId);
        	}
        }
        List<ActivityProviderType> providerTypesOnBundle = contents.getProviderTypes(bundle.getBundleId());
        if (providerTypesOnBundle != null) {
        	for (ActivityProviderType providerType : providerTypesOnBundle) {
                bundleIdMap.put(pluginId, bundle.getBundleId());
        		providerType.getModel().setBundleId(bundle.getBundleId()).setPluginId(pluginId);
        	}
        }
/*        List<ParameterTypeModel> parameterTypesOnBundle = contents.getParameterTypes(bundle.getBundleId());
        if (providerTypesOnBundle != null) {
        	for (ParameterTypeModel providerType : parameterTypesOnBundle) {
                bundleIdMap.put(pluginId, bundle.getBundleId());
        		providerType.getModel().setBundleId(bundle.getBundleId()).setPluginId(pluginId);
        	}
        }*/

        ExtensionModel model = this.extensionDao.getModel(pluginId, null);

        //ExtractはDBの登録状況とは別判断で
        //Extract job_files from bundle jar file
        {
	        String jobFilesDirPath = SystemUtils.MWA.PLUGIN_FILES + pluginId + SystemUtils.MWA.SEP;
	        File jobFilesDir = new File(jobFilesDirPath);
	        try {
	        	if (!jobFilesDir.exists()) {
	        		Extract.extract(path, SystemUtils.MWA.PLUGIN_FILES_NAME + "/", jobFilesDirPath, true);
	        	} else {
	        		//do nothing
	        	}
	        } catch (java.io.FileNotFoundException e) {
	            //Bundle doesn't have job files. Do nothing.
	        } catch (IOException e) {
	            logger.error("loadPlugin failed", e);
	        }
        }

        OperationResult result;
        if (model == null) {
            // Register plugin
            this.extensionDao.addModel(
                    new MwaPluginModel()
                    .setBundleId(Long.valueOf(bundle.getBundleId()))
                    .setName(bundleName)
                    .setVersion(bundleVersion)
                    .setId(pluginId));

            registerTemplatesToDB(templateModelsOnDB, templaesOnBundle, bundle.getBundleId(), pluginId);
            registerProviderTypesToDB(providerTypesOnDB, providerTypesOnBundle, bundle.getBundleId(), pluginId);
            result = OperationResult.newInstance();
        } else {
            result = OperationResult.newInstance(MWARC.OPERATION_FAILED);
            MwaPluginModel prevModel = new MwaPluginModel(model);
            if (!this.equals(prevModel.getBundleId(), Long.valueOf(bundle.getBundleId()))) {
                logger.info("update plugin(" + model.getName() + ", " + model.getVersion() + "): bundleId:" + prevModel.getBundleId() + "->" + bundle.getBundleId());
                this.extensionDao.updateModel(prevModel.setBundleId(bundle.getBundleId()));
            }
        }
        return result;
    }

    @Override
    public OperationResult deletePlugin(String pluginName, String pluginVersion) {
        List<String> filter = new ArrayList<String>();
        filter.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + pluginName);
        filter.add("version" + FilterOperatorEnum.EQUAL.toSymbol() + pluginVersion);

        ExtensionModel model = this.extensionDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filter,null, null, null)).first();

        OperationResult result = OperationResult.newInstance();

        if (model != null) {
//			FrameworkContloller.deleteBundle(model.getBundleId());//一時的？？
            this.activityTemplateDao.deleteModelByPluginId(model.getId());	//紐づいたPluginの削除
            this.extensionDao.deleteModel(model);
        } else {
            result = OperationResult.newInstance(MWARC.INVALID_INPUT);
        }
        return result;
    }

    public OperationResult loadBpmn(String bpmnName, String directoryPath) {

        IActivityTemplate template = this.worklflowManager.getActivityTemplate(directoryPath + bpmnName);
        if (template == null) {
        	String msg = "BPMN file not found: path=" + directoryPath + bpmnName;
        	logger.error(msg);
        	return OperationResult.newInstance(MWARC.OPERATION_FAILED, msg);
        }

        String workflowId = UUIDUtils.generateUUID(bpmnName, template.getVersion());
        ExtensionModel model = this.extensionDao.getModel(workflowId, null);
        if (model == null) {
            this.extensionDao.addModel(
                    new MwaWorkflowModel()
                    .setId(workflowId)
                    .setName(bpmnName)
                    .setVersion(template.getVersion()));

            logger.info("Added an extension(BPMN): id=" + workflowId + ", name=" + bpmnName + ", version=" + template.getVersion());
        } else {
            this.extensionDao.updateModel(
                    new MwaWorkflowModel()
                    .setId(workflowId)
                    .setName(bpmnName)
                    .setVersion(template.getVersion()));
        }

        ActivityTemplateModel templateModel = this.activityTemplateDao.getModel(template.getId(), null);
        if (templateModel == null) {
            this.activityTemplateDao.addModel(template.setExtensionId(workflowId));
            logger.info("Added a template: name=" + template.getName() + ", type=" + template.getType().name() + ", version=" + template.getVersion());
        } else {
            this.activityTemplateDao.updateModel(template.setExtensionId(workflowId).setDeleteFlag(templateModel.getDeleteFlag()));
        }
        return OperationResult.newInstance();
    }

    public boolean loadRule(String ruleName, ActivityType type) {

        String rulePath = SystemUtils.MWA.RULES + ruleName;
        IActivityTemplate template = this.ruleManager.getActivityTemplate(rulePath, type);

        if (template == null) {
            return false;
        }
        String ruleId = UUIDUtils.generateUUID(ruleName, template.getVersion());
        ExtensionModel model = this.extensionDao.getModel(ruleId, null);
        if (model == null) {
            this.extensionDao.addModel(
                    new MwaRuleModel()
                    .setId(ruleId)
                    .setName(ruleName)
                    .setVersion(template.getVersion()));

            logger.info("Added an extension(RULE): id=" + ruleId + ", name=" + ruleName + ", version=" + template.getVersion());
        } else {
            this.extensionDao.updateModel(
                    new MwaRuleModel()
                    .setId(ruleId)
                    .setName(ruleName)
                    .setVersion(template.getVersion()));
        }

        ActivityTemplateModel templateModel = this.activityTemplateDao.getModel(template.getId(), null);
        if (templateModel == null) {
            this.activityTemplateDao.addModel(template.setExtensionId(ruleId));
            logger.info("Added a template: name=" + template.getName() + ", type=" + template.getType().name() + ", version=" + template.getVersion());
        } else {
            this.activityTemplateDao.updateModel(template.setExtensionId(ruleId).setDeleteFlag(Boolean.FALSE));
        }
        return true;
    }

    public boolean addRule(String ruleName, String ruleString, ActivityType type) {
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(SystemUtils.MWA.RULES + ruleName), Charset.forName("UTF8"), StandardOpenOption.WRITE)) {
            bw.write(ruleString);
        } catch (IOException e) {
            throw new MwaError(MWARC.KBASE_ERROR);
        }

        return loadRule(ruleName, type);
    }

    @Override
    public ActivityTemplateModel getActitvityTemplate(String templateName, String templateVersion) {
        ActivityTemplateModel model = this.getActitvityTemplateCore(templateName, templateVersion);
        return model;
    }

    @Override
    public ActivityTemplateModel getActitvityTemplate(String templateId) {
        ActivityTemplateModel model = this.getActitvityTemplateCore(templateId);
        return model;
    }

    @Override
    public ActivityTemplateCollection getActitvityTemplates(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        // TODO Auto-generated method stub
        return this.activityTemplateDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
    }

    public List<IActivityTemplate> getActitvityTemplatesFull(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        // TODO Auto-generated method stub
    	ActivityTemplateCollection templateModels = this.activityTemplateDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
    	List<IActivityTemplate> templates = new ArrayList<>();
    	for (ActivityTemplateModel templateModel : templateModels.getModels()) {
    		try {
	    		IActivityTemplate template = createTemplateFromModel(templateModel);
	    		templates.add(template);
    		} catch (Exception e) {
    			//do nothing
    		}
    	}
    	return templates;
    }

    protected IActivityTemplate createTemplateFromModel(ActivityTemplateModel model) {
        IActivityTemplate result = null;
        if (model == null)
            return null;

        result = this.sysTemplates.get(model.getId());
        if (result != null) {
            //do nothing
        } else if (model.getType().isBpmn()) {
            result = this.worklflowManager.getActivityTemplate(SystemUtils.MWA.BPMNS + model.getExtensionName());
            if (result == null) {
                result = this.worklflowManager.getActivityTemplate(SystemUtils.MWA.CUSTOM_BPMNS + model.getExtensionName());
                if (result == null) {
                	String msg = "BPMN file not found: " + SystemUtils.MWA.BPMNS + model.getExtensionName() + ", " + SystemUtils.MWA.CUSTOM_BPMNS + model.getExtensionName();
                	logger.error(msg);
                	throw new MwaError(MWARC.INTEGRATION_ACTIVITY_NOT_FOUND, null, msg);
                }
            }
        } else if (model.getType().isRule()) {
            String rulePath = SystemUtils.MWA.RULES + model.getExtensionName();
            result = this.ruleManager.getActivityTemplate(rulePath, model.getType());
        }
        else if (model.getType().isBundle()) {
            if (model.getExtensionId() == null) {
                logger.error("Cannot get extensionId from model: model.name=" + model.getName() + ", model.version=" + model.getVersion());
                throw new MwaError(MWARC.INTEGRATION_ACTIVITY_NOT_FOUND);
            }
            Long bundleId = this.bundleIdMap.get(model.getExtensionId());
            if (bundleId != null) {
                Bundle bundle = FrameworkContloller.getBundle(bundleId);
                if (bundle != null) {
                    BundleContext bContext = bundle.getBundleContext();
                    ServiceReference<MwaBundleContentManager> ref = bContext.getServiceReference(MwaBundleContentManager.class);
                    MwaBundleContentManager content = bContext.getService(ref);
                    result = content.getTemplate(bundleId, model.getName(), model.getVersion());
                } else {
                    logger.error("Cannot find out bundle on system: model.name=" + model.getName() + ", model.version=" + model.getVersion());
                    throw new MwaError(MWARC.INTEGRATION_ACTIVITY_NOT_FOUND);
                }
            } else {
                logger.error("Cannot get bundleId from bundleMap: model.name=" + model.getName() + ", model.version=" + model.getVersion());
                throw new MwaError(MWARC.INTEGRATION_ACTIVITY_NOT_FOUND);
            }
        }
        if (result instanceof ActivityTemplate) {
        	((ActivityTemplate)result).setDeleteFlag(model.getDeleteFlag());
        }
        return result;
    }

    protected ActivityTemplateModel getActitvityTemplateCore(String templateName, String templateVersion) {

        List<String> filter = new ArrayList<String>();
        filter.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + templateName);
        if (!StringUtils.isEmpty(templateVersion)) 
        	filter.add("version" + FilterOperatorEnum.EQUAL.toSymbol() + templateVersion);
        else
        	filter.add("extensionVersion" + FilterOperatorEnum.NOT_EQUAL.toSymbol() + "");	//互換性のためextensionVersionなしでtemplateVersionありは許す

        List<String> sorts = Arrays.asList("extensionVersion" + "-");

        return this.activityTemplateDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filter, null, null, null)).first();
    }

    protected ActivityTemplateModel getActitvityTemplateCore(String id) {
        return this.activityTemplateDao.getModel(id, null);
    }

    @Override
    public IActivityTemplate getActitvityTemplateService(String id) {
        return this.createTemplateFromModel(this.getActitvityTemplate(id));
    }

    @Override
    public IActivityTemplate getActitvityTemplateService(String templateName, String templateVersion) {
        return this.createTemplateFromModel(this.getActitvityTemplate(templateName, templateVersion));
    }

    @Override
    public ActivityProviderTypeModel getActitvityProviderType(String providerTypeId) {
        ActivityProviderTypeModel model = this.activityProviderTypeDao.getModel(providerTypeId, null);
        return model;
    }

    @Override
    public ActivityProviderType getActitvityProviderTypeService(String providerTypeId) {
        ActivityProviderTypeModel model = this.activityProviderTypeDao.getModel(providerTypeId, null);
        return createProviderTypeFromModel(model);
    }

    protected ActivityProviderType createProviderTypeFromModel(ActivityProviderTypeModel model) {
        ActivityProviderType result = null;
        if (model == null)
            return null;
        Long bundleId = model.getBundleId();
        Bundle bundle = FrameworkContloller.getBundle(bundleId);
        BundleContext bContext = bundle.getBundleContext();
        ServiceReference<MwaBundleContentManager> ref = bContext.getServiceReference(MwaBundleContentManager.class);
        MwaBundleContentManager content = bContext.getService(ref);
        result = content.getProviderType(bundleId, model.getName(), model.getModelNumber());
        return result;
    }
    @Override
    public ActivityProviderTypeCollection getActivityProviderTypes(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        // TODO Auto-generated method stub
        return this.activityProviderTypeDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
    }
    @Override
    public ResourceRequestModel extractResourceRequest(String templateName, String templateVersion, String activityType, Map<String, Object> parameters) {

        if ("com.sony.pro.rtc.workflow.transfer.salvage".equalsIgnoreCase(templateName)) {
            String src = (String)parameters.get("src");
            UriModel uri = UriUtils.parse(src);
            if (uri != null) {
                uri.getLocation();
            }
            ResourceRequestModel request = new ResourceRequestModel();
            try {
                request.setRequestResourceId(UriUtils.percentEncode(uri.getLocation()).replace("@", "%40") + "@" + "SESSION");
            } catch (UnsupportedEncodingException e) {
                logger.error("Invalid resource: " + uri.getLocation());
            }
            Map<String, Long> values = new HashMap();
            values.put("COUNT", 1L);
            request.setCost(new CostModel().setType("SESSION").setValues(values));
            return request;
        }
        else if ("com.sony.pro.mwa.activity.rct.filemanagement.MirroringRetrControlTask".equalsIgnoreCase(templateName)) {
                String src = (String)parameters.get("src");
                UriModel uri = UriUtils.parse(src);
                if (uri != null) {
                    uri.getLocation();
                }
                ResourceRequestModel request = new ResourceRequestModel();
                try {
                    request.setRequestResourceId(UriUtils.percentEncode(uri.getLocation()).replace("@", "%40") + "@" + "SESSION");
                } catch (UnsupportedEncodingException e) {
                    logger.error("Invalid resource: " + uri.getLocation());
                }
                Map<String, Long> values = new HashMap();
                values.put("COUNT", 1L);
                request.setCost(new CostModel().setType("SESSION").setValues(values));
                return request;
            }

        Map<String, Object> param = new HashMap<>(parameters);
        param.put(PresetParameter.ActivityTemplateName.getKey(), templateName);
        param.put(PresetParameter.ActivityTemplateVersion.getKey(), templateVersion);
        param.put(PresetParameter.ActivityType.getKey(), activityType);
        ResourceRequestModel result =  this.ruleManager.getProcessEngine().startResourceRule("com.sony.pro.mwa.resource.rule.ResourceRequestRule", param);
        return result;
    }


    /**
     * parameter_setテーブルの1件の内容を取得する。
     */
	@Override
	public ParameterSetModel getParameterSet(String parameterSetId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(parameterSetId == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"parameterSetId is required param");
		}

		// 取得処理
		ParameterSetCollection col = null;
		try {
			List<String> filters = Arrays.asList(String.format("%s%s%s", "id", FilterOperatorEnum.EQUAL.toSymbol(), parameterSetId));
			col = this.parameterSetDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null,filters, null, null, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}

		// PKey検索の結果なので想定は、1件 もしくは0件である。
		if(col.getCount() == 1){
			return col.first();
		}else{
			//throw new MwaError(MWARC.INVALID_INPUT,null,"parameter_set is not found. (parameterSetId=" +parameterSetId+")");
			return null;
		}
	}

    /**
     * parameter_setテーブルの内容を列挙して取得する。
     */
	@Override
	public ParameterSetCollection getParameterSets(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		ParameterSetCollection col = null;
		try {
			col = this.parameterSetDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return col;
	}

    /**
     * parameter_setテーブルの追加をする。
     */
	@Override
	public ParameterSetModel registerParameterSet(ParameterSetModel parameterSet) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(parameterSet == null){
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		final String modelStr = ToStringBuilder.reflectionToString(parameterSet, ToStringStyle.DEFAULT_STYLE);

		// ID生成処理
		if (StringUtils.isEmpty(parameterSet.getId())) {
			parameterSet.setId(UUID.randomUUID().toString());
		} else {
			try {
				// UUIDの妥当性チェック
				UUID.fromString(parameterSet.getId());
			} catch (IllegalArgumentException e) {
				throw new MwaError(MWARC.INVALID_INPUT_FORMAT);
			}
		}

		// IDの重複確認処理
		{
			List<String> filters = Arrays.asList(String.format("%s%s%s", "id", FilterOperatorEnum.EQUAL.toSymbol(), parameterSet.getId()));
			ParameterSetCollection checkCol = this.getParameterSets(null, filters, null, null);
			if(checkCol.getCount() > 0){
				// 既にIDが登録されている。
				throw new MwaError(MWARC.INVALID_INPUT,null,"parameterSetId is duplicate value. ParameterSetModel="+modelStr);
			}
		}

//		// NAMEの重複確認処理
//		{
//			List<String> filters = Arrays.asList(String.format("%s%s%s", "parameterSetName",FilterOperatorEnum.EQUAL.toSymbol(),parameterSet.getName()));
//			ParameterSetCollection checkCol = this.getParameterSets(null, filters, null, null);
//			if(checkCol.getCount() > 0){
//				// 既にIDが登録されている。
//				throw new MwaError(MWARC.INVALID_INPUT,null,"parameterSetName is duplicate value. parameterSetModel="+modelStr);
//			}
//		}

		// 登録処理
		ParameterSetModel model = null;
		try {
			model = this.parameterSetDao.addModel(parameterSet);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"register is parameter_set error. parameterSetModel="+modelStr);
		}
		return model;
	}

    /**
     * parameter_setテーブルの更新をする。
     */
	@Override
	public ParameterSetModel updateParameterSet(ParameterSetModel parameterSet) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(parameterSet == null){
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		final String modelStr = ToStringBuilder.reflectionToString(parameterSet, ToStringStyle.DEFAULT_STYLE);

		// 存在確認
		ParameterSetModel modelCtrl = this.getParameterSet(parameterSet.getId());
		if(modelCtrl == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"parameter_set is not found. parameterSetModel="+modelStr);
		}

		// 更新処理
		ParameterSetModel model = null;
		try {
			model = this.parameterSetDao.updateModel(parameterSet);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"update is parameter_set error. parameterSetModel="+modelStr);
		}
		return model;
	}

    /**
     * parameter_setテーブルの1件内容を削除する。
     */
	@Override
	public ParameterSetModel deleteParameterSet(String parameterSetId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(parameterSetId == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"parameterSetId is required param");
		}

		// 存在確認
		ParameterSetModel modelCtrl = this.getParameterSet(parameterSetId);
		if(modelCtrl == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"parameter_set is not found. parameterSetId="+parameterSetId);
		}

		// 削除処理
		try {
			this.parameterSetDao.deleteModel(modelCtrl);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return modelCtrl;
	}

    /**
     * parameter_setテーブルを選択された条件で削除をする。
     */
	@Override
	public ParameterSetCollection deleteParameterSets(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		ParameterSetCollection col = this.getParameterSets(sorts, filters, offset, limit);
		if(col.getCount() == 0){
			throw new MwaError(MWARC.INVALID_INPUT);
		}

		for(ParameterSetModel model : col.getModels()){
			try {
				this.parameterSetDao.deleteModel(model);
			} catch (Throwable e) {
				logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
				throw new MwaError(MWARC.DATABASE_ERROR);
			}
		}
		return col;
	}

	/**
	 * WrokflowDiagramDetailModelを取得する
	 */
	@Override
	public WorkflowDiagramDetailModel getWorkflowDiagram(String workflowDiagramId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowDiagramModel workflowDiagramModel = workflowService.getWorkflowDiagram(workflowDiagramId);
		workflowModelUpdateProvider.updateActivityTemplate(workflowDiagramModel);
		WorkflowDiagramDetailModel workflowDiagramDetailModel = new WorkflowDiagramDetailModel(workflowDiagramModel);
		List<String> filters = new ArrayList<>();
		filters.add("diagramId" + FilterOperatorEnum.EQUAL.toSymbol() + workflowDiagramId);
		List<WorkflowModel> workflowModelList = getWorkflows(null, filters, null, null).getModels();
		workflowDiagramDetailModel.setWorkflows(workflowModelList);
		return workflowDiagramDetailModel;
	}

	/**
	 * WorkflowDiagramModelの一覧を取得する。
	 *
	 * @param sorts ソート条件
	 * @param filters フィルター条件
	 * @param offset オフセット
	 * @param limit 取得する数の上限
	 * @return 取得したWorkflowDiagramModelの一覧
	 */
	@Override
	public WorkflowDiagramCollection getWorkflowDiagrams(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowDiagramCollection workflowDiagramCollection = workflowService.getWorkflowDiagrams(sorts, filters, offset, limit);
		for (WorkflowDiagramModel workflowDiagramModel: workflowDiagramCollection.getModels()) {
			workflowModelUpdateProvider.updateActivityTemplate(workflowDiagramModel);
		}
		return workflowDiagramCollection;
	}

	/**
	 * WorkflowDiagramModelを新規に追加する
	 *
	 * @param workflowDiagramModelJson WorkflowModelDiagramのJson
	 * @return 作成したWorkflowDiagramModel
	 */
	@Override
	public WorkflowDiagramModel registerWorkflowDiagram(String workflowDiagramModelJson) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		
		// 初期状態workflowの生成
		String newWfModel = workflowService.createInitialWorkflow(
				workflowModelUpdateProvider.getActivityTemplate(KBaseUtil.START_TASK_WITH_ASSET, "1"),
				workflowModelUpdateProvider.getActivityTemplate(KBaseUtil.END_TASK, "1"), workflowDiagramModelJson);
		
		WorkflowDiagramModel workflowDiagramModel = workflowService.registerWorkflowDiagram(newWfModel);
		workflowModelUpdateProvider.updateActivityTemplate(workflowDiagramModel);
		return workflowDiagramModel;
	}

	/**
	 * WorkflowDiagramModelの更新
	 *
	 * @param workflowDiagramId 更新するWorkflowDiagramModel
	 * @param workflowDiagramModelJson パラメータ
	 * @return 更新したWorkflowDiagramModel
	 */
	@Override
	public WorkflowDiagramModel updateWorkflowDiagram(String workflowDiagramId, String workflowDiagramModelJson) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowDiagramModel workflowDiagramModel = workflowService.updateWorkflowDiagram(workflowDiagramId, workflowDiagramModelJson);
		workflowModelUpdateProvider.updateActivityTemplate(workflowDiagramModel);
		return workflowDiagramModel;
	}

	/**
	 * WorkflowDiagramModelの削除
	 *
	 * @return 削除されたWorkflowDiagramのCollection
	 */
	@Override
	public WorkflowDiagramCollection deleteWorkflowDiagram(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowDiagramCollection workflowDiagramCollection = workflowService.deleteWorkflowDiagram(sorts, filters, offset, limit);
		for (WorkflowDiagramModel workflowDiagramModel: workflowDiagramCollection.getModels()) {
			workflowModelUpdateProvider.updateActivityTemplate(workflowDiagramModel);
		}
		return workflowDiagramCollection;
	}

	/**
	 * WorkflowDiagramModelに対するDeploy/Undeployを実行する
	 */
	@Override
	public WorkflowDiagramModel operateWorkflowDiagram(String operation, String workflowDiagramId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowDiagramModel workflowDiagramModel = null;
		if(WorkFlowOperationEnum.DEPLOY.name().equals(operation)){
			workflowDiagramModel = deployWorkflowDiagram(workflowDiagramId);
		}else if(WorkFlowOperationEnum.UNDEPLOY.name().equals(operation)){
			workflowDiagramModel = undeployWorkflowDiagram(workflowDiagramId);
		}else{
			final String errorMessage = "operation is invalid input error.";
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT, null, errorMessage);
		}
		workflowModelUpdateProvider.updateActivityTemplate(workflowDiagramModel);
		return workflowDiagramModel;
	}

	// UIが乗り換えるまでの暫定実装。乗り換え後、削除するメソッド
	/**
	 * 指定されたWorkflowDiagramModelに紐づくWorkflowModelとBPMNを生成し、そのBOMNをロードする。
	 *
	 * @param workflowDiagramId WorkflowDiagramModelのID
	 * @return 指定されたWorkflowDiagramModel
	 */
	private WorkflowDiagramModel deployWorkflowDiagram(String workflowDiagramId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowDiagramModel workflowDiagramModel = workflowService.deployWorkflowDiagram(workflowDiagramId);

		List<String> getFilters = Arrays.asList("diagramId==" + workflowDiagramModel.getId(), "version==" + workflowDiagramModel.getVersion());
		List<String> sorts = Arrays.asList("createTime" + "-");
		WorkflowModel workflowModel  = new WorkflowModel();
		WorkflowModelCollection workflowModelCollection = this.getWorkflows(sorts, getFilters, 0, 50);

		for (WorkflowModel model: workflowModelCollection.getModels()) {
			 workflowModel = model;
			 break;
		}

		WorkflowModel deployResult = this.deployWorkflow(workflowModel);
		return deployResult;
	}

	/**
	 * 指定されたWorkflowDiagramModelに紐づくWorkflowModelを使用不可能にする。
	 *
	 * @param workflowDiagramId WorkflowDiagramModelのID
	 * @return 指定されたWorkflowDiagramModel
	 */
	private WorkflowDiagramModel undeployWorkflowDiagram(String workflowDiagramId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		
		List<String> filters = Arrays.asList("diagramId==" + workflowDiagramId);
		List<String> sorts = Arrays.asList("createTime" + "-");

		WorkflowModelCollection workflowModelCollection = this.getWorkflows(sorts, filters, 0, 50);
		for (WorkflowModel workflowModel: workflowModelCollection.getModels()) {
			if (StringUtils.isEmpty(workflowModel.getTemplateId())) {
				final String errorMessage = "The workflow %S is not associated the activity template.";
				logger.error(errorMessage);
				// Exceptionをthrowするか検討
//				throw new MwaError(MWARC.INVALID_INPUT,null,errorMessage);
			}
			// Activity Templateの取得
			ActivityTemplateModel activityTemplateModel = getActitvityTemplate(workflowModel.getTemplateId());
			activityTemplateModel.setDeleteFlag(Boolean.TRUE);

			// ActivityTempldateの更新
			try {
				this.activityTemplateDao.updateModel(activityTemplateModel);
			} catch (Throwable e) {
				logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
				// Exceptionをthrowするか検討
				//throw new MwaError(MWARC.DATABASE_ERROR);
			}

			// WorkflowModelの更新
			WorkflowDaoModel workflowDaoModel = workflowService.getWorkflowDaoModel(workflowModel.getId());
			workflowDaoModel.setDeleteFlag(Boolean.TRUE);
			workflowService.updateWorkflowDaoModel(workflowDaoModel);
		}
		return workflowService.getWorkflowDiagram(workflowDiagramId);
	}

	/**
	 * 指定されたWorkflowModelを使用不可能にする。
     *
	 * @return 指定されたWorkflowDiagramModel
	 */
	@Override
	public WorkflowModelCollection deleteWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);

		WorkflowModelCollection workflowModelCollection = this.getWorkflows(sorts, filters, offset, limit);
		List<WorkflowModel> workflowModelList = new ArrayList<>();
		for (WorkflowModel workflowModel: workflowModelCollection.getModels()) {
			// WorkflowModelの更新
			WorkflowDaoModel workflowDaoModel = workflowService.getWorkflowDaoModel(workflowModel.getId());
			workflowDaoModel.setDeleteFlag(Boolean.TRUE);
			workflowService.updateWorkflowDaoModel(workflowDaoModel);
			workflowModel.setDeleteFlag(Boolean.TRUE);
			workflowModelList.add(workflowModel);
			
			if (StringUtils.isEmpty(workflowModel.getTemplateId())) {
				final String errorMessage = String.format("The workflow %s is not associated the activity template.", workflowModel.getName());
				logger.error(errorMessage);
				throw new MwaInstanceError(MWARC.INVALID_INPUT, null, errorMessage);
			}
			// Activity Templateの取得
			ActivityTemplateModel activityTemplateModel = getActitvityTemplate(workflowModel.getTemplateId());
			activityTemplateModel.setDeleteFlag(Boolean.TRUE);

			// ActivityTempldateの更新
			try {
				this.activityTemplateDao.updateModel(activityTemplateModel);
			} catch (Throwable e) {
				logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
				// Exceptionをthrowするか検討
				//throw new MwaError(MWARC.DATABASE_ERROR);
			}
		}
		workflowModelCollection.setModels(workflowModelList);
		return workflowModelCollection;
	}

    /**
     * workflowテーブルの内容を列挙して取得する。
     */
	@Override
	public WorkflowModelCollection getWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		WorkflowModelCollection result = workflowService.getWorkflows(sorts, filters, offset, limit);
		for (WorkflowModel workflowModel: result.getModels()) {
			workflowModelUpdateProvider.updateActivityTemplate(workflowModel);
			List<StepModel> stepList = workflowModel.getSteps();
			for (StepModel step: stepList) {
				// 2017.3にてWorkflow Monitor画面でWFのステップのアイコンがでないので暫定対応
				if (step != null && step.getActivity() != null) {
					for (Map.Entry<String, Object> templateValue: step.getActivity().entrySet()) {
						if ("icon".equals(templateValue.getKey())) {
							String icon = (String) templateValue.getValue();
							if (icon.isEmpty()) {
								icon = "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-6%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cg%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M27.7%2C17.8c0-0.6%2C0-1.1-0.1-1.6L31%2C13l-2.4-4.2l-4.4%2C1.4c-0.8-0.7-1.8-1.4-2.8-1.7l-1-4.6h-4.8l-1%2C4.6%20c-1%2C0.4-2%2C1-2.8%2C1.7L7.4%2C8.8L5%2C13l3.5%2C3.1c-0.1%2C0.5-0.1%2C1.1-0.1%2C1.6c0%2C0.6%2C0%2C1.1%2C0.1%2C1.6L5%2C22.5l2.4%2C4.2l4.4-1.4%20c0.8%2C0.7%2C1.8%2C1.6%2C2.8%2C2l1%2C4.8h4.8l1-4.8c1-0.4%2C2-1.1%2C2.8-1.8l4.4%2C1.4l2.4-4.2l-3.5-3.1C27.7%2C18.9%2C27.7%2C18.3%2C27.7%2C17.8z%20M18%2C23%20c-2.8%2C0-5-2.2-5-5s2.2-5%2C5-5s5%2C2.2%2C5%2C5S20.8%2C23%2C18%2C23z%22%2F%3E%20%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";
								step.getActivity().put("icon", icon);
								break;
							}
						}
					}
				}
			}

		}
		return result;
	}

    /**
     * workflowテーブルの内容を取得する。
	 * @param workflowId WorkflowModelのID
	 * @return 指定されたWorkflowModel
     */
	@Override
	public WorkflowDetailModel getWorkflow(String workflowId) {

		// WorkflowModel の取得
		WorkflowModel model = workflowService.getWorkflow(workflowId);

		// DB から取得した Template 情報を WorkflowModel に反映
		workflowModelUpdateProvider.updateActivityTemplate(model);

		// ActivityProfileGroup と ActivityProfile を設定するため、WorkflowDetailModel へデータを詰め直す
		WorkflowDetailModel detailModel = new WorkflowDetailModel(model);

		if (detailModel.getId() != null && !detailModel.getId().isEmpty()) {
			List<String> filters = new ArrayList<String>();
			filters.add("templateId" + FilterOperatorEnum.EQUAL.toSymbol() + detailModel.getId());

			// Workflow に紐付いている ActivityProfileGroup の取得
			List<ActivityProfileGroupModel> workflowProfileGroupList = this.getActivityProfileGroups(null, filters, null, null).getModels();
			detailModel.setProfileGroups(workflowProfileGroupList);

			filters.add("groupId" + FilterOperatorEnum.EQUAL.toSymbol());

			// Workflow に紐付いている ActivityProfile の取得
			List<ActivityProfileModel> workflowProfileList = this.getActivityProfiles(null, filters, null, null).getModels();
			detailModel.setProfiles(workflowProfileList);
		} else {
			detailModel.setProfileGroups(new ArrayList<ActivityProfileGroupModel>());
			detailModel.setProfiles(new ArrayList<ActivityProfileModel>());
		}

		List<StepModel> stepList = detailModel.getSteps();
		for (StepModel step: stepList) {
			// 2017.3にてWorkflow Monitor画面でWFのステップのアイコンがでないので暫定対応
			if (step != null && step.getActivity() != null) {
				for (Map.Entry<String, Object> templateValue: step.getActivity().entrySet()) {
					if ("icon".equals(templateValue.getKey())) {
						String icon = (String) templateValue.getValue();
						if (icon.isEmpty()) {
							icon = "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-6%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cg%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M27.7%2C17.8c0-0.6%2C0-1.1-0.1-1.6L31%2C13l-2.4-4.2l-4.4%2C1.4c-0.8-0.7-1.8-1.4-2.8-1.7l-1-4.6h-4.8l-1%2C4.6%20c-1%2C0.4-2%2C1-2.8%2C1.7L7.4%2C8.8L5%2C13l3.5%2C3.1c-0.1%2C0.5-0.1%2C1.1-0.1%2C1.6c0%2C0.6%2C0%2C1.1%2C0.1%2C1.6L5%2C22.5l2.4%2C4.2l4.4-1.4%20c0.8%2C0.7%2C1.8%2C1.6%2C2.8%2C2l1%2C4.8h4.8l1-4.8c1-0.4%2C2-1.1%2C2.8-1.8l4.4%2C1.4l2.4-4.2l-3.5-3.1C27.7%2C18.9%2C27.7%2C18.3%2C27.7%2C17.8z%20M18%2C23%20c-2.8%2C0-5-2.2-5-5s2.2-5%2C5-5s5%2C2.2%2C5%2C5S20.8%2C23%2C18%2C23z%22%2F%3E%20%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";
							step.getActivity().put("icon", icon);
							break;
						}
					}
				}
			}
		}

		return detailModel;
	}

	/**
	 * WorkflowModelの更新
	 *
	 * @param workflowId 更新するWorkflowModelのid
	 * @param workflowModelJson パラメータ
	 * @return 更新したWorkflowModel
	 */
	@Override
	public WorkflowModel updateWorkflow(String workflowId, String workflowModelJson) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowModel model = workflowService.updateWorkflow(workflowId, workflowModelJson);
		workflowModelUpdateProvider.updateActivityTemplate(model);
		return model;
	}

	@Override
	public WorkflowModel registerWorkflow(String workflowModelJson ,String workflowDiagramId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		WorkflowDiagramModel workflowDiagramModel =  createWorkflowDiagramModel(workflowModelJson, workflowDiagramId);
        this.updateWorkflowDiagram(workflowDiagramId,WorkflowDiagramModelConverter.serializeWfModel(workflowDiagramModel));
        WorkflowModel deserializedWorkflowModel = KBaseUtil.deserializeWfModel(workflowModelJson);
        String workflowAlias = deserializedWorkflowModel.getAlias();
        List<String> filters = new ArrayList<>();
        filters.add("deleteFlag==" + false);
        filters.add("alias=@" + workflowAlias);
        WorkflowModelCollection workflowModels = this.getWorkflows(null, filters, null, null);
        // WorkflowModelが取得できなかった場合は、Indexを付与する必要がないためIndex生成処理をスキップ
        if (workflowModels.getCount() != 0) {
            // 取得したWorkfloeModelのListに、aliasが完全に一致するWorkflowModelが存在しない場合はIndex付与の必要なし
            if (checkAliasExist(workflowModels, workflowAlias)) {
                Integer index = createWorkflowIndex(workflowModels, workflowAlias);
                deserializedWorkflowModel.setAlias(workflowAlias + "-" + index.toString());
            }
        }
        String serealizedWorkflowModel = KBaseUtil.serializeWfModel(deserializedWorkflowModel);
		WorkflowModel workflowModel = workflowService.registerWorkflow(serealizedWorkflowModel, workflowDiagramId);
		try {
			this.deployWorkflow(workflowModel);
			workflowModelUpdateProvider.updateActivityTemplate(workflowModel);
		} catch (Exception e) {
			logger.error("As a rollback process, delete target workflowModel from database.", e);
			workflowService.deleteWorkflow(workflowModel);
//			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Succeeded to updating workflow diagram, but failed to deploy workflow.\r\n Invalid to workflow diagram or exist IN_PROGRESS workflow.");
            throw e;
		}
		return workflowModel;
	}

	private boolean canLoadBpmns() {
	    List<String> filters = new ArrayList<>();
	    filters.add("status_terminated==false");
	    ActivityInstanceCollection activityInstanceCollection = this.activityManager.getInstances(new ArrayList<>(), filters, 0, null);
	    return activityInstanceCollection.getModels().size() == 0;
    }

	/**
	 * WorkflowDiagramModelからWorkflowModelを生成し、BPMNを出力。その後そのBPMNをloadする。
	 *
	 * @param workflowModel WorkflowModelの生成元となるWorkflowDiagramModel
	 * @return 生成したWorkflowModel
	 */
	private WorkflowModel deployWorkflow(WorkflowModel workflowModel) {
	    if (!canLoadBpmns()) {
	        throw new MwaInstanceError(MWARC.OPERATION_FAILED_DEPLOY_NOT_ACCEPT, null, "Failed to deploy workflow, because Workflow of status QUEUED or IN_PROGRESS is exist.");
        }
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);

		// bpmn.xml生成
		BpmnManager bpmnManager = BpmnManager.getInstance();
        // fileName 生成
        String fileName = String.format("%s-%s.bpmn", workflowModel.getName(), workflowModel.getVersion());
		try {
            String xml = bpmnManager.generateBpmn(workflowModel);

            logger.info("fileName" + workflowModel.getName());
            logger.info("fileVersion" + workflowModel.getVersion());

            // bpmn生成
            bpmnManager.deployBpmn(xml, fileName);
        } catch (MwaError e) {
		    throw new MwaInstanceError(MWARC.INVALID_INPUT, null, e.getDeviceResponseDetails());
        }

		// worlflow_load
		this.loadBpmns();

		// extensionと紐付け
		List<String> filters = new ArrayList<>();
		filters.add("type" + FilterOperatorEnum.EQUAL.toSymbol() + "BPMN");
		filters.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + fileName);
		filters.add("version" + FilterOperatorEnum.EQUAL.toSymbol() + workflowModel.getVersion());
		ExtensionCollection extensionCollection = this.getExtensions(null, filters, null, null);
		if(extensionCollection.getCount() != 1){
			String message = "bpmn load faild: fileName=" + fileName + ", version=" + workflowModel.getVersion();
			logger.error(message);
			throw new MwaError(MWARC.INTEGRATION_ERROR,null,message);
		}
		ExtensionModel extension = extensionCollection.first();
		final String extensionId = extension.getId();

		// ExtensionContentの取得
		ExtensionContentCollection col = null;
		try {
			List<String> extensionfilters = new ArrayList<>();
			extensionfilters.add("extensionId" + FilterOperatorEnum.EQUAL.toSymbol() + extensionId);
			extensionfilters.add("contentType" + FilterOperatorEnum.EQUAL.toSymbol() + ExtensionContentType.ACTIVITY_TEMPLATE.name());
			col = this.getExtensoinContentDao().getModels(QueryCriteriaGenerator.getQueryCriteria(null, extensionfilters, null, null, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}

		// 紐付いているTemplateIdの取得
		String templateId = null;
		if(col.getCount() != 0){
			for(ExtensionContentModel extensionContentModel:col.getModels()){
				templateId = extensionContentModel.getExtensionContentId();
				break;
			}
		}

		// workflowModelにTemplateIdをセット
		workflowModel.setTemplateId(templateId);
		Long updateTime = System.currentTimeMillis();
		workflowModel.setUpdateTime(updateTime);

		final String modelStr = ToStringBuilder.reflectionToString(workflowModel, ToStringStyle.DEFAULT_STYLE);

		// 更新用WorkflowDaoModel取得
		// WorkflowDaoModel workflowDaoModel = getWorkflowDaoModel(workflowModel.getId());
		WorkflowDaoModel workflowDaoModel = workflowService.getWorkflowDaoModel(workflowModel.getId());
		{//暫定対応
			List<WFParameterDefinition> inputs = workflowModel.getInputs();
			List<WFParameterDefinition> outputs = workflowModel.getOutputs();
			List<String> reservedKeyNames = Arrays.asList(
					PresetParameter.ProfileId.name(),
					PresetParameter.ActivityInstanceStatus.name(),
					PresetParameter.ActivityInstanceStatusDetails.name(),
					PresetParameter.ActivityInstanceDeviceResponse.name(),
					PresetParameter.ActivityInstanceDeviceResponseDetails.name()
					);
			String startStepId = "InvalidId";
			String endStepId = "InvalidId";
			for (StepModel step : workflowModel.getSteps()) {
				if ("Start".equalsIgnoreCase(step.getName())) {
					startStepId = step.getId();
				} else if ("End".equalsIgnoreCase(step.getName())) {
					endStepId = step.getId();
				}
			}

			if ( workflowModel.getConnectionPorts() != null) {
				//UIで行うのでこのロジックはDisableにします
/*				for (ConnectionPortModel port : workflowModel.getConnectionPorts()) {
					if (startStepId.equalsIgnoreCase(port.getStepId()) && "DATA_OUTPUT".equalsIgnoreCase(port.getType()) && !reservedKeyNames.contains(port.getParameter().getKey())) {
						WFParameterDefinition param = new WFParameterDefinition();
						param.setKey(port.getParameter().getKey());
						param.setRequired(port.getParameter().getRequired());
						param.setTypeName(port.getParameter().getTypeName());
						inputs.add(param);
					} else if (endStepId.equalsIgnoreCase(port.getStepId()) && "DATA_INPUT".equalsIgnoreCase(port.getType()) && !reservedKeyNames.contains(port.getParameter().getKey())) {
						WFParameterDefinition param = new WFParameterDefinition();
						param.setKey(port.getParameter().getKey());
						param.setRequired(port.getParameter().getRequired());
						param.setTypeName(port.getParameter().getTypeName());
						outputs.add(param);
					}
				}*/
			}
			workflowModel.setInputs(inputs);
			workflowModel.setOutputs(outputs);
		}

		// 更新用JsonModelの生成
		String updateJsonModel = KBaseUtil.serializeWfModel(workflowModel);

		// 更新用WorkflowDaoModelのセット
		workflowDaoModel.setExtensionId(extensionId);
		workflowDaoModel.setModel(updateJsonModel);
		workflowDaoModel.setUpdateTime(updateTime);

		// Workflow更新処理
		WorkflowDaoModel model = null;
		try {
			// model = this.workflowDao.updateModel(workflowDaoModel);
			model = workflowService.updateWorkflowDaoModel(workflowDaoModel);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if( model == null){
			final String errMsg = "update is workflowVersion error. workflowVersionDaoModel="+modelStr;
			logger.error(errMsg);
			throw new MwaError(MWARC.INVALID_INPUT,null,errMsg);
		}


		// 登録用WorkFlowModelの生成
		WorkflowModel insertWorkflowModel = null;
		try {
			insertWorkflowModel = KBaseUtil.deserializeWfModel(updateJsonModel);
		} catch (MwaError e) {
			throw e;
		}

		// Workflow登録
		insertWorkflowModel.setId(UUID.randomUUID().toString());
		insertWorkflowModel.setCreateTime(null);
		insertWorkflowModel.setUpdateTime(null);

		// 登録用JsonModelの生成
		String insertJsonModel = KBaseUtil.serializeWfModel(insertWorkflowModel);
//		this.registerWorkflow(insertJsonModel, insertWorkflowModel.getVersion());

		return workflowModel;
	}

    /**
     * activity_profile_groupテーブルの1件の内容を取得する。
     */
	@Override
	public ActivityProfileGroupDetailModel getActivityProfileGroup(String activityProfileGroupId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(activityProfileGroupId == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"activityProfileGroupId is required param");
		}

		// 取得処理
		ActivityProfileGroupCollection col = null;
		try {
			List<String> filters = Arrays.asList(String.format("%s%s%s", "id", FilterOperatorEnum.EQUAL.toSymbol(), activityProfileGroupId));
			col = this.activityProfileGroupDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null,filters, null, null, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}

		// PKey検索の結果なので想定は、1件 もしくは0件である。
		if(col.getCount() != 1){
			return null;
		}

		ActivityProfileGroupDetailModel detailModel = new ActivityProfileGroupDetailModel(col.first());
		List<String> filters = Arrays.asList(String.format("%s%s%s", "groupId", FilterOperatorEnum.EQUAL.toSymbol(), activityProfileGroupId));
		List<ActivityProfileModel> activityProfileList = getActivityProfiles(null, filters, null, null).getModels();
		detailModel.setProfiles(activityProfileList);

		return detailModel;
	}

    /**
     * activity_profile_groupテーブルの内容を列挙して取得する。
     */
	@Override
	public ActivityProfileGroupCollection getActivityProfileGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		ActivityProfileGroupCollection col = null;
		try {
			col = this.activityProfileGroupDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return col;
	}

    /**
     * activity_profile_groupテーブルの追加をする。
     */
	@Override
	public ActivityProfileGroupModel registerActivityProfileGroup(ActivityProfileGroupModel activityProfileGroup) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(activityProfileGroup == null){
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		final String modelStr = ToStringBuilder.reflectionToString(activityProfileGroup, ToStringStyle.DEFAULT_STYLE);

		// ID生成処理
		if (StringUtils.isEmpty(activityProfileGroup.getId())) {
			activityProfileGroup.setId(UUID.randomUUID().toString());
		} else {
			try {
				// UUIDの妥当性チェック
				UUID.fromString(activityProfileGroup.getId());
			} catch (IllegalArgumentException e) {
				throw new MwaError(MWARC.INVALID_INPUT_FORMAT);
			}
		}

		// IDの重複確認処理
		{
			List<String> filters = Arrays.asList(String.format("%s%s%s", "id", FilterOperatorEnum.EQUAL.toSymbol(), activityProfileGroup.getId()));
			ActivityProfileGroupCollection checkCol = this.getActivityProfileGroups(null, filters, null, null);
			if(checkCol.getCount() > 0){
				// 既にIDが登録されている。
				throw new MwaError(MWARC.INVALID_INPUT,null,"activityProfileGroupId is duplicate value. activityProfileGroupModel="+modelStr);
			}
		}

		// 登録処理
		ActivityProfileGroupModel model = null;
		try {
			model = this.activityProfileGroupDao.addModel(activityProfileGroup);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"register is activity_profile_group error. activityProfileGroupModel="+modelStr);
		}
		return model;
	}

    /**
     * activity_profile_groupテーブルの更新をする。
     */
	@Override
	public ActivityProfileGroupModel updateActivityProfileGroup(ActivityProfileGroupModel activityProfileGroup) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(activityProfileGroup == null){
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		final String modelStr = ToStringBuilder.reflectionToString(activityProfileGroup, ToStringStyle.DEFAULT_STYLE);

		// 存在確認
		ActivityProfileGroupModel modelCtrl = this.getActivityProfileGroup(activityProfileGroup.getId());
		if(modelCtrl == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"activity_profile_group is not found. activityProfileGroupModel="+modelStr);
		}

		// 更新処理
		ActivityProfileGroupModel model = null;
		try {
			model = this.activityProfileGroupDao.updateModel(activityProfileGroup);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"update is activity_profile_group error. activityProfileGroupModel="+modelStr);
		}
		return model;
	}

    /**
     * activity_profile_groupテーブルを選択された条件で削除をする。
     */
	@Override
	public ActivityProfileGroupCollection deleteActivityProfileGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		ActivityProfileGroupCollection col = this.getActivityProfileGroups(sorts, filters, offset, limit);
		if(col.getCount() == 0){
			throw new MwaError(MWARC.INVALID_INPUT);
		}

		for(ActivityProfileGroupModel model : col.getModels()){
			try {
				this.activityProfileGroupDao.deleteModel(model);
			} catch (Throwable e) {
				logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
				throw new MwaError(MWARC.DATABASE_ERROR);
			}
		}
		return col;
	}

    /**
     * activity_profileテーブルの1件の内容を取得する。
     */
	@Override
	public ActivityProfileModel getActivityProfile(String activityProfileId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(activityProfileId == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"activityProfileId is required param");
		}

		// 取得処理
		ActivityProfileCollection col = null;
		try {
			List<String> filters = Arrays.asList(String.format("%s%s%s", "id", FilterOperatorEnum.EQUAL.toSymbol(), activityProfileId));
			col = this.activityProfileDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null,filters, null, null, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}

		// PKey検索の結果なので想定は、1件 もしくは0件である。
		if(col.getCount() == 1){
			return col.first();
		}else{
			return null;
		}
	}

    /**
     * activity_profileテーブルの内容を列挙して取得する。
     */
	@Override
	public ActivityProfileCollection getActivityProfiles(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		ActivityProfileCollection col = null;
		try {
			col = this.activityProfileDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return col;
	}

    /**
     * activity_profileテーブルの追加をする。
     */
	@Override
	public ActivityProfileModel registerActivityProfile(ActivityProfileModel activityProfile) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(activityProfile == null || StringUtils.isBlank(activityProfile.getTemplateId())) {
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		final String modelStr = ToStringBuilder.reflectionToString(activityProfile, ToStringStyle.DEFAULT_STYLE);

		// ID生成処理
		if (StringUtils.isEmpty(activityProfile.getId())) {
			activityProfile.setId(UUID.randomUUID().toString());
		} else {
			try {
				// UUIDの妥当性チェック
				UUID.fromString(activityProfile.getId());
			} catch (IllegalArgumentException e) {
				throw new MwaError(MWARC.INVALID_INPUT_FORMAT);
			}
		}

		// IDの重複確認処理
		{
			List<String> filters = Arrays.asList(String.format("%s%s%s", "id", FilterOperatorEnum.EQUAL.toSymbol(), activityProfile.getId()));
			ActivityProfileCollection checkCol = this.getActivityProfiles(null, filters, null, null);
			if(checkCol.getCount() > 0){
				// 既にIDが登録されている。
				throw new MwaError(MWARC.INVALID_INPUT,null,"activityProfileId is duplicate value. activityProfileModel="+modelStr);
			}
		}

		// 登録処理
		ActivityProfileModel model = null;
		try {
			model = this.activityProfileDao.addModel(activityProfile);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"register is activity_profile error. activityProfileModel="+modelStr);
		}
		return model;
	}

    /**
     * activity_profileテーブルの更新をする。
     */
	@Override
	public ActivityProfileModel updateActivityProfile(ActivityProfileModel activityProfile) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		//if(activityProfile == null || StringUtils.isBlank(activityProfile.getTemplateId())) {
		if(activityProfile == null) {
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		final String modelStr = ToStringBuilder.reflectionToString(activityProfile, ToStringStyle.DEFAULT_STYLE);

		// 存在確認
		ActivityProfileModel modelCtrl = this.getActivityProfile(activityProfile.getId());
		if(modelCtrl == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"activity_profile is not found. activityProfileModel="+modelStr);
		}

		// 更新処理
		ActivityProfileModel model = null;
		try {
			model = this.activityProfileDao.updateModel(activityProfile);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"update is activity_profile error. activityProfileModel="+modelStr);
		}
		return model;
	}

    /**
     * activity_profileテーブルを選択された条件で削除をする。
     */
	@Override
	public ActivityProfileCollection deleteActivityProfiles(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		ActivityProfileCollection col = this.getActivityProfiles(sorts, filters, offset, limit);
		if(col.getCount() == 0){
			throw new MwaError(MWARC.INVALID_INPUT);
		}

		for(ActivityProfileModel model : col.getModels()){
			try {
				this.activityProfileDao.deleteModel(model);
			} catch (Throwable e) {
				logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
				throw new MwaError(MWARC.DATABASE_ERROR);
			}
		}
		return col;
	}


    /**
     * parameter_typeテーブルの追加をする。
     */
	@Override
	public ParameterTypeModel registerParameterType(ParameterTypeModel parameterType) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(parameterType == null){
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		final String modelStr = ToStringBuilder.reflectionToString(parameterType, ToStringStyle.DEFAULT_STYLE);

		// 登録処理
		ParameterTypeModel model = null;
		try {
			model = this.parameterTypeDao.addModel(parameterType);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			throw new MwaError(MWARC.INVALID_INPUT,null,"register is parameter_type error. parameterTypeModel="+modelStr);
		}
		return model;
	}

    /**
     * parameter_typeテーブル
     */
	@Override
	public ParameterTypeCollection getParameterTypes(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		ParameterTypeCollection col = null;
		try {
			col = this.parameterTypeDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return col;
	}

	@Override
	public ParameterTypeModel getParameterType(String id) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
        List<String> filters = new ArrayList<String>();
        filters.add("id" + FilterOperatorEnum.EQUAL.toSymbol() + id);

		ParameterTypeCollection col = null;
		try {
			col = this.parameterTypeDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filters, 0, 1, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return (col != null) ? col.first() : null;
	}

	@Override
	public ParameterTypeModel getParameterTypeByName(String name) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
        List<String> filters = new ArrayList<String>();
        filters.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + name);
        List<String> sorts = Arrays.asList("version" + "-");

		ParameterTypeCollection col = null;
		try {
			col = this.parameterTypeDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, 0, 1, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return (col != null) ? col.first() : null;
	}

	private WorkflowDiagramModel createWorkflowDiagramModel(String workflowModelJson, String workflowDiagramId) {
        WorkflowDiagramModel workflowDiagramModel = getWorkflowDiagram(workflowDiagramId);
        WorkflowDiagramModel deserializedWorkflowDiagramModel = WorkflowDiagramModelConverter.deserialize(workflowModelJson);
        deserializedWorkflowDiagramModel.setId(workflowDiagramId);
        deserializedWorkflowDiagramModel.setName(workflowDiagramModel.getName());
        return deserializedWorkflowDiagramModel;
    }

    /** Workflowに付与するIndexを生成します
     * @param workflowModels WorkflowModelのList
     * @param workflowAlias Workflowのalias
     * @return 生成したIndex
     * */
	private Integer createWorkflowIndex(WorkflowModelCollection workflowModels, String workflowAlias) {
        List<Integer> numberList = new ArrayList<>();
        for (WorkflowModel model : workflowModels.getModels()) {
            // Workflow名を空文字に置き換えることでIndexを抽出
            String index = model.getAlias().replace(workflowAlias, "");
            if (!index.isEmpty()) {
                // 抽出したIndexから"-"を除去
                String formattedIndex = index.startsWith("-") ? index.substring(1, index.length()) : index;
                // 抽出したindexが数値のみで構成されているか判定
                Pattern p = Pattern.compile("^[0-9]*$");
                Matcher m = p.matcher(formattedIndex);
                if (m.find()) {
                    // indexが数値のみで構成されている場合はnumberListに格納
                    numberList.add(Integer.parseInt(formattedIndex));
                }
            }
        }
        // numberListから最大値を取得、incrementを行いreturn
        return numberList.size() == 0 ? 1 : numberList.stream().max((a, b) -> a.compareTo(b)).get() + 1;
    }

    /** 同名のWorkflowが存在するかチェックします
     * @param workflowModels チェック対象のWorkflowModelのList
     * @param workflowName チェック対象Workflowのname
     * @return 真偽地
     * */
	private boolean checkAliasExist(WorkflowModelCollection workflowModels, String workflowName) {
        List<String> aliasList = new ArrayList<>();
        for (WorkflowModel model : workflowModels.getModels()) {
            aliasList.add(model.getAlias());
        }
        return aliasList.contains(workflowName);
    }
}
