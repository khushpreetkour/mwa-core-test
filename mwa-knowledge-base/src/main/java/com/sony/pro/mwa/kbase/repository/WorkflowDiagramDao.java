package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoCollection;
import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface WorkflowDiagramDao extends ModelBaseDao<WorkflowDiagramDaoModel, WorkflowDiagramDaoCollection>{

}
