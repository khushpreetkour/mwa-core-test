package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import com.sony.pro.mwa.kbase.utils.BpmnDefinition;

@XmlType(propOrder = { "type", "id", "language", "conditionExpression"})
@XmlRootElement(name = "conditionExpression", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnConditionExpressionModel extends BpmnModelBase {
	private String type;
	private String language;
	private String conditionExpression;
	private String defaultEsc;
	
	public BpmnConditionExpressionModel() {
		this.type = BpmnDefinition.CONDITION_EXPRESSION_TYPE;
	}

	public BpmnConditionExpressionModel(String conditionExpression, Integer index) {
		this.setId("FormalExpression" + index.toString());
		this.type = BpmnDefinition.CONDITION_EXPRESSION_TYPE;
		this.language = BpmnDefinition.CONDITION_EXPRESSION_LANGUAGE_MVEL;
		this.conditionExpression = conditionExpression;
	}

	@XmlAttribute(namespace = "http://www.w3.org/2001/XMLSchema-instance")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlAttribute
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@XmlAttribute(name = "default")
	public String getDefaultEsc() {
		return defaultEsc;
	}

	public void setDefaultEsc(String defaultEsc) {
		this.defaultEsc = defaultEsc;
	}
	
	@XmlValue
	 public String getConditionExpression() {
		 return conditionExpression;
	 }
	 
	 public void setConditionExpression(String conditionExpression) {
		 this.conditionExpression = conditionExpression;
	 }

}
