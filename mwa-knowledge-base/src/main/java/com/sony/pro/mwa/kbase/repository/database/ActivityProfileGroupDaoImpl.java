package com.sony.pro.mwa.kbase.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.kbase.repository.ActivityProfileGroupDao;
import com.sony.pro.mwa.kbase.repository.database.query.ActivityProfileGroupQuery;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupModel;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ActivityProfileGroupDaoImpl extends DatabaseBaseDao<ActivityProfileGroupModel, ActivityProfileGroupCollection, QueryCriteria> implements ActivityProfileGroupDao{

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ActivityProfileGroupCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ActivityProfileGroupModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.activity_profile_group (" +
					"activity_profile_group_id" +
					", activity_template_id" +
					", activity_profile_group_name" +
					", activity_profile_group_icon" +
					", activity_profile_group_create_time" +
					", activity_profile_group_update_time" +
					") " +
			"VALUES (" +
					"?, ?, ?, ?, ?, ?" +
					") ";


	public ActivityProfileGroupModel addModel(final ActivityProfileGroupModel model) {
		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
			new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
					int colNum = 1;

					ps.setObject(colNum++, UUID.fromString(model.getId()));
					if (StringUtils.isNotBlank(model.getTemplateId())) {
						ps.setObject(colNum++, UUID.fromString(model.getTemplateId()));
					} else {
						ps.setObject(colNum++, null);
					}
					ps.setString(colNum++, model.getName());
					ps.setString(colNum++, model.getIcon());

					model.setCreateTime(System.currentTimeMillis());
					model.setUpdateTime(model.getCreateTime());
					ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
					ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));

					logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
					return ps;
				}
			},
		keyHolder);

		ActivityProfileGroupModel addedModel = model;
		return insertedRowCount != 0 ? addedModel : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.activity_profile_group " +
			"SET " +
					"activity_template_id = ?, " +
					"activity_profile_group_name = ?, " +
					"activity_profile_group_icon = ?, " +
					"activity_profile_group_update_time = ? " +
			"WHERE " +
					"activity_profile_group_id = ? ";

	public ActivityProfileGroupModel updateModel(final ActivityProfileGroupModel model) {
		final String updateSql = UPDATE_SQL;
		int updatedRowCount = jdbcTemplate.update(
			new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement ps = conn.prepareStatement(updateSql);
					int colNum = 1;

					if (StringUtils.isNotBlank(model.getTemplateId())) {
						ps.setObject(colNum++, UUID.fromString(model.getTemplateId()));
					} else {
						ps.setObject(colNum++, null);
					}
					ps.setString(colNum++, model.getName());
					ps.setString(colNum++, model.getIcon());

					model.setUpdateTime(System.currentTimeMillis());
					ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));

					ps.setObject(colNum++, UUID.fromString(model.getId()));

					return ps;
				}
			}
		);

		return updatedRowCount != 0 ? model : null;
	}

	private static final String DELETE_ALL_SQL =
			"DELETE FROM " +
					"mwa.activity_profile_group ";
	public void deleteAllModel() {
        if (jdbcTemplate.update(DELETE_ALL_SQL) == 1) {
        	//do nothing
        }
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.activity_profile_group " +
			"WHERE " +
					"activity_profile_group_id = ? ";
	public ActivityProfileGroupModel deleteModel(ActivityProfileGroupModel model) {
		Object[] paramArray = new Object[]{
				UUID.fromString(model.getId().toString())
				};
		if (jdbcTemplate.update(DELETE_SQL, paramArray) > 0) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<ActivityProfileGroupModel, ActivityProfileGroupCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ActivityProfileGroupQuery(jdbcTemplate);
	}

}
