package com.sony.pro.mwa.kbase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.internal.IWorkflowDiagramComponent;
import com.sony.pro.mwa.kbase.model.bpmnmodel.converter.WorkflowDiagramModelConverter;
import com.sony.pro.mwa.kbase.repository.WorkflowDiagramDao;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoCollection;
import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramCollection;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

public class WorkflowDiagramComponent implements IWorkflowDiagramComponent {
	private static WorkflowDiagramComponent singleton = new WorkflowDiagramComponent();
	private final static MwaLogger logger = MwaLogger.getLogger(WorkflowDiagramComponent.class);

	private WorkflowDiagramDao workflowDiagramDao;
    
    private static final String DEFAULT_VERSION = "1";
	
	public static WorkflowDiagramComponent getInstance() {
		return singleton;
	}
	
	@Autowired
	public void setWorkflowDiagramDao(WorkflowDiagramDao workflowDiagramDao) {
		this.workflowDiagramDao = workflowDiagramDao;
	}
    
    public WorkflowDiagramDao getWorkflowDiagramDao() {
    	return this.workflowDiagramDao;
    }
	
	public WorkflowDiagramModel registerWorkflowDiagram(String workflowDiagramModelJson) {
		if(workflowDiagramModelJson == null){
			final String errorMessage = "workflowDiagramModelJson parameter is null.";
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT,null,errorMessage);
		}
		WorkflowDiagramModel workflowDiagramModel = null;
		try {
			workflowDiagramModel = WorkflowDiagramModelConverter.deserialize(workflowDiagramModelJson);
		} catch (MwaError e) {
			throw e;
		}

		// WorkflowId生成処理
		if (StringUtils.isEmpty(workflowDiagramModel.getId())) {
			workflowDiagramModel.setId(UUID.randomUUID().toString());
		}
		
		String validErrorMessage = KBaseUtil.validWorkflowDiagramModel(workflowDiagramModel);
		if (!validErrorMessage.isEmpty()) {
			throw new MwaError(MWARC.INVALID_INPUT, null, validErrorMessage);
		}

		// WorkflowDaoModelに値をセット
		WorkflowDiagramDaoModel workflowDiagramDaoModel = new WorkflowDiagramDaoModel();
		workflowDiagramDaoModel.setWorkflowId(workflowDiagramModel.getId());
		workflowDiagramDaoModel.setDeleteFlag(false);
		workflowDiagramDaoModel.setWorkflowName(workflowDiagramModel.getName());
		workflowDiagramDaoModel.setVersion(DEFAULT_VERSION);
		Long createTime = System.currentTimeMillis();
		workflowDiagramDaoModel.setCreateTime(createTime);
		workflowDiagramDaoModel.setUpdateTime(createTime);

		final String modelStr = ToStringBuilder.reflectionToString(workflowDiagramDaoModel, ToStringStyle.DEFAULT_STYLE);

		// WorkflowIdの重複確認処理
		List<String> filters = new ArrayList<>();
		filters.add(String.format("%s%s%s", "id",FilterOperatorEnum.EQUAL.toSymbol(),workflowDiagramDaoModel.getWorkflowId()));
		WorkflowDiagramCollection checkColletion = null;
		try {
			checkColletion = this.getWorkflowDiagrams(null, filters, null, null);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (checkColletion.getCount() > 0){
			// 既にIDが登録されている。
			final String errorMessage = "workflowDiagramId is duplicate value.  workflowDiagramModel=" + modelStr;
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT, null, errorMessage);
		}

		// WorkflowModelに登録情報を反映
		workflowDiagramModel.setId(workflowDiagramDaoModel.getWorkflowId());
		workflowDiagramModel.setVersion(DEFAULT_VERSION);
		workflowDiagramModel.setCreateTime(createTime);
		workflowDiagramModel.setUpdateTime(createTime);

		// modelの生成
		String updateJsonModel = WorkflowDiagramModelConverter.serializeWfModel(workflowDiagramModel);

		workflowDiagramDaoModel.setModel(updateJsonModel);

		// 登録処理
		WorkflowDiagramDaoModel model = null;
		try {
			model = this.workflowDiagramDao.addModel(workflowDiagramDaoModel);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			final String errorMessage = "register is workflowDiagram error. workflowDiagramModel=" + modelStr;
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT, null, errorMessage);
		}
		return workflowDiagramModel;
	}
	

	public WorkflowDiagramModel updateWorkflowDiagram(String workflowDiagramId, String workflowDiagramModelJson) {
		if(workflowDiagramModelJson == null){
			final String errorMessage = "workflowDiagramModelJson parameter is null.";
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT, null, errorMessage);
		}

		WorkflowDiagramDaoModel workflowDiagramDaoModel = workflowDiagramDao.getModel(workflowDiagramId, null);

		WorkflowDiagramModel updateWorkflowDiagramModel = null;
		try {
			updateWorkflowDiagramModel = WorkflowDiagramModelConverter.deserialize(workflowDiagramModelJson);
		} catch (MwaError e) {
			throw e;
		}
		
		String validErrorMessage = KBaseUtil.validWorkflowDiagramModel(updateWorkflowDiagramModel);
		if (!validErrorMessage.isEmpty()) {
			throw new MwaError(MWARC.INVALID_INPUT, null, validErrorMessage);
		}

		Long updateTime = System.currentTimeMillis();
		updateWorkflowDiagramModel.setUpdateTime(updateTime);

		// 更新用JsonModelの生成
		String updateJsonModel = WorkflowDiagramModelConverter.serializeWfModel(updateWorkflowDiagramModel);
		workflowDiagramDaoModel.setWorkflowName(updateWorkflowDiagramModel.getName());
		workflowDiagramDaoModel.setModel(updateJsonModel);
		workflowDiagramDaoModel.setUpdateTime(updateTime);
		workflowDiagramDaoModel.setVersion(updateWorkflowDiagramModel.getVersion());

		final String modelStr = ToStringBuilder.reflectionToString(workflowDiagramDaoModel, ToStringStyle.DEFAULT_STYLE);

		// Workflow更新処理
		WorkflowDiagramDaoModel model = null;
		try {
			model = this.workflowDiagramDao.updateModel(workflowDiagramDaoModel);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			final String errorMessage = "update is workflowDiagramVersion error. workflowDiagramVersionModel=" + modelStr;
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT,null,errorMessage);
		}

		WorkflowDiagramModel workflowDiagramModel = null;
		try {
			 workflowDiagramModel = WorkflowDiagramModelConverter.deserialize(model.getModel());
		} catch (MwaError e) {
				throw e;
		}
		return workflowDiagramModel;
	}
	
	public WorkflowDiagramModel updateWorkflowDiagramModel(WorkflowDiagramModel model) {
		String modelJson = WorkflowDiagramModelConverter.serializeWfModel(model);
		WorkflowDiagramModel workflowDiagramDaoModel = this.updateWorkflowDiagram(model.getId(), modelJson);
		return workflowDiagramDaoModel;
	}
	
	public WorkflowDiagramModel deleteWorkflowDiagramDaoModel(String daoModelId) {
		WorkflowDiagramDaoModel deleteModel = getWorkflowDiagramDaoModel(daoModelId);
		WorkflowDiagramDaoModel deleteDaoModel = workflowDiagramDao.deleteModel(deleteModel);
		// deleteしたModelをWorkflowDaigramModelに変換し、変換したModelをreturnする。
		WorkflowDiagramModel diagramModel;
		try {
			diagramModel = WorkflowDiagramModelConverter.deserialize(deleteDaoModel.getModel());
		} catch (MwaError e) {
			throw e;
		}
	
		
		return diagramModel;
	}
	
	public WorkflowDiagramModel getWorkflowDiagram(String workflowDiagramId) {
		if (workflowDiagramId == null) {
			final String errorMessage = "workflowDiagramId is required param.";
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT, null, "workflowDiagramId is required param.");
		}
		WorkflowDiagramDaoModel workflowDiagramDaoModel = workflowDiagramDao.getModel(workflowDiagramId, null);
		WorkflowDiagramModel workflowDiagramModel = null;
		try {
			workflowDiagramModel = WorkflowDiagramModelConverter.deserialize(workflowDiagramDaoModel.getModel());
		} catch (MwaError e) {
			throw e;
		}
		return workflowDiagramModel;
	}
	
	public WorkflowDiagramCollection getWorkflowDiagrams(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		if (sorts == null) {
			sorts = Arrays.asList("createTime+");
		}
		WorkflowDiagramDaoCollection workflowDiagramDaoCollection = null;
		try {
			workflowDiagramDaoCollection = workflowDiagramDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		WorkflowDiagramCollection workflowDiagramCollection = new WorkflowDiagramCollection();
		workflowDiagramCollection.setCount(workflowDiagramDaoCollection.getCount());
		workflowDiagramCollection.setTotalCount(workflowDiagramDaoCollection.getTotalCount());
		List<WorkflowDiagramModel> models = new ArrayList<>();
		for (WorkflowDiagramDaoModel workflowDiagramDaoModel: workflowDiagramDaoCollection.getModels()) {
			WorkflowDiagramModel workflowDiagramModel = null;
			try {
				workflowDiagramModel = WorkflowDiagramModelConverter.deserialize(workflowDiagramDaoModel.getModel());
			} catch (MwaError e) {
				throw e;
			}
			models.add(workflowDiagramModel);
		}
		workflowDiagramCollection.setModels(models);
		return workflowDiagramCollection;
	}
	
	public WorkflowDiagramDaoModel getWorkflowDiagramDaoModel(String workflowDiagramId) {
		WorkflowDiagramDaoModel workflowDiagramDaoModel = workflowDiagramDao.getModel(workflowDiagramId, null);
		return workflowDiagramDaoModel;
	}
	
	public WorkflowDiagramDaoCollection getWorkflowDiagramDaoModels(QueryCriteria query) {
		WorkflowDiagramDaoCollection workflowDiagramDaoCollectoin = workflowDiagramDao.getModels(query);
		return workflowDiagramDaoCollectoin;
	}
	
}
