package com.sony.pro.mwa.kbase.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.kbase.repository.database.query.ExtensionContentQuery;
import com.sony.pro.mwa.kbase.repository.database.query.ExtensionQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.ExtensionContentType;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.repository.ExtensionContentDao;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.model.kbase.ExtensionContentCollection;
import com.sony.pro.mwa.model.kbase.ExtensionContentModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ExtensionContentDaoImpl extends DatabaseBaseDao<ExtensionContentModel, ExtensionContentCollection, QueryCriteria> implements ExtensionContentDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

    public ExtensionContentCollection getModels(QueryCriteria criteria) {
        return super.collection(criteria);
    }

    public ExtensionContentModel getModel(String id, Principal principal) {
        return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
    }

    private static final String INSERT_SQL =
            "INSERT INTO mwa.extension_content(" +
            		"  extension_id" +
            		", extension_content_type" +
            		", extension_content_id" +
            		") " +
            "VALUES (" +
                    "?, ?, ?" +
                    ") ";
    public ExtensionContentModel addModel(final ExtensionContentModel model) {
        final String insertSql = INSERT_SQL;
        KeyHolder keyHolder = new GeneratedKeyHolder();

        int insertedRowCount = jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                        PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
                        int colNum = 1;

                        ps.setObject(colNum++, model.getExtensionId());
                        ps.setString(colNum++, model.getExtensionContentType());
                        ps.setString(colNum++, model.getExtensionContentId());

                        logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
                        return ps;
                    }
                },
                keyHolder);

        ExtensionContentModel addedModel =model;
        return insertedRowCount != 0 ? addedModel : null;
    }

    public ExtensionContentModel updateModel(final ExtensionContentModel acti) {
    	throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
    }

    public ExtensionContentModel deleteModel(ExtensionContentModel model) {
    	throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
    }
    private static final String DELETE_MAP_SQL =
            "DELETE FROM " +
                    "mwa.extension_content " +
            "WHERE " +
                    "extension_content_id = ? ";

    public void deleteModelByContentId(String extensionContentId) {
        String deleteSql = DELETE_MAP_SQL;

        Object[] paramArray = new Object[]{UUID.fromString(extensionContentId)};
        if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
        }
        return;
    }
    private static final String DELETE_MAP_SQL2 =
            "DELETE FROM " +
                    "mwa.extension_content " +
            "WHERE " +
                    "extension_id = ? ";

    public void deleteModelById(String extensionId) {
        String deleteSql = DELETE_MAP_SQL;

        Object[] paramArray = new Object[]{UUID.fromString(extensionId)};
        if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
        }
        return;
    }

    @Override
    protected IQuery<ExtensionContentModel, ExtensionContentCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
        return new ExtensionContentQuery(jdbcTemplate);
    }
}
