package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNEdgeModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNPlaneModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNShapeModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnEndEventModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;

public class PlaneModel extends InnerBpmnModelBuilderBase<BpmnBPMNPlaneModel> {
	
	
	private String bpmnElement;
	private BpmnBPMNPlaneModel bpmnPlaneModel;
	private List<BpmnBPMNEdgeModel> bpmnEdgeModels;
	private List<BpmnBPMNShapeModel> bpmnShapeModels;
	
	public PlaneModel(String index) {
		super(BpmnDefinition.PLANE_ID + index);
	}
	
	public void planeInitialize(WorkflowModel workflowModel, Map<String, GatewayModel> gatewayModelMap, Map<String, FlowModel> flowModelMap) {
		bpmnShapeModels = new ArrayList<>();
		// initialize shapeModels
		for(StepModel stepModel: workflowModel.getSteps()) {
			ShapeModel shapeModel = new ShapeModel(stepModel.getName());
			shapeModel.shapeInitialize(stepModel, flowModelMap);
			shapeModel.createBpmnBPMNShapeModel();
			bpmnShapeModels.add(shapeModel.getModel());
		}
		setBpmnShapeModels(bpmnShapeModels);
		
		// initialize edgeModels
		bpmnEdgeModels = new ArrayList<>();
		for (FlowModel flowModel: flowModelMap.values()) {
			EdgeModel edgeModel = new EdgeModel(flowModel.getId());
			edgeModel.edgeInitialize(workflowModel, gatewayModelMap, flowModel, bpmnShapeModels, bpmnEdgeModels);
			edgeModel.createBpmnBPMNEdgeModel();
			bpmnEdgeModels.add(edgeModel.getModel());
		}
		setBpmnEdgeModels(bpmnEdgeModels);
		setBpmnElement(BpmnDefinition.BPMN_ELEMENT_INIITALS + workflowModel.getId());
	}
	
	public void createBpmnPlaneModel() {
		bpmnPlaneModel = new BpmnBPMNPlaneModel();
		bpmnPlaneModel.setId(id);
		bpmnPlaneModel.setBpmnElement(bpmnElement);
		bpmnPlaneModel.setBPMNEdge(bpmnEdgeModels);
		bpmnPlaneModel.setBPMNShape(bpmnShapeModels);
	}
	
	public BpmnBPMNPlaneModel getModel() {
		return bpmnPlaneModel;
	}

	
	/* setter and getter */
	public List<BpmnBPMNEdgeModel> getBpmnEdgeModels() {
		return bpmnEdgeModels;
	}

	public void setBpmnEdgeModels(List<BpmnBPMNEdgeModel> bpmnEdgeModels) {
		this.bpmnEdgeModels = bpmnEdgeModels;
	}

	public List<BpmnBPMNShapeModel> getBpmnShapeModels() {
		return bpmnShapeModels;
	}

	public void setBpmnShapeModels(List<BpmnBPMNShapeModel> bpmnShapeModels) {
		this.bpmnShapeModels = bpmnShapeModels;
	}

	public String getBpmnElement() {
		return bpmnElement;
	}

	public void setBpmnElement(String bpmnElement) {
		this.bpmnElement = bpmnElement;
	}
	
}
