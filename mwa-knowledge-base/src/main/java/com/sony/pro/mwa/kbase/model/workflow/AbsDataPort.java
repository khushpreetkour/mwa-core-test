package com.sony.pro.mwa.kbase.model.workflow;

import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.parameter.ParameterDefinition;

public class AbsDataPort extends ParameterDefinition {
	//Stepの概念を入れる必要あり
	public String stepName;
	
	public AbsDataPort() {
	}

	public AbsDataPort(ParameterDefinition source) {
		super(source);
	}
	public AbsDataPort(AbsDataPort source) {
		super(source);
		this.stepName = source.getStepName();
	}

	@Override
	public String getKey() {
		return KBaseUtil.generateStepPortKey(this.stepName, super.getKey());
	}
	
	public String getStepName() {
		return this.stepName;
	}
	
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
}
