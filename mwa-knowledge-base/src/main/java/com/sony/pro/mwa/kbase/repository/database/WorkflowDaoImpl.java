package com.sony.pro.mwa.kbase.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.kbase.repository.WorkflowDao;
import com.sony.pro.mwa.kbase.repository.database.query.WorkflowQuery;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModelCollection;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class WorkflowDaoImpl extends DatabaseBaseDao<WorkflowDaoModel, WorkflowDaoModelCollection, QueryCriteria> implements WorkflowDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public WorkflowDaoModelCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public WorkflowDaoModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.workflow (" +
					"workflow_id" +
					", workflow_version" +
					", workflow_name" +
					", workflow_diagram_id" +
					", workflow_model" +
					", workflow_delete_flag" +
					", extension_id" +
					", workflow_alias" +
					", workflow_predefine_flag" +
					", workflow_create_time" +
					", workflow_update_time" +
					") " +
			"VALUES (" +
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
					") ";

	public WorkflowDaoModel addModel(final WorkflowDaoModel model) {

		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;

                        ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getVersion());
                        ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getDiagramId());
                        ps.setString(colNum++, model.getModel());
                        if(model.getDeleteFlag() == null){
                        	model.setDeleteFlag(Boolean.FALSE);
                        }
						ps.setBoolean(colNum++, model.getDeleteFlag());
						ps.setObject(colNum++, null);
						ps.setString(colNum++, model.getAlias());
						ps.setBoolean(colNum++, model.getPredefineFlag());
						if(model.getCreateTime() == null){
							model.setCreateTime(System.currentTimeMillis());
							model.setUpdateTime(model.getCreateTime());
						}
						ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));

						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);

		WorkflowDaoModel addedInstance = model;
		return insertedRowCount != 0 ? addedInstance : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.workflow " +
			"SET " +
					"workflow_name = ? " +
					", workflow_version = ? " +
					", workflow_model = ? " +
					", workflow_delete_flag = ? " +
					", extension_id = ? " +
					", workflow_alias = ? " +
					", workflow_predefine_flag = ? " +
					", workflow_update_time = ? " +
			"WHERE " +
					"workflow_id = ? AND workflow_version = ?";

	public WorkflowDaoModel updateModel(final WorkflowDaoModel model) {
		final String updateSql = UPDATE_SQL;
		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(updateSql);
						int colNum = 1;

						ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getVersion());
						ps.setString(colNum++, model.getModel());
						ps.setBoolean(colNum++, model.getDeleteFlag());

						UUID extensionId = null;
						if(StringUtils.isNotEmpty(model.getExtensionId())){
							extensionId = UUID.fromString(model.getExtensionId());
						}
						ps.setObject(colNum++, extensionId);
						ps.setString(colNum++, model.getAlias());
						ps.setBoolean(colNum++, model.getPredefineFlag());
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));




                        ps.setObject(colNum++, UUID.fromString(model.getId()));
                        ps.setString(colNum++, model.getVersion());
						return ps;
					}
				});

		return updatedRowCount != 0 ? model : null;
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.workflow " +
			"WHERE " +
			        "workflow_id = ? AND workflow_version = ?";

	public WorkflowDaoModel deleteModel(WorkflowDaoModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId()),model.getVersion()};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<WorkflowDaoModel, WorkflowDaoModelCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new WorkflowQuery(jdbcTemplate);
	}


}
