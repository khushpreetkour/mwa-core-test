package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BPMNEdge")
public class BpmnBPMNEdgeModel extends BpmnModelBase {

	private String bpmnElement;
	private String sourceElement;
	private String targetElement;
	private List<BpmnWaypointModel> waypoint;

	public void setId(String id) {
		super.setId(id);
	}

	@XmlAttribute
	public String getBpmnElement() {
		return bpmnElement;
	}

	public void setBpmnElement(String bpmnElement) {
		this.bpmnElement = bpmnElement;
	}

	@XmlAttribute
	public String getSourceElement() {
		return sourceElement;
	}

	public void setSourceElement(String sourceElement) {
		this.sourceElement = sourceElement;
	}

	@XmlAttribute
	public String getTargetElement() {
		return targetElement;
	}

	public void setTargetElement(String targetElement) {
		this.targetElement = targetElement;
	}

	@XmlElement(name = "waypoint", namespace = "http://www.omg.org/spec/DD/20100524/DI")
	public List<BpmnWaypointModel> getWaypoint() {
		return waypoint;
	}

	public void setWaypoint(List<BpmnWaypointModel> waypoint) {
		this.waypoint = waypoint;
	}

	
}
