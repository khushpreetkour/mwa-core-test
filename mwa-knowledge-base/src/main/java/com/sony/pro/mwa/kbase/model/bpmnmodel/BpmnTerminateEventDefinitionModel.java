package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id" })
@XmlRootElement(name = "terminateEventDefinition", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnTerminateEventDefinitionModel extends BpmnModelBase {

}
