package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sony.pro.mwa.kbase.utils.BpmnDefinition;

@XmlRootElement(name = "BPMNPlane")
public class BpmnBPMNPlaneModel extends BpmnModelBase {

	private String bpmnElement;
	private List<BpmnBPMNEdgeModel> BPMNEdge = new ArrayList<>();
	private List<BpmnBPMNShapeModel> BPMNShape = new ArrayList<>();

	@XmlAttribute
	public String getBpmnElement() {
		return bpmnElement;
	}

	public void setBpmnElement(String bpmnElement) {
		this.bpmnElement = bpmnElement;
	}

	@XmlElement(name = "BPMNEdge", namespace = "http://www.omg.org/spec/BPMN/20100524/DI")
	public List<BpmnBPMNEdgeModel> getBPMNEdge() {
		return BPMNEdge;
	}

	public void setBPMNEdge(List<BpmnBPMNEdgeModel> BPMNEdge) {
		this.BPMNEdge = BPMNEdge;
	}

	@XmlElement(name = "BPMNShape", namespace = "http://www.omg.org/spec/BPMN/20100524/DI")
	public List<BpmnBPMNShapeModel> getBPMNShape() {
		return BPMNShape;
	}

	public void setBPMNShape(List<BpmnBPMNShapeModel> BPMNShape) {
		this.BPMNShape = BPMNShape;
	}
}
