package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "itemSubjectRef", "name" })
@XmlRootElement(name = "dataOutput", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnDataOutputModel extends BpmnModelBase {

	private String itemSubjectRef;
	private String name;

	@XmlAttribute
	public String getItemSubjectRef() {
		return itemSubjectRef;
	}

	public void setItemSubjectRef(String itemSubjectRef) {
		this.itemSubjectRef = itemSubjectRef;
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
