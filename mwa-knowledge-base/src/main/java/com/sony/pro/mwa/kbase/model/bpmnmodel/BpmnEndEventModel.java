package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "name", "incoming", "terminateEventDefinition" })
@XmlRootElement(name = "endEvent", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnEndEventModel extends BpmnModelBase {

	private String name;
	private List<String> incoming = new ArrayList<>();
	private BpmnTerminateEventDefinitionModel terminateEventDefinition;

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<String> getIncoming() {
		return incoming;
	}

	public void setIncoming(List<String> incoming) {
		this.incoming = incoming;
	}

	@XmlElement(namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnTerminateEventDefinitionModel getTerminateEventDefinition() {
		return terminateEventDefinition;
	}

	public void setTerminateEventDefinition(BpmnTerminateEventDefinitionModel terminateEventDefinition) {
		this.terminateEventDefinition = terminateEventDefinition;
	}

}
