package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnStartEventModel;

public class StartEventModel extends InnerBpmnModelBuilderBase<BpmnStartEventModel>{
	private String name;
	private List<String> outgoing = new ArrayList<>();;
	private BpmnStartEventModel startEventModel;

	/**
	 * コンストラクタ
	 * @param id
	 */
	public StartEventModel(String id) {
		super(id);
		
	}

	public void createBpmnStartEventModel() {
		startEventModel = new BpmnStartEventModel();
		startEventModel.setId(id);
		startEventModel.setName(name);
		startEventModel.setOutgoing(outgoing);
	}
	
	@Override
	public BpmnStartEventModel getModel() {
		return startEventModel;
	}
	
	
	/* setter and getter */
	public List<String> getOutgoing(){
		return outgoing;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
