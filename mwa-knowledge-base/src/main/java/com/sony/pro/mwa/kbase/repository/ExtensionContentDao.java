package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.ExtensionContentCollection;
import com.sony.pro.mwa.model.kbase.ExtensionContentModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ExtensionContentDao extends ModelBaseDao<ExtensionContentModel, ExtensionContentCollection> {
}
