package com.sony.pro.mwa.kbase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.bpmnmanager.model.DiagramModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.EndEventModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.FlowModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.GatewayModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.ParallelGatewayModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.ProcessModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.PropertyModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.StartEventModel;
import com.sony.pro.mwa.kbase.bpmnmanager.model.TaskModel;
import com.sony.pro.mwa.kbase.internal.IBpmnManager;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnDefinitionsModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnItemDefinitionModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.generator.BpmnXmlGenerator;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.rc.MWARC;

public class BpmnManager implements IBpmnManager {
	private static BpmnManager singleton = new BpmnManager();
	
	private static final String BPMNS = "/bpmns/";
	private static final String CUSTOM_BPMNS = "/custom_bpmns/";
	private static final String S_S = "%s%s";
	public static final String MWA_HOME = System.getenv("MWA_HOME");
	private static final String JSON_WF_MODEL = "jsonWfModel";
	private final static MwaLogger logger = MwaLogger.getLogger(BpmnManager.class);
	
	public static BpmnManager getInstance() {
		return singleton;
	}
	
	/**
	 * bpmnを生成します。
	 * @param jsonWfModel
	 * @return
	 */
	public String generateBpmn(String jsonWfModel) {
		logger.debug(JSON_WF_MODEL + "=" + jsonWfModel);

		// 疎通確認のためコメントアウト
		WorkflowModel wfModel = KBaseUtil.deserializeWfModel(jsonWfModel);
		String xml = generateBpmn(wfModel);

		logger.debug("generateBpmn=" + xml);

		return xml;
	}
	
	public String generateBpmn(WorkflowModel workflowModel) {
		
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		
		//IBpmnModel bpmnModel = null;
		
		Map<String, PropertyModel> propertyModelMap = new HashMap<>();
		Map<String, TaskModel> taskModelMap = new HashMap<>();
		Map<String, GatewayModel> gatewayModelMap = new HashMap<>();
		Map<String, ParallelGatewayModel> parallelGatewayModelMap = new HashMap<>();
		Map<String, StartEventModel> startEventModelMap = new HashMap<>();
		Map<String, EndEventModel> endEventModelMap = new HashMap<>();
		Map<String,FlowModel> flowModelMap = new HashMap<>();	
		 
		/* Generate BpmnItemDefinitionModelList */
		List<BpmnItemDefinitionModel> itemDefinitoinModelList = new ArrayList();
		// TODO: wfModel ループで回す。
		BpmnItemDefinitionModel bpmnItemDefinitionModel = new BpmnItemDefinitionModel();
		// 暫定でItemDefinitionは決め打ち。
		bpmnItemDefinitionModel.setId("TypeString");
		bpmnItemDefinitionModel.setItemKind("Physical");
		bpmnItemDefinitionModel.setStructureRef("String");	
		itemDefinitoinModelList.add(bpmnItemDefinitionModel);
		// -----------------
		
		/* Generate  processModel */
		ProcessModel processModel = new ProcessModel();
		processModel.initialize(workflowModel, propertyModelMap, taskModelMap, gatewayModelMap, parallelGatewayModelMap, startEventModelMap, 
				endEventModelMap, flowModelMap);

		/* Generate diagramModel */
		/* idに付与される識別番号 1固定? */
		DiagramModel diagramModel = new DiagramModel("1");
		diagramModel.initialize(workflowModel, gatewayModelMap, flowModelMap);
		
		/* Convert Model to BPMNModel */
		processModel.createBpmnProcessModel(propertyModelMap, taskModelMap, gatewayModelMap, parallelGatewayModelMap, startEventModelMap, endEventModelMap, flowModelMap);
		diagramModel.createBpmnBPMNDiagramModel();
		
		/* Generate BpmnDefinitionModel to be the root element of xml */
		BpmnDefinitionsModel bpmnDefinitionModel = new BpmnDefinitionsModel(itemDefinitoinModelList, processModel.getModel(), diagramModel.getModel());
		
		BpmnXmlGenerator generator = new BpmnXmlGenerator();
		String xml = generator.generate(bpmnDefinitionModel);
		
		logger.debug("End:" + methodName);

		return xml;
	}
	
	public void deployBpmn(String xml, String fileName) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);

		// 環境変数の取得
		String path = String.format(S_S, MWA_HOME, CUSTOM_BPMNS); //結合 ⇒ String path=mwa_home + "\\custom_bpmns\\"

		// 出力ファイル
		File file = new File(String.format(S_S, path, fileName)); //結合 ⇒ File file = new File(path + "\\" + fileName);

		// 出力ストリームの作成
		try (FileOutputStream os = new FileOutputStream(file);
				OutputStreamWriter ou = new OutputStreamWriter(os, "UTF-8");
				PrintWriter pw = new PrintWriter(ou)) {

			// 書き込み
			pw.print(xml);

		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			throw new MwaError(MWARC.INTEGRATION_ERROR, null, "Failed in file output. (no write access permission)");
		} catch (IOException ex) {
			throw new MwaError(MWARC.INTEGRATION_ERROR, null, "Failed in file output.");
		}

	}
}
