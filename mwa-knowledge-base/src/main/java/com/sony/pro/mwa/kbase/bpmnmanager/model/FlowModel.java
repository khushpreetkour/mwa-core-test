package com.sony.pro.mwa.kbase.bpmnmanager.model;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnConditionExpressionModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnSequenceFlowModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;

public class FlowModel extends InnerBpmnModelBuilderBase<BpmnSequenceFlowModel>{
	private String priority;
	private String name;
	private String sourceRef;
	private String targetRef;
	private BpmnConditionExpressionModel bpmnConditionExpressionModel;
	private BpmnSequenceFlowModel bpmnSequenceFlowModel;
	/**
	 * コンストラクタ
	 * @param id
	 */
	public FlowModel(String index) {
		super(BpmnDefinition.PREFIX_OF_FLOW_MODEL_ID + index);
	}

	public void createBpmnSequenceFlowModel() {
		bpmnSequenceFlowModel = new BpmnSequenceFlowModel();
		bpmnSequenceFlowModel.setId(this.id);
		bpmnSequenceFlowModel.setPriority("1");
		bpmnSequenceFlowModel.setName(this.name);
		bpmnSequenceFlowModel.setSourceRef(this.sourceRef);
		bpmnSequenceFlowModel.setTargetRef(this.targetRef);
		bpmnSequenceFlowModel.setConditionExpression(bpmnConditionExpressionModel);
	}
	
	@Override
	public BpmnSequenceFlowModel getModel() {
//		//BpmnConditionExpressionModel bcem = new BpmnConditionExpressionModel();
//		BpmnSequenceFlowModel sfm = new BpmnSequenceFlowModel();
//		sfm.setId(this.id);
//		sfm.setPriority(this.priority);
//		sfm.setName(this.name);
//		sfm.setSourceRef(this.sourceRef);
//		sfm.setTargetRef(this.targetRef);
		return bpmnSequenceFlowModel;
	}
	
	public void createBpmnConditionExpressionModel(String conditionExpression, Integer index) {
		conditionExpression = "return " + conditionExpression + ";";
		bpmnConditionExpressionModel = new BpmnConditionExpressionModel(conditionExpression, index);
	}

	/*setter and getter*/
	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getPriority() {
	    return priority;
	}

	public void setPriority(String priority){
		this.priority = priority;
	}

	public String getSourceRef() {
		return sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	public String getTargetRef() {
		return targetRef;
	}

	public void setTargetRef(String targetRef) {
		this.targetRef = targetRef;
	}
	
}
