package com.sony.pro.mwa.kbase.model.workflow;

import java.util.List;

import com.sony.pro.mwa.parameter.ParameterDefinition;

public class Valve {
	protected String condition;
	
	protected List<ParameterDefinition> inputs;
	protected List<DataLink> targetLinks;
}
