package com.sony.pro.mwa.kbase.bundle.osgi;

import org.osgi.framework.launch.Framework;

public class FrameworkSingleton {
	private static final FrameworkSingleton instance = new FrameworkSingleton();
	private Framework m_fwk;
	private FrameworkRuntime thread;
	
	private FrameworkSingleton(){
	}
	
	/**
	 * Get Singleton Instance
	 * @return
	 */
	public static FrameworkSingleton getInstance(){
		return instance;
	}

	/**
	 * @return the m_fwk
	 */
	public Framework getM_fwk() {
		return m_fwk;
	}

	/**
	 * @param m_fwk the m_fwk to set
	 */
	public void setM_fwk(Framework m_fwk) {
		this.m_fwk = m_fwk;
	}

	/**
	 * @return the thread
	 */
	public FrameworkRuntime getThread() {
		return thread;
	}

	/**
	 * @param thread the thread to set
	 */
	public void setThread(FrameworkRuntime thread) {
		this.thread = thread;
	}

}
