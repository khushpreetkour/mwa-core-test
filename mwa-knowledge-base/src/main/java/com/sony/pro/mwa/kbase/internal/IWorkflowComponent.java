package com.sony.pro.mwa.kbase.internal;

import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModelCollection;

import java.util.List;

public interface IWorkflowComponent {
	/**
	 * 
	 * @param workflowModel 登録されるWorkflowModelの情報
	 * @return 登録されたWorkflowModel
	 */
	public WorkflowModel registerWorkflow(String workflowModel);
	
	// 下記IFはいらない想定だが、仕様変更が入る可能性を考慮して今は残しておく
	/**
	 * WorkflowModelの登録
	 *
	 * @param workflowModelJson 登録されるWorkflowModelの情報
	 * @param version WorkflowModelのバージョン
	 * @return 登録されたWorkflowModel
	 */
	public WorkflowModel registerWorkflow(String workflowModel, String workflowDiagramId, String version);

	// TODO できれが公開したくない。。。
	public WorkflowDaoModel getWorkflowDaoModel(String id);

	/**
	 * WorkflowModelの一覧取得
	 *
	 * @param sorts 並び替え
	 * @param filters 条件
	 * @param offset オフセット
	 * @param limit リミット
	 * @return 取得したWorkflowModelの一覧
	 */
	public WorkflowModelCollection getWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	/**
	 * WorkflowModelの取得
	 *
	 * @param workflowId 取得するworkflowModelのID
	 * @return IDに紐付いたWorkflowModel
	 */
	public WorkflowModel getWorkflow(String workflowId);

	/**
	 * WorkflowModelの更新
	 *
	 * @param workflowId 取得するworkflowModelのID
	 * @param WorkflowModel 更新されるWorkflowModel情報
	 * @return 更新されたWorkflowModel
	 */
	public WorkflowModel updateWorkflow(String workflowId, String workflowModelJson);

	/**
	 * WorkflowModelの更新
	 *
	 * @param workflowModelJson 更新されるWorkflowModelの情報
	 * @return 更新されたWorkflowModel
	 */

	// TODO できれが公開したくない。。。
	public WorkflowDaoModel updateWorkflowDaoModel(WorkflowDaoModel model);

	/**
	 *
	 * @param workflowDiagramId WorkflowModelを取得する際に指定するDiagramのid
	 * @return 取得したWorkflowModelの一覧
	 */
	public WorkflowModelCollection getWorkflowCollectionByDiagramId(String workflowDiagramId);
	
	public WorkflowModel deleteWorkflowDaoModel(String workflowDaoModelId);
}
