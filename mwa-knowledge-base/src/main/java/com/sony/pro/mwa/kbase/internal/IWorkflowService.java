package com.sony.pro.mwa.kbase.internal;

import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoModel;
import com.sony.pro.mwa.model.kbase.tempmodels.ConnectionPortModel;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramCollection;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModelCollection;
import com.sony.pro.mwa.service.activity.IActivityTemplate;

import java.util.List;

public interface IWorkflowService {

	/**
	 * WorkflowDiagramModelの登録
	 *
	 * @param workflowDiagramModelJson 登録されるWorkflowDiagramModel
	 * @return 登録されたWorkflowDiagramModel
	 */
	public WorkflowDiagramModel registerWorkflowDiagram(String workflowDiagramModelJson);

	public WorkflowDaoModel updateWorkflowDaoModel(WorkflowDaoModel model);

	/**
	 * WorkflowDiagramModelの更新
	 *
	 * @param workflowDiagramId 更新されるWorkflowDiagramModelのID
	 * @param workflowDiagramModelJson 更新されるWorkflowDiagramModel情報
	 * @return 更新されたWorkflowDiagramModel
	 */
	public WorkflowDiagramModel updateWorkflowDiagram(String workflowDiagramId, String workflowDiagramModelJson);

	/**
	 * WorkflowDiagramModelの一括削除
	 * @param sorts 並び替え
	 * @param filters 条件
	 * @param offset オフセット
	 * @param limit リミット
	 * @return 削除されたWorkflowDaigramModelの一覧
	 */
	public WorkflowDiagramCollection deleteWorkflowDiagram(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	public WorkflowDaoModel getWorkflowDaoModel(String workflowId);

	public WorkflowDiagramDaoModel getWorkflowDiagramDaoModel(String workflowDiagramId);

	/**
	 * IDに紐づくWorkflowDiagramModelの取得
	 *
	 * @param workflowDiagramId 取得するworkflowDiagramModelのID
	 * @return IDに紐付いたWorkflowDiagramModel
	 */
	public WorkflowDiagramModel getWorkflowDiagram(String workflowDiagramId);

	/**
	 * WorkflowDiagramModelの一覧取得
	 * @param sorts 並び替え
	 * @param filters 条件
	 * @param offset オフセット
	 * @param limit リミット
	 * @return 取得したWorkflowDiagramModelの一覧
	 */
	public WorkflowDiagramCollection getWorkflowDiagrams(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	/**
	 * WorkflowModelの一覧取得
	 *
	 * @param sorts 並び替え
	 * @param filters 条件
	 * @param offset オフセット
	 * @param limit リミット
	 * @return
	 */
	public WorkflowModelCollection getWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit);

	/**
	 * WorkflowModelの取得
	 *
	 * @param workflowId 取得するworkflowModelのID
	 * @return IDに紐付いたWorkflowModel
	 */
	public WorkflowModel getWorkflow(String workflowId);
	
	/**
	 * WorkflowModelの登録
	 * 
	 * @param workflowModelJson 登録されるWorkflowModel情報
	 * @param workflowDiagramId WorkflowModelのもとになるworkflowDiagramId
	 * @return 登録されたWorkflowModel
	 */
	public WorkflowModel registerWorkflow(String workflowModelJson);
	
	// 下記IFはいらない想定だが、仕様変更が入る可能性を考慮して今は残しておく
	/**
	 * WorkflowModelの登録
	 * 
	 * @param workflowModelJson 登録されるWorkflowModel情報
	 * @param workflowDiagramId WorkflowModelのもとになるworkflowDiagramId
	 * @return 登録されたWorkflowModel
	 */
	public WorkflowModel registerWorkflow(String workflowModelJson, String workflowDiagramId);

	/**
	 * WorkflowModelの更新
	 *
	 * @param workflowId 取得するworkflowModelのID
	 * @param WorkflowModel 更新されるWorkflowModel情報
	 * @return 更新されたWorkflowModel
	 */
	public WorkflowModel updateWorkflow(String workflowId, String workflowModelJson);

	/**
	 * WorkflowDiagramModelのDeploy
	 *
	 * @param workflowDiagramId DeployされるWorkflowDiagramModelのID
	 * @return DeployされたWorkflowDiagramModel
	 */
	public WorkflowDiagramModel deployWorkflowDiagram(String workflowDiagramId);
	
	/**
	 * WorkflowModelの初期状態を生成する
	 * @param startActivityTemplate StartTaskのactivityTemplate情報
	 * @param endActivityTemplate endTaskのactivityTemplate情報
	 * @param workflowDiagramModelJson WorkflowModelDiagramのJson
	 */
	public String createInitialWorkflow(IActivityTemplate startActivityTemplate, IActivityTemplate endActivityTemplate, String workflowDiagramModelJson);
	
	/**
	 * workflowModelの削除
	 * @param workflowModelJson 削除するWorkflowModel情報
	 * @return
	 */
	public WorkflowModelCollection deleteWorkflow(WorkflowModel workflowModel);

}
