package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.ParameterSetCollection;
import com.sony.pro.mwa.model.kbase.ParameterSetModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ParameterSetDao extends ModelBaseDao<ParameterSetModel, ParameterSetCollection> {
}
