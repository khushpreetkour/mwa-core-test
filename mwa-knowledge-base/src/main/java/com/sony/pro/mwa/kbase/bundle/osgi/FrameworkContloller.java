package com.sony.pro.mwa.kbase.bundle.osgi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.felix.framework.FrameworkFactory;
import org.apache.felix.main.AutoProcessor;
import org.apache.felix.main.Main;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.launch.Framework;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.SystemPropertyKey;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.MWARC;

public class FrameworkContloller {

	private final static MwaLogger logger = MwaLogger.getLogger(FrameworkContloller.class);

	public FrameworkContloller() {
		// TODO Auto-generated constructor stub
	}

	public static void create(){
	       // Print welcome banner.
        logger.info(
        		"\n"
        		+ "Create Felix Framework\n"
        		+ "============================================\n"
        		);

        org.osgi.framework.BundleContext test;
        Framework m_fwk = null;

        try
        {
        	String customExtra = "";
        	String customExtraEnv = System.getenv("MWA_EXTRA_PACKAGES");
        	if (customExtraEnv != null) {
        		customExtra = ", " + customExtraEnv;
        		logger.info("Extra packages: " + customExtra);
        	}
        	String SYSTEM_PACKAGES_EXTRA =
        		    "com.sony.pro.mwa"+
        		    ", com.sony.pro.mwa.common.utils" +
        		    ", com.sony.pro.mwa.common.log" +
        		    ", com.sony.pro.mwa.common.http" +
        		    ", com.sony.pro.mwa.bundle" +
        		    ", com.sony.pro.mwa.datatype" +
        		    ", com.sony.pro.mwa.enumeration"+
        		    ", com.sony.pro.mwa.enumeration.activity"+
        		    ", com.sony.pro.mwa.enumeration.provider"+
        		    ", com.sony.pro.mwa.enumeration.resource"+
        		    ", com.sony.pro.mwa.enumeration.event"+
        		    ", com.sony.pro.mwa.exception"+
        		    ", com.sony.pro.mwa.model"+
        		    ", com.sony.pro.mwa.model.activity"+
        		    ", com.sony.pro.mwa.model.kbase"+
        		    ", com.sony.pro.mwa.model.provider"+
        		    ", com.sony.pro.mwa.model.storage"+
        		    ", com.sony.pro.mwa.model.scheduler"+
        		    ", com.sony.pro.mwa.model.resource" +
        		    ", com.sony.pro.mwa.model.event" +
        		    ", com.sony.pro.mwa.model.workflow"+
        		    ", com.sony.pro.mwa.parameter" +
        		    ", com.sony.pro.mwa.parameter.type" +
        		    ", com.sony.pro.mwa.rc"+
        		    ", com.sony.pro.mwa.repository.query" +
        		    ", com.sony.pro.mwa.service" +
        		    ", com.sony.pro.mwa.service.activity" +
        		    ", com.sony.pro.mwa.service.kbase" +
        		    ", com.sony.pro.mwa.service.provider" +
        		    ", com.sony.pro.mwa.service.workflow" +
        		    ", com.sony.pro.mwa.service.storage" +
        		    ", com.sony.pro.mwa.service.resource" +
        		    ", com.sony.pro.mwa.service.event" +
        		    ", com.sony.pro.mwa.service.scheduler" +
        		    ", com.sony.pro.mwa.internal.definite" +
        		    ", com.sony.pro.mwa.internal.service" +
        		    ", com.sony.pro.mwa.internal.utils" +
        		    ", com.sony.pro.mwa.utils" +
        		    ", com.sony.pro.mwa.utils.rest" +
					", com.sony.pro.mwa.kbase.bundle.osgi" +
        		    ", com.sony.pro.mwa.activity.template" +
        		    ", com.sony.pro.mwa.activity.template.*" +
        		    ", com.sony.pro.mwa.activity.template.parameter" +
        		    ", com.sony.pro.mwa.activity.framework" +
        		    ", com.sony.pro.mwa.activity.framework.executor" +
        		    ", com.sony.pro.mwa.activity.framework.internal" +
        		    ", com.sony.pro.mwa.activity.framework.standard" +
        		    ", com.sony.pro.mwa.activity.framework.standard.sample" +
        		    ", com.sony.pro.mwa.activity.framework.stm" +
        		    ", com.sony.pro.mwa.activity.framework.*" +
					", com.sony.pro.mwa.workflow" +
					", com.sony.pro.mwa.workflow.*" +
				    ", org.apache.log4j;version=1.2.17" +
					", org.apache.log4j.Logger;version=1.2.17" +
				    ", org.osgi.framework" +
				    ", org.osgi.framework.launch" +
					", javax.xml.bind" +
					", javax.xml.bind.annotation" +
					", javax.xml.bind.annotation.adapters" +
					", javax.xml.bind.api" +
					", javax.xml.datatype" +
					", javax.xml.namespace" +
					", javax.xml.rpc" +
					", javax.xml.rpc.server" +
					", javax.xml.rpc.soap" +
					", javax.xml.soap" +
					", javax.xml.stream" +
					", javax.xml.stream.events" +
					", javax.xml.stream.util" +
					", javax.xml.transform.stax" +
					", javax.xml.ws" +
					", javax.xml.ws.api" +
					", javax.xml.ws.handler" +
					", javax.xml.ws.soap" +
					", javax.security.auth" +
					", javax.security.auth.callback" +
					", javax.security.auth.login" +
					", javax.security.auth.x500" +
					", javax.security.cert" +
					", javax.crypto" +
                    ", javax.management" +
                    ", javax.net" +
					", javax.net.ssl" +
					", javax.naming" +
					", javax.naming.spi" +
					", javax.sql" +
					", javax.jws.api" +
					", javax.sound" +
					", sun.net.www" +
					", org.xml" +
					", org.w3c" +
					", org.w3c.dom" +
					", org.w3c.dom.traversal" +
					", com.sun.xml.internal.ws.api.message" +
					", com.sun.xml.internal.bind" + customExtra;

        		Map<String, Object> config = new HashMap<String, Object>();
        		config.put(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, SYSTEM_PACKAGES_EXTRA);
        		String osgiRoot = System.getProperty(SystemPropertyKey.OSGI_CACHE_ROOT.toString());
        		if (osgiRoot != null)
        			config.put("felix.cache.rootdir", osgiRoot);


            m_fwk = getFrameworkFactory().newFramework(config);
            m_fwk.init();
            AutoProcessor.process(null, m_fwk.getBundleContext());

            FrameworkSingleton.getInstance().setM_fwk(m_fwk);
        }
        catch (Exception ex)
        {
            logger.error("Could not create framework", ex);
            System.exit(-1);
        }
	}

	public static void start(){
        FrameworkRuntime frameworkThread = new FrameworkRuntime();
        FrameworkSingleton.getInstance().setThread(frameworkThread);
        frameworkThread.start();
	}

	public static void shutdown(){
		Framework m_fwk = FrameworkSingleton.getInstance().getM_fwk();

		if (m_fwk == null)
			return;

		Bundle[] bundleList = m_fwk.getBundleContext().getBundles();
		for(Bundle bundle : bundleList){
			try {
				bundle.stop();
				bundle.uninstall();
			} catch (BundleException e) {
				// TODO Auto-generated catch block
				if (!"Cannot uninstall the system bundle.".endsWith(e.getMessage()))
		            logger.error("Cannot uninstall the system bundle", e);
			}
		}
        FrameworkRuntime frameworkThread = FrameworkSingleton.getInstance().getThread();
        if (frameworkThread != null)
        	frameworkThread.stopFramework();
	}

	public static Bundle addBundle(String jarPath){
		Framework m_fwk =  FrameworkSingleton.getInstance().getM_fwk();
		jarPath = "file:" + jarPath;
		Bundle bundle = null;
		try {
			bundle = m_fwk.getBundleContext().installBundle(jarPath);
			bundle.start();
			logger.info("Framework Add Bundle : " + jarPath);
		} catch (BundleException e) {
			// TODO Auto-generated catch block
            logger.error("Cannot addBundle", e);
		}
		return bundle;
	}


	public static void deleteBundle(long bundleId){
		Framework m_fwk = FrameworkSingleton.getInstance().getM_fwk();
		Bundle bundle = m_fwk.getBundleContext().getBundle(bundleId);
		try {
			bundle.stop();
			bundle.uninstall();
		} catch (BundleException e) {
			// TODO Auto-generated catch block
            logger.error("Cannot deleteBundle", e);
		}

	}
	public static void deleteBundleAll(){
		Framework m_fwk = FrameworkSingleton.getInstance().getM_fwk();

		if (m_fwk == null)
			return;

		Bundle[] bundleList = m_fwk.getBundleContext().getBundles();
		for(Bundle bundle : bundleList){
			try {
				bundle.stop();
                for(int i = 0; i  < 10; i++) {
                    if(FrameworkSingleton.getInstance().getM_fwk().getState() == 4) {
                        break;
                    }
                    logger.warn("Bandle has not stopped yet. Wait 1000 msec and check again.");
                    Thread.sleep(1000);
                    if(i == 9) {
                        logger.error("Failed to Bundle stop.");
                        throw new MwaError(MWARC.KBASE_ERROR);
                    }
                }
				bundle.uninstall();
			} catch (BundleException e) {
				// TODO Auto-generated catch block
				if (!"Cannot uninstall the system bundle.".endsWith(e.getMessage()))
		            logger.error("Cannot uninstall the system bundle", e);
			} catch (InterruptedException e) {
                logger.error(e.getMessage(), e);
            }
		}
	}

	public static Bundle getBundle(long bundleId){
		Framework m_fwk = FrameworkSingleton.getInstance().getM_fwk();
		Bundle bundle = m_fwk.getBundleContext().getBundle(bundleId);
		return bundle;
	}

	public static void getBundleList(){
//		Framework m_fwk =  FrameworkSingleton.getInstance().getM_fwk();
//		m_fwk.getBundleContext().
	}

	public static Class classLoader(String className, long bundleId) throws ClassNotFoundException{
		Framework m_fwk =  FrameworkSingleton.getInstance().getM_fwk();
		Bundle bundle = m_fwk.getBundleContext().getBundle(bundleId);
		return bundle.loadClass(className);
	}

	public static Class classLoader(String className) throws ClassNotFoundException{
		Framework m_fwk =  FrameworkSingleton.getInstance().getM_fwk();

		Bundle[] bundleList = m_fwk.getBundleContext().getBundles();

		for(Bundle bundle  : bundleList){
			try {
				return bundle.loadClass(className);
			} catch (ClassNotFoundException e) {

			}
		}
		throw new ClassNotFoundException("classLoader cannot find class :" + className);
	}

    private static FrameworkFactory getFrameworkFactory() throws Exception
    {
        java.net.URL url = Main.class.getClassLoader().getResource(
            "META-INF/services/org.osgi.framework.launch.FrameworkFactory");
        if (url != null)
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            try
            {
                for (String s = br.readLine(); s != null; s = br.readLine())
                {
                    s = s.trim();
                    // Try to load first non-empty, non-commented line.
                    if ((s.length() > 0) && (s.charAt(0) != '#'))
                    {
                        return (FrameworkFactory) Class.forName(s).newInstance();
                    }
                }
            }
            finally
            {
                if (br != null) br.close();
            }
        }

        throw new Exception("Could not find framework factory.");
    }
}
