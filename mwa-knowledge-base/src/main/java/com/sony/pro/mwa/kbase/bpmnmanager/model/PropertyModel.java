package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.Map;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnPropertyModel;

public class PropertyModel  extends InnerBpmnModelBuilderBase<BpmnPropertyModel>{
	private String subjectRef;
	private BpmnPropertyModel propertyModel;
	
	/**
	 * コンストラクタ
	 * @param id
	 */
	public PropertyModel(String id) {
		super(id);
	}
	
	public void createBpmnPropertyModel() {
		propertyModel = new BpmnPropertyModel();
		propertyModel.setId(id);
		propertyModel.setItemSubjectRef(this.subjectRef);
	}
	
	@Override
	public BpmnPropertyModel getModel() {	
		return propertyModel;
	}

	/**
	 * subjectRef(property種別)を取得します。
	 * @return
	 */
	public String getSubjectRef() {
		return subjectRef;
	}

	/**
	 * subjectRef(property種別)を設定します。
	 * @param subjectRef
	 */
	public void setSubjectRef(String subjectRef) {
		this.subjectRef = subjectRef;
	}
	
}
