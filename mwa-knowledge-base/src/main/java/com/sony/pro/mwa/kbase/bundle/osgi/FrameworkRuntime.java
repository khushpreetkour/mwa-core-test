package com.sony.pro.mwa.kbase.bundle.osgi;

import org.osgi.framework.BundleException;
import org.osgi.framework.launch.Framework;

import com.sony.pro.mwa.common.log.MwaLogger;

public class FrameworkRuntime {

	private final static MwaLogger logger = MwaLogger.getLogger(FrameworkRuntime.class);
  public void start(){
      Framework m_fwk = null;

      try
      {
    	  m_fwk =  FrameworkSingleton.getInstance().getM_fwk();
    	  logger.info("Framwork Starting: state=" + m_fwk.getState());
          m_fwk.start();
          logger.info("Framwork Started: state=" + m_fwk.getState());
      }
      catch (Exception ex)
      {
    	  logger.error("Could not create framework", ex);
          stopFramework();
      }
  }
  
  public void stopFramework(){
	  Framework  m_fwk =  FrameworkSingleton.getInstance().getM_fwk();
	  try {
			logger.info("Framwork Stopping1: state=" + m_fwk.getState());;
			m_fwk.stop();
			logger.info("Framwork Stopping2: state=" + m_fwk.getState());;
			m_fwk.waitForStop(0);
			logger.info("Framwork Stopped: state=" + m_fwk.getState());;
		} catch (BundleException e) {
			// TODO Auto-generated catch block
	    	  logger.error("Could not create framework", e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
	    	  logger.error("Could not create framework", e);
		} 
	
	m_fwk = null;
	FrameworkSingleton.getInstance().setM_fwk(null);
  }
}

