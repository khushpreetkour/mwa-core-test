package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "schemaLocation", "id", "expressionLanguage", "targetNamespace", "itemDefinition", "process", "diagram"})
@XmlRootElement(name = "definitions", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnDefinitionsModel extends BpmnModelBase {
	@XmlTransient
	private String schemaLocation;

	@XmlTransient
	private String targetNamespace;

	@XmlTransient
	private String expressionLanguage;

	private BpmnProcessModel process;
	
	private BpmnBPMNDiagramModel diagram;

	private List<BpmnItemDefinitionModel> itemDefinition = new ArrayList<>();

	public BpmnDefinitionsModel(List<BpmnItemDefinitionModel> itemDefinitonList, BpmnProcessModel processModel, BpmnBPMNDiagramModel diagramModel) {
		super.setId("Bpmn_Definition"); 
		this.itemDefinition = itemDefinitonList;
		this.process = processModel;
		this.diagram = diagramModel;
		this.schemaLocation = "http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd http://www.jboss.org/drools drools.xsd http://www.bpsim.org/schemas/1.0 bpsim.xsd";
		this.targetNamespace = "http://www.jboss.org/drools";
		this.expressionLanguage = "http://www.mvel.org/2.0";
	}
	
	
	public BpmnDefinitionsModel() {
		this.schemaLocation = "http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd http://www.jboss.org/drools drools.xsd http://www.bpsim.org/schemas/1.0 bpsim.xsd";
		this.targetNamespace = "http://www.jboss.org/drools";
		this.expressionLanguage = "http://www.mvel.org/2.0";
	}

	@XmlAttribute(name = "schemaLocation", namespace = "http://www.w3.org/2001/XMLSchema-instance")
	public String getSchemaLocation() {
		return schemaLocation;
	}

	@XmlElement(name = "process", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnProcessModel getProcess() {
		return process;
	}

	public void setProcess(BpmnProcessModel process) {
		this.process = process;
	}
	
	@XmlElement(name = "BPMNDiagram", namespace = "http://www.omg.org/spec/BPMN/20100524/DI")
	public BpmnBPMNDiagramModel getDiagram() {
		return diagram;
	}

	public void setDiagram(BpmnBPMNDiagramModel diagram) {
		this.diagram = diagram;
	}
	
	public void setSchemaLocation(String schemaLocation) {
		this.schemaLocation = schemaLocation;
	}

	@XmlAttribute(name = "targetNamespace")
	public String getTargetNamespace() {
		return targetNamespace;
	}

	public void setTargetNamespace(String targetNamespace) {
		this.targetNamespace = targetNamespace;
	}

	@XmlAttribute(name = "expressionLanguage")
	public String getExpressionLanguage() {
		return expressionLanguage;
	}

	public void setExpressionLanguage(String expressionLanguage) {
		this.expressionLanguage = expressionLanguage;
	}

	@XmlElement(name = "itemDefinition", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnItemDefinitionModel> getItemDefinition() {
		return itemDefinition;
	}

	public void setItemDefinition(List<BpmnItemDefinitionModel> itemDefinition) {
		this.itemDefinition = itemDefinition;
	}

}