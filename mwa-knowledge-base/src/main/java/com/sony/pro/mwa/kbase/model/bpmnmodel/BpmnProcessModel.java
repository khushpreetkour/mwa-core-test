package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "version", "packageName", "adHoc", "name", "isExecutable", "processType", "property", "startEvent", "sequenceFlow", "task", "exclusiveGateway", "parallelGateway", "endEvent" })
@XmlRootElement(name = "process", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnProcessModel extends BpmnModelBase {

	private String version;
	private String packageName;
	private String adHoc;
	private String name;
	private String isExecutable;
	private String processType;
	private List<BpmnSequenceFlowModel> sequenceFlow = new ArrayList<BpmnSequenceFlowModel>();
	private List<BpmnTaskModel> task = new ArrayList<BpmnTaskModel>();
	private List<BpmnPropertyModel> property = new ArrayList<BpmnPropertyModel>();
	private List<BpmnExclusiveGatewayModel> exclusiveGateway = new ArrayList<BpmnExclusiveGatewayModel>();
	private List<BpmnParallelGatewayModel> parallelGateway = new ArrayList<BpmnParallelGatewayModel>();
	private List<BpmnStartEventModel> startEvent = new ArrayList<BpmnStartEventModel>();
	private List<BpmnEndEventModel> endEvent = new ArrayList<BpmnEndEventModel>();

	@XmlAttribute(namespace = "http://www.jboss.org/drools")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlAttribute(namespace = "http://www.jboss.org/drools")
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	@XmlAttribute(namespace = "http://www.jboss.org/drools")
	public String getAdHoc() {
		return adHoc;
	}

	public void setAdHoc(String adHoc) {
		this.adHoc = adHoc;
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute
	public String getIsExecutable() {
		return isExecutable;
	}

	public void setIsExecutable(String isExecutable) {
		this.isExecutable = isExecutable;
	}

	@XmlAttribute
	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	@XmlElement(name = "sequenceFlow", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnSequenceFlowModel> getSequenceFlow() {
		return sequenceFlow;
	}

	public void setSequenceFlow(List<BpmnSequenceFlowModel> sequenceFlow) {
		this.sequenceFlow = sequenceFlow;
	}

	@XmlElement(name = "task", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnTaskModel> getTask() {
		return task;
	}

	public void setTask(List<BpmnTaskModel> task) {
		this.task = task;
	}

	@XmlElement(name = "property", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnPropertyModel> getProperty() {
		return property;
	}

	public void setProperty(List<BpmnPropertyModel> property) {
		this.property = property;
	}

	@XmlElement(name = "exclusiveGateway", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnExclusiveGatewayModel> getExclusiveGateway() {
		return exclusiveGateway;
	}

	public void setExclusiveGateway(List<BpmnExclusiveGatewayModel> exclusiveGateway) {
		this.exclusiveGateway = exclusiveGateway;
	}
	
	@XmlElement(name = "parallelGateway", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnParallelGatewayModel> getParallelGateway() {
		return parallelGateway;
	}
	
	public void setParallelGateway(List<BpmnParallelGatewayModel> parallelGateway) {
		this.parallelGateway = parallelGateway;
	}

	@XmlElement(name = "startEvent", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnStartEventModel> getStartEvent() {
		return startEvent;
	}

	public void setStartEvent(List<BpmnStartEventModel> startEvent) {
		this.startEvent = startEvent;
	}

	@XmlElement(name = "endEvent", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnEndEventModel> getEndEvent() {
		return endEvent;
	}

	public void setEndEvent(List<BpmnEndEventModel> endEvent) {
		this.endEvent = endEvent;
	}

}
