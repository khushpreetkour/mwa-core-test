package com.sony.pro.mwa.kbase.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.internal.service.IKnowledgeBaseInternal;
import com.sony.pro.mwa.kbase.KnowledgeBaseImpl;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.ParameterDefinition;
import com.sony.pro.mwa.service.activity.IActivityTemplate;

public class WorkflowModelUpdateProvider {
	private IKnowledgeBaseInternal knowledgeBase;
	private final static MwaLogger logger = MwaLogger.getLogger(WorkflowModelUpdateProvider.class);
	
	public WorkflowModelUpdateProvider(IKnowledgeBaseInternal knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}
	
	/**
	 * StepのActivityTemplate情報をDBから取得した値に更新する
	 * 
	 * @param step 更新されるStep
	 */
	public void updateActivityTemplate(WorkflowDiagramModel workflowDiagramModel) {
		List<StepModel> stepList = workflowDiagramModel.getSteps();
		if (stepList != null) {
			for (StepModel step: stepList) {				
				Map<String, Object> savedActivityTemplate = step.getActivity();
				String templateName = "";
				String templateVersion = "";
				if (savedActivityTemplate == null) {
					continue;
				}
					
				for (Map.Entry<String, Object> templateValue: savedActivityTemplate.entrySet()) {
					if ("name".equals(templateValue.getKey())) {
						templateName = (String) templateValue.getValue();
					}
					if ("version".equals(templateValue.getKey())) {
						templateVersion = (String) templateValue.getValue();
					}
				}
				ActivityTemplate activityCacheInKBase = getActivityTemplate(templateName, templateVersion);
				if (activityCacheInKBase != null) {
					Map<String, Object> act = createActivityTemplateMap(activityCacheInKBase);
					step.setActivity(act);
				}
				KBaseUtil.StepType stepType = KBaseUtil.StepType.getStepType(templateName);
				if (KBaseUtil.StepType.START == stepType) {
					// Start/Endはユーザーが値を変更する可能性があるため、DBの値を使用する
					Object inputList = savedActivityTemplate.get("inputs");
					
					if (inputList == null) {
						inputList = new ArrayList();
					}
					step.getActivity().put("inputs", inputList);
					// workflowDiagramModel.importInputs((ArrayList)inputList);
				}
				if (KBaseUtil.StepType.END == stepType) {
					Object inputList = savedActivityTemplate.get("inputs");
					
					if (inputList == null) {
						inputList = new ArrayList();
					}
					step.getActivity().put("inputs", inputList);
					
					Object outputList = savedActivityTemplate.get("outputs");
					
					if (outputList == null) {
						outputList = new ArrayList();
					}
					step.getActivity().put("outputs", outputList);
					// workflowDiagramModel.importOutputs((ArrayList)outputList);
				}
			}
		}
	}
	
	public void updateActivityTemplate(WorkflowModel workflowModel) {
		List<StepModel> stepList = workflowModel.getSteps();
		if (stepList != null) {
			for (StepModel step: stepList) {				
				Map<String, Object> savedActivityTemplate = step.getActivity();
				String templateName = "";
				String templateVersion = "";
				if (savedActivityTemplate == null) {
					continue;
				}
					
				for (Map.Entry<String, Object> templateValue: savedActivityTemplate.entrySet()) {
					if ("name".equals(templateValue.getKey())) {
						templateName = (String) templateValue.getValue();
					}
					if ("version".equals(templateValue.getKey())) {
						templateVersion = (String) templateValue.getValue();
					}
				}
				ActivityTemplate activityCacheInKBase = getActivityTemplate(templateName, templateVersion);
				if (activityCacheInKBase != null) {
					Map<String, Object> act = createActivityTemplateMap(activityCacheInKBase);
					step.setActivity(act);
				}
				KBaseUtil.StepType stepType = KBaseUtil.StepType.getStepType(templateName);
				if (!workflowModel.getPredefineFlag()) {
					if (KBaseUtil.StepType.START == stepType) {
						// Start/Endはユーザーが値を変更する可能性があるため、DBの値を使用する
						Object inputList = savedActivityTemplate.get("inputs");
						
						if (inputList == null) {
							inputList = new ArrayList();
						}
						step.getActivity().put("inputs", inputList);
						// workflowDiagramModel.importInputs((ArrayList)inputList);
					}
					if (KBaseUtil.StepType.END == stepType) {
						Object inputList = savedActivityTemplate.get("inputs");
						
						if (inputList == null) {
							inputList = new ArrayList();
						}
						step.getActivity().put("inputs", inputList);
						
						Object outputList = savedActivityTemplate.get("outputs");
						
						if (outputList == null) {
							outputList = new ArrayList();
						}
						step.getActivity().put("outputs", outputList);
						// workflowDiagramModel.importOutputs((ArrayList)outputList);
					}
				}
			}
		}
	}
	
	/**
	 * TemplateNameとTemplateVersionが一致するActivityTemplateを取得する。
	 * 
	 * @param templateName ActivityTemplateの名前
	 * @param templateVersion ActivityTemplateのバージョン
	 * @return 取得したActivityTemplate
	 */
	public ActivityTemplate getActivityTemplate(String templateName, String templateVersion) {
    	List<String> filters = new ArrayList<>();
		filters.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + templateName);
		filters.add("version" + FilterOperatorEnum.EQUAL.toSymbol() + templateVersion);
		List<IActivityTemplate> activityTemplateList = knowledgeBase.getActitvityTemplatesFull(null, filters, null, null);
		if (activityTemplateList.size() == 1) {
			return (ActivityTemplate) activityTemplateList.get(0);
		} else {
			return null;
		}
    }
	
	/**
	 * ActivityTemplateからMap<String, Object>型のインスタンスを生成する。
	 * 
	 * @param activityTemplate 元になるActivityTemplate
	 * @return 生成したMap
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> createActivityTemplateMap(ActivityTemplate activityTemplate) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> activityTemplateMap = mapper.convertValue(activityTemplate, Map.class);
		return activityTemplateMap;
	}

}
