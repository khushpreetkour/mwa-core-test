package com.sony.pro.mwa.kbase.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.model.provider.ActivityProviderTypeCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.ColumnUtils;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class ActivityProviderTypeQuery extends Query<ActivityProviderTypeModel, ActivityProviderTypeCollection, QueryCriteria> {
    public enum COLUMN {
        ID("i.activity_provider_type_id", "id", UUID.class, true),
        NAME("i.activity_provider_type_name", "name", String.class, true),
        MODEL_NUMBER("i.activity_provider_type_model_number", "modelNumber", String.class, true),
        EXTENSION_ID("k.extension_id", "extensionId", UUID.class, true),
        EXTENSION_NAME("l.extension_name", "extensionName", String.class, true),
        BUNDLE_ID("m.extension_property_value", "bundleId", String.class, true),
        CREATE_TIME("i.activity_provider_type_create_time", "createTime", Timestamp.class, true),
        UPDATE_TIME("i.activity_provider_type_update_time", "updateTime", Timestamp.class, true),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }
    
    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String WITH_RECURSIVE_CLAUSE = "";
/*    private static final String WITH_RECURSIVE_CLAUSE =
            "WITH RECURSIVE " +
                    "recursive_user_group(system_user_group_id, system_user_group_name, system_user_group_parent_system_user_group_id) AS ( " +
            "SELECT " +
                    "ug.system_user_group_id, ug.system_user_group_name, ug.system_user_group_parent_system_user_group_id " +
            "FROM " +
                    "dig.system_user_group AS ug, " +
                    "dig.system_user_system_user_group_system_role AS ugr " +
            "WHERE " +
                    "ug.system_user_group_id = ugr.system_user_group_id " +
            "AND " +
                    "ugr.system_user_id = ? " +
            "UNION " +
            "SELECT " +
                    "ug.system_user_group_id, ug.system_user_group_name, ug.system_user_group_parent_system_user_group_id " +
            "FROM " +
                    "dig.system_user_group AS ug, recursive_user_group AS pug " +
            "WHERE " +
                    "pug.system_user_group_parent_system_user_group_id = ug.system_user_group_id " +
                    ") ";*/

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.NAME.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " + 
            "mwa.activity_provider_type AS i "+
            "LEFT JOIN mwa.extension_content AS k ON i.activity_provider_type_id=k.extension_content_id " +
            "LEFT JOIN mwa.extension AS l ON k.extension_id=l.extension_id " +
            "LEFT JOIN mwa.extension_property AS m ON m.extension_id=l.extension_id and m.extension_property_name = 'BUNDLE_ID' " +
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    


    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        for (COLUMN enumColumn : COLUMN.values()) {
            map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }

    public ActivityProviderTypeQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }
    
    @Override
    protected RowMapper<ActivityProviderTypeModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<ActivityProviderTypeModel> {

        /*
         * (non-Javadoc)
         * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
         */
        public ActivityProviderTypeModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        	String id = rs.getString(COLUMN.ID.alias);
        	String name = rs.getString(COLUMN.NAME.alias);
        	String modelNumber = rs.getString(COLUMN.MODEL_NUMBER.alias);

        	String bundleIdStr = rs.getString(COLUMN.BUNDLE_ID.alias);
        	Long bundleId = null;
        	if (bundleIdStr != null) {
        		try {
        			bundleId = Long.valueOf(bundleIdStr);
        		} catch (NumberFormatException e) {
        		}
        	}
        	ActivityProviderTypeModel model = new ActivityProviderTypeModel();
        	model.setId(id);
        	model.setName(name);
        	model.setModelNumber(modelNumber);
        	model.setPluginId(rs.getString(COLUMN.EXTENSION_ID.alias));
        	model.setBundleId(bundleId);
        	model.setCreateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.CREATE_TIME.alias));
        	model.setUpdateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.UPDATE_TIME.alias));
        	return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }

}
