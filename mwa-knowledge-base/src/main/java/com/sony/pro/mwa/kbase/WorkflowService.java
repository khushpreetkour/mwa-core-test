package com.sony.pro.mwa.kbase;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.internal.IBpmnManager;
import com.sony.pro.mwa.kbase.internal.IWorkflowComponent;
import com.sony.pro.mwa.kbase.internal.IWorkflowDiagramComponent;
import com.sony.pro.mwa.kbase.internal.IWorkflowService;
import com.sony.pro.mwa.kbase.model.bpmnmodel.converter.WorkflowDiagramModelConverter;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.kbase.utils.WorkflowModelUpdateProvider;
import com.sony.pro.mwa.kbase.utils.KBaseUtil.StepType;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoCollection;
import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoModel;
import com.sony.pro.mwa.model.kbase.tempmodels.ConnectionPortModel;
import com.sony.pro.mwa.model.kbase.tempmodels.Parameter;
import com.sony.pro.mwa.model.kbase.tempmodels.PositionModel;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramCollection;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModelCollection;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.service.activity.IActivityTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class WorkflowService implements IWorkflowService {
	private static WorkflowService singleton = new WorkflowService();
	private final static MwaLogger logger = MwaLogger.getLogger(WorkflowService.class);

	private IWorkflowDiagramComponent workflowDiagramComponent;
	private IWorkflowComponent workflowComponent;
	private IBpmnManager bpmnManager;
	
	private static final String START_TASK = "com.sony.pro.mwa.control.activity.StartTask";
    private static final String END_TASK = "com.sony.pro.mwa.control.activity.EndTask";

	public static WorkflowService getInstance() {
		return singleton;
	}

	public void setWorkflowDiagramComponent(IWorkflowDiagramComponent workflowDiagramComponent) {
		this.workflowDiagramComponent = workflowDiagramComponent;
	}

	public IWorkflowDiagramComponent getWorkflowDiagramComponent() {
		return workflowDiagramComponent;
	}

	public void setWorkflowComponent(IWorkflowComponent workflowComponent) {
		this.workflowComponent = workflowComponent;
	}

	public IWorkflowComponent getWorkflowComponent() {
		return workflowComponent;
	}

	public void setBpmnManager(IBpmnManager bpmnManager) {
		this.bpmnManager = bpmnManager;
	}

	public IBpmnManager getBpmnManager() {
		return bpmnManager;
	}

	public WorkflowDiagramModel registerWorkflowDiagram(String workflowDiagramModelJson) {
		return workflowDiagramComponent.registerWorkflowDiagram(workflowDiagramModelJson);
	}

	public WorkflowDaoModel updateWorkflowDaoModel(WorkflowDaoModel model) {
		return workflowComponent.updateWorkflowDaoModel(model);
	}

	public WorkflowDiagramModel updateWorkflowDiagram(String workflowDiagramId, String workflowDiagramModelJson) {
		return workflowDiagramComponent.updateWorkflowDiagram(workflowDiagramId, workflowDiagramModelJson);
	}

	public WorkflowDiagramCollection deleteWorkflowDiagram(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		WorkflowDiagramDaoCollection daoCollection = null;
		try {
			daoCollection = workflowDiagramComponent.getWorkflowDiagramDaoModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}

		// List<WorkflowDiagramModel> succeedDeleteList = new ArrayList<WorkflowDiagramModel>();
		List<WorkflowDiagramModel> modelCollection = new ArrayList<WorkflowDiagramModel>();
		for (WorkflowDiagramDaoModel daoModel: daoCollection.getModels()) {
			try {
			//	succeedDeleteList.add(workflowDiagramComponent.deleteWorkflowDiagramDaoModel(daoModel.getWorkflowId()));
				modelCollection.add(workflowDiagramComponent.deleteWorkflowDiagramDaoModel(daoModel.getWorkflowId()));
				// DiagramModelに紐付くWorkflowModelを使用不可の状態にする
				WorkflowModelCollection workflowCollection = workflowComponent.getWorkflowCollectionByDiagramId(daoModel.getWorkflowId());
				for (WorkflowModel workflowModel: workflowCollection.getModels()) {
					updateDeleteFrag(workflowModel);
				}
			} catch (Throwable e) {
				logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			}
		}
		WorkflowDiagramCollection resultCollection = new WorkflowDiagramCollection();
		resultCollection.setModels(modelCollection);
		resultCollection.setCount((long)modelCollection.size());
		resultCollection.setTotalCount(daoCollection.getTotalCount());
		return resultCollection;
	}
	
	public WorkflowModelCollection deleteWorkflow(WorkflowModel workflowModel) {
		WorkflowModelCollection workflowModelCollection = null;
		String workflowId = workflowModel.getId();
		List<String> filters = Arrays.asList("id==" + workflowId);
		try {
			workflowModelCollection = workflowComponent.getWorkflows(null, filters, null, null);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		List<WorkflowModel> workflowModels = new ArrayList<WorkflowModel>();
		WorkflowModel deletedWorkflowModel = null;
		for (WorkflowModel model : workflowModelCollection.getModels()) {
			if (model.getId().equals(workflowModel.getId())) {
				deletedWorkflowModel = workflowComponent.deleteWorkflowDaoModel(model.getId());
				workflowModels.add(deletedWorkflowModel);
				logger.info("Succeeded in deleting workflowModel.");
			}
		}
		WorkflowModelCollection resultCollection = new WorkflowModelCollection();
		resultCollection.setModels(workflowModels);
		resultCollection.setCount((long) workflowModels.size());
		resultCollection.setTotalCount(workflowModelCollection.getTotalCount());
		return resultCollection;
	}

	public WorkflowDiagramModel getWorkflowDiagram(String workflowDiagramId) {
		return workflowDiagramComponent.getWorkflowDiagram(workflowDiagramId);
	}

	public WorkflowDiagramCollection getWorkflowDiagrams(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		return workflowDiagramComponent.getWorkflowDiagrams(sorts, filters, offset, limit);
	}

	public WorkflowDaoModel getWorkflowDaoModel(String workflowId) {
		return workflowComponent.getWorkflowDaoModel(workflowId);
	}

	public WorkflowDiagramDaoModel getWorkflowDiagramDaoModel(String workflowDiagramId) {
		return workflowDiagramComponent.getWorkflowDiagramDaoModel(workflowDiagramId);
	}

	public WorkflowModelCollection getWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		return workflowComponent.getWorkflows(sorts, filters, offset, limit);
	}

	public WorkflowModel getWorkflow(String workflowId) {
		return workflowComponent.getWorkflow(workflowId);
	}
	
	public WorkflowModel registerWorkflow(String workflowModelJson) {
		return workflowComponent.registerWorkflow(workflowModelJson);
	}
	
	/**
	 * 下記IFはいらない想定だが、仕様変更が入る可能性を考慮して今は残しておく
	 */
	public WorkflowModel registerWorkflow(String workflowModelJson, String workflowDiagramId) {
		WorkflowModelCollection workflowCollection = workflowComponent.getWorkflowCollectionByDiagramId(workflowDiagramId);
		String version = String.valueOf(workflowCollection.getModels().size() + 1);
		return workflowComponent.registerWorkflow(workflowModelJson, workflowDiagramId, version);
	}

	public WorkflowModel updateWorkflow(String workflowId, String workflowModelJson) {
		return workflowComponent.updateWorkflow(workflowId, workflowModelJson);
	}

	public WorkflowDiagramModel deployWorkflowDiagram(String workflowDiagramId) {
		// 作成するWorkflowModelのVersionを生成する
		WorkflowModelCollection workflowCollection = workflowComponent.getWorkflowCollectionByDiagramId(workflowDiagramId);
		String version = String.valueOf(workflowCollection.getModels().size() + 1);

		WorkflowDiagramDaoModel workflowDiagramDaoModel = workflowDiagramComponent.getWorkflowDiagramDaoModel(workflowDiagramId);
		WorkflowModel workflowModel = workflowComponent.registerWorkflow(workflowDiagramDaoModel.getModel(), workflowDiagramId, version);
		// 前回まで使用していたWorkflowModelを使えないようにする
		if (workflowCollection.getModels().size() != 0) {
			WorkflowModel previousWorkflowModel = workflowCollection.getModels().get(workflowCollection.getModels().size() - 1);
			updateDeleteFrag(previousWorkflowModel);
		}
		// WorkflowDiagramModelのVersionを更新する
		WorkflowDiagramModel workflowDiagramModel = WorkflowDiagramModelConverter.deserialize(workflowDiagramDaoModel.getModel());
		this.updateWorkflowDiagramModelWithVersion(workflowDiagramModel, version);
		//workflowComponent.deployWorkflow(workflowModel);
		return workflowDiagramModel;
	}
	
	public String createInitialWorkflow(IActivityTemplate startActivityTemplate, IActivityTemplate endActivityTemplate, String workflowDiagramModelJson) {
		List<StepModel> stepModelList = new ArrayList<>();
		List<ConnectionPortModel> connectionPortList = new ArrayList<>();
		
		//　Step Model及びConnectionPort Modelを生成しListに追加する
		createStepAndConnectionPort(startActivityTemplate, stepModelList, connectionPortList, 100, 100, 0);
		createStepAndConnectionPort(endActivityTemplate, stepModelList, connectionPortList, 585, 100, 0);
		
		// workflowDiagramModelJsonのdeserialize
		WorkflowDiagramModel diagramModel = WorkflowDiagramModelConverter.deserialize(workflowDiagramModelJson);
		// 作成したStepModelsをDiagramModelsに格納
		diagramModel.setSteps(stepModelList);
		diagramModel.setConnectionPorts(connectionPortList);
		String newWfModel = WorkflowDiagramModelConverter.serializeWfModel(diagramModel);
		return newWfModel;
	}

	private void updateDeleteFrag(WorkflowModel model) {
		WorkflowDaoModel daoModel = workflowComponent.getWorkflowDaoModel(model.getId());
		daoModel.setDeleteFlag(true);
		model.setDeleteFlag(true);
		String updateJsonModel = WorkflowDiagramModelConverter.serializeWfModel(model);
		daoModel.setModel(updateJsonModel);
		workflowComponent.updateWorkflowDaoModel(daoModel);
	}

	private void updateWorkflowDiagramModelWithVersion(WorkflowDiagramModel model, String version) {
		WorkflowDiagramDaoModel daoModel = getWorkflowDiagramDaoModel(model.getId());
		daoModel.setVersion(version);
		model.setVersion(version);
		String updateModelJson = WorkflowDiagramModelConverter.serializeWfModel(model);
		daoModel.setModel(updateModelJson);
		workflowDiagramComponent.updateWorkflowDiagramModel(model);
	}
	
	/**
	 * Step Model及びConnectionPort Modelを生成し、定義したListに追加する
	 * @param activityTemplate activityTemplate情報
	 * @param steoModelList Step Modelを管理するList
	 * @param connectionPortList ConnectionPortModelを管理するList
	 * @param positionX x座標の位置情報
	 * @param positionY y座標の位置情報
	 * @param positionZ z座標の位置情報
	 */
	private void createStepAndConnectionPort(IActivityTemplate activityTemplate, List<StepModel> stepModelList, List<ConnectionPortModel> connectionPortList, int positionX, int positionY, int positionZ) {
		
		// Connection PortのInput Port, Output Port間のPosition差分を調整する数値
		int positionDifferenceNumber = 32;
		
		// DATA_INPUTポート及びDATA_OUTPUTポートに追加するパラメーターの生成
		List<Parameter> inputParameterList = createInputParameter(activityTemplate);
		List<Parameter> outputParameterList = createOutputParameterList(activityTemplate);
		
		String templateName = activityTemplate.getName();
		// StepModelの生成
		// nameをaliasから取得
		StepModel stepModel = createStepModel(activityTemplate.getAlias(), activityTemplate, positionX, positionY, positionZ);
		stepModelList.add(stepModel);
		
		// ConnectionPortの生成
		if (KBaseUtil.StepType.START == KBaseUtil.StepType.getStepType(templateName)) {
		// if (templateName.equals(START_TASK)) {
			// ConnectionPortのPositionはStepの位置情報(x座標)に32を足す暫定対応
			connectionPortList.add(createConnectionPort(stepModel.getId(), BpmnDefinition.CONTROL_OUTPUT, null, positionX + positionDifferenceNumber, positionY, positionZ));
		} else if (KBaseUtil.StepType.END == KBaseUtil.StepType.getStepType(templateName)) {
			// ConnectionPortのPositionはStepの位置情報(x座標)から32を引く暫定対応
			connectionPortList.add(createConnectionPort(stepModel.getId(), BpmnDefinition.CONTROL_INPUT, null, positionX - positionDifferenceNumber, positionY, positionZ));
		} else {
			// else処理内では何も行わない
		}
		// DATA_INPUTポートの生成
		for (Parameter inputParameter : inputParameterList) {
			connectionPortList.add(createConnectionPort(stepModel.getId(), BpmnDefinition.DATA_INPUT, inputParameter, positionX + positionDifferenceNumber, positionY, positionZ));
		}
		// DATA_OUTPUTポートの生成
		for (Parameter outputParameter : outputParameterList) {
			connectionPortList.add(createConnectionPort(stepModel.getId(), BpmnDefinition.DATA_OUTPUT, outputParameter, positionX + positionDifferenceNumber, positionY, positionZ));
		}
	}
	
	/**
	 * DATA_INPUTに設定するパラメータ情報を生成する
	 * @param activityTemplate ActivityTemplate情報
	 * @return 生成したパラメータ情報一覧
	 */
	private List<Parameter> createInputParameter(IActivityTemplate activityTemplate) {
		List<Parameter> inputParameterList = new ArrayList<>();
		for (IParameterDefinition parameterDefinition: activityTemplate.getInputs()) {
			Parameter inputParameter = new Parameter();
			inputParameter.setKey(parameterDefinition.getKey());
			inputParameter.setTypeName(parameterDefinition.getTypeName());
			inputParameter.setRequired(parameterDefinition.getRequired());
			inputParameter.setValue(null);
			inputParameterList.add(inputParameter);
		}
		return inputParameterList;
	}
	
	/**
	 * DATA_OUTPUTに設定するパラメータ情報を生成する
	 * @param activityTemplate ActivityTemplate情報
	 * @return 生成したパラメータ情報一覧
	 */
	private List<Parameter> createOutputParameterList(IActivityTemplate activityTemplate) {
		List<Parameter> outputParameterList = new ArrayList<>();
		for (IParameterDefinition parameterDefinition: activityTemplate.getOutputs()) {
			Parameter outputParameter = new Parameter();
			outputParameter.setKey(parameterDefinition.getKey());
			outputParameter.setTypeName(parameterDefinition.getTypeName());
			outputParameter.setRequired(parameterDefinition.getRequired());
			outputParameter.setValue(null);
			outputParameterList.add(outputParameter);
		}
		return outputParameterList;
	}
	
	/**
	 * StepModelを生成する
	 *
	 * @param stepName stepのname
	 * @param activityTemplate ActivityTemplate情報
	 * @param positionX x座標の位置情報
	 * @param positionY y座標の位置情報
	 * @param positionZ z座標の位置情報
	 * @return 生成したStepModel
	 */
	private StepModel createStepModel(String stepName, IActivityTemplate activityTemplate, int positionX, int positionY, int positionZ) {
		StepModel stepModel = new StepModel();
		stepModel.setName(stepName);
		stepModel.setId(UUID.randomUUID().toString());
		stepModel.setActivity(new ObjectMapper().convertValue(activityTemplate, Map.class));
		stepModel.setPosition(createPositionModel(positionX, positionY, positionZ));
		return stepModel;
	}
	
	/**
	 * PositionModelを生成する
	 *
	 * @param positionX x座標の位置情報
	 * @param positionY y座標の位置情報
	 * @param positionZ z座標の位置情報
	 * @return 生成したPositionModel
	 */
	private PositionModel createPositionModel(int positionX, int positionY, int positionZ) {
		PositionModel positionModel = new PositionModel();
		positionModel.setX(positionX);
		positionModel.setY(positionY);
		positionModel.setZ(positionZ);
		return positionModel;
	}
	
	/**
	 * Start or End Taskに設定するConnectionPortModelの生成
	 *
	 * @param stepId 該当StepのId
	 * @param type ConnectionPortのType
	 * @param positionX x座標の位置情報
	 * @param positionY y座標の位置情報
	 * @param positionZ z座標の位置情報
	 * @return 生成したConnectionPortModel
	 */
	private ConnectionPortModel createConnectionPort(String stepId, String type, Parameter parameter, int positionX, int positionY, int positionZ) {
		ConnectionPortModel connectionPort = new ConnectionPortModel();
		String newConnectionPortId = UUID.randomUUID().toString();
		connectionPort.setId(newConnectionPortId);
		connectionPort.setStepId(stepId);
		connectionPort.setType(type);
		connectionPort.setParameter(parameter);
		connectionPort.setPosition(createPositionModel(positionX, positionY, positionZ));
		return connectionPort;
	}
}
