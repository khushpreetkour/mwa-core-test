package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"id","name","dataInputRefs"})
@XmlRootElement(name = "inputSet" , namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnInputSetModel extends BpmnModelBase {

	private String name;
	private List<String> dataInputRefs = new ArrayList<String>();

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "dataInputRefs" , namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<String> getDataInputRefs() {
		return dataInputRefs;
	}

	public void setDataInputRefs(List<String> dataInputRefs) {
		this.dataInputRefs = dataInputRefs;
	}

}
