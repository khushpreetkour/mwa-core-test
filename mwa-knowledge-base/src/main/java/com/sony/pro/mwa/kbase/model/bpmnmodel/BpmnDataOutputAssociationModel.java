package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "sourceRef", "targetRef", "assignment" })
@XmlRootElement(name = "dataOutputAssociation", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnDataOutputAssociationModel extends BpmnModelBase {

	private String sourceRef;
	private String targetRef;
	private BpmnAssignmentModel assignment;

	@XmlElement(name = "sourceRef", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public String getSourceRef() {
		return sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	@XmlElement(name = "targetRef", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public String getTargetRef() {
		return targetRef;
	}

	public void setTargetRef(String targetRef) {
		this.targetRef = targetRef;
	}

	@XmlElement(name = "assignment", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnAssignmentModel getAssignment() {
		return assignment;
	}

	public void setAssignment(BpmnAssignmentModel assignment) {
		this.assignment = assignment;
	}
}
