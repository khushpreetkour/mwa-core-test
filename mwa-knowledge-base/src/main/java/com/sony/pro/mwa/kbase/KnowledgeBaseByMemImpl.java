package com.sony.pro.mwa.kbase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.model.kbase.ExtensionCollection;
import com.sony.pro.mwa.parameter.OperationResult;

public class KnowledgeBaseByMemImpl extends KnowledgeBaseImpl {

    Map<String, ActivityTemplate> map;

    public KnowledgeBaseByMemImpl() {
        super();
        map = new HashMap<>();
    }

    @Override
    public void initialize() {
        //do nothing
    }

    @Override
    public void storeJarFile(String bundleFilePath) {
        throw new RuntimeException("KnowledgeBaseByMemImpl: Unsupported Operation");
    }

    @Override
    public OperationResult loadPlugin(String pluginName, String pluginVersion) {
        throw new RuntimeException("KnowledgeBaseByMemImpl: Unsupported Operation");
    }

    @Override
    public OperationResult deletePlugin(String pluginName, String pluginVersion) {
        throw new RuntimeException("KnowledgeBaseByMemImpl: Unsupported Operation");
    }

    @Override
    public ExtensionCollection getPlugins(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        throw new RuntimeException("KnowledgeBaseByMemImpl: Unsupported Operation");
    }

    @Override
    public ExtensionCollection getBpmns(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        throw new RuntimeException("KnowledgeBaseByMemImpl: Unsupported Operation");
    }

    @Override
    public ExtensionCollection getRules(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        throw new RuntimeException("KnowledgeBaseByMemImpl: Unsupported Operation");
    }

    public void addTemplate(ActivityTemplate template) {
        map.put(template.getId(), template);
        this.activityTemplateDao.addModel(template);
    }

    protected ActivityTemplate createTemplateFromModel(ActivityTemplateModel model) {
        return map.get(model.getId());
    }
}
