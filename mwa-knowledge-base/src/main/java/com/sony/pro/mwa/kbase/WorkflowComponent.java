package com.sony.pro.mwa.kbase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.internal.IWorkflowComponent;
import com.sony.pro.mwa.kbase.repository.WorkflowDao;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModelCollection;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModelCollection;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

public class WorkflowComponent implements IWorkflowComponent {
	private static WorkflowComponent singleton = new WorkflowComponent();
	private final static MwaLogger logger = MwaLogger.getLogger(WorkflowComponent.class);

	private WorkflowDao workflowDao;

	public static WorkflowComponent getInstance() {
		return singleton;
	}

	@Autowired
	public void setWorkflowDao(WorkflowDao workflowDao) {
		this.workflowDao = workflowDao;
	}

    public WorkflowDao getWorkflowDao() {
		return this.workflowDao;
	}

    public WorkflowModel registerWorkflow(String workflowModelJson) {
    	WorkflowModel workflowModel = null;
		try {
			workflowModel = KBaseUtil.deserializeWfModel(workflowModelJson);
		} catch (MwaError e) {
			throw e;
		}

		// WorkflowDaoModelに値をセット
		WorkflowDaoModel workflowDaoModel = new WorkflowDaoModel();
		workflowDaoModel.setId(UUID.randomUUID().toString());
		workflowDaoModel.setDeleteFlag(false);
		workflowDaoModel.setVersion("1");
		Long createTime = System.currentTimeMillis();
		workflowDaoModel.setAlias(workflowModel.getAlias());
		workflowDaoModel.setPredefineFlag(false);
		workflowDaoModel.setName(workflowModel.getName());
		workflowDaoModel.setCreateTime(createTime);
		workflowDaoModel.setUpdateTime(createTime);

		final String modelStr = ToStringBuilder.reflectionToString(workflowDaoModel, ToStringStyle.DEFAULT_STYLE);

		// WorkflowIdの重複確認処理
		List<String> filters = new ArrayList<>();
		filters.add(String.format("%s%s%s", "workflowId",FilterOperatorEnum.EQUAL.toSymbol(),workflowDaoModel.getId()));
		WorkflowModelCollection checkCol = null;
		try {
			checkCol = this.getWorkflows(null, filters, null, null);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(checkCol.getCount() > 0){
			// 既にIDが登録されている。
			final String errMsg = "workflowId is duplicate value.  workflowModel="+modelStr;
			logger.error(errMsg);
			throw new MwaError(MWARC.INVALID_INPUT,null,errMsg);
		}

		// WorkflowModelに登録情報を反映
		workflowModel.setDeleteFlag(false);
		workflowModel.setId(workflowDaoModel.getId());
		workflowModel.setPredefineFlag(false);
		workflowModel.setCreateTime(createTime);
		workflowModel.setUpdateTime(createTime);

		// modelの生成
		String updateJsonModel = KBaseUtil.serializeWfModel(workflowModel);

		workflowDaoModel.setModel(updateJsonModel);

		// 登録処理
		WorkflowDaoModel model = null;
		try {
			model = this.workflowDao.addModel(workflowDaoModel);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			final String errorMessage = "register is workflow error. workflowModel=" + modelStr;
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT, null, errorMessage);
		}
		return workflowModel;
    }

	public WorkflowModel registerWorkflow(String workflowModelJson, String workflowDiagramId, String version) {

		WorkflowModel workflowModel = null;
		try {
			workflowModel = KBaseUtil.deserializeWfModel(workflowModelJson);
		} catch (MwaError e) {
			throw e;
		}

		// WorkflowDaoModelに値をセット
		WorkflowDaoModel workflowDaoModel = new WorkflowDaoModel();
		workflowDaoModel.setId(UUID.randomUUID().toString());
		workflowDaoModel.setDeleteFlag(false);
		workflowDaoModel.setVersion(version);
		workflowDaoModel.setDiagramId(workflowDiagramId);
		Long createTime = System.currentTimeMillis();
		workflowDaoModel.setAlias(workflowModel.getAlias());
		workflowDaoModel.setPredefineFlag(false);
		workflowDaoModel.setName(workflowModel.getName());
		workflowDaoModel.setCreateTime(createTime);
		workflowDaoModel.setUpdateTime(createTime);

		final String modelStr = ToStringBuilder.reflectionToString(workflowDaoModel, ToStringStyle.DEFAULT_STYLE);

		// WorkflowIdの重複確認処理
		List<String> filters = new ArrayList<>();
		filters.add(String.format("%s%s%s", "workflowId",FilterOperatorEnum.EQUAL.toSymbol(),workflowDaoModel.getId()));
		WorkflowModelCollection checkCol = null;
		try {
			checkCol = this.getWorkflows(null, filters, null, null);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(checkCol.getCount() > 0){
			// 既にIDが登録されている。
			final String errMsg = "workflowId is duplicate value.  workflowModel="+modelStr;
			logger.error(errMsg);
			throw new MwaError(MWARC.INVALID_INPUT,null,errMsg);
		}

		// WorkflowModelに登録情報を反映
		workflowModel.setDeleteFlag(false);
		workflowModel.setId(workflowDaoModel.getId());
		workflowModel.setDiagramId(workflowDiagramId);
		workflowModel.setVersion(version);
		workflowModel.setPredefineFlag(false);
		workflowModel.setCreateTime(createTime);
		workflowModel.setUpdateTime(createTime);

		// modelの生成
		String updateJsonModel = KBaseUtil.serializeWfModel(workflowModel);

		workflowDaoModel.setModel(updateJsonModel);

		// 登録処理
		WorkflowDaoModel model = null;
		try {
			model = this.workflowDao.addModel(workflowDaoModel);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if(model == null){
			final String errorMessage = "register is workflow error. workflowModel=" + modelStr;
			logger.error(errorMessage);
			throw new MwaError(MWARC.INVALID_INPUT, null, errorMessage);
		}
		return workflowModel;
	}

	public WorkflowModel getWorkflow(String workflowId) {
		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
		logger.debug("Start:" + methodName);
		if(workflowId == null){
			final String errMsg = "workflowId is required param";
			logger.error(errMsg);
			throw new MwaError(MWARC.INVALID_INPUT,null,"workflowId is required param");
		}

		// 取得処理
		WorkflowDaoModel workflowDaoModel = workflowDao.getModel(workflowId, null);
		final String model = workflowDaoModel.getModel();
		WorkflowModel workflowModel = null;
		try {
			 workflowModel = KBaseUtil.deserializeWfModel(model);
		} catch (MwaError e) {
				throw e;
		}
		return workflowModel;
	}

	public WorkflowModelCollection getWorkflows(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		if (sorts == null) {
			sorts = Arrays.asList("createTime+");
		}
		WorkflowDaoModelCollection workflowDaoModelCollection = null;
		try {
			workflowDaoModelCollection = workflowDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		WorkflowModelCollection workflowModelCollection = new WorkflowModelCollection();
		workflowModelCollection.setCount(workflowDaoModelCollection.getCount());
		workflowModelCollection.setTotalCount(workflowDaoModelCollection.getTotalCount());
		List<WorkflowModel> models = new ArrayList<>();
		for (WorkflowDaoModel workflowDaoModel: workflowDaoModelCollection.getModels()) {
			WorkflowModel workflowModel = null;
			try {
				workflowModel = KBaseUtil.deserializeWfModel(workflowDaoModel.getModel());
			}
			catch (MwaError e) {
				throw e;
			}
			models.add(workflowModel);
		}
		workflowModelCollection.setModels(models);
		return workflowModelCollection;
	}

	public WorkflowModel updateWorkflow(String workflowId, String workflowModelJson) {
		// Workflow取得
		WorkflowDaoModel workflowDaoModel = this.getWorkflowDaoModel(workflowId);

		// 更新対象と更新のRequest body をデシリアライズ
		WorkflowModel updateWorkflowModel = null;
		WorkflowModel nowWorkflowModel = null;
		try {
			updateWorkflowModel = KBaseUtil.deserializeWfModel(workflowModelJson);
			nowWorkflowModel =  KBaseUtil.deserializeWfModel(workflowDaoModel.getModel());
		} catch (MwaError e) {
			throw e;
		}

		// 更新Modelの作成
		updateWorkflowModel.setId(nowWorkflowModel.getId());
		updateWorkflowModel.setVersion(nowWorkflowModel.getVersion());
		if(nowWorkflowModel.getTemplateId() != null){
			updateWorkflowModel.setName(nowWorkflowModel.getName());
			updateWorkflowModel.setTemplateId(nowWorkflowModel.getTemplateId());
		}else{
			updateWorkflowModel.setTemplateId(null);
		}
		Long updateTime = System.currentTimeMillis();
		updateWorkflowModel.setUpdateTime(updateTime);

		// 更新用JsonModelの生成
		String updateJsonModel = KBaseUtil.serializeWfModel(updateWorkflowModel);

		// Workflow更新内容のセット
		workflowDaoModel.setName(updateWorkflowModel.getName());
		workflowDaoModel.setAlias(updateWorkflowModel.getAlias());
		workflowDaoModel.setPredefineFlag(updateWorkflowModel.getPredefineFlag());
		workflowDaoModel.setModel(updateJsonModel);
		workflowDaoModel.setUpdateTime(updateTime);

		final String modelStr = ToStringBuilder.reflectionToString(workflowDaoModel, ToStringStyle.DEFAULT_STYLE);

		// Workflow更新処理
		WorkflowDaoModel model = null;
		try {
			model = this.workflowDao.updateModel(workflowDaoModel);
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if( model == null){
			final String errMsg = "update is workflowVersion error. workflowVersionDaoModel="+modelStr;
			logger.error(errMsg);
			throw new MwaError(MWARC.INVALID_INPUT,null,errMsg);
		}

		WorkflowModel workflowModel = null;
		try {
			 workflowModel = KBaseUtil.deserializeWfModel(model.getModel());
		} catch (MwaError e) {
				throw e;
		}
		return workflowModel;
	}

	public WorkflowDaoModel updateWorkflowDaoModel(WorkflowDaoModel model) {
		return workflowDao.updateModel(model);
	}

	public WorkflowModelCollection getWorkflowCollectionByDiagramId(String workflowDiagramId) {
		List<String> filters = new ArrayList<String>();
		String filter = "diagramId==" + workflowDiagramId;
		filters.add(filter);
		return getWorkflows(null, filters, 0, 50);
	}


	public WorkflowDaoModel getWorkflowDaoModel(String workflowId){
		return workflowDao.getModel(workflowId, null);
	}

	private String incrementVersion(String version) {
		int incrementedVersion = Integer.parseInt(version) + 1;
		return String.valueOf(incrementedVersion);
	}

	public WorkflowModel deleteWorkflowDaoModel(String workflowModelId) {
		WorkflowDaoModel deleteWorkflowDaoModel = workflowDao.deleteModel(getWorkflowDaoModel(workflowModelId));
		return KBaseUtil.deserializeWfModel(deleteWorkflowDaoModel.getModel());
	}
}
