package com.sony.pro.mwa.kbase.model.bpmnmodel.converter;

/**
 * @param <T>
 *            変換元のModelクラス
 */
public interface IModelConvertStrategy<T> {
	public void mapToBuilderModels(T model);// TODO objectクラス指定
}
