package com.sony.pro.mwa.kbase.utils;

import com.sony.pro.mwa.common.log.MwaLogger;

public class BpmnDefinition {
	private static final MwaLogger logger = MwaLogger.getLogger(BpmnDefinition.class);

	/* Process Model */

	// Process Model
	public static final boolean CONST_ADHOC = false;
	public static final boolean CONST_IS_EXECUTABLE = true;
	public static final String CONST_PROCESS_TYPE_VALUE = "Private";
	public static final String PREFIX_OF_WORKFLOW_ID = "_";
	public static final String PREFIX_OF_OUTPUT_PARAMETER = "OUTPUT.";
	public static final String PREFIX_OF_INPUT_PARAMETER = "INPUT.";
	public static final String PACKAGE_NAME = "MWA";
	public static final String CONNECTION_PART_OF_GLOBAL_ID = "_";
	public static final String PREFIX_OF_BPMN_ID = "$";

	// TaskModel
	public static final String SUFFIX_OF_IOSPECIFICATION = "_iospec";
	public static final String SUFFIX_OF_INPUTSET = "_inputset";
	public static final String SUFFIX_OF_OUTPUTSET = "_outputset";
	/* 同STEP内のINPUT/OUTPUTで、同じ変数名があった場合に被ってしまうため
	 * IN/OUTでPREFIXを分けることとした */
	public static final String INFIX_OF_INNER_INPUT_REF = "_in_ref_";
	public static final String INFIX_OF_INNER_OUTPUT_REF = "_out_ref_";
	public static final String INFIX_OF_INPUT_ASSOCIATE = "_inasc_";
	public static final String INFIX_OF_OUTPUT_ASSOCIATE = "_outasc_";
	public static final String TASK_ICON_NAME = "task.png";

	// Gateway Model
	// There are multiple outputs from the GateWay step.
	public static final String DIRECTON_DIVERGING = "Diverging";
	// Enter from multiple steps to the gateway step.
	public static final String DIRECTON_CONVERGING ="Converging";
	public static final String ESCAPE_TARGET = "\\\"";

	// ConditionExpressionModel
	public static final String CONDITION_EXPRESSION_TYPE = "bpmn2:tFormalExpression";
	public static final String CONDITION_EXPRESSION_LANGUAGE_JAVA = "http://www.java.com/java";
	public static final String CONDITION_EXPRESSION_LANGUAGE_MVEL = "http://www.mvel.org/2.0";
	public static final String CONDITION = "condition";
	public static final String CONDITION_CONNECTION_PORT_ID = "conditionConnectionPortId";
	public static final String AND_CONDITION = "&&";
	public static final String OR_CONDITION = "||";
	public static final String CONVERTED_AND_CONDITION = "&amp;&amp;";

	// assignment
	public static final String ASSIGNMENT_ID_PREFIX = "Assignment_";
	public static final String ASSIGNMENT_FORMAL_EXPRESSION_ID_PREFIX = "AssignmentFormalExpression_";

  	// flowModel
	public static final String PREFIX_OF_FLOW_MODEL_ID = "flow_";

	/* ------------- */

	/* Diagram Model */
	public static final String DIAGRAM_ID = "BPMN_Diagram_";
	public static final String PLANE_ID = "BPMNPlane_Process_";
	public static final String SHAPE_ID = "BPMNShape_";
	public static final String EDGE_ID = "BPMNEdge_";
	public static final String CONTROL_INPUT = "CONTROL_INPUT";
	public static final String CONTROL_OUTPUT = "CONTROL_OUTPUT";
	public static final String DATA_INPUT = "DATA_INPUT";
	public static final String DATA_OUTPUT = "DATA_OUTPUT";
	public static final Integer EDGE_START_POINT = 0;
	public static final Integer EDGE_MIDDLE_POINT = 1;
	public static final Integer EDGE_END_POINT = 2;
	public static final String BPMN_ELEMENT_INIITALS = "_";
	/*---------------*/
	
}
