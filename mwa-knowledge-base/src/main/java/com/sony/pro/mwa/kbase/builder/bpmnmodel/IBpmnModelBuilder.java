package com.sony.pro.mwa.kbase.builder.bpmnmodel;

import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnModelBase;

public interface IBpmnModelBuilder<T extends BpmnModelBase> {
	/**
	 * bpmnモデルを生成します。
	 * @return
	 */
	public T getModel();
}
