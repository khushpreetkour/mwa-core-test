package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "priority", "name", "sourceRef", "targetRef", "conditionExpression" })
@XmlRootElement(name = "sequenceFlow", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnSequenceFlowModel extends BpmnModelBase {
	private String priority;
	private String name;
	private String sourceRef;
	private String targetRef;
	private BpmnConditionExpressionModel conditionExpression;

	@XmlAttribute(namespace = "http://www.jboss.org/drools")
	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute
	public String getSourceRef() {
		return sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	@XmlAttribute
	public String getTargetRef() {
		return targetRef;
	}

	public void setTargetRef(String targetRef) {
		this.targetRef = targetRef;
	}

	@XmlElement(name = "conditionExpression", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnConditionExpressionModel getConditionExpression() {
		return conditionExpression;
	}

	public void setConditionExpression(BpmnConditionExpressionModel conditionExpression) {
		this.conditionExpression = conditionExpression;
	}

}
