package com.sony.pro.mwa.kbase.model.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.template.ActivityTemplate;

public class Workflow {	//BPMNファイルに相当
	//protected Map<String, IControlFlowItem> steps;	//nameで引けるようにMapとする
	protected List<IControlFlowItem> steps;	//暫定（Class図作成用。。。）
	protected List<ControlLink> ctrlLink;
	protected List<DataLink> dataLink;

	protected ActivityTemplate template;	//WorkflowもTemplateを持つ
	protected List<DataPortOutput> inputs = new ArrayList<>();	//Workflowへの入力、後段のStepからすると出力されてきたデータと同様。なのでDataPortはOutput
	protected List<DataPortInput> outputs = new ArrayList<>();
	
	protected Map<String, Object> configParams;
}
