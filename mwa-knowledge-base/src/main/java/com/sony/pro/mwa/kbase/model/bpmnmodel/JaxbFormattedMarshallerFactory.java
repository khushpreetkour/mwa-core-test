package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class JaxbFormattedMarshallerFactory<T> {

	public static <T> String createFormattedMarshaller(T t) throws JAXBException {
		StringWriter writer = new StringWriter();

		JAXBContext context = JAXBContext.newInstance(t.getClass());
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new Bpmn2NamespacePrefixMapper());
		m.marshal(t, writer);
		return writer.toString();
	}

}
