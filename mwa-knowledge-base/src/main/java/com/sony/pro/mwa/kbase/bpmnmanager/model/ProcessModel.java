package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.kbase.tempmodels.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.kbase.builder.bpmnmodel.IBpmnModelBuilder;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnProcessModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.converter.datamodels.BuilderPropertyModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.model.kbase.tempmodels.constants.PortType;
import com.sony.pro.mwa.rc.MWARC;

public class ProcessModel implements IBpmnModelBuilder<BpmnProcessModel> {
    protected MwaLogger logger = MwaLogger.getLogger(ProcessModel.class);
    private String id;
    private String version;
    private String packageName;
    private Boolean adHoc;
    private String name;
    private Boolean isExecutable;
    private String processType;
    private BpmnProcessModel bpmnProcessModel;
    private String startStepId;
    private String endStepId;
    private List<String> conditionalStepIds = new ArrayList<String>();
    private List<String> convergingStepIds = new ArrayList<String>();

    public ProcessModel() {
        this.adHoc = BpmnDefinition.CONST_ADHOC;
        this.isExecutable = BpmnDefinition.CONST_IS_EXECUTABLE;
        this.processType = BpmnDefinition.CONST_PROCESS_TYPE_VALUE;
        this.packageName = BpmnDefinition.PACKAGE_NAME;
    }

    /**
     * BpmnProcessModel生成に必要なModel群を準備します
     *
     * @param workflowModel
     * @param propertyModelMap
     * @param taskModelMap
     * @param gatewayModelMap
     * @param startEventModelMap
     * @param endEventModelMap
     * @param flowModelMap
     */
    public void initialize(
            WorkflowModel workflowModel,
            Map<String, PropertyModel> propertyModelMap,
            Map<String, TaskModel> taskModelMap,
            Map<String, GatewayModel> gatewayModelMap,
            Map<String, ParallelGatewayModel> parallelGatewayModelMap,
            Map<String, StartEventModel> startEventModelMap,
            Map<String, EndEventModel> endEventModelMap,
            Map<String, FlowModel> flowModelMap) {
        /*
         * このinitialize内でworkflowModelを書き換えて処理するが、上位処理で元のworkflowModelを使用したいため、
         * このメソッド内で処理する用のworkflowModelを生成する。
         */
        WorkflowModel exeBpmnWorkflowModel = KBaseUtil.deserializeWfModel(KBaseUtil.serializeWfModel(workflowModel));

        this.setId(BpmnDefinition.PREFIX_OF_WORKFLOW_ID + exeBpmnWorkflowModel.getId());
        this.setName(exeBpmnWorkflowModel.getName());
        this.setVersion(exeBpmnWorkflowModel.getVersion());

        // 制御系ステップ(Start/End/Gateway)はそれぞれに紐づくActivityを追加する必要がある(GUIで進捗表示を行うため)
        createTask(exeBpmnWorkflowModel, taskModelMap);

        // Workflowのグローバル変数として"-"が使えないため"_"に変換する
        convertStepId(exeBpmnWorkflowModel);

//		for (ConnectionPortModel connectionPort: exeBpmnWorkflowModel.getConnectionPorts()) {
//			if (connectionPort.getType().equals("DATA_INPUT") && connectionPort.getParameter().getKey().equals("condition")) {
////				connectionPort.getParameter().setValue(connectionPort.getParameter().getValue().replace("-", "_"));
//			}
//		}

        // 各Stepの対応するタイプに合わせてModelを作成する
        for (StepModel step : exeBpmnWorkflowModel.getSteps()) {
            if ((null == step) || (null == step.getName())) {
                // exeBpmnWorkflowModel に StepModel が存在しない場合、エラーとする。
                throw new MwaInstanceError(MWARC.INVALID_INPUT, null,"There is no Step Model in Workflow Model.");
            }
            createStepParameter(step, taskModelMap, gatewayModelMap, parallelGatewayModelMap, startEventModelMap, endEventModelMap);
        }

        // StartStepのOutputパラメータを生成したStartTaskに移す
        String startTaskId = "";
        for (Map.Entry<String, TaskModel> taskEntry: taskModelMap.entrySet()) {
            // 本当はDisplayNameでStepTypeを取得したくないが...
            if (KBaseUtil.StepType.START == KBaseUtil.StepType.getStepType(taskEntry.getValue().getDisplayName())) {
                startTaskId = taskEntry.getKey();
                break;
            }
        }
        // StartEventStepのOutputパラメータを後段のStepが使うことがあるため、OUTPUT_DATAのStepIdをStartTaskStepに変換する
        for (ConnectionPortModel connectionPort: exeBpmnWorkflowModel.getConnectionPorts()) {
            if (PortType.DATA_OUTPUT.name().equals(connectionPort.getType())) {
                KBaseUtil.StepType stepType = getStepTypeFromStepId(exeBpmnWorkflowModel, connectionPort.getStepId());
                boolean isWorkflowInput = false;
                for (WFParameterDefinition wfParameterDefinition: exeBpmnWorkflowModel.getInputs()) {
                    if (connectionPort.getParameter().getKey().equals(wfParameterDefinition.getKey())) {
                        isWorkflowInput = true;
                        break;
                    }
                }
                if (!isWorkflowInput && KBaseUtil.StepType.START == stepType) {
                    connectionPort.setStepId(startTaskId);
                }
                // Workflow上のグローバル変数に該当する部分を作成する
                createOutputProperty(connectionPort.getParameter(), connectionPort.getStepId(), stepType, propertyModelMap, isWorkflowInput);
            }
        }

        /* ConnectionPortModel から dataOutputAssociation の基となる情報を作成する */
        for (ConnectionPortModel connectionPort : exeBpmnWorkflowModel.getConnectionPorts()) {
            // DATA_OUTPUT を基に dataOutputAssociation 情報を作成する。
            String connectionPortStepId = connectionPort.getStepId();
            // connectionPort の stepId が start イベント、end イベントConditional, Convergingブランチの ID であれば TaskModel が作られないため処理対象外とする。
            if (connectionPortStepId.equals(this.endStepId) || connectionPortStepId.equals(this.startStepId)
                    || this.conditionalStepIds.contains(connectionPortStepId) || this.convergingStepIds.contains(connectionPortStepId)) {
                continue;
            }
            if (PortType.DATA_OUTPUT.name().equals(connectionPort.getType())) {
                createDataOutputAssociation(connectionPort, taskModelMap, exeBpmnWorkflowModel);
            }

            // 即値対応
            // 即値の時は、type:DATA_INPUT に値が入る
            // 即値の時は、connections には入らないため後続処理で上書きされることはない。
            if (PortType.DATA_INPUT.name().equals(connectionPort.getType())) {
                // Condition Branch(Diverging)の場合は処理対象外とするため、判定処理を行う。
                KBaseUtil.StepType stepType = getStepTypeFromStepId(exeBpmnWorkflowModel, connectionPortStepId);
                if (KBaseUtil.StepType.CONDITION_BRANCH == stepType) {
                    continue;
                }
                createImmediateDataInputAssociation(connectionPort, taskModelMap, exeBpmnWorkflowModel);
            }
        }

        /* Connection,ConnectionPortModel から FlowMapを生成する */
        for (ConnectionModel connection : exeBpmnWorkflowModel.getConnections()) {
            Integer index = (Integer) flowModelMap.size();
            // 1.作成時に付与されるidに識別番号を付けます
            FlowModel flowModel = new FlowModel(index.toString());
            flowModelMap.put(flowModel.getId(), flowModel);

            // ★★★For Debug
            // Portに紐づくStepのnameやidをDebugで追うためのDebug Code
//            String fromConnectionPortId = connection.getFromConnectionPortId();
//            String toConnectionPortId = connection.getToConnectionPortId();
//            String fromStepId = "";
//            String toStepId = "";
//            String portType = "";
//            String fromPortKey = "";
//            String toPortKey = "";
//            for (ConnectionPortModel connectionPortModel: exeBpmnWorkflowModel.getConnectionPorts()) {
//                if (fromConnectionPortId.equals(connectionPortModel.getId())) {
//                    fromStepId = connectionPortModel.getStepId();
//                    portType = connectionPortModel.getType();
//                    if (connectionPortModel.getParameter() != null) {
//                        fromPortKey = connectionPortModel.getParameter().getKey();
//                    }
//                }
//                if (toConnectionPortId.equals(connectionPortModel.getId())) {
//                    toStepId = connectionPortModel.getStepId();
//                    if (connectionPortModel.getParameter() != null) {
//                        toPortKey = connectionPortModel.getParameter().getKey();
//                    }
//                }
//            }
//            String fromStepName = "";
//            String toStepName = "";
//            for (StepModel step: exeBpmnWorkflowModel.getSteps()) {
//                if (fromStepId.equals(step.getId())) {
//                    for (Map.Entry<String, Object> activityParameter: step.getActivity().entrySet()) {
//                        if ("name".equals(activityParameter.getKey())) {
//                            fromStepName = (String)activityParameter.getValue();
//                        }
//                    }
//                }
//                if (toStepId.equals(step.getId())) {
//                    for (Map.Entry<String, Object> activityParameter: step.getActivity().entrySet()) {
//                        if ("name".equals(activityParameter.getKey())) {
//                            toStepName = (String)activityParameter.getValue();
//                        }
//                    }
//                }
//            }

            // 2. connectionPortModel をループさせ、 Step処理で登録した情報と照らし合わせてFlowModelを作成します
            for (ConnectionPortModel connectionPort : exeBpmnWorkflowModel.getConnectionPorts()) {
                // Mapは、StepのIDで管理しているので、StepIdで判定する。
                if (startEventModelMap.containsKey(connectionPort.getStepId())) {
                    setOutgoingForStartEvent(connection, connectionPort, flowModel.getId(), startEventModelMap, flowModelMap);
                } else if (endEventModelMap.containsKey(connectionPort.getStepId())) {
                    setIncomingForEndEvent(connection, connectionPort, flowModel.getId(), endEventModelMap, flowModelMap);
                } else if (gatewayModelMap.containsKey(connectionPort.getStepId())) {
                    setIncomingOutgoingForGateway(connection, connectionPort, flowModel.getId(), gatewayModelMap, flowModelMap);
                } else if (parallelGatewayModelMap.containsKey(connectionPort.getStepId())) {
                    setIncomingOutgoingForParallelGateway(connection, connectionPort, flowModel.getId(), parallelGatewayModelMap, flowModelMap);
                } else if (taskModelMap.containsKey(connectionPort.getStepId())) {
                    setIoSpecificationAndAssociationForTask(connection, connectionPort, flowModel.getId(), taskModelMap, propertyModelMap, flowModelMap, exeBpmnWorkflowModel.getConnectionPorts());
                } else {
                    logger.warn("Connection can not be created due to incorrect step type.");
                }
            }
        }

        /* GatewayのDefaultSequence情報とCondition情報を登録する */
        Integer index = 0;
        for (Map.Entry<String, GatewayModel> gatewayModelMapEntry : gatewayModelMap.entrySet()) {
            index++;
            GatewayModel gatewayModel = gatewayModelMapEntry.getValue();
            List<ConnectionPortModel> relatedDataInputPortList = new ArrayList<>();
            List<ConnectionPortModel> relatedControlOutputPortList = new ArrayList<>();
            String conditionControlOutputPortId = null;
            if (BpmnDefinition.DIRECTON_DIVERGING.equals(gatewayModel.getGatewayDirection())) {
                for (ConnectionPortModel connectionPortModel : exeBpmnWorkflowModel.getConnectionPorts()) {
                    if (gatewayModel.getId().equals(connectionPortModel.getStepId())) {
                        // Condition 登録用
                        if (BpmnDefinition.DATA_INPUT.equals(connectionPortModel.getType())) {
                            relatedDataInputPortList.add(connectionPortModel);
                        }
                        // default 登録用
                        if (BpmnDefinition.DATA_INPUT.equals(connectionPortModel.getType()) &&
                                BpmnDefinition.CONDITION_CONNECTION_PORT_ID.equals(connectionPortModel.getParameter().getKey())) {
                            conditionControlOutputPortId = formatString(connectionPortModel.getParameter().getValue());
                        }
                        if (BpmnDefinition.CONTROL_OUTPUT.equals(connectionPortModel.getType())) {
                            relatedControlOutputPortList.add(connectionPortModel);
                        }
                    }
                }

                ConnectionPortModel defaultConnectionPortModel = null;
                for (ConnectionPortModel connectionPortModel : relatedControlOutputPortList) {
                    if (!conditionControlOutputPortId.equals(connectionPortModel.getId())) {
                        defaultConnectionPortModel = connectionPortModel;
                    }
                }

                // TODO このへんのValidation系の実装はこの段階で行うのではなく、もっと前に行うべき。
//				List<ConnectionPortModel> sizecheckInputPort = new ArrayList<>();
//				for (ConnectionPortModel portModel : relatedDataInputPortList) {
//					String sizeCheckKey = portModel.getParameter().getKey();
//					if (sizeCheckKey.equals(BpmnDefinition.CONDITION) || sizeCheckKey.equals(BpmnDefinition.CONDITION_CONNECTION_PORT_ID)) {
//						sizecheckInputPort.add(portModel);
//					}
//				}
//				if (sizecheckInputPort.size() != 2) {
//					// Diverging の時、INPUTが2つでない場合エラーとする
//					throw new MwaInstanceError(MWARC.INVALID_INPUT, null,
//							"There are two DATA_INPUT of Diverging Gateway.");
//				}

                // Default Sequence になる Target Step のIDを取得する
                String defaultTargetStepId = "";
                for (ConnectionModel connection : exeBpmnWorkflowModel.getConnections()) {
                    // connections から、Default となる CONTROL_OUTPUT で紐づく target の ConnectionPortId を判別する。
                    if (defaultConnectionPortModel.getId().equals(connection.getFromConnectionPortId())) {
                        // この時の ToConnectionPortId が target の ConnectionPortId となる。
                        for (ConnectionPortModel connectionPortModel : exeBpmnWorkflowModel.getConnectionPorts()) {
                            if (connection.getToConnectionPortId().equals(connectionPortModel.getId())) {
                                // TargetRef と比較するので、StepId を代入する。
                                defaultTargetStepId = connectionPortModel.getStepId();
                                break;
                            }
                        }
                        break;
                    }
                }

                // DefaultになるTargetPortのステップを取得する
                for (Map.Entry<String, FlowModel> flowModelMapEntry : flowModelMap.entrySet()) {
                    FlowModel flowModel = flowModelMapEntry.getValue();
                    if ((BpmnDefinition.PREFIX_OF_BPMN_ID + gatewayModel.getName()).equals(flowModel.getSourceRef()) && defaultTargetStepId.equals(flowModel.getTargetRef())) {
                        gatewayModel.setDefaultEsc(flowModel.getId());
                        break;
                    }
                }

                // 4. GatewayのCondition情報を登録する
                // Gatewayに紐づく条件式と対象のDefaultPortを取得する
                String condition = "";
                for (ConnectionPortModel dataInputPortModel : relatedDataInputPortList) {
                    if (BpmnDefinition.CONDITION.equals(dataInputPortModel.getParameter().getKey())) {
                        String conditionListString = formatString(dataInputPortModel.getParameter().getValue());
                        ObjectMapper mapper = new ObjectMapper();
                        List<ConditionModel> conditionModelList = new ArrayList<>();
                        try {
                            conditionModelList = mapper.readValue(conditionListString, new TypeReference<List<ConditionModel>>() {
                            });
                        } catch (Exception e) {
                            logger.error("Failed to convert ConditionModel.", e);
                        }
                        condition = createCondition(conditionModelList);
                    }
                }

                // 紐づくSequenceFlowにConditioinExpressionを追加する
                for (Map.Entry<String, FlowModel> flowModelMapEntry : flowModelMap.entrySet()) {
                    if (!condition.isEmpty() && defaultTargetStepId != null && gatewayModel.getId().equals(flowModelMapEntry.getValue().getSourceRef()) &&
                            !defaultTargetStepId.equals(flowModelMapEntry.getValue().getTargetRef())) {
                        flowModelMapEntry.getValue().createBpmnConditionExpressionModel(condition, index);
                        break;
                    }
                }
            }
        }
    }

    public String formatString(String id) {
        if (id.substring(0, 2).equals(BpmnDefinition.ESCAPE_TARGET) && id.substring(id.length() - 2, id.length()).equals(BpmnDefinition.ESCAPE_TARGET)) {
            id = id.substring(2, id.length() - 2);
        }
        return id;
    }

    private String createCondition(List<ConditionModel> conditionModelList) {
        StringBuffer stringBuffer = new StringBuffer();
        int conditionListSize = conditionModelList.size();
        for (int i = 0; conditionListSize > i; i++) {
            ConditionModel conditionModel = conditionModelList.get(i);
            // 先頭のConditionOperatorは必ず無視する仕様
            if (i != 0) {
                stringBuffer.append(convertConditionOperator(conditionModel.getConditionOperator()));
            }
            String leftValue = createConditionParam(conditionModel.getLeftValue());
            leftValue.replace("_", "-");
            String rightValue = createConditionParam(conditionModel.getRightValue()).replace("\\\"", "\"");
            rightValue.replace("_", "-");
            stringBuffer.append(leftValue);
            stringBuffer.append(conditionModel.getComparisonOperator());
            stringBuffer.append(rightValue);
            // Listの先頭から評価する仕様のため、今回が"||"で次回が"&&"のときは()で挟む
            if (i != 0 && BpmnDefinition.OR_CONDITION.equals(conditionModel.getConditionOperator()) &&
                    conditionListSize > (i + 1) && BpmnDefinition.AND_CONDITION.equals(conditionModelList.get(i + 1).getConditionOperator())) {
                stringBuffer.insert(0, "(");
                stringBuffer.append(")");
            }
        }
        return stringBuffer.toString();
    }

    private String convertConditionOperator(String conditionOperator) {
        switch (conditionOperator) {
            case BpmnDefinition.AND_CONDITION:
                return BpmnDefinition.CONVERTED_AND_CONDITION;
            default:
                return conditionOperator;
        }
    }

    /**
     * conditionのvalueに入ってくる値を判別し式で使える文字列に変換する
     *
     * @param value
     * @return String
     */
    private String createConditionParam(String value) {
        String result = null;
        if (value == null) {
        	//TODO：暫定で文字列想定
            result = "null";
        } else if (value.startsWith(BpmnDefinition.PREFIX_OF_BPMN_ID)) {
            // グローバル変数を指定する場合
            if (convertForGlobalVariable(value.substring(0, value.indexOf("."))).equals(this.startStepId)) {
                // StepId が、StartTask の ID であれば key 名のみとする。
                result = value.substring(value.indexOf(".") + 1);
            } else {
                // StartTask でない場合は、StepId と 接続部の「.」を「_」に変換する
                result = convertForGlobalVariable(value);
            }
        } else if (value.startsWith("\"")) {
            // 文字列で指定する場合
            result = "'" + value.substring(1, value.length() - 1) + "'";
        } else {
            // 数字を指定する場合
            // ※UI側で入力制御を行うので上記以外はない
            result = value;
        }
        return result;
    }

    /**
     * WorkflowModel内のStepIdのConvert処理を行う
     * "-"から"_"へ
     *
     * @param workflowModel
     */
    private void convertStepId(WorkflowModel workflowModel) {
        for (StepModel step : workflowModel.getSteps()) {
            // Step の中の StepId を変換する
            String stepId = step.getId();
            String conversionStepId = BpmnDefinition.PREFIX_OF_BPMN_ID + convertForGlobalVariable(stepId);
            step.setId(conversionStepId);
            for (ConnectionPortModel connectionPort : workflowModel.getConnectionPorts()) {
                // connectionPort の中の StepId を変換する
                if (connectionPort.getStepId().equals(stepId)) {
                    connectionPort.setStepId(conversionStepId);
                }
            }
        }
    }

    /**
     * グローバル変数で使用するための変換を行う
     *
     * @param str
     * @return 変換後文字列
     */
    private String convertForGlobalVariable(String str) {
        return str.replace("-", "_").replace(".", "_");
    }

    /**
     * stepNameで使用するための変換を行う
     *
     * @param str
     * @return 変換後文字列
     */
    private String convertUnderScoreToDash(String str) {
        return str.replace("_", "-");
    }

    /**
     * ConnectionPortsのStepIdを基にStepTypeを取得する
     *
     * @param workflowModel
     * @param stepId
     */
    private KBaseUtil.StepType getStepTypeFromStepId(WorkflowModel workflowModel, String stepId) {
        KBaseUtil.StepType result = null;
        for (StepModel step : workflowModel.getSteps()) {
            if (stepId.equals(step.getId())) {
                result = KBaseUtil.StepType.getStepType(step.getActivity().get("name").toString());
            }
        }
        return result;
    }

    /**
     * BPMNでグローバル変数の作成
     * 命名規則：stepId + "_" + key名
     * WFのOUTPUT（EndTask）の時は、「OUTPUT.key名」とする。
     * WFのINPUT（StartTask）の時は、「key名」とする。
     *
     * @param output
     * @param stepId
     * @param propertyModelMap
     */
    private void createOutputProperty(Parameter output, String stepId, KBaseUtil.StepType stepType, Map<String, PropertyModel> propertyModelMap, boolean isWorkflowInput) {
        // 命名規則：stepId + "_" + key名
        //  ※ stepId は、UUID でくるので「-」を「_」に変換する処理が必要。
        //  WFのOUTPUT（EndTask）の時は、「OUTPUT.key名」とする。
        //  WFのINPUT（StartTask）の時は、「key名」とする。
        String key = output.getKey();
        String prefix = stepId + BpmnDefinition.CONNECTION_PART_OF_GLOBAL_ID;
        //if (KBaseUtil.StepType.START == stepType && isWorkflowInput) {
        if (KBaseUtil.StepType.START == stepType) {
            prefix = "";
        } else if (KBaseUtil.StepType.END == stepType) {
            prefix = BpmnDefinition.PREFIX_OF_OUTPUT_PARAMETER;
        }
        PropertyModel propertyModel = new PropertyModel(prefix + key);
        propertyModel.setSubjectRef(output.getTypeName());
        propertyModelMap.put(propertyModel.getId(), propertyModel);
    }

    /**
     * Step が Start, End, ConditionalBranch, ConvergingGateway, ParallelBranchの場合は、各Taskを追加する
     *
     * @param workflowModel
     * @param taskModelMap
     */
    private void createTask(WorkflowModel workflowModel, Map<String, TaskModel> taskModelMap){
        List<StepModel> newStepList = new ArrayList<StepModel>();
        for (StepModel step : workflowModel.getSteps()) {
            if (step == null || step.getName() == null || step.getActivity() == null) {
                throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "No required parameter for task model.");
            }
            KBaseUtil.StepType stepType = KBaseUtil.StepType.getStepType(step.getActivity().get("name").toString());
            String originalStepId = step.getId();
            if (KBaseUtil.StepType.ACTIVITY == stepType) {
                // Activityの時は、何もしない。
            } else if ((KBaseUtil.StepType.CONDITION_BRANCH == stepType)
                    || (KBaseUtil.StepType.PARALLEL_BRANCH == stepType)
                    || (KBaseUtil.StepType.END == stepType)) {
                createTaskData(workflowModel, newStepList, step, originalStepId, stepType);
                createConnectionPortAndConnectionFrontTaskModel(workflowModel, step.getId(), originalStepId);
            } else if ((KBaseUtil.StepType.START == stepType)
                    || (KBaseUtil.StepType.CONVERGING_BRANCH == stepType)
                    || (KBaseUtil.StepType.PARALLEL_CONVERGING_BRANCH == stepType)) {
                createTaskData(workflowModel, newStepList, step, originalStepId, stepType);
                createConnectionPortAndConnectionBehindTaskModel(workflowModel, step.getId(), originalStepId);
            }
        }
        for (StepModel newStep : newStepList) {
            workflowModel.getSteps().add(newStep);
        }
    }

    /**
     * Step, Connection Port, 及びConnection情報を生成します
     *
     * @param workflowModel  該当のWorkflowModel
     * @param newStepList    新規に生成されるStepを格納するList
     * @param step           基となるStep
     * @param originalStepId 基となるStepのId
     */
    private void createTaskData(WorkflowModel workflowModel, List<StepModel> newStepList, StepModel step, String originalStepId, KBaseUtil.StepType stepType) {
        // TODO 本当はここで変換したIDを使うのではなく、普通のUUIDを使うようにしたい。
//		    String newStepId = UUID.randomUUID().toString();
        String newStepId = convertForGlobalVariable(UUID.randomUUID().toString());

        // 追加するStepにOriginalのIDを入れ、Originalのステップには新規IDを適用する。 → IDの割り当てロジックを逆にするとWorkflow Monitor画面にStatusが表示されない
        changeStepId(step, newStepId, workflowModel.getConnectionPorts());
        changeConditionalScript(workflowModel.getConnectionPorts(), originalStepId, newStepId);

        StepModel newStep = createNewStep(step, originalStepId);
        List<ConnectionPortModel> connectionPorts = workflowModel.getConnectionPorts();
        List<ConnectionPortModel> inputConnectionPorts = new ArrayList<>();
        List<ConnectionPortModel> relatedConnectionPorts = getRelatedConnectionPorts(connectionPorts, step);

        if (!(stepType.equals(KBaseUtil.StepType.CONDITION_BRANCH))) {
            for (ConnectionPortModel relatedConnectionPort : relatedConnectionPorts) {
                if (BpmnDefinition.DATA_INPUT.equals(relatedConnectionPort.getType())) {
                    ConnectionPortModel newConnectionPort = createNewConnectionPort(relatedConnectionPort, newStep);
                    inputConnectionPorts.add(newConnectionPort);
                    if (!(relatedConnectionPort.getParameter().getKey().equals("ProfileId"))) {
                        // Connectionシーケンスの作成
                        workflowModel.getConnections().add(createNewConnectionModel(relatedConnectionPorts, relatedConnectionPort, newConnectionPort));
                    }
                }
            }
            for (ConnectionPortModel newConnectionPort : inputConnectionPorts) {
                connectionPorts.add(newConnectionPort);
            }
        }

        newStepList.add(createNewStep(step, originalStepId));
        // 追加したStepとnameがかぶってしまうため、元のStepのnameをidに変更する
        changeStepName(step);
    }

    /**
     * 該当のStepに関連するConnection Portを取得します
     *
     * @param connectionPorts 　全Connection Port
     * @param step            該当のStep
     * @return relatedConnectionPorts 該当のStepに関連するConnection PortのList
     */
    private List<ConnectionPortModel> getRelatedConnectionPorts(List<ConnectionPortModel> connectionPorts, StepModel step) {
        List<ConnectionPortModel> relatedConnectionPorts = new ArrayList<>();
        for (ConnectionPortModel connectionPort : connectionPorts) {
            if (step.getId().equals(connectionPort.getStepId())) {
                relatedConnectionPorts.add(connectionPort);
            }
        }
        return relatedConnectionPorts;
    }

    /**
     * Connection Portを新規に生成します
     *
     * @param connectionPort 基となるConnection Port
     * @param newStep        該当のStep
     * @return newConnectionPort 生成したConnection Port
     */
    private ConnectionPortModel createNewConnectionPort(ConnectionPortModel connectionPort, StepModel newStep) {
        ConnectionPortModel newConnectionPort = new ConnectionPortModel();
        newConnectionPort.setId(UUID.randomUUID().toString());
        newConnectionPort.setStepId(newStep.getId());
        newConnectionPort.setType(connectionPort.getType());
        newConnectionPort.setParameter(connectionPort.getParameter());
        newConnectionPort.setPosition(connectionPort.getPosition());
        return newConnectionPort;
    }

    /**
     * Connection Modelを新規に生成します
     *
     * @param relatedConnectionPorts  該当のStepに関連するConnection Port
     * @param dataInputConnectionPort 基となるConnection Port
     * @param newConnectionPort       新規に生成されるConnection Port
     * @return newConnection 生成したConnection Model
     */
    private ConnectionModel createNewConnectionModel(List<ConnectionPortModel> relatedConnectionPorts, ConnectionPortModel dataInputConnectionPort, ConnectionPortModel newConnectionPort){
        ConnectionModel newConnection = new ConnectionModel();
        // 紐づくOutputの検索
        for (ConnectionPortModel relatedConnectionPort : relatedConnectionPorts) {
            if (BpmnDefinition.DATA_OUTPUT.equals(relatedConnectionPort.getType()) && relatedConnectionPort.getParameter() != null && dataInputConnectionPort.getParameter().getKey().equals(relatedConnectionPort.getParameter().getKey())) {
                newConnection.setFromConnectionPortId(relatedConnectionPort.getId());
                break;
            }
        }
        if (newConnection.getFromConnectionPortId() == null) {
            throw new MwaError(MWARC.INVALID_INPUT,null,"fromConnectionPortId of ConnectionModel is invalid.");
        }
        newConnection.setToConnectionPortId(newConnectionPort.getId());
        return newConnection;
    }

    /**
     * イベント用のTaskとなるStepを生成する
     *
     * @param baseStep
     * @return StepModel
     */
    private StepModel createNewStep(StepModel baseStep, String newTaskStepId) {
        StepModel newStep = new StepModel();
        newStep.setId(newTaskStepId);
        String stepName = baseStep.getName();
        newStep.setName(stepName);
        newStep.setActivity(baseStep.getActivity());
        newStep.setPosition(baseStep.getPosition());
        return newStep;
    }

    private void changeStepId(StepModel step, String newStepId, List<ConnectionPortModel> connectionPorts) {
        for (ConnectionPortModel connectionPort : connectionPorts) {
            if (step.getId().equals(connectionPort.getStepId())) {
                connectionPort.setStepId(newStepId);
            }
        }
        step.setId(newStepId);
    }

    /**
     * 新Task追加に伴い、基のStepのStepNameを変更する
     *
     * @param step
     */
    private void changeStepName(StepModel step) {
        step.setName(step.getId());
    }

    private void changeConditionalScript(List<ConnectionPortModel> connectionPorts, String originalStepId, String newStepId) {
        for (ConnectionPortModel connectionPort : connectionPorts) {
            if (connectionPort.getType().equals("DATA_INPUT") && connectionPort.getParameter().getKey().equals("condition")) {
                // TODO 呼び出し元で変換処理がかかっていない普通のUUIDが入っているなら、下記を削除できる
                String targetNewStepId = newStepId.replace("_", "-");
                connectionPort.getParameter().setValue(connectionPort.getParameter().getValue().replace(originalStepId, targetNewStepId));
            }
        }
    }

    /**
     * @param step
     * @param taskModelMap
     * @param gatewayModelMap
     * @param startEventModelMap
     * @param endEventModelMap
     */
    private void createStepParameter(
            StepModel step,
            Map<String, TaskModel> taskModelMap,
            Map<String, GatewayModel> gatewayModelMap,
            Map<String, ParallelGatewayModel> parallelGatewayModelMap,
            Map<String, StartEventModel> startEventModelMap,
            Map<String, EndEventModel> endEventModelMap) {
        // StepModel の name を基に、StepType を取得する。
        KBaseUtil.StepType stepType = KBaseUtil.StepType.getStepType(step.getActivity().get("name").toString());

        // 全ての Step に Activity がある想定になったので、Activity が取得できないとエラーとする。
        if (null == step.getActivity()) {
            throw new MwaInstanceError(MWARC.INVALID_INPUT, null,"No required parameter for task model.");
        }

        // StepType毎にModel作成処理を行う。
        if ((BpmnDefinition.PREFIX_OF_BPMN_ID + step.getName()).equals(step.getId())) {
            if (KBaseUtil.StepType.ACTIVITY == stepType) {
                // ACTIVITYステップ
                // ※ACTIVITYステップの場合は、TaskModelを作成しMapに追加する処理のみ。
                // holderにTaskビルダーを登録する。
                taskModelMap.put(step.getId(), createTaskModel(step, step.getId()));

//			} else if (("OR").equals(step.getType())) {
//				// BRANCHステップ
//				// Gatewayは仕様未確定の為、いったん保留
//
            } else if (KBaseUtil.StepType.CONDITION_BRANCH == stepType) {
                // Condition Branch (Diverging)ステップ
                this.conditionalStepIds.add(step.getId());
                gatewayModelMap.put(step.getId(), createGatewayModel(step, BpmnDefinition.DIRECTON_DIVERGING));
            } else if (KBaseUtil.StepType.PARALLEL_BRANCH == stepType) {
                // Parallel Branch ステップ ⇒ Parallel の場合もGatewayDirectionには、「Diverging」を入れる
                parallelGatewayModelMap.put(step.getId(), createParallelGatewayModel(step, BpmnDefinition.DIRECTON_DIVERGING));
            } else if (KBaseUtil.StepType.CONVERGING_BRANCH == stepType) {
                // Converging Branch ステップ
                this.convergingStepIds.add(step.getId());
                gatewayModelMap.put(step.getId(), createGatewayModel(step, BpmnDefinition.DIRECTON_CONVERGING));
            } else if (KBaseUtil.StepType.PARALLEL_CONVERGING_BRANCH == stepType) {
                // Parallel Converging Branch ステップ
                parallelGatewayModelMap.put(step.getId(), createParallelGatewayModel(step, BpmnDefinition.DIRECTON_CONVERGING));
            } else if (KBaseUtil.StepType.START == stepType) {
                // StartEvent
                this.startStepId = step.getId();
                StartEventModel startModel = new StartEventModel(this.startStepId);

                startModel.setName(KBaseUtil.StepType.START.toString());
                startEventModelMap.put(startModel.getId(), startModel);
            } else if (KBaseUtil.StepType.END == stepType) {
                // EndEvent
                this.endStepId = step.getId();
                EndEventModel endEventModel = new EndEventModel(this.endStepId);

                endEventModel.setName(KBaseUtil.StepType.END.toString());
                endEventModelMap.put(endEventModel.getId(), endEventModel);
            }
        } else {
            // 新規追加Taskの場合、Activityとして処理を行う
            // holderにTaskビルダーを登録する。
            taskModelMap.put(step.getId(), createTaskModel(step, step.getId()));
        }
    }

    /**
     * TaskModelMapに追加するTaskModelを作成する。
     *
     * @param step
     * @param stepId
     * @return
     */
    private TaskModel createTaskModel(StepModel step, String stepId) {
        TaskModel taskModel = new TaskModel(stepId);
        String taskName = this.convertUnderScoreToDash(stepId.substring(1));
        taskModel.setName(taskName);
        taskModel.setDisplayName(step.getActivity().get("name").toString());
        taskModel.setTaskName(generateMwaWorkflowTaskName(
                step.getActivity().get("name").toString(),
                step.getActivity().get("version").toString()));

        return taskModel;
    }

    /**
     * GatewayModelに追加するgatewayModelを作成する。
     *
     * @param step
     * @param gatewayDirection
     * @return
     */
    private GatewayModel createGatewayModel(StepModel step, String gatewayDirection) {
        GatewayModel gatewayModel = new GatewayModel(step.getId());
        String stepName = step.getName();
        gatewayModel.setName(stepName);
        gatewayModel.setGatewayDirection(gatewayDirection);
        return gatewayModel;
    }

    /**
     * ParrallelGatewayModelに追加するparallelGatewayModelを作成する。
     *
     * @param step
     * @param gatewayDirection
     * @return
     */
    private ParallelGatewayModel createParallelGatewayModel(StepModel step, String gatewayDirection) {
        ParallelGatewayModel parallelGatewayModel = new ParallelGatewayModel(step.getId());
        String stepName = step.getName();
        parallelGatewayModel.setName(stepName);
        parallelGatewayModel.setGatewayDirection(gatewayDirection);
        return parallelGatewayModel;
    }

    /**
     * 実際のEventの後ろに作成したTaskModelに紐づくconnectionsの編集、ConnectionPortの追加を行う
     * (startTask, convergingBranchTask)
     *
     * @param workflowModel
     * @param existStepId   作成されるステップのベースになるステップのID
     * @param newStepId     これから作成されるステップのID
     */
    private void createConnectionPortAndConnectionBehindTaskModel(WorkflowModel workflowModel, String existStepId, String newStepId) {
        // 1. connectionsを回して、fromに「該当StepId」かつそのConnectionPortが「CONTROL_OUTPUT」になっているconnectionを取り出す
        // 2. 新規ConnectionPortを2つ生成する。（追加Taskの「CONTROL_INPUT」「CONTROL_OUTPUT」）
        // 3. 対象のconnectionを削除して、新規connectionを2つ生成する。
        for (ConnectionModel connection : workflowModel.getConnections()) {
            boolean createStatus = false;
            String fromConnectionPortId = connection.getFromConnectionPortId();
            for (ConnectionPortModel connectionPort : workflowModel.getConnectionPorts()) {
                if (connectionPort.getId().equals(fromConnectionPortId)
                        && connectionPort.getStepId().equals(existStepId)
                        && connectionPort.getType().equals(BpmnDefinition.CONTROL_OUTPUT)) {
                    createConnectionPortAndConnectionForNewTaskModel(workflowModel, connection, newStepId);
                    createStatus = true;
                    break;
                }
            }
            if (createStatus) {
                break;
            }
        }
    }

    /**
     * 実際のEventの前に作成したTaskModelに紐づくconnectionsの編集、ConnectionPortの追加を行う
     * (endTask, conditionBranchTask, parallelBranchTask)
     *
     * @param workflowModel
     * @param existStepId
     * @param newStepId
     */
    private void createConnectionPortAndConnectionFrontTaskModel(WorkflowModel workflowModel, String existStepId, String newStepId) {
        // 1. connectionsを回して、Toに「該当StepId」かつそのConnectionPortが「CONTROL_INPUT」になっているconnectionを取り出す
        // 2. 新規ConnectionPortを2つ生成する。（追加Taskの「CONTROL_INPUT」「CONTROL_OUTPUT」）
        // 3. 対象のconnectionを削除して、新規connectionを2つ生成する。
        for (ConnectionModel connection : workflowModel.getConnections()) {
            boolean createStatus = false;
            String ToConnectionPortId = connection.getToConnectionPortId();
            for (ConnectionPortModel connectionPort : workflowModel.getConnectionPorts()) {
                if (connectionPort.getId().equals(ToConnectionPortId)
                        && connectionPort.getStepId().equals(existStepId)
                        && connectionPort.getType().equals(BpmnDefinition.CONTROL_INPUT)) {
                    createConnectionPortAndConnectionForNewTaskModel(workflowModel, connection, newStepId);
                    createStatus = true;
                    break;
                }
            }
            if (createStatus) {
                break;
            }
        }
    }

    /**
     * connectionsを指定して、connectionsの編集、ConnectionPortの追加を行う
     *
     * @param workflowModel
     * @param targetConnection
     * @param newStepId
     */
    private void createConnectionPortAndConnectionForNewTaskModel(WorkflowModel workflowModel, ConnectionModel targetConnection, String newStepId) {
        // connectionPortModelを作成する。
        ConnectionPortModel fromConnectionPort = new ConnectionPortModel();
        String newFromConnectionPortId = UUID.randomUUID().toString();
        fromConnectionPort.setId(newFromConnectionPortId);
        fromConnectionPort.setStepId(newStepId);
        fromConnectionPort.setType(BpmnDefinition.CONTROL_INPUT);
        // TODO:あとでPositionの対応が必要になった時実装する。
//		fromConnectionPort.setPosition();

        ConnectionPortModel toConnectionPort = new ConnectionPortModel();
        String newToConnectionPortId = UUID.randomUUID().toString();
        toConnectionPort.setId(newToConnectionPortId);
        toConnectionPort.setStepId(newStepId);
        toConnectionPort.setType(BpmnDefinition.CONTROL_OUTPUT);
        // TODO:あとでPositionの対応が必要になった時実装する。
//		toConnectionPort.setPosition();

        workflowModel.getConnectionPorts().add(fromConnectionPort);
        workflowModel.getConnectionPorts().add(toConnectionPort);

        ConnectionModel fromConnection = new ConnectionModel();
        fromConnection.setFromConnectionPortId(targetConnection.getFromConnectionPortId());
        fromConnection.setToConnectionPortId(newFromConnectionPortId);

        ConnectionModel toConnection = new ConnectionModel();
        toConnection.setFromConnectionPortId(newToConnectionPortId);
        toConnection.setToConnectionPortId(targetConnection.getToConnectionPortId());

        workflowModel.getConnections().add(fromConnection);
        workflowModel.getConnections().add(toConnection);
        workflowModel.getConnections().remove(targetConnection);
    }

    /**
     * MwaのWorkflowで用いるタスク名を生成する。
     *
     * @param name
     * @param version
     * @return
     */
    private String generateMwaWorkflowTaskName(String name, String version) {
        return name + "@" + version;
    }

    /**
     * ConnectionPort の type:DATA_OUTPUT のデータを対象とし、Association 情報を作成する。
     *
     * @param connectionPort
     * @param taskModelMap
     */
    private void createDataOutputAssociation(ConnectionPortModel connectionPort, Map<String, TaskModel> taskModelMap, WorkflowModel exeBpmnWorkflowModel) {
        Parameter param = connectionPort.getParameter();
        if (param != null) {
            String connectionPortStepId = connectionPort.getStepId();
            KBaseUtil.StepType stepType = getStepTypeFromStepId(exeBpmnWorkflowModel, connectionPort.getStepId());

            // 親タスクに property を登録する。
            BuilderPropertyModel property = new BuilderPropertyModel(
                    param.getKey(), param.getKey(), param.getTypeName(), param.getValue(),
                    connectionPortStepId, null, stepType);
            // taskModelMap は、StepId で管理するので、StepId で取得する。
            TaskModel taskModel = taskModelMap.get(connectionPortStepId);
            taskModel.putOutput(property); /* パラメータの登録 */
        }
    }

    /**
     * ConnectionPort の type:DATA_INPUT のデータを対象とし、Association 情報を作成する。
     *
     * @param connectionPort
     * @param taskModelMap
     */
    private void createImmediateDataInputAssociation(ConnectionPortModel connectionPort, Map<String, TaskModel> taskModelMap, WorkflowModel exeBpmnWorkflowModel) {
        Parameter param = connectionPort.getParameter();
        if (param != null && (null != param.getValue())) {
            String connectionPortStepId = connectionPort.getStepId();
            KBaseUtil.StepType stepType = getStepTypeFromStepId(exeBpmnWorkflowModel, connectionPort.getStepId());

            // 親タスクに property を登録する。
            BuilderPropertyModel property = new BuilderPropertyModel(
                    param.getKey(), param.getKey(), param.getTypeName(), param.getValue(),
                    null, connectionPortStepId, stepType);
            // taskModelMap は、StepId で管理するので、StepId で取得する。
            TaskModel taskModel = taskModelMap.get(connectionPortStepId);
            taskModel.putInput(property); /* パラメータの登録 */
        }
    }

    /**
     * Create a FlowModel for StartEvent
     *
     * @param connectionModel
     * @param connectionPortModel
     * @param flowId
     * @param startEventModelMap
     * @param flowModelMap
     */
    private void setOutgoingForStartEvent(
            ConnectionModel connectionModel,
            ConnectionPortModel connectionPortModel,
            String flowId,
            Map<String, StartEventModel> startEventModelMap,
            Map<String, FlowModel> flowModelMap) {
        if ((PortType.CONTROL_OUTPUT.name()).equals(connectionPortModel.getType()) &&
                connectionPortModel.getId().equals(connectionModel.getFromConnectionPortId())) {
            // ConnectionPortModelのCONTROL_OUTPUT かつ connections.OutputConnectionPortId にあるID の場合
            FlowModel flowModel = flowModelMap.get(flowId);
            flowModel.setSourceRef(connectionPortModel.getStepId());
            startEventModelMap.get(connectionPortModel.getStepId()).getOutgoing().add(flowId);
        }
    }

    /***
     * Create a FlowModel for EndEvent
     *
     * @param connectionModel
     * @param connectionPortModel
     * @param flowId
     * @param endEventModelMap
     * @param flowModelMap
     */
    private void setIncomingForEndEvent(
            ConnectionModel connectionModel,
            ConnectionPortModel connectionPortModel,
            String flowId,
            Map<String, EndEventModel> endEventModelMap,
            Map<String, FlowModel> flowModelMap) {
        if ((PortType.CONTROL_INPUT.name()).equals(connectionPortModel.getType()) &&
                connectionPortModel.getId().equals(connectionModel.getToConnectionPortId())) {
            FlowModel flowModel = flowModelMap.get(flowId);
            flowModel.setTargetRef(connectionPortModel.getStepId());
            endEventModelMap.get(connectionPortModel.getStepId()).getIncoming().add(flowId);
        }
    }

    /***
     *  Create a FlowModel for Gateway
     *
     * @param connectionModel
     * @param connectionPortModel
     * @param flowId
     * @param gatewayModelMap
     * @param flowModelMap
     */
    private void setIncomingOutgoingForGateway(
            ConnectionModel connectionModel,
            ConnectionPortModel connectionPortModel,
            String flowId,
            Map<String, GatewayModel> gatewayModelMap,
            Map<String, FlowModel> flowModelMap) {
        FlowModel flowModel = flowModelMap.get(flowId);
        GatewayModel gatewayModel = gatewayModelMap.get(connectionPortModel.getStepId());
        // 1. Input information
        if ((PortType.CONTROL_INPUT.name()).equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getToConnectionPortId())) {
            flowModel.setTargetRef(connectionPortModel.getStepId());
            gatewayModel.getIncoming().add(flowId);
        }
        // 2. Output information
        if ((PortType.CONTROL_OUTPUT.name()).equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getFromConnectionPortId())) {
            flowModel.setSourceRef(connectionPortModel.getStepId());
            gatewayModel.getOutgoing().add(flowId);
        }
    }

    /***
     *  Create a FlowModel for ParallelGateway
     *
     * @param connectionModel
     * @param connectionPortModel
     * @param flowId
     * @param parallelGatewayModelMap
     * @param flowModelMap
     */
    private void setIncomingOutgoingForParallelGateway(
            ConnectionModel connectionModel,
            ConnectionPortModel connectionPortModel,
            String flowId,
            Map<String, ParallelGatewayModel> parallelGatewayModelMap,
            Map<String, FlowModel> flowModelMap) {
        FlowModel flowModel = flowModelMap.get(flowId);
        ParallelGatewayModel gatewayModel = parallelGatewayModelMap.get(connectionPortModel.getStepId());
        // 1. Input information
        if ((PortType.CONTROL_INPUT.name()).equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getToConnectionPortId())) {
            flowModel.setTargetRef(connectionPortModel.getStepId());
            gatewayModel.getIncoming().add(flowId);
        }
        // 2. Output information
        if ((PortType.CONTROL_OUTPUT.name()).equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getFromConnectionPortId())) {
            flowModel.setSourceRef(connectionPortModel.getStepId());
            gatewayModel.getOutgoing().add(flowId);
        }
    }

    /**
     * Create a FlowModel for Task (StepType: Activity)
     *
     * @param connectionModel
     * @param connectionPortModel
     * @param flowId
     * @param taskModelMap
     * @param propertyModelMap
     * @param flowModelMap
     * @param connectionPortList
     */
    private void setIoSpecificationAndAssociationForTask(
            ConnectionModel connectionModel,
            ConnectionPortModel connectionPortModel,
            String flowId,
            Map<String, TaskModel> taskModelMap,
            Map<String, PropertyModel> propertyModelMap,
            Map<String, FlowModel> flowModelMap,
            List<ConnectionPortModel> connectionPortList) {
        // Control Portの場合、FlowにSourceRef/TargetRefの値を詰めてTaskModelを更新する
        if (PortType.CONTROL_OUTPUT.name().equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getFromConnectionPortId())) {
            setOutgoingForControlOutputPort(connectionPortModel, flowId, taskModelMap, flowModelMap);
        }
        if (PortType.CONTROL_INPUT.name().equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getToConnectionPortId())) {
            setInComingForControlInputPort(connectionPortModel, flowId, taskModelMap, flowModelMap);
        }

        // DATA_INPUTのときはBuilderPropertyModelを生成し、TaskModelを更新する
        if ((PortType.DATA_INPUT.name()).equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getToConnectionPortId())) {
            setDataInputAndDataInputAssociationForDataInputPort(connectionModel, connectionPortModel, flowId, taskModelMap, flowModelMap, connectionPortList);
        }

        // DATA_OUTPUTのときはBuilderPropertyModelを生成し、TaskModelを更新する
        if ((PortType.DATA_OUTPUT.name()).equals(connectionPortModel.getType()) && connectionPortModel.getId().equals(connectionModel.getFromConnectionPortId())) {
            setDataOutputAndDataOutputAssociationForDataOutputPort(connectionModel, connectionPortModel, flowId, taskModelMap, flowModelMap, connectionPortList);
        }
    }

    /**
     * ControlOutputPortからSourceRefとOutgoingを設定する
     *
     * @param connectionPortModel
     * @param flowId
     * @param taskModelMap
     * @param flowModelMap
     */
    private void setOutgoingForControlOutputPort(ConnectionPortModel connectionPortModel, String flowId, Map<String, TaskModel> taskModelMap, Map<String, FlowModel> flowModelMap) {
        FlowModel flowModel = flowModelMap.get(flowId);
        // SourceRef には StepId を set する。
        flowModel.setSourceRef(connectionPortModel.getStepId());
        // taskModelMap は、StepId で管理するので StepId で取得する。
        taskModelMap.get(connectionPortModel.getStepId()).getOutgoing().add(flowId);
    }

    /**
     * ControlInputPortからTargetRefとIncomingを設定する
     *
     * @param connectionPortModel
     * @param flowId
     * @param taskModelMap
     * @param flowModelMap
     */
    private void setInComingForControlInputPort(ConnectionPortModel connectionPortModel, String flowId, Map<String, TaskModel> taskModelMap, Map<String, FlowModel> flowModelMap) {
        FlowModel flowModel = flowModelMap.get(flowId);
        // TargetRef には StepId を set する。
        flowModel.setTargetRef(connectionPortModel.getStepId());
        // taskModelMap は、StepId で管理するので、StepId で取得する。
        taskModelMap.get(connectionPortModel.getStepId()).getIncoming().add(flowId);
    }

    /**
     * DataInputPortからDataInputとDataInputAssociationを設定する
     *
     * @param connectionModel
     * @param connectionPortModel
     * @param flowId
     * @param taskModelMap
     * @param flowModelMap
     * @param connectionPortList
     */
    private void setDataInputAndDataInputAssociationForDataInputPort(
            ConnectionModel connectionModel,
            ConnectionPortModel connectionPortModel,
            String flowId,
            Map<String, TaskModel> taskModelMap,
            Map<String, FlowModel> flowModelMap,
            List<ConnectionPortModel> connectionPortList) {
        Parameter param = null;
        Parameter toParameter = connectionPortModel.getParameter();
        // target
        String toConnectionPortStepId = connectionPortModel.getStepId();
        // source
        String fromConnectionPortStepId = "";
        // connectionPortModelのListをループし、connectionModelの入力元のPortIdとmatchする時、Parameterを取得
        for (ConnectionPortModel connectionPort : connectionPortList) {
            if (connectionPort.getId().equals(connectionModel.getFromConnectionPortId())) {
                param = connectionPort.getParameter(); // 入力すべきParameter情報
                fromConnectionPortStepId = connectionPort.getStepId();
                break;
            }
        }
        // 取得したParameterからkey,valueをINPUT_DATA向けに修正し、parameterを作成TaskModelのinput項目として登録する
        if (param != null) {
            // sourceStep の StepType を設定する
            KBaseUtil.StepType stepType = KBaseUtil.StepType.ACTIVITY;
            //TODO: startStepがなぜかでシリアライズで複数登録され、startStepIdとのマッチでは厳密に判定できない模様
/*            if (fromConnectionPortStepId.equals(this.startStepId)) {
                stepType = KBaseUtil.StepType.START;
            }*/

            TaskModel taskModel = taskModelMap.get(connectionPortModel.getStepId());
            TaskModel fromTaskModel = taskModelMap.get(fromConnectionPortStepId);
            if (fromTaskModel != null && fromTaskModel.getDisplayName() != null) {
                if ( KBaseUtil.StepType.START.equals(KBaseUtil.StepType.getStepType(fromTaskModel.getDisplayName()))) {
                	stepType = KBaseUtil.StepType.START;
                }
            } else if (taskModel != null && taskModel.getDisplayName() != null) {
                if ( KBaseUtil.StepType.START.equals(KBaseUtil.StepType.getStepType(taskModel.getDisplayName()))) {
                	stepType = KBaseUtil.StepType.START;
                }
            }

            BuilderPropertyModel property = new BuilderPropertyModel(
                    param.getKey(), toParameter.getKey(), param.getTypeName(), param.getValue(),
                    fromConnectionPortStepId, toConnectionPortStepId, stepType);
            // taskModelMap は、StepId で管理するので、StepId で取得する。
            taskModel.putInput(property); /* 入力するパラメータの登録 */
        }
        // flowModelMapにはデータ情報は登録しないので、要素を削除する。
        if (flowModelMap.containsKey(flowId)) {
            flowModelMap.remove(flowId);
        }
    }

    /**
     * DataOutputPortからDataOutputとDataOutputAssociationを設定する
     *
     * @param connectionModel
     * @param connectionPortModel
     * @param flowId
     * @param taskModelMap
     * @param flowModelMap
     * @param connectionPortList
     */
    private void setDataOutputAndDataOutputAssociationForDataOutputPort(
            ConnectionModel connectionModel,
            ConnectionPortModel connectionPortModel,
            String flowId,
            Map<String, TaskModel> taskModelMap,
            Map<String, FlowModel> flowModelMap,
            List<ConnectionPortModel> connectionPortList) {
        Parameter param = null;
        // target
        String toConnectionPortStepId = "";
        // source
        String fromConnectionPortStepId = connectionPortModel.getStepId();
        // connectionPortModelをループさせ、入力先のidと一致するParameterを取得する
        for (ConnectionPortModel connectionPort : connectionPortList) {
            if (connectionPort.getId().equals(connectionModel.getToConnectionPortId())) {
                param = connectionPort.getParameter();
                toConnectionPortStepId = connectionPort.getStepId();
                break;
            }
        }
        if (param != null) {
            // targetStep の StepType を設定する
            KBaseUtil.StepType stepType = KBaseUtil.StepType.ACTIVITY;
            if (toConnectionPortStepId.equals(this.endStepId)) {
                stepType = KBaseUtil.StepType.END;
            }
//					if (StringUtils.isNotEmpty(param.getValue())) {
            // ここにvalueが入力されていた場合、WFのOUTPUTとして、ふるまって欲しいので。
//						param.setValue(PREFIX_OF_OUTPUT_PARAMETER + param.getValue());
//					}
            BuilderPropertyModel property = new BuilderPropertyModel(
                    param.getKey(), param.getKey(), param.getTypeName(), param.getValue(),
                    fromConnectionPortStepId, toConnectionPortStepId, stepType);
            // taskModelMap は、StepId で管理するので、StepId で取得する。
            TaskModel taskModel = taskModelMap.get(connectionPortModel.getStepId());
            taskModel.putOutput(property);
        }
        // flowModelMapにはデータ情報は登録しないので、要素を削除する。
        if (flowModelMap.containsKey(flowId)) {
            flowModelMap.remove(flowId);
        }
    }

    /**
     * @param propertyModelMap
     * @param taskModelMap
     * @param gatewayModelMap
     * @param parallelGatewayModelMap
     * @param startEventModel
     * @param endEventModel
     * @param flowModel
     */
    public void createBpmnProcessModel(
            Map<String, PropertyModel> propertyModelMap,
            Map<String, TaskModel> taskModelMap,
            Map<String, GatewayModel> gatewayModelMap,
            Map<String, ParallelGatewayModel> parallelGatewayModelMap,
            Map<String, StartEventModel> startEventModel,
            Map<String, EndEventModel> endEventModel,
            Map<String, FlowModel> flowModel) {
        bpmnProcessModel = new BpmnProcessModel();
        bpmnProcessModel.setId(this.id);
        bpmnProcessModel.setVersion(this.version);
        bpmnProcessModel.setName(this.name);
        bpmnProcessModel.setAdHoc(this.adHoc.toString());
        bpmnProcessModel.setIsExecutable(this.isExecutable.toString());
        bpmnProcessModel.setProcessType(this.processType);
        bpmnProcessModel.setPackageName(this.packageName);

        for (String key : taskModelMap.keySet()) {
            taskModelMap.get(key).createBpmnTaskModek();
            bpmnProcessModel.getTask().add(taskModelMap.get(key).getModel());
        }

        for (String key : propertyModelMap.keySet()) {
            propertyModelMap.get(key).createBpmnPropertyModel();
            bpmnProcessModel.getProperty().add(propertyModelMap.get(key).getModel());
        }

        for (String key : flowModel.keySet()) {
            flowModel.get(key).createBpmnSequenceFlowModel();
            bpmnProcessModel.getSequenceFlow().add(flowModel.get(key).getModel());
        }

        for (String key : startEventModel.keySet()) {
            startEventModel.get(key).createBpmnStartEventModel();
            bpmnProcessModel.getStartEvent().add(startEventModel.get(key).getModel());
        }

        for (String key : endEventModel.keySet()) {
            endEventModel.get(key).createBpmnEndEventModel();
            bpmnProcessModel.getEndEvent().add(endEventModel.get(key).getModel());
        }

        for (String key : gatewayModelMap.keySet()) {
            gatewayModelMap.get(key).createBpmnExclusiveGatewayModel();
            bpmnProcessModel.getExclusiveGateway().add(gatewayModelMap.get(key).getModel());
        }

        for (String key : parallelGatewayModelMap.keySet()) {
            parallelGatewayModelMap.get(key).createBpmnParallelGatewayModel();
            bpmnProcessModel.getParallelGateway().add(parallelGatewayModelMap.get(key).getModel());
        }
    }

    @Override
    public BpmnProcessModel getModel() {
        return bpmnProcessModel;
    }

    /* setter and Getter */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Boolean getAdHoc() {
        return adHoc;
    }

    public void setAdHoc(Boolean adHoc) {
        this.adHoc = adHoc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsExecutable() {
        return isExecutable;
    }

    public void setIsExecutable(Boolean isExecutable) {
        this.isExecutable = isExecutable;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

}
