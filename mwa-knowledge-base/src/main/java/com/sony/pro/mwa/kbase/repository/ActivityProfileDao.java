package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.ActivityProfileCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityProfileDao extends ModelBaseDao<ActivityProfileModel, ActivityProfileCollection> {
}
