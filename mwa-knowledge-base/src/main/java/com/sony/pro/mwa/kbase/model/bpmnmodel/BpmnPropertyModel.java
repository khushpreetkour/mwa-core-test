package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "itemSubjectRef" })
@XmlRootElement(name = "property", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnPropertyModel extends BpmnModelBase {

	private String itemSubjectRef;

	@XmlAttribute
	public String getItemSubjectRef() {
		return itemSubjectRef;
	}

	public void setItemSubjectRef(String itemSubjectRef) {
		this.itemSubjectRef = itemSubjectRef;
	}

}
