package com.sony.pro.mwa.kbase.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.drools.core.rule.constraint.ConditionAnalyzer.ThisInvocation;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.rc.MWARC;

public class KBaseUtil {
	private final static MwaLogger logger = MwaLogger.getLogger(KBaseUtil.class);
	
	public static final String START_TASK = "com.sony.pro.mwa.control.activity.StartTask";
	public static final String START_TASK_WITH_ASSET = "com.sony.pro.mwa.activity.nvx.workflow.editor.StartTaskWithAsset";
	public static final String START_TASK_WITH_BIN = "com.sony.pro.mwa.control.activity.StartTaskWithBin";
	public static final String START_TASK_WITH_ASSET_AND_BIN = "com.sony.pro.mwa.activity.nvx.workflow.editor.StartTaskWithAssetAndBin";
    public static final String END_TASK = "com.sony.pro.mwa.control.activity.EndTask";
    public static final String CONDITION_BRANCH_TASK = "com.sony.pro.mwa.control.activity.ConditionalBranchTask";
    public static final String PARALLEL_BRANCH_TASK = "com.sony.pro.mwa.control.activity.ParallelBranchTask";
    public static final String CONVERGING_BRANCH_TASK = "com.sony.pro.mwa.control.activity.ConditionalConvergingTask";
    public static final String PARALLEL_CONVERGING_BRANCH_TASK = "com.sony.pro.mwa.control.activity.ConvergingTask";
	
	public static enum StepType {
		START,
		END,
		ACTIVITY,
		CONDITION_BRANCH,
		PARALLEL_BRANCH,
		CONVERGING_BRANCH,
		PARALLEL_CONVERGING_BRANCH;
		
		public static StepType getStepType(String templateName) {
			switch (templateName) {
				case START_TASK:
				case START_TASK_WITH_ASSET:
				case START_TASK_WITH_BIN:
				case START_TASK_WITH_ASSET_AND_BIN:
					return START;
				case END_TASK:
					return END;
				case CONDITION_BRANCH_TASK:
					return CONDITION_BRANCH;
				case PARALLEL_BRANCH_TASK:
					return PARALLEL_BRANCH;
				case CONVERGING_BRANCH_TASK:
					return CONVERGING_BRANCH;
				case PARALLEL_CONVERGING_BRANCH_TASK:
					return PARALLEL_CONVERGING_BRANCH;
				default:
					return ACTIVITY;
			}
		}
	}
	
	public static String generateStepPortKey(String stepName, String key) {
		return stepName + "." + key;
	}
	
	/**
	 * JSONデータをWorkflowModelデシリアライズします。
	 * @param jsonWfModel
	 * @return
	 */
	public static WorkflowModel deserializeWfModel(String jsonWfModel) {// TODO

		ObjectMapper mapper = new ObjectMapper();
		WorkflowModel workflowModel = new WorkflowModel();
		try {
			workflowModel = mapper.readValue(jsonWfModel, WorkflowModel.class);
		} catch (JsonParseException e) {
			final String errMsg = "Failed to Deserializing.";
			logger.error(errMsg, e);
			throw new MwaError(MWARC.INVALID_INPUT, null, errMsg);
		} catch (JsonMappingException e) {
			final String errMsg = "Failed to Deserializing.";
			logger.error(errMsg, e);
			throw new MwaError(MWARC.INVALID_INPUT, null, errMsg);
		} catch (IOException e) {
			final String errMsg = "Failed to Deserializing.";
			logger.error(errMsg, e);
			throw new MwaError(MWARC.INVALID_INPUT, null, errMsg);
		}

		return workflowModel;// TODO この処理はUtilityに移動する。。
	}

	/**
	 * JSONデータをWorkflowModelをシリアライズします。
	 * @param wfModel
	 * @return
	 */
	public static String serializeWfModel(WorkflowModel wfModel) {// TODO

		ObjectMapper mapper = new ObjectMapper();
		String ret = "";
		try {
			ret = mapper.writeValueAsString(wfModel);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return ret;// TODO この処理はUtilityに移動する。。
	}
	
	/**
	 * workflowDiagramModelの妥当性を検証します。
	 * @param workflowDiagramModel　
	 * @return　エラーメッセージ
	 */
	public static String validWorkflowDiagramModel(WorkflowDiagramModel workflowDiagramModel) {
		
		try {
			// UUIDの妥当性チェック
			UUID.fromString(workflowDiagramModel.getId());
		} catch (IllegalArgumentException e) {
			final String errorMessage = "workflowId invalid input format.";
			logger.error(errorMessage);
			return errorMessage;
		}
		return "";
	}
}
