package com.sony.pro.mwa.kbase.repository.mem;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.common.utils.UUIDUtils;
import com.sony.pro.mwa.kbase.repository.ActivityTemplateDao;
import com.sony.pro.mwa.model.activity.ActivityTemplateCollection;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.QueryCriteria;

public class ActivityTemplateDaoImpl implements ActivityTemplateDao {
	
	Map<String, ActivityTemplateModel> map;
	
	public ActivityTemplateDaoImpl() {
		this.map = new HashMap<>();
	}

	@Override
	public ActivityTemplateCollection getModels(QueryCriteria criteria) {
		List<List<Filtering>> filterings = criteria.getFilterings();
		String name = pickupValFromFilter(filterings, "name");
		String version = pickupValFromFilter(filterings, "version");
		ActivityTemplateCollection result = new ActivityTemplateCollection();
		
		if (name != null && version != null) {
			List<ActivityTemplateModel> models = new ArrayList<>();
			models.add(this.map.get(UUIDUtils.generateUUID(name, version)));
			result.setModels(models);
		} else {
			result.setModels(new ArrayList<ActivityTemplateModel>(map.values()));
		}
		return result;
	}

	protected String pickupValFromFilter(List<List<Filtering>> filterings, String key) {
		for (List<Filtering> orFilterings : filterings) {
			for (Filtering filtering : orFilterings) {
				if ( key.equals(filtering.getKey())) {
					return filtering.getValue();
				}
			}
		}
		return null;
	}

	@Override
	public ActivityTemplateModel getModel(String id, Principal principal) {
		return this.map.get(id);
	}

	@Override
	public ActivityTemplateModel addModel(ActivityTemplateModel model) {
		this.map.put(model.getId(), model);
		return model;
	}

	@Override
	public ActivityTemplateModel updateModel(ActivityTemplateModel model) {
		this.map.put(model.getId(), model);
		return model;
	}

	@Override
	public ActivityTemplateModel deleteModel(ActivityTemplateModel model) {
		return this.map.remove(model.getId());
	}

	@Override
	public void deleteModelByPluginId(String pluginId) {
		throw new RuntimeException("ActivityTemplateDaoImpl: Unsupported Operation");
	}
}
