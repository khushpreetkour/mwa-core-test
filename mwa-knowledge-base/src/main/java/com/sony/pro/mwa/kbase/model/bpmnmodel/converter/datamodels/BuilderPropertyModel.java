package com.sony.pro.mwa.kbase.model.bpmnmodel.converter.datamodels;

import com.sony.pro.mwa.kbase.utils.KBaseUtil;

public class BuilderPropertyModel {
	private String key;
	private String name;
	private String type;
	private String value;
	private String sourceStepId;
	private String targetStepId;
	private KBaseUtil.StepType stepType;

	public BuilderPropertyModel(String key, String name, String type, String value, String sourceStepId, String targetStepId, KBaseUtil.StepType stepType) {
		this.key = key;
		this.name = name;
		this.type = type;
		this.setValue(value);
		this.sourceStepId = sourceStepId;
		this.targetStepId = targetStepId;
		this.stepType = stepType;
	}

	/**
	 * Keyを取得します。
	 *
	 * @return Key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Keyを設定します。
	 *
	 * @param Key
	 *            Key
	 */
	public void setKey(String Key) {
		this.key = Key;
	}

	/**
	 * typeを取得します。
	 *
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * typeを設定します。
	 *
	 * @param type
	 *            type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * valueを取得します。
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * valueを設定します。
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * nameを取得します。
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * nameを設定します。
	 *
	 * @return
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * sourceStepIdを取得します。
	 *
	 * @return
	 */
	public String getSourceStepId() {
		return sourceStepId;
	}

	/**
	 * sourceStepIdを設定します。
	 *
	 * @return
	 */
	public void setSourceStepId(String sourceStepId) {
		this.sourceStepId = sourceStepId;
	}
	/**
	 * targetStepIdを取得します。
	 *
	 * @return
	 */
	public String getTargetStepId() {
		return targetStepId;
	}

	/**
	 * targetStepIdを設定します。
	 *
	 * @return
	 */
	public void setTargetStepId(String targetStepId) {
		this.targetStepId = targetStepId;
	}

	/**
	 * stepTypeを取得します。
	 *
	 * @return
	 */
	public KBaseUtil.StepType getStepType() {
		return stepType;
	}

	/**
	 * stepTypeを設定します。
	 *
	 * @return
	 */
	public void setStepType(KBaseUtil.StepType stepType) {
		this.stepType = stepType;
	}
}
