package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "taskName", "displayName", "icon", "name", "incoming", "outgoing", "ioSpecification", "dataInputAssociation", "dataOutputAssociation" })
@XmlRootElement(name = "task", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnTaskModel extends BpmnModelBase {

	private String taskName;
	private String displayName;
	private String icon;
	private String name;
	private List<String> incoming = new ArrayList<String>();
	private List<String> outgoing = new ArrayList<String>();
	private List<BpmnIoSpecificationModel> ioSpecification = new ArrayList<BpmnIoSpecificationModel>();
	private List<BpmnDataInputAssociationModel> dataInputAssociation = new ArrayList<BpmnDataInputAssociationModel>();
	private List<BpmnDataOutputAssociationModel> dataOutputAssociation = new ArrayList<BpmnDataOutputAssociationModel>();

	@XmlAttribute(namespace = "http://www.jboss.org/drools")
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	@XmlAttribute(namespace = "http://www.jboss.org/drools")
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@XmlAttribute(namespace = "http://www.jboss.org/drools")
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "incoming", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<String> getIncoming() {
		return incoming;
	}

	public void setIncoming(List<String> incoming) {
		this.incoming = incoming;
	}

	@XmlElement(name = "outgoing", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<String> getOutgoing() {
		return outgoing;
	}

	public void setOutgoing(List<String> outgoing) {
		this.outgoing = outgoing;
	}

	@XmlElement(name = "ioSpecification", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnIoSpecificationModel> getIoSpecification() {
		return ioSpecification;
	}

	public void setIoSpecification(List<BpmnIoSpecificationModel> ioSpecification) {
		this.ioSpecification = ioSpecification;
	}

	@XmlElement(name = "dataInputAssociation", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnDataInputAssociationModel> getDataInputAssociation() {
		return dataInputAssociation;
	}

	public void setDataInputAssociation(List<BpmnDataInputAssociationModel> dataInputAssociation) {
		this.dataInputAssociation = dataInputAssociation;
	}

	@XmlElement(name = "dataOutputAssociation", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnDataOutputAssociationModel> getDataOutputAssociation() {
		return dataOutputAssociation;
	}

	public void setDataOutputAssociation(List<BpmnDataOutputAssociationModel> dataOutputAssociation) {
		this.dataOutputAssociation = dataOutputAssociation;
	}
}
