package com.sony.pro.mwa.kbase.model.bpmnmodel.generator;

import javax.xml.bind.JAXBException;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.kbase.KnowledgeBaseImpl;
import com.sony.pro.mwa.kbase.model.bpmnmodel.IBpmnModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.JaxbFormattedMarshallerFactory;
import com.sony.pro.mwa.rc.MWARC;

public class BpmnXmlGenerator {
	private final static MwaLogger logger = MwaLogger.getLogger(BpmnXmlGenerator.class);
	// TODO 最終的にはutil化する
	public String generate(IBpmnModel model) {
		String ret = "";

		try {
			ret = JaxbFormattedMarshallerFactory.createFormattedMarshaller(model);
		} catch (JAXBException e) {
			logger.debug(e);
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Failed to Generating xml.");
		}
		// JAXB.marshal(model, writer);
		return ret;
	}
}
