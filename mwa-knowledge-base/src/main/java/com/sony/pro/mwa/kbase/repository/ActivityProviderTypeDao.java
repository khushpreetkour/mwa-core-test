package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.provider.ActivityProviderTypeCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityProviderTypeDao extends ModelBaseDao<ActivityProviderTypeModel, ActivityProviderTypeCollection> {
}
