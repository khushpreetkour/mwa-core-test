package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BPMNShape")
public class BpmnBPMNShapeModel extends BpmnModelBase {

	private String bpmnElement;
	private BpmnBoundsModel Bounds;

	@XmlAttribute
	public String getBpmnElement() {
		return bpmnElement;
	}

	public void setBpmnElement(String bpmnElement) {
		this.bpmnElement = bpmnElement;
	}

	@XmlElement(name = "Bounds", namespace = "http://www.omg.org/spec/DD/20100524/DC")
	public BpmnBoundsModel getBounds() {
		return Bounds;
	}

	public void setBounds(BpmnBoundsModel Bounds) {
		this.Bounds = Bounds;
	}

}
