package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNEdgeModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNShapeModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBoundsModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnWaypointModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;

public class EdgeModel  extends InnerBpmnModelBuilderBase<BpmnBPMNEdgeModel> {

	private String bpmnElement;
	private String sourceElement;
	private String targetElement;
	private List<BpmnWaypointModel> waypointModels;
	private BpmnBPMNEdgeModel bpmnEdgeModel;

	public EdgeModel(String id) {
		super(BpmnDefinition.EDGE_ID + id);
		// TODO Auto-generated constructor stub
	}

	public void edgeInitialize(WorkflowModel workflowModel, Map<String, GatewayModel> gatewayModelMap, FlowModel flowModel, List <BpmnBPMNShapeModel> bpmnShapeModels, List <BpmnBPMNEdgeModel> bpmnEdgeModels) {
		createBpmnEdgeModel(workflowModel, flowModel, bpmnShapeModels, bpmnEdgeModels);
		setBpmnElement(flowModel.getId());
		setSourceElement(BpmnDefinition.SHAPE_ID + flowModel.getSourceRef());
		setTargetElement(BpmnDefinition.SHAPE_ID + flowModel.getTargetRef());
		setWaypointModels(waypointModels);
	}


	private void createBpmnEdgeModel(WorkflowModel workflowModel, FlowModel flowModel, List <BpmnBPMNShapeModel> bpmnShapeModels, List <BpmnBPMNEdgeModel> bpmnEdgeModels) {
//		String sourceStepName = flowModel.getSourceRef();
//		String destinationStepName = flowModel.getTargetRef();
//		ConnectionPortModel sourcePortModel = null;
//		ConnectionPortModel destinationPortModel = null;
//		for (ConnectionPortModel connectionPortModel: workflowModel.getConnectionPorts()) {
//			if (sourceStepName.equals(connectionPortModel.getStepName()) && BpmnDefinition.CONTROL_OUTPUT.equals(connectionPortModel.getType())) {
//				String targetPortId = connectionPortModel.getId();
//				sourcePortModel = connectionPortModel;
//				for (ConnectionModel connectionModel: workflowModel.getConnections()) {
//					if (targetPortId.equals(connectionModel.getInputConnectionPortId())) {
//						String targetDestinationPort = connectionModel.getOutputConnectionPortId();
//						for (ConnectionPortModel connectionPortModel2: workflowModel.getConnectionPorts()) {
//							if (targetDestinationPort.equals(connectionPortModel2.getId()) && BpmnDefinition.CONTROL_INPUT.equals(connectionPortModel2.getType())) {
//								if (destinationStepName.equals(connectionPortModel2.getStepName())) {
//									destinationPortModel = connectionPortModel2;
//									break;
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//
//		GatewayModel sourceGatewayModel = null;
//		for (Map.Entry<String, GatewayModel> gatewayModel: gatewayModelMap.entrySet()) {
//			if (sourceStepName.equals(gatewayModel.getValue().getName())) {
//				sourceGatewayModel = gatewayModel.getValue();
//			}
//		}
//
//		// 位置の計算を行う
//		waypointModels = new ArrayList();
//		Double xPosition = null;
//		Double yPosition = null;
//		BpmnWaypointModel waypoint = new BpmnWaypointModel();
//		waypoint.setType("dc:Point");
//		if (sourceGatewayModel != null) {
//			if (BpmnDefinition.DIRECTON_DIVERGING.equals(sourceGatewayModel.getGatewayDirection())) {
//
//			} else {
//
//			}
//		} else {
//
//		}

		BpmnBoundsModel outgoingBoundParam = null;
		BpmnBoundsModel incomingBoundParam = null;
		for(BpmnBPMNShapeModel shapeModel: bpmnShapeModels) {
			// flowModelのSourceをshapeIdの形式にし、shapeModelのIdとマッチしたら出力する側の座標を確保
			// Branch が2回ヒットするため、nullチェックを行う
			if(shapeModel.getId().equals(BpmnDefinition.SHAPE_ID + flowModel.getSourceRef()) && outgoingBoundParam == null) {
				 outgoingBoundParam = shapeModel.getBounds();
			}
			// flowModelのtargetIdとshapeModelのIdがマッチしたら入力される側の座標を確保
			// TODO: BPMNShape_ Hard Coding
			if(shapeModel.getId().equals(BpmnDefinition.SHAPE_ID + flowModel.getTargetRef()) && incomingBoundParam == null) {
				incomingBoundParam = shapeModel.getBounds();
			}

			// 入出力の座標が確保できたら
			if (outgoingBoundParam != null && incomingBoundParam != null) {
				for(StepModel step: workflowModel.getSteps()) {
					// 入力側のstepと一致するステップを準備
					if(flowModel.getSourceRef().equals(step.getName())) {
						setEdgeModelParameter(flowModel, outgoingBoundParam, incomingBoundParam, step, bpmnEdgeModels);
					}
				}
				break;
			}
		}
	}

	private void setEdgeModelParameter(FlowModel flowModel, BpmnBoundsModel outgoingBoundParam,
		BpmnBoundsModel incomingBoundParam, StepModel step, List<BpmnBPMNEdgeModel> bpmnEdgeModels) {

		waypointModels = new ArrayList<>();
		// Stepのtypeを取得する
		KBaseUtil.StepType stepType = KBaseUtil.StepType.getStepType(step.getActivity().get("name").toString());
		String changedEventName = step.getId();
		// Step間の線を開始、中間、終着の3点で座標設定を行う
		for(int coordinateChangePoint=BpmnDefinition.EDGE_START_POINT; coordinateChangePoint <= BpmnDefinition.EDGE_END_POINT; coordinateChangePoint++) {
			Double xPosition = null;
			Double yPosition = null;
			BpmnWaypointModel waypoint = new BpmnWaypointModel();
			waypoint.setType("dc:Point");
			if (step.getName().equals(changedEventName)) {
				switch(stepType) {
				case CONDITION_BRANCH:
				case PARALLEL_BRANCH:
					if(coordinateChangePoint == BpmnDefinition.EDGE_START_POINT) {
						if (checkRegisteredEdge(bpmnEdgeModels, BpmnDefinition.SHAPE_ID + flowModel.getSourceRef())) {
						  xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth()) / 2;
						  yPosition = Double.parseDouble(outgoingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight());
						} else {
						  xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth());
						  yPosition = Double.parseDouble(outgoingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
						}
					} else if(coordinateChangePoint == BpmnDefinition.EDGE_MIDDLE_POINT) {
						if (checkRegisteredEdge(bpmnEdgeModels, BpmnDefinition.SHAPE_ID + flowModel.getSourceRef())) {
						  xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth()) / 2;
						  yPosition = Double.parseDouble(incomingBoundParam.getY()) + Double.parseDouble(incomingBoundParam.getHeight()) /2;
						} else {
						  xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth());
						  yPosition = Double.parseDouble(incomingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
						}
					} else if(coordinateChangePoint == BpmnDefinition.EDGE_END_POINT){
						  yPosition = Double.parseDouble(incomingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
						  xPosition = Double.parseDouble(incomingBoundParam.getX());
					}
					break;
				default:
					if(coordinateChangePoint == BpmnDefinition.EDGE_START_POINT) {
					  xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth());
					  yPosition = Double.parseDouble(outgoingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
					} else if(coordinateChangePoint == BpmnDefinition.EDGE_MIDDLE_POINT) {
					  xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth());
					  yPosition = Double.parseDouble(incomingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
					} else if(coordinateChangePoint == BpmnDefinition.EDGE_END_POINT) {
					  xPosition = Double.parseDouble(incomingBoundParam.getX());
			          yPosition = Double.parseDouble(incomingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
					}
					break;
				}

			} else {
				if(coordinateChangePoint == BpmnDefinition.EDGE_START_POINT) {
					xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth());
					yPosition = Double.parseDouble(outgoingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
				} else if(coordinateChangePoint == BpmnDefinition.EDGE_MIDDLE_POINT) {
					xPosition = Double.parseDouble(outgoingBoundParam.getX()) + Double.parseDouble(outgoingBoundParam.getWidth());
					yPosition = Double.parseDouble(incomingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
				} else if(coordinateChangePoint == BpmnDefinition.EDGE_END_POINT) {
					xPosition = Double.parseDouble(incomingBoundParam.getX());
					yPosition = Double.parseDouble(incomingBoundParam.getY()) + Double.parseDouble(outgoingBoundParam.getHeight()) / 2;
				}
			}
			waypoint.setX(xPosition.toString());
			waypoint.setY(yPosition.toString());
			waypointModels.add(waypoint);
		}
	}

	// GateWay の場合2方向に出力されるので、2回同じ方向に進む座標を作成しないために登録済みCheckが必要。
	private Boolean checkRegisteredEdge(List <BpmnBPMNEdgeModel> bpmnEdges, String SourceElement) {
		Boolean result = false;
		for(BpmnBPMNEdgeModel edgeModel: bpmnEdges) {
			if(edgeModel.getSourceElement().equals(SourceElement)) {
				result = true;
				break;
			}
		}
		return result;
	}

	public void createBpmnBPMNEdgeModel() {
		bpmnEdgeModel = new BpmnBPMNEdgeModel();
		bpmnEdgeModel.setId(id);
		bpmnEdgeModel.setBpmnElement(bpmnElement);
		bpmnEdgeModel.setSourceElement(sourceElement);
		bpmnEdgeModel.setTargetElement(targetElement);
		bpmnEdgeModel.setWaypoint(waypointModels);
	}

	@Override
	public BpmnBPMNEdgeModel getModel() {
		// TODO Auto-generated method stub
		return bpmnEdgeModel;
	}

	public String getBpmnElement() {
		return bpmnElement;
	}

	public void setBpmnElement(String bpmnElement) {
		this.bpmnElement = bpmnElement;
	}

	public String getSourceElement() {
		return sourceElement;
	}

	public void setSourceElement(String sourceElement) {
		this.sourceElement = sourceElement;
	}

	public String getTargetElement() {
		return targetElement;
	}

	public void setTargetElement(String targetElement) {
		this.targetElement = targetElement;
	}

	public List<BpmnWaypointModel> getWaypointModels() {
		return waypointModels;
	}

	public void setWaypointModels(List<BpmnWaypointModel> waypointModels) {
		this.waypointModels = waypointModels;
	}

}
