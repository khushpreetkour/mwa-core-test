package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.ExtensionCollection;
import com.sony.pro.mwa.model.kbase.ExtensionModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ExtensionDao extends ModelBaseDao<ExtensionModel, ExtensionCollection> {
	public void deleteAllModel(String type);
}
