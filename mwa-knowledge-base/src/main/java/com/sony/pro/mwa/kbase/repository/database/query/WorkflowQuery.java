package com.sony.pro.mwa.kbase.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModelCollection;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.ColumnUtils;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class WorkflowQuery extends Query<WorkflowDaoModel, WorkflowDaoModelCollection, QueryCriteria> {
    public enum COLUMN {
    	ID("w.workflow_id", "id", UUID.class, true),
        VERSION("w.workflow_version", "version", String.class, true),
    	NAME("w.workflow_name", "name", String.class, true),
    	DIAGRAM_ID("w.workflow_diagram_id", "diagramId", String.class, true),
        MODEL("w.workflow_model", "model", String.class, true),
        DELETE_FLAG("w.workflow_delete_flag", "deleteFlag", Boolean.class, true),
        TEMPLATE_ID("w.extension_id", "templateId", UUID.class, false),
        ALIAS("w.workflow_alias", "alias", String.class, true),
        PREDEFINE_FLAG("w.workflow_predefine_flag", "predefineFlag", Boolean.class, true),
        CREATE_TIME("w.workflow_create_time", "createTime", Timestamp.class, true),
        UPDATE_TIME("w.workflow_update_time", "updateTime", Timestamp.class, true),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

//    private static final String WITH_RECURSIVE_CLAUSE = "";

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
    		"FROM mwa.workflow AS w "
    		+ "LEFT OUTER JOIN "
    		+ "( SELECT e.extension_id,ec.extension_content_id FROM mwa.extension AS e INNER JOIN mwa.extension_content AS ec ON e.extension_id = ec.extension_id ) AS x "
    		+ "ON w.extension_id = x.extension_id " +
    			"WHERE " +
    			"TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        for (COLUMN enumColumn : COLUMN.values()) {
            map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }

    public WorkflowQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }

    @Override
    protected RowMapper<WorkflowDaoModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<WorkflowDaoModel> {
        public WorkflowDaoModel mapRow(ResultSet rs, int rowNum) throws SQLException {

        	WorkflowDaoModel workflowModel = new WorkflowDaoModel();
        	workflowModel.setId(rs.getString(COLUMN.ID.alias));
        	workflowModel.setName(rs.getString(COLUMN.NAME.alias));
        	workflowModel.setVersion(rs.getString(COLUMN.VERSION.alias));
        	workflowModel.setDiagramId(rs.getString(COLUMN.DIAGRAM_ID.alias));
        	workflowModel.setModel(rs.getString(COLUMN.MODEL.alias));
        	workflowModel.setDeleteFlag(rs.getBoolean(COLUMN.DELETE_FLAG.alias));
        	workflowModel.setTemplateId(rs.getString(COLUMN.TEMPLATE_ID.alias));
        	workflowModel.setExtensionId(rs.getString(COLUMN.TEMPLATE_ID.alias));
        	workflowModel.setAlias(rs.getString(COLUMN.ALIAS.alias));
        	workflowModel.setPredefineFlag(rs.getBoolean(COLUMN.PREDEFINE_FLAG.alias));
        	workflowModel.setCreateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.CREATE_TIME.alias));
        	workflowModel.setUpdateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.UPDATE_TIME.alias));
        	return workflowModel;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }
}
