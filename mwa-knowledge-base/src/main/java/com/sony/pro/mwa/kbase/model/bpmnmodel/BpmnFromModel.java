package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlType(propOrder={"type","id", "value"})
@XmlRootElement(name = "from" , namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnFromModel extends BpmnModelBase {

	private String type;
	private String value;

	@XmlAttribute(namespace = "http://www.w3.org/2001/XMLSchema-instance")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlValue
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
