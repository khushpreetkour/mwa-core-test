package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnEndEventModel;

public class EndEventModel extends InnerBpmnModelBuilderBase<BpmnEndEventModel> {
	private String name;
	private List<String> incoming = new ArrayList<>();
	private BpmnEndEventModel endEventModel;
	
	/**
	 * コンストラクタ
	 * @return
	 */
	public EndEventModel(String id) {
		super(id);
	}
	
	public void createBpmnEndEventModel() {
		endEventModel = new BpmnEndEventModel();
		endEventModel.setId(id);
		endEventModel.setName(name);
		endEventModel.setIncoming(incoming);
	}
	
	@Override
	public BpmnEndEventModel getModel() {
		return endEventModel;
	}

	/* setter and getter */
	public List<String> getIncoming() {
		return incoming;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
