package com.sony.pro.mwa.kbase.builder.bpmnmodel;

import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnModelBase;

/**
 * BpmnBuilderの規定クラスです。
 * @param <T> 出力するBpmnModelの種別
 */
public abstract class InnerBpmnModelBuilderBase<T extends BpmnModelBase> implements IBpmnModelBuilder<T> {
	protected String id = "";

	/**
	 * コンストラクタ
	 * @param id
	 */
	public InnerBpmnModelBuilderBase(String id) {
		this.id = id;
	}

	/**
	 * IDを取得します。
	 * @return
	 */
	public String getId() {
		return id;
	}
}
