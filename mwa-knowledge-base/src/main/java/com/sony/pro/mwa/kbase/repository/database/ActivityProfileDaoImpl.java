package com.sony.pro.mwa.kbase.repository.database;

import java.security.Principal;
import java.sql.*;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.internal.utils.InternalConverter;
import com.sony.pro.mwa.kbase.repository.ActivityProfileDao;
import com.sony.pro.mwa.kbase.repository.database.query.ActivityProfileQuery;
import com.sony.pro.mwa.model.kbase.ActivityProfileCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileModel;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ActivityProfileDaoImpl extends DatabaseBaseDao<ActivityProfileModel, ActivityProfileCollection, QueryCriteria> implements ActivityProfileDao{

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ActivityProfileCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ActivityProfileModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.activity_profile (" +
					"activity_profile_id" +
					", activity_profile_group_id" +
					", activity_template_id" +
					", activity_profile_name" +
					", activity_profile_icon" +
					", activity_profile_optional_field_editable_flag" +
					", activity_profile_parameters" +
					", activity_profile_priority" +
					", activity_profile_schedulable_flag" +
					", activity_profile_create_time" +
					", activity_profile_update_time" +
					") " +
			"VALUES (" +
					"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?" +
					") ";


	public ActivityProfileModel addModel(final ActivityProfileModel model) {
		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
			new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
					int colNum = 1;

					ps.setObject(colNum++, UUID.fromString(model.getId()));
					if (StringUtils.isNotBlank(model.getGroupId())) {
						ps.setObject(colNum++, UUID.fromString(model.getGroupId()));
					} else {
						ps.setObject(colNum++, null);
					}
					if (StringUtils.isNotBlank(model.getTemplateId())) {
						ps.setObject(colNum++, UUID.fromString(model.getTemplateId()));
					} else {
						ps.setObject(colNum++, null);
					}
					ps.setString(colNum++, model.getName());
					ps.setString(colNum++, model.getIcon());
					ps.setBoolean(colNum++, model.getOptionalFieldEditableFlag());

					if (model.getParameter() != null) {
						ObjectMapper mapper = new ObjectMapper();
						String parameterJson = "";
						try {
							parameterJson = mapper.writeValueAsString(model.getParameter());
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
						ps.setString(colNum++, parameterJson);
					} else {
						ps.setString(colNum++, null);
					}

					if (model.getPriority() != null) {
						ps.setInt(colNum++, model.getPriority());
					} else {
						ps.setNull(colNum++, Types.INTEGER);
					}
					ps.setBoolean(colNum++, model.getSchedulableFlag());
					model.setCreateTime(System.currentTimeMillis());
					model.setUpdateTime(model.getCreateTime());
					ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
					ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));

					logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
					return ps;
				}
			},
		keyHolder);

		ActivityProfileModel addedModel = model;
		return insertedRowCount != 0 ? addedModel : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.activity_profile " +
			"SET " +
					"activity_profile_group_id = ?, " +
					"activity_template_id = ?, " +
					"activity_profile_name = ?, " +
					"activity_profile_icon = ?, " +
					"activity_profile_optional_field_editable_flag = ?, " +
					"activity_profile_parameters = ?, " +
					"activity_profile_priority = ?, " +
					"activity_profile_schedulable_flag = ?, " +
					"activity_profile_update_time = ? " +
			"WHERE " +
					"activity_profile_id = ? ";

	public ActivityProfileModel updateModel(final ActivityProfileModel model) {
		final String updateSql = UPDATE_SQL;
		int updatedRowCount = jdbcTemplate.update(
			new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement ps = conn.prepareStatement(updateSql);
					int colNum = 1;

					if (StringUtils.isNotBlank(model.getGroupId())) {
						ps.setObject(colNum++, UUID.fromString(model.getGroupId()));
					} else {
						ps.setObject(colNum++, null);
					}
					if (StringUtils.isNotBlank(model.getTemplateId())) {
						ps.setObject(colNum++, UUID.fromString(model.getTemplateId()));
					} else {
						ps.setObject(colNum++, null);
					}

					ps.setString(colNum++, model.getName());
					ps.setString(colNum++, model.getIcon());
					ps.setBoolean(colNum++, model.getOptionalFieldEditableFlag());
					if (model.getParameter() != null) {
						ObjectMapper mapper = new ObjectMapper();
						String parameterJson = "";
						try {
							parameterJson = mapper.writeValueAsString(model.getParameter());
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
						ps.setString(colNum++, parameterJson);
					} else {
						ps.setString(colNum++, null);
					}
					if (model.getPriority() != null) {
						ps.setInt(colNum++, model.getPriority());
					} else {
						ps.setNull(colNum++, Types.INTEGER);
					}
					ps.setBoolean(colNum++, model.getSchedulableFlag());
					model.setUpdateTime(System.currentTimeMillis());
					ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));

					ps.setObject(colNum++, UUID.fromString(model.getId()));

					return ps;
				}
			}
		);

		return updatedRowCount != 0 ? model : null;
	}

	private static final String DELETE_ALL_SQL =
			"DELETE FROM " +
					"mwa.activity_profile ";
	public void deleteAllModel() {
        if (jdbcTemplate.update(DELETE_ALL_SQL) == 1) {
        	//do nothing
        }
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.activity_profile " +
			"WHERE " +
					"activity_profile_id = ? ";
	public ActivityProfileModel deleteModel(ActivityProfileModel model) {
		Object[] paramArray = new Object[]{
				UUID.fromString(model.getId().toString())
				};
		if (jdbcTemplate.update(DELETE_SQL, paramArray) > 0) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<ActivityProfileModel, ActivityProfileCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ActivityProfileQuery(jdbcTemplate);
	}

}
