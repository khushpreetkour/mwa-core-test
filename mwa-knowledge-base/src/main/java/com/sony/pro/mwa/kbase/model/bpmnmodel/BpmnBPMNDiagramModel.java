package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "BPMNDiagram")
public class BpmnBPMNDiagramModel extends BpmnModelBase {

	private String name;
	private BpmnBPMNPlaneModel BPMNPlane;

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "BPMNPlane", namespace = "http://www.omg.org/spec/BPMN/20100524/DI")
	public BpmnBPMNPlaneModel getBPMNPlane() {
		return BPMNPlane;
	}

	public void setBPMNPlane(BpmnBPMNPlaneModel BPMNPlane) {
		this.BPMNPlane = BPMNPlane;
	}
}
