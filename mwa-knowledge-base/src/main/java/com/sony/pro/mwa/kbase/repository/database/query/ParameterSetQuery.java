package com.sony.pro.mwa.kbase.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.internal.utils.InternalConverter;
import com.sony.pro.mwa.model.kbase.ParameterSetCollection;
import com.sony.pro.mwa.model.kbase.ParameterSetModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class ParameterSetQuery extends Query<ParameterSetModel, ParameterSetCollection, QueryCriteria> {
	public enum COLUMN {
		ID("i.id", "id", UUID.class, true),
		NAME("i.name", "name", String.class, false),
		PARAMETER_SET("i.parameters", "parameters", SQLXML.class, true);

		private final String name;
		private final String alias;
		private final Class<?> type;
		private final boolean sortable;

		private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
			this.name = name;
			this.alias = alias;
			this.type = type;
			this.sortable = sortable;
		}
	}

	private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
	static {
		String selectClause = "SELECT ";
		String separator = ", ";
		for (COLUMN column : COLUMN.values()) {
			selectClause += column.name + " AS " + column.alias + separator;
		}

		SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
	}

	private static final String WITH_RECURSIVE_CLAUSE = "";

	private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
	static {
		String selectClause = "SELECT ";
		String separator = ", ";
		for (COLUMN column : COLUMN.values()) {
			selectClause += column.name + " AS " + column.alias + separator;
		}

		SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
	}

	private static final String COUNT_ALIAS = "count";

	private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

	private static final String FROM_WHERE_CLAUSE_FOR_MYSQL = "FROM " + "mwa.parameter_set AS i " + "WHERE " + "TRUE ";
	private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

	private static final Map<DatabaseEnum, String> selectListQueryMap;
	static {
		Map<DatabaseEnum, String> map = new HashMap<>();
		map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
		map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		selectListQueryMap = Collections.unmodifiableMap(map);
	}

	private static final Map<DatabaseEnum, String> selectCountQueryMap;
	static {
		Map<DatabaseEnum, String> map = new HashMap<>();
		map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
		map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		selectCountQueryMap = Collections.unmodifiableMap(map);
	}

	private static final Map<String, Column> columnMap;
	static {
		Map<String, Column> map = new HashMap<>();
		for (COLUMN enumColumn : COLUMN.values()) {
			map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
		}
		columnMap = Collections.unmodifiableMap(map);
	}

	public ParameterSetQuery(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
	}

	@Override
	protected RowMapper<ParameterSetModel> createModelMapper() {
		return new ModelMapper();
	}

	@Override
	protected RowMapper<Long> createCountMapper() {
		return new CountMapper();
	}

	// @Override
	protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
		if (database == DatabaseEnum.POSTGRESQL) {
			/* String queryString = WITH_RECURSIVE_CLAUSE; List<Object>
			 * queryParamList = new ArrayList<>();
			 * queryParamList.add(criteria.getPrincipal() != null ?
			 * criteria.getPrincipal().getName() : null); return new
			 * QuerySql(queryString, queryParamList); */
		}
		return null;
	}

	private static final class ModelMapper implements RowMapper<ParameterSetModel> {
		public ParameterSetModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			ParameterSetModel model = new ParameterSetModel();
			model.setId(rs.getString(COLUMN.ID.alias));
			model.setName(rs.getString(COLUMN.NAME.alias));
			// odel.setParameters(rs.getString(COLUMN.PARAMETER_SET.alias));

			// String bundleId = rs.getString(COLUMN.BUNDLE_ID.alias);
			// if (model.getType().equals(ExtensionType.BUNDLE.name()))
			// ((MwaPluginModel)model).setBundleId(Long.parseLong(bundleId));
			String parametersStr = rs.getString(COLUMN.PARAMETER_SET.alias);
			try {
				if (parametersStr != null) {
					Map<String, Object> result = InternalConverter.xmlToMap(parametersStr);
					model.setParameters(result);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			return model;
		}
	}

	private static final class CountMapper implements RowMapper<Long> {
		public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getLong(COUNT_ALIAS);
		}
	}
}
