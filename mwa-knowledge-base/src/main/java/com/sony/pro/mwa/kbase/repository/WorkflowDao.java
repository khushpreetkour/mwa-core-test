package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.WorkflowDaoModel;
import com.sony.pro.mwa.model.kbase.WorkflowDaoModelCollection;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface WorkflowDao extends ModelBaseDao<WorkflowDaoModel, WorkflowDaoModelCollection> {
}
