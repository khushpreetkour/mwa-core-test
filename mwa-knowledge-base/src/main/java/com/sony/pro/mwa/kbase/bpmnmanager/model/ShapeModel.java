package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.Map;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNShapeModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBoundsModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;

public class ShapeModel extends InnerBpmnModelBuilderBase<BpmnBPMNShapeModel> {

	private BpmnBoundsModel bound;
	private String bpmnElement;
	private BpmnBPMNShapeModel bpmnShapeModel;

	public ShapeModel(String id) {
		super(BpmnDefinition.SHAPE_ID + id);
	}

	public void shapeInitialize(StepModel stepModel, Map<String, FlowModel> flowModelMap) {
		setBpmnElement(stepModel.getName());
		BpmnBoundsModel boundsModel = new BpmnBoundsModel();

		// StepModel の name を基に、StepType を取得する。
		KBaseUtil.StepType stepType = KBaseUtil.StepType.getStepType(stepModel.getActivity().get("name").toString());
		String changedEventName = stepModel.getId();

		// サイズを設定
		String height;
		String width;
		if (stepModel.getName().equals(changedEventName)) {
			switch(stepType) {
			case START:
			case END:
			case CONDITION_BRANCH:
			case CONVERGING_BRANCH:
			case PARALLEL_BRANCH:
				height = "50.0";
				width = "50.0";
				break;
			default:
				height = "50.0";
				width = "110.0";
				break;
			}
		} else {
			height = "50.0";
			width = "110.0";
		}

		boundsModel.setHeight(height);
		boundsModel.setWidth(width);

		// 座標設定
		boundsModel.setX(stepModel.getPosition().getX().toString());
		boundsModel.setY(stepModel.getPosition().getY().toString());
		setBound(boundsModel);
	}

	public void createBpmnBPMNShapeModel() {
		bpmnShapeModel = new BpmnBPMNShapeModel();
		bpmnShapeModel.setId(id);
		bpmnShapeModel.setBpmnElement(bpmnElement);
		bpmnShapeModel.setBounds(bound);
	}

	@Override
	public BpmnBPMNShapeModel getModel() {
		// TODO Auto-generated method stub
		return bpmnShapeModel;
	}

	/* setter and getter */
	public String getBpmnElement() {
		return bpmnElement;
	}

	public void setBpmnElement(String bpmnElement) {
		this.bpmnElement = bpmnElement;
	}

	public BpmnBPMNShapeModel getBpmnShapeModel() {
		return bpmnShapeModel;
	}

	public void setBpmnShapeModel(BpmnBPMNShapeModel bpmnShapeModel) {
		this.bpmnShapeModel = bpmnShapeModel;
	}

	public BpmnBoundsModel getBound() {
		return bound;
	}

	public void setBound(BpmnBoundsModel bound) {
		this.bound = bound;
	}

}
