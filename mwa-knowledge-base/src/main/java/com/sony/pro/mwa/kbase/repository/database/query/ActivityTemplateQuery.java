package com.sony.pro.mwa.kbase.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.model.activity.ActivityTemplateCollection;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class ActivityTemplateQuery extends Query<ActivityTemplateModel, ActivityTemplateCollection, QueryCriteria> {
    public enum COLUMN {
        ID("i.activity_template_id", "id", UUID.class, true),
        NAME("i.activity_template_name", "name", String.class, true),
        VERSION("i.activity_template_version", "version", String.class, true),
        TEMPLATE_TYPE("j.activity_template_type_name", "type", String.class, true),
        EXTENSION_ID("k.extension_id", "extensionId", UUID.class, true),
        EXTENSION_NAME("l.extension_name", "extensionName", String.class, true),
        EXTENSION_VERSION("l.extension_version", "extensionVersion", String.class, true),
        BUNDLE_ID("m.extension_property_value", "bundleId", String.class, true),
        DELETE_FLAG("i.activity_template_delete_flag", "deleteFlag", Boolean.class, true),
        ALIAS("i.activity_template_alias", "alias", String.class, true),
        ICON("i.activity_template_icon", "icon", String.class, true),
        DESCRIPTION("i.activity_template_description", "description", String.class, true),
        SYNC_FLAG("i.activity_template_sync_flag", "syncFlag", Boolean.class, true)
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String WITH_RECURSIVE_CLAUSE = "";

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " +
            "mwa.activity_template AS i "+
            "LEFT JOIN mwa.activity_template_type AS j ON i.activity_template_type_id=j.activity_template_type_id " +
            "LEFT JOIN mwa.extension_content AS k ON i.activity_template_id=k.extension_content_id " +
            "LEFT JOIN mwa.extension AS l ON k.extension_id=l.extension_id " +
            "LEFT JOIN mwa.extension_property AS m ON m.extension_id=l.extension_id and m.extension_property_name = 'BUNDLE_ID' " +
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        for (COLUMN enumColumn : COLUMN.values()) {
            map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }

    public ActivityTemplateQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }

    @Override
    protected RowMapper<ActivityTemplateModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<ActivityTemplateModel> {
        public ActivityTemplateModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        	String bundleIdStr = rs.getString(COLUMN.BUNDLE_ID.alias);
        	Long bundleId = null;
        	if (bundleIdStr != null) {
        		try {
        			bundleId = Long.valueOf(bundleIdStr);
        		} catch (NumberFormatException e) {
        		}
        	}

        	ActivityTemplateModel template =
        			new ActivityTemplateModel()
        				.setId(rs.getString(COLUMN.ID.alias))
        				.setName(rs.getString(COLUMN.NAME.alias))
        				.setVersion(rs.getString(COLUMN.VERSION.alias))
        				.setTypeByString(rs.getString(COLUMN.TEMPLATE_TYPE.alias))
        				.setExtensionId(rs.getString(COLUMN.EXTENSION_ID.alias))
        				.setBundleId(bundleId)
        				.setExtensionName(rs.getString(COLUMN.EXTENSION_NAME.alias))
        				.setDeleteFlag(rs.getBoolean(COLUMN.DELETE_FLAG.alias))
        				.setAlias(rs.getString(COLUMN.ALIAS.alias))
        				.setIcon(rs.getString(COLUMN.ICON.alias))
        				.setDescription(rs.getString(COLUMN.DESCRIPTION.alias))
        				.setSyncFlag(rs.getBoolean(COLUMN.SYNC_FLAG.alias))
        			;
        	return template;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }
}
