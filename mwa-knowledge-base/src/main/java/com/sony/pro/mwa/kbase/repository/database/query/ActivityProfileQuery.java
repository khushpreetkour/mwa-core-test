package com.sony.pro.mwa.kbase.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.model.kbase.ActivityProfileCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.ColumnUtils;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class ActivityProfileQuery extends Query<ActivityProfileModel, ActivityProfileCollection, QueryCriteria> {
	public enum COLUMN {
		ID("i.activity_profile_id", "id", UUID.class, true),
		GROUP_ID("i.activity_profile_group_id", "groupId", UUID.class, true),
		TEMPLATE_ID("i.activity_template_id", "templateId", UUID.class, true),
		NAME("i.activity_profile_name", "name", String.class, true),
		ICON("i.activity_profile_icon", "icon", String.class, true),
		OPTIONAL_FIELD_EDITABLE_FLAG("i.activity_profile_optional_field_editable_flag", "optionalFieldEditableFlag", boolean.class, true),
		PARAMETERS("i.activity_profile_parameters", "parameters", SQLXML.class, true),
		PRIORITY("i.activity_profile_priority", "priority", Integer.class, true),
		SCHEDULABLE_FLAG("i.activity_profile_schedulable_flag", "schedulableFlag", boolean.class, true),
        CREATE_TIME("i.activity_profile_create_time", "createTime", Timestamp.class, true),
        UPDATE_TIME("i.activity_profile_update_time", "updateTime", Timestamp.class, true);

		private final String name;
		private final String alias;
		private final Class<?> type;
		private final boolean sortable;

		private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
			this.name = name;
			this.alias = alias;
			this.type = type;
			this.sortable = sortable;
		}
	}

	private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
	static {
		String selectClause = "SELECT ";
		String separator = ", ";
		for (COLUMN column : COLUMN.values()) {
			selectClause += column.name + " AS " + column.alias + separator;
		}

		SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
	}

	private static final String WITH_RECURSIVE_CLAUSE = "";

	private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
	static {
		String selectClause = "SELECT ";
		String separator = ", ";
		for (COLUMN column : COLUMN.values()) {
			selectClause += column.name + " AS " + column.alias + separator;
		}

		SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
	}

	private static final String COUNT_ALIAS = "count";

	private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

	private static final String FROM_WHERE_CLAUSE_FOR_MYSQL = "FROM " + "mwa.activity_profile AS i " + "WHERE " + "TRUE ";
	private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

	private static final Map<DatabaseEnum, String> selectListQueryMap;
	static {
		Map<DatabaseEnum, String> map = new HashMap<>();
		map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
		map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		selectListQueryMap = Collections.unmodifiableMap(map);
	}

	private static final Map<DatabaseEnum, String> selectCountQueryMap;
	static {
		Map<DatabaseEnum, String> map = new HashMap<>();
		map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
		map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
		selectCountQueryMap = Collections.unmodifiableMap(map);
	}

	private static final Map<String, Column> columnMap;
	static {
		Map<String, Column> map = new HashMap<>();
		for (COLUMN enumColumn : COLUMN.values()) {
			map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
		}
		columnMap = Collections.unmodifiableMap(map);
	}

	public ActivityProfileQuery(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
	}

	@Override
	protected RowMapper<ActivityProfileModel> createModelMapper() {
		return new ModelMapper();
	}

	@Override
	protected RowMapper<Long> createCountMapper() {
		return new CountMapper();
	}

	// @Override
	protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
		if (database == DatabaseEnum.POSTGRESQL) {
			/* String queryString = WITH_RECURSIVE_CLAUSE; List<Object>
			 * queryParamList = new ArrayList<>();
			 * queryParamList.add(criteria.getPrincipal() != null ?
			 * criteria.getPrincipal().getName() : null); return new
			 * QuerySql(queryString, queryParamList); */
		}
		return null;
	}

	private static final class ModelMapper implements RowMapper<ActivityProfileModel> {
		public ActivityProfileModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			ActivityProfileModel model = new ActivityProfileModel();
			model.setId(rs.getString(COLUMN.ID.alias));
			model.setGroupId(rs.getString(COLUMN.GROUP_ID.alias));
			model.setTemplateId(rs.getString(COLUMN.TEMPLATE_ID.alias));
			model.setName(rs.getString(COLUMN.NAME.alias));
			model.setIcon(rs.getString(COLUMN.ICON.alias));
			model.setOptionalFieldEditableFlag(rs.getBoolean(COLUMN.OPTIONAL_FIELD_EDITABLE_FLAG.alias));
			String parametersStr = rs.getString(COLUMN.PARAMETERS.alias);
			try {
				if (parametersStr != null) {
					ObjectMapper mapper = new ObjectMapper();
					Map<String, Object> result = mapper.readValue(parametersStr, HashMap.class);
					model.setParameter(result);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			model.setPriority(ColumnUtils.parseInt(rs, columnMap, COLUMN.PRIORITY.alias));
			model.setSchedulableFlag(rs.getBoolean(COLUMN.SCHEDULABLE_FLAG.alias));
        	model.setCreateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.CREATE_TIME.alias));
        	model.setUpdateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.UPDATE_TIME.alias));

			return model;
		}
	}

	private static final class CountMapper implements RowMapper<Long> {
		public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getLong(COUNT_ALIAS);
		}
	}
}
