package com.sony.pro.mwa.kbase.model.rule;

import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.activity.template.ActivityTemplate;

public class MwaRule {
    private ActivityTemplate template;
    public Map<String, Object> params = null;
    public Map<String, Object> result = null;


    public MwaRule() {
        if (params == null) {
            params = new HashMap<String, Object>();
        }
        if (result == null) {
            result = new HashMap<String, Object>();
        }
    }

    public void setKeyValue(String key, Object value) {
        this.params.put(key, value);
    }

    public void setKeyValues(Map<String, Object> params) {
        this.params.putAll(params);
    }

    public boolean containsKey(String key) {
        return this.params.containsKey(key);
    }

    public int getKeyIntValue(String key) {
        String value = (String)this.params.get(key);
        return Integer.parseInt(value);
    }

    public double getKeyDoubleValue(String key) {
        String value = (String)this.params.get(key);
        return Double.parseDouble(value);
    }

    public boolean getKeyBooleanValue(String key) {
        String value = (String)this.params.get(key);
        return Boolean.parseBoolean(value);
    }

    public String getKeyStringValue(String key) {
        return (String)this.params.get(key);
    }

    public Object getKeyValue(String key) {
        return this.params.get(key);
    }

    public void setActionResult(String key, String value) {
        this.result.put(key, value);
    }

    public Map<String, Object> getActionResult() {
        return this.result;
    }

    public ActivityTemplate getTemplate() {
        return template;
    }

    public void setTemplate(ActivityTemplate template) {
        this.template = template;
    }
}