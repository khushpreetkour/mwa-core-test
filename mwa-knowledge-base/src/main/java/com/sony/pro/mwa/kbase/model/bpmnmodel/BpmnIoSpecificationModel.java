package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "dataInput", "dataOutput", "inputSet", "outputSet" })
@XmlRootElement(name = "ioSpecification", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnIoSpecificationModel extends BpmnModelBase {

	private List<BpmnDataInputModel> dataInput = new ArrayList<BpmnDataInputModel>();
	private BpmnInputSetModel inputSet;
	private List<BpmnDataOutputModel> dataOutput = new ArrayList<BpmnDataOutputModel>();
	private BpmnOutputSetModel outputSet;

	@XmlElement(name = "dataInput", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnDataInputModel> getDataInput() {
		return dataInput;
	}

	public void setDataInput(List<BpmnDataInputModel> dataInput) {
		this.dataInput = dataInput;
	}

	@XmlElement(name = "inputSet", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnInputSetModel getInputSet() {
		return inputSet;
	}

	public void setInputSet(BpmnInputSetModel inputSet) {
		this.inputSet = inputSet;
	}

	@XmlElement(name = "dataOutput", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<BpmnDataOutputModel> getDataOutput() {
		return dataOutput;
	}

	public void setDataOutput(List<BpmnDataOutputModel> dataOutput) {
		this.dataOutput = dataOutput;
	}

	@XmlElement(name = "outputSet", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnOutputSetModel getOutputSet() {
		return outputSet;
	}

	public void setOutputSet(BpmnOutputSetModel outputSet) {
		this.outputSet = outputSet;
	}

}
