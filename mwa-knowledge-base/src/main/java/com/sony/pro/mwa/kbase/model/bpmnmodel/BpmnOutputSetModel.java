package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "name", "dataOutputRefs" })
@XmlRootElement(name = "outputSet", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnOutputSetModel extends BpmnModelBase {

	private String name;
	private List<String> dataOutputRefs = new ArrayList<String>();

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "dataOutputRefs", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<String> getDataOutputRefs() {
		return dataOutputRefs;
	}

	public void setDataOutputRefs(List<String> dataOutputRefs) {
		this.dataOutputRefs = dataOutputRefs;
	}

}
