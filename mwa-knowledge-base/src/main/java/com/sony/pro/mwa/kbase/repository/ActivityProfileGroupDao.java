package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.ActivityProfileGroupCollection;
import com.sony.pro.mwa.model.kbase.ActivityProfileGroupModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityProfileGroupDao extends ModelBaseDao<ActivityProfileGroupModel, ActivityProfileGroupCollection> {
}
