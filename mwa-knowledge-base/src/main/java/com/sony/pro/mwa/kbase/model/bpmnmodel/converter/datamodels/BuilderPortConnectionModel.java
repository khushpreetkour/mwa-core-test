package com.sony.pro.mwa.kbase.model.bpmnmodel.converter.datamodels;

public class BuilderPortConnectionModel {
	private String srcPortId;
	private String destPortId;

	/**
	 * srcPortIdを取得します。
	 *
	 * @return srcPortId
	 */
	public String getSrcPortId() {
		return srcPortId;
	}

	/**
	 * srcPortIdを設定します。
	 *
	 * @param srcPortId
	 *            srcPortId
	 */
	public void setSrcPortId(String srcPortId) {
		this.srcPortId = srcPortId;
	}

	/**
	 * destPortIdを取得します。
	 *
	 * @return destPortId
	 */
	public String getDestPortId() {
		return destPortId;
	}

	/**
	 * destPortIdを設定します。
	 *
	 * @param destPortId
	 *            destPortId
	 */
	public void setDestPortId(String destPortId) {
		this.destPortId = destPortId;
	}
}
