package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnExclusiveGatewayModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;

public class GatewayModel  extends InnerBpmnModelBuilderBase<BpmnExclusiveGatewayModel> {
	
	/* Test Code */
	private List<String> incoming = new ArrayList<>();
	private List<String> outgoing = new ArrayList<>();
	private String name;
	private String gatewayDirection;
	private String defaultEsc;
	private BpmnExclusiveGatewayModel exclusiveGatewayModel;
	
	// Gatewayは仕様未確定の為、いったん保留
	public GatewayModel(String id) {
		super(id);
	}
	
	public void createBpmnExclusiveGatewayModel() {
		exclusiveGatewayModel = new BpmnExclusiveGatewayModel();
		exclusiveGatewayModel.setId(id);
		exclusiveGatewayModel.setName(name);
		exclusiveGatewayModel.setIncoming(incoming);
		exclusiveGatewayModel.setOutgoing(outgoing);
		exclusiveGatewayModel.setGatewayDirection(gatewayDirection);
		exclusiveGatewayModel.setDefaultEsc(defaultEsc);
	}

	@Override
	public BpmnExclusiveGatewayModel getModel() {
		return exclusiveGatewayModel;
	}

	/* setter and getter */
	public List<String> getIncoming() {
		return incoming;
	}
	
	public List<String> getOutgoing() {
		return outgoing;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGatewayDirection(String gatewayDirection) {
		this.gatewayDirection = gatewayDirection;
	}
	
	public String getGatewayDirection() {
		return gatewayDirection;
	}
	
	public void setDefaultEsc(String defaultEsc) {
		this.defaultEsc = defaultEsc;
	}
	
	public String getDefaultEsc() {
		return defaultEsc;
	}
	
}
