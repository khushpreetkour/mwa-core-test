package com.sony.pro.mwa.kbase.bpmnmanager.model;

import com.sony.pro.mwa.kbase.model.bpmnmodel.converter.datamodels.BuilderPropertyModel;

public class PortModel {
	private String id;
	private String type;
	private BuilderPropertyModel property;
	private String paretntId;

	public PortModel(String id) {
		this.id = id;
	}

	public PortModel(String id, String type) {
		this.id = id;
		this.type = type;
	}

	public PortModel(String id, String type, BuilderPropertyModel property) {
		this.id = id;
		this.type = type;
		this.property = property;
	}

	/* setter and getter */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BuilderPropertyModel getProperty() {
		return property;
	}

	public void setProperty(BuilderPropertyModel property) {
		this.property = property;
	}

	public String getParetntId() {
		return paretntId;
	}

	public void setParetntId(String paretntId) {
		this.paretntId = paretntId;
	}
}
