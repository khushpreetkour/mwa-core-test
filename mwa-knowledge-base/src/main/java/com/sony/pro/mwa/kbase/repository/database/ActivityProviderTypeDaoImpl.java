package com.sony.pro.mwa.kbase.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.ExtensionContentType;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.repository.database.query.ActivityProviderTypeQuery;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.kbase.repository.ActivityProviderTypeDao;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ActivityProviderTypeDaoImpl extends DatabaseBaseDao<ActivityProviderTypeModel, ActivityProviderTypeCollection, QueryCriteria> implements ActivityProviderTypeDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ActivityProviderTypeCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ActivityProviderTypeModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.activity_provider_type (" +
					"activity_provider_type_id" +
					", activity_provider_type_name" +
					", activity_provider_type_model_number" +
					", activity_provider_type_create_time" +
					", activity_provider_type_update_time" +
					") " +
			"VALUES (" +
					"?, ?, ?, ?, ?" +
					") ";
    private static final String INSERT_SQL2 =
            "INSERT INTO mwa.extension_content(" +
            		"  extension_id" +
            		", extension_content_type" +
            		", extension_content_id" +
            		") " +
            "VALUES (" +
                    "?, ?, ?" +
                    ") ";

	public ActivityProviderTypeModel addModel(final ActivityProviderTypeModel model) {
		
		deleteModel2(model);

		final String insertSql = INSERT_SQL;
        final String insertSql2 = INSERT_SQL2;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;

                        ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getModelNumber());
						model.setCreateTime(System.currentTimeMillis());
						model.setUpdateTime(model.getCreateTime());
						ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));
						
						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);

        if (model.getPluginId() != null) {
	        jdbcTemplate.update(
	                new PreparedStatementCreator() {
	                    public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
	                        PreparedStatement ps = conn.prepareStatement(INSERT_SQL2, new String[] {});
	                        int colNum = 1;
	                        ps.setObject(colNum++, UUID.fromString(model.getPluginId()));
	                        ps.setString(colNum++, ExtensionContentType.ACTIVITY_PROVIDER_TYPE.name());
	                        ps.setObject(colNum++, UUID.fromString(model.getId()));
	                        logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
	                        return ps;
	                    }
	                },
	                keyHolder);
        }
		ActivityProviderTypeModel addedInstance = model;
		return insertedRowCount != 0 ? addedInstance : null;
	}
	
	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.activity_provider_type " +
			"SET " +
					"activity_provider_type_name = ? " +
					", activity_provider_type_model_number = ? " +
					", activity_provider_type_update_time = ? " +
			"WHERE " +
					"activity_provider_type_id = ? ";

    private static final String UPDATE_SQL2 =
            "UPDATE " +
            		"mwa.extension_content " +
            "SET " +
            		"extension_id = ? " +
            "WHERE " +
            		"extension_content_id = ? ";

	public ActivityProviderTypeModel updateModel(final ActivityProviderTypeModel model) {
		final String updateSql = UPDATE_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(updateSql);
						int colNum = 1;

						ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getModelNumber());
						model.setUpdateTime(System.currentTimeMillis());
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));
						
                        ps.setObject(colNum++, UUID.fromString(model.getId()));
						
						return ps;
					}
				});

    	deleteMap(model.getId());
    	if (model.getPluginId() != null) {
	        jdbcTemplate.update(
	                new PreparedStatementCreator() {
	                    public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
	                        PreparedStatement ps = conn.prepareStatement(INSERT_SQL2, new String[] {});
	                        int colNum = 1;
	                        ps.setObject(colNum++, UUID.fromString(model.getPluginId()));
	                        ps.setString(colNum++, ExtensionContentType.ACTIVITY_PROVIDER_TYPE.name());
	                        ps.setObject(colNum++, UUID.fromString(model.getId()));
	                        logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
	                        return ps;
	                    }
	                },
	                keyHolder);
    	}
		return updatedRowCount != 0 ? model : null;
	}

    private static final String DELETE_MAP_SQL =
            "DELETE FROM " +
                    "mwa.extension_content " +
            "WHERE " +
                    "extension_content_id = ? ";

    public void deleteMap(String templateId) {
        String deleteSql = DELETE_MAP_SQL;

        Object[] paramArray = new Object[]{UUID.fromString(templateId)};
        if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
        }
        return;
    }
	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.activity_provider_type " +
			"WHERE " +
					"activity_provider_type_id = ? ";

	public ActivityProviderTypeModel deleteModel(ActivityProviderTypeModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	private static final String DELETE2_SQL =
			"DELETE FROM " +
					"mwa.activity_provider_type " +
			"WHERE " +
					"activity_provider_type_name = ?  AND activity_provider_type_model_number = ?";
	
	
	public ActivityProviderTypeModel deleteModel2(ActivityProviderTypeModel model) {
		String deleteSql = DELETE2_SQL;

		Object[] paramArray = new Object[]{model.getName(),model.getModelNumber()};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}
	
	@Override
	protected IQuery<ActivityProviderTypeModel, ActivityProviderTypeCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ActivityProviderTypeQuery(jdbcTemplate);
	}


}
