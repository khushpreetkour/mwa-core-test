package com.sony.pro.mwa.kbase.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.repository.database.query.ParameterTypeQuery;
import com.sony.pro.mwa.model.kbase.ParameterTypeCollection;
import com.sony.pro.mwa.model.kbase.ParameterTypeModel;
import com.sony.pro.mwa.kbase.repository.ParameterTypeDao;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ParameterTypeDaoImpl extends DatabaseBaseDao<ParameterTypeModel, ParameterTypeCollection, QueryCriteria> implements ParameterTypeDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ParameterTypeCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ParameterTypeModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.parameter_type (" +
					"parameter_type_id" +
					", parameter_type_name" +
					", parameter_type_version" +
					", parameter_type_values" +
					", parameter_type_create_time" +
					", parameter_type_update_time" +
					") " +
			"VALUES (" +
					"?, ?, ?, ?, ?" +
					") ";

	public ParameterTypeModel addModel(final ParameterTypeModel model) {
		
		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;

                        ps.setObject(colNum++, model.getId());
						ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getVersion());
						ps.setString(colNum++, model.getValuesString());
						model.setCreateTime(System.currentTimeMillis());
						model.setUpdateTime(model.getCreateTime()); 
						ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));
						
						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);
		
		ParameterTypeModel addedInstance = model;
		return insertedRowCount != 0 ? addedInstance : null;
	}
	
	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.parameter_type " +
			"SET " +
					"parameter_type_name = ? " +
					", parameter_type_model_number = ? " +
					", parameter_type_update_time = ? " +
			"WHERE " +
					"parameter_type_id = ? ";

	public ParameterTypeModel updateModel(final ParameterTypeModel model) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.parameter_type " +
			"WHERE " +
					"parameter_type_id = ? ";

	public ParameterTypeModel deleteModel(ParameterTypeModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{model.getId()};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	private static final String DELETE2_SQL =
			"DELETE FROM " +
					"mwa.parameter_type " +
			"WHERE " +
					"parameter_type_name = ?  AND parameter_type_version = ?";
	
	
	public ParameterTypeModel deleteModel2(ParameterTypeModel model) {
		String deleteSql = DELETE2_SQL;

		Object[] paramArray = new Object[]{model.getName(),model.getVersion()};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}
	
	@Override
	protected IQuery<ParameterTypeModel, ParameterTypeCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ParameterTypeQuery(jdbcTemplate);
	}


}
