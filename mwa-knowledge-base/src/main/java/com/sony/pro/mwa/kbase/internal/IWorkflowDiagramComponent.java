package com.sony.pro.mwa.kbase.internal;

import java.util.List;

import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoCollection;
import com.sony.pro.mwa.model.kbase.WorkflowDiagramDaoModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramCollection;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.repository.query.QueryCriteria;

public interface IWorkflowDiagramComponent {
	/**
	 * WorkflowDiagramModelの登録
	 * 
	 * @param workflowDiagramModelJson 登録されるWorkflowDiagram情報
	 * @return 登録されたWorkflowDiagramModel
	 */
	
	public WorkflowDiagramModel registerWorkflowDiagram(String workflowDiagramModelJson);
	
	/**
	 * WorkflowDiagramModelの取得
	 * 
	 * @param workflowDiagramId 取得するWorkflowDiagramModelのID
	 * @return 取得したWorkflowDiagramModel
	 */
	public WorkflowDiagramModel getWorkflowDiagram(String workflowDiagramId);
	
	
	public WorkflowDiagramDaoModel getWorkflowDiagramDaoModel(String workflowDiagramId);
	
	/**
	 * WorkflowDiagramModelの一覧取得
	 * 
	 * @param sorts 並び替え
	 * @param filters 条件
	 * @param offset オフセット
	 * @param limit リミット
	 * @return 取得したWorkflowDiagramModelの一覧
	 */
	public WorkflowDiagramCollection getWorkflowDiagrams(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	
	public WorkflowDiagramDaoCollection getWorkflowDiagramDaoModels(QueryCriteria query);
	
	/**
	 * WorkflowDiagramModelの更新
	 * 
	 * @param workflowDiagramId 更新されるWorkflowDiagramModelのID
	 * @param workflowDiagramModelJson 更新されるWorkflowDiagramModelの情報
	 * @return 更新されたWorkflowDiagramModel
	 */
	public WorkflowDiagramModel updateWorkflowDiagram(String workflowDiagramId, String workflowDiagramModelJson);
	
	public WorkflowDiagramModel updateWorkflowDiagramModel(WorkflowDiagramModel model);
		
    public WorkflowDiagramModel deleteWorkflowDiagramDaoModel(String daoModelId);
}

