package com.sony.pro.mwa.kbase.model.bpmnmodel.converter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.kbase.model.bpmnmodel.IBpmnModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.generator.BpmnXmlGenerator;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDiagramModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;
import com.sony.pro.mwa.rc.MWARC;

public class WorkflowDiagramModelConverter {
	private static final String BPMNS = "\\bpmns\\";
	private static final String S_S = "%s%s";
	public static final String MWA_HOME = System.getenv("MWA_HOME");
	private static final String JSON_WF_MODEL = "jsonWfModel";

	private static MwaLogger logger = MwaLogger.getLogger(WorkflowDiagramModelConverter.class);

	private static WorkflowDiagramModelConverter singleton = new WorkflowDiagramModelConverter();

	public static WorkflowDiagramModelConverter getInstance() {
		return singleton;
	}

	// デフォルトコンストラクタ
	private WorkflowDiagramModelConverter() {

	}

	/**
	 * bpmnを生成します。
	 * @param jsonWfModel
	 * @return
	 */
//	public String generateBpmn(String jsonWfModel) {
//		logger.debug(JSON_WF_MODEL + "=" + jsonWfModel);
//
//		// 疎通確認のためコメントアウト
//		WorkflowDiagramModel workflowDiagramModel = deserialize(jsonWfModel);
//		String xml = generateBpmn(workflowDiagramModel);
//
//		logger.debug("generateBpmn=" + xml);
//
//		return xml;
//	}

	/**
	 * bpmnを生成します。
	 * @param wfModel
	 * @return
	 */
//	public String generateBpmn(WorkflowDiagramModel workflowDiagramModel) {// TODO
//														// 最終的にはkbaseで定義したjsonWFモデルを入れること。
//
//		IBpmnModel bpmnModel = null;
//
//		new WorkflowModelConverter().mapToBuilderModels(workflowDiagramModel);
//		bpmnModel = ModelHolder.getModel().getDefinitionsModel().getModel();
//
//		BpmnXmlGenerator generator = new BpmnXmlGenerator();
//		String xml = generator.generate(bpmnModel);
//
//		return xml;
//	}

	/**
	 * JSONデータをWorkflowModelデシリアライズします。
	 * @param jsonWfModel
	 * @return
	 */
	public static WorkflowDiagramModel deserialize(String jsonWfModel) {// TODO

		ObjectMapper mapper = new ObjectMapper();
		WorkflowDiagramModel workflowDiagramModel = new WorkflowDiagramModel();
		try {
			workflowDiagramModel = mapper.readValue(jsonWfModel, WorkflowDiagramModel.class);
		} catch (JsonParseException e) {
			final String errMsg = "Failed to Deserializing.";
			logger.error(errMsg);
			throw new MwaError(MWARC.INVALID_INPUT, null, errMsg);
		} catch (JsonMappingException e) {
			final String errMsg = "Failed to Deserializing.";
			throw new MwaError(MWARC.INVALID_INPUT, null, errMsg);
		} catch (IOException e) {
			final String errMsg = "Failed to Deserializing.";
			logger.error(errMsg);
			throw new MwaError(MWARC.INVALID_INPUT, null, errMsg);
		}

		return workflowDiagramModel;// TODO この処理はUtilityに移動する。。
	}

	/**
	 * JSONデータをWorkflowDiagramModelをシリアライズします。
	 * @param jsonWfModel
	 * @return
	 */
	public static String serializeWfModel(WorkflowDiagramModel workflowDiagramModel) {// TODO

		ObjectMapper mapper = new ObjectMapper();
		String ret = "";
		try {
			ret = mapper.writeValueAsString(workflowDiagramModel);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return ret;// TODO この処理はUtilityに移動する。。
	}

//	public void deployBpmn(String xml, String fileName) {
//		String methodName = Thread.currentThread().getStackTrace()[0].getMethodName();
//		logger.debug("Start:" + methodName);
//
//		// 環境変数の取得
//		String path = String.format(S_S, MWA_HOME, BPMNS); //結合 ⇒ String path=mwa_home + "\\bpmns\\"
//
//		// 出力ファイル
//		File file = new File(String.format(S_S, path, fileName)); //結合 ⇒ File file = new File(path + "\\" + fileName);
//
//		// 出力ストリームの作成
//		try (FileOutputStream os = new FileOutputStream(file);
//				OutputStreamWriter ou = new OutputStreamWriter(os, "UTF-8");
//				PrintWriter pw = new PrintWriter(ou)) {
//
//			// 書き込み
//			pw.print(xml);
//
//		} catch (FileNotFoundException e) {
//			logger.error(e.getMessage());
//			throw new MwaError(MWARC.INTEGRATION_ERROR, null, "Failed in file output. (no write access permission)");
//		} catch (IOException ex) {
//			throw new MwaError(MWARC.INTEGRATION_ERROR, null, "Failed in file output.");
//		}
//
//	}
}
