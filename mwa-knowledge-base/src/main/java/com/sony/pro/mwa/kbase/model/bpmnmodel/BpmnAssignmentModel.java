package com.sony.pro.mwa.kbase.model.bpmnmodel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "from", "to" })
@XmlRootElement(name = "assignment", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnAssignmentModel extends BpmnModelBase {

	private BpmnFromModel from;
	private BpmnToModel to;

	@XmlElement(namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnFromModel getFrom() {
		return from;
	}

	public void setFrom(BpmnFromModel from) {
		this.from = from;
	}

	@XmlElement(namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public BpmnToModel getTo() {
		return to;
	}

	public void setTo(BpmnToModel to) {
		this.to = to;
	}
}
