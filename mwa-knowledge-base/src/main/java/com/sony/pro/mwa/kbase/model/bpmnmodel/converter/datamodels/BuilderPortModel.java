package com.sony.pro.mwa.kbase.model.bpmnmodel.converter.datamodels;

public class BuilderPortModel {
	private String id;
	private String type;
	private BuilderPropertyModel property;
	private String paretntId;

	public BuilderPortModel(String id) {
		this.id = id;
	}

	public BuilderPortModel(String id, String type) {
		this.id = id;
		this.type = type;
	}

	public BuilderPortModel(String id, String type, BuilderPropertyModel property) {
		this.id = id;
		this.type = type;
		this.property = property;
	}

	/**
	 * idを取得します。
	 *
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * idを設定します。
	 *
	 * @param id
	 *            id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * typeを取得します。
	 *
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * typeを設定します。
	 *
	 * @param type
	 *            type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * プロパティを取得します。
	 *
	 * @return
	 */
	public BuilderPropertyModel getProperty() {
		return property;
	}

	/**
	 * プロパティを設定します。
	 *
	 * @param property
	 *            property
	 */
	public void setProperty(BuilderPropertyModel property) {
		this.property = property;
	}

	/**
	 * 親タスクのIDを取得します。
	 *
	 * @return
	 */
	public String getParetntId() {
		return paretntId;
	}

	/**
	 * 親タスクのIDを設定します。
	 *
	 * @return
	 */
	public void setParetntId(String paretntId) {
		this.paretntId = paretntId;
	}
}
