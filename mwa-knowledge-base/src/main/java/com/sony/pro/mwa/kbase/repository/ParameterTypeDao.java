package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.kbase.ParameterTypeCollection;
import com.sony.pro.mwa.model.kbase.ParameterTypeModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ParameterTypeDao extends ModelBaseDao<ParameterTypeModel, ParameterTypeCollection> {
}
