package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnParallelGatewayModel;

public class ParallelGatewayModel extends InnerBpmnModelBuilderBase<BpmnParallelGatewayModel> {
	
	/* Test Code */
	private List<String> incoming = new ArrayList<>();
	private List<String> outgoing = new ArrayList<>();
	private String name;
	private String gatewayDirection;
	private BpmnParallelGatewayModel parallelGatewayModel;
	
	public ParallelGatewayModel(String id) {
		super(id);
	}
	
	public void createBpmnParallelGatewayModel() {
		parallelGatewayModel = new BpmnParallelGatewayModel();
		parallelGatewayModel.setId(id);
		parallelGatewayModel.setName(name);
		parallelGatewayModel.setIncoming(incoming);
		parallelGatewayModel.setOutgoing(outgoing);
		parallelGatewayModel.setGatewayDirection(gatewayDirection);
	}

	@Override
	public BpmnParallelGatewayModel getModel() {
		return parallelGatewayModel;
	}

	/* setter and getter */
	public List<String> getIncoming() {
		return incoming;
	}
	
	public List<String> getOutgoing() {
		return outgoing;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGatewayDirection(String gatewayDirection) {
		this.gatewayDirection = gatewayDirection;
	}
	
	public String getGatewayDirection() {
		return gatewayDirection;
	}
}
