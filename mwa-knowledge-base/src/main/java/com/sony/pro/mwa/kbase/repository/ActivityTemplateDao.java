package com.sony.pro.mwa.kbase.repository;

import com.sony.pro.mwa.model.activity.ActivityTemplateCollection;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityTemplateDao extends ModelBaseDao<ActivityTemplateModel, ActivityTemplateCollection> {
	public void deleteModelByPluginId(String pluginId);
}
