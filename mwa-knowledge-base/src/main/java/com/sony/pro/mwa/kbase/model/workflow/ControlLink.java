package com.sony.pro.mwa.kbase.model.workflow;


public class ControlLink {	//bpmn2:sequenceFlow相当
	//from, to ともStringで名前だけでもいいかも。Mapから引けるので
/*	protected String from;
	protected String to;
*/	
	
	protected IControlFlowItem from;
	protected IControlFlowItem to;
	
	//Step削除でLink自動削除のため仕組みを用意したいところcontains系か
	public boolean isLinked(String stepName) {
		return (stepName != null) && (stepName.equals(from) || stepName.equals(to));
	}
}
