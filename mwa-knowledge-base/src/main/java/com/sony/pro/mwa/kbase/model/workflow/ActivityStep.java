package com.sony.pro.mwa.kbase.model.workflow;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.activity.template.ActivityTemplate;

public class ActivityStep implements IControlFlowItem {	//bpmn2:taskに相当
	
	protected String name;
	protected ActivityTemplate template;
	
	//ProcessInstanceやDeviceなどWorkflowStep固有のパラメータをこのレイヤで解決
	
	//templateと重複してるようにみえるが、Stepに必要なパラメータの定義や動的なポート追加など（★動的なものはどこに保持する？？）
/*	protected Map<String, IDataFlowInput> stepInputs = new HashMap<String, AbsDataPort>(){{
		put(StepParameter.PARENT_INSTANCE_ID);
	}};
*/
	protected List<DataPortInput> stepInputs = new ArrayList<>();
	protected List<DataPortOutput> stepOutputs = new ArrayList<>();
	
	public void setTemplate(ActivityTemplate template) {
/*		for (IParameterDefinition paramDef : template.getInputs()) {
			//stepInputs.add(new DataPortInput(paramDef));
		}*/
	}
}
