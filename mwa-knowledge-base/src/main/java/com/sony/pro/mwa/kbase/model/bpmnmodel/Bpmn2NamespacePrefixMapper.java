package com.sony.pro.mwa.kbase.model.bpmnmodel;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class Bpmn2NamespacePrefixMapper extends NamespacePrefixMapper {

	@Override
	public String getPreferredPrefix(String namespaceURI, String arg1, boolean prefixFlg) {

		switch (namespaceURI) {
		case "http://www.w3.org/2001/XMLSchema-instance":
			return "xsi";
		case "http://www.omg.org/spec/BPMN/20100524/MODEL":
			return "bpmn2";
		case "http://www.omg.org/spec/BPMN/20100524/DI":
			return "bpmndi";
		case "http://www.omg.org/spec/DD/20100524/DC":
			return "dc";
		case "http://www.omg.org/spec/DD/20100524/DI":
			return "di";
		case "http://www.jboss.org/drools":
			return "drools";
		case "":
			return "";
		default:
			return "";
		}

	}

}
