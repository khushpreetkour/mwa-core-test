package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnAssignmentModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnDataInputAssociationModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnDataInputModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnDataOutputAssociationModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnDataOutputModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnFromModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnInputSetModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnIoSpecificationModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnOutputSetModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnTaskModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnToModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.converter.datamodels.BuilderPropertyModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.kbase.utils.KBaseUtil;

public class TaskModel extends InnerBpmnModelBuilderBase<BpmnTaskModel> {

	private String name;
	private String taskName;
	private String displayName;
	private String icon;
	private List<String> incoming = new ArrayList<>();
	private List<String> outgoing = new ArrayList<>();
	private List<BuilderPropertyModel> dataInput = new ArrayList<>();
	private List<BuilderPropertyModel> dataOutput = new ArrayList<>();

	private BpmnTaskModel taskModel;

	public TaskModel(String id) {
		super(id);
		this.setIcon(BpmnDefinition.TASK_ICON_NAME);
		// TODO Auto-generated constructor stub
	}


	/**
	 * WFのinput一覧を更新します。
	 * @param model
	 */
	public void putInput(BuilderPropertyModel model) {
		dataInput.add(model);
	}

	/**
	 * WFのinput一覧を更新します。
	 * @param model
	 */
	public void putOutput(BuilderPropertyModel model) {
		if (null != model.getKey()) {
			for (BuilderPropertyModel port: dataOutput) {
				/* createOutputAssociation にて作成した BuilderPropertyModel と key が一致した場合、
				   connections に入力された情報で上書きしたいため、createOutputAssociation で作成した方は削除する */
				if ((port.getKey() != null && port.getKey().equals(model.getKey()))
						&& (port.getSourceStepId() != null && port.getSourceStepId().equals(model.getSourceStepId()))
						&& (port.getTargetStepId() == null)) {
					dataOutput.remove(port);
					break;
				}
			}
		}
		dataOutput.add(model);
	}

	public void createBpmnTaskModek() {
		taskModel = new BpmnTaskModel();
		//taskModelの各パラメータを設定する。
		taskModel.setId(this.id);
		taskModel.setTaskName(this.taskName);
		taskModel.setIcon(this.icon);
		taskModel.setName(this.name);
		taskModel.setIncoming(incoming);
		taskModel.setOutgoing(outgoing);

		BpmnIoSpecificationModel ioSpecificationModel = new BpmnIoSpecificationModel();
		ioSpecificationModel.setId(this.id + BpmnDefinition.SUFFIX_OF_IOSPECIFICATION);

		// INPUTパラメータの登録
		List<BpmnDataInputModel> inputModelList = new ArrayList<>();
		List<BpmnDataInputAssociationModel> inputAssociationModelList = new ArrayList<>();

		BpmnInputSetModel inputSetModel = new BpmnInputSetModel();
		inputSetModel.setId(this.id + BpmnDefinition.SUFFIX_OF_INPUTSET);
		inputSetModel.setName(this.id + BpmnDefinition.SUFFIX_OF_INPUTSET);

		int assignmentIndex = 0;
		int formalExpressionIndex = 0;

		for (BuilderPropertyModel propModel : dataInput) {
			BpmnDataInputModel inputModel = new BpmnDataInputModel();
			String propKey = propModel.getTargetStepId() + BpmnDefinition.INFIX_OF_INNER_INPUT_REF + propModel.getKey();
			// inputModelListに同じIDがある場合に採番処理を行う。
			// ※ task の ioSpecification の dataInput のID／task の dataInputAssociation の IDで ID名が重複する可能性があるため。
			String suffix = createSuffixInputModel(inputModelList, propKey);
			propKey = propKey + suffix;

			inputModel.setId(propKey);
			inputModel.setName(propModel.getName());
			inputModel.setItemSubjectRef(propModel.getType());
			inputModelList.add(inputModel);
			inputSetModel.getDataInputRefs().add(propKey);

			BpmnDataInputAssociationModel inputAssociation = new BpmnDataInputAssociationModel();
			inputAssociation.setId(propModel.getTargetStepId() + BpmnDefinition.INFIX_OF_INPUT_ASSOCIATE + propModel.getKey() + suffix);
			if (StringUtils.isNotEmpty(propModel.getValue())) {
				// 即値対応
				// value 値が入っている場合は、SourceMRef ではなく、assignmentModel に詰める
				formalExpressionIndex++;
				BpmnFromModel fromModel = new BpmnFromModel();
				fromModel.setId(BpmnDefinition.ASSIGNMENT_FORMAL_EXPRESSION_ID_PREFIX + formalExpressionIndex);
				fromModel.setType(BpmnDefinition.CONDITION_EXPRESSION_TYPE);
				fromModel.setValue(deleteStartEndDoubleQuote(propModel.getValue()));
				formalExpressionIndex++;
				BpmnToModel toModel = new BpmnToModel();
				toModel.setId(BpmnDefinition.ASSIGNMENT_FORMAL_EXPRESSION_ID_PREFIX + formalExpressionIndex);
				toModel.setType(BpmnDefinition.CONDITION_EXPRESSION_TYPE);
				toModel.setValue(propKey);
				assignmentIndex++;
				BpmnAssignmentModel assignment = new BpmnAssignmentModel();
				assignment.setId(BpmnDefinition.ASSIGNMENT_ID_PREFIX + assignmentIndex);
				assignment.setFrom(fromModel);
				assignment.setTo(toModel);
				inputAssociation.setAssignment(assignment);
			} else {
				String prefix = propModel.getSourceStepId() + BpmnDefinition.CONNECTION_PART_OF_GLOBAL_ID;
				if (KBaseUtil.StepType.START == propModel.getStepType()) {
					// Startイベントの場合は、prefixをつけない。
					// ※ 作成したWFをExecuteする時に、RequestBodyのinputDetailsListで「key名」のみで指定したいため。
					prefix = "";
				}
				inputAssociation.setSourceRef(prefix + propModel.getKey());
			}
			inputAssociation.setTargetRef(propKey);
			inputAssociationModelList.add(inputAssociation);
		}
		ioSpecificationModel.setDataInput(inputModelList);
		ioSpecificationModel.setInputSet(inputSetModel);

		taskModel.setDataInputAssociation(inputAssociationModelList);

		// OUTPUTパラメータの登録
		List<BpmnDataOutputModel> outputModelList = new ArrayList<>();
		List<BpmnDataOutputAssociationModel> outputAssociationModelList = new ArrayList<>();

		BpmnOutputSetModel outputSetModel = new BpmnOutputSetModel();
		outputSetModel.setId(this.id + BpmnDefinition.SUFFIX_OF_OUTPUTSET);
		outputSetModel.setName(this.id + BpmnDefinition.SUFFIX_OF_OUTPUTSET);

		for (BuilderPropertyModel propModel : dataOutput) {
			BpmnDataOutputModel outputModel = new BpmnDataOutputModel();
			String propKey = this.id + BpmnDefinition.INFIX_OF_INNER_OUTPUT_REF + propModel.getKey();
			// outputModelListに同じIDがある場合に採番処理を行う。
			// ※ task の ioSpecification の dataOutput のID／task の dataOutputAssociation の IDで ID名が重複する可能性があるため。
			String suffix = createSuffixOutputModel(outputModelList, propKey);
			propKey = propKey + suffix;

			outputModel.setId(propKey);
			outputModel.setName(propModel.getName());
			outputModel.setItemSubjectRef(propModel.getType());
			outputModelList.add(outputModel);
			outputSetModel.getDataOutputRefs().add(propKey);

			BpmnDataOutputAssociationModel ouputAssociationModel = new BpmnDataOutputAssociationModel();
			ouputAssociationModel.setId(this.id + BpmnDefinition.INFIX_OF_OUTPUT_ASSOCIATE + propModel.getKey() + suffix);
			ouputAssociationModel.setSourceRef(propKey);
			if (StringUtils.isNotEmpty(propModel.getValue())) {
				ouputAssociationModel.setTargetRef(propModel.getValue());
			} else {
				String prefix = "";
				String targetStepId = this.id + BpmnDefinition.CONNECTION_PART_OF_GLOBAL_ID;
				if (KBaseUtil.StepType.END == propModel.getStepType()) {
					// targetRefがEndイベントの場合は、「OUTPUT.」を追加し、stepId部を空文字とする。
					// ※ 作成したWFをExecuteした時のOUTPUTに、「key名」のみで出力してほしいため。
					prefix = BpmnDefinition.PREFIX_OF_OUTPUT_PARAMETER;
					targetStepId = "";
				} else if (KBaseUtil.StepType.START == propModel.getStepType()) {
					targetStepId = "";
				}
				prefix = prefix + targetStepId;
				ouputAssociationModel.setTargetRef(prefix + propModel.getKey());
			}
			outputAssociationModelList.add(ouputAssociationModel);
		}
		// デフォルトで、OUTPUTとしてStatusを受け取る
//		setOutputStatus(outputSetModel, outputModelList, outputAssociationModelList);
		ioSpecificationModel.setDataOutput(outputModelList);
		ioSpecificationModel.setOutputSet(outputSetModel);
		taskModel.setDataOutputAssociation(outputAssociationModelList);

		// 最終的にINPUT・OUTPUTを登録する。
		taskModel.getIoSpecification().add(ioSpecificationModel);
	}

//	private void setOutputStatus(BpmnOutputSetModel outputSetModel,
//								List<BpmnDataOutputModel> outputModelList,
//								List<BpmnDataOutputAssociationModel> outputAssociationModelList) {
//
//		String key = "ActivityInstanceStatus";
//
//		outputSetModel.setId(this.id + BpmnDefinition.SUFFIX_OF_OUTPUTSET);
//		outputSetModel.setName(this.id + BpmnDefinition.SUFFIX_OF_OUTPUTSET);
//		String propKey = this.id + BpmnDefinition.INFIX_OF_INNER_OUTPUT_REF + key;
//		outputSetModel.getDataOutputRefs().add(propKey);
//
//		BpmnDataOutputModel outputModel = new BpmnDataOutputModel();
//		outputModel.setId(propKey);
//		outputModel.setName(key);
//		outputModel.setItemSubjectRef("TypeString");
//		outputModelList.add(outputModel);
//
//		BpmnDataOutputAssociationModel ouputAssociationModel = new BpmnDataOutputAssociationModel();
//
//		ouputAssociationModel.setId(this.id + BpmnDefinition.INFIX_OF_OUTPUT_ASSOCIATE + key);
//		ouputAssociationModel.setSourceRef(propKey);
//		ouputAssociationModel.setTargetRef(key);
//		outputAssociationModelList.add(ouputAssociationModel);
//	}

	/**
	 * 文字列のはじめとおわりについているダブルクォートを削除する
	 *
	 * @param value
	 * @return String
	 */
	private String deleteStartEndDoubleQuote(String value) {
		String result = null;
		Pattern sdqPattern = Pattern.compile("^\\\\\"|\\\\\"$");
		Matcher matcher = sdqPattern.matcher(value);
		result = matcher.replaceAll("");
		return result;
	}

	/**
	 * inputModelListに既に同じKeyがあった場合、Suffixを付ける必要があるので作成する。
	 *
	 * @param inputModelList
	 * @param propKey
	 * @return String
	 */
	private String createSuffixInputModel(List<BpmnDataInputModel> inputModelList, String propKey) {
		int keyExistCount = 0;
		for (BpmnDataInputModel inputModel: inputModelList) {
			if (inputModel.getId().startsWith(propKey)) {
				keyExistCount++;
			}
		}
		return createSuffix(keyExistCount);
	}

	/**
	 * outputModelListに既に同じKeyがあった場合、Suffixを付ける必要があるので作成する。
	 *
	 * @param outputModelList
	 * @param propKey
	 * @return String
	 */
	private String createSuffixOutputModel(List<BpmnDataOutputModel> outputModelList, String propKey) {
		int keyExistCount = 0;
		for (BpmnDataOutputModel outputModel: outputModelList) {
			if (outputModel.getId().startsWith(propKey)) {
				keyExistCount++;
			}
		}
		return createSuffix(keyExistCount);
	}

	/**
	 * IDに使用するSuffixを作成する。
	 * @param count
	 * @return String
	 */
	private String createSuffix(int count) {
		String result = "";
		if (count != 0) {
			result = "_" + Integer.toString(count + 1);
		}
		return result;
	}

	@Override
	public BpmnTaskModel getModel() {
		return taskModel;
	}

	/* setter and getter */
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getTaskName() {
		return taskName;
	}



	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}



	public String getDisplayName() {
		return displayName;
	}



	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}



	public String getIcon() {
		return icon;
	}



	public void setIcon(String icon) {
		this.icon = icon;
	}



	public List<String> getIncoming() {
		return incoming;
	}



	public void setIncoming(List<String> incoming) {
		this.incoming = incoming;
	}



	public List<String> getOutgoing() {
		return outgoing;
	}



	public void setOutgoing(List<String> outgoing) {
		this.outgoing = outgoing;
	}



	public List<BuilderPropertyModel> getDataInput() {
		return dataInput;
	}



	public void setDataInput(List<BuilderPropertyModel> dataInput) {
		this.dataInput = dataInput;
	}

	public List<BuilderPropertyModel> getDataOutput() {
		return dataOutput;
	}

	public void setDataOutput(List<BuilderPropertyModel> dataOutput) {
		this.dataOutput = dataOutput;
	}
}
