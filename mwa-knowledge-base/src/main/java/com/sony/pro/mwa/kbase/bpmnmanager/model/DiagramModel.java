package com.sony.pro.mwa.kbase.bpmnmanager.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.kbase.builder.bpmnmodel.InnerBpmnModelBuilderBase;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNDiagramModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNEdgeModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNPlaneModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBPMNShapeModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnBoundsModel;
import com.sony.pro.mwa.kbase.model.bpmnmodel.BpmnWaypointModel;
import com.sony.pro.mwa.kbase.utils.BpmnDefinition;
import com.sony.pro.mwa.model.kbase.tempmodels.StepModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowModel;

public class DiagramModel extends InnerBpmnModelBuilderBase<BpmnBPMNDiagramModel> {
	
	private static MwaLogger logger = MwaLogger.getLogger(DiagramModel.class);
	private BpmnBPMNDiagramModel bpmnDiagramModel;
	private BpmnBPMNPlaneModel planeModel;
	private String name;
		
	public DiagramModel(String index) {
		super(BpmnDefinition.DIAGRAM_ID + index);
	}
	
	public void initialize(WorkflowModel workflowModel, Map<String, GatewayModel> gatewayModelMap, Map<String, FlowModel> flowModelMap) {
		/* Test Code */
		/* Identification number of id */
		String index = "1";
		/* --------- */
		PlaneModel planeModel = new PlaneModel(index);
		planeModel.planeInitialize(workflowModel, gatewayModelMap, flowModelMap);
		planeModel.createBpmnPlaneModel();
		setName(workflowModel.getName());
		setBPMNPlane(planeModel.getModel());
	}

	public void createBpmnBPMNDiagramModel() {
		bpmnDiagramModel = new BpmnBPMNDiagramModel();
		bpmnDiagramModel.setId(id);
		bpmnDiagramModel.setName(name);
		bpmnDiagramModel.setBPMNPlane(planeModel);
	}
	
	@Override
	public BpmnBPMNDiagramModel getModel() {
		// TODO Auto-generated method stub
		return bpmnDiagramModel;
	}
	
	/* setter and getter */
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public BpmnBPMNPlaneModel getBPMNPlane() {
		return planeModel;
	}

	public void setBPMNPlane(BpmnBPMNPlaneModel bPMNPlane) {
		this.planeModel = bPMNPlane;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
