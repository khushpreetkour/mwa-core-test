package com.sony.pro.mwa.kbase.model.bpmnmodel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "id", "name", "gatewayDirection", "defaultEsc", "incoming", "outgoing" })
@XmlRootElement(name = "exclusiveGateway", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
public class BpmnExclusiveGatewayModel extends BpmnModelBase {

	private String name;
	private String gatewayDirection;
	private String defaultEsc;
	private List<String> incoming = new ArrayList<String>();
	private List<String> outgoing = new ArrayList<String>();

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute
	public String getGatewayDirection() {
		return gatewayDirection;
	}

	public void setGatewayDirection(String gatewayDirection) {
		this.gatewayDirection = gatewayDirection;
	}

	@XmlAttribute(name = "default")
	public String getDefaultEsc() {
		return defaultEsc;
	}

	public void setDefaultEsc(String defaultEsc) {
		this.defaultEsc = defaultEsc;
	}

	public List<String> getIncoming() {
		return incoming;
	}

	@XmlElement(name = "incoming", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public void setIncoming(List<String> incoming) {
		this.incoming = incoming;
	}

	@XmlElement(name = "outgoing", namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")
	public List<String> getOutgoing() {
		return outgoing;
	}

	public void setOutgoing(List<String> outgoing) {
		this.outgoing = outgoing;
	}

}
