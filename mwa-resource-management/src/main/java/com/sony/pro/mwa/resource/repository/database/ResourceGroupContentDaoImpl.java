package com.sony.pro.mwa.resource.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.model.resource.ResourceGroupContentCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupContentModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.resource.repository.ResourceGroupContentDao;
import com.sony.pro.mwa.resource.repository.database.query.ResourceGroupContentQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;

@Repository
public class ResourceGroupContentDaoImpl extends DatabaseBaseDao<ResourceGroupContentModel, ResourceGroupContentCollection, QueryCriteria> implements ResourceGroupContentDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ResourceGroupContentCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ResourceGroupContentModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.resource_group_content (" + 
					"  resource_group_content_resource_group_id" + 
					", resource_group_content_resource_id" + 
					", resource_group_content_resource_group_type" + 
					") " +
			"VALUES (" +
					"?, ?, (select resource_group_type from mwa.resource_group where resource_group_id = ?)" +
					") ";

	public ResourceGroupContentModel addModel(final ResourceGroupContentModel model) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int insertedRowCount = 0;
		for (final String groupId : model.getResourceGroupIds().values()) {
			insertedRowCount += jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(INSERT_SQL, new String[] {});
						int colNum = 1;
						
						ps.setObject(colNum++, UUID.fromString(groupId));
						ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setObject(colNum++, UUID.fromString(groupId));
						
						//logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
						
					}
				},
				keyHolder);
		}

		ResourceGroupContentModel addedModel = model;
		return insertedRowCount != 0 ? addedModel : null;
	}

	private static final String UPDATE_SQL = null;

	public ResourceGroupContentModel updateModel(final ResourceGroupContentModel model) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	private static final String DELETE_SQL_GROUP_ID =
			"DELETE FROM " +
					"mwa.resource_group_content " +
			"WHERE " +
					"resource_group_content_resource_group_id = ? ";

	private static final String DELETE_SQL_BY_RESOURCE_ID =
			"DELETE FROM " +
					"mwa.resource_group_content " +
			"WHERE " +
					"resource_group_content_resource_id = ? ";

	public ResourceGroupContentModel deleteModel(ResourceGroupContentModel model) {
		return deleteModelByResourceId(model);
	}

	public boolean deleteModelByGroupId(String groupId) {
		String deleteSql = DELETE_SQL_GROUP_ID;

		Object[] paramArray = new Object[]{UUID.fromString(groupId)};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return true;
		}
		return false;
	}
	public ResourceGroupContentModel deleteModelByResourceId(ResourceGroupContentModel model) {
		String deleteSql = DELETE_SQL_BY_RESOURCE_ID;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<ResourceGroupContentModel, ResourceGroupContentCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ResourceGroupContentQuery(jdbcTemplate);
	}
}
