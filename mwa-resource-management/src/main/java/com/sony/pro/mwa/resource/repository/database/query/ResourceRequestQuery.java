package com.sony.pro.mwa.resource.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.internal.utils.InternalConverter;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.ColumnUtils;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class ResourceRequestQuery extends Query<ResourceRequestModel, ResourceRequestCollection, QueryCriteria> {
    public enum COLUMN {
        ID("i.resource_request_id", "id", UUID.class, true),
        OCCUPANT_ID("i.resource_request_occupant_id", "occupantId", UUID.class, false),
        OCCUPANT_NAME("i.resource_request_occupant_name", "occupantName", String.class, true),
        RECEIVER_ID("i.resource_request_receiver_id", "receiverId", UUID.class, false),
        QUEUE_ID("i.resource_request_queue_id", "queueId", UUID.class, false),
        METADATA("i.resource_request_metadata", "metadata", SQLXML.class, false),
        ASSIGNED("i.resource_request_assigned_flag", "assignedFlag", Boolean.class, true),
        CREATE_TIME("i.resource_request_create_time", "createTime", Timestamp.class, true),
        UPDATE_TIME("i.resource_request_update_time", "updateTime", Timestamp.class, true),
        PRIORITY_0("j.resource_request_priority_0", "priority_0", String.class, true),
        PRIORITY_1("j.resource_request_priority_1", "priority_1", String.class, true),
        PRIORITY_2("j.resource_request_priority_2", "priority_2", String.class, true),
        PRIORITY_3("j.resource_request_priority_3", "priority_3", String.class, true),
        PRIORITY_4("j.resource_request_priority_4", "priority_4", String.class, true),
        ;
        
        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String WITH_RECURSIVE_CLAUSE = "";

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " + 
            "mwa.resource_request AS i "+
            "LEFT JOIN mwa.resource_request_priority AS j ON i.resource_request_id = j.resource_request_priority_resource_request_id " +
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        DatabaseEnum database = getDatabase();
        for (COLUMN enumColumn : COLUMN.values()) {
        	//XMLをサポートしてないものについてはStirngにおきかえ
        	if (!DatabaseEnum.POSTGRESQL.equals(database) && enumColumn.type.equals(SQLXML.class)) 
        		map.put(enumColumn.alias, new Column(enumColumn.name, String.class, enumColumn.sortable));
        	else
        		map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }

    public ResourceRequestQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }
    
    @Override
    protected RowMapper<ResourceRequestModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<ResourceRequestModel> {
        public ResourceRequestModel mapRow(ResultSet rs, int rowNum) throws SQLException {
            DatabaseEnum database = getDatabase();
        	ResourceRequestModel model = new ResourceRequestModel();
        	model.setId(rs.getString(COLUMN.ID.alias));
        	model.setOccupantId(rs.getString(COLUMN.OCCUPANT_ID.alias));
        	model.setOccupantName(rs.getString(COLUMN.OCCUPANT_NAME.alias));
        	model.setReceiverId(rs.getString(COLUMN.RECEIVER_ID.alias));
        	model.setQueueId(rs.getString(COLUMN.QUEUE_ID.alias));
    		try {
        		String metadata = null;

        		if (DatabaseEnum.POSTGRESQL.equals(database)) {
	        		SQLXML metadataSql = rs.getSQLXML(COLUMN.METADATA.alias);
	        		metadata = (metadataSql != null) ? metadataSql.getString() : null;
	            } else {
	            	metadata = rs.getString(COLUMN.METADATA.alias);
	            }
        		
    			if (metadata != null) {
            		Map<String,Object> result = InternalConverter.xmlToMap(metadata);
            		model.setMetadata(result);
    			}
    		} catch (Exception e) {
    			throw new RuntimeException(e);
    		}
    		
        	model.setCreateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.CREATE_TIME.alias));
        	model.setUpdateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.UPDATE_TIME.alias));
        	
        	model.getPriority().add(rs.getString(COLUMN.PRIORITY_0.alias));
        	model.getPriority().add(rs.getString(COLUMN.PRIORITY_1.alias));
        	model.getPriority().add(rs.getString(COLUMN.PRIORITY_2.alias));
        	model.getPriority().add(rs.getString(COLUMN.PRIORITY_3.alias));
        	model.getPriority().add(rs.getString(COLUMN.PRIORITY_4.alias));
        	
        	return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }
}
