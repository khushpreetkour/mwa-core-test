package com.sony.pro.mwa.resource.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.model.resource.ResourceRequestEntryCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestEntryModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.resource.repository.ResourceRequestEntryDao;
import com.sony.pro.mwa.resource.repository.database.query.ResourceRequestEntryQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.exception.MwaError;

@Repository
public class ResourceRequestEntryDaoImpl extends DatabaseBaseDao<ResourceRequestEntryModel, ResourceRequestEntryCollection, QueryCriteria> implements ResourceRequestEntryDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ResourceRequestEntryCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ResourceRequestEntryModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.resource_request_entry (" + 
					"  resource_request_entry_resource_request_id" + 
					", resource_request_entry_resource_id" + 
					", resource_request_entry_cost_type" +
					", resource_request_entry_cost_values" +
					", resource_request_entry_assigned_resource_id" +
					") " +
			"VALUES (" +
					"?, ?, ?, ?, ?" +
					") ";

	public ResourceRequestEntryModel addModel(final ResourceRequestEntryModel model) {
		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;
						
						ps.setObject(colNum++, UUID.fromString(model.getResourceRequestId()));
						ps.setObject(colNum++, UUID.fromString(model.getResourceId()));
						ps.setString(colNum++, model.getCost().getType());
						ps.setString(colNum++, Converter.mapToXml(model.getCost().getValues()));
						if (model.getAssignedResourceId() != null) 
							ps.setObject(colNum++, UUID.fromString(model.getAssignedResourceId()));
						else
							ps.setNull(colNum++, Types.OTHER);
						
						return ps;
					}
				},
				keyHolder);

		ResourceRequestEntryModel addedModel = model;
		return insertedRowCount != 0 ? addedModel : null;
	}
	
	private static final String UPDATE_SQL_FOR_OCCUPATION =
			"UPDATE " +
					"mwa.resource_request_entry " +
			"SET " +
					"resource_request_entry_cost_values = ? " +
					", resource_request_entry_assigned_resource_id = ? " +
			"WHERE " +
					"resource_request_entry_resource_request_id = ? " + 
					"AND resource_request_entry_resource_id = ?";

	public ResourceRequestEntryModel updateModel(final ResourceRequestEntryModel model) {
		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(UPDATE_SQL_FOR_OCCUPATION);
						int colNum = 1;

						ps.setString(colNum++, Converter.mapToXml(model.getCost().getValues()));
						if (model.getAssignedResourceId() != null) 
							ps.setObject(colNum++, UUID.fromString(model.getAssignedResourceId()));
						else
							ps.setNull(colNum++, Types.OTHER);
						
						ps.setObject(colNum++, UUID.fromString(model.getResourceRequestId()));
						ps.setObject(colNum++, UUID.fromString(model.getResourceId()));
						return ps;
					}
				});

		return updatedRowCount != 0 ? model : null;
	}
	
	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.resource_request_entry " +
			"WHERE " +
					"resource_request_entry_resource_request_id = ? ";

	@Override
	public ResourceRequestEntryModel deleteModel(ResourceRequestEntryModel model) {
		throw new MwaError(MWARC.INTEGRATION_ERROR, null, "Not implemented yet");
	}
	
	public boolean deleteModel(String requestId) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{UUID.fromString(requestId)};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return true;
		}
		return false;
	}

	@Override
	protected IQuery<ResourceRequestEntryModel, ResourceRequestEntryCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ResourceRequestEntryQuery(jdbcTemplate);
	}

}
