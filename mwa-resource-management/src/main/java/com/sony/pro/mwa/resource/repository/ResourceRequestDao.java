package com.sony.pro.mwa.resource.repository;

import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ResourceRequestDao extends ModelBaseDao<ResourceRequestModel, ResourceRequestCollection> {
	public int count(String whereCondition);
}
