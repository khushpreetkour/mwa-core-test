package com.sony.pro.mwa.resource.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.model.resource.ResourceGroupCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupModel;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.resource.repository.ResourceGroupDao;
import com.sony.pro.mwa.resource.repository.database.query.ResourceGroupQuery;
import com.sony.pro.mwa.common.log.MwaLogger;

@Repository
public class ResourceGroupDaoImpl extends DatabaseBaseDao<ResourceGroupModel, ResourceGroupCollection, QueryCriteria> implements ResourceGroupDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ResourceGroupCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ResourceGroupModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.resource_group(" + 
					"  resource_group_id" + 
					", resource_group_name" + 
					", resource_group_type" +
					", resource_group_sub_type_1" +
					", resource_group_sub_type_2" +
					", resource_group_sub_type_3" +
					", resource_group_create_time" +
					", resource_group_update_time" +
					") " +
			"VALUES (" +
					"?, ?, ?, ?, ?, ?, ?, ?" +
					") ";

	private static final String INSERT_SQL_FOR_CONTENT =
			"INSERT INTO mwa.resource_group_content(" + 
					"  resource_group_content_resource_group_id" + 
					", resource_group_content_resource_id" +
					", resource_group_content_resource_group_type" +
					") " +
			"VALUES (" +
					"?, ?, ?" +
					") ";

	public ResourceGroupModel addModel(final ResourceGroupModel model) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(INSERT_SQL, new String[] {});
						int colNum = 1;
						
						ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getType());
						{
							//subType3までセット
							for (int i = 0; i < 3; i++) {
								if (model.getSubTypes() != null && model.getSubTypes().size() > i) 
									ps.setString(colNum++, model.getSubTypes().get(i));
								else 
									ps.setNull(colNum++, Types.SQLXML);
							}
						}
						model.setCreateTime(System.currentTimeMillis());
						model.setUpdateTime(model.getCreateTime());
						ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));
						
						//logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);

		int insertedContentRowCount  = 0;
		for (final String id : model.getResourceIds()) {
			insertedContentRowCount += jdbcTemplate.update(
					new PreparedStatementCreator() {
						public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
							PreparedStatement ps = conn.prepareStatement(INSERT_SQL_FOR_CONTENT, new String[] {});
							int colNum = 1;
							
							ps.setObject(colNum++, UUID.fromString(model.getId()));
							ps.setObject(colNum++, UUID.fromString(id));
							ps.setString(colNum++, model.getType());
							return ps;
						}
					},
					keyHolder);
		}

		ResourceGroupModel addedModel = model;
		return insertedRowCount != 0 ? addedModel : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.resource_group " +
			"SET " +
					"resource_group_name = ? " +
					", resource_group_type = ? " +
					", resource_group_sub_type_1 = ? " +
					", resource_group_sub_type_2 = ? " +
					", resource_group_sub_type_3 = ? " +
					", resource_group_update_time = ? " +
			"WHERE " +
					"resource_group_id = ? ";

	public ResourceGroupModel updateModel(final ResourceGroupModel model) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(UPDATE_SQL);
						int colNum = 1;

						ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getType());
						{
							//subType3までセット
							for (int i = 0; i < 3; i++) {
								if (model.getSubTypes() != null && model.getSubTypes().size() > i) 
									ps.setString(colNum++, model.getSubTypes().get(i));
								else 
									ps.setNull(colNum++, Types.SQLXML);
							}
						}
						model.setUpdateTime(System.currentTimeMillis());
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));

						ps.setObject(colNum++, UUID.fromString(model.getId()));
						return ps;
					}
				});

		deleteContents(model);	//事前にContentを一斉削除して再登録。★Modelの中身が削除されないように注意。要改善。
		int updatedContentRowCount = 0;
		{
			for (final String resourceId : model.getResourceIds()) {
				updatedContentRowCount += jdbcTemplate.update(
						new PreparedStatementCreator() {
							public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
								PreparedStatement ps = conn.prepareStatement(INSERT_SQL_FOR_CONTENT, new String[] {});
								int colNum = 1;
								
								ps.setObject(colNum++, UUID.fromString(model.getId()));
								ps.setObject(colNum++, UUID.fromString(resourceId));
								ps.setString(colNum++, model.getType());
								return ps;
							}
						},
						keyHolder);
			}
		}

		return updatedRowCount != 0 ? model : null;
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.resource_group " +
			"WHERE " +
					"resource_group_id = ? ";

	private static final String DELETE_SQL_FOR_CONTENT =
			"DELETE FROM " +
					"mwa.resource_group_content " +
			"WHERE " +
					"resource_group_content_resource_group_id = ? ";

	public ResourceGroupModel deleteModel(ResourceGroupModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}
	
	public ResourceGroupModel deleteContents(ResourceGroupModel model) {
		String deleteSql = DELETE_SQL_FOR_CONTENT;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<ResourceGroupModel, ResourceGroupCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ResourceGroupQuery(jdbcTemplate);
	}
}
