package com.sony.pro.mwa.resource.repository;

import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ResourceDao extends ModelBaseDao<ResourceModel, ResourceCollection> {
}
