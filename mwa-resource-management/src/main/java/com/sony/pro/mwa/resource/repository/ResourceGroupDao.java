package com.sony.pro.mwa.resource.repository;

import com.sony.pro.mwa.model.resource.ResourceGroupCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ResourceGroupDao extends ModelBaseDao<ResourceGroupModel, ResourceGroupCollection> {
}
