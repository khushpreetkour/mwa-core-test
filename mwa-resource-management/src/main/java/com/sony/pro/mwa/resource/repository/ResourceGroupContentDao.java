package com.sony.pro.mwa.resource.repository;

import com.sony.pro.mwa.model.resource.ResourceGroupContentCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupContentModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ResourceGroupContentDao extends ModelBaseDao<ResourceGroupContentModel, ResourceGroupContentCollection> {
	public boolean deleteModelByGroupId(String groupId);
}
