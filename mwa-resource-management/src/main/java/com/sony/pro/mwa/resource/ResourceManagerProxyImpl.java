package com.sony.pro.mwa.resource;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.event.EventTargetType;
import com.sony.pro.mwa.enumeration.resource.ResourceEvent;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.event.EventModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupModel;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.utils.RestUtils;
import com.sony.pro.mwa.utils.UriUtils;
import com.sony.pro.mwa.utils.rest.HttpMethodType;
import com.sony.pro.mwa.utils.rest.IRestInput;
import com.sony.pro.mwa.utils.rest.RestClient;
import com.sony.pro.mwa.utils.rest.RestInputImpl;

@Component
public class ResourceManagerProxyImpl implements IResourceManager {

	private final static MwaLogger logger = MwaLogger.getLogger(ResourceManagerProxyImpl.class);

//  ActivityはInternalに移動したので以下は不要になる。
//	private IKnowledgeBase kBase;
	private IEventManager eventManager;

	protected RestClient restClient = new RestClient("");
	private static final String RESOURCE_URL_FORMAT = "http://%s:%s/mwa/api/v2/resources";
	private static final String RESOURCE_URL_ADDED_ID_FORMAT = "http://%s:%s/mwa/api/v2/resources/%s";

	private static final String RESOURCE_REQUEST_URL_FORMAT = "http://%s:%s/mwa/api/v2/resource-requests";
	private static final String RESOURCE_REQUEST_URL_ADDED_ID_FORMAT = "http://%s:%s/mwa/api/v2/resource-requests/%s";

	// TODO host,portはとりあえずハードコーディング
	private static final String HOST = "localhost";
	private static final String PORT = "8081";

	public void initialize() {
		// Register System Plugins
		//ActivityはInternalに移動したので以下は不要になる。
//		if (kBase instanceof IKnowledgeBaseInternal) {
//			((IKnowledgeBaseInternal) kBase).registerSystemTemplates(ResourceTaskDefinition.getTemplates());
//			((IKnowledgeBaseInternal) kBase).registerSystemProviderTypes(ResourceTaskDefinition.getProviderTypes());
//		}
		logger.info("ResourceManager started!!");
	}

	public void destroy() {

	}

//  ActivityはInternalに移動したので以下は不要になる。
//	@Autowired
//	@Qualifier("knowledgeBase")
//	public void setKnowledgeBase(IKnowledgeBase kBase) {
//		this.kBase = kBase;
//	}

	@Autowired
	@Qualifier("eventManager")
	public void setEventManager(IEventManager eventManager) {
		this.eventManager = eventManager;
	}

	private String getResourceUrl(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlのパスまで生成
		String url = getResourceUrl();
		// queryを生成
		String query = RestUtils.createQuery(sorts, filters, offset, limit);
		// queryがnullでなければ、urlと結合する
		if (null != query) {
			url = url + "?" + query;
		}
		return url;
	}

	private String getResourceUrl() {
		return String.format(RESOURCE_URL_FORMAT, HOST, PORT);
	}

	private String getResourceUrl(String id) {
		return String.format(RESOURCE_URL_ADDED_ID_FORMAT, HOST, PORT, id);
	}

	private String getResourceRequestUrl(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlのパスまで生成
		String url = getResourceRequestUrl();
		// queryを生成
		String query = RestUtils.createQuery(sorts, filters, offset, limit);
		// queryがnullでなければ、urlと結合する
		if (null != query) {
			url = url + "?" + query;
		}
		return url;
	}

	private String getResourceRequestUrl() {
		return String.format(RESOURCE_REQUEST_URL_FORMAT, HOST, PORT);
	}

	private String getResourceRequestUrl(String id) {
		return String.format(RESOURCE_REQUEST_URL_ADDED_ID_FORMAT, HOST, PORT, id);
	}

	public ResourceModel getResource(String id) {
		// urlを取得
		String url = getResourceUrl(id);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceModel model = (ResourceModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceModel.class);
		return model;
	}

	public ResourceCollection getResources(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getResourceUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceCollection models = (ResourceCollection) RestUtils.convertToObj(restClient.getResponseBody(), ResourceCollection.class);
		return models;
	}

	protected ResourceModel getResource(String holder, String resourceType) {
		List<String> filters = new ArrayList<>();
		filters.add("holder" + FilterOperatorEnum.EQUAL.toSymbol() + holder);
		filters.add("type" + FilterOperatorEnum.EQUAL.toSymbol() + resourceType);
		ResourceCollection models = this.getResources(null, filters, null, null);
		return (models != null && models.getCount() > 0)
				? models.first()
					: null;
	}

	public ResourceModel addResource(ResourceModel model) {
		// urlを取得
		String url = getResourceUrl();
		// LocationModelをJson形式に変換
		String json = RestUtils.convertToJson(model);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.POST.name(), url, json);
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceModel result = (ResourceModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceModel.class);

		return result;
	}

	public ResourceModel updateResource(ResourceModel model) {
		// urlを取得
		String url = getResourceUrl(model.getId());
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.PUT.name(), url, RestUtils.convertToJson(model));
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		ResourceModel result = (ResourceModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceModel.class);

		return result;
	}

	@Override
	public ResourceModel deleteResource(String id) {
		// urlを取得
		String url = getResourceUrl(id);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		ResourceModel result = (ResourceModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceModel.class);
		logger.info("deleteResource: id=" + id);
		return result;
	}

	@Override
	public ResourceCollection deleteResources(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getResourceUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceCollection models = (ResourceCollection) RestUtils.convertToObj(restClient.getResponseBody(), ResourceCollection.class);
		return models;
	}

	@Override
	public ResourceRequestModel addResourceRequest(ResourceRequestModel model) {

		if (model.getCost() == null || model.getCost().getType() == null || model.getCost().getValues() == null) {
			logger.error("invalid cost");
			throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_REQUEST_COST);
		}
		if (model.getOccupantId() == null || model.getOccupantName() == null) {
			logger.error("invalid occupant info");
			throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_REQUEST_OCCUPANT);
		}
		if (model.getRequestResourceId() != null && model.getRequestResourceId().contains("@")) {
			String[] val = model.getRequestResourceId().split("@");
			try {
				val[0] = UriUtils.percentDecode(val[0]).replace("%40", "@");
			} catch (UnsupportedEncodingException e) {
				logger.error("Invalid resource: " + val[0] + ", " + val[1]);
			}
			ResourceModel resource = this.getResource(val[0], val[1]);
			if (resource == null) {
				logger.error("Resource not found: holder=" + val[0] + ", type=" + val[1]);
				throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
			}
			model.setRequestResourceId(resource.getId());
		}
		// urlを取得
		String url = getResourceRequestUrl();
		// LocationModelをJson形式に変換
		String json = RestUtils.convertToJson(model);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.POST.name(), url, json);
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceRequestModel result = (ResourceRequestModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceRequestModel.class);
		this.eventManager.addEvent(this.createResourceUpdateEvent(result.getRequestResourceId()));
		return result;
	}

	@Override
	public ResourceRequestModel updateResourceRequest(ResourceRequestModel model) {
		// urlを取得
		String url = getResourceRequestUrl(model.getId());
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.PUT.name(), url, RestUtils.convertToJson(model));
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		ResourceRequestModel result = (ResourceRequestModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceRequestModel.class);
		this.eventManager.addEvent(this.createResourceUpdateEvent(result.getRequestResourceId()));
		return result;
	}

	@Override
	public ResourceRequestModel getResourceRequest(String id) {
		// urlを取得
		String url = getResourceRequestUrl(id);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceRequestModel model = (ResourceRequestModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceRequestModel.class);
		return model;
	}

	@Override
	public ResourceRequestCollection getResourceRequests(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getResourceRequestUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceRequestCollection models = (ResourceRequestCollection) RestUtils.convertToObj(restClient.getResponseBody(), ResourceRequestCollection.class);
		return models;
	}

	@Override
	public ResourceRequestModel deleteResourceRequest(String id) {
		// urlを取得
		String url = getResourceRequestUrl(id);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		ResourceRequestModel result = (ResourceRequestModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceRequestModel.class);

		this.eventManager.addEvent(this.createResourceUpdateEvent(result.getRequestResourceId()));
		return result;
	}
	
	@Override
	public ResourceRequestModel deleteResourceRequest(String id, Boolean resourceDeletionFlag) {
		// urlを取得
		String url = getResourceRequestUrl(id);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");	//TODO: resourceDeletionFlag
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		ResourceRequestModel result = (ResourceRequestModel) RestUtils.convertToObj(restClient.getResponseBody(), ResourceRequestModel.class);

		this.eventManager.addEvent(this.createResourceUpdateEvent(result.getRequestResourceId()));
		return result;
	}

	@Override
	public ResourceRequestCollection deleteResourceRequests(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getResourceRequestUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ResourceRequestCollection models = (ResourceRequestCollection) RestUtils.convertToObj(restClient.getResponseBody(), ResourceRequestCollection.class);
		return models;
	}

	public void tryDispatch(String threadId, ResourceModel resource) {
		if (resource == null) {
			throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
		}

		ResourceModel remainResource = new ResourceModel(resource);
		{
			// 残りのリソースを算出
			List<String> filters = new ArrayList<>();
			filters.add("assignedResourceId" + FilterOperatorEnum.EQUAL.toSymbol() + resource.getId());
			ResourceRequestCollection requests = this.getResourceRequests(null, filters, null, null);
			if (requests != null) {
				for (ResourceRequestModel request : requests.getModels()) {
					try {
						remainResource.minus(request.getCost());
					} catch (MwaError e) {
						if (e.is(MWARC.OPERATION_FAILED_RESOURCE_SHORTAGE)) {
							logger.warn("Invalid resource allocation is detected. Wait release of resource(id=" + resource.getId() + ")");
							return;
						}
						throw e;
					}
				}
			}
		}

		{
			// 優先度順に割り当てを試みる
			List<String> sorts = new ArrayList<>();
			sorts.add("priority" + "+");
			sorts.add("createTime" + "+");
			List<String> filters = new ArrayList<>();
			filters.add("resourceId" + FilterOperatorEnum.EQUAL.toSymbol() + resource.getId());
			filters.add("exclusionId" + FilterOperatorEnum.EQUAL.toSymbol() + "");
			filters.add("assignedResourceId" + FilterOperatorEnum.EQUAL.toSymbol() + "");

			ResourceRequestCollection requests = this.getResourceRequests(sorts, filters, null, null);
			if (requests != null) {
				for (ResourceRequestModel request : requests.getModels()) {
					String before = remainResource.toString();
					try {
						remainResource.minus(request.getCost());
						logger.info("Reserved: occupant=" + request.getOccupantId() + ", before=" + before + ", after=" + remainResource.toString());
					} catch (MwaError e) {
						if (e.is(MWARC.OPERATION_FAILED_RESOURCE_SHORTAGE)) {
							logger.info("resoruce shortage! Wait release of resource(resource=" + resource.getId() + ", request= " + request.getId() + ")");
							break;
						}
						throw e;
					}
					request.setAssignedResourceId(resource.getId());
					this.eventManager.addEvent(createReservedEventFromRequest(request));
					this.updateResourceRequest(request);
				}
			}
		}
	}

	protected EventModel createErrorEventFromRequest(ResourceRequestModel request, MWARC rc) {
		EventModel event = new EventModel();
		event.setName(CommonEvent.NOTIFY_ERROR.getName());
		if (request.getReceiverId() != null)
			event.setTargetId(request.getReceiverId());
		else
			event.setTargetId(request.getOccupantId());
		event.setTargetType(EventTargetType.ACTIVITY_INSTANCE.name());
		java.util.Map<String, Object> params = new java.util.HashMap<>();
		params.put(PresetParameter.ErrorCode.name(), rc.code());
		event.setParams(params);
		return event;
	}

	protected EventModel createReservedEventFromRequest(ResourceRequestModel request) {
		EventModel event = new EventModel();
		event.setName(ResourceEvent.NOTIFY_RESERVED.getName());
		if (request.getReceiverId() != null)
			event.setTargetId(request.getReceiverId());
		else
			event.setTargetId(request.getOccupantId());
		event.setTargetType(EventTargetType.ACTIVITY_INSTANCE.name());
		return event;
	}

	protected EventModel createResourceUpdateEvent(String resourceId) {
		EventModel event = null;

		List<String> filters = new ArrayList<>();
		filters.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + ResourceEvent.NOTIFY_RESOURCE_UPDATE.getName());
		filters.add("targetId" + FilterOperatorEnum.EQUAL.toSymbol() + resourceId);
		// Eventの重複登録を回避
		if (this.eventManager.getEvents(null, filters, null, null).getCount() > 0) {
			event = null;
		} else {
			event = new EventModel();
			event.setName(ResourceEvent.NOTIFY_RESOURCE_UPDATE.getName());
			event.setTargetId(resourceId);
			event.setTargetType(EventTargetType.RESOURCE.name());
		}

		return event;
	}

	public void tryDispatch(String threadId, String resourceId, String eventType) {
		ResourceModel resource = this.getResource(resourceId);
		if (resource == null) {
			logger.error("Resource(id=" + resourceId + ") not found.");
			{
				// Errorを通知。Subscriberにイベント発行する感じにしたい
				List<String> filters = new ArrayList<>();
				filters.add("resourceId" + FilterOperatorEnum.EQUAL.toSymbol() + resourceId);
				ResourceRequestCollection requests = this.getResourceRequests(null, filters, null, null);
				if (requests != null) {
					for (ResourceRequestModel request : requests.getModels()) {
						this.eventManager.addEvent(createErrorEventFromRequest(request, MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND));
					}
				}
			}
			throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
		}
		this.tryDispatch(threadId, resource);
	}

	public void tryDispatchAll(String threadId, Long intervalSec) {
		// resourceのqueueを走査していく感じ？
		// getTopPriorityRequest
		// getTopPriorityResource
		ResourceCollection resources = this.getResources(null, null, null, null);
		for (ResourceModel resource : resources.getModels()) {
			this.tryDispatch(threadId, resource);
		}
	}

	@Override
	public ResourceGroupModel addResourceGroup(ResourceGroupModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceGroupModel updateResourceGroup(ResourceGroupModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceGroupModel deleteResourceGroup(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceGroupCollection deleteResourceGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceGroupModel getResourceGroup(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceGroupCollection getResourceGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// TODO Auto-generated method stub
		return null;
	}
}
