package com.sony.pro.mwa.resource.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.enumeration.ResourceGroupType;
import com.sony.pro.mwa.model.resource.ResourceGroupContentCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupContentModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.ColumnUtils;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;
import com.sony.pro.mwa.resource.repository.database.query.ResourceQuery.COLUMN;

public class ResourceGroupContentQuery extends Query<ResourceGroupContentModel, ResourceGroupContentCollection, QueryCriteria> {
    public enum COLUMN {
        ID("i.resource_id", "id", UUID.class, false),
        PARENT_ID("i.resource_parent_id", "parentId", UUID.class, false),
        TYPE("i.resource_type", "type", String.class, true),
        NAME("i.resource_name", "name", String.class, true),
        VALUES("i.resource_values", "values", String.class, false),
        HOLDER("i.resource_holder", "holder", String.class, true),
        HOLDER_TYPE("i.resource_holder_type", "holderType", String.class, true),
        CREATE_TIME("i.resource_create_time", "createTime", Timestamp.class, true),
        UPDATE_TIME("i.resource_update_time", "updateTime", Timestamp.class, true),
        GROUP_ID_1("k.resource_group_id", "groupId1", UUID.class, false),
        GROUP_TYPE_1("k.resource_group_type", "groupType1", String.class, true),
        ;
        
        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String WITH_RECURSIVE_CLAUSE = "";

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " + 
            "mwa.resource AS i "+
            	"LEFT JOIN mwa.resource_group_content AS j ON i.resource_id = j.resource_group_content_resource_id "+
            	"LEFT JOIN mwa.resource_group AS k ON j.resource_group_content_resource_group_id = k.resource_group_id "+
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        for (COLUMN enumColumn : COLUMN.values()) {
            map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }

    public ResourceGroupContentQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }
    
    @Override
    protected RowMapper<ResourceGroupContentModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<ResourceGroupContentModel> {
        public ResourceGroupContentModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        	ResourceGroupContentModel model = new ResourceGroupContentModel();
        	model.setId(rs.getString(COLUMN.ID.alias));
        	model.setParentId(rs.getString(COLUMN.PARENT_ID.alias));
        	model.setName(rs.getString(COLUMN.NAME.alias));
        	model.setType(rs.getString(COLUMN.TYPE.alias));
        	model.setValues((Map)Converter.xmlToMap(rs.getString(COLUMN.VALUES.alias)));
        	model.setHolder(rs.getString(COLUMN.HOLDER.alias));
        	model.setHolderType(rs.getString(COLUMN.HOLDER_TYPE.alias));
        	model.setCreateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.CREATE_TIME.alias));
        	model.setUpdateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.UPDATE_TIME.alias));
        	{
	        	String group1 = rs.getString(COLUMN.GROUP_ID_1.alias);
	        	String groupType1 = rs.getString(COLUMN.GROUP_TYPE_1.alias);
	        	
	        	Map<ResourceGroupType, String> groupIds = new HashMap<>();
	        	
	        	if (group1 != null) groupIds.put(ResourceGroupType.valueOf(groupType1), group1);
	        	model.setResourceGroupIds(groupIds);
        	}
        	return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }
}
