package com.sony.pro.mwa.resource;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.UUIDUtils;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.ResourceGroupType;
import com.sony.pro.mwa.enumeration.event.EventPriority;
import com.sony.pro.mwa.enumeration.event.EventTargetType;
import com.sony.pro.mwa.enumeration.resource.ResourceEvent;
import com.sony.pro.mwa.enumeration.resource.ResourceEventParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.event.EventModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupContentCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupContentModel;
import com.sony.pro.mwa.model.resource.ResourceGroupModel;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestEntryCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestEntryModel;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.resource.repository.ResourceDao;
import com.sony.pro.mwa.resource.repository.ResourceGroupContentDao;
import com.sony.pro.mwa.resource.repository.ResourceGroupDao;
import com.sony.pro.mwa.resource.repository.ResourceRequestDao;
import com.sony.pro.mwa.resource.repository.ResourceRequestEntryDao;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.utils.UriUtils;

@Component
public class ResourceManagerImpl implements IResourceManager {

	private final static MwaLogger logger = MwaLogger.getLogger(ResourceManagerImpl.class);

//	private IKnowledgeBase kBase;
	private IEventManager eventManager;
	private ResourceDao resourceDao;
	private ResourceGroupDao resourceGroupDao;
	private ResourceGroupContentDao resourceGroupContentDao;
	private ResourceRequestDao resourceRequestDao;
	private ResourceRequestEntryDao resourceRequestEntryDao;

	public void initialize() {
		logger.info("ResourceManager started!!");
	}
	public void destroy() {

	}

	@Autowired
	@Qualifier("resourceDao")
	public void setResourceDao(ResourceDao dao) {
		this.resourceDao = dao;
	}

	@Autowired
	@Qualifier("resourceRequestDao")
	public void setResourceRequestDao(ResourceRequestDao dao) {
		this.resourceRequestDao = dao;
	}

	@Autowired
	@Qualifier("resourceRequestEntryDao")
	public void setResourceRequestEntryDao(ResourceRequestEntryDao dao) {
		this.resourceRequestEntryDao = dao;
	}

	@Autowired
	@Qualifier("resourceGroupDao")
	public void setResourceGroupDao(ResourceGroupDao dao) {
		this.resourceGroupDao = dao;
	}

	@Autowired
	@Qualifier("resourceGroupContentDao")
	public void setResourceGroupContentDao(ResourceGroupContentDao dao) {
		this.resourceGroupContentDao = dao;
	}

//	@Autowired
//	@Qualifier("knowledgeBase")
//	public void setKnowledgeBase(IKnowledgeBase kBase) {
////		this.kBase = kBase;
//	}

	@Autowired
	@Qualifier("eventManager")
	public void setEventManager(IEventManager eventManager) {
		this.eventManager = eventManager;
	}

	public ResourceModel getResource(String id) {
		ResourceModel model;
		try {
			model = this.resourceDao.getModel(id, null);
		} catch (Throwable e) {
			logger.error("getResource: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (model == null)
			throw new MwaInstanceError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);

		return model;
	}
	public ResourceCollection getResources(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceCollection models;
		try {
			models = this.resourceDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getResources: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return models;
	}
	protected ResourceModel getResource(String holder, String resourceType) {
		List<String> filters = new ArrayList<>();
		filters.add("holder" + FilterOperatorEnum.EQUAL.toSymbol() + holder);
		filters.add("type" + FilterOperatorEnum.EQUAL.toSymbol() + resourceType);
		ResourceCollection models = this.getResources(null, filters, null, null);
		return (models != null && models.getCount() > 0) ? models.first() : null;
	}

	protected ResourceModel getResourceByName(String name) {
		List<String> filters = new ArrayList<>();
		filters.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + name);
		ResourceCollection models = this.getResources(null, filters, null, null);
		return (models != null && models.getCount() > 0) ? models.first() : null;
	}
	//★QueueはParentIdを使った木構造で表現
	public ResourceModel addResource(ResourceModel model) {
		if (model.getId() == null)
			model.setId(UUID.randomUUID().toString());
		ResourceModel result = this.resourceDao.addModel(model);
		return result;
	}

	public ResourceModel updateResource(ResourceModel model) {
		ResourceModel result = this.resourceDao.updateModel(model);
		return result;
	}

	@Override
	public ResourceModel deleteResource(String id) {
		ResourceModel result = this.resourceDao.deleteModel(this.getResource(id));
		if (result != null) logger.info("deleteResource: id=" + result.getId());
		return result;
	}

	@Override
	public ResourceModel deleteResourceIfNotUsed(String id) {
		ResourceModel result = null;
		ResourceRequestEntryCollection entries = this.getResourceRequestEntries(
				Arrays.<String>asList("resourceId" + FilterOperatorEnum.EQUAL.toSymbol() + id
				+ ","	//ORで条件つなぐ
				+ "assignedResourceId" + FilterOperatorEnum.EQUAL.toSymbol() + id ), 
				Arrays.<String>asList(), null, null);
		if (entries == null || entries.getModels() == null || entries.getModels().isEmpty()) {
			//resource参照しているrequestが0なのでresourceを削除
			result = this.resourceDao.deleteModel(this.getResource(id));
			if (result != null) logger.info("deleteResource: id=" + result.getId());
		} else {
			//do nothing
			String msg = "Can't delete resourc=" + id + ", some resource requests remain.";
			logger.info(msg);
			throw new MwaError(MWARC.OPERATION_FAILED, null, msg);
		}
		
		return result;
	}
	@Override
	public ResourceCollection deleteResources(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceCollection models = this.resourceDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		if (models != null) {
			for (ResourceModel model : models.getModels()) {
				this.deleteResource(model.getId());
			}
		}
		return models;
	}

	@Override
	public ResourceGroupModel getResourceGroup(String id) {
		ResourceGroupModel model;
		try {
			model = this.resourceGroupDao.getModel(id, null);
		} catch (Throwable e) {
			logger.error("getResourceGroup: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (model == null)
			throw new MwaInstanceError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
		else {
			ResourceGroupContentCollection contents = this.getResourceGroupContents(id);
			List<String> ids = new ArrayList<>();
			for (ResourceGroupContentModel content : contents.getModels()) {
				ids.add(content.getId());
			}
			if (contents != null) {
				model.setResourceIds(ids);
			}
		}

		return model;
	}
	@Override
	public ResourceGroupCollection getResourceGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceGroupCollection models;
		try {
			models = this.resourceGroupDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getResourceGroups: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		for (ResourceGroupModel model : models.getModels()) {
			if (model == null)
				throw new MwaInstanceError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
			else {
				ResourceGroupContentCollection contents = this.getResourceGroupContents(model.getId());
				List<String> ids = new ArrayList<>();
				for (ResourceGroupContentModel content : contents.getModels()) {
					ids.add(content.getId());
				}
				if (contents != null) {
					model.setResourceIds(ids);
				}
			}
		}
		return models;
	}
	protected ResourceGroupModel getResourceGroupByName(String name) {
		List<String> filters = new ArrayList<>();
		filters.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + name);
		ResourceGroupCollection models = this.getResourceGroups(null, filters, null, null);
		return (models != null && models.getCount() > 0) ? models.first() : null;
	}

	@Override
	public ResourceGroupModel addResourceGroup(ResourceGroupModel model) {
		if (model.getId() == null)
			model.setId(UUID.randomUUID().toString());
		if (model.getType() == null)
			model.setType(ResourceGroupType.GROUP.name());
		ResourceGroupModel result = this.resourceGroupDao.addModel(model);
		return result;
	}
	@Override
	public ResourceGroupModel updateResourceGroup(ResourceGroupModel model) {
		if (model.getType() == null)
			model.setType(ResourceGroupType.GROUP.name());
		ResourceGroupModel result = this.resourceGroupDao.updateModel(model);
		return result;
	}
	@Override
	public ResourceGroupModel deleteResourceGroup(String id) {
		ResourceGroupModel result = this.resourceGroupDao.deleteModel(this.getResourceGroup(id));
		logger.info("deleteResourceGroup: id=" + id);
		return result;
	}
	@Override
	public ResourceGroupCollection deleteResourceGroups(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceGroupCollection models = this.resourceGroupDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		if (models != null) {
			for (ResourceGroupModel model : models.getModels()) {
				this.deleteResourceGroup(model.getId());
			}
		}
		return models;
	}

	@Override
    @Transactional(readOnly=false, rollbackFor=Exception.class)
	public ResourceRequestModel addResourceRequest(ResourceRequestModel model) {
		//IDがnullなら割り当て
		if (model.getId() == null)
			model.setId(UUID.randomUUID().toString());
		//Costをチェック
		if (model.getResourceRequestEntryList() == null || model.getResourceRequestEntryList().isEmpty()) {
			logger.error("invalid cost");
			throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_REQUEST_COST);
		}
		//使用者をチェック
		if (model.getOccupantId() == null) {
			logger.error("invalid occupant info: id=" + model.getOccupantId() + ", name=" + model.getOccupantName());
			throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_REQUEST_OCCUPANT);
		}
		//使用するResourceのIDをチェック
		if (model.getRequestResourceId() != null && model.getRequestResourceId().contains("@")) {
			String[] val = model.getRequestResourceId().split("@");
			try {
				val[0] = UriUtils.percentDecode(val[0]).replace("%40", "@");
			} catch (UnsupportedEncodingException e) {
				logger.error("Invalid resource: " + val[0] + ", " + val[1]);
			}
			ResourceModel resource = this.getResource(val[0], val[1]);
			if (resource == null) {
				logger.error("Resource not found: holder=" + val[0] + ", type=" + val[1]);
				throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
			}
			model.setRequestResourceId(resource.getId());
		}
		if (model.getRequestResourceId() != null && !UUIDUtils.isValid(model.getRequestResourceId())) {
			ResourceModel resourceModel = this.getResourceByName(model.getRequestResourceId());
			if (resourceModel != null) {
				model.setRequestResourceId(resourceModel.getId());
			} else {
				ResourceGroupModel group = this.getResourceGroupByName(model.getRequestResourceId());
				if (group != null) {
					model.setRequestResourceId(group.getId());
				} else {
					logger.error("Resource not found: resourceId=" + model.getRequestResourceId());
					throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
				}
			}
		}
		for (ResourceRequestEntryModel entry : model.getResourceRequestEntryList()) {
			//HolderからResourceを解決する
			if (entry.getResourceId() == null && entry.getResourceHolder() != null) {
				List<String> filters = new ArrayList<>();
				filters.add("holder" + FilterOperatorEnum.EQUAL.toSymbol() + entry.getResourceHolder());
				filters.add("type" + FilterOperatorEnum.EQUAL.toSymbol() + entry.getCost().getType());

				List<ResourceModel> resources = this.getResources(null, filters, null, null).getModels();
				if (resources != null && !resources.isEmpty()) {
					//Holderから解決したresourceをセット
					entry.setResourceId(resources.get(0).getId());
				}
			}
			if (entry.getResourceId() == null) {
				String msg = "Cant't resolve resourceId: resourceRequest=" + entry.getResourceRequestId() +  ", resourceId=" + entry.getResourceId() +  ", cost.type=" + entry.getCost().getType() + ", holder=" + entry.getResourceHolder();
				logger.error(msg);
				throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
			}
		}

		//QueueIdを自動解決
		model.setQueueId(this.resolveQueueId(model));

		ResourceRequestModel result = this.resourceRequestDao.addModel(model);
		for (ResourceRequestEntryModel entry : model.getResourceRequestEntryList()) {

			entry.setResourceRequestId(model.getId());
			this.resourceRequestEntryDao.addModel(entry);
		}
		logger.info("Added a new resource request: " + model);
		EventModel event = this.createQueueEvent(result.getQueueId(), ResourceEvent.NOTIFY_RESOURCE_REQUEST_ADDITION);
		if (event != null) 
			this.eventManager.addEvent(event);
		return result;
	}
	@Override
	public ResourceRequestModel updateResourceRequest(ResourceRequestModel model) {
        if (model == null) 
            return null;
		ResourceRequestModel result = this.updateResourceRequestWithoutEvent(model);
		EventModel event = this.createQueueEvent(result.getQueueId(), ResourceEvent.NOTIFY_RESOURCE_REQUEST_UPDATE);
		if (event != null) 
			this.eventManager.addEvent(event);
		logger.info("Updated a resource request: " + model);
		return result;
	}
    @Transactional(readOnly=false, rollbackFor=Exception.class)
	protected ResourceRequestModel updateResourceRequestWithoutEvent(ResourceRequestModel model) {
        if (model == null) 
            return null;
		ResourceRequestModel result = this.resourceRequestDao.updateModel(model);
		this.resourceRequestEntryDao.deleteModel(model.getId());
		for (ResourceRequestEntryModel entry : model.getResourceRequestEntryList()) {
			this.resourceRequestEntryDao.addModel(entry);
		}
		return result;
	}
	@Override
	public ResourceRequestModel getResourceRequest(String id) {
		ResourceRequestModel model;
		try {
			model = this.resourceRequestDao.getModel(id, null);
		} catch (Throwable e) {
			logger.error("getResourceRequest: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (model == null)
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		else {
			List<String> filters = new ArrayList<>();
			filters.add("resourceRequestId" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId());
			ResourceRequestEntryCollection entries = this.getResourceRequestEntries(null, filters, null, null);
			if (entries != null)
				model.setResourceRequestEntryList(entries.getModels());
		}

		return model;
	}

	@Override
	public ResourceRequestCollection getResourceRequests(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceRequestCollection models;
		try {
			models = this.resourceRequestDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getRequestResources: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (models != null) {
			for (ResourceRequestModel request : models.getModels()) {
				filters = new ArrayList<>();
				filters.add("resourceRequestId" + FilterOperatorEnum.EQUAL.toSymbol() + request.getId());
				ResourceRequestEntryCollection entries = this.getResourceRequestEntries(null, filters, null, null);
				if (entries != null)
					request.setResourceRequestEntryList(entries.getModels());
			}
		}
		return models;
	}
	public ResourceRequestCollection getResourceRequestsWithoutEntry(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceRequestCollection models;
		try {
			models = this.resourceRequestDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getRequestResources: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return models;
	}
	@Override
	public ResourceRequestModel deleteResourceRequest(String id) {
		return deleteResourceRequest(id, false);
	}
	@Override
	public ResourceRequestModel deleteResourceRequest(String id, Boolean resourceDeletionFlag) {
		ResourceRequestModel model = this.getResourceRequest(id);
		ResourceRequestModel result = this.resourceRequestDao.deleteModel(model);
		try {
			if (model.getAssignedFlag() && model.getResourceRequestEntryList() != null) {
				for (ResourceRequestEntryModel entry : model.getResourceRequestEntryList()) {
					ResourceModel after = this.getRemainResource(entry.getAssignedResourceId());
					ResourceModel before = new ResourceModel(after);
					before.minus(entry.getCost());
					String message = "Release resource: request[" +id+"], resource["+entry.getAssignedResourceId()+"], occupant["+model.getOccupantId()+"]={" + before.toString() + " -> " + after.toString() + "}";
					logger.info(message);
					if (resourceDeletionFlag != null && resourceDeletionFlag) {
						ResourceRequestEntryCollection entries = this.getResourceRequestEntries(
								//resourceIdを要求しているrequest、または、assignされているrequestの有無を確認（resourceの抽象化ケースは対象外）
								Arrays.<String>asList("resourceId" + FilterOperatorEnum.EQUAL.toSymbol() + entry.getAssignedResourceId()
								+ ","	//ORで条件つなぐ
								+ "assignedResourceId" + FilterOperatorEnum.EQUAL.toSymbol() + entry.getAssignedResourceId() ), 
								Arrays.<String>asList(), null, null);
						if (entries != null && entries.getModels() != null && entries.getModels().isEmpty()) {
							//resource参照しているrequestが0なのでresourceを削除
							this.deleteResource(entry.getAssignedResourceId());
						}

					}
				}
			}
		} catch (Exception e) {
			logger.warn("Excpetion occured while logging resource consuming situation. e=" + e.toString(), e);
		}
		EventModel event = this.createQueueEvent(result.getQueueId(), ResourceEvent.NOTIFY_RESOURCE_REQUEST_DELETION);
		if (event != null) 
			this.eventManager.addEvent(event);

		return result;
	}
	@Override
	public ResourceRequestCollection deleteResourceRequests(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceRequestCollection models = this.resourceRequestDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		if (models != null) {
			for (ResourceRequestModel model : models.getModels()) {
				this.deleteResourceRequest(model.getId());
			}
		}
		return models;
	}

	public ResourceGroupContentCollection getResourceGroupContents(String groupId) {
		ResourceGroupContentCollection models;
		try {
			List<String> filters = new ArrayList<>();
			filters.add("groupId1" + FilterOperatorEnum.EQUAL.toSymbol() + groupId);
			filters.add("groupType1" + FilterOperatorEnum.EQUAL.toSymbol() + ResourceGroupType.GROUP.name());
			models = this.resourceGroupContentDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filters, null, null, null));
		} catch (Throwable e) {
			logger.error("getResourceRequest: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (models == null) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}

		return models;
	}

	public ResourceGroupContentCollection getResourceGroupContents(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceGroupContentCollection models;
		try {
			models = this.resourceGroupContentDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getRequestResources: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return models;
	}

	protected ResourceRequestEntryCollection getResourceRequestEntries(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ResourceRequestEntryCollection models;
		try {
			models = this.resourceRequestEntryDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getResources: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		return models;
	}
	protected ResourceRequestEntryModel addResourceRequestEntry(ResourceRequestEntryModel model) {
		ResourceRequestEntryModel result = this.resourceRequestEntryDao.addModel(model);
		return result;
	}

	protected ResourceRequestEntryModel updateResourceRequestEntry(ResourceRequestEntryModel model) {
		ResourceRequestEntryModel result = this.resourceRequestEntryDao.updateModel(model);
		return result;
	}

	protected void deleteResourceRequestEntries(String requestId) {
		this.resourceRequestEntryDao.deleteModel(requestId);
	}

	protected EventModel createErrorEventFromRequest(ResourceRequestModel request, MWARC rc) {
		EventModel event = new EventModel();
		event.setName(CommonEvent.NOTIFY_ERROR.getName());
		if (request.getReceiverId() != null)
			event.setTargetId(request.getReceiverId());
		else
			event.setTargetId(request.getOccupantId());
		event.setTargetType(EventTargetType.ACTIVITY_INSTANCE.name());
		java.util.Map<String, Object> params = new java.util.HashMap<>();
		params.put(PresetParameter.ErrorCode.name(), rc.code());
		event.setParams(params);
		event.setPriority(EventPriority.RESOURCE_DEFAULT.getValue());
		event.setExclusionId(request.getOccupantId());
		return event;
	}

	protected EventModel createReservedEventFromRequest(ResourceRequestModel request) {
		EventModel event = new EventModel();
		event.setName(ResourceEvent.NOTIFY_RESERVED.getName());
		if (request.getReceiverId() != null)
			event.setTargetId(request.getReceiverId());
		else
			event.setTargetId(request.getOccupantId());
		event.setTargetType(EventTargetType.ACTIVITY_INSTANCE.name());
		Map<String, Object> params = new TreeMap<>();
		params.put(ResourceEventParameter.ASSIGNED_RESOURCE_KEY.name(), request.getMetadata().get("KEY"));
		params.put(ResourceEventParameter.ASSIGNED_RESOURCE_ID.name(), request.getAssignedResourceId());
		if (request.getAssignedResourceId() == null) {
			logger.error("Transaction Error might occurred. ResourceRequest may be invalid.");
			throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
		}
		ResourceModel model = this.getResource(request.getAssignedResourceId());
		if (model != null) {
			params.put(ResourceEventParameter.ASSIGNED_RESOURCE_HOLDER_ID.name(), model.getHolder());
			params.put(ResourceEventParameter.ASSIGNED_RESOURCE_HOLDER_TYPE.name(), model.getHolderType());
		}
		event.setParams(params);
		event.setPriority(EventPriority.RESOURCE_DEFAULT.getValue());
		event.setExclusionId(request.getOccupantId());
		return event;
	}

	protected EventModel createQueueEvent(String queueId, ResourceEvent eventType) {
		EventModel event = null;
		if (queueId == null)
			return null;

		List<String> filters = new ArrayList<>();
		filters.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + eventType.getName());
		filters.add("targetId" + FilterOperatorEnum.EQUAL.toSymbol() + queueId);
		//Eventの重複登録を回避
		if ( this.eventManager.getEvents(null, filters, null, null).getCount() > 0) {
			event = null;
		} else {
			event = new EventModel();
			event.setName(eventType.getName());
			event.setTargetId(queueId);
			event.setTargetType(EventTargetType.QUEUE.name());
			event.setPriority(EventPriority.RESOURCE_DEFAULT.getValue());
			event.setExclusionId(queueId);
		}

		return event;
	}

	//Resourceの残量を算出する
	protected ResourceModel getRemainResource(String resourceId) {
		ResourceModel remainResource = this.getResource(resourceId);
		{
			//残りのリソースを算出
			List<String> filters = new ArrayList<>();
			filters.add("assignedResourceId" + FilterOperatorEnum.EQUAL.toSymbol() + resourceId);
			ResourceRequestEntryCollection entries = this.getResourceRequestEntries(null, filters, null, null);
			if (entries != null) {
				for (ResourceRequestEntryModel entry : entries.getModels()) {
					try {
						remainResource.minus(entry.getCost());
					} catch (MwaError e) {
						if (e.is(MWARC.OPERATION_FAILED_RESOURCE_SHORTAGE)) {
							logger.warn("Invalid resource allocation is detected. Wait release of resource(id=" + resourceId + ")");
							return null;
						}
						throw e;
					}
				}
			}
		}
		return remainResource;
	}

	//都度TopPriorityを取り直すことで、Resource状況の更新を次のTopPriority選択に反映させる
	protected ResourceRequestModel getTopPriorityRequest(String queueId) {
		//優先度順に割り当てを試みる
		List<String> sorts = new ArrayList<>();
		sorts.add("priority_0" + "+");
		sorts.add("priority_1" + "+");
		sorts.add("priority_2" + "+");
		sorts.add("priority_3" + "+");
		sorts.add("priority_4" + "+");
		sorts.add("createTime" + "+");

		List<String> filters = new ArrayList<>();
		filters.add("queueId" + FilterOperatorEnum.EQUAL.toSymbol() + queueId);		//queueの中にあるもおでpriority順に取り出し
		filters.add("assignedFlag" + FilterOperatorEnum.EQUAL.toSymbol() + false);	//アサインされたものを除外

		ResourceRequestModel request = this.getResourceRequests(sorts, filters, null, 1).first();
		return request;
	}

	//ここでは、QueueとResource(or Group)でGetする、どちらも包含した
	//Queueが指定されてればQueue上のResourceのみが対象になる
	protected List<? extends ResourceModel> getResourceOrGroup (String resourceId, String queueId) {
		{
			List<String> filters = new ArrayList<>();
			filters.add("id" + FilterOperatorEnum.EQUAL.toSymbol() + resourceId);
			if (queueId != null)
				filters.add("parentId" + FilterOperatorEnum.EQUAL.toSymbol() + queueId + ",id" + FilterOperatorEnum.EQUAL.toSymbol() + queueId);
			ResourceCollection resources = this.getResources(null, filters, null, null);
			if (resources != null && !resources.getModels().isEmpty())
				return resources.getModels();
		}

		//AとBに所属するcontentsを持ってくる仕組みが必要
		List<String> filters = new ArrayList<>();
		filters.add("groupId1" + FilterOperatorEnum.EQUAL.toSymbol() + resourceId);
		filters.add("groupType1" + FilterOperatorEnum.EQUAL.toSymbol() + ResourceGroupType.GROUP.name());
		if (queueId != null)
			filters.add("parentId" + FilterOperatorEnum.EQUAL.toSymbol() + queueId + ",id" + FilterOperatorEnum.EQUAL.toSymbol() + queueId);
		ResourceGroupContentCollection resources = this.getResourceGroupContents(null, filters, null, null);
		if (resources != null && !resources.getModels().isEmpty())
			return resources.getModels();

		return (resources != null) ? resources.getModels() : null;
	}

	//Request登録時
	//RequestからQueueを算出する
	//★Queueの変化がRequest側に反映されること(将来的な話とする)

	//Dispatch
	//変化があったQueueについて、再割り当てを試みる
	//あるQueueに対して、最優先のRequestを取得、
	//★同じTypeのResourceを複数Assignできない（要Occupantテーブルの見直し）

	//引数のQueueに対してDispatchを試みる(Resourceの再割り当てを試みる)
	protected void tryDispatch(String threadId, String queueId) {
		if (queueId == null) {
			throw new MwaError(MWARC.INVALID_INPUT_QUEUE_ID);
		}

		//resourceを優先度順に並び替え（とりあえずそのまま）
		//★都度Sort済みのListを取り直すことで、Resource状況の更新を次のTopPriority選択に反映させたい

		int maxRequestProcessing = 30;	//無限ループにならないように
		for (int i=0; i<maxRequestProcessing; i++) {
			//Queueに所属するTopPriorityのRequestを取得
			ResourceRequestModel request = getTopPriorityRequest(queueId);
			if (request == null)
				break;	//allocation不要

			{
				//Requestが要求するResourceの確保を１つずつこころみる
				ResourceRequestEntryModel lastEntry = null;

				//EntryのないResource要求はエラー扱いとする（Requestは必ずQueueに紐づけるべきだがEntryがないとQueueとの紐づけの根拠がない）
				if (request.getResourceRequestEntryList() == null || request.getResourceRequestEntryList().isEmpty()) {
					logger.warn("ResourceRequest (" + request.getId() + ") doesn't have entries, queueId=" + request.getQueueId() + ", occupantId=" + request.getOccupantId());
					{//★Transaction管理が不十分で、ユーザスレッドで一時的にEntry未登録状態はありえる。次回allocationで割り当て期待できるため、ここでは警告だけだして抜けるという対応
						//このRequestはエラーとする
						//logger.error("Request(" + request.getId() + ") caused exception. Removed from queue.");
						//this.eventManager.addEvent(createErrorEventFromRequest(request, MWARC.INVALID_INPUT_RESOURCE_REQUEST_COST));
						//this.deleteResourceRequest(request.getId());
					}
					break;
				}

				for (ResourceRequestEntryModel entry : request.getResourceRequestEntryList()) {
					lastEntry = entry;
					//Queueに属したResourceを取得
					List<? extends ResourceModel> candidates = this.getResourceOrGroup(entry.getResourceId(), queueId);

					if (candidates == null || candidates.isEmpty()) {
						logger.error("ResourceQueue (" + queueId + ") doesn't have resourceId=" + entry.getResourceId() + ", requestId=" + request.getId());
						//このRequestはエラーとする
						logger.error("Request(" + request.getId() + ") caused exception. Removed from queue.");
						
						EventModel event = this.createErrorEventFromRequest(request, MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
						if (event != null) 
							this.eventManager.addEvent(event);
						this.deleteResourceRequest(request.getId());
						break;
					}

					//resourceの優先度付はやってないので、確保できたものをさっさと使う
					for (ResourceModel candidate :  candidates) {
						//一旦、現在の使用量を算出
						ResourceModel remainResource = this.getRemainResource(candidate.getId());
						//OPERATION_FAILED_RESOURCE_SHORTAGEが例外で投げられます
						try {
							ResourceModel prev = new ResourceModel(remainResource);
							//assignを実施
							remainResource.minus(entry.getCost());
							//assignを実施
							entry.setAssignedResourceId(candidate.getId());
							logger.info("Reserve resource: request[" + request.getId() + "], resource[" + candidate.getId() + "], occupant[" + request.getOccupantId() + "]={" + prev.toString() + " -> " + remainResource.toString() + "}");
							//次のResourceを確保
							break;
						} catch (MwaError e) {
							if ( e.getMWARC().equals(MWARC.OPERATION_FAILED_RESOURCE_SHORTAGE) ) {
								//次のResourceを確保する
							} else {
								throw e;
							}
						}
					}
				}

				//全てのリソースが確保できれば登録
				if (request.getAssignedFlag()) {
					this.updateResourceRequestWithoutEvent(request);
					this.eventManager.addEvent(createReservedEventFromRequest(request));
				} else {
					if (lastEntry != null)
						logger.info("resource shortage: request=" + request.getId() + ", entry={resource=" + lastEntry.getResourceId() + ", cost=" + lastEntry.getCost().toString() + "}");
					else
						logger.info("resource shortage: request=" + request.getId());
					//Resourceは全てリリース(部分的に握るようなことはしない)
					request.releaseAllResources();
					//リソースが確保できなければ打ち切り
					break;
				}
			}
		}
	}

	//★これを呼び出す前に、request内容からqueueを解決する仕組みが必要
	public synchronized void tryDispatch(String threadId, String queueId, String eventType) {

		//Resourceの状況が変わったので割り当て見直し（UPDATEはマイナス方向もあるので、同じ処理でよいか要検討）
		if (ResourceEvent.NOTIFY_RESOURCE_REQUEST_ADDITION.isMatch(eventType)
				|| ResourceEvent.NOTIFY_RESOURCE_REQUEST_UPDATE.isMatch(eventType)
				|| ResourceEvent.NOTIFY_RESOURCE_REQUEST_DELETION.isMatch(eventType)
				) {
			if (queueId != null) {
				this.tryDispatch(threadId, queueId);
				return;
			}
			//ToDo:★ResourceGroupとQueueの変動イベントの関係を整理
			{
				//Queue変動EventなのにQueueが見つからない
				//エラーハンドリング
				logger.error("Queue (id=" + queueId + ") not found.");
				{
					//Errorを通知。Subscriberにイベント発行する感じにしたい
					List<String> filters = new ArrayList<>();
					filters.add("queueId" + FilterOperatorEnum.EQUAL.toSymbol() + queueId);
					ResourceRequestCollection requests = this.getResourceRequests(null, filters, null, null);
					if (requests != null) {
						for (ResourceRequestModel request : requests.getModels()) {
							EventModel event = this.createErrorEventFromRequest(request, MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
							if (event != null) 
								this.eventManager.addEvent(event);
						}
					}
				}
			}
		} else if (ResourceEvent.NOTIFY_RESOURCE_DELETION.isMatch(eventType)){
			//割り当てを奪う処理が必要
			//動的にdispatchすることは考えていないのでとりあえず保留

			//エラーハンドリングの前に、resourceが消えるケースがあるのでそのハンドリング
		} else {
			logger.warn("Unknown resource event: threadId=" + threadId + ", queueId=" + queueId + ", event=" + eventType);
		}
	}

	public void tryDispatchAll(String threadId, Long intervalSec) {
		//resourceのqueueを走査していく感じ？
		//getTopPriorityRequest
		//getTopPriorityResource

		List<String> filters = new ArrayList<>();
		filters.add("parentId" + FilterOperatorEnum.EQUAL.toSymbol() + "");			//parentIdがNullのものがQueueを構成するresource、それらを順次処理していく

		ResourceCollection queues = this.getResources(null, filters, null, null);
		for (ResourceModel queue : queues.getModels()) {
			this.tryDispatch(queue.getId(), threadId);
		}
	}

	private static class ArrayListCaseInsensitive extends ArrayList<String> {
		@Override
	    public boolean contains(Object o) {
	        String paramStr = (String)o;
	        for (String s : this) {
	            if (paramStr.equalsIgnoreCase(s)) return true;
	        }
	        return false;
	    }
		public boolean addAllOnlyNonDuplicate(Collection<? extends String> list) {
			boolean result = false;
			if (list == null || list.isEmpty())
				return false;
			for (String entry : list) {
				if (!this.contains(entry)) {
					this.add(entry);
					result = true;
				}
			}
			return result;
		}
	}

	//★Resource?Group？
	public List<String> getQueueIds(String resourceId) {
		//Resourceか、GroupのIDを指定して所属するResource群を取得
		List<? extends ResourceModel> resources = getResourceOrGroup(resourceId, null);
		List<String> result = new ArrayListCaseInsensitive();
		if (resources == null || resources.isEmpty())
			return null;
		for (ResourceModel resource : resources) {
			//Parentがいなければ
			if (resource.getParentId() == null) {
				//TopなのでQueueのIdとして結果リストに登録
				if (!result.contains(resource.getId())) {
					result.add(resource.getId());
				}
			} else {
				//Parentがいる場合、その親をたどるため再帰で実行
				((ArrayListCaseInsensitive) result).addAllOnlyNonDuplicate(getQueueIds(resource.getParentId()));
			}
		}
		return result;
	}
	public String resolveQueueId(ResourceRequestModel request) {
		String result = null;
		if (request == null)
			throw new MwaError(MWARC.INVALID_INPUT, null, "request is null");

		//resourceId(=groupId)からは所属する可能性のあるQueueのIdリストが算出できる(木構造をTopまでたどることで)
		//各Entryごとにリストが算出でき、全てのリストに含まれるQueueが候補のId。複数対象がある場合はどれでもOK(理想は消費状況見て振り分けだが。。)
		{
			List<String> queueIdCandidates = null;
			for (ResourceRequestEntryModel entry : request.getResourceRequestEntryList()) {
				//Entryが所属し得るQueueのIdを取得
				List<String> queueIds = this.getQueueIds(entry.getResourceId());
				if (queueIdCandidates == null) {
					//初回は特別
					queueIdCandidates = queueIds;
					continue;
				} else if (queueIds == null || queueIds.isEmpty()) {
					//do nothing, go next
				} else {
					List<String> newQueueIdCandidates = new ArrayListCaseInsensitive();
					for (String queueId : queueIds) {
						//他のEntryと共通のQueueIdであれば候補に残す
						if ( queueIdCandidates.contains(queueId) ) {
							newQueueIdCandidates.add(queueId);
						}
					}
					queueIdCandidates = newQueueIdCandidates;
				}

				if (queueIdCandidates == null || queueIdCandidates.isEmpty())
					break;	//探索打ち切り
			}
			//ここで候補の中から絞る必要ある(★とりあえず一番カウントが少ないQueueに割り振り（Queueなので空き状況の把握が難しい）、要改善)
			if (queueIdCandidates != null && !queueIdCandidates.isEmpty()) {
				Integer onQueueCount = null;
				result = queueIdCandidates.get(0);
				for (String queueId : queueIdCandidates) {
					/*
					List<String> filters = new ArrayList<>();
					filters.add("queueId" + FilterOperatorEnum.EQUAL.toSymbol() + queueId);			//parentIdがNullのものがQueueを構成するresource、それらを順次処理していく
					ResourceRequestCollection requests = this.getResourceRequestsWithoutEntry(null, filters, null, null);
					*/
					int count = this.resourceRequestDao.count("WHERE resource_request_queue_id = '" + queueId + "'");
					if (onQueueCount == null || onQueueCount > count) {
						onQueueCount = count;
						result = queueId;
						if (onQueueCount == 0)
							break;
					}
				}
			} else {
				String msg = "Can't find out resource queue to process request[" + request.getId() + "], occupant=" + request.getOccupantId() +  ".";
				for (ResourceRequestEntryModel entry : request.getResourceRequestEntryList()) {
					msg += "\n\t\tresource=" + entry.getResourceId() + ", cost=" + ((entry.getCost() != null) ? entry.getCost().toString() : "null");
				}
				logger.error(msg);
				throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_REQUEST_MISSING_QUEUE, null, msg);
			}
		}
		return result;
	}
}
