package com.sony.pro.mwa.resource.repository;

import com.sony.pro.mwa.model.resource.ResourceRequestEntryCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestEntryModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ResourceRequestEntryDao extends ModelBaseDao<ResourceRequestEntryModel, ResourceRequestEntryCollection> {
	public boolean deleteModel(String requestId);
}
