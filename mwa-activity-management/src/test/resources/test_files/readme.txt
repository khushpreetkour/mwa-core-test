
Activity開発者向けのUnitTest環境説明です。
Bundleとして登録する形だと、デバッグや修正確認が面倒なので、
OnMemoryのActivityManager/KnowledgeBaseを用いたテスト用環境を用意します。

- mwa-app-context.xml
	環境構築用のDI設定ファイル。「src/test/resources/」の直下においてください

- com.sony.pro.mwa.activity.test.UnitTestBaseForActivityManager
	テストコード用のベースクラス。
	上記設定ファイルを用いてActivityManager/KnowledgeBaseを構築します。
	テスト開始前に、this.initActivityManager(new MwaBundleImpl())を使って、Templateを登録してください。
	また、テストコードでActivityManagerを利用する際、this.getActivityManager() でインスタンスを取得してください。

	参考：mwa-activity-ht-approval
