package com.sony.pro.mwa.activity.repository;

import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.repository.ModelBaseDao;
import com.sony.pro.mwa.repository.query.QueryCriteria;

public interface ActivityInstanceDao extends ModelBaseDao<ActivityInstanceModel, ActivityInstanceCollection> {
    public ActivityInstanceModel addModel(ActivityInstanceModel model, boolean isExecutor);
	public long getFilteringCount(QueryCriteria criteria);
	public long getTotalCount(QueryCriteria criteria);
}
