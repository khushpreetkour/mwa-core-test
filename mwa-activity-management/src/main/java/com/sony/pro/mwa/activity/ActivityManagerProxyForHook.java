package com.sony.pro.mwa.activity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.internal.activity.IActivityManagerInternal;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.OperationResult;

public class ActivityManagerProxyForHook implements IActivityManagerInternal {

	IActivityManagerInternal activityManager;

	Map<String, Map<String, Object>> defaultParamMap;

	ActivityManagerProxyForHook(IActivityManagerInternal activityManager) {
		this.activityManager = activityManager;
		this.defaultParamMap = new HashMap<>();
	}

	public void setDefaultParam(String operation, Map<String, Object> defaultParams) {
		this.defaultParamMap.put(operation, defaultParams);
	}

	private Map<String, Object> getMergedParams(String operation, Map<String, Object> params) {
		Map<String, Object> mergedParams = new HashMap(this.defaultParamMap.get(CommonEvent.REQUEST_SUBMIT.getName()));
		if (params != null) {
			mergedParams.putAll(params);
		}
		return mergedParams;
	}

	@Override
	public OperationResult createInstance(String templateId, Map<String, Object> params) {
		return this.activityManager.createInstance(templateId, getMergedParams(CommonEvent.REQUEST_SUBMIT.getName(), params));
	}

	@Override
	public OperationResult createInstance(String templateName, String templateVersion, Map<String, Object> params) {
		return this.activityManager.createInstance(templateName, templateVersion, getMergedParams(CommonEvent.REQUEST_SUBMIT.getName(), params));
	}

	@Override
	public OperationResult operateActivity(String instanceId, String operation, Map<String, Object> params) {
		return this.activityManager.operateActivity(instanceId, operation, getMergedParams(operation, params));
	}

	@Override
	public OperationResult operateActivity(String parentId, String childName, String operation, Map<String, Object> params) {
		return this.activityManager.operateActivity(parentId, childName, operation, getMergedParams(operation, params));
	}

	@Override
	public ActivityInstanceModel getInstance(String instanceId) {
		return this.activityManager.getInstance(instanceId);
	}

	@Override
	public ActivityInstanceCollection getInstances(List<String> sortKey, List<String> filters, Integer offset, Integer limit) {
		return this.activityManager.getInstances(sortKey, filters, offset, limit);
	}

	@Override
	public ActivityInstanceModel getChildInstance(String parentInstanceId, String childName) {
		return this.activityManager.getChildInstance(parentInstanceId, childName);
	}

	@Override
	public ActivityInstanceCollection getChildInstances(String parentInstanceId) {
		return this.activityManager.getChildInstances(parentInstanceId);
	}

	@Override
	public ActivityInstanceModel deleteInstance(String instanceId) {
		return this.activityManager.deleteInstance(instanceId);
	}

	@Override
	public ActivityInstanceCollection deleteInstances(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		return this.activityManager.deleteInstances(sorts, filters, offset, limit);
	}

	@Override
	public void operateActivityAsync(String instanceId, String operation, Map<String, Object> params) {
		this.activityManager.operateActivityAsync(instanceId, operation, params);
	}

    @Override
    public OperationResult updateInstance(ActivityInstanceModel model) {
        return this.activityManager.updateInstance(model);
    }
}
