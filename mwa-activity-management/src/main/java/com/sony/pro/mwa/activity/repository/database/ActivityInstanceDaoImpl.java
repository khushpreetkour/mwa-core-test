package com.sony.pro.mwa.activity.repository.database;

import java.lang.management.ManagementFactory;
import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.activity.repository.ActivityInstanceDao;
import com.sony.pro.mwa.activity.repository.database.query.ActivityInstanceQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.internal.utils.InternalConverter;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/29
 * Time: 6:57
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ActivityInstanceDaoImpl extends DatabaseBaseDao<ActivityInstanceModel, ActivityInstanceCollection, QueryCriteria> implements ActivityInstanceDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	Integer LENGTH_DEVICE_RESPONSE_CODE = 256;
    Integer LENGTH_DEVICE_RESPONSE_DETAILS = 1024;

	public ActivityInstanceCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}
	public ActivityInstanceModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	public long getFilteringCount(QueryCriteria criteria) {
		return super.filteringCount(criteria);
	}

	public long getTotalCount(QueryCriteria criteria) {
		return super.totalCount(criteria);
	}

	private static final String INSERT_SQL =
			"INSERT INTO " +
					"mwa.activity_instance(" +
					"  activity_instance_id, activity_template_id, activity_instance_priority, activity_instance_status, activity_instance_status_details" +
					", activity_instance_device_response_code, activity_instance_device_response_details, activity_instance_progress" +
					", activity_instance_input_details, activity_instance_output_details" +
					", activity_instance_start_time, activity_instance_end_time, activity_instance_update_time" +
					", activity_instance_name, activity_instance_parent_instance_id, st_terminated, st_stable, st_error, st_cancel, activity_instance_create_time) " +
			"VALUES " +
					"(?, "
					+ "(SELECT activity_template_id FROM mwa.activity_template WHERE activity_template_name = ? and activity_template_version = ?), "
					+ "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

	private static final String UNIQUE_KEY = String.valueOf(ManagementFactory.getRuntimeMXBean().getStartTime());
	private static final String INSERT_SQL_WITH_EXECUTION_NODE =
	        "WITH insert AS (" + INSERT_SQL + "RETURNING activity_instance_id) "+
	        "INSERT INTO mwa.activity_instance_execution_node (activity_instance_id, execution_node, create_time) SELECT activity_instance_id, '"+UNIQUE_KEY+"', now() FROM insert";

	public ActivityInstanceModel addModel(final ActivityInstanceModel model) {
	    return addModel(model, false);
	}

	public ActivityInstanceModel addModel(final ActivityInstanceModel model, boolean isExecutor) {
		model.setCreateTime(System.currentTimeMillis());
		model.setUpdateTime(model.getCreateTime());
	    String insertSql;
	    if(isExecutor) {
            insertSql = INSERT_SQL_WITH_EXECUTION_NODE;
	    } else {
	        insertSql = INSERT_SQL;
	    }

		final long currentTime = System.currentTimeMillis();
		KeyHolder keyHolder = new GeneratedKeyHolder();

		{
			Map<String, Object> map = model.getOutputDetails();
			if (map == null) map = new java.util.HashMap<String, Object>();
			map.put("PersistentData", model.getPersistentData()); //★暫定実装
			model.setOutputDetails(map);
		}

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {

						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});

						DatabaseEnum database = DatabaseEnum.getDatabase(jdbcTemplate.getDataSource());

						int colNum = 1;

						ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getTemplateName());
						ps.setString(colNum++, model.getTemplateVersion());
                        if (model.getPriority() != null)
                            ps.setInt(colNum++, model.getPriority());
                        else
                            ps.setNull(colNum++, Types.INTEGER);
						ps.setString(colNum++, model.getStatus().getName());
						{
							if (model.getStatusDetails() != null && model.getStatusDetails().getResult() != null)
								ps.setString(colNum++, model.getStatusDetails().getResult().code());
							else
								ps.setNull(colNum++, Types.VARCHAR);
							if (model.getStatusDetails() != null && model.getStatusDetails().getDeviceResponse() != null)
								ps.setString(colNum++, StringUtils.substring(model.getStatusDetails().getDeviceResponse(), 0, LENGTH_DEVICE_RESPONSE_CODE));
							else
								ps.setNull(colNum++, Types.VARCHAR);
							if (model.getStatusDetails() != null && model.getStatusDetails().getDeviceResponseDetails() != null)
								ps.setString(colNum++, StringUtils.substring(model.getStatusDetails().getDeviceResponseDetails(), 0, LENGTH_DEVICE_RESPONSE_DETAILS));
							else
								ps.setNull(colNum++, Types.VARCHAR);
						}
						ps.setInt(colNum++, model.getProgress());

						if(DatabaseEnum.H2SQL.equals(database)){
							if (model.getInputDetails() != null) {
								try {
									String encodedStr = InternalConverter.mapToXml(model.getInputDetails());
									ps.setString(colNum++, encodedStr);
								} catch(Exception e) {
									logger.warn("Can't convert inputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.VARCHAR);
								}
							} else {
								ps.setNull(colNum++, Types.VARCHAR);
							}
							if (model.getOutputDetails() != null) {
								try {
									String encodedStr = InternalConverter.mapToXml(model.getOutputDetails());
									ps.setString(colNum++, encodedStr);
								} catch(Exception e) {
									logger.warn("Can't convert outputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.VARCHAR);
								}
							} else {
								ps.setNull(colNum++, Types.VARCHAR);
							}
						}else{
							if (model.getInputDetails() != null) {
								try {
									SQLXML sqlxml = conn.createSQLXML();
									String encodedStr = InternalConverter.mapToXml(model.getInputDetails());
									sqlxml.setString(encodedStr);
									ps.setSQLXML(colNum++, sqlxml);
								} catch(Exception e) {
									logger.warn("Can't convert inputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.SQLXML);
								}
							} else {
								ps.setNull(colNum++, Types.SQLXML);
							}
							if (model.getOutputDetails() != null) {
								try {
									SQLXML sqlxml = conn.createSQLXML();
									String encodedStr = InternalConverter.mapToXml(model.getOutputDetails());
									sqlxml.setString(encodedStr);
									ps.setSQLXML(colNum++, sqlxml);
								} catch(Exception e) {
									logger.warn("Can't convert outputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.SQLXML);
								}
							} else {
								ps.setNull(colNum++, Types.SQLXML);
							}
						}

						if (model.getStartTime() != null) {
							ps.setTimestamp(colNum++, new Timestamp(model.getStartTime()));
						} else {
							ps.setNull(colNum++, Types.TIMESTAMP);
						}
						if (model.getEndTime() != null) {
							ps.setTimestamp(colNum++, new Timestamp(model.getEndTime()));
						} else {
							ps.setNull(colNum++, Types.TIMESTAMP);
						}
						if (model.getUpdateTime() != null) {
							ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));
						} else {
							ps.setNull(colNum++, Types.TIMESTAMP);
						}
						ps.setString(colNum++, model.getName());
						if (model.getParentId() != null) {
							ps.setObject(colNum++, UUID.fromString(model.getParentId()));
						} else {
							ps.setNull(colNum++, Types.OTHER);
						}
						ps.setBoolean(colNum++, model.getStatus().isTerminated());
						ps.setBoolean(colNum++, model.getStatus().isStable());
						ps.setBoolean(colNum++, model.getStatus().isError());
						ps.setBoolean(colNum++, model.getStatus().isCancel());
						if (model.getCreateTime() != null) {
							ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
						} else {
							ps.setNull(colNum++, Types.TIMESTAMP);
						}

						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());

						return ps;
					}
				},
				keyHolder);

		return insertedRowCount != 0 ? model : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.activity_instance " +
			"SET " +
					"activity_instance_priority = ?, " +
					"activity_instance_status = ?, " +
					"activity_instance_status_details = ?, " +
					"activity_instance_device_response_code = ?, " +
					"activity_instance_device_response_details = ?, " +
					"activity_instance_progress = ?, " +
					"activity_instance_input_details = ?, " +
					"activity_instance_output_details = ?, " +
					"activity_instance_start_time = ?, " +
					"activity_instance_end_time = ?, " +
					"activity_instance_update_time = ?, " +
					"st_terminated = ?, " +
					"st_stable = ?, " +
					"st_error = ?," +
					"st_cancel = ? " +
			"WHERE " +
					"activity_instance_id = ? ";
    private static final String DELETE_SQL_EXECUTION_NODE =
            "DELETE FROM mwa.activity_instance_execution_node WHERE activity_instance_id = ? ";

	public ActivityInstanceModel updateModel(final ActivityInstanceModel model) {
		model.setUpdateTime(System.currentTimeMillis());
		{
			Map<String, Object> map = model.getOutputDetails();
			if (map == null) map = new java.util.HashMap<String, Object>();
			map.put("PersistentData", model.getPersistentData()); //★暫定実装
			model.setOutputDetails(map);
		}

		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(UPDATE_SQL);
						int colNum = 1;

						DatabaseEnum database = DatabaseEnum.getDatabase(jdbcTemplate.getDataSource());

						if (model.getPriority() != null)
						    ps.setInt(colNum++, model.getPriority());
						else
						    ps.setNull(colNum++, Types.INTEGER);
						ps.setString(colNum++, model.getStatus().getName());
						{
							if (model.getStatusDetails() != null && model.getStatusDetails().getResult() != null)
								ps.setString(colNum++, model.getStatusDetails().getResult().code());
							else
								ps.setNull(colNum++, Types.VARCHAR);
							if (model.getStatusDetails() != null && model.getStatusDetails().getDeviceResponse() != null)
                                ps.setString(colNum++, StringUtils.substring(model.getStatusDetails().getDeviceResponse(), 0, LENGTH_DEVICE_RESPONSE_CODE));
							else
								ps.setNull(colNum++, Types.VARCHAR);
							if (model.getStatusDetails() != null && model.getStatusDetails().getDeviceResponseDetails() != null)
                                ps.setString(colNum++, StringUtils.substring(model.getStatusDetails().getDeviceResponseDetails(), 0, LENGTH_DEVICE_RESPONSE_DETAILS));
							else
								ps.setNull(colNum++, Types.VARCHAR);
						}
						ps.setInt(colNum++, model.getProgress());

						if(DatabaseEnum.H2SQL.equals(database)){
							if (model.getInputDetails() != null) {
								try {
									String encodedStr = InternalConverter.mapToXml(model.getInputDetails());
									ps.setString(colNum++, encodedStr);
								} catch(Exception e) {
									logger.warn("Can't convert inputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.VARCHAR);
								}
							} else {
								ps.setNull(colNum++, Types.VARCHAR);
							}
							if (model.getOutputDetails() != null) {
								try {
									String encodedStr = InternalConverter.mapToXml(model.getOutputDetails());
									ps.setString(colNum++, encodedStr);
								} catch(Exception e) {
									logger.warn("Can't convert outputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.VARCHAR);
								}
							} else {
								ps.setNull(colNum++, Types.VARCHAR);
							}
						}else {
							if (model.getInputDetails() != null) {
								try {
									SQLXML sqlxml = conn.createSQLXML();
									String encodedStr = InternalConverter.mapToXml(model.getInputDetails());
									sqlxml.setString(encodedStr);
									ps.setSQLXML(colNum++, sqlxml);
								} catch(Exception e) {
									logger.warn("Can't convert inputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.SQLXML);
								}
							} else {
								ps.setNull(colNum++, Types.SQLXML);
							}
							if (model.getOutputDetails() != null) {
								try {
									SQLXML sqlxml = conn.createSQLXML();
									String encodedStr = InternalConverter.mapToXml(model.getOutputDetails());
									sqlxml.setString(encodedStr);
									ps.setSQLXML(colNum++, sqlxml);
								} catch(Exception e) {
									logger.warn("Can't convert outputDetails: " + e.getMessage());
									ps.setNull(colNum++, Types.SQLXML);
								}
							} else {
								ps.setNull(colNum++, Types.SQLXML);
							}
						}
						if (model.getStartTime() != null) {
							ps.setTimestamp(colNum++, new Timestamp(model.getStartTime()));
						} else {
							ps.setNull(colNum++, Types.TIMESTAMP);
						}
						if (model.getEndTime() != null) {
							ps.setTimestamp(colNum++, new Timestamp(model.getEndTime()));
						} else {
							ps.setNull(colNum++, Types.TIMESTAMP);
						}
						if (model.getUpdateTime() != null) {
							ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));
						} else {
							ps.setNull(colNum++, Types.TIMESTAMP);
						}
						ps.setBoolean(colNum++, model.getStatus().isTerminated());
						ps.setBoolean(colNum++, model.getStatus().isStable());
						ps.setBoolean(colNum++, model.getStatus().isError());
						ps.setBoolean(colNum++, model.getStatus().isCancel());

						ps.setObject(colNum++, UUID.fromString(model.getId()));

						return ps;
					}
				});

		if(updatedRowCount != 0 && model != null && model.getStatusModel() != null && model.getStatusModel().isTerminated()) {
			Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
			jdbcTemplate.update(DELETE_SQL_EXECUTION_NODE, paramArray);
		}

		return updatedRowCount != 0 ? model : null;
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.activity_instance " +
			"WHERE " +
					"activity_instance_id = ? ";

	public ActivityInstanceModel deleteModel(ActivityInstanceModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	private static final String SELECT_POLLING_TARGET_SQL =
			"SELECT DISTINCT ai.activity_instance_id, ai.activity_instance_create_time " +
			"FROM mwa.activity_instance AS ai LEFT JOIN mwa.event AS e ON ai.activity_instance_id = e.event_target_id JOIN mwa.activity_template AS at ON ai.activity_template_id = at.activity_template_id " +
			"WHERE ("
			//通常のポーリング対象
			+ "(ai.st_stable = false AND (age(now(), ai.activity_instance_update_time) > interval '00:00:03')) OR "
			////WFはステップからのコールバック待ちだが、開始してから10分動きがなければ状態確認を行う（イベント取りこぼして動けなくなっている可能性があるため）
			+ "(at.activity_template_type_id = 1 AND ai.activity_instance_start_time IS NOT NULL AND (	age(now(), ai.activity_instance_update_time) > interval '00:10:00') ) "
			+ ") "
			+ "AND ai.st_terminated = false AND e.event_target_id IS NULL AND at.activity_template_sync_flag = false " +  //完了済みと、eventがすでに積まれているもの、SyncTaskはポーリング対象から外す
			"ORDER BY ai.activity_instance_create_time DESC";

	//各スレッドがポーリング対象を見つけるために使用。event処理中でないポーリングインスタンスをリストで取得し、CONFIRM_STATUSを投げ込む際に使用（本来DAOのロジックではないが速度のため）
	//createTimeでソートしている理由はWFの子からeventを投げたかったため。だが一度のポーリングですべて発行するのであまり意味はない。
	public List<String> getPollingTargetIds() {
		List<String> result = new ArrayList<>();

		List<Map<String, Object>> ret = jdbcTemplate.queryForList(SELECT_POLLING_TARGET_SQL);
		if (ret != null) {
			for (Map<String, Object> map : ret) {
				result.add(map.get("activity_instance_id").toString());
			}
		}
		return result;
	}

	@Override
	protected IQuery<ActivityInstanceModel, ActivityInstanceCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ActivityInstanceQuery(jdbcTemplate);
	}
}
