package com.sony.pro.mwa.activity.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.model.activity.ActivityInstancePropertyCollection;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class ActivityInstancePropertyQuery extends Query<ActivityInstancePropertyModel, ActivityInstancePropertyCollection, QueryCriteria> {

    public enum COLUMN {
        PROPERTY_ID("i.activity_instance_property_id", "property_id", UUID.class, true),
        ID("i.activity_instance_id", "id", UUID.class, true),
        NAME("i.activity_instance_property_name", "name", String.class, true),
        VALUE("i.activity_instance_property_value", "value", String.class, true),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }


    public enum H2_COLUMN {
        PROPERTY_ID("i.activity_instance_property_id", "property_id", UUID.class, true),
        ID("i.activity_instance_id", "id", UUID.class, true),
        NAME("i.activity_instance_property_name", "name", String.class, true),
        VALUE("i.activity_instance_property_value", "value", String.class, true),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private H2_COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " +
            "mwa.activity_instance_property AS i "+
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {


        Map<String, Column> map = new HashMap<>();
    	if(DatabaseEnum.H2SQL.equals(getDatabase())){
            for (H2_COLUMN enumColumn : H2_COLUMN.values()) {
                map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
            }

    	}else{
            for (COLUMN enumColumn : COLUMN.values()) {
                map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
            }
    	}
        columnMap = Collections.unmodifiableMap(map);
    }


    public ActivityInstancePropertyQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }

	@Override
    protected RowMapper<ActivityInstancePropertyModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<ActivityInstancePropertyModel> {

        public ActivityInstancePropertyModel mapRow(ResultSet rs, int mapRow) throws SQLException {
        	ActivityInstancePropertyModel  model = new ActivityInstancePropertyModel();
        	model.setPropertyId(rs.getString(COLUMN.PROPERTY_ID.alias));
        	model.setId(rs.getString(COLUMN.ID.alias));
        	model.setName(rs.getString(COLUMN.NAME.alias));
        	model.setValue(rs.getString(COLUMN.VALUE.alias));


        	return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }
}
