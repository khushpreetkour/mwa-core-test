package com.sony.pro.mwa.activity.repository.mem;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.repository.ActivityInstancePropertyDao;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyCollection;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyModel;
import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.QueryCriteria;


public class ActivityInstancePropertyDaoImpl implements ActivityInstancePropertyDao {

	Map<String, ActivityInstancePropertyModel> map;

	public ActivityInstancePropertyDaoImpl() {
		this.map = new HashMap<>();
	}

	@Override
	public ActivityInstancePropertyCollection getModels(QueryCriteria criteria) {
		// TODO Auto-generated method stub
		ActivityInstancePropertyCollection result = new ActivityInstancePropertyCollection();
		List<ActivityInstancePropertyModel> models = new ArrayList<>();

		//filtering
		for (ActivityInstancePropertyModel model : map.values()) {
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "propertyId"), model.getPropertyId()) )
					continue;
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "id"), model.getId()) )
					continue;
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "name"), model.getName()) )
					continue;
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "value"), model.getValue()) )
					continue;
			models.add(model);
		}
		result.setModels(models);

		return result;
	}

	protected boolean checkConditionMatch(Filtering filter, Object value) {
		return (filter != null) && ( filter.getValue().equalsIgnoreCase(value.toString()) ^ FilterOperatorEnum.EQUAL.equals(filter.getOperator()) );
	}

	protected Filtering pickupFilter(List<List<Filtering>> filterings, String key) {
		for (List<Filtering> orFilterings : filterings) {
			for (Filtering filtering : orFilterings) {
				if ( key.equals(filtering.getKey())) {
					return filtering;
				}
			}
		}
		return null;
	}

	@Override
	public ActivityInstancePropertyModel getModel(String id, Principal principal) {
		return map.get(id);
	}

	@Override
	public ActivityInstancePropertyModel addModel(ActivityInstancePropertyModel model) {
		map.put(model.getId(), model);
		return model;
	}

	@Override
	public ActivityInstancePropertyModel updateModel(ActivityInstancePropertyModel model) {
		map.put(model.getId(), model);
		return model;
	}

	@Override
	public ActivityInstancePropertyModel deleteModel(ActivityInstancePropertyModel model) {
		map.remove(model.getId());
		return model;
	}
}
