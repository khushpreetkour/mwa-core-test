package com.sony.pro.mwa.activity.repository;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.activity.ActivityEventCollection;
import com.sony.pro.mwa.model.activity.ActivityEventModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityEventDao extends ModelBaseDao<ActivityEventModel, ActivityEventCollection> {
}
