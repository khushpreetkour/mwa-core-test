package com.sony.pro.mwa.activity.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.sony.pro.mwa.activity.repository.ActivityInstancePropertyDao;
import com.sony.pro.mwa.activity.repository.database.query.ActivityInstancePropertyQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyCollection;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyModel;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

public class ActivityInstancePropertyDaoImpl extends DatabaseBaseDao<ActivityInstancePropertyModel, ActivityInstancePropertyCollection, QueryCriteria> implements ActivityInstancePropertyDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	@Override
	public ActivityInstancePropertyCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	@Override
	public ActivityInstancePropertyModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}


	private static final String INSERT_SQL =
			"INSERT INTO " +
					"mwa.activity_instance_property(" +
					"  activity_instance_property_id, activity_instance_id, activity_instance_property_name, activity_instance_property_value) " +
			"VALUES " +
					"(?, ?, ?, ?) ";

	@Override
	public ActivityInstancePropertyModel addModel(final ActivityInstancePropertyModel model) {

		final String insertSql = INSERT_SQL;
		//final long currentTime = System.currentTimeMillis();
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});

						int colNum = 1;

						ps.setObject(colNum++, UUID.fromString(model.getPropertyId()));
						ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getName());
						ps.setString(colNum++, model.getValue());

						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());

						return ps;
					}
				},
			keyHolder);

		return insertedRowCount != 0 ? model : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.activity_instance_property " +
			"SET " +
					"activity_instance_property_value = ? " +
			"WHERE " +
					"activity_instance_id = ? " +
			"AND "   +
					"activity_instance_property_name = ? ";

	@Override
	public ActivityInstancePropertyModel updateModel(final ActivityInstancePropertyModel model) {

		final String updateSql = UPDATE_SQL;

		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(updateSql);
						int colNum = 1;

						ps.setString(colNum++, model.getValue());
						ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getName());
						return ps;
					}
				});

		return updatedRowCount != 0 ? model : null;
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.activity_instance_property " +
			"WHERE " +
					"activity_instance_id = ? ";

	public ActivityInstancePropertyModel deleteModel(ActivityInstancePropertyModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<ActivityInstancePropertyModel, ActivityInstancePropertyCollection, QueryCriteria> createQuery(
			JdbcTemplate jdbcTemplate) {
		return new ActivityInstancePropertyQuery(jdbcTemplate);
	}

}
