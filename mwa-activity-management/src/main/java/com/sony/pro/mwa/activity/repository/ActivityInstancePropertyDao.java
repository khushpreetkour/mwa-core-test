package com.sony.pro.mwa.activity.repository;

import com.sony.pro.mwa.model.activity.ActivityInstancePropertyCollection;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityInstancePropertyDao extends ModelBaseDao<ActivityInstancePropertyModel, ActivityInstancePropertyCollection> {

}
