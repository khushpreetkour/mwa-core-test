package com.sony.pro.mwa.activity.repository.mem;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.repository.ActivityInstanceDao;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.activity.ActivityTemplateCollection;
import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.service.activity.IMwaActivity;

import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/29
 * Time: 6:57
 * To change this template use File | Settings | File Templates.
 */
//@Repository
public class ActivityInstanceDaoImpl implements ActivityInstanceDao {

	Map<String, ActivityInstanceModel> map;
	
	public ActivityInstanceDaoImpl() {
		this.map = new HashMap<>();
	}

	@Override
	public ActivityInstanceCollection getModels(QueryCriteria criteria) {
		// TODO Auto-generated method stub
		ActivityInstanceCollection result = new ActivityInstanceCollection();
		List<ActivityInstanceModel> models = new ArrayList<>();
		
		//filtering
		for (ActivityInstanceModel model : map.values()) {
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "parentId"), model.getParentId()) )
					continue;
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "name"), model.getName()) )
					continue;
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "isStable"), model.getStatus().isStable()) )
					continue;
			if ( checkConditionMatch(pickupFilter(criteria.getFilterings(), "isTerminated"), model.getStatus().isTerminated()) )
					continue;
			models.add(model);
		}
		result.setModels(models);
		
		return result;
	}
	
	protected boolean checkConditionMatch(Filtering filter, Object value) {
		return (filter != null) && ( filter.getValue().equalsIgnoreCase(value.toString()) ^ FilterOperatorEnum.EQUAL.equals(filter.getOperator()) );
	}

	protected Filtering pickupFilter(List<List<Filtering>> filterings, String key) {
		for (List<Filtering> orFilterings : filterings) {
			for (Filtering filtering : orFilterings) {
				if ( key.equals(filtering.getKey())) {
					return filtering;
				}
			}
		}
		return null;
	}
	
	@Override
	public ActivityInstanceModel getModel(String id, Principal principal) {
		return map.get(id);
	}

	@Override
	public ActivityInstanceModel addModel(ActivityInstanceModel model) {
		map.put(model.getId(), model);
		return model;
	}

	@Override
	public ActivityInstanceModel addModel(ActivityInstanceModel model, boolean isExecutor) {
		map.put(model.getId(), model);
		return model;
	}

	@Override
	public ActivityInstanceModel updateModel(ActivityInstanceModel model) {
		map.put(model.getId(), model);
		return model;
	}

	@Override
	public ActivityInstanceModel deleteModel(ActivityInstanceModel model) {
		map.remove(model.getId());
		return model;
	}

	@Override
	public long getFilteringCount(QueryCriteria criteria) {
		return 0;
	}

	@Override
	public long getTotalCount(QueryCriteria criteria) {
		return 0;
	}
}
