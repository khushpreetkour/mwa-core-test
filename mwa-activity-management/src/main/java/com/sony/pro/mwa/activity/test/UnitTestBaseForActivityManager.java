package com.sony.pro.mwa.activity.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.bundle.MwaBundle;
import com.sony.pro.mwa.kbase.KnowledgeBaseByMemImpl;
import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.service.kbase.IKnowledgeBase;

public class UnitTestBaseForActivityManager {
	protected IActivityManager activityManager;
	protected IKnowledgeBase knowledgeBase;

	@Autowired
	public void setActivityManager(IActivityManager activityManager) {
		this.activityManager = activityManager;
	}
	public IActivityManager getActivityManager() {
		return this.activityManager;
	}
	@Autowired
	public void setKnowledgeBase(IKnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}
	public IKnowledgeBase getKnowledgeBase() {
		return this.knowledgeBase;
	}
	
	public void initActivityManager(MwaBundle bundle) {
		initialize();
		registerBundle(bundle);
	}
	public void initActivityManager(List<MwaBundle> bundles) {
		initialize();
		for (MwaBundle bundle : bundles) {
			registerBundle(bundle);
		}
	}
	
	protected void initialize() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("mwa-app-context.xml");
		ctx.getAutowireCapableBeanFactory().autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, true);
	}
	protected void registerBundle(MwaBundle bundle) {
		for (ActivityTemplate template : bundle.getTemplates()) { 
			((KnowledgeBaseByMemImpl)this.knowledgeBase).addTemplate(template);
		}
	}
	
}
