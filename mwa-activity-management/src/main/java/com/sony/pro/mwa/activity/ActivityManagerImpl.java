package com.sony.pro.mwa.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.activity.framework.AbsMwaExecutorActivity;
import com.sony.pro.mwa.activity.framework.internal.IMwaActivityInternal;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.activity.repository.ActivityInstanceDao;
import com.sony.pro.mwa.activity.repository.ActivityInstancePropertyDao;
import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.CipherUtils;
import com.sony.pro.mwa.common.utils.NamedLocks;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.ResourceGroupType;
import com.sony.pro.mwa.enumeration.ServiceEndpointType;
import com.sony.pro.mwa.enumeration.event.EventPriority;
import com.sony.pro.mwa.enumeration.event.EventTargetType;
import com.sony.pro.mwa.enumeration.resource.ResourceEvent;
import com.sony.pro.mwa.enumeration.resource.ResourceEventParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.internal.activity.ExternalMwaPollingTask;
import com.sony.pro.mwa.internal.activity.ExternalMwaTask;
import com.sony.pro.mwa.internal.activity.IActivityManagerInternal;
import com.sony.pro.mwa.internal.definite.PreDefine;
import com.sony.pro.mwa.internal.service.CoreModulesImpl;
import com.sony.pro.mwa.internal.utils.ResourceUtils;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyCollection;
import com.sony.pro.mwa.model.activity.ActivityInstancePropertyModel;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.model.event.EventModel;
import com.sony.pro.mwa.model.kbase.ActivityProfileModel;
import com.sony.pro.mwa.model.kbase.ParameterSetModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceGroupCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestEntryModel;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypePassword;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.activity.StatusDetails;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.kbase.IKnowledgeBase;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.service.provider.IActivityProviderManager;
import com.sony.pro.mwa.service.resource.IResource;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.service.scheduler.ISchedulerManager;
import com.sony.pro.mwa.service.storage.IStorageManager;
import com.sony.pro.mwa.service.storage.IUriResolver;

/**
 *
 *  レイヤー責務
 *  ・ActivityInstance管理
 *    ・ActivityInstanceの生成、ID発番
 *    ・ActivityInstanceの永続化（ORMもここか。。）
 *    ・ActivityInstanceの破棄（用のAPIを用意、破棄のためのロジックは別管理）
 *  ・外部へのActivity操作I/F公開
 *  ・jBPMのExecutor側との接続レイヤー
 *
 *  要件
 *  ・Activity独自の状態遷移が追加できるよう拡張できること（ManualQCなど特殊なHumanTaskや、iAPDigitizeなど、独自の状態遷移拡張）
 *
 *  ポリシー
 *  ・本クラスはSingleton
 *  ・Activityの実装側への依存関係を持たない（詳細情報を本レイヤーは意識しない）
 *
 */
@Service
@Component
public class ActivityManagerImpl implements IActivityManagerInternal {

    private final static MwaLogger logger = MwaLogger.getLogger(ActivityManagerImpl.class);
    Map<EXEC, ScheduledExecutorService> executors;
    ScheduledFuture<?> future;

    enum EXEC{
        POLLING(1), //EVENT(1),
        ;
        private int poolSize;
        private EXEC(int poolSize) {this.poolSize = poolSize;}
        public int getPoolSize() {return this.poolSize;}
    }

    private String sortSuffix(boolean isAscending) {
        return isAscending ? "+" : "-";
    }

    private ActivityInstanceDao activityInstanceDao;
    private ActivityInstancePropertyDao activityInstancePropertyDao;
    //	private ActivityEventDao activityEventDao;
    private IKnowledgeBase kBase;
    private IActivityProviderManager activityProviderManager;
    private IUriResolver uriResolver;
    private IResourceManager resourceManager;
    private IEventManager eventManager;
    private ISchedulerManager schedulerManager;

    private ICoreModules getCoreModulesForInstance(String instanceId) {
        ActivityManagerProxyForHook activityManagerProxy = new ActivityManagerProxyForHook(this);
        Map<String, Object> defaultParams = new HashMap<>();
        defaultParams.put(PresetParameter.ParentInstanceId.getKey(), instanceId);
        activityManagerProxy.setDefaultParam(CommonEvent.REQUEST_SUBMIT.getName(), defaultParams);
        return new CoreModulesImpl().setActivityManager(activityManagerProxy)
                .setActivityProviderManager(activityProviderManager)
                .setKnowledgeBase(kBase)
                .setUriResolver(uriResolver)
                .setSchedulerManager(schedulerManager)
                .setEventManager(eventManager)
                .setResourceManager(resourceManager)
                .setStorageManager((IStorageManager)uriResolver)
                ;
    }

    // parameterSetをロードする。
    private Map<String, Object> loadParameterSet(Map<String, Object> param) {
        Map<String, Object> loadedParam = new HashMap<>();
        Map<String, Object> tempParam = new HashMap<>();

        for (Map.Entry<String, Object> e : param.entrySet()) {
            if (e.getKey().equals("ParameterSet")) {
                String parameterSetId = (String) e.getValue();

                ParameterSetModel parameterSet = kBase.getParameterSet(parameterSetId);
                if (parameterSet != null)  {
                	loadedParam.putAll(parameterSet.getParameters());
                } else {
                    final String msg = "PrameterSetId:" + parameterSetId + " is not registered.";
                    throw new MwaError(MWARC.INVALID_INPUT, "", msg);
                }

            } else {
            	tempParam.put(e.getKey(), e.getValue());
            }
        }

        loadedParam.putAll(tempParam);

        return loadedParam;
    }

    // LockObjectの管理クラス
    private static NamedLocks locks = new NamedLocks();

    @Autowired
    @Qualifier("activityInstanceDao")
    public void setActivityInstanceDao(ActivityInstanceDao dao) {
        this.activityInstanceDao = dao;
    }

    @Autowired
    @Qualifier("activityInstancePropertyDao")
    public void setActivityInstancePropertyDao(ActivityInstancePropertyDao dao) {
        this.activityInstancePropertyDao = dao;
    }

    /*	@Autowired
    @Qualifier("activityEventDao")
    public void setActivityEventDao(ActivityEventDao dao) {
        this.activityEventDao = dao;
    }*/

    @Autowired
    @Qualifier("knowledgeBase")
    public void setKnowledgeBase(IKnowledgeBase kBase) {
        this.kBase = kBase;
    }

    @Autowired
    //@Qualifier("uriResolver")
    public void setUriResolver(IUriResolver uriResolver) {
        this.uriResolver = uriResolver;
    }

    @Autowired
    @Qualifier("resourceManager")
    public void setResourceManager(IResourceManager resourceManager) {
        this.resourceManager = resourceManager;
    }

    @Autowired
    @Qualifier("eventManager")
    public void setEventManager(IEventManager eventManager) {
        this.eventManager = eventManager;
    }

    @Autowired
    @Qualifier("schedulerManager")
    public void setSchedulerManager(ISchedulerManager schedulerManager) {
        this.schedulerManager = schedulerManager;
    }

    @Autowired
    @Qualifier("activityProviderManager")
    public void setActivityProviderManager(IActivityProviderManager activityProviderManager) {
        this.activityProviderManager = activityProviderManager;
    }
    public IKnowledgeBase getKnowledgeBase() {
        return this.kBase;
    }

    private ActivityManagerImpl() {
    }

    public void initialize() {
        executors = new EnumMap<>(EXEC.class);

        for (EXEC execType : EXEC.values()) {
            executors.put(execType, Executors.newScheduledThreadPool(execType.getPoolSize()));
        }

        future = executors.get(EXEC.POLLING).scheduleAtFixedRate(new Runnable() {
            ActivityManagerImpl mgr;
            @Override
            public void run() {
                try {
                    mgr.pollingStatus();
                } catch (Throwable e) {
                    logger.error("Actitive Insntace Polling Thread cansed exception: ", e);
                }
            }
            public Runnable setMgr(ActivityManagerImpl mgr){ this.mgr = mgr; return this; }
        }.setMgr(this), 1, 1, TimeUnit.SECONDS);

        logger.info("ActivityManager started!!");
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            super.finalize();
        } finally {
            destroy();
        }
    }

    public void destroy() {
        for (EXEC execType : EXEC.values()) {
            ScheduledExecutorService executor = this.executors.get(execType);
            if (executor != null) {
                try {
                    executor.shutdown();
                    if(!executor.awaitTermination(5*1000, TimeUnit.MILLISECONDS)){
                        // タイムアウトした場合、全てのスレッドを中断(interrupted)してスレッドプールを破棄する。
                        executor.shutdownNow();
                    }
                } catch (InterruptedException e) {
                    // awaitTerminationスレッドがinterruptedした場合も、全てのスレッドを中断する
                    logger.warn("awaitTermination interrupted: " + e);
                    executor.shutdownNow();
                }
            }
        }
        logger.info("ActivityManager stoped!!");
        return;
    }

    @Override
    public OperationResult createInstance(String templateId, Map<String, Object> params) {
        if(params == null) {
            params = new HashMap<>();
        }

        IActivityTemplate template = null;
        if (templateId != null) {
            template = this.kBase.getActitvityTemplateService(templateId);
        }
        if (template == null) {
            String message = "Activity Template not found: id=" + templateId;
            logger.error(message);
            throw new MwaError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
        }
        return createInstance(template, params);
    }
    @Override
    public OperationResult createInstance(String templateName, String templateVersion, Map<String, Object> params) {
        if(params == null) {
            params = new HashMap<>();
        }
        IActivityTemplate template = null;
        if (templateName != null && templateVersion != null) {
            template = this.kBase.getActitvityTemplateService(templateName, templateVersion);
        }
        if (template == null) {
            String message = "Activity Template not found: name=" + templateName + ", version=" + templateVersion;
            logger.error(message);
            throw new MwaError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
        }
        return createInstance(template, params);
    }

    protected OperationResult createInstance(IActivityTemplate template, Map<String, Object> params) {
        OperationResult result = OperationResult.newInstance();

        // TemplateのdeleteFlagがtrueならActivityInstanceを生成しないで終了させる。
        ActivityTemplateModel activityTemplateModel = this.kBase.getActitvityTemplate(template.getId());
        if(Boolean.TRUE.equals(activityTemplateModel.getDeleteFlag())){
            String message = "Activity Template undepoyed: name=" + template.getName() + ", version=" + template.getVersion();
            logger.error(message);
            throw new MwaError(MWARC.INVALID_INPUT_TEMPLATE_UNDEPLOYED);
        }
        {//Profile処理
        	String profileId = (String)params.get(PresetParameter.ProfileId.name());
        	if (profileId != null) {
	        	ActivityProfileModel profile = kBase.getActivityProfile(profileId);
	        	if (profile != null) {
		        	if (StringUtils.equalsIgnoreCase(template.getId(), profile.getTemplateId())) {
		            	profile.getParameter().putAll(params);
		            	params = profile.getParameter();
		        	}
	        	}
        	}
        }

        //TypePasswordの暗号化
        encParams(template, params);

        {	//ResourceHolder形式の解決（Cost計算前に必要）

        	//修正前 providerIdの取得がnullであった
//        	String providerId = (String)params.get(PresetParameter.ProviderId);
        	String providerId = (String)params.get(PresetParameter.ProviderId.name());
        	//ProviderIdの有無
        	if (providerId != null) {
        		String resourceName = ResourceUtils.extractResourceOfHolder(providerId);
        		if(resourceName != null){
            		ResourceCollection resourceCol = this.resourceManager.getResources(null,Arrays.asList("name" + FilterOperatorEnum.EQUAL.toSymbol() + resourceName), null, null);
            		IResource resource = null;
            		if(resourceCol.getTotalCount() > 0){
            			resource  = resourceCol.first();
            		}

            		if(resource == null){
                		ResourceGroupCollection resourceGroupCol = this.resourceManager.getResourceGroups(null,Arrays.asList("name" + FilterOperatorEnum.EQUAL.toSymbol() + resourceName), null, null);
                		if(resourceGroupCol.getTotalCount() > 0){
                			resource  = resourceGroupCol.first();
                		}
            		}

        			//Resourceの取り出し
        			if (resource != null ) {
        				resource.getType();
        				params.put(PresetParameter.ResourceIdOfHolder.getKey(), resource.getId());
        				params.put(PresetParameter.ResourceTypeOfHolder.getKey(), resource.getType());

        				final String resourceHolder = resource.getHolder();

        				//resourceHolderがUUIDの場合は、providerIdと見なして、paramsのProviderIdに詰替えセットする。
        				try {
        					UUID.fromString(resourceHolder);
        					params.put(PresetParameter.ProviderId.name(),resourceHolder);
        				} catch (Exception e) {
        					//uuid形式ではないので、警告メッセージだけだして何もしない。
        					logger.warn(e.getMessage());
        				}
        			}


        		} else {
        			//ResourceHolder形式ではないので
        		}
        	}
        	//抽象Providerという概念はなく、この時点でProvider解決はできないので、そこを根拠にCost定義するのは現状難しい
        	//また、現状ProviderIdからResourceを取得できないので、それも難しい。複数ResourceIdを持つ可能性もあるためどのみち難しいのですが。。
        	//つまり現状、ResourceHolder指定を使うしかResourceIdを通知するすべがない。。
        }

        ResourceRequestModel request = this.kBase.extractResourceRequest(template.getName(), template.getVersion(), template.getType().name(), params);
        //ここでは抽象Providerとして指定に問題がないかみるべきか？それはextractResourceRequest()で見ればよい？？

        String activityInstanceId = null;
        {
            activityInstanceId = (String)params.get(PresetParameter.ActivityInstanceId.getKey());
        }

        boolean hasParameterSetError = false;
        try {
        	params = this.loadParameterSet(params);
        } catch (MwaError e) {
        	hasParameterSetError = true;
        }

        ActivityInstanceModel model = this.createInstanceModel(activityInstanceId, template, params);
        if(params != null && params.get(PresetParameter.Properties.getKey()) != null) {
            try {
                List<ActivityInstancePropertyModel> properties = new ObjectMapper().readValue((String)params.get(PresetParameter.Properties.getKey()), new TypeReference<List<ActivityInstancePropertyModel>>() {});
                if(properties != null && properties.size() != 0){
                    Map<String, Object> propertiesMap = new HashMap<String, Object>();
                    for(ActivityInstancePropertyModel property : properties){
                        propertiesMap.put(property.getName(), property.getValue());
                    }
                    model.setProperties(propertiesMap);
                }
            } catch (Exception e) {
                logger.error("Property casting error : ", e);
                model.setProperties(null);
            }
        }
        Boolean volatileFlag = (Boolean)PresetParameter.Volatile.getType().fromString((String)params.get(PresetParameter.Volatile.getKey()));
        if (volatileFlag == null)
            volatileFlag = false;
        model.setVolatileFlag(volatileFlag.booleanValue());
        if (request == null) {
            model.setStatus(CommonState.READY);
            model.setStartTime(System.currentTimeMillis());
        } else {
            model.setStatus(CommonState.QUEUED);
        }

        //DB登録判定
        if (!template.getSyncFlag() || !volatileFlag) {
            IMwaActivityInternal instance = (IMwaActivityInternal)template.createInstance();
            if (instance instanceof AbsMwaExecutorActivity || ActivityType.WORKFLOW.equals(instance.getTemplate().getType())
                    || (template instanceof ActivityTemplate && ((ActivityTemplate)template).getNodeDependencyFlag())) {
                this.registerInstnaceModel(model, true);
            } else {
                this.registerInstnaceModel(model, false);
            }
        }

        //ここでフックしてSUBMITか、QUEUEDかを選択する感じで
        if (request != null) {
            request.setOccupantId(model.getId());
            request.setReceiverId(null);
            request.setOccupantName(template.getName());
            try {
                this.resourceManager.addResourceRequest(request);
            } catch (Exception e) {
                StatusDetails status = new StatusDetails();
                if (e instanceof MwaError)
                    status.setResult(((MwaError)e).getMWARC());
                else
                    status.setResult(MWARC.SYSTEM_ERROR);
                model.setStatus(CommonState.ERROR);
                this.updateInstanceCore(model, false);
                e.printStackTrace();
                throw e;
            }
        } else if (template.getSyncFlag()) {
            //Resource消費がないのでそれをまたずに処理？
            //即時実処理（排他処理は不要）
            result = operateActivity(model, CommonEvent.REQUEST_SUBMIT.getName(), params, true);

        } else {
            operateActivityAsync(model.getId(), CommonEvent.REQUEST_SUBMIT.getName(), params);
        }

        //DB登録してる場合はInstanceIdを返す
        if (!template.getSyncFlag() || !volatileFlag) {
            result.put(PresetParameter.ActivityInstanceId, model.getId());
        }

        if (hasParameterSetError) {
        	StatusDetails status = new StatusDetails();
            status.setResult(MWARC.INVALID_INPUT);
	        model.setStatus(CommonState.ERROR);
	        model.setStatusDetails(status);

	        this.updateInstanceCore(model, false);

            final String msg = "PrameterSetId is not registered.";
            throw new MwaError(MWARC.INVALID_INPUT, "", msg);
        }

        return result;
    }

    public OperationResult updateInstance(ActivityInstanceModel inputModel) {
        OperationResult result = OperationResult.newInstance();
        if (inputModel == null || inputModel.getId() == null) {
            return OperationResult.newInstance(MWARC.INVALID_INPUT_INSTANCE_ID, "input model or instanceId is invalid.");
        }

        try {
           if (this.lockEvent(inputModel.getId())) {
               //Instanceの更新
               ActivityInstanceModel model = this.getInstance(inputModel.getId());
               if (model == null) {
                   return OperationResult.newInstance(MWARC.INVALID_INPUT_INSTANCE_ID, "Can't find insatnce id=" + inputModel.getId());
               }
               model.setProperties(inputModel.getProperties());
               model.setName(inputModel.getName());
               model.setPriority(inputModel.getPriority());
               model.getInputDetails().put(PresetParameter.ActivityInstancePriority.getKey(), inputModel.getPriority());
               //ResourceRequest取得
               List<String> filters = new ArrayList<>();
               filters.add("occupantId" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId());
               ResourceRequestCollection requestCollection = this.resourceManager.getResourceRequests(new ArrayList<String>(), filters, 0, 1);
               if (requestCollection != null && requestCollection.getModels() != null && !requestCollection.getModels().isEmpty()) {
                   //ResourceRequestの再計算
                   IActivityTemplate template = this.kBase.getActitvityTemplateService(model.getTemplateId());
                   ResourceRequestModel extractedRequest = this.getKnowledgeBase().extractResourceRequest(template.getName(), template.getVersion(), template.getType().name(), model.getInputDetails());
                   if (extractedRequest != null) {
                       ResourceRequestModel request = requestCollection.getModels().get(0);
                       List<ResourceRequestEntryModel> newRequestEntry = replaceRequestEntryCost(request.getResourceRequestEntryList(), extractedRequest.getResourceRequestEntryList());
                       //Costの動的な変更は微妙かも？すでにアサインされている場合は、Costは変更不可にするとか
/*                       logger.info("Update a resource request, occupantId: " + model.getId() + "\n" +
                    		   "\t[before] priority: " + request.getPriority() + ", cost: " + request.getResourceRequestEntryList() + "\n" +
                    		   "\t[after ] priority: " + extractedRequest.getPriority() + ", cost: " + newRequestEntry);*/
                       request.setResourceRequestEntryList(newRequestEntry);
                       request.setPriority(extractedRequest.getPriority());
                       this.resourceManager.updateResourceRequest(request);
                   } else {
                       //can't calculate resource cost
                	   logger.warn("Can't calculate resource cost: id=" + model.getId());
                   }
               } else {
                   //can't find out corresponding resource request
            	   logger.warn("Can't find out corresponding resource request: id=" + model.getId());
               }
               this.updateInstanceCore(model, false);
           }
        } catch (Exception e) {
            logger.error("updateInstance caused exception: id=" + inputModel, e);
        } finally {
            this.eventManager.unlockEvent(inputModel.getId());
        }
        return result;
    }

    protected List<ResourceRequestEntryModel> replaceRequestEntryCost(List<ResourceRequestEntryModel> base, List<ResourceRequestEntryModel> cost) {
        for (ResourceRequestEntryModel baseEntry : base) {
            for (ResourceRequestEntryModel costEntry : cost) {
                if (baseEntry.getResourceId() != null && baseEntry.getResourceId().equals(costEntry.getResourceId())) {
                    baseEntry.setCost(costEntry.getCost());
                    break;
                }
            }
        }
        return base;
    }

    protected Boolean lockEvent(String exclusionId) {
        Boolean isLocked = false;
        for (int i=0; i<5; i++) {
            try {
                if ( this.eventManager.lockEvent(exclusionId) ) {
                    isLocked = true;
                    break;
                }
            } catch (org.springframework.dao.DuplicateKeyException e) {
                //do nothing
                logger.info("lockEvent caused exception: retry-count=" + i + ", exclusionId=" + exclusionId);
            }
            if (!isLocked)
                try { Thread.sleep(1*1000); } catch (InterruptedException e) {}
        }
        return isLocked;
    }

    @Override
    public OperationResult operateActivity(String parentId, String localName, String operation, Map<String, Object> params) {
        ActivityInstanceModel model = this.getChildInstance(parentId, localName);
        OperationResult result = operateActivity(model, operation, params);

        return result;
    }
    @Override
    public OperationResult operateActivity(String instanceId, String operation, Map<String, Object> params) {
        ActivityInstanceModel model = this.getInstance(instanceId);
        OperationResult result = operateActivity(model, operation, params);
        return result;
    }

    protected OperationResult operateActivity(ActivityInstanceModel inputModel, String operation, Map<String, Object> params) {

    	return operateActivity(inputModel, operation, params, false);
    }

    protected OperationResult operateActivity(ActivityInstanceModel inputModel, String operation, Map<String, Object> params,boolean isSync) {
        OperationResult result = OperationResult.newInstance(MWARC.SUCCESS);
        ActivityInstanceModel model = null;
        boolean isAlreadyTerminated = false;

        try {
             // RequestSubmitはManagerのレイヤーでハンドリングが必要
            //operation処理
            //クリティカルセクション内でインスタンス取得しないとNG
            synchronized (locks.get(inputModel.getId())) {
                model = this.getInstance(inputModel.getId());
                if (model == null)
                    model = inputModel;	//Volatileの場合にはInstance取得できない

                if (model != null && model.getStatus() != null)
                	isAlreadyTerminated =  model.getStatus().isTerminated();

                //リソース確保の通知を受けて、SUBMITを実施
                if (ResourceEvent.NOTIFY_RESERVED.isMatch(operation)) {
                    operation = CommonEvent.REQUEST_SUBMIT.getName();
                    String key = (String)params.get(ResourceEventParameter.ASSIGNED_RESOURCE_KEY.name());
                    String resourceId = (String)params.get(ResourceEventParameter.ASSIGNED_RESOURCE_ID.name());
                    model.getInputDetails().put(ResourceEventParameter.ASSIGNED_RESOURCE_ID.name(), resourceId);
                    params = model.getInputDetails();
                    if (PresetParameter.ProviderId.getKey().equalsIgnoreCase(key)) {
                        ResourceModel resource = this.resourceManager.getResource((String)model.getInputDetails().get(ResourceEventParameter.ASSIGNED_RESOURCE_ID.name()));
                        if (model != null) {
                            String originalProviderId = (String)model.getInputDetails().get(PresetParameter.ProviderId.getKey());
                            model.getInputDetails().put(PresetParameter.ProviderId.getKey(), resource.getHolder());
                            model.getInputDetails().put(PresetParameter.OriginalProviderId.getKey(), originalProviderId);
                        }
                    } else if (key != null) {
                        params.put(key, resourceId);
                    }
                    if(model.getStartTime() == null){
                        model.setStartTime(System.currentTimeMillis());
                    }
                }

                result = operateActivityCore(model, operation, params);

                if(isSync){
                	model.setOutputDetails(result);
                }

                this.updateInstanceCore(model, isAlreadyTerminated);
            }
//★↓これらは状態遷移系の処理責務を持ってしまっているので、一度責務分担整理したい
        } catch (MwaInstanceError e) {
            //Terminateの場合状態書き換えしたくないのでterminatedフラグをチェック
            if (model != null) {
            	if(!model.getStatus().isTerminated()) {
	                model.setStatus(CommonState.ERROR);
	                StatusDetails statusDetails = new StatusDetails();
	                statusDetails.setResult(e.getMWARC());
	                statusDetails.setDeviceResponse(e.getDeviceResponseCode());
	                statusDetails.setDeviceResponseDetails(e.getDeviceResponseDetails());
	                model.setStatusDetails(statusDetails);
	                result = OperationResultWithStatus.newInstance(MWARC.OPERATION_FAILED, e.getMessage());
	                model.setStartTime(null);
            	}
                //Operate内でModelの状態変わってる可能性があるのでupdateは必須
                this.updateInstanceCore(model, isAlreadyTerminated);
            }
            throw e;
        } catch (Throwable e) {
            if (model != null) {
            	//MwaErrorはここにくるので, 勝手にエラーに遷移させないよう、条件を設ける
            	if (!(e instanceof MwaError)) {
	                model.setStatus(CommonState.ERROR);
	                StatusDetails statusDetails = new StatusDetails();
	                statusDetails.setResult(MWARC.OPERATION_FAILED);
	                model.setStatusDetails(statusDetails);
	                model.setStartTime(null);
            	}
                //Operate内でModelの状態変わってる可能性があるのでupdateは必須
                this.updateInstanceCore(model, isAlreadyTerminated);
            }
            throw e;
        }

        return result;
    }

    protected OperationResult operateActivityCore(ActivityInstanceModel model, String operation, Map<String, Object> params) {
        OperationResult result = null;
        IMwaActivityInternal instance = null;
        IActivityTemplate template = null;
        IActivityProvider provider = null;
        {	//modelからProvider情報を取り出し
            String providerId = null;
            if (model != null && model.getInputDetails() != null)
                providerId = (String)model.getInputDetails().get(PresetParameter.ProviderId.getKey());
            if (providerId != null){
                provider = this.activityProviderManager.getProviderService(providerId);
                if(provider != null && provider.getId() == null){
                    throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND);
                }
            }
        }

        {
            if (model == null)
                throw new MwaInstanceError(MWARC.INVALID_INPUT_INSTANCE_ID);

            if (provider != null && ExternalMwaTask.isExternalProvider(provider.getProviderTypeName())) {

                IActivityTemplate actualTemplate = this.kBase.getActitvityTemplateService(model.getTemplateName(), model.getTemplateVersion());
                //★PollingかSyncかCallbackか、で振る舞い変えたい
                if (actualTemplate.getSyncFlag()) {
                    logger.warn("SyncActivity doesn't support external execution (on worker)");
                    throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, "SyncActivity doesn't support external execution (on worker)");
                } else {
                    template = ExternalMwaTask.getTemplate(ExternalMwaPollingTask.class.getName(), "1");
                }
                params = new TreeMap<>(params);	//External用に追加するのでparamsをコピーして投げ込む
                params.put(PresetParameter.ExternalActivityTemplateName.getKey(), actualTemplate.getName());
                params.put(PresetParameter.ExternalActivityTemplateVersion.getKey(), actualTemplate.getVersion());
                //★Externalにそのままprovider情報を渡すのはおかしいので、ここで落とす(どうProviderを指定するか悩ましいところ)
                //InputDetailsから落ちることがないように注意すること
                params.remove(PresetParameter.ProviderId.getKey());
            } else {
                template = this.kBase.getActitvityTemplateService(model.getTemplateName(), model.getTemplateVersion());
            }
            //hotempl

            if (template == null)
                throw new MwaInstanceError(MWARC.INVALID_INPUT_TEMPLATE_NAME);

            instance = (IMwaActivityInternal)template.createInstance();
            if (instance == null)
                throw new MwaError(MWARC.INVALID_INPUT_INSTANCE_ID);
            instance.setModel(model);
            instance.setCoreModules(getCoreModulesForInstance(model.getId()));

            if((provider != null) && (provider.getEndpoints() != null)){
            	//Providerが存在する 且つ Endpointsが存在する場合
                if(provider.getEndpoints().containsKey(ServiceEndpointType.DEFAULT.name())){
                	provider.getEndpoints().get(ServiceEndpointType.DEFAULT.name()).setProviderId(provider.getId());
                }
            }

            instance.setProvider(provider);
        }

        instance.setCoreModules(getCoreModulesForInstance(instance.getId()));
        decParams(template, params);
        result = instance.operate(operation, params);
        return result;
    }

    private Map<String, Object> encParams(IActivityTemplate template, Map<String, Object> params) {
    	for (IParameterDefinition pdef : template.getInputs()) {
    		if (pdef.getType() instanceof TypePassword) {
    			Object pparam = params.get(pdef.getKey());
    			if (pparam instanceof String)
    				params.put(pdef.getKey(), DatatypeConverter.printHexBinary(CipherUtils.encrypto(pparam.toString(), PreDefine.DEFAULT_MAGIC_NUM)));
    		}
    	}
    	return params;
    }
    private Map<String, Object> decParams(IActivityTemplate template, Map<String, Object> params) {
    	for (IParameterDefinition pdef : template.getInputs()) {
    		if (pdef.getType() instanceof TypePassword) {
    			Object param = params.get(pdef.getKey());
    			if (param instanceof String)
    				params.put(pdef.getKey(), CipherUtils.decrypto(DatatypeConverter.parseHexBinary(param.toString()), PreDefine.DEFAULT_MAGIC_NUM));
    		}
    	}
    	return params;
    }

    // 内部用
    // ProcessEngineからのコールバック処理が、同一スレッドで動くことがあり、排他がうまくいかない。
    // このため、パラメータ群を一度メモリに保持しておき、PoolingThread上でOperationを処理する機能を追加した
    @Override
    public void operateActivityAsync(String instanceId, String operation, Map<String, Object> params) {
        EventModel event = new EventModel();
        event.setName(operation);
        event.setTargetId(instanceId);
        event.setTargetType(EventTargetType.ACTIVITY_INSTANCE.name());
        event.setParams(params);
        event.setExclusionId(instanceId);
        if (CommonEvent.REQUEST_SUBMIT.isMatch(operation))
        	event.setPriority(EventPriority.ACTIVITY_INSTANCE_REQUEST_SUBMIT.getValue());
        eventManager.addEvent(event);
    }

    protected ScheduledFuture<?> invokePollingThread() {
        ScheduledFuture<?> future = executors.get(EXEC.POLLING).schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    pollingStatus();
                } catch (Exception e) {
                    logger.error("invokePollingThread caused exception: ", e);
                }
            }
        }, 0, TimeUnit.SECONDS);
        return future;
    }

    private void deleteResourceRequest(String instanceId) {
        List<String> filters = new ArrayList<String>();
        filters.add("occupantId" + FilterOperatorEnum.EQUAL.toSymbol() + instanceId);
        this.resourceManager.deleteResourceRequests(null, filters, null, null);
/*		filters = new ArrayList<String>();
        filters.add("receiverId" + FilterOperatorEnum.EQUAL.toSymbol() + instanceId);
        this.resourceManager.deleteResourceRequests(null, filters, null, null);*/
    }

    // 暫定。DAOの使い方としてはいけてない(Instaceとしてアクセスしてない)ですが、急ぎなので。
    @Override
    public ActivityInstanceModel deleteInstance(String instanceId) {
        ActivityInstanceModel model = null;
        // InstanceIdで同期を取る
        synchronized (locks.get(instanceId)) {
            model = this.getInstance(instanceId);
            this.deleteResourceRequest(instanceId);
            model = this.activityInstanceDao.deleteModel(model);
            if (model == null) {
                throw new MwaError(MWARC.INVALID_INPUT_INSTANCE_ID);
            } else {
                //this.operateActivityCore(model.getId(), SystemEvent.NOTIFY_INSTANCE_DELETION.getName(), null); //成否問わず
                logger.info("deleted activityInstance: instanceId=" + instanceId);
            }
        }
        return model;
    }
    @Override
    public ActivityInstanceCollection deleteInstances(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        ActivityInstanceCollection models = this.activityInstanceDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
        if (filters == null || filters.isEmpty()) {
        	//条件未指定の場合、最上位インスタンスのみ削除対象。子供はカスケードで処理
        	filters = new ArrayList<String>();
        	filters.add("parentId" + FilterOperatorEnum.EQUAL.toSymbol() + "");
        }

        if (models != null) {
            for (ActivityInstanceModel model : models.getModels()) {
            	try {
            		this.deleteInstance(model.getId());
            	} catch (NullPointerException e) {
            		//無視する
            	} catch (Exception e) {
            		//削除を続行
            		logger.error("Instance deletion caused exception e=" + e + ", message=" + e.getMessage(), e);
            	}
            }
        }
        return models;
    }

    //無駄なevent発行回避のためterminatedの場合STEP_DONEを発行しない
    protected ActivityInstanceModel updateInstanceCore(ActivityInstanceModel model, boolean isAlreadyTerminated) {
        ActivityInstanceModel result = this.activityInstanceDao.updateModel(model);
        if(result == null) {
            result = model;
        }

        if(model.getProperties() != null){
            for(ActivityInstancePropertyModel property : this.createInstancePropertyModels(model.getId(), model.getProperties()).getModels()){
                this.activityInstancePropertyDao.updateModel(property);
            }
        }

        if (result != null && result.getStatus().isTerminated() && !isAlreadyTerminated) {
            this.deleteResourceRequest(model.getId());
            {
                if (model.getParentId() != null) {
                	ActivityInstanceModel parentModel = this.getInstance(model.getParentId());
                	if (parentModel != null) {
	                	IActivityTemplate parentTemplate = getKnowledgeBase().getActitvityTemplateService(parentModel.getTemplateId());
	                	//ProcessEngineに孫InstanceがSTEP_DONEするのを避けるために親がWFのもののみSTEP_DONEを有効にしたが
	                	//STEP_DONEをTriggerに動くGenericTranscodeTaskなどが影響を受けたため条件を改める
	                	if (parentTemplate != null && ActivityType.WORKFLOW.equals(parentTemplate.getType())) {
	                    	//Callback処理
	                        EventModel event = new EventModel();
	                        event.setName("STEP_DONE");
	                        event.setTargetId(model.getParentId());
	                        event.setTargetType(EventTargetType.WORKFLOW.name());
	                        event.setPriority(EventPriority.WORKFLOW_STEP_DONE.getValue());
	                        event.setExclusionId(model.getParentId());	////親WFのevent処理になるので
	                        Map<String, Object> params = new HashMap<>();
	                        params.put(PresetParameter.ActivityInstanceId.getKey(), model.getId()); //targetIdはStepのIDを伝搬
	                        event.setParams(params);
	                        eventManager.addEvent(event);
	                	} else if (parentTemplate != null && !ActivityType.WORKFLOW.equals(parentTemplate.getType())) {
	                		//ここでSTEP_DONE処理内で行う親へのCONFIRM_STATUS呼び出しを別途実装（workflow-managementとの一連の処理は整理したいが・・）
	                	    if(!parentTemplate.getSyncFlag()) {
	                            this.operateActivityAsync(model.getParentId(), CommonEvent.CONFIRM_STATUS.getName(), null);
	                	    }
	                	}
                	} else {
                		logger.warn("Can't find out parent instance: id=" + model.getParentId());
                	}
                }
            }
        }

        return result;
    }

    protected ActivityInstanceModel registerInstnaceModel(ActivityInstanceModel model, boolean isExecutor) {
        ActivityInstanceModel duplicateModel = null;
        if (model.getParentId() != null && model.getName() != null) {
            try {
                duplicateModel = this.getChildInstance(model.getParentId(), model.getName());
            } catch (MwaError e) {
                if (!MWARC.INVALID_INPUT_LOCAL_NAME.equals(e.getMessage())) {
                    //do nothing
                } else {
                    throw e;
                }
            }
        }
        if (duplicateModel == null) {
            //DB登録(Deviceに投げる前にDB登録する。非同期実行のため）: VolatileであればDB登録しない
            model.getInputDetails().put(PresetParameter.NumberOfRuns.getKey(), "1");
            this.activityInstanceDao.addModel(model, isExecutor);
            if(model.getProperties() != null){
                for(ActivityInstancePropertyModel property : this.createInstancePropertyModels(model.getId(), model.getProperties()).getModels()){
                    this.activityInstancePropertyDao.addModel(property);
                }
            }
        } else if (!duplicateModel.getStatus().isTerminated()) {
            logger.error("Duplicate instance still running: id=" + duplicateModel.getId());
            throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
        } else {
            //ParentId+Nameの重複があれば、新規インスタンスで上書き、ワークフローのループ用（★存在確認→Updateの流れはTransaction管理をしたい）
            //model.setId(duplicateModel.getId());
            String countStr = (String)duplicateModel.getInputDetails().get(PresetParameter.NumberOfRuns.getKey());
            if (countStr == null)
                countStr = "0";
            model.getInputDetails().put(PresetParameter.NumberOfRuns.getKey(), "" + (Long.parseLong(countStr) + 1));
            //this.activityInstanceDao.updateModel(model);
            this.activityInstanceDao.deleteModel(duplicateModel);
            this.activityInstanceDao.addModel(model, isExecutor);
            if(model.getProperties() != null){
                for(ActivityInstancePropertyModel property : this.createInstancePropertyModels(model.getId(), model.getProperties()).getModels()){
                    this.activityInstancePropertyDao.updateModel(property);
                }
            }
        }

        logger.info("\t-> activityInstanceId=" + model.getId());
        return model;
    }
    protected ActivityInstanceModel createInstanceModel(String activityInstanceId, IActivityTemplate template, Map<String, Object> params) {
        ActivityInstanceModel model = null;
        boolean hasError = false;

        if (activityInstanceId == null) {
            // IDの生成
            activityInstanceId = createActivityInstanceId();
        }

        if (params == null) {
            throw new MwaError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
        }

        String parentInstanceId = (String) params.get(PresetParameter.ParentInstanceId.getKey());
        Object localNameObj = params.get(PresetParameter.LocalName.getKey());
        String priorityStr = (String)params.get(PresetParameter.ActivityInstancePriority.name());
        Integer priority = 50;
        try { priority = Integer.parseInt(priorityStr); } catch (Exception e) {}

        if (template == null) {
            throw new MwaError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
        } else {
            //checkInputParameter(template.getInputs(), params);
            String localName = (localNameObj == null) ? null : localNameObj.toString();
            logger.info("createActivityInstance: activityTemplateName=" + template.getName() + ", parentInstanceId=" + parentInstanceId + ", localName=" + localName);

            //ToDo: workflowのケース

            model = new ActivityInstanceModel();
            model.setId(activityInstanceId);
            model.setParentId(parentInstanceId);
            model.setName(localName);
            model.setInputDetails(params);
            model.setProgress(0);
            model.setTemplateId(template.getId());
            model.setTemplateName(template.getName());
            model.setTemplateVersion(template.getVersion());
            model.setCreateTime(System.currentTimeMillis());
            model.setPriority(priority);
        }

        return model;
    }

    protected ActivityInstancePropertyCollection createInstancePropertyModels(String activityInstanceId, Map<String, Object> properties) {


        if (activityInstanceId == null || properties == null) {
            return null;
        }

        ActivityInstancePropertyModel model = null;
        List<ActivityInstancePropertyModel> modelList = new ArrayList<ActivityInstancePropertyModel>();

        for(Map.Entry<String, Object> property : properties.entrySet()) {
            model = new ActivityInstancePropertyModel();
            model.setPropertyId(createActivityInstanceId());
            model.setId(activityInstanceId);
            model.setName(property.getKey());
            model.setValue( (property.getValue() != null) ? property.getValue().toString() : null);
            modelList.add(model);
        }

        return new ActivityInstancePropertyCollection(modelList);
    }

    protected void checkInputParameter(List<? extends IParameterDefinition> inputDefs, Map<String, Object> params) {
        for (IParameterDefinition pDef : inputDefs) {
            if (params == null) {
                throw new MwaError(MWARC.INVALID_INPUT);
            } else {
                String valueStr = (String)params.get(pDef.getKey());
                if (pDef.getRequired() && valueStr == null) {
                    throw new MwaError(MWARC.INVALID_INPUT_REQUIRED_PARAM_NOT_FOUND);
                } else {
                    try {
                        pDef.getType().fromString(valueStr);
                    } catch (Exception e) {
                        logger.warn("Invalid input: key=" + pDef.getKey() + ", value=" + valueStr);
                        throw new MwaError(MWARC.INVALID_INPUT);
                    }
                }
            }
        }
    }

    @Override
    public ActivityInstanceCollection getInstances(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
        ActivityInstanceCollection models;
        try {
            models = this.activityInstanceDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
            for(ActivityInstanceModel model : models.getModels()){
                getActivityInstanceProperties(model);
            }
        } catch (Throwable e) {
            logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
            throw new MwaError(MWARC.DATABASE_ERROR);
        }
        return models;
    }

    @Override
    public ActivityInstanceModel getInstance(String instanceId) {
        if (instanceId == null)
            throw new MwaError(MWARC.INVALID_INPUT_INSTANCE_ID);

        ActivityInstanceModel result = null;
        if (instanceId != null) {
            try {
                result = this.activityInstanceDao.getModel(instanceId, null);
                result = getActivityInstanceProperties(result);
                //Collection = this.activityInstanceDao.getInstances(QueryCriteriaGenerator.getQueryCriteria(QueryCriteria.class, "templateType", true, filter, -1, -1));
            } catch (Throwable e) {
                logger.error("getInstance: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
                throw new MwaError(MWARC.DATABASE_ERROR);
            }
        }
        return result;
    }

    private ActivityInstanceModel getActivityInstanceProperties(ActivityInstanceModel model){

        if(model != null && model.getId() != null ){

            List<String> propertyFilter = new ArrayList<String>();
            propertyFilter.add("id" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId());

            ActivityInstancePropertyCollection models = this.activityInstancePropertyDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, propertyFilter, null, null, null));

            Map<String, Object> properties = new HashMap<String, Object>();
            for(ActivityInstancePropertyModel property : models.getModels()){
                properties.put(property.getName(), property.getValue());
            }

            if(properties.size() > 0){
                model.setProperties(properties);
            }
        }
        return model;
    }

    @Override
    public ActivityInstanceCollection getChildInstances(String parentInstanceId) {
        ActivityInstanceCollection models = null;
        try {
            models = this.activityInstanceDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, Arrays.asList("parentId" + FilterOperatorEnum.EQUAL.toSymbol() + parentInstanceId), null, null, null));
        } catch (Throwable e) {
            logger.error("getChildInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
            throw new MwaError(MWARC.DATABASE_ERROR);
        }

        if (models == null || models.getModels() == null || models.getModels().isEmpty()) {
            if (!existInstance(parentInstanceId)) {
                throw new MwaError(MWARC.INVALID_INPUT_INSTANCE_ID);
            }
        }

        return models;
    }

    @Override
    public ActivityInstanceModel getChildInstance(String parentInstanceId, String localName) {
        if (parentInstanceId == null)
            throw new MwaError(MWARC.INVALID_INPUT_INSTANCE_ID);
        if (localName == null)
            throw new MwaError(MWARC.INVALID_INPUT_LOCAL_NAME);

        ActivityInstanceModel result = null;
        List<String> filter = new ArrayList<String>();
        filter.add("parentId" + FilterOperatorEnum.EQUAL.toSymbol() + parentInstanceId);
        filter.add("name" + FilterOperatorEnum.EQUAL.toSymbol() + localName);
        // 暫定でtemplateTypeでソート（先に子どもから状態確認したいのでtask→workflow）
        try {
            result = this.activityInstanceDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, filter, null, null, null)).first();
        } catch (Throwable e) {
            logger.error("e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
            throw new MwaError(MWARC.DATABASE_ERROR);
        }

        if (result == null) {
            if (existInstance(parentInstanceId)) {
                // parentは取得可能なので、localNameが悪いとみなす。直接Workflow上取り得るLocalNameかどうか判定すべきだがとりあえず。
                throw new MwaError(MWARC.INVALID_INPUT_LOCAL_NAME);
            } else {
                throw new MwaError(MWARC.INVALID_INPUT_INSTANCE_ID);
            }
        }
        return result;
    }

    private boolean existInstance(String instanceid) {
        return this.getInstance(instanceid) != null;
    }

    public void pollingStatus() {
        List<String> ids = ((com.sony.pro.mwa.activity.repository.database.ActivityInstanceDaoImpl)this.activityInstanceDao).getPollingTargetIds();
        for (String id: ids) {
            //ここではCONFIRM_STATUSのeventを発行するだけ
            //confirm自体の並列化や、他eventとの排他はeventManagerに任せる
            this.operateActivityAsync(id, CommonEvent.CONFIRM_STATUS.getName(), null);
        }
        //これ以上は投げても受け取り手がいないのでここでハンドルする
        return;
    }

    protected String createActivityInstanceId() {
        return java.util.UUID.randomUUID().toString();
    }
}
