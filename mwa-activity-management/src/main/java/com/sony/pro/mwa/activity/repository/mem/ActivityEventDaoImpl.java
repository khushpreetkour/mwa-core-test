package com.sony.pro.mwa.activity.repository.mem;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.activity.repository.ActivityEventDao;
import com.sony.pro.mwa.activity.repository.database.query.ActivityEventQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.activity.ActivityEventCollection;
import com.sony.pro.mwa.model.activity.ActivityEventModel;
import com.sony.pro.mwa.model.activity.ActivityTemplateModel;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

//@Repository
public class ActivityEventDaoImpl implements ActivityEventDao {

	Map<String, ActivityEventModel> map;
	private static Integer counter = 0;
	
	public ActivityEventDaoImpl() {
		this.map = new HashMap<>();
	}
	
	protected Integer createId() {
		synchronized (counter) {
			counter++;
		}
		return counter;
	}

	@Override
	public ActivityEventCollection getModels(QueryCriteria criteria) {
		ActivityEventCollection result = new ActivityEventCollection();
		result.setModels(new ArrayList<ActivityEventModel>(map.values()));
		return result;
	}

	@Override
	public ActivityEventModel getModel(String id, Principal principal) {
		// TODO Auto-generated method stub
		return map.get(id);
	}

	@Override
	public ActivityEventModel addModel(ActivityEventModel model) {
		model.setId(createId());
		return map.put(model.getId().toString(), model);
	}

	@Override
	public ActivityEventModel updateModel(ActivityEventModel model) {
		return null;
	}

	@Override
	public ActivityEventModel deleteModel(ActivityEventModel model) {
		return map.remove(model.getId().toString());
	}

}
