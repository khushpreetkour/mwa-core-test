package com.sony.pro.mwa.activity.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.activity.repository.ActivityEventDao;
import com.sony.pro.mwa.activity.repository.database.query.ActivityEventQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.model.activity.ActivityEventCollection;
import com.sony.pro.mwa.model.activity.ActivityEventModel;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ActivityEventDaoImpl extends DatabaseBaseDao<ActivityEventModel, ActivityEventCollection, QueryCriteria> implements ActivityEventDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

    public ActivityEventCollection getModels(QueryCriteria criteria) {
        return super.collection(criteria);
    }

    public ActivityEventModel getModel(String id, Principal principal) {
        return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
    }

    private static final String INSERT_SQL =
            "INSERT INTO mwa.activity_event(" + 
            		"  activity_event_activity_instance_id" + 
            		", activity_event_name" +
            		", activity_event_parameter" +
            		", activity_event_priority" +
            		", activity_event_created_time" +
            		") " +
            "VALUES (" +
                    "?, ?, ?, ?, current_timestamp" +
                    ") ";


    public ActivityEventModel addModel(final ActivityEventModel model) {
        final String insertSql = INSERT_SQL;
        final long currentTime = System.currentTimeMillis();
        KeyHolder keyHolder = new GeneratedKeyHolder();

        int insertedRowCount = jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                        PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
                        int colNum = 1;
                        
                        ps.setObject(colNum++, UUID.fromString(model.getInstanceId()));
                        ps.setString(colNum++, model.getOperation());
                        {
	                        if (model.getParams() != null)
	                        	ps.setString(colNum++, Converter.mapToJson(model.getParams()));
	                        else
	                        	ps.setNull(colNum++, Types.VARCHAR);
                        }
                        ps.setInt(colNum++, model.getPriority());
                        
                        logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
                        return ps;
                    }
                },
                keyHolder);

        ActivityEventModel addedInstance =model;
        return insertedRowCount != 0 ? addedInstance : null;
    }

    private static final String UPDATE_SQL =
            "UPDATE " +
                    "mwa.activity_event " +
            "SET " +
                    "activity_event_activity_instance_id = ?, " +
                    "activity_event_name = ?, " +
                    "activity_event_parameter = ? " +
            "WHERE " +
                    "activity_event_id = ? ";

    public ActivityEventModel updateModel(final ActivityEventModel model) {
        throw new RuntimeException("Update operaion is not supported for mwa.activity_event");
        
/*        final String updateSql = UPDATE_ACTIVITY_EVENT_TABLE;
        int updatedRowCount = jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                        PreparedStatement ps = conn.prepareStatement(updateSql);
                        int colNum = 1;
                        
                        
                        return ps;
                    }
                });

        return updatedRowCount != 0 ? acti : null;*/
    }


    private static final String DELETE_SQL =
            "DELETE FROM " +
                    "mwa.activity_event " +
            "WHERE " +
                    "activity_event_id = ? ";

    public ActivityEventModel deleteModel(ActivityEventModel model) {
        String deleteSql = DELETE_SQL;

        Object[] paramArray = new Object[]{model.getId()};
        if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
            return model;
        }
        return null;
    }

	@Override
	protected IQuery<ActivityEventModel, ActivityEventCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
        return new ActivityEventQuery(jdbcTemplate);
	}
}
