package com.sony.pro.mwa.activity.repository.database.query;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.activity.framework.internal.GeneralStateImpl;
import com.sony.pro.mwa.internal.utils.InternalConverter;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.ColumnUtils;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;
import com.sony.pro.mwa.service.activity.IState;
import com.sony.pro.mwa.service.activity.StatusDetails;

public class ActivityInstanceQuery extends Query<ActivityInstanceModel, ActivityInstanceCollection, QueryCriteria> {

    public enum COLUMN {
        ID("i.activity_instance_id", "id", UUID.class, true),
        TEMPLATE_ID("i.activity_template_id", "templateId", UUID.class, true),
        PRIORITY("i.activity_instance_priority", "priority", Integer.class, true),
        STATUS("i.activity_instance_status", "status_name", String.class, true),
        STATUS_DETAILS("i.activity_instance_status_details", "statusDetails", String.class, false),
        DEV_RESPONSE("i.activity_instance_device_response_code", "deviceResponse", String.class, true),
        DEV_RESPONSE_DETAILS("i.activity_instance_device_response_details", "deviceResponseDetails", String.class, true),
        PROGRESS("i.activity_instance_progress", "progress", Integer.class, true),
        INPUT_DETAILS("i.activity_instance_input_details", "inputDetails", SQLXML.class, false),
        OUTPUT_DETAILS("i.activity_instance_output_details", "outputDetails", SQLXML.class, false),
        START_TIME("i.activity_instance_start_time", "startTime", Timestamp.class, true),
        END_TIME("i.activity_instance_end_time", "endTime", Timestamp.class, true),
        UPDATE_TIME("i.activity_instance_update_time", "updateTime", Timestamp.class, true),
        NAME("i.activity_instance_name", "name", String.class, true),
        PARENT_ID("i.activity_instance_parent_instance_id", "parentId", UUID.class, true),
        IS_TERMINATED("i.st_terminated", "status_terminated", Boolean.class, true),
        IS_STABLE("i.st_stable", "status_stable", Boolean.class, true),
        IS_ERROR("i.st_error", "status_error", Boolean.class, true),
        IS_CANCEL("i.st_cancel", "status_cancel", Boolean.class, true),
        TEMPLATE_NAME("j.activity_template_name", "templateName", String.class, true),
        TEMPLATE_VERSION("j.activity_template_version", "templateVersion", String.class, true),
        TEMPLATE_TYPE("k.activity_template_type_name", "templateType", String.class, true),
        CREATE_TIME("i.activity_instance_create_time", "createTime", Timestamp.class, true),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    public enum H2_COLUMN {
        ID("i.activity_instance_id", "id", UUID.class, true),
        TEMPLATE_ID("i.activity_template_id", "templateId", UUID.class, true),
        PRIORITY("i.activity_instance_priority", "priority", Integer.class, true),
        STATUS("i.activity_instance_status", "status_name", String.class, true),
        STATUS_DETAILS("i.activity_instance_status_details", "statusDetails", String.class, false),
        DEV_RESPONSE("i.activity_instance_device_response_code", "deviceResponse", String.class, true),
        DEV_RESPONSE_DETAILS("i.activity_instance_device_response_details", "deviceResponseDetails", String.class, true),
        PROGRESS("i.activity_instance_progress", "progress", Integer.class, true),
        INPUT_DETAILS("i.activity_instance_input_details", "inputDetails", Clob.class, false),
        OUTPUT_DETAILS("i.activity_instance_output_details", "outputDetails", Clob.class, false),
        START_TIME("i.activity_instance_start_time", "startTime", Timestamp.class, true),
        END_TIME("i.activity_instance_end_time", "endTime", Timestamp.class, true),
        UPDATE_TIME("i.activity_instance_update_time", "updateTime", Timestamp.class, true),
        NAME("i.activity_instance_name", "name", String.class, true),
        PARENT_ID("i.activity_instance_parent_instance_id", "parentId", UUID.class, true),
        IS_TERMINATED("i.st_terminated", "status_terminated", Boolean.class, true),
        IS_STABLE("i.st_stable", "status_stable", Boolean.class, true),
        IS_ERROR("i.st_error", "status_error", Boolean.class, true),
        IS_CANCEL("i.st_cancel", "status_cancel", Boolean.class, true),
        TEMPLATE_NAME("j.activity_template_name", "templateName", String.class, true),
        TEMPLATE_VERSION("j.activity_template_version", "templateVersion", String.class, true),
        TEMPLATE_TYPE("k.activity_template_type_name", "templateType", String.class, true),
        CREATE_TIME("i.activity_instance_create_time", "createTime", Timestamp.class, true),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private H2_COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }


    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String WITH_RECURSIVE_CLAUSE = "";

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " +
            "mwa.activity_instance AS i "+
            "LEFT JOIN mwa.activity_template AS j ON i.activity_template_id=j.activity_template_id LEFT JOIN mwa.activity_template_type AS k ON j.activity_template_type_id=k.activity_template_type_id " +
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {


        Map<String, Column> map = new HashMap<>();
    	if(DatabaseEnum.H2SQL.equals(getDatabase())){
            for (H2_COLUMN enumColumn : H2_COLUMN.values()) {
                map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
            }

    	}else{
            for (COLUMN enumColumn : COLUMN.values()) {
                map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
            }
    	}
        columnMap = Collections.unmodifiableMap(map);
    }

    public ActivityInstanceQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }

	@Override
    protected RowMapper<ActivityInstanceModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<ActivityInstanceModel> {

        public ActivityInstanceModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        	ActivityInstanceModel  model = new ActivityInstanceModel();
        	model.setTemplateId(rs.getString(COLUMN.TEMPLATE_ID.alias));
        	model.setTemplateName(rs.getString(COLUMN.TEMPLATE_NAME.alias));
        	model.setTemplateVersion(rs.getString(COLUMN.TEMPLATE_VERSION.alias));
        	model.setId(rs.getString(COLUMN.ID.alias));
        	{
	        	String stName = rs.getString(COLUMN.STATUS.alias);
	        	boolean stTerminated = rs.getBoolean(COLUMN.IS_TERMINATED.alias);
	        	boolean stStable = rs.getBoolean(COLUMN.IS_STABLE.alias);
	        	boolean stError = rs.getBoolean(COLUMN.IS_ERROR.alias);
	        	boolean stCancel = rs.getBoolean(COLUMN.IS_CANCEL.alias);

        		IState status = new GeneralStateImpl(stName, stTerminated, stStable, stError, stCancel) {
        		};
	        	model.setStatus(status);
        	}
        	String statusDetails = rs.getString(COLUMN.STATUS_DETAILS.alias);
        	String deviceResponse = rs.getString(COLUMN.DEV_RESPONSE.alias);
           	String deviceResponseDetails = rs.getString(COLUMN.DEV_RESPONSE_DETAILS.alias);
           	//★MWARCの名前をDBから取れないためnullをStatusのnameに入れている。nameを使ったロジックがないため実用上問題ないが改善はしたい
           	model.setStatusDetails(new StatusDetails(null, statusDetails, deviceResponse, deviceResponseDetails));
           	model.setProgress(rs.getInt(COLUMN.PROGRESS.alias));

           	if(DatabaseEnum.H2SQL.equals(getDatabase())){

        		String details = rs.getString(COLUMN.INPUT_DETAILS.alias);
        		try {

        			if (details != null) {
                		Map<String,Object> result = InternalConverter.xmlToMap(details);
                		model.setInputDetails(result);
        			}
        		} catch (Exception e) {
        			throw new RuntimeException(e);
        		}
        		details = rs.getString(COLUMN.OUTPUT_DETAILS.alias);
        		try {
        			if (details != null ) {
        				Map<String,Object> result = InternalConverter.xmlToMap(details);
        				model.setOutputDetails(result);

	        			//★暫定実装
	        			if (result != null)
	        				model.setPersistentData((String)result.get("PersistentData"));
        			}
        		} catch (Exception e) {
        			throw new RuntimeException(e);
        		}

           	}else{
        		SQLXML details = rs.getSQLXML(COLUMN.INPUT_DETAILS.alias);
        		try {

        			if (details != null && details.getString() != null) {
                		Map<String,Object> result = InternalConverter.xmlToMap(details.getString());
                		model.setInputDetails(result);
        			}
        		} catch (Exception e) {
        			throw new RuntimeException(e);
        		}
        		details = rs.getSQLXML(COLUMN.OUTPUT_DETAILS.alias);
        		try {
        			if (details != null && !details.getString().isEmpty()) {
        				Map<String,Object> result = InternalConverter.xmlToMap(details.getString());
        				model.setOutputDetails(result);

	        			//★暫定実装
	        			if (result != null)
	        				model.setPersistentData((String)result.get("PersistentData"));
        			}
        		} catch (Exception e) {
        			throw new RuntimeException(e);
        		}
        	}
        	model.setName(rs.getString(COLUMN.NAME.alias));
        	model.setParentId(rs.getString(COLUMN.PARENT_ID.alias));
        	model.setStartTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.START_TIME.alias));
        	model.setEndTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.END_TIME.alias));
        	model.setUpdateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.UPDATE_TIME.alias));
        	model.setCreateTime(ColumnUtils.parseLong(rs, columnMap, COLUMN.CREATE_TIME.alias));
        	model.setPriority(ColumnUtils.parseInt(rs, columnMap, COLUMN.PRIORITY.alias));

        	return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }
}
