drop table if exists mwa.activity_event cascade;
drop table if exists mwa.activity_instance cascade;
drop table if exists mwa.map_template_plugin cascade;
drop table if exists mwa.map_template_workflow cascade;
drop table if exists mwa.plugin cascade;
drop table if exists mwa.workflow cascade;
drop table if exists mwa.activity_template cascade;
drop table if exists mwa.activity_template_type cascade;

drop table if exists mwa.errorinfo cascade;
drop table if exists mwa.requestinfo cascade;
drop sequence if exists mwa.error_info_id_seq;
drop sequence if exists mwa.request_info_id_seq;


CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS mwa.activity_template_type
(
  activity_template_type_id integer NOT NULL primary key,
  activity_template_type_name character varying(64),
  constraint "activity_template_type_name_key" UNIQUE (activity_template_type_name)
);

CREATE TABLE IF NOT EXISTS mwa.plugin
(
  plugin_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  plugin_bundle_id integer NOT NULL,
  plugin_name character varying(256) NOT NULL,
  plugin_version character varying(256) NOT NULL,
  plugin_created_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  constraint "uq_plugin_name_and_version" UNIQUE (plugin_name, plugin_version)
);

CREATE TABLE IF NOT EXISTS mwa.workflow
(
  workflow_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  workflow_name character varying(256) NOT NULL,
  workflow_version character varying(256) NOT NULL,
  workflow_created_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  constraint "uq_workflow_name_and_version" UNIQUE (workflow_name, workflow_version)
);

CREATE TABLE IF NOT EXISTS mwa.activity_template
(
  activity_template_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_template_name character varying(256) NOT NULL,
  activity_template_version character varying(64) NOT NULL,
  activity_template_category character varying(64),
  activity_template_type_id integer NOT NULL,
    foreign key (activity_template_type_id) references mwa.activity_template_type(activity_template_type_id),
  constraint "activity_template_name_version_key" UNIQUE (activity_template_name, activity_template_version)
);

CREATE TABLE IF NOT EXISTS mwa.map_template_plugin
(
  activity_template_id uuid NOT NULL,
    foreign key (activity_template_id) references mwa.activity_template(activity_template_id) match simple on update no action on delete cascade,
  plugin_id uuid NOT NULL,
    foreign key (plugin_id) references mwa.plugin(plugin_id) match simple on update no action on delete cascade
);

CREATE TABLE IF NOT EXISTS mwa.map_template_workflow
(
  activity_template_id uuid NOT NULL,
    foreign key (activity_template_id) references mwa.activity_template(activity_template_id) match simple on update no action on delete cascade,
  workflow_id uuid NOT NULL,
    foreign key (workflow_id) references mwa.workflow(workflow_id) match simple on update no action on delete cascade
);

CREATE TABLE IF NOT EXISTS mwa.activity_provider_type
(
  activity_provider_type_id uuid NOT NULL primary key,
  activity_provider_type_name character varying(64) NOT NULL,
  activity_provider_type_model_number character varying(64) NOT NULL,
  constraint "uq_activity_provider_type" UNIQUE (activity_provider_type_name, activity_provider_type_model_number)
);

CREATE TABLE IF NOT EXISTS mwa.service_endpoint_type
(
  service_endpoint_type_id uuid NOT NULL primary key,
  service_endpoint_type_name character varying(256) NOT NULL,
  activity_provider_type_id uuid NOT NULL,
    foreign key (activity_provider_type_id) references mwa.activity_provider_type(activity_provider_type_id) match simple on update no action on delete cascade,
  constraint "uq_service_endpoint_type" UNIQUE (service_endpoint_type_name, activity_provider_type_id)
);

CREATE TABLE IF NOT EXISTS mwa.map_provider_type_template
(
  activity_provider_type_id uuid NOT NULL,
    foreign key (activity_provider_type_id) references mwa.activity_provider_type(activity_provider_type_id) match simple on update no action on delete cascade,
  activity_template_name character varying(256) NOT NULL,
  activity_template_version character varying(64) NOT NULL,
  primary key(activity_provider_type_id, activity_template_name, activity_template_version)
);

CREATE TABLE IF NOT EXISTS mwa.activity_provider
(
  activity_provider_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_provider_type_id uuid NOT NULL,
    foreign key (activity_provider_type_id) references mwa.activity_provider_type(activity_provider_type_id) match simple on update no action on delete cascade,
  activity_provider_name character varying(256) NOT NULL,
  activity_provider_active boolean,
  activity_provider_exposure boolean,
  constraint "uq_provider_name" UNIQUE (activity_provider_name)
);

CREATE TABLE IF NOT EXISTS mwa.activity_provider_property
(
  activity_provider_id uuid NOT NULL,
    foreign key (activity_provider_id) references mwa.activity_provider(activity_provider_id) match simple on update no action on delete cascade,
  activity_provider_property_name character varying(64) NOT NULL,
  activity_provider_property_value character varying(256) NOT NULL,
  constraint "uq_provider_property" UNIQUE (activity_provider_id, activity_provider_property_name)
);

CREATE TABLE IF NOT EXISTS mwa.activity_provider_property_dictionary
(
  activity_provider_property_name character varying(64) NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_provider_property_type character varying(64) NOT NULL,
  activity_provider_property_category character varying(64) NOT NULL
);

CREATE TABLE IF NOT EXISTS mwa.service_endpoint
(
  service_endpoint_id uuid NOT NULL primary key,
  service_endpoint_provider_id uuid NOT NULL,
    foreign key (service_endpoint_provider_id) references mwa.activity_provider(activity_provider_id) match simple on update no action on delete cascade,
  service_endpoint_type character varying(256) NOT NULL,
  service_endpoint_host character varying(256) NOT NULL,
  service_endpoint_port integer,
  service_endpoint_user character varying(256),
  service_endpoint_password character varying(256),
  constraint "uq_service_endpoint" UNIQUE (service_endpoint_provider_id, service_endpoint_type)
);

CREATE TABLE IF NOT EXISTS mwa.activity_instance
(
  activity_instance_id uuid NOT NULL primary key DEFAULT uuid_generate_v4(),
  activity_template_id uuid NOT NULL,
  foreign key (activity_template_id) references mwa.activity_template(activity_template_id) match simple on update no action on delete no action,
  activity_instance_priority integer,
  activity_instance_status character varying(64) NOT NULL,
  activity_instance_status_details character varying(64),
  activity_instance_device_response_code character varying(256),
  activity_instance_device_response_details character varying(1024),
  activity_instance_progress integer,
  activity_instance_input_details xml,
  activity_instance_output_details xml,
  activity_instance_start_time timestamp with time zone,
  activity_instance_end_time timestamp with time zone,
  activity_instance_update_time timestamp with time zone,
  activity_instance_name character varying(256),
  activity_instance_parent_instance_id uuid,
  foreign key (activity_instance_parent_instance_id) references mwa.activity_instance(activity_instance_id) match simple on update no action on delete cascade,
  st_terminated boolean,
  st_stable boolean,
  st_error boolean,
  st_cancel boolean,
  constraint "activity_instance_name_and_parent_key" UNIQUE (activity_instance_name, activity_instance_parent_instance_id)
);

CREATE TABLE IF NOT EXISTS mwa.activity_instance_persistent
(
  activity_instance_id uuid NOT NULL primary key,
    foreign key (activity_instance_id) references mwa.activity_instance(activity_instance_id) match simple on update no action on delete cascade,
  activity_instance_persistent_data text NOT NULL
);

CREATE TABLE IF NOT EXISTS mwa.activity_event
(
  activity_event_id serial primary key NOT NULL,
  activity_event_activity_instance_id uuid,
    foreign key (activity_event_activity_instance_id) references mwa.activity_instance(activity_instance_id) match simple on update no action on delete cascade,
  activity_event_name character varying(256) NOT NULL,
  activity_event_parameter text,
  activity_event_priority integer,
  activity_event_created_time timestamp with time zone
);

CREATE OR REPLACE FUNCTION mwa.activity_instance_update_handler()
RETURNS trigger AS
$BODY$    BEGIN
IF TG_OP = 'INSERT' THEN
NEW.activity_instance_update_time := now();
ELSE
IF TG_OP = 'UPDATE' THEN
NEW.activity_instance_update_time := now();
END IF ;
END IF ;
RETURN NEW;
END ;
$BODY$
LANGUAGE plpgsql;
ALTER FUNCTION mwa.activity_instance_update_handler() OWNER TO postgres;

CREATE TRIGGER activity_insatnce_update_trigger
  BEFORE INSERT OR UPDATE
  ON mwa.activity_instance
  FOR EACH ROW
  EXECUTE PROCEDURE mwa.activity_instance_update_handler();

