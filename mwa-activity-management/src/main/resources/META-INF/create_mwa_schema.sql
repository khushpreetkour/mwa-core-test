    do
    $do$
    begin

    if exists (select 1 from information_schema.schemata where schema_name = 'mwa') then
        raise notice 'schema "mwa" already exists';
    else
        create schema mwa;
    end if;

    if exists (select 1 from information_schema.schemata where schema_name = 'mwa_pe') then
        raise notice 'schema "mwa_pe" already exists';
    else
        create schema mwa_pe;
    end if;

    end
    $do$
