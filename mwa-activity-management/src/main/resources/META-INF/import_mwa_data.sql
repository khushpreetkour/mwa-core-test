-- activity_template_type
INSERT INTO mwa.activity_template_type (activity_template_type_id, activity_template_type_name)
 SELECT 1, 'WORKFLOW'
 WHERE NOT EXISTS (SELECT activity_template_type_id FROM mwa.activity_template_type WHERE activity_template_type_name = 'WORKFLOW');

INSERT INTO mwa.activity_template_type (activity_template_type_id, activity_template_type_name)
 SELECT 2, 'TASK'
 WHERE NOT EXISTS (SELECT activity_template_type_id FROM mwa.activity_template_type WHERE activity_template_type_name = 'TASK');

INSERT INTO mwa.activity_template_type (activity_template_type_id, activity_template_type_name)
 SELECT 3, 'CONVERTER'
 WHERE NOT EXISTS (SELECT activity_template_type_id FROM mwa.activity_template_type WHERE activity_template_type_name = 'CONVERTER');
 