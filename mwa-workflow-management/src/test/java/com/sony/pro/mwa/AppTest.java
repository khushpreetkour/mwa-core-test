package com.sony.pro.mwa;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.drools.core.xml.SemanticModules;
import org.jbpm.bpmn2.xml.BPMNDISemanticModule;
import org.jbpm.bpmn2.xml.BPMNExtensionsSemanticModule;
import org.jbpm.bpmn2.xml.BPMNSemanticModule;
import org.kie.api.definition.process.Process;
import org.xml.sax.SAXException;

import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.workflow.jbpm.ProcessEngineImpl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
/*
		SemanticModules semanticModules = new SemanticModules();
		semanticModules.addSemanticModule(new BPMNSemanticModule());
		semanticModules.addSemanticModule(new BPMNExtensionsSemanticModule());
		semanticModules.addSemanticModule(new BPMNDISemanticModule());
    	org.jbpm.compiler.xml.XmlProcessReader test = new org.jbpm.compiler.xml.XmlProcessReader(semanticModules, Thread.currentThread().getContextClassLoader());
    	try {
        	FileInputStream fio = new FileInputStream(new java.io.File ("C:\\MWA\\bpmns\\test-process.bpmn") );
			List<Process> processes = test.read(fio);
			for (Process process : processes) {
				System.out.println("process: id=" + process.getId() + ", name=" + process.getName() + ", version=" + process.getVersion() );
				System.out.println("meta= " + process.getMetaData().toString());
			}
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	*/
/*		Map<String, Object> params = new HashMap<String, Object>();
    	//WorkflowManager.getInstance().startWorkflow("com.sony.pro.mwa.activity.workflow.StandardWorkflow", params);
    	WorkflowManager.getInstance().startWorkflow("mwa-wf-iap-dig-ingest", params);
        assertTrue( true );*/
    }
}
