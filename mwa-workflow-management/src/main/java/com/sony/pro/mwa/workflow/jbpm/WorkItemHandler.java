package com.sony.pro.mwa.workflow.jbpm;

import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.internal.executor.api.CommandContext;

import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.workflow.WorkflowManagerImpl;


public class WorkItemHandler implements org.kie.api.runtime.process.WorkItemHandler {

    protected String activityTemplateName;
    protected String activityTemplateVersion;
    protected boolean syncFlag;
    protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

    public WorkItemHandler(String activityTemplateName, String activityTemplateVersion, boolean syncFlag) {
        this.activityTemplateName = activityTemplateName;
        this.activityTemplateVersion = activityTemplateVersion;
        this.syncFlag = syncFlag;
    }
    public boolean isSync() { return syncFlag; }

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

        logger.debug("executeWorkItem start: ");

        // extract parameters from BPMN
        //Map<String, Object> params = new java.util.HashMap<>(workItem.getParameters());
        //jBPM上のパラメータをMWA用のMapに詰め直し
        Map<String, Object> params = new java.util.HashMap<>(workItem.getParameters());
        params.put(InternalKeyForJbpm.ProcessInstanceId.key(), workItem.getProcessInstanceId());
        params.put(InternalKeyForJbpm.WorkItemId.key(), workItem.getId());
        String localName = workItem.getName();
        WorkItemNodeAttribute attribute;
        {
            attribute = WorkflowManagerImpl.getInstance().getProcessEngine().getWorkItemNodeAttribute(workItem.getProcessInstanceId(), workItem.getId());
            if (attribute == null) {
                logger.error("Cannot obtain work item node attribute from BPMN!!!");
                throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
            } else {
                localName = attribute.getName();
            }

        }
        params.put(PresetParameter.LocalName.getKey(), localName);
        String parentInstanceId = (String)params.get(PresetParameter.ParentInstanceId.getKey());
        if (parentInstanceId == null || parentInstanceId.isEmpty()) {
            parentInstanceId = WorkflowManagerImpl.getInstance().getProcessEngine().getWorkflowAttribute(workItem.getProcessInstanceId()).getInstanceId();
            params.put(PresetParameter.ParentInstanceId.getKey(), parentInstanceId);
        }

        //submit
        OperationResult result = null;
        try {
        	//MWAにActivityを投げ込み
            result = WorkflowManagerImpl.getInstance().getActivityManager().createInstance(this.activityTemplateName, this.activityTemplateVersion, params);
            logger.debug("executeWorkItem requested: " + result.get(PresetParameter.ActivityInstanceId.getKey()) + ", activity=" + activityTemplateName);

            ActivityInstanceModel model = null;
            //Sync Instance Handling
            //Prepare instance status data for workflow handling
            if (result != null && result.containsKey(PresetParameter.ActivityInstanceId)) {
                String instanceId = (String)result.get(PresetParameter.ActivityInstanceId);
                model = WorkflowManagerImpl.getInstance().getActivityManager().getInstance(instanceId);
                if (model != null) {
                    result.put(PresetParameter.ActivityInstanceStatus, model.getStatus().getName());
                    if (model.getStatusDetails() != null && model.getStatusDetails().getResult() != null) {
                        result.put(PresetParameter.ActivityInstanceStatusDetails, model.getStatusDetails().getResult().name());
                        result.put(PresetParameter.ActivityInstanceDeviceResponse, model.getStatusDetails().getDeviceResponse());
                        result.put(PresetParameter.ActivityInstanceDeviceResponseDetails, model.getStatusDetails().getDeviceResponseDetails());
                    }
                }
            }

            if (model == null || model.getStatus().isTerminated()) {
                logger.debug("executeWorkItem done (sync) : workItem=" + workItem.getId() + ", actInstId=" + result.get(PresetParameter.ActivityInstanceId));
                manager.completeWorkItem(workItem.getId(), result);
            } else {
            	//非同期スレッド管理はMWA側に責務を移したのでここでは特にやることなし
            	//ステップの完了などはMWA側のインスタンス完了をトリガに進めます
                //Async Instance Handling
                //Create inputs for activity polling thread
            	/*
                CommandContext ctxCMD = new CommandContext();
                ctxCMD.setData(InternalKeyForJbpm.ProcessInstanceId.key(), workItem.getProcessInstanceId());
                ctxCMD.setData(InternalKeyForJbpm.WorkItemId.key(), workItem.getId());;
                ctxCMD.setData(PresetParameter.ActivityInstanceId.getKey(), result.get(PresetParameter.ActivityInstanceId));
                ctxCMD.setData(InternalKeyForJbpm.Retries.key(), "0");
                ctxCMD.setData(InternalKeyForJbpm.WorkItemAttribute.key(), attribute);
                */
                //ctxCMD.setData("callbacks", CommandForAsyncCallback.class.getName());
                
//                Long requestId = WorkflowManagerImpl.getInstance().getProcessEngine().getExecutorService().scheduleRequest(CommandForAsync.class.getName(), ctxCMD);
//                logger.debug("executeWorkItem done: requestId=" + requestId + ", actInstId=" + result.get(PresetParameter.ActivityInstanceId));
            }
        } catch (MwaError e) {
            logger.error("executeWorkItem failed: MWARC=" + e.getMWARC().code()+ "(" + e.getMWARC().name() + ")" + ", message=" + e.getMessage());
            //★エラーシグナル
            WorkflowManagerImpl.getInstance().getProcessEngine().sendErrorSignal(workItem.getProcessInstanceId());
            {
                if (result == null) {
                    result = OperationResult.newInstance(e);
                }
                result.put(PresetParameter.ActivityInstanceStatus, CommonState.ERROR.name());
                result.put(PresetParameter.ActivityInstanceStatusDetails, e.getMWARC().name());
            }
            manager.completeWorkItem(workItem.getId(), result);
        } catch (Exception e) {
            logger.error("executeWorkItem caused: exception=" + e.toString() + ", message=" + e.getMessage(), e);
            WorkflowManagerImpl.getInstance().getProcessEngine().sendErrorSignal(workItem.getProcessInstanceId());
            {
                if (result == null) {
                    result = OperationResult.newInstance(MWARC.SYSTEM_ERROR);
                }
                result.put(PresetParameter.ActivityInstanceStatus, CommonState.ERROR.name());
                result.put(PresetParameter.ActivityInstanceStatusDetails, MWARC.SYSTEM_ERROR.code());
            }
            manager.completeWorkItem(workItem.getId(), result);
        }
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        // Do nothing, notifications cannot be aborted

        //Instance自体のIdは入力値ではなく、Handler内では保持できないが、
        //入力値のParentIdとlocalNameでInstance特定可能なのでそちらを使う

        //子のインスタンスのCancelの責務はWorkflow側とする
        String parentId = (String)workItem.getParameter(PresetParameter.ParentInstanceId.getKey());
        String localName = workItem.getName();
        /*
        {
            localName = WorkflowManagerImpl.getInstance().getProcessEngine().getWorkItemNodeName(workItem.getProcessInstanceId(), workItem.getId());
            if (localName == null) {
                logger.error("Couldn't obtain localName from BPMN !!!!!");
                throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
            }
        }

        OperationResult result = WorkflowManagerImpl.getInstance().getActivityManager().operateActivity(parentId, localName, CommonEvent.REQUEST_CANCEL.getName(), null);
        String resultMsg = "result=" + ((result != null) ? result.get(PresetParameter.ErrorCode) : "null");
        */
        logger.info("abortWorkItem called: parent=" + parentId + ", localName=" + localName);

        //manager.abortWorkItem(workItem.getId());
    }
}
