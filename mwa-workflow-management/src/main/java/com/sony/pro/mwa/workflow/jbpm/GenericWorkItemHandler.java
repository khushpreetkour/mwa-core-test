package com.sony.pro.mwa.workflow.jbpm;

import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.internal.executor.api.CommandContext;

import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.workflow.WorkflowManagerImpl;
import com.sony.pro.mwa.workflow.utils.WorkflowUtils;


public class GenericWorkItemHandler extends WorkItemHandler {
	
	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	public GenericWorkItemHandler() {
		super(null,null,false);
	}
	
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		Map<String, Object> params = new java.util.HashMap<>(workItem.getParameters());
		this.activityTemplateName = (String)params.get(PresetParameter.ActivityTemplateName.getKey());
		this.activityTemplateVersion = (String)params.get(PresetParameter.ActivityTemplateVersion.getKey());
		
		//SyncFlagを取得するためWorkItemHandlerを取り出し。素直にKnowledgeBaseから取り出した方がよいかも。。。
		org.kie.api.runtime.process.WorkItemHandler wih = 
				WorkflowManagerImpl.getInstance().getProcessEngine().getWorkItem(WorkflowUtils.createWorkItemTaskName(activityTemplateName, activityTemplateVersion));
		if (wih instanceof WorkItemHandler) {
			this.syncFlag = ((WorkItemHandler)wih).isSync();
			//ここでの責務はTemplateを解決するのみ。実処理はお任せ
			super.executeWorkItem(workItem, manager);
		} else {
			if (wih == null) 
				logger.error("executeWorkItem failed: Can't resolve activityTemplate dynamically");
			else
				logger.error("executeWorkItem failed: Can't resolve activityTemplate dynamically or invalid workitem handler: " + wih.toString());
	        WorkflowManagerImpl.getInstance().getProcessEngine().sendErrorSignal(workItem.getProcessInstanceId());
        	OperationResult result = OperationResult.newInstance(MWARC.SYSTEM_ERROR);
			result.put(PresetParameter.ActivityInstanceStatus, CommonState.ERROR.name());
			result.put(PresetParameter.ActivityInstanceStatusDetails, MWARC.SYSTEM_ERROR.code());
	        manager.completeWorkItem(workItem.getId(), result);
		}
		return;
	}

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		// Do nothing, notifications cannot be aborted

		//Instance自体のIdは入力値ではなく、Handler内では保持できないが、
		//入力値のParentIdとlocalNameでInstance特定可能なのでそちらを使う
		
		//子のインスタンスのCancelの責務はWorkflow側とする
		String parentId = (String)workItem.getParameter(PresetParameter.ParentInstanceId.getKey());
		String localName = workItem.getName();
		/*
		{
			localName = WorkflowManagerImpl.getInstance().getProcessEngine().getWorkItemNodeName(workItem.getProcessInstanceId(), workItem.getId());
			if (localName == null) {
				logger.error("Couldn't obtain localName from BPMN !!!!!");
				throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
			}
		}

		OperationResult result = WorkflowManagerImpl.getInstance().getActivityManager().operateActivity(parentId, localName, CommonEvent.REQUEST_CANCEL.getName(), null);
		String resultMsg = "result=" + ((result != null) ? result.get(PresetParameter.ErrorCode) : "null");
		*/
		logger.info("abortWorkItem called: parent=" + parentId + ", localName=" + localName);
		
		//manager.abortWorkItem(workItem.getId());
	}
}
