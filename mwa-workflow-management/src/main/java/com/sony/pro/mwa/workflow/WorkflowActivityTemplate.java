package com.sony.pro.mwa.workflow;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.activity.framework.internal.IMwaActivityInternal;
import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.activity.template.IActivityFactory;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.parameter.ParameterDefinition;

public class WorkflowActivityTemplate extends ActivityTemplate {
	public WorkflowActivityTemplate(String id, String name, String version, List<ParameterDefinition> inputs, List<ParameterDefinition> outputs, String alias, String icon, String description) {
		super();
		
		this.setId(id);
		this.setName(name);			//workflow name(BPMNのparseで作れる)
		this.setVersion(version);		//workflow version(BPMNのparseで作れる)
		this.setType(ActivityType.WORKFLOW);
		this.setAlias(alias);
		this.setIcon(icon);
		this.setDescription(description);
		this.setTags(new ArrayList());
		
		this.inputs = inputs;	//BPMNのparseで作れる
		this.outputs = outputs;	//BPMNのparseで作れる
		this.factory = new IActivityFactory(){
			@Override
			public IMwaActivityInternal createInstance() {
				return new StandardWorkflow();
			}
		};
	}
	
	@Override
	public Boolean getSyncFlag() {
		return false;
	}
}
