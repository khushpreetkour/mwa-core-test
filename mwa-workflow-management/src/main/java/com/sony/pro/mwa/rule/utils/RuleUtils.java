package com.sony.pro.mwa.rule.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.definition.KnowledgePackage;
import org.drools.definition.rule.Rule;
import org.drools.io.ResourceFactory;
import org.xml.sax.SAXException;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.UUIDUtils;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.parameter.ParameterDefinition;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.rule.RuleActivityTemplate;
import com.sony.pro.mwa.service.activity.IActivityTemplate;

/**
 * @author Xi.Ou
 *
 */
public class RuleUtils {
    protected final static String RULE_TOKEN_SEPARATOR = "@";
    protected final static String WF_STEP_KEY_SEPARATOR = ".";
    protected final static String WF_OUTPUT_PREFIX = "OUTPUT.";

    public static final String XLS_TYPE_DECISION_RULE_EXT = "xls";
    public static final String DSL_TYPE_DECISION_RULE_EXT = "dsl";
    public static final String DSLR_TYPE_DECISION_RULE_EXT = "dslr";
    public static final String DRL_TYPE_DECISION_RULE_EXT = "drl";

    public static final String RULE_PARAMETER_STRING = "Parameter";

    private static MwaLogger logger = MwaLogger.getLogger(RuleUtils.class);

    public static boolean isOutputKey(String key) {
        return key != null && key.startsWith(WF_OUTPUT_PREFIX);
    }

    public static String decodeOutputKey(String key) {
        return (isOutputKey(key)) ? key.substring(WF_OUTPUT_PREFIX.length()) : key;
    }

    public static IActivityTemplate createTemplateFromRuleTable(String rulePath, ActivityType type)
            throws SAXException, IOException  {
        IActivityTemplate result = null;

        final DecisionTableConfiguration dtableconfiguration =
                KnowledgeBuilderFactory.newDecisionTableConfiguration();
        dtableconfiguration.setInputType(DecisionTableInputType.XLS);

        final KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

        switch(type) {
        case DTABLE:
            kbuilder.add(ResourceFactory.newFileResource(rulePath), ResourceType.DTABLE, dtableconfiguration);
            break;
        case DSL:
            kbuilder.add(ResourceFactory.newFileResource(rulePath), ResourceType.DSL);
            break;
        case DSLR:
            kbuilder.add(ResourceFactory.newFileResource(rulePath), ResourceType.DSLR);
            break;
        case DRL:
            kbuilder.add(ResourceFactory.newFileResource(rulePath), ResourceType.DRL);
            break;
        case WORKFLOW:
        case TASK:
        case CONVERTER:
        default:
            break;
        }

        // output parser error log
        KnowledgeBuilderErrors kErrors = kbuilder.getErrors();
        if (kErrors.size() == 0) {
            Collection<KnowledgePackage> knowledgePackages = kbuilder.getKnowledgePackages();
            Iterator<KnowledgePackage> kpItr = knowledgePackages.iterator();
            while (kpItr.hasNext()) {
                KnowledgePackage knowledgePackage = kpItr.next();
                String avoidName = "com.sony.pro.mwa.kbase.model.rule";
                if (knowledgePackage != null) {
                    if (!knowledgePackage.getName().equals(avoidName)) {
                        result = createTemplate(knowledgePackage, type);
                    }
                }
                else {
                    String message = "createTemplateFromRuleTable: It is a Empty rule.";
                    throw new MwaError(MWARC.INTEGRATION_INVALID_MWARC_FORMAT, "", message);
                }
            }
        }
        else {
            for (KnowledgeBuilderError error: kErrors) {
                logger.error(error.getMessage());
            }
            logger.error("Could not parse rule. file: " + FilenameUtils.getName(rulePath));
            String message = "Rule parse error.";
            throw new MwaError(MWARC.INTEGRATION_INVALID_MWARC_FORMAT, "", message);
        }

        return result;
    }

    protected static IActivityTemplate createTemplate(KnowledgePackage knowledgePackage, ActivityType type) {
        ActivityTemplate result = null;

        String packageName = knowledgePackage.getName();
        String version = "1";
        List<ParameterDefinition> inputs = new ArrayList<>();
        List<ParameterDefinition> outputs = new ArrayList<>();

        Collection<Rule> rules = knowledgePackage.getRules();
        Iterator<Rule> ruleItr = rules.iterator();
        while (ruleItr.hasNext()) {
            Rule rule = ruleItr.next();

            String[] ruleName = rule.getName().split("_");
            if (RULE_PARAMETER_STRING.equals(ruleName[0])) {
                String key = (String)rule.getMetaData().get("Key");
                String value = (String)rule.getMetaData().get("Value");
                ParameterDefinition pdef =
                        new ParameterDefinition().setKey(value).setRequired(false).setType(TypeString.instance());
                if ("version".equals(key)) {
                    version = value;
                }
                else if ("input".equals(key)) {
                    inputs.add(pdef);
                }
                else if ("output".equals(key)) {
                    pdef.setKey(decodeOutputKey(pdef.getKey()));
                    outputs.add(pdef);
                }

            }
        }

        String id = UUIDUtils.generateUUID(packageName + RULE_TOKEN_SEPARATOR + version);
        result = new RuleActivityTemplate(id, packageName, version, inputs, outputs).addTag(packageName);
        result.setType(type);

        return result;
    }

    public static String convertTemplateIdToProcessId(String templateId) {
        if (templateId != null)
            return "_" + templateId;
        else
            return null;
    }

}
