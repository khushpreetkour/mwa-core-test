package com.sony.pro.mwa.rule;

import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;

/**
 * @author Xi.Ou
 *
 */
public class ExecuteRuleActivity extends SyncTaskBase {

    Integer progress = 0;

    /* (非 Javadoc)
     * @see com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity#requestSubmitImpl(java.util.Map)
     */
    @Override
    protected OperationResult requestSubmitImpl(Map<String, Object> params) {
        OperationResult result = OperationResult.newInstance();

        if (params != null) {
            Map<String, Object> ruleParams = new HashMap<>(params);

            ruleParams.put(PresetParameter.ParentInstanceId.getKey(), this.getId());
            ruleParams.put(PresetParameter.ActivityTemplateId.getKey(), this.getTemplate().getId());
            try {
                String rulePackage = this.getTemplate().getName();
                result = RuleManagerImpl.getInstance().getProcessEngine().startRule(rulePackage, ruleParams);
                logger.info("requestSubmitImpl: templateName=" + rulePackage + ", type=" + this.getTemplate().getType());
            } catch (MwaError e) {
                throw e;
            } catch (Exception e) {
                String message = "requestSubmitImpl: exception=" + e.getMessage();
                logger.error(message, e);
                throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
            }
        } else {
            String message = "requestSubmitImpl: parameter is null.";
            logger.error(message);
            throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
        }

        return result;
    }

}
