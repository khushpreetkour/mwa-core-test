package com.sony.pro.mwa.workflow.jbpm;

import org.kie.api.runtime.process.ProcessInstance;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.workflow.WorkflowEvent;

public class JbpmMapper {
	public static IEvent convertEvent(Integer jbpmState) {
		if (jbpmState == null) {
			return WorkflowEvent.NOTIFY_COMPLETED;
		} else {
			switch (jbpmState) {
			case ProcessInstance.STATE_ACTIVE:
			case ProcessInstance.STATE_PENDING:
				return WorkflowEvent.NOTIFY_UPDATED;
			case ProcessInstance.STATE_COMPLETED:
				return WorkflowEvent.NOTIFY_COMPLETED;
			case ProcessInstance.STATE_SUSPENDED:
			case ProcessInstance.STATE_ABORTED:
			default:
				return CommonEvent.NOTIFY_ERROR;
			}
		}
	}
}
