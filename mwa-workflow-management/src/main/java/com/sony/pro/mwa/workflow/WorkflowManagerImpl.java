package com.sony.pro.mwa.workflow;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.kie.api.runtime.KieSession;
import org.kie.internal.executor.api.ExecutionResults;
import org.kie.internal.runtime.manager.SessionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.xml.sax.SAXException;

import com.sony.pro.mwa.internal.activity.IActivityManagerInternal;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.NamedLocks;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.rule.RuleManagerImpl;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.workflow.IRuleManager;
import com.sony.pro.mwa.service.workflow.IWorkflowManager;
import com.sony.pro.mwa.workflow.jbpm.EventListener;
import com.sony.pro.mwa.workflow.jbpm.InternalKeyForJbpm;
import com.sony.pro.mwa.workflow.jbpm.ProcessEngineImpl;
import com.sony.pro.mwa.workflow.utils.WorkflowUtils;

public class WorkflowManagerImpl implements IWorkflowManager {

    protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
    protected Map<String, IActivityTemplate> bpmnMapCache;

    private static WorkflowManagerImpl singleton = new WorkflowManagerImpl();

    ProcessEngineImpl processEngine;
    IActivityManagerInternal activityManager;
    RuleManagerImpl ruleManager;

    private static NamedLocks locks = new NamedLocks();

    Long processId;
    Map<String, Object> paramMap;

    @Autowired
    @Qualifier("processEngine")
    public void setProcessEngine(ProcessEngineImpl processEngine) {
        this.processEngine = processEngine;
        this.processEngine.addListener(new EventListener());
        // TODO
        RuleManagerImpl.getInstance().setProcessEngine(processEngine);
    }
    @Override
    public ProcessEngineImpl getProcessEngine() {
        return this.processEngine;
    }

    @Autowired
    @Qualifier("activityManager")
    public void setActivityManager(IActivityManagerInternal activityManager) {
        this.activityManager = activityManager;
        // TODO
        RuleManagerImpl.getInstance().setActivityManager(activityManager);
    }
    public IActivityManagerInternal getActivityManager() {
        return this.activityManager;
    }

    public static WorkflowManagerImpl getInstance() {
        return singleton;
    }

    private WorkflowManagerImpl() {
        this.bpmnMapCache = new HashMap<>();

        ruleManager = RuleManagerImpl.getInstance();
    }

    @Override
    public IRuleManager getRuleManager() {
        return ruleManager;
    }

    //本当はこっちを呼ぶべき。。。
    public OperationResult startWorkflow(final String workflowName, Map<String, Object> parameters) {
        if (true) {
            //Is this method deprecated??
            throw new MwaInstanceError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
        }
        logger.info("startWorkflow: workflowName=" + workflowName + ", parameters=");

        // Map<String, Object> paramMap = new HashMap<String, Object>();
        // Long processId = null;
        paramMap = new HashMap<String, Object>();

        if (parameters != null) {

            for (Map.Entry<String, Object> param : parameters.entrySet()) {
                logger.info("\t" + "key: " + param.getKey() + ", val: " + param.getValue());
                paramMap.put(param.getKey(), param.getValue());
            }
        }

        try {
            new Runnable() {
                @Override
                public void run() {
                    processId = processEngine.startProcess(workflowName, paramMap);
                }
            }.run();
            // processId = this.processEngine.startProcess(workflowName, paramMap);
            logger.info("\t\t-> " + "processId: " + processId);
        } catch (Exception e) {
            logger.error("startWorkflow failed", e);
        }

        OperationResult result = OperationResult.newInstance();
        result.put("ProcessId", processId);

        return result;
    }

    public void stopWorkflow(String workflowInstanceId) {
        logger.info("cancelWorkflow: workflowInstanceId=" + workflowInstanceId);
        return ;
    }

    @Override
    public IActivityTemplate getActivityTemplate(String bpmnPath) {

        IActivityTemplate result = null;

        result = this.bpmnMapCache.get(bpmnPath);

        if (result == null) {
            try {
                result = WorkflowUtils.createTemplateFromBpmnPath(bpmnPath);
                if (result != null)
                    this.bpmnMapCache.put(bpmnPath, result);
            } catch (java.io.FileNotFoundException e) {
                //logger.error("FileNotFoundException occurred: e=" + e.toString(), e);
                //logger.warn("FileNotFoundException occurred: e=" + e.toString());
            } catch (SAXException | IOException e) {
                logger.error("Exception occurred: e=" + e.toString(), e);
            }
        }

        return result;
    }

    @Override
    public void reloadProcessEngine(List<String> bpmnPaths) {
        this.processEngine.reloadBpmns(bpmnPaths);
        return;
    }

    @Override
    public void registerTemplate(String templateName, String templateVersion, boolean syncFlag) {
        String workItemName = WorkflowUtils.createWorkItemTaskName(templateName, templateVersion);
        this.getProcessEngine().registerWorkItem(workItemName, new com.sony.pro.mwa.workflow.jbpm.WorkItemHandler(templateName, templateVersion, syncFlag));
    }

    @Override
    public void notifyStepDone(String activityInstanceId) {
    	ActivityInstanceModel model = this.getActivityManager().getInstance(activityInstanceId);
    	if (model == null) {
    		logger.error("notifyStepDone input is invalid: id=" + activityInstanceId);
    		return;
    	}
    	
    	String processInstanceIdStr = (String)model.getInputDetails().get(InternalKeyForJbpm.ProcessInstanceId.key());
    	Long processInstanceId = (processInstanceIdStr != null) ? Long.parseLong(processInstanceIdStr) : null;
    	String workItemId = (String)model.getInputDetails().get(InternalKeyForJbpm.WorkItemId.key());
    	
        //Signal発行は先に行う。workitemを完了してしまうとProcessが先に進んでしまうため
        if (model.getStatus().isError()) {
        	if (processInstanceId == null) {
        		//通知できない
        	} else {
        		WorkflowManagerImpl.getInstance().getProcessEngine().sendErrorSignal(processInstanceId);
        	}
        }

        //結果のマッピング（ActivityResult -> Workflow）
        ExecutionResults executionResults = new ExecutionResults();
        if (model != null) {
            for (Entry<String, Object> entry : model.getOutputDetails().entrySet()) {
                executionResults.setData(entry.getKey(), entry.getValue());
            }
            {
                executionResults.setData(PresetParameter.ActivityInstanceStatus.getKey(), model.getStatus().getName());
                if (model.getStatusDetails() != null && model.getStatusDetails().getResult() != null) {
                    executionResults.setData(PresetParameter.ActivityInstanceStatusDetails.getKey(), model.getStatusDetails().getResult().name());
                    executionResults.setData(PresetParameter.ActivityInstanceDeviceResponse.getKey(), model.getStatusDetails().getDeviceResponse());
                    executionResults.setData(PresetParameter.ActivityInstanceDeviceResponseDetails.getKey(), model.getStatusDetails().getDeviceResponseDetails());
                }
            }
        }

        logger.debug("completeWorkItem: id=" + workItemId);
        if (workItemId == null) {
        	//最上位のWorklfowはworkItemIdがないのでスルー
        } else if (processInstanceId != null) {
        	try {

                synchronized (locks.get(processInstanceId)) {
                    KieSession ksession = WorkflowManagerImpl.getInstance().getProcessEngine().getKieSession(processInstanceId);
                    ksession.getWorkItemManager().completeWorkItem(Long.parseLong(workItemId), executionResults.getData());
                }

		        logger.info("[ydbg]complete processInstanceId=" + processInstanceId + ", workItemId=" + workItemId + ", activityInstanceId=" + activityInstanceId);
        	} catch (org.drools.persistence.SessionNotFoundException|org.kie.internal.runtime.manager.SessionNotFoundException e) {
        		//すでにセッションがないケース？process側が非同期ではないので基本的にないはずだが・・
        		logger.warn("Can't find process instance id=" + processInstanceId);
        	} catch (IllegalStateException e) {
        		if ( e.getCause() instanceof InvocationTargetException ) {
        			if (e.getCause().getCause() instanceof org.drools.persistence.SessionNotFoundException ||
        					e.getCause().getCause() instanceof org.kie.internal.runtime.manager.SessionNotFoundException ) {
	        			//同期のActivtyだけでBPMNが構成されていると、完了でここに入ってくるケースあり
	            		logger.info("Can't find process instance id (sync case)=" + processInstanceId);
        			} else
            			throw e;
        		} else
        			throw e;
        	}
        } else {
        	//do nothig
        	logger.debug("completeWorkItem: id=" + workItemId);
        }
    	//Step完了のタイミングで親WFの進捗更新のためCONFIRMイベントを実行（再度EventQueueに入れると遅いので）
    	if (model.getParentId() != null)
    		this.getActivityManager().operateActivity(model.getParentId(), CommonEvent.CONFIRM_STATUS.getName(), null);
    }
}
