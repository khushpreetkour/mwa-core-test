package com.sony.pro.mwa.workflow.jbpm;

import java.util.Map;

import org.drools.core.xml.ExtensibleXmlParser;
import org.jbpm.bpmn2.core.Message;
import org.jbpm.bpmn2.xml.ProcessHandler;
import org.xml.sax.SAXException;
import org.jbpm.compiler.xml.ProcessBuildData;
import org.jbpm.ruleflow.core.RuleFlowProcess;

public class MwaProcessHandler extends ProcessHandler {
    protected final static String ATTR_CANCEL_DISABLE = "_ATTR_CANCEL_DISABLE";
	public Object end(final String uri, final String localName, final ExtensibleXmlParser parser) throws SAXException {
		RuleFlowProcess result = (RuleFlowProcess)super.end(uri, localName, parser);
		Map<String, Object> messages = (Map<String, Object>)((ProcessBuildData) parser.getData()).getMetaData("Messages");
		if (messages != null) {
			Message attrCancelDisable = (Message)messages.get(ATTR_CANCEL_DISABLE);
			if (attrCancelDisable != null)
				result.setMetaData(ATTR_CANCEL_DISABLE, Boolean.TRUE);
		}
		return result;
	}
}
