package com.sony.pro.mwa.workflow;

import java.util.Arrays;

import com.sony.pro.mwa.service.activity.IState;

public enum WorkflowState implements IState {
                //stable	//terminated	//error		//cancel
    QUEUED		(true,		false,			false,		false),
    IN_PROGRESS	(true,		false,			false,		false),
    CANCELLING	(false,		false,			false,		true) {
        @Override
        protected String[] valueStrings() {
            String[] valueStrings = {"CANCELING", "CANCELLING"};
            return valueStrings;
        }
    },
    MANUAL_COMPLETED(true,		true,			false,		false) {
        @Override
        protected String[] valueStrings() {
            String[] valueStrings = {"MANUAL_COMPLETE", "MANUAL_COMPLETED"};
            return valueStrings;
        }
    },
    COMPLETED	(true,		true,			false,		false) {
        @Override
        protected String[] valueStrings() {
            String[] valueStrings = {"COMPLETE", "COMPLETED"};
            return valueStrings;
        }
    },
    ERROR		(true,		true,			true,		false),
    CANCELLED	(true,		true,			false,		true) {
        @Override
        protected String[] valueStrings() {
            String[] valueStrings = {"CANCEL", "CANCELED", "CANCELLED", "Pass End"};
            return valueStrings;
        }
    },
    @Deprecated
    CREATED		(true,		false,			false,		false),
    ;

    private boolean stableFlag;
    private boolean terminatedFlag;
    private boolean errorFlag;
    private boolean cancelFlag;

    private WorkflowState (boolean stableFlag, boolean terminatedFlag, boolean errorFlag, boolean cancelFlag) {
        this.stableFlag = stableFlag;
        this.terminatedFlag = terminatedFlag;
        this.errorFlag = errorFlag;
        this.cancelFlag = cancelFlag;
    }

    @Override
    public String getName() { return super.name(); }

    //Default
    @Override
    public boolean isTerminated() { return this.terminatedFlag; }
    @Override
    public boolean isStable() { return this.stableFlag; }
    @Override
    public boolean isError() { return this.errorFlag; }
    @Override
    public boolean isCancel() { return this.cancelFlag; }
    public boolean match(String valueStr) { return Arrays.asList(this.valueStrings()).contains(valueStr); }
    protected String[] valueStrings() {
        String[] values = {this.name()};
        return values;
    }

    public static WorkflowState valueOfExt(String valueStr) {
        for (WorkflowState state : WorkflowState.values()) {
            if (state.match(valueStr)) {
                return state;
            }
        }
        return null;
    }

}
