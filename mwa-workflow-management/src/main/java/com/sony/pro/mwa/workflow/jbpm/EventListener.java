package com.sony.pro.mwa.workflow.jbpm;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.process.core.context.variable.VariableScope;
import org.jbpm.process.instance.context.variable.VariableScopeInstance;
import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.event.process.ProcessNodeLeftEvent;
import org.kie.api.event.process.ProcessNodeTriggeredEvent;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.event.process.ProcessVariableChangedEvent;
import org.kie.api.marshalling.ObjectMarshallingStrategy;
import org.kie.api.runtime.EnvironmentName;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.workflow.WorkflowManagerImpl;

public class EventListener implements ProcessEventListener {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	@Override
	public void afterNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("afterNodeLeft event occurred");
	}

	@Override
	public void afterNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("afterNodeTriggered event occurred");
	}

	public String getEndStepName(ProcessCompletedEvent arg0) {
		String endEventUniqueId = null;
		Map<String, Integer> iterationLevels = ((org.jbpm.ruleflow.instance.RuleFlowProcessInstance)arg0.getProcessInstance()).getIterationLevels();
		if (iterationLevels != null) {
			for (Map.Entry<String, Integer> entry : iterationLevels.entrySet()) {
				if ( entry.getKey().startsWith("EndEvent") ) {
					endEventUniqueId = entry.getKey();
					break;
				}
			}
			org.kie.api.definition.process.Node[] nodes = ((org.jbpm.ruleflow.core.RuleFlowProcess)arg0.getProcessInstance().getProcess()).getNodes();
			for (org.kie.api.definition.process.Node node : nodes) {
				if ( node.getMetaData().get("UniqueId") == endEventUniqueId ) {
					return node.getName();
				}
			}
		}
		return null;
	}
	
	@Override
	public void afterProcessCompleted(ProcessCompletedEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("afterProcessCompleted event occurred");
/*		logger.debug("--  Process Instance  ----------------------------");
		logger.debug("id = " + arg0.getProcessInstance().getId() 
				+ ", processId = " + arg0.getProcessInstance().getProcessId()
				+ ", processName = " + arg0.getProcessInstance().getProcessName()
				+ ", eventTypes = " + arg0.getProcessInstance().getEventTypes().toString()
				+ ", state = " + arg0.getProcessInstance().getState());*/

		//jBPMのProcess変数からWorkflowのActivityInstanceIdを取得し、Callbackでイベントを通知
		Map<String, Object> params = extractVariables(arg0);
		
		if (params != null) {
			String aInstanceId = (String)params.get(PresetParameter.ParentInstanceId.getKey());
			params.put(InternalKeyForJbpm.ProcessInstanceStatus.key(), arg0.getProcessInstance().getState());
			
			{	//Pickup workflow error
				if (arg0 instanceof org.drools.core.event.ProcessCompletedEventImpl) {
					org.drools.core.event.ProcessCompletedEventImpl completedEvent = (org.drools.core.event.ProcessCompletedEventImpl)arg0;
					Object eventSource = completedEvent.getSource();
					if (eventSource instanceof org.jbpm.ruleflow.instance.RuleFlowProcessInstance) {
						org.jbpm.ruleflow.instance.RuleFlowProcessInstance procInstance = (org.jbpm.ruleflow.instance.RuleFlowProcessInstance)eventSource;
						org.jbpm.ruleflow.core.RuleFlowProcess proc = (org.jbpm.ruleflow.core.RuleFlowProcess)procInstance.getProcess();
			        	org.jbpm.bpmn2.core.Definitions definitions = (org.jbpm.bpmn2.core.Definitions)proc.getMetaData().get("Definitions");
			        	//outcomeからError名を取得する
			        	if (definitions != null && procInstance.getOutcome() != null) {
				        	for (org.jbpm.bpmn2.core.Error error : definitions.getErrors()) {
				        		if (procInstance.getOutcome().equals(error.getErrorCode())) {
									//workflow errorのコードをセット
									params.put(PresetParameter.ErrorCode.getKey(), error.getErrorCode());
									params.put(PresetParameter.ErrorDetails.getKey(), error.getId());
				        		}
				        	}
			        	}
					}
				}
			}
			{
				String endStepName = getEndStepName(arg0);
				if (endStepName != null) {
					params.put(PresetParameter.EndStepName.getKey(), endStepName);
				}
			}
			
			//ActivityInstanceへの通知(REQUEST_SUBMITの処理スレッド上で本COMPLETED処理が呼ばれるケースがあり、ActivityInstanceへの排他がとれないためAsyncで処理している)
			WorkflowManagerImpl.getInstance().getActivityManager().operateActivityAsync(aInstanceId, CommonEvent.CONFIRM_STATUS.getName(), params);
	
			//後始末としてjBPM上のProcessInstanceを削除(本処理はInstance削除後のSignal処理に問題があるため、MWA側インスタンスの削除時に行うことにする)
			//ProcessEngine.getInstance().deleteProcessInstance(arg0.getProcessInstance().getId());
		} else {
			logger.error("Can't complete process properly becuase can't extract process variables from ProcessCompletedEvent.");
		}
	}
	
	public Map<String, Object> extractVariables(ProcessCompletedEvent event) {
		ObjectMarshallingStrategy[] strategies = (ObjectMarshallingStrategy[]) event.getKieRuntime().getEnvironment().get(EnvironmentName.OBJECT_MARSHALLING_STRATEGIES);

		VariableScopeInstance variableScope = 
        (VariableScopeInstance) ((org.jbpm.process.instance.ProcessInstance)event.getProcessInstance()).getContextInstance(VariableScope.VARIABLE_SCOPE);

		if (variableScope != null)
			return new HashMap<>(variableScope.getVariables());
		else
			return null;
	}

	@Override
	public void afterProcessStarted(ProcessStartedEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("afterProcessStarted event occurred");
	}

	@Override
	public void afterVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("afterVariableChanged event occurred");

	}

	@Override
	public void beforeNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("beforeNodeLeft event occurred");

	}

	@Override
	public void beforeNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("beforeNodeTriggered event occurred");

	}

	@Override
	public void beforeProcessCompleted(ProcessCompletedEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("beforeProcessCompleted event occurred");
	}

	@Override
	public void beforeProcessStarted(ProcessStartedEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("beforeProcessStarted event occurred");

	}

	@Override
	public void beforeVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub
		logger.debug("beforeVariableChanged event occurred");
	}

}
