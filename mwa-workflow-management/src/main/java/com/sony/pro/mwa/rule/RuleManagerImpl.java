package com.sony.pro.mwa.rule;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;

import com.sony.pro.mwa.internal.activity.IActivityManagerInternal;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rule.utils.RuleUtils;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.service.workflow.IProcessEngine;
import com.sony.pro.mwa.service.workflow.IRuleManager;
import com.sony.pro.mwa.workflow.jbpm.ProcessEngineImpl;

/**
 * @author Xi.Ou
 *
 */
public class RuleManagerImpl implements IRuleManager {

    protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
    protected Map<String, IActivityTemplate> ruleMapCache;

    private static RuleManagerImpl singleton = new RuleManagerImpl();

    ProcessEngineImpl processEngine;
    IActivityManagerInternal activityManager;

    public void setProcessEngine(ProcessEngineImpl processEngine) {
        this.processEngine = processEngine;
    }

    public void setActivityManager(IActivityManagerInternal activityManager) {
        this.activityManager = activityManager;
    }
    public IActivityManagerInternal getActivityManager() {
        return this.activityManager;
    }

    public static RuleManagerImpl getInstance() {
        return singleton;
    }

    private RuleManagerImpl() {
        this.ruleMapCache = new HashMap<>();
    }

    public OperationResult startRule(final String rulePackage, final Map<String, Object> parameters) {
        logger.info("startRuleDecision: rulePackage=" + rulePackage + ", parameters=");

        Map<String, Object> paramMap = new HashMap<String, Object>();
        OperationResult result = OperationResult.newInstance();

        if (parameters != null) {
            for (Map.Entry<String, Object> param : parameters.entrySet()) {
                logger.info("\t" + "key: " + param.getKey() + ", val: " + param.getValue());
                paramMap.put(param.getKey(), param.getValue());
            }
        }

        try {
            result = this.processEngine.startRule(rulePackage, parameters);
        } catch (Exception e) {
            logger.error("startWorkflow failed", e);
        }

        return result;
    }

    @Override
    public IActivityTemplate getActivityTemplate(String rulePath, ActivityType type) {

        IActivityTemplate result = null;

        result = this.ruleMapCache.get(rulePath);

        if (result == null) {
            try {
                result = RuleUtils.createTemplateFromRuleTable(rulePath, type);
                if (result != null) {
                    this.ruleMapCache.put(rulePath, result);
                }
            } catch (java.io.FileNotFoundException e) {
                logger.error("FileNotFoundException occurred: path=" + rulePath +  "e=" + e.toString());
            } catch (SAXException | IOException e) {
                logger.error("Exception occurred: e=" + e.toString(), e);
            }
        }

        return result;
    }

    @Override
    public void reloadRuleEngine(List<String> rulePaths) {
        this.processEngine.reloadRules(rulePaths);
        return;
    }

    @Override
    public IProcessEngine getProcessEngine() {
        return this.processEngine;
    }

    @Override
    public void registerTemplate(String templateName, String templateVersion,
            boolean syncFlag) {
        /*
        String workItemName = WorkflowUtils.createWorkItemTaskName(templateName, templateVersion);
        this.getProcessEngine().registerWorkItem(workItemName, new com.sony.pro.mwa.workflow.jbpm.WorkItemHandler(templateName, templateVersion, syncFlag));
        */
    }

}
