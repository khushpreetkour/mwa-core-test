package com.sony.pro.mwa.rule;

import java.util.List;

import com.sony.pro.mwa.activity.framework.internal.IMwaActivityInternal;
import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.activity.template.IActivityFactory;
import com.sony.pro.mwa.enumeration.ActivityType;
import com.sony.pro.mwa.parameter.ParameterDefinition;


/**
 * @author Xi.Ou
 *
 */
public class RuleActivityTemplate extends ActivityTemplate {
    public RuleActivityTemplate(String id, String name, String version, List<ParameterDefinition> inputs, List<ParameterDefinition> outputs) {
        super();

        this.setId(id);
        this.setName(name);			//rule package name
        this.setVersion(version);		//rule version(metadata in first table)
        this.setType(ActivityType.DTABLE);

        this.inputs = inputs;
        this.outputs = outputs;
        this.factory = new IActivityFactory(){
            @Override
            public IMwaActivityInternal createInstance() {
                return new ExecuteRuleActivity();
            }
        };
    }

    @Override
    public Boolean getSyncFlag() {
        return true;
    }

}
