package com.sony.pro.mwa.workflow.rc;

import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.GroupId;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.rc.MWARC_GENERIC;

public class MWARC_WF extends MWARC_GENERIC {
	
	private static final String GROUPID = GroupId.EXTERNAL.id();
	private static String REGEX = IMWARC.PREFIX + IMWARC.DELIMITER + GROUPID + "[0-9a-fA-F]{4}";
	private static java.util.regex.Pattern PATTERN = java.util.regex.Pattern.compile(REGEX);
	public static final MWARC_WF WOKFLOW_INTERNAL_ERROR_CODE = new MWARC_WF ("WOKFLOW_INTERNAL_ERROR_CODE", IMWARC.PREFIX + IMWARC.DELIMITER + GroupId.SYSTEM.id() + "FFFF");

	public MWARC_WF(String name, String code) {
		super(name, code);
		
		if (name == null || !checkFormat(code)) {
			throw new MwaError(MWARC.INTEGRATION_INVALID_MWARC_FORMAT);
		}
	}

	//processを修正するまでは意図しないrcが通知される可能性があるためチェックをOffにしておく
	public static boolean checkFormat(String code) {
		//return MWARCUtil.checkFormat(PATTERN, code);
		return true;
	}
}
