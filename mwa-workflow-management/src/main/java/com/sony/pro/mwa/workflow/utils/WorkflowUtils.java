package com.sony.pro.mwa.workflow.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.drools.core.xml.SemanticModules;
import org.jbpm.bpmn2.xml.BPMNDISemanticModule;
import org.jbpm.bpmn2.xml.BPMNExtensionsSemanticModule;
import org.jbpm.bpmn2.xml.BPMNSemanticModule;
import org.kie.api.definition.process.Process;
import org.xml.sax.SAXException;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.parameter.ParameterDefinition;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IActivityTemplate;
import com.sony.pro.mwa.workflow.WorkflowActivityTemplate;
import com.sony.pro.mwa.workflow.jbpm.MwaProcessHandler;

public class WorkflowUtils {
    protected final static String WF_TOKEN_SEPARATOR = "@";
    protected final static String WF_STEP_KEY_SEPARATOR = ".";
    protected final static String WF_OUTPUT_PREFIX = "OUTPUT.";
    
    protected final static String ATTR_CANCEL_DISABLE = "_ATTR_CANCEL_DISABLE";
    
    protected static Map<String, Object> attrs = new ConcurrentHashMap<>();

    public static boolean isOutputKey(String key) {
        return key != null && key.startsWith(WorkflowUtils.WF_OUTPUT_PREFIX);
    }

    public static String decodeOutputKey(String key) {
        return (isOutputKey(key)) ? key.substring(WorkflowUtils.WF_OUTPUT_PREFIX.length()) : key;
    }

    public static String createWorkItemTaskName(String templateName, String templateVersion) {
        return templateName + WorkflowUtils.WF_TOKEN_SEPARATOR + templateVersion;
    }

    public static IActivityTemplate createTemplateFromBpmnPath(String bpmnPath) throws SAXException, IOException  {
        InputStream is = new FileInputStream(new java.io.File (bpmnPath) );
        IActivityTemplate result = null;

        //SemanticModule次第でInput/Outputを取れる可能性あり（ItemDefinition_XXみたいな）
        SemanticModules semanticModules = new SemanticModules();
        BPMNSemanticModule bpmnSemanticModule = new BPMNSemanticModule();
        bpmnSemanticModule.addHandler("process", new MwaProcessHandler());	//Overwrite process handler
        semanticModules.addSemanticModule(bpmnSemanticModule);
        semanticModules.addSemanticModule(new BPMNExtensionsSemanticModule());
        semanticModules.addSemanticModule(new BPMNDISemanticModule());

        org.jbpm.compiler.xml.XmlProcessReader procReader = new org.jbpm.compiler.xml.XmlProcessReader(semanticModules, Thread.currentThread().getContextClassLoader());

        List<Process> processes = procReader.read(is);
        for (Process process : processes) {
            if (process != null) {
            	Boolean cancelable = true;
            	Boolean attrCancelDisable = (Boolean)((org.jbpm.ruleflow.core.RuleFlowProcess)process).getMetaData(ATTR_CANCEL_DISABLE);
            	if (attrCancelDisable != null) 
            		cancelable = !attrCancelDisable;
            	
                result = createTemplateFromProcess(process, cancelable);
                break;
            }
        }
        if (is != null) {
        	is.close();
        	is = null;
        }

        return result;
    }

    protected static IActivityTemplate createTemplateFromProcess(Process process, Boolean cancelable) {
        ActivityTemplate result = null;
        if (process != null) {
            List<ParameterDefinition> inputs = new ArrayList<>();
            List<ParameterDefinition> outputs = new ArrayList<>();
            //WFのIn/Outをbpmnから取り出し
            if (process instanceof org.jbpm.ruleflow.core.RuleFlowProcess) {
                String[] wfInputs = ((org.jbpm.ruleflow.core.RuleFlowProcess)process).getVariableScope().getVariableNames();
                for (String key : wfInputs) {
                    //★全部TypeStringにしてるが、本来はparseするなりして、TemplateのPrameter定義から型を引いてきたい。Editorの際に再考する
                    ParameterDefinition pdef = new ParameterDefinition().setKey(key).setRequired(false).setType(TypeString.instance());
                    if (WorkflowUtils.isOutputKey(key)) {
                        pdef.setKey(decodeOutputKey(pdef.getKey()));
                        outputs.add(pdef);
                    } else {
                        inputs.add(pdef);
                    }
                }
            }
        	if (cancelable == null || cancelable) 
        		attrs.put(process.getName(), cancelable);
            result = new WorkflowActivityTemplate(
                    convertProcessIdToTemplateId(process.getId()),
                    process.getName(),
                    process.getVersion(),
                    inputs,
                    outputs,
                    "",
                    "",
                    "").addTag(process.getPackageName());
        } else {
            String message = "createTemplateFromProcess: Null process is input.";
            throw new MwaError(MWARC.INTEGRATION_INVALID_MWARC_FORMAT, "", message);
        }
        return result;
    }

    public static String convertProcessIdToTemplateId(String processId) {
        if (processId != null && processId.startsWith("_"))
            processId = processId.substring(1);	//remove "_"
        return processId;
    }

    public static String convertTemplateIdToProcessId(String templateId) {
        if (templateId != null)
            return "_" + templateId;
        else
            return null;
    }

    protected static String extractString(String str, String separator, boolean head) {
        String result = null;
        if (str != null && separator != null) {
            int idx = str.indexOf(separator);
            if (idx >= 0) {
                if (head)
                    result = str.substring(0, idx);
                else
                    result = str.substring(idx + separator.length());
            }
        }
        return result;
    }

    public static String getTemplateName(String workItemTaskName) {
        return extractString(workItemTaskName, WF_TOKEN_SEPARATOR, true);
    }

    public static String getTemplateVersion(String workItemTaskName) {
        return extractString(workItemTaskName, WF_TOKEN_SEPARATOR, false);
    }

    public static String getStepName(String fullStepKey) {
        return extractString(fullStepKey, WF_STEP_KEY_SEPARATOR, true);
    }
    public static String getStepKey(String fullStepKey) {
        return extractString(fullStepKey, WF_STEP_KEY_SEPARATOR, false);
    }
    
    public static Boolean isCancelable(String templateName) {
    	Boolean result = (Boolean)attrs.get(templateName);
    	return result == null ? false : result;
    }
}
