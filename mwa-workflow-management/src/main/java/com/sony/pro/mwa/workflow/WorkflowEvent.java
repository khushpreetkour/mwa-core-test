package com.sony.pro.mwa.workflow;

import com.sony.pro.mwa.activity.framework.stm.EventType;
import com.sony.pro.mwa.service.activity.IEvent;

public enum WorkflowEvent implements IEvent {
	//The following CommonEvents are defined.
	// - REQUEST_SUBMIT,
	// - CONFIRM_STATUS,
	// - NOTIFY_ERROR,
	
	//Workflow Specific Events
	NOTIFY_COMPLETED(EventType.NOTIFY),
	NOTIFY_MANUAL_COMPLETED(EventType.NOTIFY),
	NOTIFY_CANCELLED(EventType.NOTIFY),
	NOTIFY_UPDATED(EventType.NOTIFY),
	;
	
	EventType type;
	
	private WorkflowEvent(EventType type) { this.type = type; }

	public boolean isMatch(String eventName) { return this.toString().equals(eventName); }
	public boolean isRequestEvent() { return this.type.isRequest(); }
	public boolean isNotifyEvent() { return this.type.isNotify(); }
	public boolean isConfirmEvent() { return this.type.isConfirm(); }

	public String getName() {
		return super.name();
	}
}
