package com.sony.pro.mwa.workflow.jbpm;


public enum InternalKeyForJbpm {
    ProcessInstanceId,
    WorkItemId,
    ProcessInstanceStatus,
    Retries("retries"),
    WorkItemAttribute,
    WorkflowProgress,
    ;

    protected String key;
    private InternalKeyForJbpm() {
        this(null);
    }
    private InternalKeyForJbpm(String key) {
        this.key = key;
    }

    public String key() {
        return (this.key != null) ? this.key : this.name();
    }
}
