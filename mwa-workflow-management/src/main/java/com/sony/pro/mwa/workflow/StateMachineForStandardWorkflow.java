package com.sony.pro.mwa.workflow;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.activity.framework.stm.SystemEvent;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;
import com.sony.pro.mwa.workflow.StateMachineForStandardWorkflow;
import com.sony.pro.mwa.workflow.WorkflowEvent;
import com.sony.pro.mwa.workflow.WorkflowState;

public class StateMachineForStandardWorkflow implements IStateMachine {
	
	private static final StateMachineForStandardWorkflow instance = new StateMachineForStandardWorkflow();
	
	//状態遷移表の定義
	private final static Object[][] stTableDef = {
			//CurrentState,				//Event,							//NextState
		{	WorkflowState.QUEUED,		CommonEvent.REQUEST_SUBMIT,			WorkflowState.IN_PROGRESS},
		{	WorkflowState.QUEUED,		CommonEvent.NOTIFY_ERROR,			WorkflowState.ERROR},
		{	WorkflowState.QUEUED,		CommonEvent.REQUEST_CANCEL,			WorkflowState.CANCELLED},
		{	WorkflowState.QUEUED,		WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.MANUAL_COMPLETED},
		{	CommonState.QUEUED,			CommonEvent.REQUEST_SUBMIT,			WorkflowState.IN_PROGRESS},
		{	CommonState.QUEUED,			CommonEvent.NOTIFY_ERROR,			WorkflowState.ERROR},
		{	CommonState.QUEUED,			CommonEvent.REQUEST_CANCEL,			WorkflowState.CANCELLED},
		{	CommonState.QUEUED,			WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.CREATED,		CommonEvent.REQUEST_SUBMIT,			WorkflowState.IN_PROGRESS},
		{	WorkflowState.CREATED,		CommonEvent.NOTIFY_ERROR,			WorkflowState.ERROR},
		{	WorkflowState.CREATED,		WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.IN_PROGRESS,	CommonEvent.REQUEST_CANCEL,			WorkflowState.CANCELLING},
		{	WorkflowState.IN_PROGRESS,	CommonEvent.NOTIFY_ERROR,			WorkflowState.ERROR},
		{	WorkflowState.IN_PROGRESS,	WorkflowEvent.NOTIFY_UPDATED,		WorkflowState.IN_PROGRESS},
		{	WorkflowState.IN_PROGRESS,	WorkflowEvent.NOTIFY_COMPLETED,		WorkflowState.COMPLETED},
		{	WorkflowState.IN_PROGRESS,	WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.IN_PROGRESS,	WorkflowEvent.NOTIFY_CANCELLED,		WorkflowState.CANCELLED},
		{	WorkflowState.CANCELLING,	WorkflowEvent.NOTIFY_UPDATED,		WorkflowState.CANCELLING},
		{	WorkflowState.CANCELLING,	WorkflowEvent.NOTIFY_CANCELLED,		WorkflowState.CANCELLED},
		{	WorkflowState.CANCELLING,	CommonEvent.NOTIFY_ERROR,			WorkflowState.ERROR},
		{	WorkflowState.CANCELLING,	WorkflowEvent.NOTIFY_COMPLETED,		WorkflowState.COMPLETED},
		{	WorkflowState.CANCELLING,	WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.CANCELLED,	WorkflowEvent.NOTIFY_UPDATED,		WorkflowState.CANCELLED},
		{	WorkflowState.CANCELLED,	WorkflowEvent.NOTIFY_CANCELLED,		WorkflowState.CANCELLED},
		{	WorkflowState.CANCELLED,	CommonEvent.NOTIFY_ERROR,			WorkflowState.CANCELLED},
		{	WorkflowState.CANCELLED,	WorkflowEvent.NOTIFY_COMPLETED,		WorkflowState.CANCELLED},
		{	WorkflowState.CANCELLED,	WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.CANCELLED},
		{	WorkflowState.MANUAL_COMPLETED,	WorkflowEvent.NOTIFY_UPDATED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.MANUAL_COMPLETED,	WorkflowEvent.NOTIFY_CANCELLED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.MANUAL_COMPLETED,	CommonEvent.NOTIFY_ERROR,			WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.MANUAL_COMPLETED,	WorkflowEvent.NOTIFY_COMPLETED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.MANUAL_COMPLETED,	WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.COMPLETED,	WorkflowEvent.NOTIFY_UPDATED,		WorkflowState.COMPLETED},
		{	WorkflowState.COMPLETED,	WorkflowEvent.NOTIFY_CANCELLED,		WorkflowState.COMPLETED},
		{	WorkflowState.COMPLETED,	CommonEvent.NOTIFY_ERROR,			WorkflowState.COMPLETED},
		{	WorkflowState.COMPLETED,	WorkflowEvent.NOTIFY_COMPLETED,		WorkflowState.COMPLETED},
		{	WorkflowState.COMPLETED,	WorkflowEvent.NOTIFY_MANUAL_COMPLETED,		WorkflowState.COMPLETED},
		
		//ActivityInstance削除をトリガにProcessEngine側のInstanceも削除するため、SystemEventをハンドリング
		{	WorkflowState.COMPLETED,	SystemEvent.NOTIFY_INSTANCE_DELETION,		WorkflowState.COMPLETED},
		{	WorkflowState.MANUAL_COMPLETED,	SystemEvent.NOTIFY_INSTANCE_DELETION,		WorkflowState.MANUAL_COMPLETED},
		{	WorkflowState.CANCELLED,	SystemEvent.NOTIFY_INSTANCE_DELETION,		WorkflowState.CANCELLED},
		{	WorkflowState.ERROR,		SystemEvent.NOTIFY_INSTANCE_DELETION,		WorkflowState.ERROR},
	};

	private final static HashMap<Map.Entry<IState, IEvent>, IState> stTable = new HashMap<Map.Entry<IState, IEvent>, IState>() {{
		//初期化処理、stTableDefをMapに詰めてるだけ(直接Mapで定義してもよかったが、定義を配列で切り出した方がシンプルで管理しやすいので)
		for (int i = 0; i < stTableDef.length; i++) {
			put(new AbstractMap.SimpleEntry<IState, IEvent>((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	private StateMachineForStandardWorkflow() {
		
	}
	
	public static StateMachineForStandardWorkflow getInstance() {
        return instance;
    }
	
	public boolean isAcceptable(IState current, IEvent event) {
		IState next = getNextState(current, event);
		return (next != null) ? true : false;
	}

	public IState getNextState(IState state, IEvent event) {
		//Mapから取り出して返す
		return stTable.get(new AbstractMap.SimpleEntry<IState, IEvent>(state, event));
	}
}
