package com.sony.pro.mwa.workflow;

import com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.activity.framework.stm.SystemEvent;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceSimpleModel;
import com.sony.pro.mwa.model.kbase.tempmodels.WorkflowDetailModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;
import com.sony.pro.mwa.workflow.jbpm.InternalKeyForJbpm;
import com.sony.pro.mwa.workflow.jbpm.JbpmMapper;
import com.sony.pro.mwa.workflow.jbpm.WorkflowAttribute;
import com.sony.pro.mwa.workflow.rc.MWARC_WF;
import com.sony.pro.mwa.workflow.utils.WorkflowUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.kie.internal.runtime.manager.SessionNotFoundException;

public class StandardWorkflow extends AbsMwaActivity {

    enum VARIABLE {
        PROCESS_INSTANCE_ID,
        STEP_STATUS_LIST,
        TOTAL_STEP,
        END_STEP
    }

    /*-------------------------------------------------------------------------------------------
     *  Member変数。必ずset/getInstanceDetailsで上位からアクセス可能であること。メンバ変数の永続化と復元のため。
     -------------------------------------------------------------------------------------------*/
    Long processInstanceId;
    Integer totalStep;
    String endStep;

    /*-------------------------------------------------------------------------------------------
     *  コンストラクタ
     -------------------------------------------------------------------------------------------*/
    public StandardWorkflow() {
        //インスタンス生成時の初期状態はQueued
        //状態遷移用にStateMachineForStandardJobを用いる
        super(CommonState.QUEUED, StateMachineForStandardWorkflow.getInstance());
        this.processInstanceId = null;
        this.totalStep = null;
        this.endStep = null;
    }

    /*-------------------------------------------------------------------------------------------
     *  Activity Framework 接続用の Override
     -------------------------------------------------------------------------------------------*/
    @Override
    public String getPersistentDataImpl() {
        //return "" + processInstanceId;
        Map<String, Object> variables = new HashMap<>();
        variables.put(VARIABLE.PROCESS_INSTANCE_ID.name(), this.processInstanceId);
        variables.put(VARIABLE.TOTAL_STEP.name(), this.totalStep);
        variables.put(VARIABLE.END_STEP.name(), this.endStep);
        return Converter.mapToJson(variables);
    }

    @Override
    public void setPersistentDataImpl(String details) {
        Map<String, Object> variables = Converter.jsonToMap(details);
        if (variables == null) {
            //この分岐は互換性のため
            try {
                this.processInstanceId = (details != null) ? Long.valueOf(details) : null;
            } catch (Exception e) {
            }
        } else {
            Object processInstanceIdObj = variables.get(VARIABLE.PROCESS_INSTANCE_ID.name());
            if (processInstanceIdObj instanceof Long)
                this.processInstanceId = (Long)processInstanceIdObj;
            else if (processInstanceIdObj instanceof Integer)
                this.processInstanceId = new Long((Integer)processInstanceIdObj);
            this.totalStep = (Integer)variables.get(VARIABLE.TOTAL_STEP.name());
            this.endStep = (String)variables.get(VARIABLE.END_STEP.name());
        }
        return;
    }

    @Override
    protected OperationResult requestSubmitImpl(Map<String, Object> params) {
        logger.debug("requestSubmitImpl called.");
        OperationResult result = OperationResult.newInstance();

        if (params != null) {
            Map<String, Object> workflowParams = new HashMap<>(params);

            workflowParams.put(PresetParameter.ParentInstanceId.getKey(), this.getId());
            workflowParams.put(PresetParameter.ActivityTemplateId.getKey(), this.getTemplate().getId());
            try {
                String bpmnProcessId = WorkflowUtils.convertTemplateIdToProcessId(this.getTemplate().getId());
                this.processInstanceId = WorkflowManagerImpl.getInstance().getProcessEngine().startProcess(bpmnProcessId, workflowParams);
                // this.processInstanceId = (Long)WorkflowManagerImpl.getInstance().startWorkflow(bpmnProcessId, workflowParams).get("processId");
                logger.info("requestSubmitImpl: templateName=" + this.getTemplate().getName() + ", processInstanceId=" + this.processInstanceId);
                //Integer state = WorkflowManagerImpl.getInstance().getProcessEngine().getState(this.processId);
            } catch (MwaError e) {
                throw e;
            } catch (Exception e) {
                String message = "requestSubmitImpl: exception=" + e.getMessage();
                logger.error(message, e);
                throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
            }
        } else {
            String message = "requestSubmitImpl: parameter is null.";
            logger.error(message);
            throw new MwaInstanceError(MWARC.SYSTEM_ERROR);
        }

        return result;
    }

    @Override
    public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
        logger.debug("confirmStatusImpl called: status=" + this.getStatus());

        //Eventを返さない特殊な返り値（★ActivityFrameworkの実装を前提としていて適切な実装ではないがとりあえず）
        OperationResultWithStatus result = OperationResultWithStatus.newInstance((IEvent)null, null);

        if (params != null && params.containsKey(PresetParameter.ErrorCode.getKey())) {
            String code = (String)params.get(PresetParameter.ErrorCode.getKey());
            String name = (String)params.get(PresetParameter.ErrorDetails.getKey());
            String devCode = null;
            String devDetails = null;
            WorkflowManagerImpl.getInstance().getProcessEngine().removeWorkflowAttributeFromList(processInstanceId);
            WorkflowManagerImpl.getInstance().getProcessEngine().deleteProcessInstance(this.processInstanceId);

            if (MWARC_WF.checkFormat(code)) {
                //以下は子のInstanceのError情報をWorkflowのError情報として引用するための処理
                if (MWARC_WF.WOKFLOW_INTERNAL_ERROR_CODE.code().equalsIgnoreCase(code)) {
                    ActivityInstanceCollection children = this.getCoreModules().getActivityManager().getChildInstances(this.getId());
                    ActivityInstanceSimpleModel errorModel = null;
                    for (ActivityInstanceSimpleModel model : children.getModels()) {
                        if (model.getStatus().isError()) {
                            if ( errorModel == null || model.getEndTime() < errorModel.getEndTime()) {
                                errorModel = model;
                            }
                        }
                    }
                    if (errorModel != null) {
                        ActivityInstanceModel model = this.getCoreModules().getActivityManager().getInstance(errorModel.getId());
                        if (model != null && model.getStatusDetails() != null) {
                            code = model.getStatusDetails().getResult().code();
                            devCode = model.getStatusDetails().getDeviceResponse();
                            devDetails = model.getStatusDetails().getDeviceResponseDetails();
                        }
                    }
                }
                throw new MwaInstanceError(new MWARC_WF(name, code), devCode, devDetails);
            } else {
                logger.error("Workflow caused invalid error: name=" + name + ", code=" + code);
                throw new MwaInstanceError(MWARC.INTEGRATION_INVALID_MWARC_FORMAT);
            }
        }
        if (params != null && this.endStep == null)
            this.endStep = (String)params.get(PresetParameter.EndStepName.getKey());

        if (this.endStep != null) {	//Workflowの最終Step名から状態を取得（推奨）

            //子のInstanceが停止したことを確認していく
            ActivityInstanceCollection children = this.getCoreModules().getActivityManager().getChildInstances(this.getId());
            boolean isAllTerminated = true;
            for (ActivityInstanceModel model : children.getModels()) {
                isAllTerminated &= model.getStatus().isTerminated();
            }
            if (isAllTerminated) {
                //全ての子Instanceが停止したため終了処理
                WorkflowState state = WorkflowState.valueOfExt(this.endStep);
                if (WorkflowState.COMPLETED.equals(state)) {
                    result = OperationResultWithStatus.newInstance(WorkflowEvent.NOTIFY_COMPLETED, 100);
                } else if (WorkflowState.CANCELLED.equals(state)) {
                    result = OperationResultWithStatus.newInstance(WorkflowEvent.NOTIFY_CANCELLED, 1);
                } else if (WorkflowState.MANUAL_COMPLETED.equals(state)) {
                    result = OperationResultWithStatus.newInstance(WorkflowEvent.NOTIFY_MANUAL_COMPLETED, 1);
                } else {
                    result = OperationResultWithStatus.newInstance(CommonEvent.NOTIFY_ERROR, 0);
                }

                if (params != null) {
                    for (Entry<String, Object> entry : params.entrySet()) {
                        if (WorkflowUtils.isOutputKey(entry.getKey())) {
                            this.setOutput(WorkflowUtils.decodeOutputKey(entry.getKey()), entry.getValue());
                        }
                    }
                }

                // remove workflow attribute from list
                WorkflowManagerImpl.getInstance().getProcessEngine().removeWorkflowAttributeFromList(processInstanceId);
                WorkflowManagerImpl.getInstance().getProcessEngine().deleteProcessInstance(this.processInstanceId);
            }
        } else if (params != null && params.containsKey(InternalKeyForJbpm.ProcessInstanceStatus.key())) {	//jBPMのProcessInstanceの状態からWorkflow状態を取得（Worklfow記述のフォロー用）
            //jBPMからのコールバック
            Integer state = (Integer)params.get(InternalKeyForJbpm.ProcessInstanceStatus.key());
            IEvent event = JbpmMapper.convertEvent(state);
            Integer progress = (WorkflowEvent.NOTIFY_COMPLETED.equals(event)) ? 100 : 10;
            logger.debug("jBPM processId=" + this.processInstanceId + ", state=" + state);
            WorkflowManagerImpl.getInstance().getProcessEngine().removeWorkflowAttributeFromList(processInstanceId);
            WorkflowManagerImpl.getInstance().getProcessEngine().deleteProcessInstance(this.processInstanceId);
            result = OperationResultWithStatus.newInstance(event, progress);

            // [NVX-5654 [NVX][PF][MWA] Editor記述のWFのエラーハンドリング実装（簡易版）] の対応
			WorkflowDetailModel detailModel = this.getCoreModules().getKnowledgeBase().getWorkflow(this.getTemplate().getId());
			if (detailModel != null && !detailModel.getPredefineFlag()) {
			    ActivityInstanceCollection children = this.getCoreModules().getActivityManager().getChildInstances(this.getId());
			    for (ActivityInstanceSimpleModel model : children.getModels()) {
			        if (model.getStatus().isError()) {
			        	result = OperationResultWithStatus.newInstance(CommonEvent.NOTIFY_ERROR, this.getProgress());
			        	break;
			        }
			    }
			}

            for (Entry<String, Object> entry : params.entrySet()) {
                if (WorkflowUtils.isOutputKey(entry.getKey())) {
                    this.setOutput(WorkflowUtils.decodeOutputKey(entry.getKey()), entry.getValue());
                }
            }
        } else {
            result = updateProgress();
        }

        return result;
    }
    @Override
    public OperationResult requestCancelImpl(Map<String, Object> params) {
        logger.info("requestCancelImpl called: processId=" + this.processInstanceId);
        OperationResult result = OperationResult.newInstance();
        
        if (WorkflowState.QUEUED.match(this.getStatus().getName())) {
        	//do nothing accept cancel operation
        	return result;
        } else if (!WorkflowUtils.isCancelable(this.getTemplate().getName())) {
    		throw new MwaError(MWARC.OPERATION_FAILED, null, "This activity is not cancelable");
        }

        ActivityInstanceCollection children = this.getCoreModules().getActivityManager().getChildInstances(this.getId());
        for (ActivityInstanceSimpleModel model : children.getModels()) {
            if (!model.getStatus().isTerminated()) {
                try {
                    this.getCoreModules().getActivityManager().operateActivity(model.getId(), CommonEvent.REQUEST_CANCEL.name(), null);
                } catch (MwaError e) {
                    //ignore operation failure
                }
            }
        }

        if (this.processInstanceId != null) {
        	try {
        		WorkflowManagerImpl.getInstance().getProcessEngine().sendCancelSignal(this.processInstanceId);
        	} catch (SessionNotFoundException e) {
        		logger.error("requestCancelImpl caused SessionNotFoundException: " + e);
        		throw new MwaError(MWARC.OPERATION_FAILED_UNACCEPTABLE_STATE, null, "WorkflowEngine can't accept cancel operation");
        	} catch (IllegalStateException e) {
        		logger.error("requestCancelImpl caused IllegalStateException: " + e, e);
        		throw new MwaError(MWARC.OPERATION_FAILED_UNACCEPTABLE_STATE, null, "WorkflowEngine can't accept cancel operation");
        	}
        }
        //do nothing
        return result;
    }

    /*
    @Override
    public OperationResult requestStopImpl(Map<IParameterDefinition, Object> params) {
        logger.info("requestStopImpl called.");
        return OperationResult.newSuccessInstance();
    }

    @Override
    public OperationResult requestPauseImpl(Map<IParameterDefinition, Object> params) {
        logger.info("requestPauseImpl called.");
        return OperationResult.newSuccessInstance();
    }*/

    @Override
    protected OperationResult processEventImpl(IEvent event, Map<String, Object> params) {
        // TODO Auto-generated method stub
        OperationResult result = OperationResult.newInstance();

        if (WorkflowEvent.NOTIFY_UPDATED.equals(event)) {
            if (params != null) {
                Integer progress;
                if (params.containsKey(InternalKeyForJbpm.WorkflowProgress.key())) {
                    String progressStr = (String)params.get(InternalKeyForJbpm.WorkflowProgress.key());
                    progress = Integer.parseInt(progressStr);
                    logger.info("Set workflow progress to [" + progress + "]");
                    this.model.setProgress(progress);
                }
                else {
                    progress = getProgress();
                }
                result = OperationResultWithStatus.newInstance(WorkflowEvent.NOTIFY_UPDATED, progress);
                result.put("progress", getProgress());
            }
        }
        else {
            logger.info("Invalid Event: event=" + ((event != null) ? event.getClass().getName() : "null"));
            throw new MwaError(MWARC.INVALID_INPUT_EVENT);
        }
        return result;
    }

    @Override
    protected List<? extends IEvent> getSupportedEventListImpl() {
        // TODO Auto-generated method stub
        List<IEvent> result = new ArrayList<IEvent>(Arrays.asList(WorkflowEvent.values()));
        result.add(SystemEvent.NOTIFY_INSTANCE_DELETION);
        result.add(WorkflowEvent.NOTIFY_UPDATED);
        return result;
    }

    @Override
    protected List<? extends IState> getSupportedStateListImpl() {
        // TODO Auto-generated method stub
        return Arrays.asList(WorkflowState.values());
    }

    private OperationResultWithStatus updateProgress() {
        WorkflowAttribute attribute = WorkflowManagerImpl.getInstance().getProcessEngine().getWorkflowAttribute(this.processInstanceId);
        if (attribute != null) {
            logger.debug("#### OUDBG #### : name -> " + attribute.getName());
            logger.debug("#### OUDBG #### : instanceId -> " + attribute.getInstanceId());
            logger.debug("#### OUDBG #### : totalItems -> " + attribute.getTotalItems());
            logger.debug("#### OUDBG #### : completedItemList -> " + attribute.getCompletedItemList());
            //logger.debug("#### OUDBG #### : variables -> " + attribute.getVariables());
            this.totalStep = attribute.getTotalItems();
            Integer progress = this.getProgress();
            Integer completed = attribute.getCompletedItems();
            Integer calcProgress = (int)(completed.intValue() * 100.0 / this.totalStep.intValue());
            Integer childProgress = 0;

            // Get currently child activity progress
            ActivityInstanceCollection children = this.getCoreModules().getActivityManager().getChildInstances(this.getId());
            for (ActivityInstanceSimpleModel model : children.getModels()) {
                if (model.getStatus().isTerminated()) {
                    continue;
                }
                else {
                    childProgress = model.getProgress();
                    break;
                }
            }
            // Added in progress child activity progress
            calcProgress += (int)((100.0 / this.totalStep.intValue()) * (childProgress.intValue() / 100.0));

            this.model.setProgress(calcProgress.intValue() < progress.intValue() ? progress : calcProgress);
        } else {
	        Integer processStatus = WorkflowManagerImpl.getInstance().getProcessEngine().getState(this.processInstanceId);
        	String msg = "Internal Server Error. (CODE: " + MWARC.OPERATION_FAILED_PROCESS_NOT_FOUND.name() + ", PID:" + this.processInstanceId + ")";
	        if (processStatus != null) {
	        	//processEngine上にInstanceが存在しない（別スレッドで完了してしまった）
	            logger.warn(msg);
	        	throw new MwaInstanceError(MWARC.OPERATION_FAILED_PROCESS_NOT_FOUND, null, msg);
	        } else {
	        	//attributeが存在しないが、Instanceは存在する（attributeのみ永続化されないので消えてしまった）
	            logger.warn(msg);
	        	throw new MwaInstanceError(MWARC.OPERATION_FAILED_PROCESS_NOT_FOUND, null, msg);
	        }
        }

        return OperationResultWithStatus.newInstance(WorkflowEvent.NOTIFY_UPDATED, getProgress());
    }

}
