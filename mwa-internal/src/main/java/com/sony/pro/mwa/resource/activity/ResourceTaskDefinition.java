package com.sony.pro.mwa.resource.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.control.activity.EndTask;
import com.sony.pro.mwa.control.activity.ConditionalConvergingTask;
import com.sony.pro.mwa.control.activity.ConvergingTask;
import com.sony.pro.mwa.control.activity.ConditionalBranchTask;
import com.sony.pro.mwa.control.activity.ParallelBranchTask;
import com.sony.pro.mwa.control.activity.StartTask;
import com.sony.pro.mwa.service.provider.ActivityProviderType;

public class ResourceTaskDefinition {

    @SuppressWarnings("serial")
    static ActivityTemplate templates[] = {
            new ActivityTemplate(
                    StartTask.class,
                    StartTask.class.getName(),
                    "1",
                    Arrays.asList(StartTask.INPUTS.values()),
                    Arrays.asList(StartTask.OUTPUTS.values()),
                    "Start",
                    "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-6%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M30.7%2C17.3l-12-12c-0.4-0.4-1-0.4-1.4%2C0s-0.4%2C1%2C0%2C1.4L27.6%2C17H6c-0.6%2C0-1%2C0.4-1%2C1s0.4%2C1%2C1%2C1h21.6L17.3%2C29.3%20c-0.4%2C0.4-0.4%2C1%2C0%2C1.4c0.2%2C0.2%2C0.5%2C0.3%2C0.7%2C0.3s0.5-0.1%2C0.7-0.3l12-12C31.1%2C18.3%2C31.1%2C17.7%2C30.7%2C17.3z%22%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E",
                    "A \"Start\" step shows where a process can begin."),
            new ActivityTemplate(
                    EndTask.class,
                    EndTask.class.getName(),
                    "1",
                    Arrays.asList(EndTask.INPUTS.values()),
                    Arrays.asList(EndTask.OUTPUTS.values()),
                    "End",
                    "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-6%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cg%3E%20%3Cpolygon%20fill%3D%22%23E0E0E0%22%20points%3D%2227%2C15%2027%2C6%2015%2C6%2015%2C4%2013%2C4%2013%2C30%2010%2C30%2010%2C32%2019%2C32%2019%2C30%2015%2C30%2015%2C15%20%22%2F%3E%20%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E",
                    "An \"End\" step marks where a workflow ends."),
            new ActivityTemplate(
                    ConditionalBranchTask.class,
                    ConditionalBranchTask.class.getName(),
                    "1",
                    Arrays.asList(ConditionalBranchTask.INPUTS.values()),
                    Arrays.asList(ConditionalBranchTask.OUTPUTS.values()),
                    "Conditional Branch",
                    "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-6%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M28.5%2C22.8l0.2%2C4.4l-7.1-7.1c-0.9-0.9-2-1.7-3.2-2.2c1.2-0.5%2C2.3-1.3%2C3.2-2.2l7.1-7.1l-0.2%2C4.4l1.2%2C0L30%2C6%20l-7.2%2C0.3l0%2C1.2l4.4-0.2l-7.1%2C7.1c-1.7%2C1.7-4%2C2.6-6.4%2C2.6H5v2h8.9c2.4%2C0%2C4.7%2C0.9%2C6.4%2C2.6l7.1%2C7.1l-4.4-0.2l0%2C1.2L30%2C30l-0.3-7.2%20L28.5%2C22.8z%22%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E",
                    "A \"Conditional Branch\" step is used to select paths within a workflow.Condition expression is associated with a branch’s outgoing sequence flows, only one of the paths can be taken."),
            new ActivityTemplate(
                    ParallelBranchTask.class,
                    ParallelBranchTask.class.getName(),
                    "1",
                    Arrays.asList(ParallelBranchTask.INPUTS.values()),
                    Arrays.asList(ParallelBranchTask.OUTPUTS.values()),
                    "Parallel Branch",
                    "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-6%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M28.5%2C22.8l0.2%2C4.4l-7.1-7.1c-0.9-0.9-2-1.7-3.2-2.2c1.2-0.5%2C2.3-1.3%2C3.2-2.2l7.1-7.1l-0.2%2C4.4l1.2%2C0L30%2C6%20l-7.2%2C0.3l0%2C1.2l4.4-0.2l-7.1%2C7.1c-1.7%2C1.7-4%2C2.6-6.4%2C2.6H5v2h8.9c2.4%2C0%2C4.7%2C0.9%2C6.4%2C2.6l7.1%2C7.1l-4.4-0.2l0%2C1.2L30%2C30l-0.3-7.2%20L28.5%2C22.8z%22%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E",
                    "A \"Parallel Branch\" step can be used to create parallel paths within a workflow. All of the paths can be taken."),
            new ActivityTemplate(
                    ConvergingTask.class,
                    ConvergingTask.class.getName(),
                    "1",
                    Arrays.asList(ConvergingTask.INPUTS.values()),
                    Arrays.asList(ConvergingTask.OUTPUTS.values()),
                    "Parallel Converging",
                    "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-5%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M25.7%2C12.7l-0.9%2C1l3.6%2C3.4H28h-6.9c-2.4%2C0-4.7-0.9-6.4-2.6L6.7%2C6.3L5.3%2C7.7l8.1%2C8.1c0.9%2C0.9%2C2%2C1.7%2C3.2%2C2.2%20c-1.2%2C0.5-2.3%2C1.3-3.2%2C2.2l-8.1%2C8.1l1.4%2C1.4l8.1-8.1c1.7-1.7%2C4-2.6%2C6.4-2.6H28h0.4l-3.6%2C3.4l0.9%2C1l5.7-5.3L25.7%2C12.7z%22%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E",
                    "A \"Parallel Converging\" step is used to control how Sequence Flows interact as they converge within a workflow. It's used to converge for \"Parallel Branch\"."),
            new ActivityTemplate(
                    ConditionalConvergingTask.class,
                    ConditionalConvergingTask.class.getName(),
                    "1",
                    Arrays.asList(ConditionalConvergingTask.INPUTS.values()),
                    Arrays.asList(ConditionalConvergingTask.OUTPUTS.values()),
                    "Conditional Converging",
                    "data:image/svg+xml;charset=utf8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%3Csvg%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20x%3D%220px%22%20y%3D%220px%22%20width%3D%2236px%22%20height%3D%2236px%22%20viewBox%3D%220%200%2036%2036%22%20enable-background%3D%22new%200%200%2036%2036%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22bg_x28_%E6%9B%B8%E3%81%8D%E5%87%BA%E3%81%97%E6%99%82%E3%81%AF%E9%9D%9E%E8%A1%A8%E7%A4%BA%E3%81%AB%E3%81%99%E3%82%8B_xFF09_%22%20display%3D%22none%22%3E%20%3Crect%20x%3D%22-5%22%20y%3D%22-6%22%20display%3D%22inline%22%20fill%3D%22%237F7F7F%22%20width%3D%2248%22%20height%3D%2248%22%2F%3E%3C%2Fg%3E%3Cg%20id%3D%22%E3%83%AC%E3%82%A4%E3%83%A4%E3%83%BC_1%22%3E%20%3Cpath%20fill%3D%22%23E0E0E0%22%20d%3D%22M25.7%2C12.7l-0.9%2C1l3.6%2C3.4H28h-6.9c-2.4%2C0-4.7-0.9-6.4-2.6L6.7%2C6.3L5.3%2C7.7l8.1%2C8.1c0.9%2C0.9%2C2%2C1.7%2C3.2%2C2.2%20c-1.2%2C0.5-2.3%2C1.3-3.2%2C2.2l-8.1%2C8.1l1.4%2C1.4l8.1-8.1c1.7-1.7%2C4-2.6%2C6.4-2.6H28h0.4l-3.6%2C3.4l0.9%2C1l5.7-5.3L25.7%2C12.7z%22%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E",
                    "A \"Conditional Converging\" step is used to control how Sequence Flows interact as they converge within a workflow. It's used to converge for \"Conditional Branch\"."),
            new ActivityTemplate(
                    ReserveResourceTask.class,
                    ReserveResourceTask.class.getName(),
                    "1",
                    Arrays.asList(ReserveResourceTask.INPUTS.values()),
                    Arrays.asList(ReserveResourceTask.OUTPUTS.values())),
            new ActivityTemplate(
                    ReleaseResourceTask.class,
                    ReleaseResourceTask.class.getName(),
                    "1",
                    Arrays.asList(ReleaseResourceTask.INPUTS.values()),
                    Arrays.asList(ReleaseResourceTask.OUTPUTS.values())),
            new ActivityTemplate(
                    AllocateResourceTask.class,
                    AllocateResourceTask.class.getName(),
                    "1",
                    Arrays.asList(AllocateResourceTask.INPUTS.values()),
                    Arrays.asList(AllocateResourceTask.OUTPUTS.values())),
            new ActivityTemplate(
                    FreeResourceTask.class,
                    FreeResourceTask.class.getName(),
                    "1",
                    Arrays.asList(FreeResourceTask.INPUTS.values()),
                    Arrays.asList(FreeResourceTask.OUTPUTS.values())),
            new ActivityTemplate(
                    AllocatePreviewResourceTask.class,
                    AllocatePreviewResourceTask.class.getName(),
                    "1",
                    Arrays.asList(AllocatePreviewResourceTask.INPUTS.values()),
                    Arrays.asList(AllocatePreviewResourceTask.OUTPUTS.values())),
            new ActivityTemplate(
                    AutoFreeResourceTask.class,
                    AutoFreeResourceTask.class.getName(),
                    "1",
                    Arrays.asList(AutoFreeResourceTask.INPUTS.values()),
                    Arrays.asList(AutoFreeResourceTask.OUTPUTS.values())),
            new ActivityTemplate(
                    AllocateDriveResourceTask.class,
                    AllocateDriveResourceTask.class.getName(),
                    "1",
                    Arrays.asList(AllocateDriveResourceTask.INPUTS.values()),
                    Arrays.asList(AllocateDriveResourceTask.OUTPUTS.values())),
            new ActivityTemplate(
            		DeleteResourceTask.class,
            		DeleteResourceTask.class.getName(),
                    "1",
                    Arrays.asList(DeleteResourceTask.INPUTS.values()),
                    Arrays.asList(DeleteResourceTask.OUTPUTS.values())),
    };

    public static final ActivityTemplate RESERVE  = templates[0];
    public static final ActivityTemplate RELEASE  = templates[1];
    public static final ActivityTemplate ALLOCATE = templates[2];
    public static final ActivityTemplate FREE     = templates[3];
    public static final ActivityTemplate ALLOCATE_PREVIEW = templates[4];
    public static final ActivityTemplate AUTO_FREE        = templates[5];

    public static ActivityTemplate getTemplate(String id) {
        for (ActivityTemplate template : templates) {
            if (template.getId().equals(id))
                return template;
        }
        return null;
    }
    public static ActivityTemplate getTemplate(String name, String version) {
        for (ActivityTemplate template : templates) {
            if (template.getName().equals(name) && template.getVersion().equals(version))
                return template;
        }
        return null;
    }

    public static List<ActivityTemplate> getTemplates() {
        return Arrays.asList(templates);
    }

    public static List<ActivityProviderType> getProviderTypes() {
        return new ArrayList<>();//Arrays.asList(activityProviderTypes);
    }
/*
    static ActivityProviderType activityProviderTypes[] = { new ActivityProviderType(
            "MWA", "v1", null, Arrays.asList(ProviderParams.values()))
            .addActivityTemplateList(Arrays.asList(templates)), };

    public enum ProviderParams implements IParameterDefinition {
        ;

        private ProviderParams(IParameterType type, boolean required) {
            this.type = type;
            this.required = required;
        }

        public IParameterType getType() {
            return type;
        }

        IParameterType type;
        boolean required;

        @Override
        public boolean getRequired() {
            return required;
        }

        @Override
        public String getKey() {
            return this.name();
        }

        @Override
        public String getTypeName() {
            return type.name();
        }
    }*/
}
