package com.sony.pro.mwa.repository.database;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/11/25
 * Time: 6:51
 * To change this template use File | Settings | File Templates.
 */
public abstract class DatabaseBaseDao<T, C extends Collection<T>, Q extends QueryCriteria> {
    protected JdbcTemplate jdbcTemplate;
    private IQuery<T, C, Q> query;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        query = createQuery(jdbcTemplate);
    }

    protected abstract IQuery<T, C, Q> createQuery(JdbcTemplate jdbcTemplate);

    protected C collection(Q criteria) {
        return query.collection(criteria);
    }

    protected T first(Q criteria) {
        return query.first(criteria);
    }

    protected long filteringCount(Q criteria) {
        return query.filteringCount(criteria);
    }

    protected long totalCount(Q criteria) {
        return query.totalCount(criteria);
    }
}
