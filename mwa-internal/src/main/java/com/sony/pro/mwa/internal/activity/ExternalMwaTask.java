package com.sony.pro.mwa.internal.activity;

import java.util.Arrays;
import java.util.List;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.service.provider.ActivityProviderType;

public class ExternalMwaTask {

	@SuppressWarnings("serial")
	static ActivityTemplate templates[] = {
			new ActivityTemplate(
					ExternalMwaPollingTask.class,
					ExternalMwaPollingTask.class.getName(),
					"1",
					Arrays.asList(ExternalMwaPollingTask.INPUTS.values()),
					Arrays.asList(ExternalMwaPollingTask.OUTPUTS.values())),
			new ActivityTemplate(
					CleanupActivityInstanceTask.class,
					CleanupActivityInstanceTask.class.getName(),
					"1",
					Arrays.asList(CleanupActivityInstanceTask.INPUTS.values()),
					Arrays.asList(CleanupActivityInstanceTask.OUTPUTS.values())),

	};

	public static final ActivityTemplate POLLING = templates[0];
    public static final ActivityTemplate CLEANUP  = templates[1];

	public static ActivityTemplate getTemplate(String id) {
		for (ActivityTemplate template : templates) {
			if (template.getId().equals(id))
				return template;
		}
		return null;
	}

	public static ActivityTemplate getTemplate(String name, String version) {
		for (ActivityTemplate template : templates) {
			if (template.getName().equals(name) && template.getVersion().equals(version))
				return template;
		}
		return null;
	}

	public static List<ActivityTemplate> getTemplates() {
		return Arrays.asList(templates);
	}

	public static List<ActivityProviderType> getProviderTypes() {
		return Arrays.asList(activityProviderTypes);
	}

	static ActivityProviderType activityProviderTypes[] = { new ActivityProviderType("ExternalMWA", "v2", null, Arrays.asList(ProviderParams.values())).addActivityTemplateList(Arrays
			.asList(templates)), };

	public static boolean isExternalProvider(String providerType) {
		return "ExternalMWA".equalsIgnoreCase(providerType);
	}

	public enum ProviderParams implements IParameterDefinition {
		;

		private ProviderParams(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		public IParameterType getType() {
			return type;
		}

		IParameterType type;
		boolean required;

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public String getTypeName() {
			return type.name();
		}
	}
}
