package com.sony.pro.mwa.internal.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.executor.ExecutorTaskCallable;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.utils.ParameterUtils;

public class CleanupActivityInstanceTaskCallable extends ExecutorTaskCallable {

	private static final long INTGER_MAXVALUE = 2147483647L;
	private static final String SUCCESS = "Success";
	private static final String NOT_FOUND = "NotFound";
	private static final String FAILURE = "Failure";

	private IActivityManager activityManager;
	List<? extends IParameterDefinition> typeDefs;
	private int resultCount = 0;
	private int totalCount = 0;

	private static final MwaLogger logger = MwaLogger.getLogger(CleanupActivityInstanceTaskCallable.class);

	public CleanupActivityInstanceTaskCallable(IActivityManager activityManager, List<? extends IParameterDefinition> typeDefs) {
		super();
		this.activityManager = activityManager;
		this.typeDefs = typeDefs;
	}

	@Override
	public void validInput(Map<String, Object> input) throws Exception {
		logger.info("validInput begin");
		Map<String, Object> inputMap = ParameterUtils.FROM_STRING.convert(typeDefs, input);
		
		Map<String, Object> outputMap = new HashMap<>();
		outputMap.put(CleanupActivityInstanceTask.OUTPUTS.result.name(), FAILURE);
		outputMap.put(CleanupActivityInstanceTask.OUTPUTS.resultCount.name(), 0);

		// offsetのMAXVALUE値チェック
		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.offset.name())) {
			final Long offset = (Long) inputMap.get(CleanupActivityInstanceTask.INPUTS.offset.name());
			if (offset == null || offset.longValue() > INTGER_MAXVALUE) {
				this.setErrorOutputs(outputMap);
				final String errMsg = "offset is invalid input!";
				logger.error(errMsg);
				throw new MwaInstanceError(MWARC.INVALID_INPUT, null, errMsg);
			}
		}

		// limitのMAXVALUE値チェック
		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.limit.name())) {
			final Long limit = (Long) inputMap.get(CleanupActivityInstanceTask.INPUTS.limit.name());
			if (limit == null || limit.longValue() > INTGER_MAXVALUE) {
				this.setErrorOutputs(outputMap);
				final String errMsg = "limit is invalid input!";
				logger.error(errMsg);
				throw new MwaInstanceError(MWARC.INVALID_INPUT, null, errMsg);
			}
		}

		// retentionDaysの妥当性チェック
		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.retentionDays.name())) {
			final Long retentionDays = (Long) inputMap.get(CleanupActivityInstanceTask.INPUTS.retentionDays.name());
			if (retentionDays == null || retentionDays.longValue() > INTGER_MAXVALUE || retentionDays.longValue() < 0) {
				this.setErrorOutputs(outputMap);
				final String errMsg = "retentionDays is invalid input!";
				logger.error(errMsg);
				throw new MwaInstanceError(MWARC.INVALID_INPUT, null, errMsg);
			}
		}
		
		inputMap = null;
		outputMap = null;

		logger.info("validInput end");
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> doJob(Map<String, Object> input) throws Exception {
		logger.info("doJob begin");

		Map<String, Object> outputMap = new HashMap<>();
		
		List<String> sortKey = new ArrayList<>();
		List<String> filters = new ArrayList<>();
		Integer offset = null;
		Integer limit = null;
		int retentionDays = 0;
		
		Map<String, Object> inputMap = ParameterUtils.FROM_STRING.convert(typeDefs, input);
		
		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.sort.name())) {
			sortKey = (List<String>) inputMap.get(CleanupActivityInstanceTask.INPUTS.sort.name());
		}
		
		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.filter.name())) {
			filters = (List<String>) inputMap.get(CleanupActivityInstanceTask.INPUTS.filter.name());
		}

		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.offset.name())) {
			final Long offsetLong = (Long) inputMap.get(CleanupActivityInstanceTask.INPUTS.offset.name());
			final String offsetStr = Long.toString(offsetLong);
			offset = Integer.parseInt(offsetStr);
		}

		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.limit.name())) {
			final Long limitLong = (Long) inputMap.get(CleanupActivityInstanceTask.INPUTS.limit.name());
			final String limitStr = Long.toString(limitLong);
			limit = Integer.parseInt(limitStr);
		}

		if (inputMap.containsKey(CleanupActivityInstanceTask.INPUTS.retentionDays.name())) {
			final Long retentionDaysLong = (Long) inputMap.get(CleanupActivityInstanceTask.INPUTS.retentionDays.name());
			final String retentionDaysLongStr = Long.toString(retentionDaysLong);
			retentionDays = Integer.parseInt(retentionDaysLongStr);
		}
		
		Calendar cal = Calendar.getInstance();	

		if(retentionDays > 1){
			cal.add(Calendar.DATE, ((retentionDays - 1) * -1));
		}
		
		if(retentionDays > 0){
			cal.set(Calendar.AM_PM, 0);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		}

		final long retentionDaysFilterLong = cal.getTimeInMillis();
    	
		String retentionDaysFilter = String.format("endTime<%s", retentionDaysFilterLong);
		filters.add(retentionDaysFilter);

		//IndexEngine側の削除など、cascadeで消すと整合性がとれなくなるケースがあり、
		//子供から順に消す必要があるが、createTimeのソートで代用
		String creationDateSort = String.format("createTime-", retentionDaysFilterLong);
		sortKey.add(creationDateSort);

		ActivityInstanceCollection models = this.activityManager.getInstances(sortKey, filters, offset, limit);

		String result = null;

		if (models != null) {
			
			String modelsCoountStr = models.getCount().toString();
			this.totalCount = Integer.parseInt(modelsCoountStr);
			
			for (ActivityInstanceModel model : models.getModels()) {
				ActivityInstanceModel activityInstanceModel = ((IActivityManagerInternal) this.activityManager).deleteInstance(model.getId());
				if (activityInstanceModel == null) {
					// 削除失敗　削除処理が失敗。　
					result = FAILURE;
					break;
				}
				resultCount ++;
				result = SUCCESS;
			}
			
			if(this.totalCount == 0){
				//削除対象なし
				result = NOT_FOUND;
			}

		} else {
			// modelsがnullのことがないため　発生しないはず。
			result = NOT_FOUND;
		}

		outputMap.put(CleanupActivityInstanceTask.OUTPUTS.result.name(), result);
		outputMap.put(CleanupActivityInstanceTask.OUTPUTS.resultCount.name(), resultCount);

		logger.info("doJob end");
		return outputMap;
	}

	@Override
	public Boolean cancelActivity() {
		return false;
	}

	@Override
	public Integer getAcitivityProgress() {

		if(this.totalCount == 0){
			return 0;
		}else{
			return (this.resultCount / this.totalCount) * 100;
		}
	}

}
