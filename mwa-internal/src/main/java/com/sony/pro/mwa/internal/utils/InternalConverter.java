package com.sony.pro.mwa.internal.utils;

import java.util.Map;

import javax.xml.bind.JAXBException;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.HashMapWrapper;

public class InternalConverter {
	private final static MwaLogger logger = MwaLogger.getLogger(InternalConverter.class);

	public static String mapToXml(Map<String, Object> map) {
		String result = null;
		javax.xml.bind.JAXBContext jc;
		try {
			HashMapWrapper wmap = new HashMapWrapper(map);
			jc = javax.xml.bind.JAXBContext.newInstance(HashMapWrapper.class);
			javax.xml.bind.Marshaller mu = jc.createMarshaller();
			java.io.StringWriter writer = new java.io.StringWriter();
			mu.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT,  Boolean.TRUE);
			mu.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			mu.marshal(wmap, writer);
			result = writer.toString();
		} catch (JAXBException e) {
			logger.error("mapToXml", e);
		}
		return result;
	}

	public static Map<String, Object> xmlToMap(String xml) {
		if (xml == null || xml.isEmpty())
			return null;
		
		Map<String, Object> result = null;
		javax.xml.bind.JAXBContext jc;
		try {
			jc = javax.xml.bind.JAXBContext.newInstance(HashMapWrapper.class);
			javax.xml.bind.Unmarshaller umu = jc.createUnmarshaller();
			Object obj = umu.unmarshal(new java.io.StringReader(xml));
			result = HashMapWrapper.class.cast(obj);
		} catch (JAXBException e) {
			logger.error("xmlToMap", e);
		}
		return result;
	}
}
