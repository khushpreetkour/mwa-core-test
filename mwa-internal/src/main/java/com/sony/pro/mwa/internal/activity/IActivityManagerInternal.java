package com.sony.pro.mwa.internal.activity;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.service.activity.IActivityManager;

public interface IActivityManagerInternal extends IActivityManager {
	public ActivityInstanceModel deleteInstance(String instanceId);
	public ActivityInstanceCollection deleteInstances(List<String> sorts, List<String> filters, Integer offset, Integer limit);
	@Deprecated
	public void operateActivityAsync(String instanceId, String operation, Map<String, Object> params);
}
