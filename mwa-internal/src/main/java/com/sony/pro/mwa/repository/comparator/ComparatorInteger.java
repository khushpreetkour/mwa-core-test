package com.sony.pro.mwa.repository.comparator;

import java.lang.reflect.Field;

import com.sony.pro.mwa.common.log.MwaLogger;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/07
 * Time: 7:00
 * To change this template use File | Settings | File Templates.
 */
public class ComparatorInteger extends Comparator {

	private final static MwaLogger logger = MwaLogger.getLogger(ComparatorInteger.class);
    @Override
    public int compare(Object o1, Object o2) {
        Integer io1 = 0;
        Integer io2 = 0;
        Field field = null;
        try {
            field = o1.getClass().getDeclaredField(this.name);
            field.setAccessible(true);
            io1 = (Integer) field.get(o1);
            io2 = (Integer) field.get(o2);
        } catch (NoSuchFieldException e) {
            logger.error("compare: ", e);	//To change body of catch statement use File | Settings | File Templates.
        }  catch (IllegalAccessException e) {
        	logger.error("compare: ", e);  //To change body of catch statement use File | Settings | File Templates.
        }

        return io1 - io2;
    }

    @Override
    public boolean compareTo(Object o1, String value) {
        Integer io1 = null;
        String  so1 = "";
        Field field = null;
        try {
            field = o1.getClass().getDeclaredField(this.name);
            field.setAccessible(true);
            io1 = (Integer) field.get(o1);
            if(io1 != null)
                so1 = io1.toString();
        } catch (NoSuchFieldException e) {
        	logger.error("compareTo: ", e);  //To change body of catch statement use File | Settings | File Templates.
        }  catch (IllegalAccessException e) {
        	logger.error("compareTo: ", e);  //To change body of catch statement use File | Settings | File Templates.
        }

        return so1.compareTo(value) == 0;
    }
}
