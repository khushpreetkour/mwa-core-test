package com.sony.pro.mwa.control.activity;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;

public class ParallelBranchTask extends SyncTaskBase {
	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		return OperationResult.newInstance();
	}
	
	public enum INPUTS implements IParameterDefinition {
		;

		IParameterType type;
		boolean required;
		String key;
		
		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}
		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}
		
		@Override
		public boolean getRequired() { return required; }
		@Override
		public String getKey() { return this.name(); }
		@Override
		public IParameterType getType() { return type; }
		@Override
		public String getTypeName() {return this.getType().name();}
	}
	
	public enum OUTPUTS implements IParameterDefinition {
		;

		IParameterType type;
		boolean required;
		
		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() { return required; }
		@Override
		public String getKey() { return this.name(); }
		@Override
		public IParameterType getType() { return type; }
		@Override
		public String getTypeName() {return this.getType().name();}
	}
}
