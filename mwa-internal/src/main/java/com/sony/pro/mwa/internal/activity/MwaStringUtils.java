package com.sony.pro.mwa.internal.activity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.enumeration.ServiceEndpointType;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.provider.IActivityProvider;

/**
 * ODAライブラリとの通信関連で使用するStringUtilsクラスです。
 */
public class MwaStringUtils {
	/**
	 * baseUrlを置換してFMと通信するためのURLを作成します。
	 * 
	 * @param baseUrl
	 *            FMと通信するときの置換前URL
	 *            ex.「http://%1$s:%2$s/library/networkPath?applicationId=%3$s」
	 * @param serviceEndPointModel
	 *            TaskのServiceEndpointModel
	 * @param map
	 *            IActivityProviderのProperties
	 * @return FMと通信するための完全URL
	 */
	public static String createUrl(String scheme, String apiVersion, String resource, IActivityProvider provider) {
		// Host、Port、ApplocationIdを取得
		
		ServiceEndpointModel endpoint = provider.getEndpoints().get(ServiceEndpointType.DEFAULT.name());

		String baseUrl = scheme + "://";
		String url = null;
		if (endpoint != null) {
			final String host = endpoint.getHost();
			final Integer port = endpoint.getPort();
			baseUrl += host;
			if (port != null)
				baseUrl += ":" + port;

			//ToDo: 暫定です。。ModelNumberからAPI Versionとりたかったんですがね。。
			baseUrl += "/mwa/api/" + apiVersion;
			url = baseUrl + "/" + resource + "/";
		}
		else {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, "", "proxy id is not specified.");
		}
		return url;
	}

	/**
	 * 
	 * @param targetMap
	 * @return
	 */
	public static String convetMapToJson(Map<String, Object> targetMap) {
		String json = null;
/*		try {
			ObjectMapper objectMapper = new ObjectMapper();
			json = objectMapper.writeValueAsString(targetMap);
		} catch (JsonGenerationException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json generation error getPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json mapping error getPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"jason mapping IO error getPersistentDataImpl failed.");
		}*/
		json = "[";
		boolean flag = false;
		for (Map.Entry<String, Object> p : targetMap.entrySet()) {
			if (flag) { json += ","; }
			json += "{";
			json += "\"key\": \"" + p.getKey().replace("\\", "\\\\") + "\"";
			json += ",\"value\": \"" + ((String)p.getValue()).replace("\\", "\\\\") + "\"";
			json += "}";
			flag = true;
		}
		json += "]";

		return json;
	}

	/**
	 * 
	 * @param json
	 * @return
	 */
	public static Map<String, Object> convertJsonToMap(String json) {
		// json(mwa) --> map(activity)
		Map<String, Object> kv = null;
		try {
			kv = new ObjectMapper().readValue(json, HashMap.class);
		} catch (JsonParseException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json generation error setPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json mapping error setPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"jason mapping IO error setPersistentDataImpl failed.");
		}
		return kv;
	}

	/**
	 * 
	 * @param targetMap
	 * @return
	 */
	public static String convetListToJson(List<Object> targetList) {
		String json = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			json = objectMapper.writeValueAsString(targetList);
		} catch (JsonGenerationException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json generation error getPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json mapping error getPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"jason mapping IO error getPersistentDataImpl failed.");
		}
		return json;
	}

	/**
	 * 
	 * @param json
	 * @return
	 */
	public static List<Object> convertJsonToList(String json) {
		// json(mwa) --> map(activity)
		List<Object> list = null;
		try {
			list = new ObjectMapper().readValue(json, List.class);
		} catch (JsonParseException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json generation error setPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"json mapping error setPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR,
					MWARC.INTEGRATION_ERROR.name(),
					"jason mapping IO error setPersistentDataImpl failed.");
		}
		return list;
	}

	/**
	 * urlをエンコードします。
	 * 
	 * エンコードの方式はURLEncode.encod(str, "UTF-8")に準拠し、
	 * エンコードされない文字"*"、"-"、" "をそれぞれ"%2a"、"%2d"、"%20" にreplaceします。
	 * 
	 * @param str
	 *            対象のurl
	 * @return エンコードされたurl
	 */
	public static String urlEncoder(String str) {
		try {
			str = URLEncoder.encode(str, "UTF-8");
			str = str.replace("*", "%2a");
			str = str.replace("-", "%2d");
			str = str.replace("+", "%20");
		} catch (UnsupportedEncodingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		}

		return str;
	}

}
