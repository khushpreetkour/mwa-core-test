package com.sony.pro.mwa.repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.repository.comparator.Comparator;
import com.sony.pro.mwa.repository.comparator.ComparatorInteger;
import com.sony.pro.mwa.repository.comparator.ComparatorString;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/04
 * Time: 7:35
 * To change this template use File | Settings | File Templates.
 */
public abstract class ObjectAccessObject<T> {
	private final static MwaLogger logger = MwaLogger.getLogger(ObjectAccessObject.class);
    private HashMap<String, Comparator> comparators; // <attributeName, Comparator>

    protected HashMap<Object, T> objectCollection;
    protected HashMap<String, Class> primitiveComparators = new HashMap<String, Class>(); // <type, Comparator>

    /**
     * constructor
     * <b>restricted to call constructor without any parameters</b>
     */
    private ObjectAccessObject() {}

    /**
     * constructor
     * @param entityClass
     */
    public ObjectAccessObject(Class<T> entityClass)
    {
        this.registerBasicComparators(entityClass);
        objectCollection = new HashMap<Object, T>();
    }

    /**
     * registers objects onto the list.
     * This will be called at the beginning of <code>getObjectCollection()</code>
     * and <code>readObjects()</code> to update the list.
     * Implementer should add Objects onto the list by calling <code>addObject(T object)</code>
     * in the inherited method.
     */
    protected abstract void registerObjects();

    private void updateList()
    {
        this.clearList();
        this.registerObjects();
    }

    /**
     * Gets the current list of all objects.
     * @return List
     */
    public HashMap<Object, T> getObjectCollection() {
        this.updateList();
        return objectCollection;
    }

    /**
     * Adds an Object onto the list.
     * @param object
     */
    protected void addObject(Object key, T object)
    {
        this.objectCollection.put(key, object);
    }

    /**
     * clears the list.
     */
    private void clearList()
    {
        this.objectCollection.clear();
    }

    /**
     * Get a list of Device
     * @param sortKey
     * @param isAscending
     * @param filterKey
     * @param filterValue
     * @param offset
     * @param limit
     * @return
     */
    public ArrayList<T> readObjects(
            String sortKey,
            Boolean isAscending,
            String  filterKey,
            String  filterValue,
            Integer offset,
            Integer limit
    )
    throws NotImplementedException
    {
        // update the list
        updateList();

        ArrayList<T> list1 = new ArrayList<T>();

        // filter
        if(this.objectCollection != null)
        {
            if(filterKey != null && !filterKey.equals(""))
            {
                Comparator comp = getComparator(filterKey);
                if(comp != null)
                    for(Object object : new ArrayList<T>(this.objectCollection.values()))
                    {
                        if(comp.compareTo(object, filterValue))
                            list1.add((T) object);
                    }
            }
            else
            {
                list1 = new ArrayList<T>(this.objectCollection.values());
            }
        }

        // sort
        if(sortKey != null && !sortKey.equals(""))
        {
            // throws exception if isAscending is null
            if(isAscending == null) throw new NotImplementedException();

            if(isAscending)
                Collections.sort(list1, getComparator(sortKey));
            else
                Collections.sort(list1, Collections.reverseOrder(getComparator(sortKey)));
        }

        // offset, limit
        ArrayList<T> list2 = new ArrayList<T>();
        if(offset == null) offset = 0;
        if(limit == null)  limit = list1.size();

        for(int i=offset; i<offset+limit; i++)
        {
            if(i<list1.size())
            {
                list2.add(list1.get(i));
            }
        }

        return list2;
    }

    /**
     * register additional comparators here by using <code>addComparetor(Stromg type, C;ass _class)</code>
     */
    protected void registerComparators() {}

    protected void addComparator(String type, Class _class)
    {
            primitiveComparators.put(type, _class);
    }

    private void registerBasicComparators(Class<T> entityClass)
    {
        try {
            addComparator("Integer", (new ComparatorInteger()).getClass());
            addComparator("String", (new ComparatorString()).getClass());
            registerComparators();

            // extract DeviceModel
            comparators = new HashMap<String, Comparator>();

            for (Field field : entityClass.getDeclaredFields()) {
//            for (Field field : DeviceModel.class.getDeclaredFields()) {
                String[] typeline = field.getGenericType().toString().split("\\.");
                String type = typeline[typeline.length - 1];
                Comparator comperator = null;
                if(primitiveComparators.get(type) != null)
                {
                    comperator = (Comparator) primitiveComparators.get(type).newInstance();
                    if(comperator != null)
                    {
                        comperator.setName(field.getName());
                        comparators.put(field.getName(), comperator);
                    }
                }
            }
        } catch (InstantiationException e) {
        	logger.error("registerBasicComparators", e);  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
        	logger.error("registerBasicComparators", e);  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private Comparator getComparator(String name)
    {
        return comparators.get(name);
    }
}
