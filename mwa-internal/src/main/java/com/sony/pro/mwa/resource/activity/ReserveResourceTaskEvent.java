package com.sony.pro.mwa.resource.activity;

import com.sony.pro.mwa.activity.framework.stm.EventType;
import com.sony.pro.mwa.service.activity.IEvent;

public enum ReserveResourceTaskEvent implements IEvent {
	NOTIFY_RESERVED		(EventType.NOTIFY),
	;
	
	EventType type;
	
	private ReserveResourceTaskEvent(EventType type) { this.type = type; }

	public boolean isMatch(String eventName) { return this.toString().equals(eventName); }
	public boolean isRequestEvent() { return this.type.isRequest(); }
	public boolean isNotifyEvent() { return this.type.isNotify(); }
	public boolean isConfirmEvent() { return this.type.isConfirm(); }

	@Override
	public String getName() {
		return this.name();
	}
}
