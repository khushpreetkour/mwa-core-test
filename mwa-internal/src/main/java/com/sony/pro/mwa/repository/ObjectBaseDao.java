package com.sony.pro.mwa.repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.Sorting;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 14/02/21
 * Time: 15:08
 * To change this template use File | Settings | File Templates.
 */
public abstract class ObjectBaseDao<T, C extends Collection<T>> {
    private static String generateComparatorKey(String fieldName, boolean isAscending) {
        return fieldName + Boolean.toString(isAscending);
    }

    private Map<String, java.util.Comparator<T>> comparatorMap = new HashMap<>();
    private String defaultComparatorKeyName;
	private final static MwaLogger logger = MwaLogger.getLogger(ObjectBaseDao.class);

    public ObjectBaseDao() {
        setComparators();
    }

    protected abstract void getAllObjects(Set<T> objectSet);

    protected abstract void setComparators();

    protected void addComparator(String fieldName, boolean isAscending, Comparator<T> comparator) {
        addComparator(fieldName, isAscending, comparator, false);
    }

    protected void addComparator(String fieldName, boolean isAscending, Comparator<T> comparator, boolean isDefault) {
        String keyName = generateComparatorKey(fieldName, isAscending);
        comparatorMap.put(keyName, comparator);
        if (comparatorMap.size() == 1 || isDefault == true) {
            defaultComparatorKeyName = keyName;
        }
    }

    protected boolean addObject(Set<T> objectSet, T object) {
        if (object != null) {
            return objectSet.add(object);
        } else {
            return false;
        }
    }

    protected boolean addObjects(Set<T> objectSet, List<T> objects) {
        if (CollectionUtils.isEmpty(objects) != true) {
            return objectSet.addAll(objects);
        } else {
            return false;
        }
    }

    protected void clearObjects(Set<T> objectSet) {
        objectSet.clear();
    }

    protected C getObjects(QueryCriteria criteria) {
        return getObjects(criteria, null);
    }

    protected C getObjects(QueryCriteria criteria, List<T> appendedObjects) {
        TreeSet<T> objectSet = new TreeSet<>(new ObjectComparator(comparatorMap, comparatorMap.get(defaultComparatorKeyName), criteria.getSortings()));
        if (CollectionUtils.isEmpty(appendedObjects) != true) {
            objectSet.addAll(appendedObjects);
        }

        getAllObjects(objectSet);

        long totalCount = 0;
        Iterator<T> iObject = objectSet.iterator();
        while (iObject.hasNext()) {
            T object = iObject.next();
            totalCount++;
            if (isMatching(object, criteria.getFilterings()) != true) {
                iObject.remove();
            }
        }

        long filteringCount = objectSet.size();
        NavigableSet<T> subObjectSet = objectSet;
        subObjectSet = getSubSet(subObjectSet, criteria.getOffset(), criteria.getLimit());
        C collection = newCollectionInstance();
        collection.setTotalCount(totalCount);
        collection.setCount(filteringCount);
        collection.setModels(new ArrayList<>(subObjectSet));
        return collection;
    }

    @SuppressWarnings("unchecked")
    private C newCollectionInstance() {
        try {
            Class<?> clazz = this.getClass();
            Type type = clazz.getGenericSuperclass();
            ParameterizedType pt = (ParameterizedType) type;
            Type[] actualTypeArguments = pt.getActualTypeArguments();
            Class<?> entityClass = (Class<?>) actualTypeArguments[1];
            return (C) entityClass.newInstance();
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    protected boolean isMatching(T object, List<List<Filtering>> filterings) {
        if (filterings != null) {
            for (List<Filtering> orFiltering : filterings) {
                if (isOrMatching(object, orFiltering) != true) {
                    return false;
                }
            }
        }
        return true;
    }

    protected boolean isOrMatching(Object object, List<Filtering> filterings) {
        try {
            for (Filtering filtering : filterings) {
                Class c = object.getClass();
                Method m = c.getMethod("get" + StringUtils.capitalize(filtering.getKey()));
                if (m != null) {
                    Object o = m.invoke(object);
                    if ((Integer.class.isInstance(o) && isMatching((Integer) o, filtering.getOperator(), Integer.parseInt(filtering.getValue()))) ||
                            (Long.class.isInstance(o) && isMatching((Long) o, filtering.getOperator(), Long.parseLong(filtering.getValue()))) ||
                            (String.class.isInstance(o) && isMatching((String) o, filtering.getOperator(), filtering.getValue())) ||
                            (Boolean.class.isInstance(o) && isMatching((Boolean) o, filtering.getOperator(), Boolean.parseBoolean(filtering.getValue()))) ||
                            (Enum.class.isAssignableFrom(o.getClass()) && isMatching(o.toString(), filtering.getOperator(), filtering.getValue()))) {
                        return true;
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            logger.error("isOrMatching", e);
        } catch (Exception e) {
            logger.error("isOrMatching", e);
        }
        return false;
    }

    protected boolean isMatching(String left, FilterOperatorEnum op, String right) {
        if (op == FilterOperatorEnum.EQUAL) {
            return StringUtils.equalsIgnoreCase(left, right);
        } else if (op == FilterOperatorEnum.NOT_EQUAL) {
            return !StringUtils.equalsIgnoreCase(left, right);
        } else if (op == FilterOperatorEnum.PARTIAL_MATCHING) {
            return StringUtils.contains(left, right);
        } else if (op == FilterOperatorEnum.PARTIAL_UNMATCHING) {
            return !StringUtils.contains(left, right);
        }
        return false;
    }

    protected boolean isMatching(Boolean left, FilterOperatorEnum op, Boolean right) {
        if (op == FilterOperatorEnum.EQUAL) {
            return left == right;
        } else if (op == FilterOperatorEnum.NOT_EQUAL) {
            return left != right;
        }
        return false;
    }

    protected boolean isMatching(Integer left, FilterOperatorEnum op, Integer right) {
        if (op == FilterOperatorEnum.EQUAL) {
            return left == right;
        } else if (op == FilterOperatorEnum.NOT_EQUAL) {
            return left != right;
        } else if (op == FilterOperatorEnum.GREATER_THAN) {
            return left > right;
        } else if (op == FilterOperatorEnum.LESS_THAN) {
            return left < right;
        } else if (op == FilterOperatorEnum.GREATER_THAN_OR_EQUAL) {
            return left >= right;
        } else if (op == FilterOperatorEnum.LESS_THAN_OR_EQUAL) {
            return left <= right;
        }
        return false;
    }

    protected boolean isMatching(Long left, FilterOperatorEnum op, Long right) {
        if (op == FilterOperatorEnum.EQUAL) {
            return left == right;
        } else if (op == FilterOperatorEnum.NOT_EQUAL) {
            return left != right;
        } else if (op == FilterOperatorEnum.GREATER_THAN) {
            return left > right;
        } else if (op == FilterOperatorEnum.LESS_THAN) {
            return left < right;
        } else if (op == FilterOperatorEnum.GREATER_THAN_OR_EQUAL) {
            return left >= right;
        } else if (op == FilterOperatorEnum.LESS_THAN_OR_EQUAL) {
            return left <= right;
        }
        return false;
    }

    private NavigableSet<T> getSubSet(NavigableSet<T> objectSet, long offset, long limit) {
        if (CollectionUtils.isEmpty(objectSet) == true) {
            return objectSet;
        } else if (objectSet.size() <= offset || limit == 0) {
            return new TreeSet<>();
        }

        int headIndex = 0;
        if (offset >= 0) {
            headIndex = (int) offset;
        }

        int tailIndex = objectSet.size() - 1;
        if (limit > 0) {
            tailIndex = headIndex + (int) limit - 1;
            if (tailIndex >= objectSet.size()) {
                tailIndex = objectSet.size() - 1;
            }
        }

        List<T> objects = new ArrayList<>(objectSet);
        return objectSet.subSet(objects.get(headIndex), true, objects.get(tailIndex), true);
    }

    private class ObjectComparator implements java.util.Comparator<T> {
        private List<java.util.Comparator<T>> comparators = new ArrayList<>();
        private java.util.Comparator<T> defaultComparator;

        public ObjectComparator(Map<String, java.util.Comparator<T>> comparatorMap, java.util.Comparator<T> defaultComparator, List<Sorting> sortings) {
            this.defaultComparator = defaultComparator;

            if (CollectionUtils.isEmpty(sortings) != true) {
                for (Sorting sorting : sortings) {
                    String comparatorKey = generateComparatorKey(sorting.getName(), sorting.isAscending());
                    if (comparatorMap.containsKey(comparatorKey) == true) {
                        comparators.add(comparatorMap.get(comparatorKey));
                    }
                }
            } else {
                comparators.add(defaultComparator);
            }
        }

        public int compare(T o1, T o2) {
            for (java.util.Comparator<T> comparator : comparators) {
                int diff;
                if ((diff = comparator.compare(o1, o2)) != 0) {
                    return diff;
                }
            }
            return defaultComparator.compare(o1, o2);
        }
    }
}
