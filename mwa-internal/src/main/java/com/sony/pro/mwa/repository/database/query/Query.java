package com.sony.pro.mwa.repository.database.query;

import com.sony.pro.mwa.common.utils.ListUtils;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.ColumnUtils;
import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;
import com.sony.pro.mwa.repository.query.QuerySqlGenerator;
import com.sony.pro.mwa.repository.query.Sorting;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.SQLXML;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 9:47
 * To change this template use File | Settings | File Templates.
 */
public abstract class Query<T, C extends Collection<T>, Q extends QueryCriteria> implements IQuery<T, C, Q> {
    private final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

    private static final Map<FilterOperatorEnum, SqlOperatorEnum> operatorMap;
    static {
        Map<FilterOperatorEnum, SqlOperatorEnum> map = new HashMap<>();
        map.put(FilterOperatorEnum.EQUAL, SqlOperatorEnum.EQUAL);
        map.put(FilterOperatorEnum.NOT_EQUAL, SqlOperatorEnum.NOT_EQUAL);
        map.put(FilterOperatorEnum.GREATER_THAN, SqlOperatorEnum.GREATER_THAN);
        map.put(FilterOperatorEnum.LESS_THAN, SqlOperatorEnum.LESS_THAN);
        map.put(FilterOperatorEnum.GREATER_THAN_OR_EQUAL, SqlOperatorEnum.GREATER_THAN_OR_EQUAL);
        map.put(FilterOperatorEnum.LESS_THAN_OR_EQUAL, SqlOperatorEnum.LESS_THAN_OR_EQUAL);
        map.put(FilterOperatorEnum.PARTIAL_MATCHING, SqlOperatorEnum.PARTIAL_MATCHING);
        map.put(FilterOperatorEnum.PARTIAL_UNMATCHING, SqlOperatorEnum.PARTIAL_UNMATCHING);
        operatorMap = Collections.unmodifiableMap(map);
    }

    private static final String filteringCriteriaFormat = "%s %s ? ";
    private static final String filteringCriteriaFormatForString = "UPPER(%s) %s UPPER(?) ";
    private static final String filteringCriteriaFormatForNULL = "%s IS NULL ";
    private static final String filteringCriteriaFormatForNOTNULL = "%s IS NOT NULL ";
    private static final String filteringCriteriaFormatForXMLElement = "'%4$s' %3$s ANY(xpath('//*[key = \"%1$s\"]/value/text()', %2$s)::text[]) ";
    private static final String filteringCriteriaFormatForXMLElementWithPartialMatching = "xpath('//*[key = \"%1$s\"]/value/text()', %2$s)::text %3$s '{%%%4$s%%}' ";
    private static final String filteringCriteriaFormatForNULLXMLElement = "xpath('//*[key = \"%s\"]/value/text()', %s)::text = '{}' ";
    private static final String filteringCriteriaFormatForNOTNULLXMLElement = "xpath('//*[key = \"%s\"]/value/text()', %s)::text != '{}' ";
    private static final String orderByClauseFormat = "ORDER BY %s ";
    private static final String limitClause = "LIMIT ? ";
    private static final String offsetClause = "OFFSET ? ";
    private static final String limitAndOffsetClause = "LIMIT ?, ? ";

    private final JdbcTemplate jdbcTemplate;
    private static DatabaseEnum database;
    private final String selectListString;
    private final String selectCountString;
    private final QuerySql baseQuerySql;
    private final Map<String, Column> columnMap;

    public Query(
            JdbcTemplate jdbcTemplate,
            Map<DatabaseEnum, String> selectListQueryMap,
            Map<DatabaseEnum, String> selectCountQueryMap,
            QuerySql baseQuerySql,
            Map<String, Column> columnMap) {
        this.jdbcTemplate = jdbcTemplate;
        this.database = DatabaseEnum.getDatabase(jdbcTemplate.getDataSource());
        this.selectListString = selectListQueryMap.get(database);
        this.selectCountString = selectCountQueryMap.get(database);
        this.baseQuerySql = baseQuerySql;
        this.columnMap = columnMap;
    }

    public Query(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.database = DatabaseEnum.getDatabase(jdbcTemplate.getDataSource());
        this.selectListString = null;
        this.selectCountString = null;
        this.baseQuerySql = null;
        this.columnMap = null;
    }

    public C collection(Q criteria) {

        QuerySql filteringQuerySql = createFilteringQuerySql(criteria, columnMap);
        QuerySql withRecursiveQuerySql = createWithRecursiveQuerySql(criteria, database);

        List<T> models = queryList(
                QuerySqlGenerator.generateListQuerySql(
                        selectListString,
                        withRecursiveQuerySql,
                        baseQuerySql,
                        filteringQuerySql,
                        createSortingQuerySql(criteria, columnMap),
                        createLimitationQuerySql(criteria, database)
                ),
                createModelMapper());

        RowMapper<Long> countMapper = null;

        long filteringCount = 0;
        if (isTopNCount(models, criteria) != true) {
            countMapper = createCountMapper();

            filteringCount = queryCount(
                    QuerySqlGenerator.generateFilteringCountQuerySql(
                            selectCountString,
                            withRecursiveQuerySql,
                            baseQuerySql,
                            filteringQuerySql
                    ),
                    countMapper
            );

        } else {
            filteringCount = models.size();
        }

        long totalCount = 0;
        if (isFiltering(filteringQuerySql) == true) {
            if (countMapper == null) {
                countMapper = createCountMapper();
            }

            totalCount = queryCount(
                    QuerySqlGenerator.generateTotalCountQuerySql(
                            selectCountString,
                            withRecursiveQuerySql,
                            baseQuerySql
                    ),
                    countMapper
            );
        } else {
            totalCount = filteringCount;
        }

        C collection = newCollectionInstance();
        collection.setModels(models);
        collection.setCount(filteringCount);
        collection.setTotalCount(totalCount);

        return collection;
    }

    public T first(Q criteria) {
        QuerySql querySql = QuerySqlGenerator.generateListQuerySql(
                selectListString,
                createWithRecursiveQuerySql(criteria, database),
                baseQuerySql,
                createFilteringQuerySql(criteria, columnMap),
                createSortingQuerySql(criteria, columnMap),
                createLimitationQuerySql(criteria, database)
        );
        return ListUtils.getValidFirstItem(queryList(querySql, createModelMapper()));
    }

    public long filteringCount(Q criteria) {
        return queryCount(
                QuerySqlGenerator.generateFilteringCountQuerySql(
                        selectCountString,
                        createWithRecursiveQuerySql(criteria, database),
                        baseQuerySql,
                        this.createFilteringQuerySql(criteria, columnMap)
                ),
                createCountMapper()
        );
    }

    public long totalCount(Q criteria) {
        return queryCount(
                QuerySqlGenerator.generateTotalCountQuerySql(
                        selectCountString,
                        createWithRecursiveQuerySql(criteria, database),
                        baseQuerySql
                ),
                createCountMapper()
        );
    }

    protected QuerySql createWithRecursiveQuerySql(Q criteria, DatabaseEnum database) {
        return null;
    }

    protected abstract RowMapper<T> createModelMapper();
    protected abstract RowMapper<Long> createCountMapper();

    protected boolean isTopNCount(List<T> models, QueryCriteria criteria) {
        if (criteria.getOffset() <= 0 && (criteria.getLimit() < 0 || criteria.getLimit() > models.size())) {
            return true;
        }

        return false;
    }

    protected boolean isFiltering(QuerySql filteringQuerySql) {
        return filteringQuerySql != null && StringUtils.isEmpty(filteringQuerySql.getString()) != true;
    }

    protected List<T> queryList(QuerySql querySql, RowMapper<T> rowMapper) {
        return query(querySql, rowMapper);
    }

    protected Long queryCount(QuerySql querySql, RowMapper<Long> rowMapper) {
        return query(querySql, rowMapper).get(0);
    }

    @SuppressWarnings("unchecked")
    private C newCollectionInstance() {
        try {
            Class<?> clazz = this.getClass();
            Type type = clazz.getGenericSuperclass();
            ParameterizedType pt = (ParameterizedType) type;
            Type[] actualTypeArguments = pt.getActualTypeArguments();
            Class<?> entityClass = (Class<?>) actualTypeArguments[1];
            return (C) entityClass.newInstance();
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    private <O> List<O> query(QuerySql queryString, RowMapper<O> rowMapper) {
        logger.debug(queryString.toString());
        return jdbcTemplate.query(queryString.getString(), queryString.getParamList().toArray(), rowMapper);
    }

    protected QuerySql createFilteringQuerySql(QueryCriteria criteria, Map<String, Column> columnMap) {
        String queryString = StringUtils.EMPTY;
        List<Object> queryParamList = new ArrayList<>();

        if (CollectionUtils.isEmpty(criteria.getFilterings()) != true) {
            for (List<Filtering> orFiltering : criteria.getFilterings()) {
                String partOfQueryString = StringUtils.EMPTY;

                for (Filtering filtering : orFiltering) {
                    if (StringUtils.isEmpty(filtering.getKey()) != true && columnMap.containsKey(filtering.getKey()) == true) {
                        if (StringUtils.isEmpty(filtering.getValue()) != true) {
                            if (columnMap.get(filtering.getKey()).getClazz() == String.class) {
                                partOfQueryString += String.format(filteringCriteriaFormatForString, columnMap.get(filtering.getKey()).getName(), operatorMap.get(filtering.getOperator()).toSymbol());
                                if (operatorMap.get(filtering.getOperator()) != SqlOperatorEnum.PARTIAL_MATCHING && operatorMap.get(filtering.getOperator()) != SqlOperatorEnum.PARTIAL_UNMATCHING) {
                                    queryParamList.add(ColumnUtils.valueOf(columnMap.get(filtering.getKey()).getClazz(), filtering.getValue()));
                                } else {
                                    queryParamList.add(ColumnUtils.valueOf(columnMap.get(filtering.getKey()).getClazz(), "%" + percentEscape(filtering.getValue()) + "%"));
                                }
                            } else if (columnMap.get(filtering.getKey()).getClazz() == SQLXML.class) {
                                String xmlFilteringFormat = filteringCriteriaFormatForXMLElement;
                                if (operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.PARTIAL_MATCHING || operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.PARTIAL_UNMATCHING) {
                                    xmlFilteringFormat = filteringCriteriaFormatForXMLElementWithPartialMatching;
                                }
                                partOfQueryString += String.format(xmlFilteringFormat, queryXmlEscape(filtering.getInnerKey()), columnMap.get(filtering.getKey()).getName(), operatorMap.get(filtering.getOperator()).toSymbol(), queryXmlEscape(filtering.getValue()));
                            } else {
                                if (operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.PARTIAL_MATCHING || operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.PARTIAL_UNMATCHING) {
                                    break;
                                }
                                partOfQueryString += String.format(filteringCriteriaFormat, columnMap.get(filtering.getKey()).getName(), operatorMap.get(filtering.getOperator()).toSymbol());
                                queryParamList.add(ColumnUtils.valueOf(columnMap.get(filtering.getKey()).getClazz(), filtering.getValue()));
                            }
                        } else {
                            if (columnMap.get(filtering.getKey()).getClazz() != SQLXML.class) {
                                if (operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.EQUAL) {
                                    partOfQueryString += String.format(filteringCriteriaFormatForNULL, columnMap.get(filtering.getKey()).getName());
                                } else if (operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.NOT_EQUAL) {
                                    partOfQueryString += String.format(filteringCriteriaFormatForNOTNULL, columnMap.get(filtering.getKey()).getName());
                                } else {
                                    break;
                                }
                            } else {
                                if (operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.EQUAL) {
                                    partOfQueryString += String.format(filteringCriteriaFormatForNULLXMLElement, queryXmlEscape(filtering.getInnerKey()), columnMap.get(filtering.getKey()).getName());
                                } else if (operatorMap.get(filtering.getOperator()) == SqlOperatorEnum.NOT_EQUAL) {
                                    partOfQueryString += String.format(filteringCriteriaFormatForNOTNULLXMLElement, queryXmlEscape(filtering.getInnerKey()), columnMap.get(filtering.getKey()).getName());
                                } else {
                                    break;
                                }
                            }
                        }
                    } else {
                        partOfQueryString += "FALSE ";
                    }
                    partOfQueryString += "OR ";
                }

                if (StringUtils.isEmpty(partOfQueryString) != true) {
                    partOfQueryString = StringUtils.removeEnd(partOfQueryString, " OR ");
                    queryString += String.format("AND (%s) ", partOfQueryString);
                }

            }
        }

        return new QuerySql(queryString, queryParamList);
    }

    protected QuerySql createSortingQuerySql(QueryCriteria criteria, Map<String, Column> columnMap) {
        String queryString = StringUtils.EMPTY;
        List<Object> queryParamList = new ArrayList<>();

        if (CollectionUtils.isEmpty(criteria.getSortings()) != true) {
            List<String> orderBys = new ArrayList<>();
            for (Sorting sorting : criteria.getSortings()) {
                if (columnMap.containsKey(sorting.getName()) == true && columnMap.get(sorting.getName()).isSortable() == true) {
                    orderBys.add(columnMap.get(sorting.getName()).getName() + " " + (sorting.isAscending() == true ? "ASC" : "DESC"));
                }
            }

            if (CollectionUtils.isEmpty(orderBys) != true) {
                queryString += String.format(orderByClauseFormat, org.springframework.util.StringUtils.collectionToCommaDelimitedString(orderBys));
            }
        }

        return new QuerySql(queryString, queryParamList);
    }

    protected QuerySql createLimitationQuerySql(QueryCriteria criteria, DatabaseEnum databaseEnum) {
        String queryString = StringUtils.EMPTY;
        List<Object> queryParamList = new ArrayList<>();

        if (databaseEnum == DatabaseEnum.MYSQL) {
            if (criteria.getOffset() >= 0 || criteria.getLimit() >= 0) {
                queryString += limitAndOffsetClause;
                if (criteria.getOffset() >= 0) {
                    queryParamList.add(criteria.getOffset());
                } else {
                    queryParamList.add(0);
                }

                if (criteria.getLimit() >= 0) {
                    queryParamList.add(criteria.getLimit());
                } else {
                    queryParamList.add(Long.MAX_VALUE);
                }
            }
        } else if (databaseEnum == DatabaseEnum.POSTGRESQL || databaseEnum == DatabaseEnum.H2SQL) {
            if (criteria.getLimit() >= 0) {
                queryString += limitClause;
                queryParamList.add(criteria.getLimit());
            }

            if (criteria.getOffset() >= 0) {
                queryString += offsetClause;
                queryParamList.add(criteria.getOffset());
            }
        }

        return new QuerySql(queryString, queryParamList);
    }

    private String percentEscape(String str) {
        return str != null ? str.replace("%", "\\%") : null;
    }
    private String queryXmlEscape(String str) {
        return str.replace("'", "''");
    }

	public static DatabaseEnum getDatabase() {
		return database;
	}

}
