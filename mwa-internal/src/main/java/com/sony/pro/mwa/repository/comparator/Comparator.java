package com.sony.pro.mwa.repository.comparator;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/07
 * Time: 7:23
 * To change this template use File | Settings | File Templates.
 */
public abstract class Comparator implements java.util.Comparator{
    protected String name;

    public Comparator() {}

    public Comparator(String name)
    {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * compareTo
     * @param o1 Object
     * @param o2 attribute to be compared with
     * @return
     */
    public abstract boolean compareTo(Object o1, String value);
}
