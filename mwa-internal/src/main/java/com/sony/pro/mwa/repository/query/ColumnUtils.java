package com.sony.pro.mwa.repository.query;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.common.utils.Converter;
import com.sony.pro.mwa.model.Wrapper;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 5:48
 * To change this template use File | Settings | File Templates.
 */
public class ColumnUtils {
	private final static MwaLogger logger = MwaLogger.getLogger(ColumnUtils.class);
    /**
     * @param clazz
     * @param s
     * @return
     */
    public static Object valueOf(Class<?> clazz, String s) {
        try {
            if (clazz.equals(Timestamp.class) == true) {
                if (StringUtils.isNumeric(s) == true) {
                    return new Timestamp(Long.valueOf(s));
                }
            } else if (clazz.equals(String.class) == true) {
                return s;
            } else if (clazz.equals(UUID.class) == true) {
                return UUID.fromString(s);
            } else if (clazz.equals(SQLXML.class) == true) {
                return null;
            } else {
                Method method = clazz.getMethod("valueOf", String.class);
                if (method != null) {
                    return method.invoke(null, s);
                }
            }
        } catch (Exception e) {
            logger.error("valueOf", e); 
        }

        return null;
    }

    public static String toString(ResultSet rs, Map<String, Column> columnMap, String name) throws SQLException {
        Column column = columnMap.get(name);
        if (rs.getObject(name) != null && column != null) {
            Class<?> clazz = column.getClazz();
            if (clazz == String.class) {
                return rs.getString(name);
            } else if (clazz == Boolean.class) {
                return String.valueOf(rs.getBoolean(name));
            } else if (clazz == Integer.class) {
                return String.valueOf(rs.getInt(name));
            } else if (clazz == Long.class) {
                return String.valueOf(rs.getLong(name));
            } else if (clazz == Float.class) {
                return String.valueOf(rs.getFloat(name));
            } else if (clazz == Timestamp.class) {
                return String.valueOf(rs.getTimestamp(name).getTime());
            } else if (clazz == SQLXML.class) {
                return rs.getSQLXML(name).getString();
            } else if (clazz == UUID.class) {
                return rs.getString(name);
            }
        }
        return null;
    }

    public static Long toLong(ResultSet rs, Map<String, Column> columnMap, String name) throws SQLException {
        Column column = columnMap.get(name);
        if (rs.getObject(name) != null && column != null) {
            Class<?> clazz = column.getClazz();
            if (clazz == String.class) {
                return Long.valueOf(rs.getString(name));
            } else if (clazz == Integer.class) {
                return Long.valueOf(rs.getInt(name));
            } else if (clazz == Long.class) {
                return rs.getLong(name);
            } else if (clazz == Float.class) {
                return Long.valueOf((int)rs.getFloat(name));
            } else if (clazz == Timestamp.class) {
                return rs.getTimestamp(name).getTime();
            }
        }
        return null;
    }

    public static Long parseLong(ResultSet rs, Map<String, Column> columnMap, String name) throws SQLException {
        Column column = columnMap.get(name);
        if (rs.getObject(name) != null && column != null) {
            Class<?> clazz = column.getClazz();
            if (clazz == Long.class) {
                return rs.getLong(name);
            } else if (clazz == Timestamp.class) {
                return rs.getTimestamp(name).getTime();
            }
        }
        return null;
    }

    public static Integer parseInt(ResultSet rs, Map<String, Column> columnMap, String name) throws SQLException {
        Column column = columnMap.get(name);
        if (rs.getObject(name) != null && column != null) {
            Class<?> clazz = column.getClazz();
            if (clazz == Integer.class) {
                return rs.getInt(name);
            }
        }
        return null;
    }

    public static Boolean parseBoolean(ResultSet rs, Map<String, Column> columnMap, String name) throws SQLException {
        Column column = columnMap.get(name);
        if (rs.getObject(name) != null && column != null) {
            Class<?> clazz = column.getClazz();
            if (clazz == Boolean.class) {
                return rs.getBoolean(name);
            }
        }
        return null;
    }

    public static <T> T parseObj(Class<? extends Wrapper<T>> wrappedClass, ResultSet rs, Map<String, Column> columnMap, String name) throws SQLException {
        Column column = columnMap.get(name);
        if (rs.getObject(name) != null && column != null) {
            if (column.getClazz() == SQLXML.class) {
                Wrapper<T> wrapper = Converter.toObject(rs.getString(name), wrappedClass);
                if (wrapper != null) {
                    return wrapper.getBody();
                }
            }
        }
        return null;
    }
}
