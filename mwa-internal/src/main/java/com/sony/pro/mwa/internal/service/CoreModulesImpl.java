package com.sony.pro.mwa.internal.service;

import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.service.activity.IActivityManager;
import com.sony.pro.mwa.service.event.IEventManager;
import com.sony.pro.mwa.service.kbase.IKnowledgeBase;
import com.sony.pro.mwa.service.provider.IActivityProviderManager;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.service.scheduler.ISchedulerManager;
import com.sony.pro.mwa.service.storage.IStorageManager;
import com.sony.pro.mwa.service.storage.IUriResolver;

public class CoreModulesImpl implements ICoreModules {

	IActivityManager activityManager;
	IKnowledgeBase knowledgeBase;
	IActivityProviderManager activityProviderManager;
	IStorageManager storageManager;
	IUriResolver uriResolver;
	IResourceManager resourceManager;
	IEventManager eventManager;
	ISchedulerManager schedulerManager;
	
	@Override
	public IActivityManager getActivityManager() {
		// TODO Auto-generated method stub
		return activityManager;
	}
	public CoreModulesImpl setActivityManager(IActivityManager activityManager) {
		this.activityManager = activityManager;
		return this;
	}

	@Override
	public IKnowledgeBase getKnowledgeBase() {
		// TODO Auto-generated method stub
		return knowledgeBase;
	}
	public CoreModulesImpl setKnowledgeBase(IKnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
		return this;
	}

	@Override
	public IActivityProviderManager getActivityProviderManager() {
		// TODO Auto-generated method stub
		return activityProviderManager;
	}
	public CoreModulesImpl setActivityProviderManager(IActivityProviderManager activityProviderManager) {
		this.activityProviderManager = activityProviderManager;
		return this;
	}

	@Override
	public IStorageManager getStorageManager() {
		// TODO Auto-generated method stub
		return storageManager;
	}
	public CoreModulesImpl setStorageManager(IStorageManager storageManager) {
		this.storageManager = storageManager;
		return this;
	}

	@Override
	public IUriResolver getUriResolver() {
		// TODO Auto-generated method stub
		return uriResolver;
	}
	public CoreModulesImpl setUriResolver(IUriResolver uriResolver) {
		this.uriResolver = uriResolver;
		return this;
	}
	@Override
	public IResourceManager getResourceManager() {
		// TODO Auto-generated method stub
		return resourceManager;
	}
	public CoreModulesImpl setResourceManager(IResourceManager resourceManager) {
		this.resourceManager = resourceManager;
		return this;
	}

	@Override
	public IEventManager getEventManager() {
		// TODO Auto-generated method stub
		return eventManager;
	}
	public CoreModulesImpl setEventManager(IEventManager eventManager) {
		this.eventManager = eventManager;
		return this;
	}
	@Override
	public ISchedulerManager getSchedulerManager() {
		// TODO Auto-generated method stub
		return schedulerManager;
	}
	public CoreModulesImpl setSchedulerManager(ISchedulerManager schedulerManager) {
		this.schedulerManager = schedulerManager;
		return this;
	}
}
