package com.sony.pro.mwa.resource.activity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.activity.framework.internal.AbsMwaActivity;
import com.sony.pro.mwa.enumeration.resource.ResourceEventParameter;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.resource.CostModel;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestEntryModel;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeCost;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeResource;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class ReserveResourceTask extends AbsMwaActivity {

	private static final int PROGRESS_100 = 100;
	private static final int indexCount = 2;
	private static String assignedResourceId[] = {OUTPUTS.assignedResourceId_0.getKey(), OUTPUTS.assignedResourceId_1.getKey()};
	private static String resourceHolderId[] = {OUTPUTS.resourceHolderId_0.getKey(), OUTPUTS.resourceHolderId_1.getKey()};
	private static String resourceHolderType[] = {OUTPUTS.resourceHolderType_0.getKey(), OUTPUTS.resourceHolderType_1.getKey()};

	public ReserveResourceTask() {
		super(ReserveResourceTaskState.CREATED, new ReserveResourceTaskStateMachine());
	}

	@Override
	protected OperationResult processEventImpl(IEvent event, Map<String, Object> params) {
		OperationResult result = OperationResult.newInstance();
		if (ReserveResourceTaskEvent.NOTIFY_RESERVED.equals(event)) {
			Map<String, Object> output = this.getOutputDetails();
			ResourceRequestModel request = this.getCoreModules().getResourceManager().getResourceRequest("★");
			if ( request.getResourceRequestEntryList() != null) {
				int i = 0;
				for (ResourceRequestEntryModel entry : request.getResourceRequestEntryList()) {
					String resourceId = entry.getAssignedResourceId();
					if (resourceId != null) {
						ResourceModel resource = this.getCoreModules().getResourceManager().getResource(resourceId);
						if (resource == null) {
							logger.error("Not found resource: resource=" + resourceId);
						} else {
							if (i == 0) {
								output.put(OUTPUTS.assignedResourceId.getKey(), assignedResourceId);
								output.put(OUTPUTS.resourceHolderId.getKey(), resource.getHolder());
								output.put(OUTPUTS.resourceHolderType.getKey(), resource.getHolderType());
							} else if (i >= indexCount) {
								logger.error("Over index: index=" + indexCount + ", resource=" + resourceId);
								break;
							}
							output.put(assignedResourceId[i], resource.getId());
							output.put(resourceHolderId[i], resource.getHolder());
							output.put(resourceHolderType[i], resource.getHolderType());
						}
					}
				}
			}

			this.setOutputDetails(output);
			result.put(output);
		}
		return result;
	}

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		if (this.getStatus().equals(ReserveResourceTaskState.QUEUED)) {
			this.initState = ReserveResourceTaskState.QUEUED; //★master-workerのフック処理への対処。全体的にちゃんとロジックを組みなおしたい。
			//params.get(key);
			String assignedResourceId = (String)params.get(ResourceEventParameter.ASSIGNED_RESOURCE_ID.name());
			String assignedResourceHolderId = (String)params.get(ResourceEventParameter.ASSIGNED_RESOURCE_HOLDER_ID.name());
			String assignedResourceHolderType = (String)params.get(ResourceEventParameter.ASSIGNED_RESOURCE_HOLDER_TYPE.name());

			Map<String, Object> output = this.getOutputDetails();
			if (assignedResourceId != null) {
				output.put(OUTPUTS.assignedResourceId.getKey(), assignedResourceId);
				output.put(OUTPUTS.assignedResourceId_0.getKey(), assignedResourceId);
			}
			if (assignedResourceHolderId != null) {
				output.put(OUTPUTS.resourceHolderId.getKey(), assignedResourceHolderId);
				output.put(OUTPUTS.resourceHolderId_0.getKey(), assignedResourceHolderId);
			}
			if (assignedResourceHolderType != null) {
				output.put(OUTPUTS.resourceHolderType.getKey(), assignedResourceHolderType);
				output.put(OUTPUTS.resourceHolderType_0.getKey(), assignedResourceHolderType);
			}
			this.setOutputDetails(output);
			this.updateProgress(PROGRESS_100);

			OperationResult result = OperationResult.newInstance();
			result.put(output);
			return result;
		}
		// TODO Auto-generated method stub
		String occupantId = (String)params.get(ReserveResourceTask.INPUTS.occupantId.getKey());
		String receiverId = (String)params.get(ReserveResourceTask.INPUTS.receiverId.getKey());
		String priorityStr = (String)params.get(ReserveResourceTask.INPUTS.priority.getKey());


		ResourceRequestModel request = new ResourceRequestModel();
		request.setId(this.getId());	//instanceIdをrequestIdに使う
		if (priorityStr != null) {
			List<String> priority = (List<String>)TypeListString.instance().fromString(priorityStr);
			request.setPriority(priority);
		}
		{
			ResourceRequestEntryModel resourceRequestEntryModel = null;

			final String costStr = (String)params.get(ReserveResourceTask.INPUTS.cost.getKey());
			final String costStr0 = (String)params.get(ReserveResourceTask.INPUTS.cost_0.getKey());
			final String costStr1 = (String)params.get(ReserveResourceTask.INPUTS.cost_1.getKey());

			//COST_0の入力値がない場合（レガシーな実装の場合）　互換用に残してる
			if(StringUtils.isEmpty(costStr0)){
				resourceRequestEntryModel = this.getResourceRequestEntry(request.getId(), (String)params.get(ReserveResourceTask.INPUTS.requestResourceId.getKey()), costStr);
				if (resourceRequestEntryModel != null) {
					request.addResourceRequestEntry(resourceRequestEntryModel);
				}
			}else{
				resourceRequestEntryModel = this.getResourceRequestEntry(request.getId(), (String)params.get(ReserveResourceTask.INPUTS.requestResourceId_0.getKey()), costStr0);
				if (resourceRequestEntryModel != null) {
					request.addResourceRequestEntry(resourceRequestEntryModel);
				}
				if(!StringUtils.isEmpty(costStr1)){
					resourceRequestEntryModel = this.getResourceRequestEntry(request.getId(), (String)params.get(ReserveResourceTask.INPUTS.requestResourceId_1.getKey()),costStr1);
					if (resourceRequestEntryModel != null) {
						request.addResourceRequestEntry(resourceRequestEntryModel);
					}
				}
			}
		}

		if (request.getResourceRequestEntryList() == null || request.getResourceRequestEntryList().isEmpty())
			throw new MwaInstanceError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);

		request.setOccupantId(occupantId);

		String occupantName = (String)params.get(ReserveResourceTask.INPUTS.occupantName.getKey());
		if (occupantName == null) {
			occupantName = this.getClass().getName();
		}
		request.setOccupantName(occupantName);
		request.setReceiverId(this.getId());
		request = this.getCoreModules().getResourceManager().addResourceRequest(request);

		String msg = "";
		for (Map.Entry<String, Object> entry : params.entrySet())
			msg += "(" + entry.getKey() + ", " + entry.getValue() + ")";
		logger.info("requestSubmitImpl: " + msg);

		Map<String, Object> output = new HashMap<>();
		output.put(OUTPUTS.requestId.getKey(), request.getId());
		this.setOutputDetails(output);
		return OperationResult.newInstance().put(OUTPUTS.requestId, request.getId());
	}

	private ResourceRequestEntryModel getResourceRequestEntry(String resourceRequestId, String resourceId, String costStr) {
		CostModel cost = (CostModel) TypeCost.instance().fromString(costStr);
		if (cost != null && resourceId != null)
			logger.info("[dbg] resourceId=" + resourceId + ", cost=" + cost.toString());
		else {
			logger.debug("invalid resource entry: resourceId=" + resourceId + ", cost=" + costStr);
			return null;
		}

		ResourceRequestEntryModel entry = new ResourceRequestEntryModel();
		entry.setResourceRequestId(resourceRequestId);
		entry.setResourceId(resourceId);
		entry.setCost(cost);
		return entry;
	}


	@Override
	protected List<? extends IEvent> getSupportedEventListImpl() {
		return Arrays.asList(ReserveResourceTaskEvent.values());
	}

	@Override
	protected List<? extends IState> getSupportedStateListImpl() {
		// TODO Auto-generated method stub
		return Arrays.asList(ReserveResourceTaskState.values());
	}

	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPersistentDataImpl() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPersistentDataImpl(String details) {
	}

	public enum INPUTS implements IParameterDefinition {
		occupantId(TypeString.instance(), true),
		occupantName(TypeString.instance(), false),
		receiverId(TypeString.instance(), false),
		priority(TypeListString.instance(), false),
		//List化する前のAPI
		@Deprecated
		requestResourceId(TypeString.instance(), false),
		@Deprecated
		cost(TypeCost.instance(), false),
		//List化後のAPI(パラメータマッピングがしやすいよう、resourceとcostをそれぞれ用意している)
		requestResourceId_0(TypeString.instance(), false),
		cost_0(TypeCost.instance(), false),
		requestResourceId_1(TypeString.instance(), false),
		cost_1(TypeCost.instance(), false),
		;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}
		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() { return required; }
		@Override
		public String getKey() { return this.name(); }
		@Override
		public IParameterType getType() { return type; }
		@Override
		public String getTypeName() {return this.getType().name();}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		requestId(TypeString.instance(), false),
		//List化する前のAPI
		@Deprecated
		assignedResourceId(TypeResource.instance(), true),
		@Deprecated
		resourceHolderId(TypeString.instance(), false),		//ResourceHolderIdを使って、ProviderなりLocationの情報を取ってもらう想定
		@Deprecated
		resourceHolderType(TypeString.instance(), false),
		//List化後のAPI(パラメータマッピングがしやすいよう、resourceとcostをそれぞれ用意している)
		assignedResourceId_0(TypeResource.instance(), false),
		resourceHolderId_0(TypeString.instance(), false),		//ResourceHolderIdを使って、ProviderなりLocationの情報を取ってもらう想定
		resourceHolderType_0(TypeString.instance(), false),
		assignedResourceId_1(TypeResource.instance(), false),
		resourceHolderId_1(TypeString.instance(), false),		//ResourceHolderIdを使って、ProviderなりLocationの情報を取ってもらう想定
		resourceHolderType_1(TypeString.instance(), false),
		;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() { return required; }
		@Override
		public String getKey() { return this.name(); }
		@Override
		public IParameterType getType() { return type; }
		@Override
		public String getTypeName() {return this.getType().name();}

		IParameterType type;
		boolean required;
	}

}
