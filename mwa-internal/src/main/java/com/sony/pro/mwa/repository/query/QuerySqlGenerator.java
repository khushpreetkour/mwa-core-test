package com.sony.pro.mwa.repository.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 6:03
 * To change this template use File | Settings | File Templates.
 */
public class QuerySqlGenerator {
    public static QuerySql generateTotalCountQuerySql(String selectCountString, QuerySql withRecursiveQuerySql, QuerySql baseQuerySql) {
        if (StringUtils.isEmpty(selectCountString) != true) {
            String queryString = selectCountString;
            List<Object> paramList = new ArrayList<>();
            if (withRecursiveQuerySql != null) {
                queryString = withRecursiveQuerySql.getString() + queryString;
                if (CollectionUtils.isEmpty(withRecursiveQuerySql.getParamList()) != true) {
                    paramList.addAll(withRecursiveQuerySql.getParamList());
                }
            }
            if (baseQuerySql != null) {
                queryString += baseQuerySql.getString();
                if (CollectionUtils.isEmpty(baseQuerySql.getParamList()) != true) {
                    paramList.addAll(baseQuerySql.getParamList());
                }
            }
            return new QuerySql(queryString, paramList);
        }
        return null;
    }

    public static QuerySql generateFilteringCountQuerySql(String selectCountString, QuerySql withRecursiveQuerySql, QuerySql baseQuerySql, QuerySql filteringQuerySql) {
        if (StringUtils.isEmpty(selectCountString) != true) {
            String queryString = selectCountString;
            List<Object> paramList = new ArrayList<>();
            if (withRecursiveQuerySql != null) {
                queryString = withRecursiveQuerySql.getString() + queryString;
                if (CollectionUtils.isEmpty(withRecursiveQuerySql.getParamList()) != true) {
                    paramList.addAll(withRecursiveQuerySql.getParamList());
                }
            }
            if (baseQuerySql != null) {
                queryString += baseQuerySql.getString();
                if (CollectionUtils.isEmpty(baseQuerySql.getParamList()) != true) {
                    paramList.addAll(baseQuerySql.getParamList());
                }
            }
            if (filteringQuerySql != null) {
                queryString += filteringQuerySql.getString();
                if (CollectionUtils.isEmpty(filteringQuerySql.getParamList()) != true) {
                    paramList.addAll(filteringQuerySql.getParamList());
                }
            }
            return new QuerySql(queryString, paramList);
        }
        return null;
    }

    public static QuerySql generateListQuerySql(String selectListString, QuerySql withRecursiveQuerySql, QuerySql baseQuerySql, QuerySql filteringQuerySql, QuerySql sortingQuerySql, QuerySql limitationQuerySql) {
        if (StringUtils.isEmpty(selectListString) != true) {
            String queryString = selectListString;
            List<Object> paramList = new ArrayList<>();
            if (withRecursiveQuerySql != null) {
                queryString = withRecursiveQuerySql.getString() + queryString;
                if (CollectionUtils.isEmpty(withRecursiveQuerySql.getParamList()) != true) {
                    paramList.addAll(withRecursiveQuerySql.getParamList());
                }
            }
            if (baseQuerySql != null) {
                queryString += baseQuerySql.getString();
                if (CollectionUtils.isEmpty(baseQuerySql.getParamList()) != true) {
                    paramList.addAll(baseQuerySql.getParamList());
                }
            }
            if (filteringQuerySql != null) {
                queryString += filteringQuerySql.getString();
                if (CollectionUtils.isEmpty(filteringQuerySql.getParamList()) != true) {
                    paramList.addAll(filteringQuerySql.getParamList());
                }
            }
            if (sortingQuerySql != null) {
                queryString += sortingQuerySql.getString();
                if (CollectionUtils.isEmpty(sortingQuerySql.getParamList()) != true) {
                    paramList.addAll(sortingQuerySql.getParamList());
                }
            }
            if (limitationQuerySql != null) {
                queryString += limitationQuerySql.getString();
                if (CollectionUtils.isEmpty(limitationQuerySql.getParamList()) != true) {
                    paramList.addAll(limitationQuerySql.getParamList());
                }
            }
            return new QuerySql(queryString, paramList);
        }
        return null;
    }
}