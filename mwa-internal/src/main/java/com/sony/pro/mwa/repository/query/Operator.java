package com.sony.pro.mwa.repository.query;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 6:00
 * To change this template use File | Settings | File Templates.
 */
public enum Operator {
    EQUAL("="),
    NOT_EQUAL("!="),
    GREATER_THAN(">"),
    LESS_THAN("<"),
    GREATER_THAN_OR_EQUAL(">="),
    LESS_THAN_OR_EQUAL("<="),
    IN("IN"),
    NOT_IN("NOT IN"),
    FORWARD_MATCHING("LIKE"),
    BACKWARD_MATCHING("LIKE"),
    PARTIAL_MATCHING("LIKE"),
    IS_NULL("IS NULL"),
    IS_NOT_NULL("IS NOT NULL");

    private String symbol;

    private Operator(String symbol) {
        this.symbol = symbol;
    }

    public String toSymbol() {
        return symbol;
    }
}
