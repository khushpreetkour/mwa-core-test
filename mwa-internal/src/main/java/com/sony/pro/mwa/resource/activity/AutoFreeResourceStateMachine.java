package com.sony.pro.mwa.resource.activity;


import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class AutoFreeResourceStateMachine implements IStateMachine {
	
	private static final AutoFreeResourceStateMachine instance = new AutoFreeResourceStateMachine();
	
	//状態遷移表の定義
	private final static Object[][] stTableDef = {
			//CurrentState,					　　　//Event,							　　　　　　//NextState
		{	AutoFreeResourceTaskState.READY,			AutoFreeResourceEvent.NOTIFY_STARTED,		AutoFreeResourceTaskState.IN_PROGRESS},
		{	AutoFreeResourceTaskState.READY,			AutoFreeResourceEvent.NOTIFY_UPDATED,		AutoFreeResourceTaskState.READY},	
		{	AutoFreeResourceTaskState.READY,			AutoFreeResourceEvent.NOTIFY_COMPLETED,		AutoFreeResourceTaskState.COMPLETED},
		{	AutoFreeResourceTaskState.CREATED,		CommonEvent.REQUEST_SUBMIT,           	AutoFreeResourceTaskState.IN_PROGRESS},
        {	AutoFreeResourceTaskState.CREATED,		CommonEvent.NOTIFY_ERROR,       		AutoFreeResourceTaskState.ERROR},
        {	AutoFreeResourceTaskState.IN_PROGRESS,			AutoFreeResourceEvent.REQUEST_START,	AutoFreeResourceTaskState.IN_PROGRESS},
        {	AutoFreeResourceTaskState.IN_PROGRESS,			AutoFreeResourceEvent.REQUEST_UPDATE,	AutoFreeResourceTaskState.IN_PROGRESS},
		{	AutoFreeResourceTaskState.IN_PROGRESS,			CommonEvent.NOTIFY_ERROR,				AutoFreeResourceTaskState.ERROR},
		{	AutoFreeResourceTaskState.IN_PROGRESS,			AutoFreeResourceEvent.NOTIFY_UPDATED,	AutoFreeResourceTaskState.IN_PROGRESS},
		{	AutoFreeResourceTaskState.IN_PROGRESS,		AutoFreeResourceEvent.REQUEST_STOP,		AutoFreeResourceTaskState.MANUAL_COMPLETED},
		{	AutoFreeResourceTaskState.IN_PROGRESS,			AutoFreeResourceEvent.NOTIFY_STOP,		AutoFreeResourceTaskState.COMPLETED},
	};

	@SuppressWarnings("serial")
	private final static HashMap<Map.Entry<IState, IEvent>, IState> stTable = new HashMap<Map.Entry<IState, IEvent>, IState>() {{
		//初期化処理、stTableDefをMapに詰めてるだけ(直接Mapで定義してもよかったが、定義を配列で切り出した方がシンプルで管理しやすいので)
		for (int i = 0; i < stTableDef.length; i++) {
			put(new AbstractMap.SimpleEntry<IState, IEvent>((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	private AutoFreeResourceStateMachine() {

	}
	
	public static AutoFreeResourceStateMachine getInstance() {
        return instance;
    }
	
	public boolean isAcceptable(IState current, IEvent event) {
		IState next = getNextState(current, event);
		return (next != null) ? true : false;
	}

	public IState getNextState(IState state, IEvent event) {
		//Mapから取り出して返す
		return stTable.get(new AbstractMap.SimpleEntry<IState, IEvent>(state, event));
	}
}
