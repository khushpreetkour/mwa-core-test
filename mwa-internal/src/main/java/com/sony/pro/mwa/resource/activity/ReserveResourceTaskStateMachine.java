package com.sony.pro.mwa.resource.activity;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.sony.pro.mwa.activity.framework.stm.AbsStateMachine;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class ReserveResourceTaskStateMachine extends AbsStateMachine {
	
	//State Transition Table
	private final static Object[][] stTableDef = {
			//CurrentState,					//Event,											//NextState
		{	ReserveResourceTaskState.QUEUED,	CommonEvent.REQUEST_SUBMIT,		ReserveResourceTaskState.COMPLETED},
		{	ReserveResourceTaskState.QUEUED,	CommonEvent.NOTIFY_ERROR,		ReserveResourceTaskState.ERROR},
		{	ReserveResourceTaskState.QUEUED,	CommonEvent.REQUEST_CANCEL,		ReserveResourceTaskState.CANCELLED},
		{	ReserveResourceTaskState.CREATED,	CommonEvent.REQUEST_SUBMIT,		ReserveResourceTaskState.QUEUED},
		{	ReserveResourceTaskState.CREATED,	CommonEvent.NOTIFY_ERROR,		ReserveResourceTaskState.ERROR},
		{	ReserveResourceTaskState.CREATED,	CommonEvent.REQUEST_CANCEL,		ReserveResourceTaskState.CANCELLED},
	};

	private final static HashMap<Pair<IState, IEvent>, IState> stTable = new HashMap<Pair<IState, IEvent>, IState>() {{
		//Initialization
		for (int i = 0; i < stTableDef.length; i++) {
			put(Pair.of((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	@Override
	protected Map<Pair<IState, IEvent>, IState> getStateTransitionTable() {
		return stTable;
	}
}
