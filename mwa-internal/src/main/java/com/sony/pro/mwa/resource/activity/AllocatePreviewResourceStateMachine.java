package com.sony.pro.mwa.resource.activity;


import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.standard.PollingTaskEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class AllocatePreviewResourceStateMachine implements IStateMachine {
	
	private static final AllocatePreviewResourceStateMachine instance = new AllocatePreviewResourceStateMachine();
	
	//状態遷移表の定義
	private final static Object[][] stTableDef = {
			//CurrentState,					　　　//Event,							　　　　　　//NextState
		{	AllocatePreviewResourceTaskState.CREATED,		CommonEvent.REQUEST_SUBMIT,			AllocatePreviewResourceTaskState.IN_PROGRESS},
		{	AllocatePreviewResourceTaskState.CREATED,		CommonEvent.NOTIFY_ERROR,			AllocatePreviewResourceTaskState.ERROR},
		{	AllocatePreviewResourceTaskState.IN_PROGRESS,	CommonEvent.REQUEST_CANCEL,			AllocatePreviewResourceTaskState.CANCELLING},
		{	AllocatePreviewResourceTaskState.IN_PROGRESS,	PollingTaskEvent.NOTIFY_COMPLETED,		AllocatePreviewResourceTaskState.COMPLETED},
		{	AllocatePreviewResourceTaskState.IN_PROGRESS,	PollingTaskEvent.NOTIFY_UPDATED,		AllocatePreviewResourceTaskState.IN_PROGRESS},
		{	AllocatePreviewResourceTaskState.IN_PROGRESS,	CommonEvent.NOTIFY_ERROR,			AllocatePreviewResourceTaskState.ERROR},
		
		{	AllocatePreviewResourceTaskState.IN_PROGRESS,	AllocatePreviewResourceEvent.REQUEST_UPDATE, AllocatePreviewResourceTaskState.IN_PROGRESS},
		{	AllocatePreviewResourceTaskState.IN_PROGRESS,	AllocatePreviewResourceEvent.REQUEST_STOP,	AllocatePreviewResourceTaskState.MANUAL_COMPLETED},
		{	AllocatePreviewResourceTaskState.CANCELLING,	PollingTaskEvent.NOTIFY_UPDATED,		AllocatePreviewResourceTaskState.CANCELLING},
		{	AllocatePreviewResourceTaskState.CANCELLING,	PollingTaskEvent.NOTIFY_COMPLETED,		AllocatePreviewResourceTaskState.COMPLETED},
		{	AllocatePreviewResourceTaskState.CANCELLING,	CommonEvent.NOTIFY_ERROR,			AllocatePreviewResourceTaskState.ERROR},
		{	AllocatePreviewResourceTaskState.CANCELLING,	PollingTaskEvent.NOTIFY_CANCELLED,		AllocatePreviewResourceTaskState.CANCELLED},
	};

	@SuppressWarnings("serial")
	private final static HashMap<Map.Entry<IState, IEvent>, IState> stTable = new HashMap<Map.Entry<IState, IEvent>, IState>() {{
		//初期化処理、stTableDefをMapに詰めてるだけ(直接Mapで定義してもよかったが、定義を配列で切り出した方がシンプルで管理しやすいので)
		for (int i = 0; i < stTableDef.length; i++) {
			put(new AbstractMap.SimpleEntry<IState, IEvent>((IState)stTableDef[i][0], (IEvent)stTableDef[i][1]), (IState)stTableDef[i][2]);
		}
	}};
	
	private AllocatePreviewResourceStateMachine() {

	}
	
	public static AllocatePreviewResourceStateMachine getInstance() {
        return instance;
    }
	
	public boolean isAcceptable(IState current, IEvent event) {
		IState next = getNextState(current, event);
		return (next != null) ? true : false;
	}

	public IState getNextState(IState state, IEvent event) {
		//Mapから取り出して返す
		return stTable.get(new AbstractMap.SimpleEntry<IState, IEvent>(state, event));
	}
}
