package com.sony.pro.mwa.repository.query;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 5:48
 * To change this template use File | Settings | File Templates.
 */
public class Column {
    private String name;
    private Class<?> clazz;
    private boolean sortable;

    /**
     * @param name
     * @param clazz
     */
    public Column(String name, Class<?> clazz, boolean sortable) {
        this.name = name;
        this.clazz = clazz;
        this.sortable = sortable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }
}
