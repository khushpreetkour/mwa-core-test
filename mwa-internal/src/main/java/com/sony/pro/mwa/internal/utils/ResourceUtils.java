package com.sony.pro.mwa.internal.utils;

public class ResourceUtils {
	static final String RESOURCE_HOLDER = "resourceHolder";
	static final String RESOURCE_GROUP = "resourceGroup";
	static final String RESOURCE = "resource";

	public static String extractResourceOfHolder(String holderString) {
		String result = null;
		if (isResourceHolderFormat(holderString)) 
			result = holderString.substring(RESOURCE_HOLDER.length()+1, holderString.length()-1);
		return result;
	}

	public static String extractResourceGroup(String resourceGroupString) {
		String result = null;
		if (isResourceFormat(resourceGroupString)) 
			result = resourceGroupString.substring(RESOURCE_GROUP.length()+1, resourceGroupString.length()-1);
		return result;
	}
	public static String extractResource(String resourceString) {
		String result = null;
		if (isResourceFormat(resourceString)) 
			result = resourceString.substring(RESOURCE.length()+1, resourceString.length()-1);
		return result;
	}

	public static String createResourceHolderString(String str) {
		return createFormatString(RESOURCE_HOLDER, str);
	}
	public static String createResourceGroupString(String str) {
		return createFormatString(RESOURCE_GROUP, str);
	}
	public static String createResourceString(String str) {
		return createFormatString(RESOURCE, str);
	}
	private static String createFormatString(String pattern, String resourceId) {
		return pattern + "(" + resourceId + ")";
	}

	public static boolean isResourceHolderFormat(String str) {
		return isPatternFormat(RESOURCE_HOLDER, str);
	}

	public static boolean isResourceGroupFormat(String str) {
		return isPatternFormat(RESOURCE_GROUP, str);
	}
	public static boolean isResourceFormat(String str) {
		return isPatternFormat(RESOURCE, str);
	}

	private static boolean isPatternFormat(String pattern, String str) {
		boolean result = false;
		if (str != null && str.toUpperCase().startsWith(pattern.toUpperCase() + "(")) {
			result = true;
		}
		return result;
	}
}
