package com.sony.pro.mwa.resource.activity;

import com.sony.pro.mwa.service.activity.IState;

public enum ReserveResourceTaskState implements IState {
				//stable	//terminated	//error		//cancel
	CREATED		(true,		false,			false,		false),
	QUEUED		(true,		false,			false,		false),
	COMPLETED	(true,		true,			false,		false),
	ERROR		(true,		true,			true,		false),
	CANCELLED	(true,		true,			false,		true),
	;
	
	private boolean stableFlag;		//If stableFlag is false, this instance becomes target of status polling
	private boolean terminatedFlag;	//MWA treated the instance as terminated status (out of scope of instance life cycle)
	private boolean errorFlag;		//This instance becomes error handling target
	private boolean cancelledFlag;	//This instance becomes cancel handling target
	
	private ReserveResourceTaskState (boolean stableFlag, boolean terminatedFlag, boolean errorFlag, boolean cancelledFlag) {
		this.stableFlag = stableFlag;
		this.terminatedFlag = terminatedFlag;
		this.errorFlag = errorFlag;
		this.cancelledFlag = cancelledFlag;
	}
	
	public String getName() { return super.name(); }
	
	//Default
	public boolean isTerminated() { return this.terminatedFlag; }
	public boolean isStable() { return this.stableFlag; }
	public boolean isError() { return this.errorFlag; }
	public boolean isCancel() { return this.cancelledFlag; }
}
