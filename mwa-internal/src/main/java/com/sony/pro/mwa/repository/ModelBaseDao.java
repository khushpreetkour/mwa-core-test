package com.sony.pro.mwa.repository;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.repository.query.QueryCriteria;

import java.security.Principal;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 14/03/03
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */
public interface ModelBaseDao<T, C extends Collection<T>> {
    public C getModels(QueryCriteria criteria);

    public T getModel(String id, Principal principal);

    public T addModel(T model);

    public T updateModel(T model);

    public T deleteModel(T model);
}
