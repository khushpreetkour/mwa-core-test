package com.sony.pro.mwa.repository.query.criteria;

import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.Sorting;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 9:35
 * To change this template use File | Settings | File Templates.
 */
public class QueryCriteriaGenerator {
    @SuppressWarnings("unchecked")
    public static <T extends QueryCriteria> T getQueryCriteria(Class<? extends QueryCriteria> clazz, List<String> sorts, List<String> filters, Integer offset, Integer limit, Principal principal) {
        QueryCriteria criteria;
        try {
            criteria = clazz.newInstance();
        } catch (java.lang.Exception e) {
            throw new RuntimeException(e);
        }

        if (CollectionUtils.isEmpty(sorts) != true) {
            List<Sorting> sortings = new ArrayList<>();
            for (String sortString : sorts) {
                String sortKey = sortString;
                Boolean isAscending = true;
                if (sortString.endsWith("+") == true) {
                    sortKey = sortString.substring(0, sortString.length() - 1);
                    isAscending = true;
                } else if (sortString.endsWith("-") == true) {
                    sortKey = sortString.substring(0, sortString.length() - 1);
                    isAscending = false;
                }
                sortings.add(new Sorting(sortKey, BooleanUtils.isNotFalse(isAscending)));
            }

            criteria.setSortings(sortings);
        }

        if (CollectionUtils.isEmpty(filters) != true) {

            List<List<Filtering>> filterings = new ArrayList<>();

            for (String filterOrString : filters) {
                List<Filtering> orFiltering = new ArrayList<>();
                for (String filterString : StringUtils.isEmpty(filterOrString) != true ? filterOrString.split("(?<!\\\\),") : new String[0]) {
                    FilterOperatorEnum op = findFilterOperator(filterString);
                    if (op != FilterOperatorEnum.UNKNOWN) {

                        String[] filterKeyValue = filterString.split(op.toSymbol(), 2);
                        String filterKey = filterKeyValue[0].split("\\(")[0];

                        String filterInnerKey = null;
                        Matcher matcher = Pattern.compile("\\((.+)\\)").matcher(filterKeyValue[0]);
                        if (matcher.find() == true) {
                            filterInnerKey = matcher.group(1).replace("\\,", ",");
                        }

                        String filterValue = null;
                        if (filterKeyValue.length > 1) {
                            filterValue = filterKeyValue[1].replace("\\,", ",");
                        }

                        orFiltering.add(new Filtering(filterKey, filterInnerKey, op, filterValue));
                    } else {
                        orFiltering.add(new Filtering(filterString, null, op, null));
                    }
                }

                filterings.add(orFiltering);
            }
            criteria.setFilterings(filterings);
        }

        criteria.setOffset(offset != null ? offset : -1);
        criteria.setLimit(limit != null ? limit : -1);
        criteria.setPrincipal(principal);

        return (T) criteria;
    }

    private static FilterOperatorEnum findFilterOperator(String filterString) {

        if (StringUtils.isEmpty(filterString) != true) {
            char[] chars = filterString.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                if (chars[i] == '=') {
                    if (i + 1 < chars.length) {
                        if (chars[i + 1] == '=') {
                            return FilterOperatorEnum.EQUAL;
                        } else if (chars[i + 1] == '@') {
                            return FilterOperatorEnum.PARTIAL_MATCHING;
                        }
                    }
                    return FilterOperatorEnum.UNKNOWN;
                } else if (chars[i] == '>') {
                    if (i + 1 < chars.length) {
                        if (chars[i + 1] == '=') {
                            return FilterOperatorEnum.GREATER_THAN_OR_EQUAL;
                        }
                    }
                    return FilterOperatorEnum.GREATER_THAN;
                } else if (chars[i] == '<') {
                    if (i + 1 < chars.length) {
                        if (chars[i + 1] == '=') {
                            return FilterOperatorEnum.LESS_THAN_OR_EQUAL;
                        }
                    }
                    return FilterOperatorEnum.LESS_THAN;
                } else if (chars[i] == '!') {
                    if (i + 1 < chars.length) {
                        if (chars[i + 1] == '=') {
                            return FilterOperatorEnum.NOT_EQUAL;
                        } else if (chars[i + 1] == '@') {
                            return FilterOperatorEnum.PARTIAL_UNMATCHING;
                        }
                    }
                    return FilterOperatorEnum.UNKNOWN;
                }
            }
        }
        return FilterOperatorEnum.UNKNOWN;
    }


    public static QueryCriteria getQueryCriteria(List<String> sorts, List<String> filters, Integer offset, Integer limit, Principal principal) {
        return getQueryCriteria(QueryCriteria.class, sorts, filters, offset, limit, principal);
    }

    public static QueryCriteria getQueryCriteria(Principal principal) {
        return getQueryCriteria(QueryCriteria.class, null, null, null, null, principal);
    }

    public static <T extends QueryCriteria> T getQueryCriteriaById(Class<? extends QueryCriteria> clazz, String id, Principal principal) {
        return getQueryCriteria(clazz, null, Arrays.asList("id" + FilterOperatorEnum.EQUAL.toSymbol() + id), null, null, principal);
    }

    public static QueryCriteria getQueryCriteriaById(String id, Principal principal) {
        return getQueryCriteriaById(QueryCriteria.class, id, principal);
    }

    public static <T extends QueryCriteria> T getQueryCriteriaByIds(Class<? extends QueryCriteria> clazz, String ids, Principal principal) {
        String filter = StringUtils.EMPTY;
        for (String id : StringUtils.isEmpty(ids) != true ? ids.split("(?<!\\\\),") : new String[0]) {
            filter += "id" + FilterOperatorEnum.EQUAL.toSymbol() + id + ",";
        }

        if (StringUtils.isEmpty(filter) == true) {
            filter = "id" + FilterOperatorEnum.EQUAL.toSymbol();
        }

        return getQueryCriteria(clazz, null, Arrays.asList(StringUtils.removeEnd(filter, ",")), null, null, principal);
    }

    public static QueryCriteria getQueryCriteriaByIds(String ids, Principal principal) {
        return getQueryCriteriaByIds(QueryCriteria.class, ids, principal);
    }
}
