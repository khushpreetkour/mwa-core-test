package com.sony.pro.mwa.resource.activity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.activity.framework.AbsMwaCallbackActivity;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.model.scheduler.SimpleTriggerInputModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

/**
 *
 */
public class AutoFreeResourceTask extends AbsMwaCallbackActivity {

	private static final String MWA_PROVIDER_NAME = "MWA_LOCAL";

	/*-------------------------------------------------------------------------------------------
	 *  Member変数。必ずset/getInstanceDetailsで上位からアクセス可能であること。メンバ変数の永続化と復元のため。
	 -------------------------------------------------------------------------------------------*/
	protected Map<String, String> persistentData = new HashMap<String, String>();

	private final String releaseTaskName = "com.sony.pro.mwa.resource.activity.ReleaseResourceTask";
	private final String releaseTaskVersion = "1";

	private String resourceName = MWA_PROVIDER_NAME;
	private String resourceId = MWA_PROVIDER_NAME;
	private String requestResourceId = MWA_PROVIDER_NAME;
	private List<String> requestResourceIds = new ArrayList<String>();
	private boolean isRetry = false;
	private int intTimeOutPeriod = 0;
	private String providerId;

	public AutoFreeResourceTask() {
		super(AutoFreeResourceTaskState.CREATED, AutoFreeResourceStateMachine.getInstance());
		logger.debug("Construct processed...");
	}

	public enum INPUTS implements IParameterDefinition {
		ProviderId(TypeString.instance(), false),
		ResourceName(TypeString.instance(), true),
		RequestResourceId(TypeString.instance(), false),
		timeOutPeriod(TypeString.instance(), true), ;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}

		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		Result(TypeBoolean.instance(), true), ;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
	}

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {

		validInput(params);

		if (this.resourceName.contains(" ")) {
			this.resourceName = this.resourceName.replace(" ", "%20");
		}

		// scheduler発火時間の取得
		String fireTime = null;
		{
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			cal.add(Calendar.SECOND, this.intTimeOutPeriod);
			Date date = cal.getTime();
			fireTime = sdf.format(date);
		}

		// scheduler登録
		SimpleTriggerInputModel simpleTriggerInputModel = new SimpleTriggerInputModel();

		String host = null;
		String port = null;
		if (this.providerId != null) {
			host = this.getDefaultEndpoint().getHost();
			port = this.getDefaultEndpoint().getPort().toString();
		} else {

			ActivityProviderCollection col = this.getCoreModules().getActivityProviderManager()
					.getProviders(null, Arrays.asList("name" + FilterOperatorEnum.EQUAL.toSymbol() + MWA_PROVIDER_NAME), null, null);

			if (col.getCount() == 1) {
				ActivityProviderModel model = col.first();

				if (model.getEndpointList().size() == 1) {
					ServiceEndpointModel serviceEndpointModel = model.getEndpointList().get(0);
					host = serviceEndpointModel.getHost();
					port = serviceEndpointModel.getPort().toString();
				} else {
					throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, null, null);
				}
			} else {
				throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, null, null);
			}
		}

		final String endPoint = String.format("http://%s:%s/mwa/api/v2/activity-instances/%s?operation=REQUEST_START", host, port, this.getId());

		simpleTriggerInputModel.setEndpoint(endPoint);

		simpleTriggerInputModel.setHttpMethodType("POST");
		simpleTriggerInputModel.setStartTime(fireTime);

		KeyValueModel[] models = new KeyValueModel[3];
		models[0] = new KeyValueModel("ResourceName", this.resourceName);
		models[1] = new KeyValueModel("RequestResourceId", this.requestResourceId);
		models[2] = new KeyValueModel("timeOutPeriod", Integer.toString(this.intTimeOutPeriod));

		final String jsonParam = this.convetKeyValuesToJson(models);

		simpleTriggerInputModel.setParameter(jsonParam);
		KeyValueModel keyValueModel = this.getCoreModules().getSchedulerManager().registSimpleScheduleJob(simpleTriggerInputModel);

		Map<String, String> resultParam = new HashMap<String, String>();
		resultParam.put("jsonParam", jsonParam);
		resultParam.put("SubmitTriggerSchedId", keyValueModel.getValue());
		resultParam.put("endPoint", endPoint);
		resultParam.put("ResourceName", this.resourceName);
		resultParam.put("RequestResourceId", this.requestResourceId);
		resultParam.put("timeOutPeriod", Integer.toString(this.intTimeOutPeriod));

		// スケジューラの結果をDBにセット.
		this.persistentData = resultParam;

		return OperationResult.newInstance();

	}

	/**
	 * REQUEST_STARTイベント処理をする。
	 * 
	 * @param params
	 * @return
	 */
	public OperationResult requestStartImpl(Map<String, Object> params) {

		validInput(params);

		Map<String, Object> outputMap = new HashMap<String, Object>();

		if (isRegistered()) {
			if (isRequesed()) {
				releaseResource();
			}

			/* if (this.requestResourceIds.size() == 0) { deleteResource(); } */
		} else {
			logger.warn("Not found " + this.resourceName);
		}
		OperationResultWithStatus result = OperationResultWithStatus.newInstance(AutoFreeResourceEvent.NOTIFY_STOP, 100);

		// DBに登録
		outputMap.put(OUTPUTS.Result.getKey(), true);
		this.setOutputDetails(params);
		return result;
	}

	/**
	 * REQUEST_UPDATEイベント処理をする。
	 * 
	 * @param params
	 * @return
	 */
	public OperationResult requestUpdateImpl(Map<String, Object> params) {

		initInput();

		Map<String, Object> outputMap = new HashMap<String, Object>();
		OperationResultWithStatus result = null;

		if (isRegistered()) {
			isRequesed();
		} else {
			logger.warn("Not found " + this.resourceName);
		}

		if (this.isRetry) {

			// 既に登録しているSimpleTriggerの取り消し。
			String scheId = (String) this.persistentData.get("SubmitTriggerSchedId");
			this.getCoreModules().getSchedulerManager().deleteSimpleTrigger(scheId);

			// scheduler発火時間の取得
			String fireTime = null;
			{
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
				cal.add(Calendar.SECOND, this.intTimeOutPeriod);
				Date date = cal.getTime();
				fireTime = sdf.format(date);
			}

			// scheduler登録
			SimpleTriggerInputModel simpleTriggerInputModel = new SimpleTriggerInputModel();

			String endPoint = (String) this.persistentData.get("endPoint");
			String jsonParam = (String) this.persistentData.get("jsonParam");

			simpleTriggerInputModel.setEndpoint(endPoint);
			simpleTriggerInputModel.setHttpMethodType("POST");
			simpleTriggerInputModel.setStartTime(fireTime);
			simpleTriggerInputModel.setParameter(jsonParam);

			KeyValueModel keyValueModel = this.getCoreModules().getSchedulerManager().registSimpleScheduleJob(simpleTriggerInputModel);

			Map<String, String> resultParam = new HashMap<String, String>();
			resultParam.put("jsonParam", jsonParam);
			resultParam.put("SubmitTriggerSchedId", keyValueModel.getValue());
			resultParam.put("endPoint", endPoint);

			resultParam.put("ResourceName", this.resourceName);
			resultParam.put("RequestResourceId", this.requestResourceId);
			String timeOutPeriod = (String) this.persistentData.get("timeOutPeriod");
			resultParam.put("timeOutPeriod", timeOutPeriod);

			// スケジューラの結果をDBにセット.
			this.persistentData = resultParam;

			result = OperationResultWithStatus.newInstance(AutoFreeResourceEvent.NOTIFY_UPDATED, 100);

			outputMap.put(OUTPUTS.Result.getKey(), true);
		} else {

			result = OperationResultWithStatus.newInstance(AutoFreeResourceEvent.NOTIFY_UPDATED, 100);
			outputMap.put(OUTPUTS.Result.getKey(), false);
		}

		result.put(outputMap);
		// DBに登録

		this.setOutputDetails(outputMap);
		return result;
	}

	/**
	 * REQUEST_STOPイベント処理をする。
	 * 
	 * @param params
	 * @return
	 */
	public OperationResult requestStopImpl(Map<String, Object> params) {

		// 既に登録しているSimpleTriggerの取り消し。
		String scheId = (String) this.persistentData.get("SubmitTriggerSchedId");
		this.getCoreModules().getSchedulerManager().deleteSimpleTrigger(scheId);

		initInput();

		Map<String, Object> outputMap = new HashMap<String, Object>();

		if (isRegistered()) {
			if (isRequesed()) {
				releaseResource();
			}

			/* if (this.requestResourceIds.size() == 0) { deleteResource(); } */
			outputMap.put(OUTPUTS.Result.getKey(), true);
		} else {
			logger.warn("Not found " + this.resourceName);
			outputMap.put(OUTPUTS.Result.getKey(), false);
		}

		OperationResultWithStatus result = OperationResultWithStatus.newInstance(AutoFreeResourceEvent.REQUEST_STOP, 100);

		result.put(outputMap);
		// DBに登録
		this.setOutputDetails(outputMap);
		return result;
	}

	private void validInput(Map<String, Object> params) {
		this.resourceName = (String) params.get(INPUTS.ResourceName.name());
		if (this.resourceName == null || this.resourceName.isEmpty()) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, MWA_PROVIDER_NAME, "The resource name can not be emptied!");
		}
		this.requestResourceId = (String) params.get(INPUTS.RequestResourceId.name());

		String timeOutPeriod = (String) params.get(INPUTS.timeOutPeriod.name());
		try {
			this.intTimeOutPeriod = Integer.parseInt(timeOutPeriod);
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, MWA_PROVIDER_NAME, "timeOutPeriod is not Integer value!");
		}

		String providerIdStr = (String) params.get(INPUTS.ProviderId.name());
		if (StringUtils.isEmpty(providerIdStr)) {
			this.providerId = null;
		} else {
			this.providerId = providerIdStr;
		}
	}

	private void initInput() {

		this.resourceName = (String) this.persistentData.get("ResourceName");
		if (this.resourceName == null || this.resourceName.isEmpty()) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, MWA_PROVIDER_NAME, "The resource name can not be emptied!");
		}
		this.requestResourceId = (String) this.persistentData.get("RequestResourceId");

		String timeOutPeriod = (String) this.persistentData.get("timeOutPeriod");
		try {
			this.intTimeOutPeriod = Integer.parseInt(timeOutPeriod);
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, MWA_PROVIDER_NAME, "timeOutPeriod is not Integer value!");
		}

	}

	private boolean isRegistered() {
		boolean result = false;

		ResourceCollection resources = this.getCoreModules().getResourceManager().getResources(null, null, null, null);
		if (resources.getTotalCount() > 0) {
			for (ResourceModel resource : resources.getModels()) {
				if (resource.getName() != null && resource.getName().equals(this.resourceName)) {
					this.resourceId = resource.getId();
					logger.info("Found " + this.resourceName + ":" + this.resourceId + " " + resource.toString());
					result = true;
					break;
				}
			}
		} else {
			this.resourceId = MWA_PROVIDER_NAME;
			result = false;
		}
		return result;
	}

	private boolean isRequesed() {
		boolean result = false;
		this.isRetry = false;
		ResourceRequestCollection resourceRequests = this.getCoreModules().getResourceManager().getResourceRequests(null, null, null, null);

		if (resourceRequests.getTotalCount() == 1) {
			// 自身しかない場合自身を再帰的に呼ぶためスケジュール発火依頼をする。
			this.isRetry = true;
			result = false;
		} else if (resourceRequests.getTotalCount() > 0) {
			for (ResourceRequestModel resourceRequest : resourceRequests.getModels()) {
				for (int i = 0; i < resourceRequest.getResourceRequestEntryList().size(); i++) {
					String resourceId = resourceRequest.getResourceRequestEntryList().get(i).getResourceId();
					if (resourceId != null && resourceId.equals(this.resourceId)) {
						requestResourceIds.add(resourceRequest.getId());
						logger.info("Found " + this.resourceName + " requested. requestId: " + this.requestResourceId);
						result = true;
						break;
					}
				}
			}
		} else {
			result = false;
		}

		return result;
	}

	private void releaseResource() {
		Map<String, Object> inputParams = new HashMap<String, Object>();

		if ((this.requestResourceId != null) && (!this.requestResourceId.isEmpty())) {
			inputParams.put("requestId", this.requestResourceId);
			OperationResult result = this.getCoreModules().getActivityManager().createInstance(releaseTaskName, releaseTaskVersion, inputParams);
			String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
			if (instanceId == null) {
				throw new MwaError(MWARC.SYSTEM_ERROR);
			}
			this.requestResourceIds.remove(this.requestResourceId);
		} else {
			if (this.requestResourceIds.size() > 0) {
				for (String id : this.requestResourceIds) {
					inputParams.put("requestId", id);
					OperationResult result = this.getCoreModules().getActivityManager().createInstance(releaseTaskName, releaseTaskVersion, inputParams);
					String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
					if (instanceId == null) {
						throw new MwaError(MWARC.SYSTEM_ERROR);
					}
				}
				this.requestResourceIds.clear();
			}
		}
	}

	private void deleteResource() {
		ResourceModel resource = null;

		resource = this.getCoreModules().getResourceManager().deleteResource(this.resourceId);
		if (resource == null) {
			logger.warn("Delete " + this.resourceName + ":" + this.resourceId + " failed!");
		} else {
			logger.info("Deleted " + this.resourceName + ":" + this.resourceId + " " + resource.toString());
		}
	}

	@Override
	protected OperationResult processEventImpl(IEvent event, Map<String, Object> params) {
		OperationResult result = null;

		logger.info("Event:" + event.getName());
		// Eventと実装メソッドのマッピング
		if (AutoFreeResourceEvent.REQUEST_START.equals(event)) {
			result = requestStartImpl(params);
		} else if (AutoFreeResourceEvent.REQUEST_STOP.equals(event)) {
			result = requestStopImpl(params);
		} else if (AutoFreeResourceEvent.REQUEST_UPDATE.equals(event)) {
			result = requestUpdateImpl(params);
		}

		return result;
	}

	@Override
	protected List<? extends IEvent> getSupportedEventListImpl() {
		return Arrays.asList(AutoFreeResourceEvent.values());
	}

	@Override
	protected List<? extends IState> getSupportedStateListImpl() {
		return Arrays.asList(AutoFreeResourceTaskState.values());
	}

	@Override
	public String getPersistentDataImpl() {
		// map(activity) --> json(mwa)
		String json = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			json = objectMapper.writeValueAsString(this.persistentData);
		} catch (JsonGenerationException e) {
			logger.error("json generation error getPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (JsonMappingException e) {
			logger.error("json mapping error getPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (IOException e) {
			logger.error("jason mapping IO error getPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setPersistentDataImpl(String details) {
		Map<String, String> kv = null;
		try {
			kv = new ObjectMapper().readValue(details, HashMap.class);
		} catch (JsonParseException e) {
			logger.error("json generation error setPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (JsonMappingException e) {
			logger.error("json mapping error setPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (IOException e) {
			logger.error("jason mapping IO error setPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		}
		this.persistentData = kv;

	}

	/**
	 * KeyValueModel[]のJsonを生成する。
	 * 
	 * @param KeyValueModel
	 *            [] models
	 * @return json
	 */
	public String convetKeyValuesToJson(KeyValueModel[] models) {
		String jsonParam = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			jsonParam = objectMapper.writeValueAsString(models);
		} catch (JsonGenerationException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, MWARC.INTEGRATION_ERROR.name(), "json generation error getPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, MWARC.INTEGRATION_ERROR.name(), "json mapping error getPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, MWARC.INTEGRATION_ERROR.name(), "jason mapping IO error getPersistentDataImpl failed.");
		}
		return jsonParam;
	}
}
