package com.sony.pro.mwa.repository.database;

import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 9:51
 * To change this template use File | Settings | File Templates.
 */
public enum DatabaseEnum {
    UNKNOWN, POSTGRESQL, MYSQL, H2SQL;

    private static DatabaseEnum currentDatabaseEnum = null;

    public static DatabaseEnum getDatabase(DataSource dataSource) {

        if (currentDatabaseEnum != null) {
            return currentDatabaseEnum;
        }

        String urlString = null;
        Connection conn = DataSourceUtils.getConnection(dataSource);
        try {
            urlString = conn.getMetaData().getURL();

            if (urlString.toLowerCase().startsWith("jdbc:postgresql://") == true) {
                currentDatabaseEnum = DatabaseEnum.POSTGRESQL;
            } else if (urlString.toLowerCase().startsWith("jdbc:mysql://") == true) {
                currentDatabaseEnum = DatabaseEnum.MYSQL;
            } else if (urlString.toLowerCase().startsWith("jdbc:h2") == true) {
                currentDatabaseEnum = DatabaseEnum.H2SQL;
            } else {
                currentDatabaseEnum = DatabaseEnum.UNKNOWN;
            }
        } catch (java.lang.Exception e) {
            currentDatabaseEnum = DatabaseEnum.UNKNOWN;
        } finally {
            DataSourceUtils.releaseConnection(conn, dataSource);
        }

        return currentDatabaseEnum;
    }
}
