package com.sony.pro.mwa.resource.activity;

import com.sony.pro.mwa.service.activity.IState;

public enum AutoFreeResourceTaskState implements IState {
	//name		//stable	//terminated	//error		//cancel
	READY	    (false,		false,			false,		false),
	CREATED		(true,		false,			false,		false),
	IN_PROGRESS (true,		false,			false,		false),
	COMPLETED	(true,		true,			false,		false),
	ERROR		(true,		true,			true,		false),
	MANUAL_COMPLETED	(true,		true,			false,		false),
	;
	
	private boolean stableFlag;
	private boolean terminatedFlag;
	private boolean errorFlag;
	private boolean cancelFlag;

	private AutoFreeResourceTaskState(boolean stableFlag, boolean terminatedFlag, boolean errorFlag, boolean cancelFlag) {
		this.stableFlag = stableFlag;
		this.terminatedFlag = terminatedFlag;
		this.errorFlag = errorFlag;
		this.cancelFlag = cancelFlag;
	}
	
	// Default
	public boolean isTerminated() {
		return this.terminatedFlag;
	}

	public boolean isStable() {
		return this.stableFlag;
	}

	public boolean isError() {
		return this.errorFlag;
	}

	public boolean isCancel() {
		return this.cancelFlag;
	}

	public String getName() {
		return super.name();
	}

}
