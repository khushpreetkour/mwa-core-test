package com.sony.pro.mwa.control.activity;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.internal.EndTaskBase;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;

public class EndTask extends EndTaskBase {
	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		OperationResult result = OperationResult.newInstance();
		for (Map.Entry<String, Object> param : params.entrySet()) {
			result.put(param.getKey(), param.getValue());
		}
		return result;
	}
	
	public enum INPUTS implements IParameterDefinition {
		;
		
		IParameterType type;
		boolean required;
		String key;
		
		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}
		
		@Override
		public boolean getRequired() {
			return required;
		}
		@Override
		public String getKey() {
			return this.name();
		}
		@Override
		public IParameterType getType() {
			return type;
		}
		@Override
		public String getTypeName() {
			return this.getType().name();
		}
	}
	
	public enum OUTPUTS implements IParameterDefinition {
		;
		
		IParameterType type;
		boolean required;
		
		@Override
		public boolean getRequired() {
			return required;
		}
		@Override
		public String getKey() {
			return this.name();
		}
		@Override
		public IParameterType getType() {
			return type;
		}
		@Override
		public String getTypeName() {
			return this.getType().name();
		}
	}
}
