package com.sony.pro.mwa.control.activity;

import com.sony.pro.mwa.activity.framework.internal.StartTaskBase;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.type.TypeString;

import java.util.Map;

public class StartTaskWithBin extends StartTaskBase {
    public enum INPUTS implements IParameterDefinition {
        binId(TypeString.instance(), false, false);

        String key;
        IParameterType type;
        boolean required;
        boolean visibleFlag;

        private INPUTS(IParameterType type, boolean required, boolean visibleFlag) {
            this.key = this.name();
            this.type = type;
            this.required = required;
            this.visibleFlag = visibleFlag;
        }

        @Override
        public boolean getRequired() {
            return required;
        }
        @Override
        public String getKey() {
            return this.name();
        }
        @Override
        public IParameterType getType() {
            return type;
        }
        @Override
        public String getTypeName() {
            return this.getType().name();
        }
        public boolean getVisibleFlag() { return visibleFlag; }
    }

    public enum OUTPUTS implements IParameterDefinition {
        binId(TypeString.instance(), false);

        String key;
        IParameterType type;
        boolean required;

        private OUTPUTS(IParameterType type, boolean required) {
            this.key = this.name();
            this.type = type;
            this.required = required;
        }

        @Override
        public boolean getRequired() {
            return required;
        }
        @Override
        public String getKey() {
            return this.name();
        }
        @Override
        public IParameterType getType() {
            return type;
        }
        @Override
        public String getTypeName() {
            return this.getType().name();
        }
    }

    @Override
    protected OperationResult requestSubmitImpl(Map<String, Object> params) {
        OperationResult result = OperationResult.newInstance();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            result.put(param.getKey(), param.getValue());
        }
        return result;
    }
}
