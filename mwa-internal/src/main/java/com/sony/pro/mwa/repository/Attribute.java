package com.sony.pro.mwa.repository;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/07
 * Time: 3:09
 * To change this template use File | Settings | File Templates.
 */
public class Attribute
{
    private String attribute;
    private String type;

    public Attribute(String attribute, String type)
    {
        this.attribute = attribute;
        this.type = type;
    }

    public String getAttribute() {
        return attribute;
    }

    public String getType() {
        return type;
    }
}
