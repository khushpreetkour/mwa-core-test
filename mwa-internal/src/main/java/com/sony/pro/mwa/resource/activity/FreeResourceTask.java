package com.sony.pro.mwa.resource.activity;

import java.util.*;
import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.ParameterUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author xi.ou
 *
 */
public class FreeResourceTask extends SyncTaskBase {
// TODO : TemplateVersion が"1" で固定されています
private final String releaseTaskName = "com.sony.pro.mwa.resource.activity.ReleaseResourceTask";
	private final String releaseTaskVersion = "1";

	// TODO : メンバ変数を使用しないようにリファクタリングが必要
	private String resourceName = "";
	private String resourceId = "";
	private String requestResourceId = "";
	private Boolean resourceDeletionFlag = null;
	private List<String> requestResourceIds = new ArrayList<>();

	public enum INPUTS implements IParameterDefinition {
		ResourceName(TypeString.instance(), true),
		RequestResourceId(TypeString.instance(), false), 
		ResourceDeletionFlag(TypeBoolean.instance(), false),
		;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}

		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		Result(TypeBoolean.instance(), true),
		ResourceId(TypeString.instance(), true), ;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
	}

	protected Map<String, Object> validate(List<? extends IParameterDefinition> paramDefs, Map<String, Object> params) {
		return ParameterUtils.FROM_STRING.convert(paramDefs, params);
	}

	/***
	 * Jobを実行する
	 * @param input 入力パラメータ
	 * @return 出力パラメータ
	 */
	@Override
	public OperationResult requestSubmitImpl(Map<String, Object> input) {
		OperationResult result = OperationResultWithStatus.newInstance();
		Map<String, Object> inputMap = this.validate(Arrays.asList(INPUTS.values()), input);

		this.resourceName = (String) inputMap.get(INPUTS.ResourceName.name());
		if (resourceName.contains(" ")) {
			resourceName = resourceName.replace(" ", "%20");
		}
		this.requestResourceId = (String) inputMap.get(INPUTS.RequestResourceId.name());
		this.resourceDeletionFlag = (Boolean) inputMap.get(INPUTS.ResourceDeletionFlag.name());

		if (isRegistered()) {
			/*
			if (isRequesed()) {
				releaseResource();
			} else {
				logger.info("Not found ResourceRequestEntry.");
			}
			*/
			releaseResource(this.resourceDeletionFlag);
		} else {
			logger.warn("Not found " + this.resourceName);
		}
		result.put(OUTPUTS.ResourceId.getKey(), this.resourceId);
		return result;
	}

	private boolean isRegistered() {
		boolean result = false;

		ResourceCollection resources = this.getCoreModules().getResourceManager().getResources(null, null, null, null);
		if (resources.getTotalCount() > 0) {
			for (ResourceModel resource : resources.getModels()) {
				if (resource.getName() != null && resource.getName().equals(this.resourceName)) {
					this.resourceId = resource.getId();
					logger.info("Found " + this.resourceName + ":" + this.resourceId + " " + resource.toString());
					result = true;
					break;
				}
			}
		} else {
			this.resourceId = StringUtils.EMPTY;
			result = false;
		}
		return result;
	}

	private boolean isRequesed() {
		boolean result = false;
		List<String> filters = new ArrayList<>();
		filters.add("occupantName" + FilterOperatorEnum.EQUAL.toSymbol() +  this.resourceName);

		ResourceRequestCollection resourceRequests = this.getCoreModules().getResourceManager().getResourceRequests(null, filters, null, null);
		if (resourceRequests.getTotalCount() > 0) {
			for (ResourceRequestModel resourceRequest : resourceRequests.getModels()) {
				for (int i = 0; i < resourceRequest.getResourceRequestEntryList().size(); i++) {
					String resourceId = resourceRequest.getResourceRequestEntryList().get(i).getResourceId();
					if (resourceId != null && resourceId.equals(this.resourceId)) {
						requestResourceIds.add(resourceRequest.getId());
						logger.info("Found " + this.resourceName + " requested. requestId: " + this.requestResourceId);
						result = true;
						break;
					}
				}
			}
		} else {
			result = false;
		}
		return result;
	}

	private void releaseResource(Boolean resourceDeletionFlag) {
		if (resourceDeletionFlag == null) {
			resourceDeletionFlag = false;
		}
		Map<String, Object> inputParams = new HashMap<String, Object>();

		if ((this.requestResourceId != null) && (!this.requestResourceId.isEmpty())) {
			inputParams.put("requestId", this.requestResourceId);
			inputParams.put("resourceDeletionFlag", resourceDeletionFlag.toString());

			OperationResult result = this.getCoreModules().getActivityManager().createInstance(releaseTaskName, releaseTaskVersion, inputParams);
			String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
			if (instanceId == null) {
				throw new MwaError(MWARC.SYSTEM_ERROR);
			}
			this.requestResourceIds.remove(this.requestResourceId);
		} else {
			if (this.requestResourceIds.size() > 0) {
				for (String id : this.requestResourceIds) {
					inputParams.put("requestId", id);
					inputParams.put("resourceDeletionFlag", resourceDeletionFlag.toString());
					OperationResult result = this.getCoreModules().getActivityManager().createInstance(releaseTaskName, releaseTaskVersion, inputParams);
					String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
					if (instanceId == null) {
						throw new MwaError(MWARC.SYSTEM_ERROR);
					}
				}
				this.requestResourceIds.clear();
			}
		}
	}
}
