package com.sony.pro.mwa.repository.database.query;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 6:00
 * To change this template use File | Settings | File Templates.
 */
public enum SqlOperatorEnum {
    EQUAL("="),
    NOT_EQUAL("!="),
    GREATER_THAN(">"),
    LESS_THAN("<"),
    GREATER_THAN_OR_EQUAL(">="),
    LESS_THAN_OR_EQUAL("<="),
    IN("IN"),
    NOT_IN("NOT IN"),
    PARTIAL_MATCHING("LIKE"),
    PARTIAL_UNMATCHING("NOT LIKE"),
    IS_NULL("IS NULL"),
    IS_NOT_NULL("IS NOT NULL");

    private String symbol;

    private SqlOperatorEnum(String symbol) {
        this.symbol = symbol;
    }

    public String toSymbol() {
        return symbol;
    }
}
