package com.sony.pro.mwa.resource.activity;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.rc.MWARC;

public class ReleaseResourceTask extends SyncTaskBase {

	Integer progress = 0;

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		String requestId = (String)params.get(INPUTS.requestId.getKey());
        String resourceDeletionFlagStr = (String) params.get(INPUTS.resourceDeletionFlag.getKey());
        Boolean resourceDeletionFlag = (resourceDeletionFlagStr != null) ? Boolean.valueOf(resourceDeletionFlagStr) : null;
		ResourceRequestModel model = this.getCoreModules().getResourceManager().deleteResourceRequest(requestId, resourceDeletionFlag);

		if (model == null)
			return OperationResult.newInstance(MWARC.OPERATION_FAILED_RESOURCE_REQUEST_NOT_FOUND, "requestId=" + requestId);
		return OperationResult.newInstance();
	}

	public enum INPUTS implements IParameterDefinition {
		requestId(TypeString.instance(), false),
        resourceDeletionFlag(TypeBoolean.instance(), false),
		;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}
		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() { return required; }
		@Override
		public String getKey() { return this.name(); }
		@Override
		public IParameterType getType() { return type; }
		@Override
		public String getTypeName() {return this.getType().name();}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() { return required; }
		@Override
		public String getKey() { return this.name(); }
		@Override
		public IParameterType getType() { return type; }
		@Override
		public String getTypeName() {return this.getType().name();}

		IParameterType type;
		boolean required;
	}
}
