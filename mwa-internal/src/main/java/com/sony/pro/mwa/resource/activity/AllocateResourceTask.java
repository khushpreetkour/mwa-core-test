package com.sony.pro.mwa.resource.activity;

import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.standard.PollingTaskBase;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskEvent;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.resource.CostModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeCost;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;

/**
 * @author xi.ou
 *
 */
public class AllocateResourceTask extends PollingTaskBase {
	protected Map<String, Object> persistentData = new HashMap<String, Object>();
	Map<String, Object> outputMap = new HashMap<String, Object>();

	// TODO:staticは？
	private final String reserveTaskName = "com.sony.pro.mwa.resource.activity.ReserveResourceTask";
	private final String reserveTaskVersion = "1";

	private final String freeResouceTaskName = "com.sony.pro.mwa.resource.activity.ReleaseResourceTask";
	private final String freeResouceTaskVersion = "1";

	// TODO: MessageHolderへ。少なくともどこかで共通で持ちたいです。AllocateDriveResourceTaskにも同様の定義をしました。
	private static final String MSG_INTERNAL_ERROR = "Internal error was occured. Please contact the system administrator.";

	private String resourceName = "";
	private String resourceId = "";
	private CostModel resourceCost;

	public enum INPUTS implements IParameterDefinition {
		ResourceName(TypeString.instance(), true),
		ResourceCost(TypeString.instance(), true),;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}

		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		Result(TypeBoolean.instance(), true),
		ResourceId(TypeString.instance(), true),
		ResourceRequestId(TypeString.instance(), true),;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
	}

	@Override
	public OperationResult requestStopImpl(Map<String, Object> params) {
		logger.info("requestStopImpl: " + params.toString());
		return null;
	}

	@Override
	public OperationResult requestPauseImpl(Map<String, Object> params) {
		logger.info("requestPauseImpl: " + params.toString());
		return null;
	}

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		validInput(params);

		if (this.resourceName.contains(" ")) {
			this.resourceName = this.resourceName.replace(" ", "%20");
		}

		if (!isRegistered()) {
			allocateResource();
		}
		reserveResource();

		return OperationResult.newInstance();
	}

	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		ActivityInstanceModel model = this.getCoreModules().getActivityManager().getChildInstances(this.getId()).first();
		Map<String, Object> reserveResult = model.getOutputDetails();

		if (this.getStatus().isCancel()) {
			// もし子Activity(ReserveResourceTask)が終了していたら。
			if (model != null && model.getStatus().isTerminated()) {
				// Resourceの解放を行う。
				this.releaseResource();
				outputMap.put(OUTPUTS.ResourceRequestId.getKey(), reserveResult.get(ReserveResourceTask.OUTPUTS.requestId.getKey()));
				this.setOutputDetails(outputMap);
				// ActivityをCancelledにする。
				return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_CANCELLED, 100);
			}
			// ReserveResourceTaskはキャンセルを許容しないため、Terminatedになるまで待つ。
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_UPDATED, 0);
		}

		if (model.getStatus().isTerminated()) {
			outputMap = this.getOutputDetails();
			outputMap.put(OUTPUTS.ResourceRequestId.getKey(), reserveResult.get(ReserveResourceTask.OUTPUTS.requestId.getKey()));
			this.setOutputDetails(outputMap);
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_COMPLETED, 100);
		} else {
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_UPDATED, 0);
		}
	}

	@Override
	public String getPersistentDataImpl() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	@Override
	public void setPersistentDataImpl(String details) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public OperationResult requestCancelImpl(Map<String, Object> params) {
		logger.info("requestCancelImpl: " + params.toString());
		OperationResult result = OperationResult.newInstance();
		return result;
	}

	private void validInput(Map<String, Object> params) {
		this.resourceName = (String) params.get(INPUTS.ResourceName.name());
		if (this.resourceName == null || this.resourceName.isEmpty()) {
			// ODA-FMでの処理でしか使用されなくなったため、INVALID_INPUTをスローする
			throw new MwaInstanceError(MWARC.INVALID_INPUT, "", "Input parameter 'ResourceName' does not allow empty characters.");
		}

		String cost = (String) params.get(INPUTS.ResourceCost.name());
		if (cost == null || cost.isEmpty()) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, "", "Input parameter 'ResourceCost' does not allow empty characters.");
		} else {
			this.resourceCost = (CostModel) TypeCost.instance().fromString(cost);
			if (this.resourceCost != null) {
				logger.info("Requested resource cost: " + this.resourceCost.toString());
			}
		}
	}

	private boolean isRegistered() {
		boolean result = false;

		ResourceCollection resources = this.getCoreModules().getResourceManager().getResources(null, null, null, null);
		if (resources.getTotalCount() > 0) {
			for (ResourceModel resource : resources.getModels()) {
				if (resource.getName().equals(this.resourceName)) {
					result = true;
					this.resourceId = resource.getId();
					logger.info("Found " + this.resourceName + ":" + this.resourceId + " " + resource.toString());
					outputMap = this.getOutputDetails();
					outputMap.put(OUTPUTS.ResourceId.getKey(), this.resourceId);
					this.setOutputDetails(outputMap);
					break;
				}
			}
		} else {
			result = false;
			this.resourceId = "";
		}
		return result;
	}

	private void allocateResource() {
		ResourceModel model = new ResourceModel();
		ResourceModel result = null;

		model.setName(this.resourceName);
		model.setType(this.resourceCost.getType());
		model.setValueList(this.resourceCost.getValueList());

		result = this.getCoreModules().getResourceManager().addResource(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		} else {
			this.resourceId = result.getId();
			logger.info("Created " + this.resourceName + ":" + this.resourceId + " " + result.toString());
			outputMap = this.getOutputDetails();
			outputMap.put(OUTPUTS.ResourceId.getKey(), this.resourceId);
			this.setOutputDetails(outputMap);
		}
	}

	private void reserveResource() {
		Map<String, Object> inputParams = new HashMap<String, Object>();

		if (this.getParentId() != null) {
			inputParams.put("occupantId", this.getParentId());
		} else {
			inputParams.put("occupantId", this.getId());
		}

		inputParams.put("occupantName", this.resourceName);
		inputParams.put("receiverId", this.getId());
		inputParams.put("requestResourceId", this.resourceId);
		inputParams.put("cost", com.sony.pro.mwa.common.utils.Converter.toXmlString(this.resourceCost));
		OperationResult result = this.getCoreModules().getActivityManager().createInstance(reserveTaskName, reserveTaskVersion, inputParams);
		String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
		if (instanceId == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR, null, MSG_INTERNAL_ERROR);
		} else {
			persistentData.put(OUTPUTS.ResourceRequestId.name(), instanceId);
		}
	}

	private void releaseResource() {
		Map<String, Object> inputParams = new HashMap<String, Object>();
		Map<String, Object> outputMap = this.getOutputDetails();
		String resourceRequestId = (String) outputMap.get(OUTPUTS.ResourceRequestId.getKey());
		inputParams.put("requestId", resourceRequestId);
		try {
			OperationResult result = this.getCoreModules().getActivityManager().createInstance(freeResouceTaskName, freeResouceTaskVersion, inputParams);
			String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
			if (instanceId == null) {
				throw new MwaError(MWARC.SYSTEM_ERROR, null, MSG_INTERNAL_ERROR);
			}
		} catch (Exception e) {
			logger.warn("Failed to release resource.");
		}
	}

}
