package com.sony.pro.mwa.resource.activity;

import com.sony.pro.mwa.activity.framework.stm.EventType;
import com.sony.pro.mwa.service.activity.IEvent;


/**
 *  WatchDirectoryTask用のEvent定義
 *  
 *  ここで、WatchDirectoryTaskのAdapterが取り扱うEventを列挙する
 *  その際、各EventのTypeを指定すること
 *  
 *  EventType
 *    - Request : 主にユーザからの操作要求。Deviceなどにリクエストを行い、成功した場合に状態遷移が生じるEventType
 *    - Decided : 主にDeviceからの
 *  
 *  なお、標準の状態遷移を利用したい場合、StateMachine、State、Event、は "com.sony.pro.mwa.activity.standard.task" を利用可能
 *  その場合、これら独自定義は必要ない
 */
public enum AllocatePreviewResourceEvent implements IEvent {
	REQUEST_UPDATE		(EventType.REQUEST),
	REQUEST_STOP		(EventType.REQUEST),
	;
	
	EventType type;
	
	/**
	 * コンストラクタ
	 * 引数でEventの種別を定義
	 * 
	 * @param type	Event種別を定義
	 */	
	private AllocatePreviewResourceEvent(EventType type) { this.type = type; }

	/**
	 * EventのEnum名と引数文字列の一致を確認する
	 * 例．RequestCancel.isMatch("RequestCancel") の場合、true
	 * 
	 * @param eventName
	 * @return 一致していればtrue
	 */	
	public boolean isMatch(String eventName) { return this.toString().equals(eventName); }

	/**
	 * RequestEventのチェック。Eventの登録時に指定したTypeの種別確認用。
	 * 例．RequestCancel.isRequestEvent() の場合、true
	 * 
	 * @param eventName
	 * @return EventTypeがRequestであればtrue
	 */	
	public boolean isRequestEvent() { return this.type.isRequest(); }
	
	/**
	 * DecidedEventのチェック。Eventの登録時に指定したTypeの種別確認用。
	 * 例．NotifyError.isDecidedEvent() の場合、true
	 * 
	 * @param eventName
	 * @return EventTypeがDecidedであればtrue
	 */	
	public boolean isNotifyEvent() { return this.type.isNotify(); }
	
	/**
	 * UndecidedEventのチェック。Eventの登録時に指定したTypeの種別確認用。
	 * 例．標準EventではUndecidedEventは
	 * 
	 * @param eventName
	 * @return EventTypeがUndecidedであればtrue
	 */	
	public boolean isConfirmEvent() { return this.type.isConfirm(); }

	public String getName() {
		return super.name();
	}
}
