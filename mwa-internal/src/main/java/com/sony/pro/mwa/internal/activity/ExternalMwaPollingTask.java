package com.sony.pro.mwa.internal.activity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.framework.AbsMwaPollingActivity;
import com.sony.pro.mwa.activity.framework.internal.GeneralEventImpl;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskBase;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.activity.framework.stm.EventType;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.utils.rest.HttpMethodType;
import com.sony.pro.mwa.utils.rest.IRestInput;
import com.sony.pro.mwa.utils.rest.RestClient;
import com.sony.pro.mwa.utils.rest.RestInputImpl;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.utils.RestUtils;
import com.sony.pro.mwa.model.KeyValueCollection;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.StateModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceCollection;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;
import com.sony.pro.mwa.utils.Converter;

public class ExternalMwaPollingTask extends AbsMwaPollingActivity {

	public ExternalMwaPollingTask() {
		super(CommonState.QUEUED, new GeneralStateMachine());
	}

	String instanceId;
	protected RestClient restClient = new RestClient("");

	public String getExternalUrl() {
		return MwaStringUtils.createUrl("http", "v2", "activity-instances", this.getProvider()) + instanceId;
	}
	public String getExternalUrl(String operation) {
		return this.getExternalUrl() + "?operation=" + operation;
	}

	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> arg0) {
		ActivityInstanceModel model = this.getExternalInstance();
		if ( model.getStatus().isTerminated() ) {
			if ( model.getStatus().isError() )
				throw new MwaInstanceError(model.getStatusDetails().getResult(), model.getStatusDetails().getDeviceResponse(), model.getStatusDetails().getDeviceResponseDetails());
		}
		StateModel state = new StateModel(model.getStatus());
		//ポーリングをし続ける必要があるため
		state.setStableFlag(false);
		return OperationResultWithStatus.newInstance(new GeneralEventImpl(model.getStatus().getName(), EventType.NOTIFY).setNextState(state), model.getProgress());
	}

	@Override
	public String getPersistentDataImpl() {
		// TODO Auto-generated method stub
		return this.instanceId;
	}

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		logger.debug("requestSubmitImpl called.");

		// 外に投げる前にProxyに投げ続けるのを避けるためにKeyを削除
		params.remove(PresetParameter.ProxyId.getKey());
		params.put(PresetParameter.ActivityInstanceId.getKey(), this.getId());

		String templateName = (String) params.get(PresetParameter.ExternalActivityTemplateName.getKey());
		String templateVersion = (String) params.get(PresetParameter.ExternalActivityTemplateVersion.getKey());
		String templateId = (String) params.get(PresetParameter.ExternalActivityTemplateId.getKey());
		this.getInputDetails().put(PresetParameter.ExternalActivityTemplateName.getKey(), templateName);
		this.getInputDetails().put(PresetParameter.ExternalActivityTemplateVersion.getKey(), templateVersion);
		this.getInputDetails().put(PresetParameter.ExternalActivityTemplateId.getKey(), templateId);

		String resource = "activities";

		if (templateId != null) {
			resource += "/" + templateId;
		} else if (templateName != null && templateVersion != null) {
			resource += "/" + templateName + "/" + templateVersion;
		} else {
			logger.warn("template name+version or template id are not specified: id=" + templateId + ", name=" + templateName + ", version=" + templateVersion);
			throw new MwaInstanceError(MWARC.INVALID_INPUT_TEMPLATE_NAME);
		}

		String json = RestUtils.convertToJson(Converter.mapToKeyValueList(params));
		String url = MwaStringUtils.createUrl("http", "v2", resource, this.getProvider());
		
		IRestInput restInput = new RestInputImpl(HttpMethodType.POST.name(), url, json);
		this.restClient.rest(restInput);
		OperationResult result = OperationResult.newInstance();
		List<LinkedHashMap> resultList = (List<LinkedHashMap>) RestUtils.convertToObj(restClient.getResponseBody(), ArrayList.class);
		for (LinkedHashMap entry : resultList) {
			result.put((String)entry.get("key"), entry.get("value"));
		}
		if (result != null) {
			this.instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
		}
		return result;
	}

	@Override
	public void setPersistentDataImpl(String arg0) {
		this.instanceId = arg0;
	}

	protected ActivityInstanceModel getExternalInstance() {
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), this.getExternalUrl(), "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ActivityInstanceModel model = (ActivityInstanceModel) RestUtils.convertToObj(restClient.getResponseBody(), ActivityInstanceModel.class);
		return model;
	}
	
	protected OperationResult operateExternalInstance(String operation, Map<String, Object> params) {
		// LocationModelをJson形式に変換
		String json = RestUtils.convertToJson(params);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.POST.name(), this.getExternalUrl(operation), json);
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		OperationResult result = (OperationResult) RestUtils.convertToObj(restClient.getResponseBody(), OperationResult.class);

		return result;
	}

/*	private OperationResult requestImpl(String URL, HttpMethodType httpMethodType, Map<String, Object> params) {
		// 変数宣言
		OperationResult result = null;
		RestClient client = null;
		// GET用URL生成（CreateUrl）
		// restInput.urlに上記urlをセット
		IRestInput restInput = new RestInputImpl(httpMethodType.name(), URL, MwaStringUtils.convetMapToJson(params));

		try {
			// RestClientをnewする（引数：this.getId()）
			client = new RestClient(this.getId());
			// 上記インスタンスのrestメソッドを呼び出す
			// RestClient.rest(httpMethodType.name() ,restNetWorkPathInput)
			client.rest(restInput);

			// result.putでレスポンスのxmlをつめる（restClient.XmlReslut）
			result = OperationResult.newInstance();
			result.put("networkPaths", client.getResponseBody());
		} catch (MwaInstanceError e) {
			logger.error(e.getDescription());
			throw e;

		} finally {

			if (client != null) {
				client = null;
			}

		}
		// setOutputDetailsを呼び出す
		this.setOutputDetails(result);
		return result;
	}
*/
	public enum INPUTS implements IParameterDefinition {
		;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}

		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return key;
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		String key;
		IParameterType type;
		boolean required;
	}

	public enum OUTPUTS implements IParameterDefinition {
		;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
	}

	@Override
	protected OperationResult processEventImpl(IEvent event, Map<String, Object> params) {
		if (CommonEvent.REQUEST_SUBMIT.equals(event))
			return this.requestSubmitImpl(params);
		return this.operateExternalInstance(event.getName(), params);
	}
	@Override
	protected List<? extends IEvent> getSupportedEventListImpl() {
		return null;
	}
	@Override
	protected List<? extends IState> getSupportedStateListImpl() {
		return null;
	}
	
	@Override
	protected IEvent getCorrespondingEvent(String eventName) {
		for (CommonEvent event : CommonEvent.values()) {
			if (event.isMatch(eventName) ) {
				return event;
			}
		}
		return new GeneralEventImpl(eventName, EventType.REQUEST);
	}

	@Override
	public void setModel(ActivityInstanceModel model) {
		this.model = model;
		/*
		//Modelからは完全なStateが復元できないため、ここで行う（Templateから復元できればねぇ。。。）
		this.model.setStatus(this.getRealStatus(model.getStatus().getName()));
		*/
		if (model.getPersistentData() != null) 
			this.setPersistentData(model.getPersistentData());
	}

}
