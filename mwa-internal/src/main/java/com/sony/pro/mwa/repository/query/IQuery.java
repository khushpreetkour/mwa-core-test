package com.sony.pro.mwa.repository.query;

import com.sony.pro.mwa.model.Collection;
import com.sony.pro.mwa.repository.query.QueryCriteria;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/12/05
 * Time: 2:05
 * To change this template use File | Settings | File Templates.
 */
public interface IQuery<T, C extends Collection<T>, Q extends QueryCriteria> {
    public C collection(Q criteria);
    public T first(Q criteria);
    public long filteringCount(Q criteria);
    public long totalCount(Q criteria);
}
