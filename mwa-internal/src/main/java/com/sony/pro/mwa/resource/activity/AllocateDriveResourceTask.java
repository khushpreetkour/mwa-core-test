package com.sony.pro.mwa.resource.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskBase;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskEvent;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.resource.CostModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeCost;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;

/**
 * @author xi.ou
 *
 */
public class AllocateDriveResourceTask extends PollingTaskBase {
	private static final String DRIVE_COST_FTORMAT = "<costModel><type>DRIVE</type><valueList><key>COUNT</key><value>%S</value></valueList></costModel>";
	private static final String RESOURCE_NAME_ODS_DRIVE = "ODS-DRIVE";
	private static final String NOT_MOUNT_CARTRIDGE = "1";
	private static final String MOUNT_CARTRIDGE = "0";

	protected Map<String, Object> persistentData = new HashMap<String, Object>();
	Map<String, Object> outputMap = new HashMap<String, Object>();

	private final String reserveTaskName = "com.sony.pro.mwa.resource.activity.ReserveResourceTask";
	private final String reserveTaskVersion = "1";
	private final String freeResouceTaskName = "com.sony.pro.mwa.resource.activity.ReleaseResourceTask";
	private final String freeResouceTaskVersion = "1";

	// TODO: MessageHolderへ。少なくともどこかで共通で持ちたいです。
	private static final String MSG_INTERNAL_ERROR = "Internal error was occured. Please contact the system administrator.";

	private String resourceName = "";
	private String resourceId;
	private String driveResourceName = "";
	private String driveResourceId;
	private String isNotMountCartridgeToNo;
	private CostModel resourceCost;
	private CostModel driveResourceCost;
	private boolean skipAllocate = false;

	public enum INPUTS implements IParameterDefinition {
		ResourceName(TypeString.instance(), true),
		ResourceCost(TypeString.instance(), true),
		DriveResourceName(TypeString.instance(), false),
		DriveResourceCost(TypeString.instance(), true),
		isNotMountCartridge(TypeString.instance(), true),;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}

		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		Result(TypeBoolean.instance(), true),
		ResourceId(TypeString.instance(), true),
		ResourceRequestId(TypeString.instance(), true),;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
	}

	@Override
	public OperationResult requestStopImpl(Map<String, Object> params) {
		logger.info("requestStopImpl: " + params.toString());
		return null;
	}

	@Override
	public OperationResult requestPauseImpl(Map<String, Object> params) {
		logger.info("requestPauseImpl: " + params.toString());
		return null;
	}

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		validInput(params);

		if (this.resourceName.contains(" ")) {
			this.resourceName = this.resourceName.replace(" ", "%20");
		}

		if (skipAllocate) {
			outputMap.clear();
			outputMap.put(OUTPUTS.ResourceId.getKey(), "");
			outputMap.put(OUTPUTS.ResourceRequestId.getKey(), "");
			this.setOutputDetails(outputMap);

			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_COMPLETED, 100);
		} else {
			if (!isCartridgeResourceRegistered()) {
				allocateCartridgeResource();
			}

			// DriveリソースカウントのUPDATE
			updateDriveResourceCount();

			reserveResource();

			return OperationResult.newInstance();
		}
	}

	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		ActivityInstanceModel model = this.getCoreModules().getActivityManager().getChildInstances(this.getId()).first();
		Map<String, Object> reserveResult = model.getOutputDetails();

		if (this.getStatus().isCancel()) {
			// もし子Activity(ReserveResourceTask)が終了していたら。
			if (model != null && model.getStatus().isTerminated()) {
				// Resourceの解放を行う。
				this.releaseResource();
				outputMap.put(OUTPUTS.ResourceRequestId.getKey(), reserveResult.get(ReserveResourceTask.OUTPUTS.requestId.getKey()));
				this.setOutputDetails(outputMap);
				// ActivityをCancelledにする。
				return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_CANCELLED, 100);
			}
			// ReserveResourceTaskはキャンセルを許容しないため、Terminatedになるまで待つ。
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_UPDATED, 0);
		}

		if (model.getStatus().isTerminated()) {

			outputMap = this.getOutputDetails();
			outputMap.put(OUTPUTS.ResourceRequestId.getKey(), reserveResult.get(ReserveResourceTask.OUTPUTS.requestId.getKey()));
			this.setOutputDetails(outputMap);
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_COMPLETED, 100);
		} else {
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_UPDATED, 0);
		}
	}

	@Override
	public String getPersistentDataImpl() {
		String json = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			json = objectMapper.writeValueAsString(this.persistentData);
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, MSG_INTERNAL_ERROR);
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, MSG_INTERNAL_ERROR);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, MSG_INTERNAL_ERROR);
		}
		return json;
	}

	@Override
	public void setPersistentDataImpl(String details) {
		Map<String, Object> kv = null;
		try {
			kv = new ObjectMapper().readValue(details, HashMap.class);
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, MSG_INTERNAL_ERROR);
		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, MSG_INTERNAL_ERROR);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, null, MSG_INTERNAL_ERROR);
		}
		this.persistentData = kv;

	}

	@Override
	public OperationResult requestCancelImpl(Map<String, Object> params) {
		logger.info("requestCancelImpl: " + params.toString());
		OperationResult result = OperationResult.newInstance();
		return result;
	}

	private void validInput(Map<String, Object> params) {

		this.resourceName = (String) params.get(INPUTS.ResourceName.name());

		if (StringUtils.isBlank(this.resourceName)) {
			throw new MwaInstanceError(
					MWARC.INVALID_INPUT,
					"",
					"Input parameter 'resourceName' does not allow empty characters.");
		}

		String cost = (String) params.get(INPUTS.ResourceCost.name());
		if (cost == null || cost.isEmpty()) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT,
					"",
					"Input parameter 'ResourceCost' does not allow empty characters.");
		} else {
			this.resourceCost = (CostModel) TypeCost.instance().fromString(cost);
			if (this.resourceCost != null) {
				logger.info("Requested resource cost: " + this.resourceCost.toString());
			}
		}

		this.driveResourceName = (String) params.get(INPUTS.DriveResourceName.name());

		if (this.driveResourceName == null) {
			this.driveResourceName = RESOURCE_NAME_ODS_DRIVE;
		} else if (StringUtils.isBlank(this.driveResourceName)) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT,
					"",
					"Input parameter 'DriveResourceName' does not allow empty characters.");
		}

		String driveCost = (String) params.get(INPUTS.DriveResourceCost.name());
		if (driveCost == null || driveCost.isEmpty()) {
			throw new MwaInstanceError(
					MWARC.INVALID_INPUT,
					"",
					"Input parameter 'DriveResourceCost' does not allow empty characters.");
		} else {

			try {
				Integer.parseInt(driveCost);
			} catch (Exception e) {
				throw new MwaInstanceError(
						MWARC.INVALID_INPUT,
						"",
						"Input parameter 'DriveResourceCost' must be a number.");
			}

			final String driveCostXml = String.format(DRIVE_COST_FTORMAT, driveCost);
			this.driveResourceCost = (CostModel) TypeCost.instance().fromString(driveCostXml);
			if (this.driveResourceCost != null) {
				logger.info("Requested resource cost: " + this.driveResourceCost.toString());
			}
		}

		// boolean型にしましょう
		String isNotMountCartridgeStr = (String) params.get(INPUTS.isNotMountCartridge.name());

		if (StringUtils.isBlank(isNotMountCartridgeStr)) {
			throw new MwaInstanceError(
					MWARC.INVALID_INPUT,
					"",
					"Input parameter 'isNotMountCartridge' does not allow empty characters.");
		}

		isNotMountCartridgeStr = isNotMountCartridgeStr.toLowerCase();

		if ("true".equals(isNotMountCartridgeStr)) {
			this.isNotMountCartridgeToNo = NOT_MOUNT_CARTRIDGE;
		} else if ("false".equals(isNotMountCartridgeStr)) {
			this.isNotMountCartridgeToNo = MOUNT_CARTRIDGE;
		} else {
			throw new MwaInstanceError(
					MWARC.INVALID_INPUT,
					"",
					String.format("Invalid value was specified for 'isNotMountCartridge'. value:%s", isNotMountCartridgeStr));
		}
	}

	private boolean isCartridgeResourceRegistered() {
		boolean result = false;

		ResourceCollection resources = this.getCoreModules().getResourceManager().getResources(null, null, null, null);
		if (resources.getTotalCount() > 0) {
			for (ResourceModel resource : resources.getModels()) {
				if (resource.getName().equals(this.resourceName)) {
					result = true;
					this.resourceId = resource.getId();
					logger.info("Found " + this.resourceName + ":" + this.resourceId + " " + resource.toString());
					outputMap = this.getOutputDetails();
					outputMap.put(OUTPUTS.ResourceId.getKey(), this.resourceId);
					this.setOutputDetails(outputMap);

					if (this.driveResourceId != null) {
						break;
					}
				} else if (resource.getName().equals(this.driveResourceName)) {
					this.driveResourceId = resource.getId();

					if (this.resourceId != null) {
						break;
					}
				}

			}
		} else {
			result = false;
			this.resourceId = "";
		}
		return result;
	}

	private void updateDriveResourceCount() {
		ResourceModel model = new ResourceModel();
		ResourceModel result = null;

		model.setId(this.driveResourceId);
		model.setName(this.driveResourceName);
		model.setType(this.driveResourceCost.getType());
		model.setValueList(this.driveResourceCost.getValueList());

		result = this.getCoreModules().getResourceManager().updateResource(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		} else {
			this.driveResourceId = result.getId();
			logger.info("Created " + this.driveResourceName + ":" + this.driveResourceId + " " + result.toString());
			outputMap = this.getOutputDetails();
			// outputMap.put(OUTPUTS.driveResourceId.getKey(),
			// this.driveResourceId);
			this.setOutputDetails(outputMap);
		}
	}

	private void allocateCartridgeResource() {
		ResourceModel model = new ResourceModel();
		ResourceModel result = null;

		model.setName(this.resourceName);
		model.setType(this.resourceCost.getType());
		model.setValueList(this.resourceCost.getValueList());
		model.setParentId(this.driveResourceId);

		result = this.getCoreModules().getResourceManager().addResource(model);
		if (result == null) {
			throw new MwaError(
					MWARC.SYSTEM_ERROR, null, MSG_INTERNAL_ERROR);
		} else {
			this.resourceId = result.getId();
			logger.info("Created " + this.resourceName + ":" + this.resourceId + " " + result.toString());
			outputMap = this.getOutputDetails();
			outputMap.put(OUTPUTS.ResourceId.getKey(), this.resourceId);
			this.setOutputDetails(outputMap);
		}
	}

	private void reserveResource() {
		Map<String, Object> inputParams = new HashMap<String, Object>();

		if (this.getParentId() != null) {
			inputParams.put("occupantId", this.getParentId());
		} else {
			inputParams.put("occupantId", this.getId());
		}

		inputParams.put("occupantName", this.resourceName);

		inputParams.put("receiverId", this.getId());
		// JIRA1075対応
		// inputParams.put("requestResourceId", this.resourceId);
		inputParams.put("cost", com.sony.pro.mwa.common.utils.Converter.toXmlString(this.resourceCost));

		// JIRA1074追加
		{
			List<String> priorityList = new ArrayList<>();

			// PrioritｙListにカートリッジの有無をセット
			priorityList.add(this.isNotMountCartridgeToNo);

			// PrioritｙListにカートリッジ名のセット
			{
				String cartridgeId = null;
				if (this.resourceName.contains("%20")) {
					cartridgeId = this.resourceName.replace("%20", " ");
				} else {
					cartridgeId = this.resourceName;
				}
				priorityList.add(cartridgeId);
			}

			// PrioritｙListにcurrentTimeをセット
			{
				final long currentTimeMillis = System.currentTimeMillis();
				priorityList.add(Long.toString(currentTimeMillis));
			}

			String priorityListStr = TypeListString.instance().toString(priorityList);
			final String requestResourcedriveCostXml = String.format(DRIVE_COST_FTORMAT, "1");
			CostModel requestResourcedriveCost = (CostModel) TypeCost.instance().fromString(requestResourcedriveCostXml);

			inputParams.put("requestResourceId_0", this.resourceId);
			inputParams.put("cost_0", com.sony.pro.mwa.common.utils.Converter.toXmlString(this.resourceCost));
			inputParams.put("requestResourceId_1", this.driveResourceId);
			inputParams.put("cost_1", com.sony.pro.mwa.common.utils.Converter.toXmlString(requestResourcedriveCost));
			inputParams.put("priority", priorityListStr);

		}

		OperationResult result = this.getCoreModules().getActivityManager().createInstance(reserveTaskName, reserveTaskVersion, inputParams);
		String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
		if (instanceId == null) {
			throw new MwaError(
					MWARC.SYSTEM_ERROR,
					null, MSG_INTERNAL_ERROR);
		} else {
			persistentData.put(OUTPUTS.ResourceRequestId.name(), instanceId);
		}
	}

	private void releaseResource() {
		Map<String, Object> inputParams = new HashMap<String, Object>();
		Map<String, Object> outputMap = this.getOutputDetails();
		String resourceRequestId = (String) outputMap.get(OUTPUTS.ResourceRequestId.getKey());
		inputParams.put("requestId", resourceRequestId);
		try {
			OperationResult result = this.getCoreModules().getActivityManager().createInstance(freeResouceTaskName, freeResouceTaskVersion, inputParams);
			String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
			if (instanceId == null) {
				throw new MwaError(
						MWARC.SYSTEM_ERROR, null, MSG_INTERNAL_ERROR);
			}
		} catch (Exception e) {
			// TODO:例外は不要ですか？
			logger.warn("Failed to release resource.");
		}
	}

}
