package com.sony.pro.mwa.repository.query;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/28
 * Time: 6:01
 * To change this template use File | Settings | File Templates.
 */
public class QuerySql {
    private String string;
    private List<Object> paramList;

    /**
     * @param string
     * @param paramList
     */
    public QuerySql(String string, List<Object> paramList) {
        this.string = string;
        this.paramList = paramList;
    }

    /**
     * @return the string
     */
    public String getString() {
        return string;
    }

    /**
     * @param string the string to set
     */
    public void setString(String string) {
        this.string = string;
    }

    /**
     * @return the paramList
     */
    public List<Object> getParamList() {
        return paramList;
    }

    /**
     * @param paramList the paramList to set
     */
    public void setParamList(List<Object> paramList) {
        this.paramList = paramList;
    }

    /**
     *
     */
    @Override
    public String toString() {
        String resultString = string;

        if (paramList.size() != 0) {
            for (int i=0; i<paramList.size(); i++) {
                String variable = "?";
                if (paramList.get(i) != null) {
                    Class<?> variableType = paramList.get(i).getClass();
                    if (variableType == String.class) {
                        variable = "'" + String.valueOf(paramList.get(i)) + "'";
                    } else if (variableType == Boolean.class || variableType.getSuperclass() == Number.class) {
                        variable = String.valueOf(paramList.get(i));
                    } else if (variableType == Timestamp.class) {
                        variable = "'" + ((Timestamp) paramList.get(i)).toString() + "'";
                    } else if (variableType == UUID.class) {
                        variable = "'" + ((UUID) paramList.get(i)).toString() + "'";
                    }
                }

                resultString = StringUtils.replaceOnce(resultString, "?", variable);
            }

        }

        return resultString;
    }
}
