package com.sony.pro.mwa.repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;


public class Page<T> implements List<T> {
    private String totalCount;
    private String count;
    protected List<T> list;

    public Page() {
    	this.list = new ArrayList<T>();
    }

    public Page(List<T> list) {
        this.list = list;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<T> getList() {
        return this.list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.list.size();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return this.list.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return this.list.contains(o);
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return this.list.iterator();
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return this.list.toArray();
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return this.list.toArray();
	}

	@Override
	public boolean add(T e) {
		// TODO Auto-generated method stub
		return this.list.add(e);
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return this.list.remove(o);
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return this.list.containsAll(c);
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return this.list.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection c) {
		// TODO Auto-generated method stub
		return this.list.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return this.list.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return this.list.retainAll(c);
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		this.list.clear();
	}

	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		return this.list.get(index);
	}

	@Override
	public T set(int index, T element) {
		// TODO Auto-generated method stub
		return this.list.set(index, element);
	}

	@Override
	public void add(int index, T element) {
		// TODO Auto-generated method stub
		this.list.add(index, element);
	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		return this.list.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return this.list.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return this.list.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return this.list.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub
		return this.list.listIterator(index);
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return this.list.subList(fromIndex, toIndex);
	}
}
