package com.sony.pro.mwa.internal.activity;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.sony.pro.mwa.activity.framework.AbsMwaExecutorActivity;
import com.sony.pro.mwa.activity.framework.executor.ExecutorServiceManager;
import com.sony.pro.mwa.activity.framework.executor.ExecutorTaskCallable;
import com.sony.pro.mwa.activity.framework.standard.ExecutorTaskEvent;
import com.sony.pro.mwa.activity.framework.standard.ExecutorTaskState;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeLong;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class CleanupActivityInstanceTask extends AbsMwaExecutorActivity {
	
	/*-------------------------------------------------------------------------------------------
	 *  Template用定義  INPUTS
	 -------------------------------------------------------------------------------------------*/
	public enum INPUTS implements IParameterDefinition {
		ParentInstanceId(TypeString.instance(), false),
		sort(TypeListString.instance(), false), 
		filter(TypeListString.instance(), false), 
		offset(TypeLong.instance(), false), 
		limit(TypeLong.instance(), false), 
		retentionDays(TypeLong.instance(), false), 
		;

		private INPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return type.name();
		}

		IParameterType type;
		boolean required;
	}

	/*-------------------------------------------------------------------------------------------
	 *  Template用定義  OUTPUTS
	 -------------------------------------------------------------------------------------------*/
	public enum OUTPUTS implements IParameterDefinition {
		result(TypeString.instance(), true),
		resultCount(TypeLong.instance(), true)
		;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		public boolean getRequired() {
			return required;
		}

		public String getKey() {
			return this.name();
		}

		public IParameterType getType() {
			return type;
		}

		public String getTypeName() {
			return type.name();
		}

		IParameterType type;
		boolean required;
	}
	

	@Override
	public ExecutorTaskCallable createTaskCallable() {
		
		return new CleanupActivityInstanceTaskCallable(this.getCoreModules().getActivityManager(),this.getTemplate().getInputs());
		
	}
	
	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		
		ExecutorTaskCallable task = ExecutorServiceManager.getInstance().getActivityInstance(this.getId());
		CleanupActivityInstanceTaskCallable cleanupActivityInstanceTask = (CleanupActivityInstanceTaskCallable) task;
		
		// 現在の状態を取得
		IState iStatus = this.getStatus();
		if (!(iStatus instanceof ExecutorTaskState)) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", "");
		}
		ExecutorTaskState state = (ExecutorTaskState) iStatus;

		Map<String, Object> output = new HashMap<>();
		
		// Decid Event
		IEvent event = ExecutorTaskEvent.NOTIFY_READY;
		
		if (null == cleanupActivityInstanceTask) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_INSTANCE_ID);
		}

		Integer insProgress = null;
		if(task.getAcitivityProgress() != null){
			insProgress = task.getAcitivityProgress();
		}

		if (cleanupActivityInstanceTask.getFuture().isDone()) {
			try {
				output = cleanupActivityInstanceTask.getFuture().get();
				this.setOutputDetails(output);
				event = ExecutorTaskEvent.NOTIFY_COMPLETED;
				insProgress = 100;
			} catch (InterruptedException | ExecutionException e) {
				try {
					output = cleanupActivityInstanceTask.getErrorOutputs();
					if (null != output && output.size() > 0) {
						this.setOutputDetails(output);
					}
				} catch (NoSuchMethodError medhodErr) {
					logger.info("Executor ver 1.2.1 does not have getErrorOutputs method. ");
				}
				if (e.getCause() != null && (e.getCause().getClass().getName().equals(MwaError.class.getName()) || e.getCause().getClass().getName().equals(MwaInstanceError.class.getName()))) {
					throw (MwaError) e.getCause();

				}
				event = CommonEvent.NOTIFY_ERROR;
				ExecutorServiceManager.getInstance().removeActivityInstance(this.getId());
				throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, "", e.getMessage());
			}
			
		} else if (task.getCalledFlg()) {
			event = ExecutorTaskEvent.NOTIFY_IN_PROGRESS;
		}
		
		
		// 終了状態処理
		if (ExecutorTaskState.COMPLETED.equals(state) || ExecutorTaskState.ERROR.equals(state) ){
			ExecutorServiceManager.getInstance().removeActivityInstance(this.getId());
		}

		OperationResultWithStatus resultWithStatus = OperationResultWithStatus.newInstance(event, insProgress);
		
		return resultWithStatus;
	}

	@Override
	public Integer getMaxConcurrentJobs() {
		return 1;
	}

}
