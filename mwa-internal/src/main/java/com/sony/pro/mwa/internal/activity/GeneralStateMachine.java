package com.sony.pro.mwa.internal.activity;

import com.sony.pro.mwa.activity.framework.internal.GeneralEventImpl;
import com.sony.pro.mwa.activity.framework.internal.GeneralStateImpl;
import com.sony.pro.mwa.activity.framework.stm.CommonEvent;
import com.sony.pro.mwa.activity.framework.stm.CommonState;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

public class GeneralStateMachine implements IStateMachine {

	@Override
	public boolean isAcceptable(IState current, IEvent event) {
		return true;
	}

	@Override
	public IState getNextState(IState state, IEvent event) {
		if (event instanceof GeneralEventImpl) {
			return ((GeneralEventImpl)event).getNextState();
		} else if (CommonEvent.REQUEST_SUBMIT.equals(event)) {
			return new GeneralStateImpl(state.getName(), false, false, false, false);
		}
		return null;
	}


}
