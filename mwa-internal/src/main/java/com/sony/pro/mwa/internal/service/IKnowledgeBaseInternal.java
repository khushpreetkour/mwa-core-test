package com.sony.pro.mwa.internal.service;

import java.util.List;

import com.sony.pro.mwa.activity.template.ActivityTemplate;
import com.sony.pro.mwa.service.kbase.IKnowledgeBase;
import com.sony.pro.mwa.service.provider.ActivityProviderType;

public interface IKnowledgeBaseInternal extends IKnowledgeBase {
    public void initialize();
    // add by Xi.Ou for reload framework controller from AMS
    public void setClearCache(Boolean clearCache);
    public void destroy();
    public void registerSystemTemplates(List<ActivityTemplate> templates);
    public void registerSystemProviderTypes(List<ActivityProviderType> providerTypes);
}
