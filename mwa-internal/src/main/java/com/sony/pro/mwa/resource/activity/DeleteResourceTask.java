package com.sony.pro.mwa.resource.activity;

import java.util.*;
import com.sony.pro.mwa.activity.framework.standard.SyncTaskBase;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.model.resource.ResourceRequestCollection;
import com.sony.pro.mwa.model.resource.ResourceRequestModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.resource.IResourceManager;
import com.sony.pro.mwa.utils.ParameterUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author xi.ou
 *
 */
public class DeleteResourceTask extends SyncTaskBase {

	public enum INPUTS implements IParameterDefinition {
		ResourceId(TypeString.instance(), true),
		;

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}

		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		Result(TypeBoolean.instance(), true),
		ResourceId(TypeString.instance(), true), ;

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
	}

	protected Map<String, Object> validate(List<? extends IParameterDefinition> paramDefs, Map<String, Object> params) {
		return ParameterUtils.FROM_STRING.convert(paramDefs, params);
	}

	/***
	 * Jobを実行する
	 * @param input 入力パラメータ
	 * @return 出力パラメータ
	 */
	@Override
	public OperationResult requestSubmitImpl(Map<String, Object> input) {
		OperationResult result = OperationResultWithStatus.newInstance();
		Map<String, Object> inputMap = this.validate(Arrays.asList(INPUTS.values()), input);

		String resourceId = (String) inputMap.get(INPUTS.ResourceId.name());
		Boolean delResult = false;
		
		IResourceManager rm = this.getCoreModules().getResourceManager();
		try {
			rm.deleteResourceIfNotUsed(resourceId);
			delResult = true;
		} catch (MwaError e) {
			logger.error("Can't delete resource: id=" + resourceId + ", msg=[" + e.getMWARC() + "] " + e.getDeviceResponseDetails(),  e);
			delResult = false;
		} catch (Exception e) {
			logger.error("Can't delete resource: id=" + resourceId + ", msg=" + e.getMessage(), e);
			delResult = false;
		}
		
		result.put(OUTPUTS.ResourceId.getKey(), resourceId);
		result.put(OUTPUTS.Result.getKey(), delResult);
		return result;
	}

}
