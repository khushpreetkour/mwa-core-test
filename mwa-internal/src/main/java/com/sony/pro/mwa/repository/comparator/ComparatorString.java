package com.sony.pro.mwa.repository.comparator;

import java.lang.reflect.Field;

import com.sony.pro.mwa.common.log.MwaLogger;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/10/07
 * Time: 7:00
 * To change this template use File | Settings | File Templates.
 */
public class ComparatorString extends Comparator {

	private final static MwaLogger logger = MwaLogger.getLogger(ComparatorString.class);
    @Override
    public int compare(Object o1, Object o2) {
        String so1 = "";
        String so2 = null;
        Field field = null;
        try {
            field = o1.getClass().getDeclaredField(this.name);
            field.setAccessible(true);
            so1 = (String) field.get(o1);
            so2 = (String) field.get(o2);
        } catch (NoSuchFieldException e) {
        	logger.error("compare:", e);  //To change body of catch statement use File | Settings | File Templates.
        }  catch (IllegalAccessException e) {
        	logger.error("compare:", e);	//To change body of catch statement use File | Settings | File Templates.
        }

        return so1.compareTo(so2);
    }

    @Override
    public boolean compareTo(Object o1, String value) {
        String so1 = "";
        Field field = null;
        try {
            field = o1.getClass().getDeclaredField(this.name);
            field.setAccessible(true);
            so1 = (String) field.get(o1);
        } catch (NoSuchFieldException e) {
        	logger.error("compareTo:", e);  //To change body of catch statement use File | Settings | File Templates.
        }  catch (IllegalAccessException e) {
        	logger.error("compareTo:", e);  //To change body of catch statement use File | Settings | File Templates.
        }

        return so1.compareTo(value) == 0;
    }
}
