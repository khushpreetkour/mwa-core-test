package com.sony.pro.mwa.resource.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskBase;
import com.sony.pro.mwa.activity.framework.standard.PollingTaskEvent;
import com.sony.pro.mwa.enumeration.PresetParameter;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.KeyValueModel;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.resource.CostModel;
import com.sony.pro.mwa.model.resource.ResourceCollection;
import com.sony.pro.mwa.model.resource.ResourceModel;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.parameter.OperationResultWithStatus;
import com.sony.pro.mwa.parameter.type.TypeBoolean;
import com.sony.pro.mwa.parameter.type.TypeCost;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeString;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.activity.IEvent;
import com.sony.pro.mwa.service.activity.IState;

/**
 *
 */
public class AllocatePreviewResourceTask extends PollingTaskBase {
	private static final String DRIVE_COST_FTORMAT = "<costModel><type>DRIVE</type><valueList><key>COUNT</key><value>%S</value></valueList></costModel>";
	private static final String PREVIEW_MODE = "2";
	private static final String RESOURCE_NAME_ODS_DRIVE = "ODS-DRIVE";
	protected Map<String, Object> persistentData = new HashMap<String, Object>();
	Map<String, Object> outputMap = new HashMap<String, Object>();

	private final String reserveTaskName = "com.sony.pro.mwa.resource.activity.ReserveResourceTask";
	private final String reserveTaskVersion = "1";

	private String freeToReservedResourceId;
	private String cartridgeSerialId;
	private String resourceId;
	private String driveResourceId;

	private String driveResourceName = RESOURCE_NAME_ODS_DRIVE;
	private String isNotMountCartridgeToNo = PREVIEW_MODE;
	private CostModel resourceCost;
	private CostModel driveResourceCost;
	private boolean skipAllocate = false;
	private int intTimeOutPeriod = 0;
	private String providerId;

	public AllocatePreviewResourceTask() {
		super(AllocatePreviewResourceTaskState.CREATED, AllocatePreviewResourceStateMachine.getInstance());
	}

	public enum INPUTS implements IParameterDefinition {
		ProviderId(TypeString.instance(), false),
		cartridgeSerialId(TypeString.instance(), true),
		ResourceCost(TypeString.instance(), true),
		DriveResourceCost(TypeString.instance(), true),
		driveLetter(TypeString.instance(), false),
		timeOutPeriod(TypeString.instance(), true);

		private INPUTS(IParameterType type, boolean required) {
			this.key = this.name();
			this.type = type;
			this.required = required;
		}

		private INPUTS(IParameterDefinition param, boolean required) {
			this.key = param.getKey();
			this.type = param.getType();
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
		String key;
	}

	public enum OUTPUTS implements IParameterDefinition {
		Result(TypeBoolean.instance(), true),
		AutoFreeResourceTaskInstanceId(TypeString.instance(), true);

		private OUTPUTS(IParameterType type, boolean required) {
			this.type = type;
			this.required = required;
		}

		@Override
		public boolean getRequired() {
			return required;
		}

		@Override
		public String getKey() {
			return this.name();
		}

		@Override
		public IParameterType getType() {
			return type;
		}

		@Override
		public String getTypeName() {
			return this.getType().name();
		}

		IParameterType type;
		boolean required;
	}

	@Override
	public OperationResult requestStopImpl(Map<String, Object> params) {
		logger.info("requestStopImpl: " + params.toString());

		String autoFreeResourceTaskInstanceId = (String) this.persistentData.get("autoFreeResourceTaskInstanceId");

		OperationResult operationResult = this.getCoreModules().getActivityManager().operateActivity(autoFreeResourceTaskInstanceId, "REQUEST_STOP", null);

		Map<String, Object> resultParam = new HashMap<String, Object>();
		resultParam.put("autoFreeResourceTaskInstanceId", autoFreeResourceTaskInstanceId);

		this.persistentData = resultParam;
		this.setOutputDetails(operationResult);
		return operationResult;
	}

	@Override
	public OperationResult requestCancelImpl(Map<String, Object> params) {
		logger.info("requestCancelImpl: " + params.toString());

		String autoFreeResourceTaskInstanceId = (String) this.persistentData.get("autoFreeResourceTaskInstanceId");

		OperationResult operationResult = this.getCoreModules().getActivityManager().operateActivity(autoFreeResourceTaskInstanceId, "REQUEST_STOP", null);

		Map<String, Object> resultParam = new HashMap<String, Object>();
		resultParam.put("autoFreeResourceTaskInstanceId", autoFreeResourceTaskInstanceId);

		this.persistentData = resultParam;

		this.setOutputDetails(operationResult);
		return operationResult;
	}

	@Override
	public OperationResult requestPauseImpl(Map<String, Object> params) {
		logger.info("requestPauseImpl: " + params.toString());
		return null;
	}

	public OperationResult requestUpdateImpl(Map<String, Object> params) {
		logger.info("requestUpdateImpl: " + params.toString());

		String autoFreeResourceTaskInstanceId = (String) this.persistentData.get("autoFreeResourceTaskInstanceId");

		OperationResult operationResult = this.getCoreModules().getActivityManager().operateActivity(autoFreeResourceTaskInstanceId, "REQUEST_UPDATE", null);

		Map<String, Object> resultParam = new HashMap<String, Object>();
		resultParam.put("autoFreeResourceTaskInstanceId", autoFreeResourceTaskInstanceId);

		this.setOutputDetails(operationResult);
		this.persistentData = resultParam;

		return operationResult;
	}

	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {
		validInput(params);

		if (this.cartridgeSerialId.contains(" ")) {
			this.cartridgeSerialId = this.cartridgeSerialId.replace(" ", "%20");
		}

		if (skipAllocate) {
			outputMap.clear();
			// outputMap.put(OUTPUTS.ResourceId.getKey(), "");
			// outputMap.put(OUTPUTS.ReservedResourceId.getKey(), "");
			this.setOutputDetails(outputMap);

			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_COMPLETED, 100);
		} else {
			if (!isRegistered()) {
				allocateCartridgeResource();
			}

			// DriveリソースカウントのUPDATE
			updateDriveResourceCount();

			reserveResource();

			// 子インスタンス作成
			Map<String, Object> subParams = new HashMap<>();
			subParams.put("ResourceName", this.cartridgeSerialId);
			subParams.put("RequestResourceId", this.freeToReservedResourceId);
			subParams.put("timeOutPeriod", Integer.toString(this.intTimeOutPeriod));
			if(StringUtils.isNotEmpty(this.providerId )){
				subParams.put("ProviderId", this.providerId);
			}

			OperationResult result = this.getCoreModules().getActivityManager().createInstance("com.sony.pro.mwa.resource.activity.AutoFreeResourceTask", "1", subParams);
			String autoFreeResourceTaskInstanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());

			Map<String, Object> resultParam = new HashMap<String, Object>();
			resultParam.put("autoFreeResourceTaskInstanceId", autoFreeResourceTaskInstanceId);

			this.persistentData = resultParam;

			return OperationResult.newInstance();
		}
	}

	@Override
	protected OperationResult processEventImpl(IEvent event, Map<String, Object> params) {
		OperationResult result = null;

		// Eventと実装メソッドのマッピング
		if (PollingTaskEvent.REQUEST_PAUSE.equals(event)) {
			result = requestPauseImpl(params);
		} else if (AllocatePreviewResourceEvent.REQUEST_STOP.equals(event)) {
			result = requestStopImpl(params);
		} else if (AllocatePreviewResourceEvent.REQUEST_UPDATE.equals(event)) {
			result = requestUpdateImpl(params);
		}
		// else if (AllocatePreviewResourceEvent.REQUEST_CANCEL.equals(event)) {
		// result = requestUpdateImpl(params);
		// }

		return result;
	}

	@Override
	public OperationResultWithStatus confirmStatusImpl(Map<String, Object> params) {
		if (this.getStatus().isCancel()) {
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_CANCELLED, 100);
		}

		ActivityInstanceModel model = this.getCoreModules().getActivityManager().getChildInstances(this.getId()).first();

		String autoFreeResourceTaskInstanceId = (String) this.persistentData.get("autoFreeResourceTaskInstanceId");
		outputMap = this.getOutputDetails();
		outputMap.put(OUTPUTS.AutoFreeResourceTaskInstanceId.getKey(), autoFreeResourceTaskInstanceId);
		
		this.setOutputDetails(outputMap);
		
		if (model.getStatus().isTerminated()) {

			outputMap.put(OUTPUTS.Result.getKey(), true);
			this.setOutputDetails(outputMap);
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_COMPLETED, 100);
		} else {
			this.setOutputDetails(outputMap);
			return OperationResultWithStatus.newInstance(PollingTaskEvent.NOTIFY_UPDATED, 0);
		}
	}

	@Override
	public String getPersistentDataImpl() {
		String json = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			json = objectMapper.writeValueAsString(this.persistentData);
		} catch (JsonGenerationException e) {
			logger.error("json generation error getPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (JsonMappingException e) {
			logger.error("json mapping error getPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (IOException e) {
			logger.error("jason mapping IO error getPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		}
		return json;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setPersistentDataImpl(String details) {
		Map<String, Object> kv = null;
		try {
			kv = new ObjectMapper().readValue(details, HashMap.class);
		} catch (JsonParseException e) {
			logger.error("json generation error setPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (JsonMappingException e) {
			logger.error("json mapping error setPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		} catch (IOException e) {
			logger.error("jason mapping IO error setPersistentDataImpl failed.");
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		}
		this.persistentData = kv;

	}

	private void validInput(Map<String, Object> params) {
		this.cartridgeSerialId = (String) params.get(INPUTS.cartridgeSerialId.name());
		if (this.cartridgeSerialId == null || this.cartridgeSerialId.isEmpty()) {
			skipAllocate = true;
		}

		
		String providerIdStr = (String) params.get(INPUTS.ProviderId.name());
		if(StringUtils.isEmpty(providerIdStr)){
			this.providerId = null;
		}else{
			this.providerId = providerIdStr;
		}
		
		
		String cost = (String) params.get(INPUTS.ResourceCost.name());
		if (cost == null || cost.isEmpty()) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, "", "The resource name can not be emptied!");
		} else {
			this.resourceCost = (CostModel) TypeCost.instance().fromString(cost);
			if (this.resourceCost != null) {
				logger.info("Requested resource cost: " + this.resourceCost.toString());
			}
		}

		String timeOutPeriod = (String) params.get(INPUTS.timeOutPeriod.name());
		try {
			this.intTimeOutPeriod = Integer.parseInt(timeOutPeriod);
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, "", "timeOutPeriod is not Integer value!");
		}

		String driveCost = (String) params.get(INPUTS.DriveResourceCost.name());
		if (driveCost == null || driveCost.isEmpty()) {

			final String errorMsg = "The resource name can not be emptied!.";
			logger.error(errorMsg);
			throw new MwaInstanceError(MWARC.INVALID_INPUT, "", errorMsg);
		} else {

			try {
				Integer.parseInt(driveCost);
			} catch (Exception e) {
				final String errorMsg = "The resource name numberformat error!.";
				logger.error(errorMsg);
				throw new MwaInstanceError(MWARC.INVALID_INPUT, "", errorMsg);
			}

			final String driveCostXml = String.format(DRIVE_COST_FTORMAT, driveCost);
			this.driveResourceCost = (CostModel) TypeCost.instance().fromString(driveCostXml);

			if (this.driveResourceCost != null) {
				logger.info("Requested driveResourceCost: " + this.driveResourceCost.toString());
			}
		}
	}

	private boolean isRegistered() {
		boolean result = false;

		ResourceCollection resources = this.getCoreModules().getResourceManager().getResources(null, null, null, null);
		if (resources.getTotalCount() > 0) {
			for (ResourceModel resource : resources.getModels()) {
				if (resource.getName().equals(this.cartridgeSerialId)) {
					result = true;
					this.resourceId = resource.getId();
					logger.info("Found " + this.cartridgeSerialId + ":" + this.resourceId + " " + resource.toString());
					outputMap = this.getOutputDetails();
					this.setOutputDetails(outputMap);
					
					if (this.driveResourceId != null) {
						break;
					}
				}else if (resource.getName().equals(this.driveResourceName)) {
					this.driveResourceId = resource.getId();

					if (this.resourceId != null) {
						break;
					}
				}
			}
		} else {
			result = false;
			this.resourceId = "";
		}
		return result;
	}

	private void updateDriveResourceCount() {
		ResourceModel model = new ResourceModel();
		ResourceModel result = null;

		model.setId(this.driveResourceId);
		model.setName(this.driveResourceName);
		model.setType(this.driveResourceCost.getType());
		model.setValueList(this.driveResourceCost.getValueList());

		result = this.getCoreModules().getResourceManager().updateResource(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		} else {
			this.driveResourceId = result.getId();
			logger.info("Created " + this.driveResourceName + ":" + this.driveResourceId + " " + result.toString());
			outputMap = this.getOutputDetails();
			// outputMap.put(OUTPUTS.driveResourceId.getKey(),
			// this.driveResourceId);
			this.setOutputDetails(outputMap);
		}
	}

	private void allocateCartridgeResource() {
		ResourceModel model = new ResourceModel();
		ResourceModel result = null;

		model.setName(this.cartridgeSerialId);
		model.setType(this.resourceCost.getType());
		model.setValueList(this.resourceCost.getValueList());
		model.setParentId(this.driveResourceId);
		
		result = this.getCoreModules().getResourceManager().addResource(model);
		if (result == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		} else {
			this.resourceId = result.getId();
			logger.info("Created " + this.cartridgeSerialId + ":" + this.resourceId + " " + result.toString());
			outputMap = this.getOutputDetails();
			// outputMap.put(OUTPUTS.ResourceId.getKey(), this.resourceId);
			this.setOutputDetails(outputMap);
		}
	}

	private void reserveResource() {
		Map<String, Object> inputParams = new HashMap<String, Object>();

		if (this.getParentId() != null) {
			inputParams.put("occupantId", this.getParentId());
		} else {
			inputParams.put("occupantId", this.getId());
		}

		inputParams.put("receiverId", this.getId());
		//JIRA1075対応で廃止
//		inputParams.put("requestResourceId", this.resourceId);
		inputParams.put("cost", com.sony.pro.mwa.common.utils.Converter.toXmlString(this.resourceCost));

		//JIRA1074対応
		{

			List<String> priorityList = new ArrayList<>();

			// PrioritｙListにカートリッジの有無をセット
			priorityList.add(this.isNotMountCartridgeToNo);

			// PrioritｙListにカートリッジ名のセット
			{
				String cartridgeId = null;
				if (this.cartridgeSerialId.contains("%20")) {
					cartridgeId = this.cartridgeSerialId.replace("%20", " ");
				} else {
					cartridgeId = this.cartridgeSerialId;
				}
				priorityList.add(cartridgeId);
			}

			// PrioritｙListにcurrentTimeをセット
			{
				final long currentTimeMillis = System.currentTimeMillis();
				priorityList.add(Long.toString(currentTimeMillis));
			}

			String priorityListStr = TypeListString.instance().toString(priorityList);

			final String requestResourcedriveCostXml = String.format(DRIVE_COST_FTORMAT, "1");
			CostModel requestResourcedriveCost = (CostModel) TypeCost.instance().fromString(requestResourcedriveCostXml);

			inputParams.put("requestResourceId_0", this.resourceId);
			inputParams.put("cost_0", com.sony.pro.mwa.common.utils.Converter.toXmlString(this.resourceCost));
			inputParams.put("requestResourceId_1", this.driveResourceId);
			inputParams.put("cost_1", com.sony.pro.mwa.common.utils.Converter.toXmlString(requestResourcedriveCost));
			inputParams.put("priority", priorityListStr);
		}

		OperationResult result = this.getCoreModules().getActivityManager().createInstance(reserveTaskName, reserveTaskVersion, inputParams);
		String instanceId = (String) result.get(PresetParameter.ActivityInstanceId.getKey());
		if (instanceId == null) {
			throw new MwaError(MWARC.SYSTEM_ERROR);
		} else {
			this.freeToReservedResourceId = instanceId;
			// persistentData.put(OUTPUTS.ReservedResourceId.name(),
			// instanceId);
		}
	}

	/**
	 * KeyValueModel[]のJsonを生成する。
	 * 
	 * @param KeyValueModel
	 *            [] models
	 * @return json
	 */
	public String convetKeyValuesToJson(KeyValueModel[] models) {
		String jsonParam = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			jsonParam = objectMapper.writeValueAsString(models);
		} catch (JsonGenerationException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, MWARC.INTEGRATION_ERROR.name(), "json generation error getPersistentDataImpl failed.");
		} catch (JsonMappingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, MWARC.INTEGRATION_ERROR.name(), "json mapping error getPersistentDataImpl failed.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR, MWARC.INTEGRATION_ERROR.name(), "jason mapping IO error getPersistentDataImpl failed.");
		}
		return jsonParam;
	}

	@Override
	protected List<? extends IEvent> getSupportedEventListImpl() {
		return Arrays.asList(AllocatePreviewResourceEvent.values());
	}

	@Override
	protected List<? extends IState> getSupportedStateListImpl() {
		return Arrays.asList(AllocatePreviewResourceTaskState.values());
	}

}
