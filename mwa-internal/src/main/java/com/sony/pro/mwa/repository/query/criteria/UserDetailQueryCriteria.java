package com.sony.pro.mwa.repository.query.criteria;

import com.sony.pro.mwa.enumeration.AxisEnum;
import com.sony.pro.mwa.repository.query.Filtering;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.Sorting;

import java.security.Principal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Developer
 * Date: 13/11/27
 * Time: 7:25
 * To change this template use File | Settings | File Templates.
 */
public class UserDetailQueryCriteria extends QueryCriteria {
    private AxisEnum userGroupAxis;

    public UserDetailQueryCriteria() {}

    public UserDetailQueryCriteria(AxisEnum userGroupAxis, QueryCriteria criteria) {
        super(criteria.getSortings(), criteria.getFilterings(), criteria.getOffset(), criteria.getLimit(), criteria.getPrincipal());
        this.userGroupAxis = userGroupAxis;
    }

    public UserDetailQueryCriteria(AxisEnum userGroupAxis, List<Sorting> sortings, List<List<Filtering>> filterings, long offset, long limit, Principal principal) {
        super(sortings, filterings, offset, limit, principal);
        this.userGroupAxis = userGroupAxis;
    }

    public AxisEnum getUserGroupAxis() {
        return userGroupAxis;
    }

    public void setUserGroupAxis(AxisEnum userGroupAxis) {
        this.userGroupAxis = userGroupAxis;
    }
}
