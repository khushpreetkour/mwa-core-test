package com.sony.pro.mwa.provider.repository;

import com.sony.pro.mwa.model.provider.ServiceEndpointCollection;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ServiceEndpointDao extends ModelBaseDao<ServiceEndpointModel, ServiceEndpointCollection> {
	public int deleteModelByProvider(String providerId);
}
