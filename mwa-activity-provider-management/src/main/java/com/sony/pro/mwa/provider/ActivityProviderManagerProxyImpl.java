package com.sony.pro.mwa.provider;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.utils.rest.HttpMethodType;
import com.sony.pro.mwa.utils.rest.IRestInput;
import com.sony.pro.mwa.utils.rest.RestClient;
import com.sony.pro.mwa.utils.rest.RestInputImpl;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.utils.RestUtils;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.service.provider.IActivityProviderManager;

@Component
public class ActivityProviderManagerProxyImpl implements IActivityProviderManager {

	private final static MwaLogger logger = MwaLogger.getLogger(ActivityProviderManagerProxyImpl.class);

	protected RestClient restClient = new RestClient("");
	private static final String URL_FORMAT = "http://%s:%s/mwa/api/v2/activity-providers";
	private static final String URL_ADDED_ID_FORMAT = "http://%s:%s/mwa/api/v2/activity-providers/%s";

	// TODO host,portはとりあえずハードコーディング
	private static final String HOST = "localhost";
	private static final String PORT = "8081";

	public void initialize() {
	}

	public void destroy() {
	}

	private String getUrl(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlのパスまで生成
		String url = getUrl();
		// queryを生成
		String query = RestUtils.createQuery(sorts, filters, offset, limit);
		// queryがnullでなければ、urlと結合する
		if (null != query) {
			url = url + "?" + query;
		}
		return url;
	}

	private String getUrl() {
		return String.format(URL_FORMAT, HOST, PORT);
	}

	private String getUrl(String id) {
		return String.format(URL_ADDED_ID_FORMAT, HOST, PORT, id);
	}

	@Override
	public ActivityProviderCollection getProviders(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ActivityProviderCollection models = (ActivityProviderCollection) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderCollection.class);

		return models;
	}

	@Override
	public ActivityProviderModel getProvider(String providerId) {
		// urlを取得
		String url = getUrl(providerId);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ActivityProviderModel model = (ActivityProviderModel) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderModel.class);
		return model;
	}

	@Override
	public IActivityProvider getProviderService(String providerId) {
		if (providerId == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		// urlを取得
		String url = getUrl(providerId);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.GET.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ActivityProviderModel model = (ActivityProviderModel) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderModel.class);

		ActivityProvider result = new ActivityProvider();
		result.setModel(model);
		return result;
	}

	@Override
	@Transactional(value = "transactionManagerForMwa", readOnly = false, rollbackFor = Exception.class)
	public ActivityProviderModel addProvider(ActivityProviderModel provider) {
		// urlを取得
		String url = getUrl();
		// LocationModelをJson形式に変換
		String json = RestUtils.convertToJson(provider);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.POST.name(), url, json);
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ActivityProviderModel result = (ActivityProviderModel) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderModel.class);

		return result;
	}

	@Override
	@Transactional(value = "transactionManagerForMwa", readOnly = false, rollbackFor = Exception.class)
	public ActivityProviderModel updateProvider(ActivityProviderModel provider) {
		// urlを取得
		String url = getUrl(provider.getId());
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.PUT.name(), url, RestUtils.convertToJson(provider));
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		ActivityProviderModel result = (ActivityProviderModel) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderModel.class);

		return result;
	}

	@Override
	public ActivityProviderModel deleteProvider(String providerId) {
		// urlを取得
		String url = getUrl(providerId);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationModelに変換
		ActivityProviderModel result = (ActivityProviderModel) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderModel.class);
		return result;
	}

	@Override
	public ActivityProviderCollection deleteProviders(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		// urlを取得
		String url = getUrl(sorts, filters, offset, limit);
		// inputを生成
		IRestInput restInput = new RestInputImpl(HttpMethodType.DELETE.name(), url, "");
		// rest通信
		this.restClient.rest(restInput);

		// restの結果をLocationCollectionに変換
		ActivityProviderCollection models = (ActivityProviderCollection) RestUtils.convertToObj(restClient.getResponseBody(), ActivityProviderCollection.class);
		return models;
	}

	@Override
	public ActivityProviderTypeModel addProvidertype(ActivityProviderTypeModel arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/* public ActivityProviderTypeModel
	 * addProvidertype(ActivityProviderTypeModel model) {
	 * model.setEndpointTypes(endpointTypes);
	 * model.setId(UUIDUtils.generateUUID(name, modelNumber));
	 * model.setModelNumber(modelNumber); model.setName(name);
	 * model.setTemplateInfos(templateInfos);
	 * 
	 * ActivityProviderTypeModel prevModel =
	 * this.activityProviderTypeDao.getModel(model.getId(), null); if (prevModel
	 * == null) { this.activityProviderTypeDao.addModel(model); } else {
	 * this.activityProviderTypeDao.updateModel(model); } return model; } */
}
