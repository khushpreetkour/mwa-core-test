package com.sony.pro.mwa.provider.repository.database.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sony.pro.mwa.model.GenericPropertyCollection;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.database.query.Query;
import com.sony.pro.mwa.repository.query.Column;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.QuerySql;

public class ActivityProviderPropertyQuery extends Query<GenericPropertyModel, GenericPropertyCollection, QueryCriteria> {
    public enum COLUMN {
        ID("i.activity_provider_id", "providerId", UUID.class, true),
        NAME("i.activity_provider_property_name", "name", String.class, true),
        VALUE("i.activity_provider_property_value", "value", String.class, true),
        ;

        private final String name;
        private final String alias;
        private final Class<?> type;
        private final boolean sortable;

        private COLUMN(String name, String alias, Class<?> type, boolean sortable) {
            this.name = name;
            this.alias = alias;
            this.type = type;
            this.sortable = sortable;
        }
    }

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_MYSQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_MYSQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String WITH_RECURSIVE_CLAUSE = "";
/*    private static final String WITH_RECURSIVE_CLAUSE =
            "WITH RECURSIVE " +
                    "recursive_user_group(system_user_group_id, system_user_group_name, system_user_group_parent_system_user_group_id) AS ( " +
            "SELECT " +
                    "ug.system_user_group_id, ug.system_user_group_name, ug.system_user_group_parent_system_user_group_id " +
            "FROM " +
                    "dig.system_user_group AS ug, " +
                    "dig.system_user_system_user_group_system_role AS ugr " +
            "WHERE " +
                    "ug.system_user_group_id = ugr.system_user_group_id " +
            "AND " +
                    "ugr.system_user_id = ? " +
            "UNION " +
            "SELECT " +
                    "ug.system_user_group_id, ug.system_user_group_name, ug.system_user_group_parent_system_user_group_id " +
            "FROM " +
                    "dig.system_user_group AS ug, recursive_user_group AS pug " +
            "WHERE " +
                    "pug.system_user_group_parent_system_user_group_id = ug.system_user_group_id " +
                    ") ";*/

    private static final String SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL;
    static {
        String selectClause = "SELECT ";
        String separator = ", ";
        for (COLUMN column : COLUMN.values()) {
            selectClause += column.name + " AS " + column.alias + separator;
        }

        SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL = selectClause.substring(0, selectClause.length() - separator.length()) + " ";
    }

    private static final String COUNT_ALIAS = "count";

    private static final String SELECT_CLAUSE_FOR_COUNT = "SELECT COUNT(" + COLUMN.ID.name + ") AS " + COUNT_ALIAS + " ";

    private static final String FROM_WHERE_CLAUSE_FOR_MYSQL =
            "FROM " + 
            "mwa.activity_provider_property AS i "+
            "WHERE " +
            "TRUE ";
    private static final String FROM_WHERE_CLAUSE_FOR_POSTGRESQL = FROM_WHERE_CLAUSE_FOR_MYSQL;

    


    private static final Map<DatabaseEnum, String> selectListQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_LIST_FOR_MYSQL + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_LIST_FOR_POSTGRESQL + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectListQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<DatabaseEnum, String> selectCountQueryMap;
    static {
        Map<DatabaseEnum, String> map = new HashMap<>();
        map.put(DatabaseEnum.MYSQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_MYSQL);
        map.put(DatabaseEnum.POSTGRESQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        map.put(DatabaseEnum.H2SQL, SELECT_CLAUSE_FOR_COUNT + FROM_WHERE_CLAUSE_FOR_POSTGRESQL);
        selectCountQueryMap = Collections.unmodifiableMap(map);
    }

    private static final Map<String, Column> columnMap;
    static {
        Map<String, Column> map = new HashMap<>();
        for (COLUMN enumColumn : COLUMN.values()) {
            map.put(enumColumn.alias, new Column(enumColumn.name, enumColumn.type, enumColumn.sortable));
        }
        columnMap = Collections.unmodifiableMap(map);
    }

    public ActivityProviderPropertyQuery(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, selectListQueryMap, selectCountQueryMap, null, columnMap);
    }
    
    @Override
    protected RowMapper<GenericPropertyModel> createModelMapper() {
        return new ModelMapper();
    }

    @Override
    protected RowMapper<Long> createCountMapper() {
        return new CountMapper();
    }

    //@Override
    protected QuerySql createWithRecursiveQuerySql(QueryCriteria criteria, DatabaseEnum database) {
        if (database == DatabaseEnum.POSTGRESQL) {
/*            String queryString = WITH_RECURSIVE_CLAUSE;
            List<Object> queryParamList = new ArrayList<>();
            queryParamList.add(criteria.getPrincipal() != null ? criteria.getPrincipal().getName() : null);
            return new QuerySql(queryString, queryParamList);*/
        }
        return null;
    }

    private static final class ModelMapper implements RowMapper<GenericPropertyModel> {

        /*
         * (non-Javadoc)
         * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
         */
        public GenericPropertyModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        	String id = rs.getString(COLUMN.ID.alias);
        	String name = rs.getString(COLUMN.NAME.alias);
        	String value = rs.getString(COLUMN.VALUE.alias);
        	GenericPropertyModel model = new GenericPropertyModel();
        	model.setId(id);
        	model.setPropertyName(name);
        	model.setPropertyValue(value);
        	return model;
        }
    }

    private static final class CountMapper implements RowMapper<Long> {
        public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
            return rs.getLong(COUNT_ALIAS);
        }
    }

}
