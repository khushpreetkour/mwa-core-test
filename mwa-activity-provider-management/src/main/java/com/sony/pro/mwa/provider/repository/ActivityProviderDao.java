package com.sony.pro.mwa.provider.repository;

import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityProviderDao extends ModelBaseDao<ActivityProviderModel, ActivityProviderCollection> {
	public ActivityProviderModel getModelByName(String name);
}
