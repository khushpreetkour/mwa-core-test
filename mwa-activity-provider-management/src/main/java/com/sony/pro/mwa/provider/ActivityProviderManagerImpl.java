package com.sony.pro.mwa.provider;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.GenericPropertyCollection;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ActivityProviderTypeModel;
import com.sony.pro.mwa.model.provider.LocalPerspectiveCollection;
import com.sony.pro.mwa.model.provider.LocalPerspectiveModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointCollection;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.provider.repository.ActivityProviderPropertyDao;
import com.sony.pro.mwa.provider.repository.LocalPerspectiveDao;
import com.sony.pro.mwa.provider.repository.ServiceEndpointDao;
import com.sony.pro.mwa.provider.repository.ActivityProviderDao;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;
import com.sony.pro.mwa.service.provider.IActivityProviderManager;
import com.sony.pro.mwa.service.provider.IActivityProvider;

@Component
public class ActivityProviderManagerImpl implements IActivityProviderManager {

	private final static MwaLogger logger = MwaLogger.getLogger(ActivityProviderManagerImpl.class);
	
	private ActivityProviderDao activityProviderDao;
	private ServiceEndpointDao serviceEndpointDao;
	private ActivityProviderPropertyDao activityProviderPropertyDao;
	private LocalPerspectiveDao localPerspectiveDao;
	
	@Autowired
	@Qualifier("activityProviderDao")
	public void setActivityProviderDao(ActivityProviderDao dao) {
		this.activityProviderDao = dao;
	}
	
	@Autowired
	@Qualifier("serviceEndpointDao")
	public void setServiceEndpointDao(ServiceEndpointDao dao) {
		this.serviceEndpointDao = dao;
	}

	@Autowired
	@Qualifier("activityProviderPropertyDao")
	public void setActivityProviderPropertyDao(ActivityProviderPropertyDao dao) {
		this.activityProviderPropertyDao = dao;
	}

	@Autowired
	@Qualifier("localPerspectiveDao")
	public void setLocalPerspectiveDao(LocalPerspectiveDao dao) {
		this.localPerspectiveDao = dao;
	}
	
	public void initialize() {
	}
	
	public void destroy() {
	}
	
	@Override
	public ActivityProviderCollection getProviders(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		if (sorts == null) {
			sorts = Arrays.asList("createTime" + "+");
		}
		
		ActivityProviderCollection models;
		try {
			models = this.activityProviderDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (models != null) {
			for (ActivityProviderModel model : models.getModels()) {
				model = setProviderDetails(model);
			}
		} else {
		}
		return models;
	}

	@Override
	public ActivityProviderModel getProvider(String providerId) {
		ActivityProviderModel model = this.activityProviderDao.getModel(providerId, null);
		return setProviderDetails(model);
	}

	@Override
	public IActivityProvider getProviderService(String providerId) {
		if (providerId == null)
			throw new MwaError(MWARC.INVALID_INPUT);
		ActivityProviderModel model = this.getProvider(providerId);
		if (model == null)
			throw new MwaError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND);
		model = setProviderDetails(model);
		ActivityProvider result = new ActivityProvider();
		result.setModel(model);
		return result;
	}
	
	protected ActivityProviderModel setProviderDetails(ActivityProviderModel model) {
		if (model != null) {
			//Set template endpoint
			ServiceEndpointCollection endpoints = this.serviceEndpointDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, Arrays.asList("providerId" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId()), null, null, null));
			model.setEndpointList(endpoints.getModels());
			//Set property
			GenericPropertyCollection properties = this.activityProviderPropertyDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, Arrays.asList("providerId" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId()), null, null, null));
			model.setPropertyList(properties.getModels());

			//Set local perspective
			LocalPerspectiveCollection perspectives = this.localPerspectiveDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, Arrays.asList("providerId" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId()), null, null, null));
			model.setLocalPerspectiveList(perspectives.getModels());

		}
		return model;
	}
/*
	protected ActivityProviderTypeModel setProviderTypeDetails(ActivityProviderTypeModel model) {
		if (model != null) {
			ServiceEndpointTypeCollection endpointTypes = this.serviceEndpointTypeDao.getModels(QueryCriteriaGenerator.getQueryCriteria(null, Arrays.asList("providerTypeId" + FilterOperatorEnum.EQUAL.toSymbol() + model.getId()), null, null, null));
			model.setEndpointTypes(endpointTypes.getModels());
		}
		return model;
	}
*/
	@Override
	@Transactional(value="transactionManagerForMwa", readOnly=false, rollbackFor=Exception.class)
	public ActivityProviderModel addProvider(ActivityProviderModel provider) {
		if (provider == null) {
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		if (provider.getId() == null || provider.getId().isEmpty()) {
			provider.setId(UUID.randomUUID().toString());
		} else {
			try {
				UUID.fromString(provider.getId());
			} catch (IllegalArgumentException e) {
				throw new MwaError(MWARC.INVALID_INPUT_FORMAT);
			}
		}

		ActivityProviderModel model = this.activityProviderDao.getModel(provider.getId(), null);
		if (model != null)
			throw new MwaError(MWARC.INVALID_INPUT_PROVIDER_ALREAD_EXISTS);
		
		model = this.activityProviderDao.addModel(provider);
		
		if (model != null) {
			//Set ActivityTemplate
			if (model.getEndpoints() != null) { 
				for (ServiceEndpointModel ep : model.getEndpointList()) {
					if (ep.getId() == null || ep.getId().isEmpty()) {
						ep.setId(UUID.randomUUID().toString());
					}
					ep.setProviderId(provider.getId());
					this.serviceEndpointDao.addModel(ep);
				}
			}
			//Set property
			if (model.getProperties() != null) {
				for (GenericPropertyModel gpm : model.getPropertyList()) {
					gpm.setId(provider.getId());
					this.activityProviderPropertyDao.addModel(gpm);
				}
			}
			if (model.getLocalPerspectiveList() != null) {
				for (LocalPerspectiveModel lpm : model.getLocalPerspectiveList()) {
					lpm.setActivityProviderId(provider.getId());
					this.localPerspectiveDao.addModel(lpm);
				}

			}
		} else {
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		return model;
	}
	
	protected ActivityProviderModel deleteEndpoints(ActivityProviderModel inModel) {
		this.serviceEndpointDao.deleteModelByProvider(inModel.getId());
		inModel.setEndpointList(null);
		return inModel;
	}
	protected ActivityProviderModel deleteProperties(ActivityProviderModel inModel) {
		this.activityProviderPropertyDao.deleteModelByProvider(inModel.getId());
		inModel.setPropertyList(null);
		return inModel;
	}
	protected ActivityProviderModel deleteLocalPerspectives(ActivityProviderModel inModel) {
		this.localPerspectiveDao.deleteModelByProvider(inModel.getId());
		inModel.setLocalPerspectiveList(null);
		return inModel;
	}
	@Override
	@Transactional(value="transactionManagerForMwa", readOnly=false, rollbackFor=Exception.class)
	public ActivityProviderModel updateProvider(ActivityProviderModel model) {
		ActivityProviderModel modelCtrl = null;
		if (model.getId() != null) { 
			//Modelのコピーインスタンスを取得。基本的にメンバの削除などはこちらに対して行い、オリジナルはそのままレスポンスで返します。
			modelCtrl = this.activityProviderDao.getModel(model.getId(), null);
		} 
		if (modelCtrl == null)
			throw new MwaError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND);
		model = this.activityProviderDao.updateModel(model);
		if (model != null) {
			modelCtrl.getId();
			//Set ActivityTemplate
			modelCtrl = this.deleteEndpoints(modelCtrl);
			if (model.getEndpoints() != null) { 
				for (ServiceEndpointModel ep : model.getEndpointList()) {
					if (ep.getId() == null || ep.getId().isEmpty()) {
						ep.setId(UUID.randomUUID().toString());
					}
					ep.setProviderId(model.getId());
					this.serviceEndpointDao.addModel(ep);
				}
			}
			//Set property
			modelCtrl = this.deleteProperties(modelCtrl);
			if (model.getProperties() != null) {
				for (GenericPropertyModel gpm : model.getPropertyList()) {
					gpm.setId(model.getId());
					this.activityProviderPropertyDao.addModel(gpm);
				}
			}
			//Set local perspective
			modelCtrl = this.deleteLocalPerspectives(modelCtrl);
			if (model.getLocalPerspectiveList() != null) {
				for (LocalPerspectiveModel lpm : model.getLocalPerspectiveList()) {
					lpm.setActivityProviderId(model.getId());
					this.localPerspectiveDao.addModel(lpm);
				}
			}
		} else {
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		return model;
	}
	
	@Override
	public ActivityProviderModel deleteProvider(String providerId) {
		ActivityProviderModel model = this.activityProviderDao.getModel(providerId, null);
		if (model != null) {
			this.activityProviderDao.deleteModel(model);
			return model;
		} else {
			throw new MwaError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND);
		}
	}

	@Override
	public ActivityProviderCollection deleteProviders(List<String> sorts, List<String> filters, Integer offset, Integer limit) {
		ActivityProviderCollection models;
		try {
			models = this.activityProviderDao.getModels(QueryCriteriaGenerator.getQueryCriteria(sorts, filters, offset, limit, null));
		} catch (Throwable e) {
			logger.error("getInstances: e=" + e.toString() + ", e.message=" + e.getMessage() + ", e.localizedMessage" + e.getLocalizedMessage(), e);
			throw new MwaError(MWARC.DATABASE_ERROR);
		}
		if (models != null) {
			for (ActivityProviderModel model : models.getModels()) {
				this.deleteProvider(model.getId());
			}
		}
		return models;
	}
	
	@Override
	public ActivityProviderTypeModel addProvidertype(ActivityProviderTypeModel arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
/*	public ActivityProviderTypeModel addProvidertype(ActivityProviderTypeModel model) {
		model.setEndpointTypes(endpointTypes);
		model.setId(UUIDUtils.generateUUID(name, modelNumber));
		model.setModelNumber(modelNumber);
		model.setName(name);
		model.setTemplateInfos(templateInfos);
		
		ActivityProviderTypeModel prevModel = this.activityProviderTypeDao.getModel(model.getId(), null);
		if (prevModel == null) { 
			this.activityProviderTypeDao.addModel(model);
		} else { 
			this.activityProviderTypeDao.updateModel(model);
		}
		return model;
	}*/
}
