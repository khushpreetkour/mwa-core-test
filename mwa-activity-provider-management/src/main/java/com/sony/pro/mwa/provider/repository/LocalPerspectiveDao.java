package com.sony.pro.mwa.provider.repository;

import com.sony.pro.mwa.model.provider.LocalPerspectiveCollection;
import com.sony.pro.mwa.model.provider.LocalPerspectiveModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface LocalPerspectiveDao extends ModelBaseDao<LocalPerspectiveModel, LocalPerspectiveCollection> {
	public int deleteModelByProvider(String providerId);
}
