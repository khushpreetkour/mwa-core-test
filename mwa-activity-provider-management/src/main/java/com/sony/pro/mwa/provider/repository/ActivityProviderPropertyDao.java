package com.sony.pro.mwa.provider.repository;

import com.sony.pro.mwa.model.GenericPropertyCollection;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.repository.ModelBaseDao;

public interface ActivityProviderPropertyDao extends ModelBaseDao<GenericPropertyModel, GenericPropertyCollection> {
	public int deleteModelByProvider(String providerId);
}
