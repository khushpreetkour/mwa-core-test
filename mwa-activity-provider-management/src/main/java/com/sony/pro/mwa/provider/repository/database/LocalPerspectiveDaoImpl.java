package com.sony.pro.mwa.provider.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.model.provider.LocalPerspectiveCollection;
import com.sony.pro.mwa.model.provider.LocalPerspectiveModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.provider.repository.LocalPerspectiveDao;
import com.sony.pro.mwa.provider.repository.database.query.LocalPerspectiveQuery;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;

@Repository
public class LocalPerspectiveDaoImpl extends DatabaseBaseDao<LocalPerspectiveModel, LocalPerspectiveCollection, QueryCriteria> implements LocalPerspectiveDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public LocalPerspectiveCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public LocalPerspectiveModel getModel(String id, Principal principal) {
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
		//return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.location_perspective (" +
					"" +
					"location_id" +
					", location_perspective_name" +
					", location_perspective_base_path" +
					", location_perspective_user" +
					", location_perspective_password" +
					", location_perspective_activity_provider_id" +
					
					") " +
			"VALUES (" +
					"(SELECT location_id FROM mwa.location WHERE location_name = ?), ?, ?, ?, ?, ?" +
					") ";

	public LocalPerspectiveModel addModel(final LocalPerspectiveModel model) {
		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;

						ps.setString(colNum++, model.getLocationName());
						ps.setString(colNum++, model.getPerspective());
						ps.setString(colNum++, model.getLocalBasePath());
						if (model.getUser() != null) 
							ps.setString(colNum++, model.getUser());
						else
							ps.setNull(colNum++, Types.VARCHAR);
						if (model.getPassword() != null) 
							ps.setString(colNum++, model.getPassword());
						else
							ps.setNull(colNum++, Types.VARCHAR);
						ps.setObject(colNum++, UUID.fromString(model.getActivityProviderId()));
						
						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);

		return insertedRowCount != 0 ? model : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.location_perspective " +
			"SET " +
					"location_perspective_base_path = ? " +
					"location_perspective_user = ? " +
					"location_perspective_password = ? " +
			"WHERE " +
					"location_perspective_activity_provider_id = ? " + "AND " +
					"location_id = (SELECT location_id FROM mwa.location WHERE location_name = ?) " + "AND " +
					"location_perspective_name = ? " + "AND " +
					"TRUE";

	public LocalPerspectiveModel updateModel(final LocalPerspectiveModel model) {
		final String updateSql = UPDATE_SQL;
		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(updateSql);
						int colNum = 1;

						ps.setString(colNum++, model.getLocalBasePath());
						ps.setString(colNum++, model.getUser());
						ps.setString(colNum++, model.getPassword());
						
						ps.setObject(colNum++, UUID.fromString(model.getActivityProviderId()));
						ps.setString(colNum++, model.getLocationName());
						ps.setString(colNum++, model.getPerspective());
						
						return ps;
					}
				});

		return updatedRowCount != 0 ? model : null;
	}


	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.location_perspective " +
			"WHERE " +
					"location_perspective_activity_provider_id = ? " + "AND " +
					"location_id = (SELECT location_id FROM mwa.location WHERE location_name = ?) " + "AND " +
					"location_perspective_name = ? " + "AND " +
					"TRUE";

	public LocalPerspectiveModel deleteModel(LocalPerspectiveModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{
				UUID.fromString(model.getActivityProviderId()),
				model.getLocationName(),
				model.getPerspective()
		};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}
	
	private static final String DELETE_SQL_BY_PROVIDER_ID =
			"DELETE FROM " +
					"mwa.location_perspective " +
			"WHERE " +
					"location_perspective_activity_provider_id = ? " + "AND " +
					"TRUE";
	public int deleteModelByProvider(String providerId) {
		Object[] paramArray = new Object[]{
				UUID.fromString(providerId)
				};
		return jdbcTemplate.update(DELETE_SQL_BY_PROVIDER_ID, paramArray);
	}

	@Override
	protected IQuery<LocalPerspectiveModel, LocalPerspectiveCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new LocalPerspectiveQuery(jdbcTemplate);
	}

}
