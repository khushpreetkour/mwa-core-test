package com.sony.pro.mwa.provider.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Arrays;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.provider.repository.ActivityProviderDao;
import com.sony.pro.mwa.provider.repository.database.query.ActivityProviderQuery;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.database.DatabaseEnum;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ActivityProviderDaoImpl extends DatabaseBaseDao<ActivityProviderModel, ActivityProviderCollection, QueryCriteria> implements ActivityProviderDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public ActivityProviderCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public ActivityProviderModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}
	public ActivityProviderModel getModelByName(String name) {
		return super.collection(QueryCriteriaGenerator.getQueryCriteria(null, Arrays.asList("name" + FilterOperatorEnum.EQUAL.toSymbol() + name), null, null, null)).first();
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.activity_provider (" +
					"activity_provider_id" +
					", activity_provider_type_id" +
					", activity_provider_name" +
					", activity_provider_active" +
					", activity_provider_exposure" +
					", activity_provider_create_time" +
					", activity_provider_update_time" +
					") " +
			"VALUES (" +
					"?" +
					", (SELECT activity_provider_type_id FROM mwa.activity_provider_type WHERE activity_provider_type_name = ?) " +
					", ?, ?, ?, ?, ?" +
					") ";
	private static final String INSERT_SQL_FOR_POSTGRES =
			"INSERT INTO mwa.activity_provider (" +
					"activity_provider_id" +
					", activity_provider_type_id" +
					", activity_provider_name" +
					", activity_provider_active" +
					", activity_provider_exposure" +
					", activity_provider_create_time" +
					", activity_provider_update_time" +
					") " +
			"VALUES (" +
					"?" +
					", (SELECT activity_provider_type_id FROM mwa.activity_provider_type WHERE activity_provider_type_name = ? " + "offset 0 limit 1" + ") " +
					", ?, ?, ?, ?, ?" +
					") ";

	public ActivityProviderModel addModel(final ActivityProviderModel model) {
		DatabaseEnum database = DatabaseEnum.getDatabase(jdbcTemplate.getDataSource());
		final String insertSql = (database.equals(DatabaseEnum.POSTGRESQL)) ? INSERT_SQL_FOR_POSTGRES : INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;

						ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getProviderTypeName());
						ps.setString(colNum++, model.getName());
						ps.setBoolean(colNum++, model.getActive());
						ps.setBoolean(colNum++, model.getExposure());
						model.setCreateTime(System.currentTimeMillis());
						model.setUpdateTime(model.getCreateTime());
						ps.setTimestamp(colNum++, new Timestamp(model.getCreateTime()));
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));

						
						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);

		ActivityProviderModel addedInstance = model;
		return insertedRowCount != 0 ? addedInstance : null;
	}

	private static final String UPDATE_SQL =
			"UPDATE " +
					"mwa.activity_provider " +
			"SET " +
					"activity_provider_type_id = (SELECT activity_provider_type_id FROM mwa.activity_provider_type WHERE activity_provider_type_name = ?) " +
					", activity_provider_name = ? " +
					", activity_provider_active = ? " +
					", activity_provider_exposure = ? " +
					", activity_provider_update_time = ? " +
			"WHERE " +
					"activity_provider_id = ? ";

	public ActivityProviderModel updateModel(final ActivityProviderModel model) {
		final String updateSql = UPDATE_SQL;
		int updatedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(updateSql);
						int colNum = 1;

						ps.setString(colNum++, model.getProviderTypeName());
						ps.setString(colNum++, model.getName());
						ps.setBoolean(colNum++, model.getActive());
						ps.setBoolean(colNum++, model.getExposure());
						model.setUpdateTime(System.currentTimeMillis());
						ps.setTimestamp(colNum++, new Timestamp(model.getUpdateTime()));
						
						ps.setObject(colNum++, UUID.fromString(model.getId()));

						return ps;
					}
				});

		return updatedRowCount != 0 ? model : null;
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.activity_provider " +
			"WHERE " +
					"activity_provider_id = ? ";

	public ActivityProviderModel deleteModel(ActivityProviderModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{UUID.fromString(model.getId())};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}

	@Override
	protected IQuery<ActivityProviderModel, ActivityProviderCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ActivityProviderQuery(jdbcTemplate);
	}


}
