package com.sony.pro.mwa.provider.repository.database;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.GenericPropertyCollection;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.provider.LocalPerspectiveModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.provider.repository.ActivityProviderPropertyDao;
import com.sony.pro.mwa.provider.repository.database.query.ActivityProviderPropertyQuery;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.repository.database.DatabaseBaseDao;
import com.sony.pro.mwa.repository.query.IQuery;
import com.sony.pro.mwa.repository.query.QueryCriteria;
import com.sony.pro.mwa.repository.query.criteria.QueryCriteriaGenerator;

@Repository
public class ActivityProviderPropertyDaoImpl extends DatabaseBaseDao<GenericPropertyModel, GenericPropertyCollection, QueryCriteria> implements ActivityProviderPropertyDao {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	public GenericPropertyCollection getModels(QueryCriteria criteria) {
		return super.collection(criteria);
	}

	public GenericPropertyModel getModel(String id, Principal principal) {
		return super.first(QueryCriteriaGenerator.getQueryCriteriaById(id, principal));
	}

	private static final String INSERT_SQL =
			"INSERT INTO mwa.activity_provider_property (" +
					"activity_provider_id" +
					", activity_provider_property_name" +
					", activity_provider_property_value" +
					") " +
			"VALUES (" +
					"?, ?, ?" +
					") ";

	@Override
	public GenericPropertyModel addModel(final GenericPropertyModel model) {
		final String insertSql = INSERT_SQL;
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int insertedRowCount = jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
						PreparedStatement ps = conn.prepareStatement(insertSql, new String[] {});
						int colNum = 1;

						ps.setObject(colNum++, UUID.fromString(model.getId()));
						ps.setString(colNum++, model.getPropertyName());
						ps.setString(colNum++, model.getPropertyValue());
						
						logger.debug(((org.apache.commons.dbcp.DelegatingPreparedStatement)ps).getDelegate().toString());
						return ps;
					}
				},
				keyHolder);

		GenericPropertyModel addedInstance = model;
		return insertedRowCount != 0 ? addedInstance : null;
	}

	@Override
	public GenericPropertyModel updateModel(GenericPropertyModel model) {
		logger.error("ActivityProviderPropertyDaoImpl.updateModel is unsupported.");
		throw new MwaError(MWARC.SYSTEM_ERROR_IMPLEMENTATION);
	}

	private static final String DELETE_SQL =
			"DELETE FROM " +
					"mwa.activity_provider_property " +
			"WHERE " +
					"activity_provider_id = ? " + "AND " +
					"activity_provider_property_name = ? " + "AND " +
					"TRUE";
	
	public GenericPropertyModel deleteModel(GenericPropertyModel model) {
		String deleteSql = DELETE_SQL;

		Object[] paramArray = new Object[]{
				UUID.fromString(model.getId()),
				model.getPropertyName()
		};
		if (jdbcTemplate.update(deleteSql, paramArray) == 1) {
			return model;
		}
		return null;
	}
	
	private static final String DELETE_SQL_BY_PROVIDER_ID =
			"DELETE FROM " +
					"mwa.activity_provider_property " +
			"WHERE " +
					"activity_provider_id = ? " + "AND " +
					"TRUE";
	public int deleteModelByProvider(String providerId) {
		Object[] paramArray = new Object[]{
				UUID.fromString(providerId)
				};
		return jdbcTemplate.update(DELETE_SQL_BY_PROVIDER_ID, paramArray);
	}


	@Override
	protected IQuery<GenericPropertyModel, GenericPropertyCollection, QueryCriteria> createQuery(JdbcTemplate jdbcTemplate) {
		return new ActivityProviderPropertyQuery(jdbcTemplate);
	}

}
