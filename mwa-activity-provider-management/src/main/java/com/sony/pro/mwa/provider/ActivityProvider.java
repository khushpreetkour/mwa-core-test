package com.sony.pro.mwa.provider;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.enumeration.provider.PresetProviderProperties;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.LocalPerspectiveModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.service.provider.IActivityProvider;

public class ActivityProvider implements IActivityProvider {

	ActivityProviderModel model;
	
	public ActivityProviderModel getModel() {
		return model;
	}

	@Override
	public void setModel(ActivityProviderModel model) {
		this.model = model;
	}

	@Override
	public String getId() {
		return model.getId();
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return model.getName();
	}

	@Override
	public Boolean getActive() {
		// TODO Auto-generated method stub
		return model.getActive();
	}

	@Override
	public Boolean getExposure() {
		// TODO Auto-generated method stub
		return model.getExposure();
	}

	@Override
	public String getProviderTypeName() {
		// TODO Auto-generated method stub
		return model.getProviderTypeName();
	}

	@Override
	public List<GenericPropertyModel> getPropertyList() {
		// TODO Auto-generated method stub
		return model.getPropertyList();
	}

	@Override
	public List<ServiceEndpointModel> getEndpointList() {
		// TODO Auto-generated method stub
		return model.getEndpointList();
	}

	@Override
	public Map<String, Object> getProperties() {
		// TODO Auto-generated method stub
		return model.getProperties();
	}

	@Override
	public Map<String, ServiceEndpointModel> getEndpoints() {
		// TODO Auto-generated method stub
		return model.getEndpoints();
	}

	@Override
	public List<LocalPerspectiveModel> getLocalPerspectiveList() {
		// TODO Auto-generated method stub
		return this.model.getLocalPerspectiveList();
	}

	public void setLocalPerspectiveList(List<LocalPerspectiveModel> localPerspectives) {
		// TODO Auto-generated method stub
		this.model.setLocalPerspectiveList(localPerspectives);
	}

	@Override
	public String getDefaultPerspective() {
		if (this.getProperties() != null)
			return (String)this.getProperties().get(PresetProviderProperties.defaultPerspective.getKey());
		else
			return null;
	}
}
