package com.sony.pro.mwa;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sony.pro.mwa.service.provider.IActivityProviderManager;


public class InternalTest {

	protected IActivityProviderManager activityProviderManager;

	@Autowired
	public void setActivityProviderManager(IActivityProviderManager serviceProviderManager) {
		this.activityProviderManager = serviceProviderManager;
	}
	public IActivityProviderManager getActivityProviderManager() {
		return this.activityProviderManager;
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("mwa-app-context.xml");
		ctx.getAutowireCapableBeanFactory().autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, true);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test() {
		
/*		ActivityProviderModel spm =  new ActivityProviderModel();
		spm.setActive(false);
		spm.setExposure(true);
		spm.setId("0000cc04-11b7-6c2b-6241-679f9be16da9");
		spm.setName("TEST-PROVIDER");
		{
			List<ActivityTemplateEndpointModel> templateEndpoints = new ArrayList<>();
			List<ServiceEndpointModel> endpoints= new ArrayList<>();
			
			ServiceEndpointModel se1 = new ServiceEndpointModel();
			se1.setType("TEST-EP1");
			se1.setHost("localhost");
			se1.setPort(8001);
			se1.setUser("TEST-USER");
			se1.setPassword("TEST-PASS");
			endpoints.add(se1);
			
			ServiceEndpointModel se2 = new ServiceEndpointModel();
			se2.setType("TEST-EP2");
			se2.setHost("localhost");
			se2.setPort(8002);
			se2.setUser("TEST-USER");
			se2.setPassword("TEST-PASS");
			endpoints.add(se2);

			ActivityTemplateEndpointModel atc = new ActivityTemplateEndpointModel();
			atc.setEndpointList(endpoints);
			atc.setTemplateId("06c25acf-237a-6c6f-4dc0-10eff3d93a56");
			templateEndpoints.add(atc);
			spm.setTemplateEndpointList(templateEndpoints);
		}
		{
			List<GenericPropertyModel> list = new ArrayList<>();
			GenericPropertyModel model1 = new GenericPropertyModel();
			model1.setPropertyName("TEST-KEY-1-1");
			model1.setPropertyValue("TEST-VALUE-1");
			list.add(model1);
			GenericPropertyModel model2 = new GenericPropertyModel();
			model2.setPropertyName("TEST-KEY-1-2");
			model2.setPropertyValue("TEST-VALUE-2");
			list.add(model2);
			spm.setPropertyList(list);
		}
		
		this.activityProviderManager.addProvider(spm);
		
		ActivityProvider sp = this.activityProviderManager.getProvider(spm.getId());
		System.out.println("sp=" + sp.getName());
		
		spm.setName("RENAMED-TEST-PROVIDER");
		{
			List<ActivityTemplateEndpointModel> templateEndpoints = new ArrayList<>();
			List<ServiceEndpointModel> endpoints= new ArrayList<>();
			
			ServiceEndpointModel se1 = new ServiceEndpointModel();
			se1.setType("TEST-EP1");
			se1.setHost("localhost");
			se1.setPort(8001);
			se1.setUser("TEST-USER");
			se1.setPassword("TEST-PASS");
			endpoints.add(se1);

			ActivityTemplateEndpointModel atc = new ActivityTemplateEndpointModel();
			atc.setEndpointList(endpoints);
			atc.setTemplateId("06c25acf-237a-6c6f-4dc0-10eff3d93a56");
			templateEndpoints.add(atc);
			spm.setTemplateEndpointList(templateEndpoints);
		}
		this.activityProviderManager.updateProvider(spm);

		sp = this.activityProviderManager.getProvider(spm.getId());
		System.out.println("sp=" + sp.getName());
		
		this.activityProviderManager.deleteProvider(spm.getId());
		
		sp = this.activityProviderManager.getProvider(spm.getId());
		if (sp == null) 
			System.out.println("sp=" + null);*/
	}
}
